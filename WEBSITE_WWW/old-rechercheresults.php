<?php
$annee = explode("-", $annonces->annee);
$annee = $annee[1].".".$annee[0];
$expertisee = explode("-", $annonces->expertisee);
$expertisee = $expertisee[1].".".$expertisee[0];
if($annonces->ville == "")
{
	$requete_ville = mysqli_query($connect1, "SELECT ville FROM liste_npa WHERE code_postal='$annonces->codepostal'");
	$ville = mysqli_fetch_object($requete_ville);
	$ville = $ville->ville;
}
else
	$ville = $annonces->ville;
$canton = $annonces->canton;
?>
			<div id="annonce">
			<div style="float:left;width:180px;height:135px;position:relative;">
				<img src="<?PHP if($annonces->image != "") echo "photos/".$annonces->image; else echo "images/photo-nondispo.png";?>" style="width:180px;height:135px;z-index:10;"/>
				<img src="<?PHP if(preg_match("/www.fahrzeugnet.ch/", $annonces->url)) echo "images/plateforme-fahrzeugnet.png"; else if(preg_match("/topannonces.ch/", $annonces->url)) echo "images/plateforme-topannonces.png";?>" style="position:absolute;bottom:0;right:0;z-index:11;"/>
			</div>
			<div style="float:left;width:492px;height:40px;padding-left:10px;vertical-align:top;color:#000000;background-color:#dae5f2;margin-bottom:10px;line-height:20px;">
			<span style="float:left;width:242px;font-weight:bold;font-size:16px;color:#009ada;"><?PHP echo $annonces->marque." ".$annonces->modele;?></span>
			<span style="float:left;width:50px;font-size:14px;margin-left:10px;text-align:middle;"><?PHP if($annee != "00.0000") echo $annee; else echo "&nbsp;";?></span>
			<span style="float:left;width:70px;font-size:14px;margin-left:10px;text-align:right;"><?PHP echo $annonces->kilometres;?> km</span>
			<span style="float:left;width:100px;font-weight:bold;font-size:16px;margin-left:10px;text-align:right;color:#009ada;"><?PHP echo "CHF ".$annonces->prix.".-";?></span>
			</div>
			<div style="float:left;width:482px;margin-left:10px;vertical-align:top;color:#000000;font-size:14px;margin-bottom:10px;">
			<span style="float:left;width:160.66px;"><?PHP if($annonces->cylindrees != 0) echo $annonces->cylindrees." cm3";?></span>
			<span style="float:left;width:160.66px;"><?PHP if($annonces->puissancechevaux != 0) echo $annonces->puissancechevaux." chevaux";?></span>
			<span style="float:left;width:160.66px;"><?PHP if($annonces->carburant != "") echo $annonces->carburant;?></span>
			</div>
			<div style="float:left;width:482px;margin-left:10px;vertical-align:top;color:#000000;font-size:14px;">
			<span style="float:left;width:160.66px;"><?PHP if($annonces->transmission != "") echo "Boite ".$annonces->transmission;?></span>
			<span style="float:left;width:160.66px;">Expertisee:<br><?PHP if($expertisee != "00.0000") echo $expertisee; else echo "Non";?></span>
			<span style="float:left;width:160.66px;"><?PHP echo $annonces->codepostal." ".$ville." (".$canton.")";?></span><br/>
			</div></a>
			</div><div style="clear:both;margin-bottom:20px;"></div>

  <!-- RESULTATS -->
  <a href="<?PHP echo $annonces->url;?>"><div class="col-sm-10 resultcars">

  <!-- LIGNE RESLUTAT -->
    <div class="col-sm-12 cars">
    <div class="col-xs-2">
    <img src="<?PHP if($annonces->image != "") echo "photos/".$annonces->image; else echo "images/photo-nondispo.png";?>" class="img-responsive" width="180" height="135"/>
    </div>
    <div class="col-xs-10">
   <div class="col-xs-12 cardesc1">
   <div class="col-xs-3"><h5>AC ACECA</h5></div><div class="col-xs-3 active"><h5>01.1955</h5></div><div class="col-xs-3 active"><h5>2'000 Km</h5></div><div class="col-xs-3 active"><h5>CHF 185'000</h5></div>
   </div>
   <div class="col-xs-12 cardesc2">
   <div class="col-xs-4">1991 cm3</div><div class="col-xs-4">87 chevaux</div><div class="col-xs-4">Essence</div>
   </div>
   <div class="col-xs-12 cardesc3">
   <div class="col-xs-4">Bo�te : Manuelle</div><div class="col-xs-4">Expertis�e : 05.2014</div><div class="col-xs-4">4132 Muttenz (BL)</div>
   </div>
    </div>
    </div>

  </div></a>