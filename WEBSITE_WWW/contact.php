<?php
require_once('vendor/autoload.php');
require_once('classes/email.php');

include("header.php");
include("body.php");
$ok = 1;
if(!isset($_POST['nom']))
    $ok = 0;
else if((isset($_POST['nom'])) && ($_POST['nom'] == ""))
    $ok = 0;
else if((isset($_POST['prenom'])) && ($_POST['prenom'] == ""))
    $ok = 0;
else if((isset($_POST['email'])) && ($_POST['email'] == ""))
    $ok = 0;
else if((isset($_POST['message'])) && ($_POST['message'] == ""))
	$ok = 0;
	
	

$secret = "6LfUhlUUAAAAAArLszhTtTYcHQTHSKfMIT5d1kmi";
$gRecaptchaResponse = (isset($_POST['g-recaptcha-response'])) ? $_POST['g-recaptcha-response'] : '';
$recaptcha = new \ReCaptcha\ReCaptcha($secret);
$resp = $recaptcha->verify($gRecaptchaResponse, $_SERVER['REMOTE_ADDR']);
if (!$resp->isSuccess()){
	$ok = 0;
}


if($ok == 1)
{
    $mail = email::init();

    $mail->setFrom('contact@autospot.ch', 'AutoSpot');
    $mail->addAddress('contact@autospot.ch');
    $mail->addReplyTo($_POST['email'], $_POST['prenom'].' '.$_POST['nom']);
    $mail->isHTML(true);
    $mail->CharSet = 'UTF-8';
    $mail->Subject = "Message de ".$_POST['prenom']." ".$_POST['nom']." sur AutoSpot.ch";
    $mail->Body = $_POST['message'];
	$mail->send();
}

?>
<!-- Contact -->
<div class="container-fluid">

<div class="contact">
    <h3 class="center">Nous contacter</h3>

    <form class="form-horizontal" role="form" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <?php if ($ok == 1) : ?>
            <div class="form-group">
                <label class="col-sm-2"></label>
                <div class="col-sm-10">
                    <div class="alert alert-success" role="alert">
                        Votre message a bien été expédié. Nous vous répondrons dans les plus brefs délais.
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
          <label class="control-label col-sm-2" for="nom">Nom:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="nom" name="nom" placeholder="Entrez votre nom" value="<?PHP if((isset($_POST['nom'])) && ($ok == 0)) echo $_POST['nom'];?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="prenom">Prénom:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Entrez votre prénom" value="<?PHP if((isset($_POST['prenom'])) && ($ok == 0)) echo $_POST['prenom'];?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="email">Email:</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="Entrez votre email" value="<?PHP if((isset($_POST['email'])) && ($ok == 0)) echo $_POST['email'];?>">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="message">Message:</label>
          <div class="col-sm-10">
            <textarea id="message" name="message" class="form-control" rows=10 placeholder="Entrez votre message" value="<?PHP if((isset($_POST['message'])) && ($ok == 0)) echo $_POST['message'];?>"></textarea>
          </div>
        </div>
		
		<div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
		  <div class="g-recaptcha" data-sitekey="6LfUhlUUAAAAADFtz6TSa-0HVxBQYmxATuHJLlNt"></div>
          </div>
        </div>
		
		
        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Envoyer votre message</button>
          </div>
        </div>
		
		
		
      </form>
</div>
  
<!-- END Contact -->    
</div>
<?php include("footer.php"); ?>