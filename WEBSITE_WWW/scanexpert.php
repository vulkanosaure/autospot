<?php
session_start();
include("header.php");
include("body.php");
?>
<!-- FORFAIT ScanExpert -->
<div class="clear"></div>
<div class="container">
<div class="container center-block">

<div class="carteForfait maxW">
<div class="carteHead visio">
<p>ScanExpert</p>
</div>
<!-- modif--------->
<div class="carteBody" style="text-align:justify; text-justify:inter-word;">
<p>Suivant un cahier des charges précis, nos garages partenaires effectuent, pour le forfait ScanExpert, 82 points de contrôle structurés en 9 catégories :</p><br />
<!-- update -->
<?php include_once('scanexpert_controles.php'); ?>
<p>&nbsp;</p></div>
<div class="carteFoot visio">
    <span>&nbsp;</span>
    <span class="prixChoixForfait">199.-</span>
</div>
</div>
            
</div>
<div class="clear"></div>
</div>

<script type="text/javascript">
    jQuery('.scanexpert-controles-toggle').on('click', function(e) {
        $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle')
    });
    jQuery('.testRoutierControl').off('click');
    jQuery('.testRoutierControl').on('click', function(e) {
        $(this).toggleClass('fa-plus-circle fa-minus-circle')
    });
</script>

<?php include("footer.php"); ?>