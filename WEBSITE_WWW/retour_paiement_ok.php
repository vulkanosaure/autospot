<?php
include_once('db_connexion.php');
require_once('vendor/autoload.php');
require_once('classes/mailer.php');
require_once('classes/demande.php');
require_once('classes/client.php');
require_once('classes/annonce.php');
require_once('classes/commande_rapport.php');

require_once('appbackend/admin/include/db_connect.php');
require_once('appbackend/events.php');

$type = array_key_exists('type', $_GET) && !empty($_GET['type']) ? trim($_GET['type']) : null;
$id = array_key_exists('id', $_GET) && intval($_GET['id']) != 0 ? intval($_GET['id']) : null;

if ($type == 'forfait' && $id !== null) {   
    $sql = sprintf('SELECT * FROM demandes_exam WHERE id="%s";', mysqli_real_escape_string($connect1, $id));
    $query = mysqli_query($connect1, $sql);
    $demande = mysqli_fetch_object($query);

    if ($demande !== false) {
        if ($demande->payment_status == demande::PAYMENT_PROCESSING || $demande->payment_status == NULL) {
            $sql = sprintf(
                'UPDATE demandes_exam SET payment_status="%s" WHERE id = "%s"',
                mysqli_real_escape_string($connect1, demande::PAYMENT_ACCEPTED),
                mysqli_real_escape_string($connect1, $demande->id)
            );
						mysqli_query($connect1, $sql);   
						
						//send emails/sms
						//new : move into this condition for one call max
						new_demande($dbh, $id);
				}
				

        header('Location: je-choisis-mon-forfait.php?id='.$demande->id_annonces_clients.'&step5=1');
        exit;
    }
} elseif ($type == 'rapport' && $id !== null) {
    $commande = commande_rapport::getById($id);

    if ($commande !== null) {
        $client = client::getById($commande->id_client);
        $demande = demande::getById($commande->id_demande_exam);

        if ($demande !== null && $client !== null && $commande->paiement_status == commande_rapport::PAIEMENT_PROCESSING) {
            $sql = sprintf(
                'UPDATE commandes_rapports_inspection SET paiement_status="%s" WHERE id = "%s"', 
                mysqli_real_escape_string($connect1, commande_rapport::PAIEMENT_ACCEPTED),
                mysqli_real_escape_string($connect1, $commande->id)
            );
            $updated = mysqli_query($connect1, $sql);

            if ($updated) {
                $annonce = annonce::getById($demande->id_annonces_clients);

                $date_inspection = demande::getCompletedDate($demande);
                $date_demande = \DateTime::createFromFormat('Y-m-d H:i:s', $demande->demande_ts, new \DateTimeZone('Europe/Paris'));
								
                // Send email
                $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
                $twig = new \Twig_Environment($loader);
                $body = $twig->render('rapport_inspection.html.twig', [
                    'id_demande' => $commande->id_demande_exam, 
                    'prenom' => $client->prenom,
                    'nom' => $client->nom,
                    'date_demande' => $date_demande->format('d/m/Y'),
                    'heure_demande' => $date_demande->format('H:i'),
                    'date_inspection' => $date_inspection->format('d/m/Y'),
                    'heure_inspection' => $date_inspection->format('H:i'),
                    'marque' => $annonce->marque,
                    'modele' => $annonce->modele,
                ]);
                
                $mail = mailer::init();

                $mail->setFrom('contact@autospot.ch', 'AutoSpot');
                $mail->addAddress($client->email);
                
                $mail->Subject = 'Autospot.ch - Rapport d\'inspection technique';
                $mail->Body = $body;
                $mail->send();
            }
        }

        header('Location: rapport_inspection.php?id='.$commande->id_demande_exam.'&id_commande='.$commande->id.'&step5');
        exit;
    }
}

header('Location: index.php');
exit;