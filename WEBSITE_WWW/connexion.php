<?php
session_start();
require_once('vendor/autoload.php');
include('db_connexion.php');
require_once('classes/PHPMailer2.php');


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



$msg = new \Plasticbrain\FlashMessages\FlashMessages();

$referer = array_key_exists('referer', $_GET) && !empty($_GET['referer']) ? trim($_GET['referer']) : 'inscription.php';

if (count($_POST) > 0)
{
    $action = array_key_exists('action', $_POST) && !empty($_POST['action']) ? trim($_POST['action']) : null;

    if ($action == 'signup') {

		$secret = "6LfUhlUUAAAAAArLszhTtTYcHQTHSKfMIT5d1kmi";
		$gRecaptchaResponse = (isset($_POST['g-recaptcha-response'])) ? $_POST['g-recaptcha-response'] : '';
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
		$resp = $recaptcha->verify($gRecaptchaResponse, $_SERVER['REMOTE_ADDR']);
		if (ENVIRONMENT == 'prod' && !$resp->isSuccess()) {
			$errors = $resp->getErrorCodes();
			$msg->error('Veuillez valider le filtre antispam s\'il vous plaît.');
            header('Location: '.$referer);
            exit;
			}
		
        $sunom = mysqli_real_escape_string($connect1, $_POST['sunom']);
        $suprenom = mysqli_real_escape_string($connect1, $_POST['suprenom']);
        $suemail = mysqli_real_escape_string($connect1, $_POST['suemail']);
        $adresse = mysqli_real_escape_string($connect1, $_POST['adresse']);
        $numero = mysqli_real_escape_string($connect1, $_POST['numero']);
        $codePostal = mysqli_real_escape_string($connect1, $_POST['codePostal']);
        $ville = mysqli_real_escape_string($connect1, $_POST['ville']);
				$type_client = mysqli_real_escape_string($connect1, $_POST['type_client']);
        $sid = uniqid('', true);
        $supwd = md5($_POST['supwd']);
				$date = date("Y-m-d H:i:s");
				$tel = $_POST["telephone"];
				
        $requete = mysqli_query(
            $connect1,
            sprintf('SELECT id FROM clients WHERE email = "%s"', $suemail)
        );
        $client = mysqli_fetch_object($requete);

        if ($client === null) {
            mysqli_query(
                $connect1,
                "INSERT INTO clients (sid, lang, lang2, date, nom, prenom, adresse, adresse_num, code_postal, ville, email, mot_de_passe, type, telephone) VALUES ('$sid', '$_SESSION[lang]', '$_SESSION[lang]', '$date', '$sunom', '$suprenom', '$adresse', '$numero', '$codePostal', '$ville', '$suemail', '$supwd', $type_client, '$tel')"
						);
						
						$error = mysqli_error($connect1);
						
						
            $requete = mysqli_query(
                $connect1,
                "SELECT id FROM clients WHERE email='$suemail' && mot_de_passe='$supwd'"
            );
            $clients = mysqli_fetch_assoc($requete);
            $_SESSION['id_client'] = $clients['id'];

            $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
            $twig = new \Twig_Environment($loader);

            if ($_GET['ref'] == 'choisir-forfait') {
                $body = $twig->render('inscription.html.twig', [
                    'url' => 'http://www.autospot.ch/inscription_confirmation.php?key='. md5($suemail) .'&ref=choisir-forfait&step='. $_GET['step'] .'&annonce='. $_GET['annonce']
                ]);
            }else{
                $body = $twig->render('inscription.html.twig', ['url' => sprintf('http://www.autospot.ch/inscription_confirmation.php?key=%s', md5($suemail))]);
            }

            $mail = new PHPMailer2();

            $mail->setFrom('contact@autospot.ch', 'AutoSpot');
            $mail->addAddress($suemail);
            $mail->addReplyTo('no-reply@autospot.ch', 'Ne pas répondre');
            $mail->isHTML(true);

            // Peut servir pour ajouter une signature DKIM si le problème de spam sur hotmail persiste.. ???
            // $mail->DKIM_domain = "autospot.ch";
            // $mail->DKIM_private =$_SERVER['DOCUMENT_ROOT']."/dkim-private.pem";
            // $mail->DKIM_selector = "mail";
            // $mail->DKIM_passphrase = "";
            // $mail->DKIM_identifier = $mail->From;

            $mail->Subject = 'Confirmez votre inscription';
            $mail->Body = $body;
            $mail->send();
        } else {
            $msg->error('Un compte utilisateur existe déjà pour cette adresse email !');
        }
    } elseif($action == 'login') {
        $sipseudo = mysqli_real_escape_string($connect1, $_POST['sipseudo']);
        $sipwd = md5($_POST['sipwd']);
        $requete = mysqli_query($connect1, "SELECT id FROM clients WHERE email='$sipseudo' && mot_de_passe='$sipwd'");
        $clients = mysqli_fetch_assoc($requete);

        if (isset($clients['id'])) {
            $_SESSION['id_client'] = $clients['id'];
        } else {
            $msg->error('Identifiants incorrects');
        }
    }
}

if (ENVIRONMENT == 'prod' || true) header('Location: '.$referer);
exit;