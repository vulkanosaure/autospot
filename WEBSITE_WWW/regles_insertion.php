<?PHP
session_start();
include("header.php");
include("body.php");
?>


<style>
	li{
		margin-left:65px;
		font-size:16px !important;
	}
</style>


<!-- RÈGLES D'INSERTION -->
<div class="container">
<h3 class="center">Règles d'insertion</h3>

<div id="regles" class="container center-block">

<h4 class='subtitle_legals'>Introduction</h4>
<p>Avant chaque publication d'annonce, notre service qualité contrôle si les règles d'insertion ci-dessous ont bien été respectées. Si cela ne devait pas être le cas, votre annonce ne serait pas publiée. Dans ce cas, vous seriez immédiatement informé par email pour que vous corrigiez votre annonce dans les 24 heures. Si vous ne le faite pas dans le laps de temps indiqué, votre annonce ne sera pas publiée.</p>
<p>L'utilisateur est seul responsable de l'utilisation qu'il fait du site et notamment des contenus qu'il pourrait publier sur le site. <br />
En publiant une annonce sur le site, l'utilisateur doit disposer de tous les droits et autorisations nécessaire à la diffusion de l'annonce. De plus, il s'engage à ce que ses contenus soient licites, ne portent pas atteinte à l'ordre public et n'enfreignent pas les lois suisses.</p>

<h4 class='subtitle_legals'>Véhicule</h4>

<ul>
	<li>Un véhicule ne peut posséder qu'une seule annonce à la fois.</li>
	<li>Le vendeur qui insère l'annonce doit effectivement disposer du véhicule.</li>
	<li>Le vendeur ainsi que le véhicule proposé doivent se trouver en Suisse.</li>
	<li>Lors de l'insertion de son annonce, le vendeur doit vérifier que l'adresse postal qui s'affiche à l'étape 4 correspond à l'adresse où se situe le véhicule.</li>
	<li>Lors de l'insertion de son annonce, le vendeur doit vérifier que son numéro de téléphone portable est correct.</li>
	<li>Les données relatives au véhicule figurant dans l'annonce doivent être conformes à la réalité.</li>
	<li>Le vendeur est tenu de supprimer son annonce aussitôt son véhicule vendu.</li>
	
</ul>


<h4 class='subtitle_legals'>Photos</h4>

<ul>
	<li>Les photos du véhicule doivent être conformes à la réalité (pas de photo provenant de catalogue ou d'une autre annonce). Les photos ne doivent pas être modifiées (sauf pour en améliorer la qualité).</li>
	<li>Selon notre charte qualité, nous exigeons au minimum 1 photo du véhicule. Pour vendre votre véhicule plus rapidement, nous vous conseillons d'ajouter à votre annonce au minimum 4 photos.</li>
	
</ul>

<h4 class='subtitle_legals'>Annonce</h4>

<ul>
	<li>Il ne doit pas figurer dans l’annonce l’adresse d’un site web, de majuscules pour en améliorer la visibilité.</li>
	<li>Il n’est pas autorisé de supprimer une annonce pour l’insérer à nouveau.</li>
	<li>Le prix affiché sur l’annonce doit correspondre au prix net, à savoir le prix de vente que l’utilisateur devra payer.</li>
	
</ul>





<h4 class='subtitle_legals'>Garage partenaire</h4>

<p>Selon notre charte qualité, lorsqu'un vendeur dépose son annonce sur autospot il accepte la possibilité qu'un acheteur potentiel demande à ce que la voiture du vendeur soit vérifié chez l’un de nos garages partenaires. Ce service est gratuit pour le vendeur.</p>

<p>Aucune demande d’inspection technique possible sur les véhicules provenant des vendeurs professionnels et de nos garages partenaires.</p>

<p>En cas de demande d'inspection technique, il est demandé au vendeur de contacter le garage partenaire en charge de l'inspection technique.</p>

<p>Dès la date et l'heure de l'inspection technique fixée, le vendeur est tenu d'honorer son rendez-vous avec le garage partenaire. Si le vendeur ne peut pas présenter sa voiture au garage partenaire en charge de l'inspection technique, il peut le contacter afin de fixer un nouveau rendez-vous. </p>

<p>Bien entendu, si le véhicule est vendu entre-temps, le vendeur est tenu de retirer immédiatement l'annonce d'Autospot ce qui a pour effet d'annuler automatiquement la demande d'inspection technique.</p>

<p>Le vendeur ne doit pas perturber le mécanicien dans l'exécution de l'inspection technique de sa voiture.</p>

<p>Pour que le garage partenaire puisse exécuter l'inspection technique dans les meilleures conditions, le vendeur doit : </p>

<ul>
	<li>1. Présenter au garage partenaire un véhicule lavé et séché.</li>
	<li>2. Enlever tous les sièges bébé/enfants et rehausseurs</li>
	<li>3. Présenter un véhicule vide et propre à l'intérieur.</li>
	<li>4. Préparer tous les documents du véhicule (permis de circulation, carnet d'entretien et factures des services/entretiens).</li>

</ul>

<h4 class='subtitle_legals'>Publicité</h4>

<p>Il est interdit de faire de la publicité pour un site concurrent d'Autospot ou un site de petites annonce en suisse ou à l'étranger.</p>

<h4 class='subtitle_legals'>Responsabilité</h4>

<p>Autospot peut supprimer une annonce sans justification, même si celles-ci ne contreviennent pas explicitement aux présentes règles d'insertion. Autospot n'est pas responsable du contenu des annonces. Seul le vendeur qui publie une annonce est entièrement responsable du contenu de son annonce.</p>




</div>    
<div class="clear50"></div>

<!-- /RÈGLES D'INSERTION -->
</div>
<?php include("footer.php"); ?>