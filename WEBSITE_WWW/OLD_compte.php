<?PHP
session_start();
if(!isset($_SESSION['id_client']))
{
    header("Location: inscription.php");
    exit();
}
include("header.php");
include("body.php");
?>

<!-- COMPTE -->
<div class="container">

<div id="moncompte">
<!-- MENU du COMPTE -->
<div id="menucompte" class="col-sm-12">
    <a href="#annonces"><button class="btn btn-register">Mes annonces</button></a>
    <a href="#favoris"><button class="btn btn-register">Mes favoris</button></a>
    <a href="#vendeurs"><button class="btn btn-register">Mes vendeurs préférés</button></a>
    <a href="#securspot"><button class="btn btn-register">Mon SecurSpot</button></a>
    <a href="#profil"><button class="btn btn-register">Mon profil</button></a>
</div>
   
<!-- END MENU du COMPTE -->
    
    <!-- RUBRIQUES -->
    <div id="rubriques">
    <!-- MES ANNONCES -->
    <div id="annonces" class="col-sm-12 collapse in">
        <hr class="blue">
    <h4>Mes annonces <a href="#moncompte"><button class="btn btn-register pull-right">x</button></a></h4>
    <h3>« Vendre sa voiture n’a jamais été aussi simple ! »</h3>
    <i>Afin de faciliter l’insertion de votre annonce, munissez-vous de votre permis de circulation
L’utilisateur devra insérer :</i>
	<ul>
        <li>Marque</li>
        <li>Modèle</li>
        <li>Version (se situe au point 21 de votre permis de circulation)</li>
        <li>Nombre de places : (se situe au point 27 de votre permis de circulation)</li>
        <li>Date de la 1ère immatriculation : (se situe au point 36 de votre permis de circulation)</li>
        <li>Cylindrée : (se situe au point 37 de votre permis de circulation)</li>
        <li>Puissance en KW (se situe au point 76 de votre permis de circulation)</li>	
        </ul>
        
        <a href="annonce.html"><button class="btn btn-closeaccount">Entrer les données de votre véhicule</button></a>
    </div>
    <!-- END MES ANNONCES -->
    
    <!-- MES FAVORIS -->
    <div id="favoris" class="col-sm-12 collapse">
        <hr class="blue">
    <h4>Mes favoris<a href="#moncompte"><button class="btn btn-register pull-right">x</button></a></h4>
   
    </div>
    <!-- END MES FAVORIS -->
    
    <!-- MES VENDEURS -->
    <div id="vendeurs" class="col-sm-12 collapse">
        <hr class="blue">
    <h4>Mes vendeurs <a href="#moncompte"><button class="btn btn-register pull-right">x</button></a></h4>
        
    </div>
    <!-- END MES VENDEURS -->
    
    <!-- MON SECURSPOT -->
    <div id="securspot" class="col-sm-12 collapse">
        <hr class="blue">
        <h4>Mon SecurSpot <a href="#moncompte"><button class="btn btn-register pull-right">x</button></a></h4>
    
        <p>Grâce au service innovant SecurSpot, démarquer vous des autres annonces de véhicules d’occasions en affichant l’historique kilométrique de votre voiture.<br />
        L’achat d’un véhicule d’occasion présente toujours des risques pour l’acheteur. Profiter gratuitement de se service et le label SecurSpot sera visible sur votre annonce.<br />
        Pour cela, rien de plus simple, prenez votre livret de service et toutes vos factures d’entretiens.</p>
        <p>De la date la plus ancienne à la plus récente, insérer :</p>
        <ul>
            <li>Les kilométrages</li>
            <li>La date associée (mois, année)</li>
            <li>Le nom du garage/concessionnaire et le code postal où à lieu le service/entretien/expertise</li>
            <li>Le code postal du domicile des anciens propriétaires et du propriétaire actuel</li>
        </ul>
            <h5>VOS AVANTAGES</h5>
            <ul>
                <li>L’acheteur potentiel se sent rassuré car l’historique du véhicule est affiché</li>
                <li>Pour l’acheteur, pas de risque de fraude au compteur kilométrique</li>
                <li>L’acheteur potentiel peut rapidement, depuis son ordinateur, constater si le véhicule a été suivi chez le même garage/concessionnaire</li>
                <li>Il peut également contrôler le nombre de propriétaires précédents ainsi que la durée de détention du véhicule</li>
                <li>Avec le système gratuit SecurSpot, vous mettez en avant votre voiture et œuvrez pour des annonces de qualité.</li>
            </ul>
        
    </div>
    <!-- END MON SECURSPOT -->
    
    <!-- MON PROFIL -->
    <div id="profil" class="col-sm-12 collapse">
        <hr class="blue">
    <h4>Mon profil <a href="#moncompte"><button class="btn btn-register pull-right">x</button></a></h4>
        
    </div>
    <!-- END MON PROFIL -->
    </div>
    <div class="clear"></div>
    <hr class="blue">
    <div class="clear"></div>

    </div>
    <!-- END RUBRIQUES -->


<!-- END COMPTE -->
</div>
    
<?php include("footer.php"); ?>