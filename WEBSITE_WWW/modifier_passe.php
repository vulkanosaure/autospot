<?php
session_start();

require_once('vendor/autoload.php');
require_once('db_connexion.php');
require_once('classes/PHPMailer2.php');

if(!isset($_SESSION['id_client']))
{
    header("Location: inscription.php");
    exit();
}

if (isset($_POST['sauvegarder']))
{
    $sql = sprintf(
        'SELECT id,email FROM clients WHERE id = "%s" AND mot_de_passe = MD5("%s")', 
        mysqli_real_escape_string($connect1, $_SESSION['id_client']),
        mysqli_real_escape_string($connect1, $_POST['passeActu'])
    );
    $requete = mysqli_query($connect1, $sql);
    $client = mysqli_fetch_object($requete);

    if ($client === null) {
        $erreur = "Mot de passe incorrect !";
    } elseif ($_POST['passeNew'] != $_POST['passeConfirm']) {
        $erreur = "La confirmation de votre nouveau mot de passe ne correspond pas. Veuillez réessayer!";
    } else {
        $sql = sprintf(
            'UPDATE clients SET mot_de_passe = MD5("%s") WHERE id = "%s"',
            mysqli_real_escape_string($_POST['passeNew']),
            mysqli_real_escape_string($client->id)
        );
        $query = mysqli_query($connect1, $sql);

        $notice = 'Mot de passe modifié avec succès !';

        $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
        $twig = new \Twig_Environment($loader);
        $body = $twig->render('modifier_passe.html.twig', ['url' => 'http://www.autospot.ch/mot_de_passe_oublie.php']);
        
        $mail = new PHPMailer2();

        $mail->setFrom('contact@autospot.ch', 'AutoSpot');
        $mail->addAddress($client->email);
        $mail->addReplyTo('no-reply@autospot.ch', 'Ne pas répondre');
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Modification de votre mot de passe';
        $mail->Body = $body;
        $mail->send();
    }
}
include("header.php");
include("body.php");
?>
<a id="haut"></a>

<!-- ESPACE UTILISATEUR -->
<div class="container-fluid">
        <h3 class="center">Espace utilisateur</h3>

  <hr class="blue">
    <h3>Modifier mon mot de passe</h3>
    <hr class="blue">
    <div class="clear"></div>

    <div id="modifMotPasse" class="col-sm-12">
        <form id="modifPasse" class="form-horizontal" method="post">
            <?php if (isset($erreur)): ?>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <div class="alert alert-danger" role="alert">
                            <?php echo $erreur; ?>
                        </div>
                    </div>
                </div>
            <?php elseif (isset($notice)): ?>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <div class="alert alert-success" role="alert">
                            <?php echo $notice; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <label class="control-label col-sm-3" for="passeActu">Mot de passe actuel <span class="requis">*</span></label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="passeActu" name="passeActu" placeholder="" required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3" for="passeNew">Nouveau mot de passe <span class="requis">*</span></label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="passeNew" name="passeNew" placeholder="" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="passeConfirm">Confirmer le nouveau mot de passe <span class="requis">*</span></label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="passeConfirm" name="passeConfirm" placeholder="" required>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <a class="btn btn-default" href="/compte.php?profil">Annuler</a>
                    <button type="submit" name="sauvegarder" class="btn btn-primary">Sauvegarder</button>
                </div>
            </div>
        </form>
    </div>
    
    <div class="clear"></div>
</div>
<!-- FIN ESPACE UTILISATEUR -->
</div>

<?php if (isset($notice)): ?>
    <script type="text/javascript">
        window.setTimeout(function() {
            window.location.href = '/compte.php?profil';
        }, 5000);
    </script>
<?php endif; ?>

<?php include("footer.php"); ?>