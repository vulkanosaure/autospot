<?php
include("remove_accents.php");
$nbresultperpage = 20;
$i=0;


//filtres param par default
if(!isset($_GET['photo'])) $_GET['photo'] = 'oui';
if(!isset($_GET['expertisee'])) $_GET['expertisee'] = 'non';



if(isset($_GET['q']))
{
	if($_GET['q'] == $LANG[4])
		$getq = "";
	else
		$getq = $_GET['q'];
	$getq = preg_replace("/\s+/", " ", trim(mb_strtolower(remove_accents($getq))));
	if(preg_match("/\+/", $getq))
		$q = explode("+", $getq);
	else if(preg_match("/ /", $getq))
		$q = explode(" ", $getq);
	else
		$q = $getq;
}
else
{
	$getq = "";
	$q = $getq;
}
if(isset($_GET['r']))
	$r = $_GET['r'];
else
{
	if($_SERVER['SERVER_NAME'] == "www.motospot.ch")
		$r = "moto";
	else
		$r = "voiture";
}
if(isset($_GET["start"]))
	$start = $_GET["start"];
else
	$start = 0;
if(isset($_GET["trier"]))
	$trier = $_GET["trier"];
else
	$trier = "prixcroissant";
$req1 = "";
$req2 = "";
if((isset($q)) && (is_array($q)))
{
	$req1 = " WHERE (";
	$req2 = " WHERE (";
	while(isset($q[$i]))
	{
		if($req1 == " WHERE (")
		{
			$req1 = $req1."search REGEXP '$q[$i]'";
			$req2 = $req2."search REGEXP '$q[$i]'";
		}
		else
		{
			$req1 = $req1." && search REGEXP '$q[$i]'";
			$req2 = $req2." && search REGEXP '$q[$i]'";
		}
		$i++;
	}
	$req1 = $req1.")";
	$req2 = $req2.")";
}
else if((isset($q)) && ($q != ""))
{
	$req1 = " WHERE search REGEXP '$q'";
	$req2 = " WHERE search REGEXP '$q'";
}
else
{
	$req1 = " WHERE";
	$req2 = " WHERE";
}
if($req1 != " WHERE")
{
	$req1 = $req1." && choix='$r'";
	$req2 = $req2." && choix='$r'";
}
else
{
	$req1 = $req1." choix='$r'";
	$req2 = $req2." choix='$r'";
}
//
if (isset($_GET['reference']) && !empty($_GET['reference'])) {
    $reference = $_GET['reference'];
    $savedParams = $_GET;
    $_GET = [];

    $req1 = $req1." && annonces_clients.id like '$reference'";
    $req2 = " WHERE annonces_clients.id like '$reference'";
}

//
if(isset($_GET["marque"]))
{
	if($_GET["marque"] != "Toutes")
	{
		$marque = $_GET['marque'];
		if($marque == "MERCEDES-BENZ")
			$marque = "MERCEDES";
		else if($marque == "ALFA+ROMEO");
			$marque = "ALFA";
		if($req1 != "")
		{
			$req1 = $req1." && marque REGEXP '$_GET[marque]'";
			$req2 = $req2." && marque REGEXP '$_GET[marque]'";
		}
		else
		{
			$req1 = " WHERE marque REGEXP '$_GET[marque]'";
			$req2 = " WHERE marque REGEXP '$_GET[marque]'";
		}
	}
}
if(isset($_GET["modele"]))
{
	if($_GET["modele"] != "Tous" && $_GET["modele"] != "")
	{
		if($req1 != "")
		{
			$req1 = $req1." && modele REGEXP '$_GET[modele]'";
			$req2 = $req2." && modele REGEXP '$_GET[modele]'";
		}
		else
		{
			$req1 = " WHERE modele REGEXP '$_GET[modele]'";
			$req2 = " WHERE modele REGEXP '$_GET[modele]'";
		}
	}
}
if(isset($_GET["version"]))
{
	if($_GET["version"] != "")
	{
		if($req1 != "")
		{
			$req1 = $req1." && version REGEXP '$_GET[version]'";
			$req2 = $req2." && version REGEXP '$_GET[version]'";
		}
		else
		{
			$req1 = " WHERE version REGEXP '$_GET[version]'";
			$req2 = " WHERE version REGEXP '$_GET[version]'";
		}
	}
}
if(isset($_GET["prixMin"]))
{
	if($_GET["prixMin"] != "")
	{
		if($req1 != "")
		{
			$req1 = $req1." && (prix>='$_GET[prixMin]' && prix<='$_GET[prixMax]')";
			$req2 = $req2." && (prix>='$_GET[prixMin]' && prix<='$_GET[prixMax]')";
		}
		else
		{
			$req1 = " WHERE (prix>='$_GET[prixMin]' && prix<='$_GET[prixMax]')";
			$req2 = " WHERE (prix>='$_GET[prixMin]' && prix<='$_GET[prixMax]')";
		}
	}
}
if(isset($_GET["anneeMin"]))
{
	if($_GET["anneeMin"] != "")
	{
		if($req1 != "")
		{
			$req1 = $req1." && (year>='$_GET[anneeMin]' && year<='$_GET[anneeMax]')";
			$req2 = $req2." && (year>='$_GET[anneeMin]' && year<='$_GET[anneeMax]')";
		}
		else
		{
			$req1 = " WHERE (year>='$_GET[anneeMin]' && year<='$_GET[anneeMax]')";
			$req2 = " WHERE (year>='$_GET[anneeMin]' && year<='$_GET[anneeMax]')";
		}
	}
}
if(isset($_GET["kilometresMin"]))
{
	if($_GET["kilometresMin"] != "")
	{
		if($req1 != "")
		{
			$req1 = $req1." && (kilometrage>='$_GET[kilometresMin]' && kilometrage<='$_GET[kilometresMax]')";
			$req2 = $req2." && (kilometrage>='$_GET[kilometresMin]' && kilometrage<='$_GET[kilometresMax]')";
		}
		else
		{
			$req1 = " WHERE (kilometrage>='$_GET[kilometresMin]' && kilometrage<='$_GET[kilometresMax]')";
			$req2 = " WHERE (kilometrage>='$_GET[kilometresMin]' && kilometrage<='$_GET[kilometresMax]')";
		}
	}
}
if(isset($_GET["chevauxMin"]))
{
	if($_GET["chevauxMin"] != "")
	{
        $chevauxMin = $_GET["chevauxMin"] / 1.359;
        $chevauxMax = $_GET["chevauxMax"] / 1.359;
        if($req1 != "")
		{
			$req1 = $req1." && (puissancechevaux>='$chevauxMin' && puissancechevaux<='$chevauxMax')";
			$req2 = $req2." && (puissancechevaux>='$chevauxMin' && puissancechevaux<='$chevauxMax')";
		}
		else
		{
			$req1 = " WHERE (puissancechevaux>='$chevauxMin' && puissancechevaux<='$chevauxMax')";
			$req2 = " WHERE (puissancechevaux>='$chevauxMin' && puissancechevaux<='$chevauxMax')";
		}
	}
}
if(isset($_GET["cylindrees"]))
{
	if($_GET["cylindrees"] != "")
	{
		if($req1 != "")
		{
			$req1 = $req1." && cylindrees<='$_GET[cylindrees]'";
			$req2 = $req2." && cylindrees<='$_GET[cylindrees]'";
		}
		else
		{
			$req1 = " WHERE cylindrees<='$_GET[cylindrees]'";
			$req2 = " WHERE cylindrees<='$_GET[cylindrees]'";
		}
	}
}
if(isset($_GET["typevehicule"]))
{
	if($_GET["typevehicule"] != "Tous")
	{
		if($req1 != "")
		{
			$req1 = $req1." && typevehicule REGEXP '$_GET[typevehicule]'";
			$req2 = $req2." && typevehicule REGEXP '$_GET[typevehicule]'";
		}
		else
		{
			$req1 = " WHERE typevehicule REGEXP '$_GET[typevehicule]'";
			$req2 = " WHERE typevehicule REGEXP '$_GET[typevehicule]'";
		}
	}
}
if(isset($_GET["transmission"]))
{
	if($_GET["transmission"] != "Toutes")
	{
		if($req1 != "")
		{
			$req1 = $req1." && transmission REGEXP '$_GET[transmission]'";
			$req2 = $req2." && transmission REGEXP '$_GET[transmission]'";
		}
		else
		{
			$req1 = " WHERE transmission REGEXP '$_GET[transmission]'";
			$req2 = " WHERE transmission REGEXP '$_GET[transmission]'";
		}
	}
}


if(isset($_GET["carburant"]))
{
	if($_GET["carburant"] != "Tous")
	{
		if($req1 != "")
		{
			$req1 = $req1." && carburant REGEXP '$_GET[carburant]'";
			$req2 = $req2." && carburant REGEXP '$_GET[carburant]'";
		}
		else
		{
			$req1 = " WHERE carburant REGEXP '$_GET[carburant]'";
			$req2 = " WHERE carburant REGEXP '$_GET[carburant]'";
		}
	}
}


if(isset($_GET["carrosserie"]))
{
	if($_GET["carrosserie"] != "Toutes")
	{
		if($req1 != "")
		{
			$req1 = $req1." && carrosserie REGEXP '$_GET[carrosserie]'";
			$req2 = $req2." && carrosserie REGEXP '$_GET[carrosserie]'";
		}
		else
		{
			$req1 = " WHERE carrosserie REGEXP '$_GET[carrosserie]'";
			$req2 = " WHERE carrosserie REGEXP '$_GET[carrosserie]'";
		}
	}
}
if(isset($_GET["couleurexterne"]))
{
	if($_GET["couleurexterne"] != "Toutes")
	{
		if($req1 != "")
		{
			$req1 = $req1." && couleurexterne REGEXP '$_GET[couleurexterne]'";
			$req2 = $req2." && couleurexterne REGEXP '$_GET[couleurexterne]'";
		}
		else
		{
			$req1 = " WHERE couleurexterne REGEXP '$_GET[couleurexterne]'";
			$req2 = " WHERE couleurexterne REGEXP '$_GET[couleurexterne]'";
		}
	}
}
if(isset($_GET["rouesmotrices"]))
{
	if($_GET["rouesmotrices"] != "Toutes")
	{
		if($req1 != "")
		{
			$req1 = $req1." && rouesmotrices REGEXP '$_GET[rouesmotrices]'";
			$req2 = $req2." && rouesmotrices REGEXP '$_GET[rouesmotrices]'";
		}
		else
		{
			$req1 = " WHERE rouesmotrices REGEXP '$_GET[rouesmotrices]'";
			$req2 = " WHERE rouesmotrices REGEXP '$_GET[rouesmotrices]'";
		}
	}
}
if(isset($_GET["expertisee"]))
{
	if($_GET["expertisee"] == "oui")
	{
		if($req1 != "")
		{
			$req1 = $req1." && expertisee!='0000-00-00'";
			$req2 = $req2." && expertisee!='0000-00-00'";
		}
		else
		{
			$req1 = " WHERE expertisee!='0000-00-00'";
			$req2 = " WHERE expertisee!='0000-00-00'";
		}
	}
}
if((isset($_GET["photo"])) && ($_GET["photo"] == "oui"))
{
	if($req1 != "")
	{
		$req1 = $req1." && image1!=''";
		$req2 = $req2." && image1!=''";
	}
	else
	{
		$req1 = " WHERE image1!=''";
		$req2 = " WHERE image1!=''";
	}
}
if(isset($_GET["annonceurs"]))
{
	if($_GET["annonceurs"] == "particuliers")
	{
		$req1 = $req1." && professionnel='0'";
		$req2 = $req2." && professionnel='0'";
	}
	else if($_GET["annonceurs"] == "professionnels")
	{
		$req1 = $req1." && professionnel='1'";
		$req2 = $req2." && professionnel='1'";
	}
}
if(isset($_GET["region"]))
{
	if($_GET["region"] == "suisseallemande")
	{
		$req2 = $req2." && (canton='BE' || canton='UR' || canton='SZ' || canton='OW' || canton='NW' || canton='LU' || canton='ZH' || canton='GL' || canton='ZG' || canton='SO' || canton='BS' || canton='BL' || canton='SH' || canton='AR' || canton='AI' || canton='SG' || canton='AG' || canton='TG' || canton='GR' || canton='FR')";
	}
	else if($_GET["region"] == "suisseitalienne")
	{
		$req2 = $req2." && (canton='TI')";
	}
	else if($_GET["region"] == "suisseromande")
	{
		// $req2 = $req2." && (canton='VD' || canton='VS' || canton='GE' || canton='FR' || canton='NE' || canton='JU')";
		$req2 = $req2." && (canton='VD' || canton='VS' || canton='GE' || canton='FR' || canton='NE' || canton='JU' || canton='')";
	}
}
$req2 = $req2." && valide='1'";



//https://www.autospot.ch/index.php?r=voiture&marque=Toutes&modele=Tous&version=&reference=397537&prixMin=&prixMax=&anneeMin=&anneeMax=&kilometresMin=&kilometresMax=&chevauxMin=&chevauxMax=&cylindrees=&typevehicule=Tous&transmission=Toutes&carburant=Tous&carrosserie=Toutes&couleurexterne=Toutes&rouesmotrices=Toutes&expertisee=&photo=&region=suisse&version=&prixMin=&prixMax=&anneeMin=&anneeMax=&kilometresMin=&kilometresMax=&chevauxMin=&chevauxMax=&expertisee=oui&photo=oui&trier=&type_vendeur=0
//type vendeur

/*
WHERE choix='voiture' && image1!='' && valide='1' AND visible=1 ORDER BY image1 DESC'
(3 pages)

*/


if(isset($_GET["type_vendeur"]) && $_GET["type_vendeur"] != '')
{
	if(preg_match("/WHERE/", $req1)) $req1 .= " AND type=".$_GET["type_vendeur"];
	else $req1 .= " WHERE type=".$_GET["type_vendeur"];
	
	if(preg_match("/WHERE/", $req2)) $req2 .= " AND type=".$_GET["type_vendeur"];
	else $req2 .= " WHERE type=".$_GET["type_vendeur"];
	
}



//filter visibility

if(preg_match("/WHERE/", $req1)) $req1 .= " AND visible=1";
else $req1 .= " WHERE visible=1";

if(preg_match("/WHERE/", $req2)) $req2 .= " AND visible=1";
else $req2 .= " WHERE visible=1";




if(isset($_GET["trier"]))
{
	switch($_GET["trier"])
	{
		case "prixcroissant":
			$req1 = $req1." ORDER BY prix ASC, image1 DESC";
			$req2 = $req2." ORDER BY prix ASC, image1 DESC";
			break;
		case "prixdecroissant":
			$req1 = $req1." ORDER BY prix DESC, image1 DESC";
			$req2 = $req2." ORDER BY prix DESC, image1 DESC";
			break;
		case "kmcroissant":
			$req1 = $req1." ORDER BY kilometrage ASC, image1 DESC";
			$req2 = $req2." ORDER BY kilometrage ASC, image1 DESC";
			break;
		case "kmdecroissant":
			$req1 = $req1." ORDER BY kilometrage DESC, image1 DESC";
			$req2 = $req2." ORDER BY kilometrage DESC, image1 DESC";
			break;
		case "anneecroissant":
			$req1 = $req1." ORDER BY premiereimm ASC, image1 DESC";
			$req2 = $req2." ORDER BY premiereimm ASC, image1 DESC";
			break;
		case "anneedecroissant":
			$req1 = $req1." ORDER BY premiereimm DESC, image1 DESC";
			$req2 = $req2." ORDER BY premiereimm DESC, image1 DESC";
	}
}
else
{
	$req1 = $req1." ORDER BY image1 DESC";
	$req2 = $req2." ORDER BY image1 DESC";
} 


//req1 is useless... ? (to check)
$req1 = ""; 
// echo "_req2 : '".$req2."'<br />";

/*

req1 : ' WHERE choix='voiture' AND visible=1 ORDER BY image1 DESC'
req2 : ' WHERE choix='voiture' && valide='1' AND visible=1 ORDER BY image1 DESC'

req1 : ' WHERE choix='voiture' && expertisee!='0000-00-00' && image1!='' AND visible=1'
req2 : ' WHERE choix='voiture' && expertisee!='0000-00-00' && image1!='' && valide='1' AND visible=1'

expertisee
image1

*/


$count = mysqli_query($connect1, "SELECT COUNT(*) AS numberOfRows FROM $tableannonces JOIN clients ON $tableannonces.id_client=clients.id $req2");
$row = mysqli_fetch_assoc($count);
$annoncestotal = number_format($row['numberOfRows'], 0, ',', '\'');



if (isset($reference)) {
    $_GET = $savedParams;
}
?>