<?php
require_once('../appbackend/admin/include/db_connect.php');
require_once('../appbackend/admin/include/debug.php');

$GLOBALS["TRACE_ENABLED"] = true;


$sql = "SELECT * FROM annonces_clients";
$smth = $dbh->prepare($sql);	
$smth->execute();

while($annonces = $smth->fetch(PDO::FETCH_ASSOC)){
	// var_dump($annonces);
	$id_annonce = $annonces['id'];
	$cp = $annonces['codepostal'];
	
	$sql = "SELECT * FROM liste_npa WHERE code_postal=:cp";
	$smth2 = $dbh->prepare($sql);	
	$smth2->bindParam(":cp", $cp);
	$smth2->execute();
	$npa = $smth2->fetch(PDO::FETCH_ASSOC);
	// var_dump($npa);
	
	if($npa){
		$canton = $npa['canton'];
		$sql = 'UPDATE annonces_clients SET canton=:canton WHERE id=:id';
		$smth3 = $dbh->prepare($sql);	
		$smth3->bindParam(":id", $id_annonce);
		$smth3->bindParam(":canton", $canton);
		$smth3->execute();
		trace($sql);
	}
	
	
}
