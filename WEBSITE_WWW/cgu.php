<?PHP session_start();
include("header.php");
include("body.php");
?>


<style>
	li{
		margin-left:65px;
		font-size:14px !important;
	}
</style>

<!-- CGU -->
<div class="container">
    <h3 class="center">Conditions Générales d'Utilisation</h3>

<div class="row cgu">
<h4  class='subtitle_legals'>1. - Définitions</h4>
    <p>Pour les présentes <i>conditions générales d'utilisation</i>, les mots ou expressions suivants ont été définis tel qu'il suit :<br />
        <ul>
            <li>"acheteur" : <i>utilisateur</i> souhaitant acheter une voiture d'occasion.</li>
            <li>"demandeur" : utilisateur effectuant une demande d'inspection technique sur un véhicule présent sur le site <a href="http://www.autospot.ch/">autospot.ch</a></li>
            <li>"vendeur" : utilisateur non professionnel, par opposition à un vendeur professionnel, ayant inséré une annonce sur <a href="http://www.autospot.ch/">autospot.ch</a></li>
            <li>"utilisateur" : toute personne physique qui visite le site <a href="http://www.autospot.ch/">autospot.ch</a>.</li>
            <li>"tiers" : toute personne physique ou morale n'étant ni la société, ni un utilisateur, ni un garage partenaire.</li>
            <li>« conditions générales d'utilisation » ou « CGU » : les présentes conditions d'utilisation du site www.autospot.ch</li>
			<li>« espace utilisateur » : espace sur le site <a href="http://www.autospot.ch/">autospot.ch</a> qui est spécialement réservé à l'Utilisateur suite à son inscription sur <a href="http://www.autospot.ch/">autospot.ch</a>.</li>
			<li>« partenaires » : ensemble des garages faisant partie du réseau de garages</li>
			<li>« garage partenaire » : désigne le garage, faisant partie du réseau de garages, qui est mandaté par un acheteur pour effectuer un contrôle technique sur une voiture d'occasion.</li>
			<li>« services » : ensemble des services fournis par AutoSpot et ses partenaires.</li>
			<li>« tablette tactile » : appareil détenu par chaque garage partenaire contenant les points de contrôle et permettant aux partenaires d'effectuer des inspections techniques.</li>
			<li>« application mobile » : désigne l'application mobile d'autospot contenant les points de contrôle et permettant aux partenaires d'effectuer des inspections techniques.</li>
			<li>« site » : désigne le site internet www.autospot.ch.</li>
			<li>« vices cachés » : désigne les défauts dissimulés frauduleusement par le vendeur.</li>
			<li>« société » : désigne la société <a href='http://skaiweb.ch'>SkaiWeb</a>, propriétaire et gérante du site internet <a href="http://www.autospot.ch/">autospot.ch</a></li>
			<li>« bannière automobile » : désigne la bannière à fixer sur le pare-brise ou autre vitrage du véhicule du vendeur. Cette bannière automobile est offerte au vendeur par le site pour les 250 premières insertions et lors de promotions. Elle permet au vendeur d'y inscrire ses coordonnées ainsi que les caractéristiques de son véhicule en vente sur le site.</li>
        </ul>
    </p>



<h4  class='subtitle_legals'>2. - Acceptation des conditions générales d'utilisation</h4>
    <p>L'accès, l'utilisation et la consultation du site impliquent l'acceptation intégrale et sans réserve des présentes conditions générales d'utilisation.</p>
	<p>L'utilisateur déclare et reconnaît avoir pris connaissance des conditions générales d'utilisation et s'engage à les respecter.</p>
	<p>La société se réserve le droit de modifier à tout moment et sans préavis le site, les services et les présentes conditions générales d'utilisation.</p>


<h4 class='subtitle_legals'>3. - Accès au site</h4>
    <p>La société s'efforce de permettre l'accès au site 24 heures sur 24, 7 jours sur 7, sauf en cas de force majeure ou d'un événement hors du contrôle de la société, et sous réserve des éventuelles pannes et interventions de maintenance nécessaires au bon fonctionnement du site et des services qui y sont proposés.</p>
	
	<p>Par conséquent, la société ne peut garantir une disponibilité du site et/ou des services qui y sont proposés.</p>
	<p>La responsabilité de la société ne saurait être engagée en cas d'impossibilité d'accès au site et/ou d'utilisation des services.</p>


<h4 class='subtitle_legals'>4. - Utilisation du site</h4>
    <p>L'utilisateur accepte que la société ne puisse pas garantir l'absence de bugs, erreurs ou inexactitudes</p>
	
	<p>L'utilisateur s'engage à tenir à jour sur le site toutes ses informations personnelles, notamment son numéro de téléphone portable et son adresse email. </p>
	
	<p>L'utilisateur s'engage à ne pas divulguer ses informations personnelles, tels que son mot de passe. La société considère que la connexion au site par l'utilisateur est effectuée par celui-ci. La société ne saurait être tenue pour responsable si un compte utilisateur a été piraté ou utilisé à son insu. Dans ce cas, il incombe à l'utilisateur de contacter immédiatement la société, via le formulaire de contact ou via l'adresse <a href="mailto:contact@autospot.ch">contact@autospot.ch</a>. </p>
	
	<p>L'utilisateur s'engage à ne créer qu'un seul compte utilisateur. La société se réserve le droit de supprimer le ou les comptes supplémentaires qui sont créés par le même utilisateur.</p>



<h4 class='subtitle_legals'>5. – Responsabilité de l'utilisateur</h4>
<p>L'utilisateur est seul responsable de l'utilisation qu'il fait du site et notamment des contenus qu'il pourrait publier sur le site. </p>
<p>En publiant une annonce sur le site, l'utilisateur doit disposer de tous les droits et autorisations nécessaire à la diffusion de l'annonce. De plus, il s'engage à ce que ses contenus soient licites, ne portent pas atteinte à l'ordre public et n'enfreignent pas les lois suisses.</p>

<p>Si l'utilisateur enfreint ces obligations, alors la société peut à tout moment, et sans préavis, supprimer l'annonce associée au compte de l'utilisateur ainsi que le compte de l'utilisateur.</p>

<p>L'utilisateur ne peut tenir pour responsable la société si des informations personnelles accessibles dans son compte utilisateur ou mises en ligne par lui-même sont perdues. De plus, il ne peut prétendre à aucun dédommagement.</p>

<p>Il incombe à l'utilisateur de prendre toute les précautions nécessaires pour sauvegarder les informations accessibles dans son compte utilisateur ou mises en ligne par lui-même.</p>




<h4 class='subtitle_legals'>6. – Responsabilité du vendeur</h4>
    <p>En insérant une annonce sur le site, le vendeur accepte automatiquement le contenu des règles d'insertion, disponible sur le site à l'adresse http://www.autospot.ch/regles_insertion.php </p>
	
	<p>Pour insérer une annonce sur le site, les vendeurs particuliers doivent respecter la charte qualité suivante :</p>
	<p>« Le vendeur doit être un particulier, en opposition à un vendeur professionnel, son véhicule doit être utilisé à usage privé et il doit accepter la possibilité qu'un utilisateur effectue une demande d'inspection technique sur son véhicule. »</p>

	<p>Le vendeur atteste que son véhicule n'est pas accidenté et doit cocher la case « Véhicule non accidenté » lors de l'insertion de son annonce.</p>
	
	<p>En cas de changement d'adresse postal, le vendeur est tenu d'insérer sa nouvelle adresse postale dans son espace utilisateur, sous « Mon profil ».</p>
	<p>En cas de changement d'adresse email et/ou de numéro de téléphone portable, le vendeur est tenu d'insérer sa nouvelle adresse email et/ou son nouveau numéro de téléphone portable dans son espace utilisateur, sous « Mon profil ».</p>
	
	
	<p>Dès que le vendeur reçoit un email et/ou un SMS l'informant qu'une demande d'inspection technique est faite sur son véhicule, il est tenu de contacter le garage partenaire dans les plus brefs délais et selon les horaires d'ouverture du garage partenaire afin de fixer la date de l'inspection technique. Les horaires d'ouverture du garage partenaire sont visibles dans l'email reçu par le vendeur. Bien entendu, les partenaires ne sont pas tenus de répondre aux emails et/ou appels téléphoniques du vendeur en dehors des heures d'ouverture.</p>
	
	<p>Aucune demande d'inspection technique n'est possible sur le véhicule du vendeur dont la localité se situe à plus de 30 minutes du garage partenaire le plus proche. </p>
	Il est possible d'effectuer une demande d'inspection technique sur le véhicule du vendeur uniquement lorsque la localité de celui-ci se situe à maximum 30 minutes du garage partenaire le plus proche. Les informations du temps de trajet sont obtenues sur le site <a href='https://maps.google.ch/'>https://maps.google.ch/</a> et sont basées selon le trajet le plus rapide.</p>
	Bien entendu, ce temps maximum de 30 minutes est purement théorique et ne tient pas compte des éventuels ralentissements, dus au trafic routier, à un accident ou à de mauvaises conditions météorologiques, qui peuvent survenir lors du trajet depuis la localité du vendeur jusqu'au garage partenaire. Ces éventuels ralentissements peuvent ainsi augmenter le temps nécessaire pour effectuer le trajet depuis la localité du vendeur jusqu'au garage partenaire le plus proche, voire dépasser le temps de trajet maximum de 30 minutes.</p>
	Le déplacement du vendeur avec son véhicule jusqu'au garage partenaire ne lui donne droit à aucun dédommagement, même en cas d'augmentation significatif du temps de trajet pour les raisons évoquées ci-dessus.</p>
	
	<p>Le vendeur est tenu de ne pas distraire ou déranger le mécanicien du garage partenaire lors de l'inspection technique.</p>
	
	<p>Lors de l'inspection technique, le vendeur ne peut prétendre à un véhicule de courtoisie.</p>
	
	<p>Le vendeur n'est pas tenu d'assister à l'inspection technique de son véhicule.</p>
	
	<p>Seul l'utilisateur qui effectue la demande d'inspection technique et le paiement de 199.- obtient le rapport d'inspection technique. Le vendeur ne reçoit donc pas le rapport d'inspection technique.</p>
	
	
	<p>La société et ses partenaires ne peuvent être tenus pour responsables et le vendeur ne peut prétendre à aucun dédommagement si l'inspection technique est annulée pour l'une des raisons suivantes :</p>
	
	<ul>
	
		<li>Le vendeur ne parvient pas à contacter le garage partenaire en cas de numéro de téléphone non attribué, non valable ou obsolète.</li>
		<li>Le garage partenaire ne répond pas au téléphone et/ou email du vendeur.</li>
		<li>Le vendeur n'a pas contacté le garage partenaire pour fixer la date de l'inspection technique</li>
		<li>La date d'inspection technique a été fixée mais le vendeur ne s'est pas présenté chez le garage partenaire pour réaliser l'inspection technique ou s'est présenté en retard ou s'est présenté à une date erronée.</li>
	
	<li>Le vendeur ne parvient pas à localiser l'adresse du garage partenaire</li>
	<li>Un problème technique inhérent à la tablette tactile ou inhérent au garage partenaire est survenu lors de l'inspection technique</li>
	<li>Le véhicule a été vendu à un autre utilisateur et le vendeur a retiré l'annonce d'Autospot.</li>
	<li>Le véhicule a été vendu à un autre utilisateur mais le vendeur à omit de retirer l'annonce d'Autospot.</li>
	<li>Le ou les garages qui reçoivent la demande d'inspection technique la refusent pour des raisons d'emploi du temps complet dans les 7 jours ouvrables dès la date de la demande d'inspection technique.</li>
	<li>Le garage qui reçoit la demande d'inspection technique omet de l'accepter ou de la refuser sur la tablette tactile.</li>
	</ul>
	
	<p>La société et ses partenaires ne peuvent être tenus pour responsables et le vendeur ne peut prétendre à aucun dédommagement dans les cas suivants :</p>
	
	<ul>
	<li>Suite à l'insertion de son annonce sur le site, le lien « Je choisis mon forfait » n'est pas visible sur l'annonce du vendeur. Cela signifie qu'aucune demande d'inspection technique n'est possible sur son véhicule.</li>
	<li>Le vendeur ne reçoit pas de SMS et/ou d'email l'informant qu'une demande d'inspection technique est effectuée sur son véhicule et/ou l'informant de la date d'inspection technique sur son véhicule</li>
	<li>L'inspection technique a révélé un ou plusieurs problèmes et/ou dégâts et/ou dysfonctionnements, connus ou inconnus du vendeur, sur le véhicule du vendeur.</li>
	<li>Le véhicule présente, lors de l'achat par le demandeur, un ou plusieurs problèmes et/ou dégâts et/ou dysfonctionnements non signalés sur le rapport d'inspection technique. </li>
	<li>Pour des raisons étrangères à la société, le vendeur ne reçoit pas la bannière automobile ou la reçoit plusieurs jours après avoir souhaité en recevoir une.</li>
	<li>Le vendeur souhaite obtenir et prendre connaissance du rapport d'inspection technique</li>
	<li>La date de l'inspection technique est reportée.</li>
	<li>L'inspection technique dépasse le temps imparti de 60 minutes.</li>
	<li>Le véhicule ne se situe pas à l'adresse du vendeur insérée sur autospot.</li>
	</ul>
	
	<p>Si le vendeur est domicilié dans le canton de Neuchâtel mais que son véhicule est situé dans le canton de Genève, il est primordial que le vendeur insère sur autospot, dans son espace utilisateur, sous « Mon profil », l'adresse à laquelle se situe son véhicule.</p>
	<p>En effet, si une demande d'inspection technique est effectué sur son véhicule, le/les garages partenaires en charge de l'inspection technique se situeront dans le canton de Genève.</p>

	
	
	
	
	
	
	<h4 class='subtitle_legals'>7. – Responsabilité du demandeur</h4>
	<p>Lorsque le demandeur fait une demande d'inspection technique, il doit effectuer un paiement sur le site de frs 199.-. Dès celui-ci est validée, le demandeur reçoit une confirmation par email et la demande d'inspection technique est enregistrée. </p>

	<p>Dès le paiement effectué, le demandeur ne peut plus annulée sa demande d'inspection technique.</p>

	<p>La société s'engage à rembourser, dans les 24 heures, le demandeur lorsque la demande d'inspection technique ne peut être réalisé. Le remboursement s'effectue en totalité sans aucun frais. Dans ce cas, le demandeur en sera averti par SMS, email et dans son espace utilisateur.</p>

	<p>Lorsqu'un utilisateur effectue une demande d'inspection technique, celle-ci est soumise au garage le plus proche du lieu de domicile du vendeur. Si ce garage refuse l'inspection technique pour des raisons d'emploi du temps complet dans les 7 jours ouvrables dès la date de la demande d'inspection technique, celle-ci est transmise au deuxième garage le plus proche du domicile du vendeur.<br />
	La société et ses partenaires ne peuvent être tenus pour responsables et le demandeur ne peut prétendre à aucun dédommagement si les deux garages qui ont reçu la demande d'inspection technique ne peuvent la prendre en charge pour des raisons d'emploi du temps complet dans les 7 jours ouvrables dès la date de la demande d'inspection technique.</p>

	<p>Si le demandeur souhaite être présent lors de l'inspection technique, celui-ci recevra, par email, SMS et dans son espace utilisateur la date de l'inspection technique aussitôt qu'elle est connue. La date et l'heure de l'inspection technique sont définitives. Seul le vendeur peut demander au garage partenaire un nouvelle date d'inspection technique. Le demandeur ne peut pas demander un changement d'heure ou de date d'inspection technique.<br />
	Le garage partenaire débute l'inspection technique même en cas d'absence ou de retard du demandeur. </p>

	<p>Immédiatement dès la fin de l'inspection technique, le demandeur reçoit par email et dans son espace utilisateur, sous mon spot -> suivi de mes demandes, le rapport d'inspection technique au format PDF. De plus, le demandeur reçoit par email la facture acquittée (payée) correspondant à sa demande d'inspection technique.</p>


	<p>La société et ses partenaires ne peuvent être tenus pour responsables et le demandeur ne peut prétendre à aucun dédommagement si l'inspection technique est annulée pour l'une des raisons suivantes :<br />
<ul>
	<li>Le vendeur ne parvient pas à contacter le garage partenaire en cas de numéro de téléphone non attribué, non valable ou obsolète.</li>
	<li>Le garage partenaire ne répond pas au téléphone et/ou email du vendeur.</li>
	<li>Le vendeur n'a pas contacté le garage partenaire pour fixer la date de l'inspection technique.</li>
	<li>La date d'inspection technique a été fixée mais le vendeur ne s'est pas présenté chez le garage partenaire pour réaliser l'inspection technique ou s'est présenté en retard ou s'est présenté à une date erronée.</li>
	<li>Le vendeur ne parvient pas à localiser l'adresse du garage partenaire.</li>
	<li>Un problème technique inhérent à la tablette tactile ou inhérent au garage partenaire est survenu lors de l'inspection technique.</li>
	<li>Le véhicule a été vendu à un autre utilisateur et le vendeur a retiré l'annonce d'Autospot.</li>
	<li>Le véhicule a été vendu à un autre utilisateur mais le vendeur à omit de retirer l'annonce d'Autospot.</li>
	<li>Le ou les garages qui reçoivent la demande d'inspection technique la refusent pour des raisons d'emploi du temps complet dans les 7 jours ouvrables dès la date de la demande d'inspection technique</li>
	<li>Le garage qui reçoit la demande d'inspection technique omet de l'accepter ou de la refuser sur la tablette tactile.</li>
</ul>

<p>La société et ses partenaires ne peuvent être tenus pour responsables et le demandeur ne peut prétendre à aucun dédommagement dans les cas suivants :<br />
<ul>
	<li>Le demandeur ne reçoit pas de SMS et/ou d'email l'informant de sa demande d'inspection technique et/ou l'informant que le rapport d'inspection technique est disponible dans son espace utilisateur et/ou l'informant du suivi de la demande d'inspection technique.</li>
	<li>En cas de problèmes techniques, le demandeur ne reçoit pas le rapport d'inspection technique.</li>
	<li>L'inspection technique a révélé un ou plusieurs problèmes et/ou dégâts et/ou dysfonctionnements, connus ou inconnus du vendeur, sur le véhicule du vendeur.</li>
	<li>Le véhicule du vendeur présente, lors de l'achat par le demandeur, un ou plusieurs problèmes et/ou dégâts et/ou dysfonctionnements non signalés sur le rapport d'inspection technique.</li>
	<li>Le demandeur ne peut effectuer, sur le site, une demande d'inspection technique sur un véhicule dont il souhaite connaître l'état réel car le lien « Je choisis mon forfait » n'est pas visible sur l'annonce du véhicule concerné. Cela signifie qu'aucune demande d'inspection technique n'est possible sur ce véhicule.</li>
	<li>La date de l'inspection technique est reportée.</li>
	<li>L'inspection technique dépasse le temps imparti de 60 minutes.</li>
</ul>






<h4 class='subtitle_legals'>8. – Responsabilité de la société</h4>
<p>Dans un souci de constante amélioration du site et de ses services, la société ne peut être tenue pour responsable en cas d'erreurs, de vices ou de défauts présents temporairement sur le site.

<p>L'utilisateur reconnaît que les annonces disponibles sur le site n'engagent que la responsabilité des vendeurs. Les textes et photographies des annonces sont susceptibles de ne pas être complètes, exhaustifs et actuels. 

<p>Le vendeur ne peut tenir pour responsable la société et/ou ses partenaires si aucune demande d'inspection technique n'est possible sur son véhicule. <br />
Les inspections techniques ne sont pas possibles sur les véhicules suivants :

<ul>
	<li>Véhicules électriques, hybrides, gaz et bioéthanol</li>
	<li>Véhicules dont la première mise en circulation date de plus de 30 ans (véhicule de collection).</li>
	<li>Véhicules neufs</li>
	<li>Véhicules de marque suivantes : AC, ACREA, ADLER, ADR, AIXAM, ALLARD, ALVIS, AMC, AMILCAR, ARIEL, ARMSTRONG SIDELEY, ARTEGA, ASTON MARTIN, AUBURN, AUSTIN, AUSTIN HEALEY, AUTOBIANCHI, AVANTI, AXR, BERKELEY, BENTLEY, BERTONE, BOATTAIL, BORGWARD, BRABHAM, BRASIER, BRISTOL, BUGATTI, BUGGI, CARBODIE, CARVER, CATERHAM, CHEVRON, CLENET, COBRA, CORD, CROSSLE, DAF, DAREN, DATSUN, DAX, DE TOMASO, DELAGE, DELOREAN, DESOTO, DESTINY, DFSK, DIAVOLINO, DKW, DONKERVOORT, DURANT, EDSEL, EDSEL, ESSEX, EXCALIBUR, FACEL VEGA, FERRARI, FISKER, FRAMO, FREEWIEL, GINETTA, GUMPERT, HANOMAG, HEALEY, HILLMAN, HOTCHKISS, HRG, HS, HUDSON, HUMMER, HWM, IMPERIA, INNOCENTI, INTERMECCANICA, INVICTA, ISO, JOWETT, KAISER, KOENIGSEGG, KOUGAR, KTM, LAGONDA, LAMBORGHINI, LEA FRANCIS, LOCOMOBILE, LOLA, LOTUS, LTI, MALLOCK, MARCOS, MARQUIS, MARTIN, MASERATI, MATHIS, MARTA, MAXIMAG, MAYBACH, MCLAREN, MEGA, MERCURY, MESSERSCHMITT, MEV HUMMER, MINELLI, MONTERVERDI, MORGAN, MORRIS, MORS, MOWAG, NASH, NSU, OLDSMOBILE, OVERLAND, PACKARD, PAGANI, PANHARD, PANTHER, PGO, PHANTER, PIAGGIO, PIERCE ARROW, PININFARINA, PLYMOUTH, PUCH, RADICAL, RAMOO, RELIANT, REVA, RILEY, RINSPEED, ROLLS-ROYCE, ROSSION, RUF, RUSKA, SALMSON, SANTANA, SAPOROSHEZ, SBARRO, SECMA, SENECHAL, SIMCA, SINGER, SOKON, SPYKER, STEPHENS, STEYR, STUDEBAKER, STUTZ VICTORIA, SUNBEAM, SYLVA, TALBOT, TATRA, TAZZARI, TESLA, THINK, TRABANT, TRIUMPH, TVR, ULTIMA, UMM, VEM, VENTURI, VESPA, VOLTEIS, WARTBURG, WESTFIELD, WIESMANN, WILLYS, WOLSELEY, YES !, ZAGATO, ZARP, ZBR, ZIMMER</li>
	<li>Véhicules qui ne possèdent pas de plaques d'immatriculations.</li>
	<li>Véhicules dont le moteur ne démarre pas ou qui ne sont pas en état de se déplacer.</li>
	<li>Véhicules provenant des partenaires</li>
	<li>Véhicules dont la localité du vendeur se situe à plus de 30 minutes du garage partenaire le plus proche. Informations du temps de trajet obtenues sur le site <a href='https://maps.google.ch/'>https://maps.google.ch/</a> et basées selon le trajet le plus rapide.</li>
	<li>Lorsque le ou les deux garages partenaires qui reçoivent la demande d'inspection technique sont fermés pour cause de vacances.</li>
	<li>Les véhicules dont une demande d'inspection technique a été effectué par un utilisateur et que le vendeur n'a pas contacté le garage partenaire dans les 7 jours ouvrables pour fixer la date de l'inspection technique.</li>
	<li>Les véhicules dont une demande d'inspection technique a été effectué par un utilisateur et que le garage partenaire a signalé que le vendeur était absent le jour de l'inspection technique.</li>
</ul>


<p>La société se réserve le droit d'annuler, à tout moment et pour n'importe quel véhicule, la possibilité d'effectuer une demande d'inspection technique.</p>

<p>Lorsqu'une demande d'inspection technique est effectué sur un véhicule, l'inscription « demande d'inspection technique en cours » est visible sur l'annonce du véhicule concerné. Dans ce cas, il n'est pas possible d'effectuer une demande d'inspection technique sur le véhicule possédant déjà une demande d'inspection technique.</p>

<p>Lorsqu'une inspection technique est terminée, un utilisateur ne peut effectuer une nouvelle demande d'inspection technique que 90 jours suivant la date où l'inspection technique est terminée. </p>

<p>En cas de demande d'inspection technique, si le vendeur ne se déplace pas au rendez-vous de l'inspection technique chez le garage partenaire ou s'il ne contacte pas le garage partenaire dans les 7 jours ouvrables dès la date de la demande d'inspection technique pour fixer le rendez-vous de l'inspection technique, la demande d'inspection technique s'annule automatiquement. Dans ce cas, il ne sera plus possible d'effectuer une demande d'inspection technique sur le véhicule concerné.</p>

<p>En aucun cas, la société et ses partenaires ne peuvent être tenus pour responsables de problèmes techniques et/ou vices cachés liés aux véhicules présentés sur autospot.</p>

<p>La responsabilité de la société et de ses partenaires ne peut être engagée lors de transactions entre vendeurs et acheteurs.</p>

<p>Immédiatement dès la fin de l'inspection technique, la société envoie au garage partenaire la facture correspondant à l'inspection technique, par email.<br />
La société s'engage à verser au garage partenaire, dans un délai de 24 heures dès la fin de l'inspection technique, le montant correspondant à son tarif horaire ajouté de 7.7% correspondant à la TVA suisse.<br />
Bien entendu, la société ne prélève aucuns frais sur le montant versé aux partenaires.<br />
</p>

<p>Aucun abonnement, taxe ou autre commission n'est prélevé, ni demandé aux partenaires pour être présents dans le réseau.</p>

<p>La société ne peut prétendre, auprès de ses partenaires, à aucun remboursement partiel lorsqu'une inspection technique se termine avant le temps imparti de 60 minutes.</p>






<h4 class='subtitle_legals'>9. – Responsabilité des partenaires</h4>

<p>La société <a href='http://skaiweb.ch'>Skaiweb</a>, Mendonça, propriétaire du site <a href="http://www.autospot.ch">www.autospot.ch</a> met à disposition des partenaires une tablette tactile contenant une application mobile Android et les points de contrôle permettant d’effectuer les inspections techniques.</p>

<p>Cette tablette tactile est transmise à titre de prêt durant toute la durée de l’adhésion au réseau de garage partenaire et reste la propriété de la société <a href='http://skaiweb.ch'>Skaiweb</a>, Mendonça. De plus, aucune caution n’est demandé aux partenaires. <br>
En cas de dégât sur la tablette tactile inhérent à une mauvaise manipulation, il sera demandé au partenaire la somme de CHF 100.- pour le remplacement de celle-ci. <br>
Si un partenaire souhaite quitter le réseau de garage partenaire, celui-ci doit avertir au plus tôt la société <a href='http://skaiweb.ch'>Skaiweb</a>, Mendonça et s'engage à envoyer la tablette tactile dans sa boite d’origine par courrier postal au siège de la société <a href='http://skaiweb.ch'>Skaiweb</a>, Mendonça, sise à la Route de Lausanne 17, 1610 Oron-la-ville .</p>

<p>La société se réserve le droit de retirer, sans préavis,un partenaire du réseau. La société s'engage à avertir, par mail, au plus tôt le garage de son retrait du réseau. Le garage s'engage à confirmer, par mail, la bonne réception du mail de la société.</p>

<p>Les partenaires possèdent une tablette tactile équipée d'une application mobile Android contenant tous les points de contrôle nécessaires à l'exécution des inspections techniques. <br />
Les points de contrôle contenus dans l'application mobile autospot sont identiques pour tous les partenaires. </p>

<p>Avant chaque inspection technique, les partenaires s'engagent à vérifier si une mise à jour de l'application mobile est disponible sur Play Store en allumant la tablette tactile, puis en cliquant sur l'icône Play Store et, si une mise à jour est disponible, en cliquant sur « Mettre à jour ».<br />
Le travail des partenaires réside uniquement dans l'exécution de l'inspection technique. Ils n'ont aucune obligation d'explications vis-à-vis du vendeur et/ou du demandeurdemandeur quant aux résultats de l'inspection technique. </p>

<p>Les partenaires ne sont pas responsables en cas de vices cachés sur le véhicule inspectéIl n'est pas demandé aux partenaires de retirer le/les caches moteur et châssis lors des inspections techniques. Par conséquent, aucun dédommagement ne peut être demandé par le vendeur et/ou le demandeur en cas de dégâts et/ou de dysfonctionnements qui ne seraient visibles qu'en retirant un/des caches moteur et/ou châssis des véhicules inspectés.</p>

<p>Les partenaires et/ou la société ne peuvent être tenus pour responsables en cas de vices cachés sur les véhicules disponibles sur le site ayant été soumis ou non à une inspection technique.</p>

<p>Les inspections techniques durent 60 minutes. Toutefois, selon les caractéristiques et l'état du véhicule inspecté, cette durée de 60 minutes est théorique et les inspections techniques peuvent se terminer plus rapidement ou dépasser le temps imparti de 60 minutes.<br />
Les partenaires ne peuvent prétendre à aucun dédommagement en cas de dépassement du temps imparti de l'inspection technique de 60 minutes.</p>

<p>Les inspections techniques se terminent à une heure précise, à savoir lorsque les partenaires cliquent sur le bouton « Je termine l'inspection technique » de l'application mobile. A cet instant précis, la société, le garage partenaire, le demandeur et le vendeur reçoivent un email et un SMS les informant, avec l'heure précise, que l'inspection technique est terminée.<br />
Les partenaires et/ou la société ne peuvent être tenus pour responsables de l'utilisation faite du véhicule par le vendeur lorsque l'inspection technique est terminée et par conséquent des éventuels dégâts et/ou dysfonctionnements subis par le véhicule après la fin de l'inspection technique.</p>

<p>C'est la raison pour laquelle, le rapport d'inspection technique n'est valable qu'à l'heure de l'inspection technique et ne correspond à l'état réel du véhicule inspecté que pendant l'heure de l'inspection technique.</p>

<p>Le garage partenaire effectue l’inspection technique, dans le délai imparti de 60 minutes, en suivant scrupuleusement les points de contrôle contenu dans son cahier des charges. C’est la raison pour laquelle, le garage partenaire et/ou la société <a href='http://skaiweb.ch'>SkaiWeb</a> ne peuvent être tenus pour responsable en cas de dégâts et/ou de dysfonctionnement sur le véhicule qui ne peuvent pas être notifiés durant l’inspection technique.</p>

<p>Ce rapport d’inspection technique ne remplace en aucun cas une expertise auprès du service des automobiles.</p>

<p>Les partenaires doivent avertir au plus tôt la société lors de changements d'adresse et/ou de coordonnées téléphoniques et/ou bancaires, par email à l'adresse <a href="mailto:contact@autospot.ch">contact@autospot.ch</a>.</p>

<p>Lors de l'inspection technique, il est demandé aux partenaires de photographier la page droite du permis de circulation afin de ne pas photographier les informations personnelles du vendeur présente sur la partie gauche du permis de circulation.<br />
La société et/ou les partenaires ne peuvent être tenu pour responsable si des informations personnelles du vendeur sont visibles sur la photographie du permis de circulation présente dans le rapport d'inspection technique.</p>



<h4 class='subtitle_legals'>10. - Propriété Intellectuelle</h4>
<p>L'utilisation du site ne confère aucun droit sur ce site et/ou son contenu. Seul un usage strictement personnel de ce site et de son contenu est autorisé.<br />
Ce site et l'ensemble des éléments le composant tels que notamment les noms de domaine, les marques, la structure générale, les textes, graphiques, images, les bases de données, l'arborescence du site et sa navigation, les logos, la charte graphique, les sons, les photographies et l'iconographie, ainsi que tous autres éléments graphiques, sonores ou autres, reproduits sur le site ou inclus en son sein, sont la propriété exclusive de la société, indépendamment de leur protection éventuelle, en l'état actuel de la législation, par un droit d'auteur ou de toute autre manière, à l'exclusion des éléments émanant des partenaires de la société. </p>

<p>Toute reproduction et/ou représentation et/ou diffusion, en tout ou en partie, et sur tout support du site et/ou de tout ou partie de son contenu sont interdites, sauf autorisation préalable, écrite et expresse de la société. <br />
Toute personne contrevenant à cette interdiction engage sa responsabilité pénale et civile et peut être poursuivie notamment pour contrefaçon et/ou concurrence déloyale. </p>

<p>L'utilisation d'outils, tels que Spider, crawler ou robots, qui indexent et/ou transmettent de manière automatisée les contenus du site, est formellement interdite.</p>



<h4  class='subtitle_legals'>11. Liens hypertextes</h4>
<p>Le site peut contenir des liens hypertextes pointant vers d'autres sites internet sur lesquels la société n'exerce pas de contrôle. Malgré les vérifications préalables et régulières réalisées par la société pour supprimer ces liens hypertextes, nous déclinons tout responsabilité quant aux contenus qu'il est possible de trouver sur ces sites.</p>

<p>La société autorise l'insertion de liens hypertextes vers toute page ou document sous réserve que ces liens soient en relation avec l'annonce que l'utilisateur a publiée.<br />
Enfin, la société se réserve le droit de faire supprimer à tout moment, et sans préavis, un lien hypertexte.</p>



<h4  class='subtitle_legals'>12. Protections des données</h4>
<p>La société s'engage à préserver la confidentialité des informations transmises par l'utilisateur. En aucun cas elle ne transmettra à des tiers les informations transmises par l'utilisateur.</p>
	

	
	
<div class="clear"></div>
<!-- END CGU -->
</div>
<?php include("footer.php"); ?>