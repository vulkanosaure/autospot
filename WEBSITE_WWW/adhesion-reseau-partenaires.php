<?php
session_start();

include("header.php");
include("body.php");
require_once('vendor/autoload.php');
require_once('classes/email.php');

$msg = new \Plasticbrain\FlashMessages\FlashMessages();

if (count($_POST) > 0) {
	
	
	$secret = "6LfUhlUUAAAAAArLszhTtTYcHQTHSKfMIT5d1kmi";
	$gRecaptchaResponse = (isset($_POST['g-recaptcha-response'])) ? $_POST['g-recaptcha-response'] : '';
	$recaptcha = new \ReCaptcha\ReCaptcha($secret);
	$resp = $recaptcha->verify($gRecaptchaResponse, $_SERVER['REMOTE_ADDR']);
	if (!$resp->isSuccess()) {
		$errors = $resp->getErrorCodes();
		$msg->error('Veuillez valider le filtre antispam s\'il vous plaît.');
	}
	else{
	
		$name = array_key_exists('name', $_POST) && !empty($_POST['name']) ? strip_tags(trim($_POST['name'])) : '';
		$address = array_key_exists('address', $_POST) && !empty($_POST['address']) ? strip_tags(trim($_POST['address'])) : '';
		$zipcode = array_key_exists('zipcode', $_POST) && !empty($_POST['zipcode']) ? strip_tags(trim($_POST['zipcode'])) : '';
		$city = array_key_exists('city', $_POST) && !empty($_POST['city']) ? strip_tags(trim($_POST['city'])) : '';
		$canton = array_key_exists('canton', $_POST) && !empty($_POST['canton']) ? strip_tags(trim($_POST['canton'])) : '';
		$lastname = array_key_exists('lastname', $_POST) && !empty($_POST['lastname']) ? strip_tags(trim($_POST['lastname'])) : '';
		$phone = array_key_exists('phone', $_POST) && !empty($_POST['phone']) ? strip_tags(trim($_POST['phone'])) : '';
		$tarif = array_key_exists('tarif', $_POST) && !empty($_POST['tarif']) ? strip_tags(trim($_POST['tarif'])) : '';
		$website = array_key_exists('website', $_POST) && $_POST['website'] == 1 ? true : false;
		$website_url = array_key_exists('website_url', $_POST) && !empty($_POST['website_url']) ? trim($_POST['website_url']) : false;
		$upsa = array_key_exists('upsa', $_POST) && $_POST['upsa'] == 1 ? true : false;
		$question = array_key_exists('question', $_POST) && !empty($_POST['question']) ? strip_tags(trim($_POST['question'])) : false;
		$email = array_key_exists('email', $_POST) && !empty($_POST['email']) ? strip_tags(trim($_POST['email'])) : false;

		$mail = email::init();

		$loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
		$twig = new \Twig_Environment($loader);
		$body = $twig->render('join_network.html.twig', [
			'name' => $name,
			'address' => $address,
			'zipcode' => $zipcode,
			'city' => $city,
			'canton' => $canton,
			'lastname' => $lastname,
			'email' => $email,
			'phone' => $phone,
			'tarif' => $tarif,
			'website' => $website,
			'website_url' => $website_url,
			'upsa' => $upsa,
			'question' => $question,
		]);

		$mail->setFrom('contact@autospot.ch', 'AutoSpot');
		$mail->addAddress('contact@autospot.ch');
		$mail->isHTML(true);
		$mail->CharSet = 'UTF-8';
		$mail->Subject = "Demande d'information - réseau de garages partenaires";
		$mail->Body = $body;
		$mail->send();

		$msg->success('Votre demande nous a bien été transmise et nous vous en remercions. Nous vous contacterons dans les plus brefs délais.');
	}
}

?>

<div class="container">
    <h3 class="center">Adhérer à notre réseau de garages partenaires</h3>

    <div class="container-fluid">
        <?php
        if ($msg->hasMessages()) {
            $msg->display();
        }
        ?>

        <p>Vous êtes un garage et le concept d'autospot vous intéresse.</p>
        <p>Vous souhaitez en savoir plus, ou vous souhaitez adhérer à notre réseau de garages partenaires ?</p>
        <p>Vos avantages en adhérant à notre réseau de garages partenaires</p>

        <br />

        <p><i class="fa fa-check blue"></i> Une zone géographique délimitée est attribuée à chaque garage membre du réseau de garages partenaires.</p>
        <p><i class="fa fa-check blue"></i> Cette zone géographique peut-être élargie si un autre garage membre du réseau de garages partenaires refuse une inspection technique pour cause d’emploi du temps complet dans les 7 jours ouvrables suivant la date de la demande d’inspection technique.</p>
        <p><i class="fa fa-check blue"></i> si vous avez la possibilité, selon votre emploi du temps, d’agender l’inspection technique de 60 minutes dans les 7 prochains jours ouvrables suivant la date de la demande d’inspection technique, vous pouvez l’accepter et elle vous sera attribuée.</p>
        <p><i class="fa fa-check blue"></i> En acceptant une inspection technique, vous complétez votre emploi du temps et faite découvrir votre garage à de potentiels nouveaux clients.</p>
        <p><i class="fa fa-check blue"></i> Nous vous prêtons gratuitement la tablette tactile installée de l’application autospot multilingue comprenant les points de contrôle.</p>
        <p><i class="fa fa-check blue"></i> Le vendeur et, le cas échéant, l’utilisateur qui effectue la demande d’inspection technique peuvent décider d’effectuer les éventuelles réparations futures dans votre garage.</p>
        <p><i class="fa fa-check blue"></i> A la fin de l’inspection technique, vous recevez la totalité de votre tarif horaire, ajouté du montant de la TVA suisse.</p>
        <p><i class="fa fa-check blue"></i> Aucun abonnement, ni taxe ou commission à payer.</p>
        <p><i class="fa fa-check blue"></i> Aucun contrat à signer pour devenir membre du réseau de garages partenaires. Plus d’informations sur nos Conditions Générales d’Utilisation.</p>

        <br />

        <p>Veuillez, s'il vous plaît, remplir le formulaire ci-dessous :</p>

        <form class="form-horizontal" action="/adhesion-reseau-partenaires.php" method="post">
            <div class="form-group">
                <div class="col-sm-3">Nom de votre garage</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">Adresse</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="address" name="address" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 ">Code postal</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="zipcode" name="zipcode" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">Localité</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="city" name="city" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">Canton</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="canton" name="canton" required>
                </div>
            </div>

            <br />

            <div class="form-group">
                <div class="col-sm-3">Nom et prénom</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="lastname" name="lastname" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">Numéro de téléphone</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="phone" name="phone" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">Adresse email</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="email" name="email" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">Tarif horaire</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="tarif" name="tarif" required>
                </div>
            </div>

            <br />

            <div class="form-group">
                <div class="col-sm-3">Possédez-vous un site internet</div>
                <div class="col-sm-9">
                    <input type="radio" name="website" value="1" onclick="jQuery('#website_url_block').show();" /> Oui
                    <input type="radio" name="website" value="0" checked="checked" onclick="jQuery('#website_url_block').hide();" /> Non
                </div>
            </div>
            <div id="website_url_block" class="form-group" style="display: none;">
                <div class="col-sm-3">Adresse de votre site internet</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="website_url" name="website_url" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">Etes-vous membre de l'UPSA ?</div>
                <div class="col-sm-9">
                    <input type="radio" name="upsa" value="1" required/> Oui
                    <input type="radio" name="upsa" value="0" required/> Non
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    Si vous avez d’éventuelles questions ou remarques, n'hésitez pas à nous les transmettre.
                </div>
                <div class="col-sm-9 col-sm-offset-3">
                    <textarea class="form-control" name="question"></textarea>
                </div>
            </div>
			
			
			<div class="form-group">
                <div class="col-sm-9 col-sm-offset-3"></div>
                <div class="col-sm-9 col-sm-offset-3">
				<div class="g-recaptcha" data-sitekey="6LfUhlUUAAAAADFtz6TSa-0HVxBQYmxATuHJLlNt"></div>
                </div>
            </div>

            <br />

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <input class="btn btn-autospot" type="submit" value="Envoyer" />
                </div>
            </div>
        </form>
    </div>
</div>

<?php include("footer.php"); ?>
