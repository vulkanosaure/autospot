<?PHP
session_start();
include("db_connexion.php");

if(!isset($_SESSION['id_client']))
{
    header("Location: inscription.php");
    exit();
}
if(isset($_POST['sauvegarder']))
{
    $nom = mysqli_real_escape_string($connect1, $_POST['nom']);
    $prenom = mysqli_real_escape_string($connect1, $_POST['prenom']);
    $adresse = mysqli_real_escape_string($connect1, $_POST['adresse']);
    $numero = mysqli_real_escape_string($connect1, $_POST['numero']);
    $cp = mysqli_real_escape_string($connect1, $_POST['cp']);
    $ville = mysqli_real_escape_string($connect1, $_POST['ville']);
    $portableNew = mysqli_real_escape_string($connect1, $_POST['portableNew']);
    $portableConfirm = mysqli_real_escape_string($connect1, $_POST['portableConfirm']);

    if($portableNew != "")
    {
        if($_POST['$portableNew'] != $_POST['$portableConfirm'])
            $erreur = "La confirmation de votre nouveau numéro de téléphone ne correspond pas. Veuillez réessayer!";
        else
            mysqli_query($connect1, "UPDATE clients SET telephone='$portableNew' WHERE id='$_SESSION[id_client]'");
    }
    mysqli_query($connect1, "UPDATE clients SET nom='$nom', prenom='$prenom', adresse='$adresse', adresse_num='$numero', code_postal='$cp', ville='$ville', bank_etablissement='$bank_etablissement', bank_iban='$bank_iban', bank_nom='$bank_nom', bank_prenom='$bank_prenom', bank_cp='$bank_cp', bank_ville='$bank_ville' WHERE id='$_SESSION[id_client]'");

    header("Location: /compte.php?profil");
    exit();
}
include("header.php");
include("body.php");
$requete_client = mysqli_query($connect1, "SELECT * FROM clients WHERE id='$_SESSION[id_client]'");
$client = mysqli_fetch_array($requete_client);

?>
<a id="haut"></a>

<!-- ESPACE UTILISATEUR -->
<div class="container-fluid">
  <hr class="blue">
    <h3>Modifier mes coordonnées</h3>
    <hr class="blue">
    <div class="clear"></div>
    <div id="modifCoordonnees" class="col-sm-9">
    <form id="modifCoord" class="form-horizontal" method="POST">
  <div class="form-group">
    <label class="control-label col-sm-3" for="nom">Nom</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="nom" name="nom" value="<?PHP echo $client['nom'];?>">
    </div>
    <div class="col-sm-3 hidden-xs">
        <a class="btn btn-danger" href="supprimer_compte.php">Supprimer mon compte</a>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3" for="prenom">Prénom</label>
    <div class="col-sm-6"> 
      <input type="text" class="form-control" id="prenom" name="prenom" value="<?PHP echo $client['prenom'];?>">
    </div>
  </div>
        
        <div class="clear"></div>
        
        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12" for="adresse">Adresse & N°</label>
            <div class="col-sm-5 col-xs-10"> 
              <input type="text" class="form-control" id="adresse" name="adresse" value="<?PHP echo $client['adresse'];?>">
            </div>
            <div class="col-sm-1 col-xs-2"> 
              <input type="text" class="form-control" id="numero" name="numero" value="<?PHP echo $client['adresse_num'];?>">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12" for="cp">Code postal & ville</label>
            <div class="col-sm-2 col-xs-3"> 
                <input type="text" class="form-control" id="cp" name="cp" value="<?PHP echo $client['code_postal'];?>">
            </div>
            <div class="col-sm-4 col-xs-9"> 
                <input type="text" class="form-control" id="ville" name="ville" value="<?PHP echo $client['ville'];?>">
            </div>
        </div>
        <div class="clear"></div>
        
    <div class="form-group">
        <label class="control-label col-sm-3" for="portableActu"><small>N° de téléphone portable actuel</small></label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="portableActu" name="portableActu" value="<?PHP echo $client['telephone'];?>">
    </div>
  </div>
     <div class="form-group">
         <label class="control-label col-sm-3" for="portableNew"><small>Nouveau N° de téléphone portable</small></label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="portableNew" name="portableNew">
    </div>
  </div>
     <div class="form-group">
    <label class="control-label col-sm-3" for="portableConfirm"><small>Confirmer le nouveau N°</small></label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="portableConfirm" name="portableConfirm">
    </div>
  </div>



  <div class="form-group"> 
    <div class="col-sm-offset-3 col-sm-6">
      <a class="btn btn-default" href="/compte.php?profil">Annuler</a>
      <button type="submit" name="sauvegarder" class="btn btn-primary">Sauvegarder</button>
    </div>
    <div class="col-xs-12 visible-xs">
        <a class="btn btn-danger" href="supprimer_compte.php">Supprimer mon compte</a>
    </div>
  </div>
</form>
    </div>
    
    <div class="clear"></div>
</div>
<!-- FIN ESPACE UTILISATEUR -->
</div>
<?php include("footer.php"); ?>