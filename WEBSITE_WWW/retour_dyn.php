<?php

require_once("./appbackend/admin/include/db_connect.php");
require_once('./appbackend/events_utils.php');


function traceDb($_content)
{
	global $connect1;
	mysqli_query($connect1, "INSERT INTO trace VALUES ('', NOW(), '".$_content."')");
}


$content = 'retour_dyn\n';
$content .= 'POST : '.json_encode($_POST).'\n';
$content .= 'GET : '.json_encode($_GET).'\n';

if(isset($_GET['id_demande']) && isset($_GET['NUMXKP']) && isset($_GET['TRANSACTIONID'])){
	
	traceDb($content);
	setPaymentInfo($dbh, $_GET['id_demande'], json_encode($_GET));
	
}

/*
GET : {
	"id_demande":"198",
	"NUMXKP":"1528103600",
	"TRANSACTIONID":"320612",
	"PAIEMENT":"MASTERCARD",
	"MONTANTXKP":"199.00",
	"DEVISEXKP":"CHF",
	"SCOREXKP":"0",
	"IPXKP":"90.63.216.93",
	"PAYSRXKP":"CH",
	"PAYSBXKP":"CH",
	"ALIAS":"1CE6FDE9B76D59DE722B3F6D0A941267B065D208682E9984AAE75D2976A134C03298CA28F717193452433D8AED4DA3846CC",
	"RESPONSE":"00"
	
}
*/