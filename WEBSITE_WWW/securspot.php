<?PHP
session_start();
include("header.php");
include("body.php");
?>
<!-- SECURSPOT -->
<div class="container">
<h3 class="center">SecurSpot</h3>

<div id="secur" class="container center-block">

             

<p>Notre site de petites annonces AutoSpot à pour mission, non seulement de proposer des annonces auto de qualité, mais surtout de réduire au maximum les risques liés à l’achat d’une voiture d’occasion.<br /> 
Nous souhaitons, au travers divers outils et services que nous allons mettre en place, réduire voire effacer les risques liés à l’achat d’un véhicule d’occasion.</p>

<p>C’est la raison pour laquelle nous avons, entres autres, intégré SecurSpot afin de permettre aux acheteurs potentiels de connaître à l’avance l’historique du véhicule qui les intéresse.<br/>
En effet, depuis sa mise en circulation, un véhicule à inévitablement fait l’objet de services réguliers, de contrôles, de réparations ou de changements de détenteurs.</p>

<p>Le rapport SecurSpot, totalement gratuit, se crée au moment de l’insertion de l’annonce sur autospot par le vendeur. Lui seul peut décider de créer le rapport SecurSpot.
Pour ce faire, rien de plus simple, il suffit au vendeur de prendre son carnet d’entretien du véhicule et toutes les factures correspondantes à chaque intervention, si bien entendu il les possède encore.<br />
A ce moment, il suit les différentes étapes dans l’insertion de ses informations.</p>

<p>Bien entendu, le rapport ne peut exister qu’en présence du carnet d’entretien du véhicule. Le rapport SecurSpot sera plus complet avec la présence des factures correspondantes aux différents services effectués durant la vie du véhicule.</p>

<p>Pour les acheteurs, il est important de tenir compte que de nombreux véhicules peuvent posséder plusieurs détenteurs. Dès lors, même si le dernier détenteur possède toutes les factures, il est possible que les deux premiers détenteurs ne les aient pas gardées.</p>

<p>En tant qu’acheteur, plus vous récolter d’informations sur le véhicule et donc connaissez son passé, plus vous êtes sûr d’éviter les mauvaises surprises.<br />
Si vous avez un coup de cœur pour une voiture, pensez à toujours consulter le rapport SecurSpot.</p>

    <div class="col-sm-6 tableau">
        <ul>
<li class="title center">Avantages de SecurSpot pour les acheteurs</li>
<li>Vous effectuez un achat en pleine connaissance de cause.</li>
<li>Vous obtenez un maximum d’informations utiles et précieuses avant de vous déplacer pour visiter le véhicule.</li>
<li>Vous pouvez contrôler si le véhicule à son carnet d’entretien à jour. 
C’est-à-dire si toutes les révisions recommandées par le constructeur (tous les 20’000 km) ont été effectuées.</li>
<li>Vous pouvez contrôler si le véhicule a été suivi chez un concessionnaire officiel de la marque du véhicule.</li>
<li>Vous pouvez contrôler si le véhicule a été suivi depuis le début dans le même établissement.</li>
<li>Vous pouvez contrôler quels types de services ont été effectués et/ou quelles pièces ont été changées.</li>
<li>Vous avez connaissance du nombre minimum de détenteurs.</li>
</ul>
    </div>
    <div class="col-sm-6 tableau">
<ul>
<li class="title center">Avantages de SecurSpot pour les vendeurs</li>
<li>Vous êtes le plus transparent possible. Ils se sentent rassuré.</li>
<li>En tant que vendeur, vous avez tout intérêt à rédiger votre rapport SecurSpot. Et ceci même si son contenu amène l’acheteur potentiel à soulever des questions légitimes, ou à mettre à jour des éléments qui sans cela lui seraient restés inconnus</li>
<li>Une relation de confiance se construit rapidement entre vous-même et l’acheteur potentiel.</li>
<li>Démarquez-vous des autres annonces avec le logo SecurSpot qui sera présent sur votre annonce. </li>
<li class="icone"><img src="images/distinction_kilometrique.png" > En cas de faible kilométrage, une distinction sera affichée sur la page de votre annonce. </li>
<li>Vous éviterai de nombreuses questions, car tout sera déjà présent dans le rapport SecurSpot.</li>
</ul>
            
</div>    
<div class="clear"></div>

<!-- SECURSPOT -->
</div>
<?php include("footer.php"); ?>