<!-- FILTRES VERSION BUREAU -->
<div class="row">

    <div class="col-sm-2 resultFilters">
        <form role="form" name="formaffiner" action="//<?PHP echo $_SERVER["HTTP_HOST"].$_SERVER['PHP_SELF']; ?>" method="GET">
            <input type="hidden" id="r" name="r" value="<?PHP if ((preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) || ((isset($_GET['r'])) && ($_GET["r"] == "moto"))) echo "moto";
else echo "voiture"; ?>">
            <!-- COLLAPSE -->
            <div class="container-fluid">
                <div class="form-group">
                    <label for="marque" class="small"><?PHP echo $LANG[32]; ?></label>
                    <?PHP
                    if ((isset($_GET['r'])) && ($_GET['r'] == "Toutes"))
                        $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche ORDER BY priorite DESC, marque ASC");
                    else if (isset($_GET['r']))
                        $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='$_GET[r]' ORDER BY priorite DESC, marque ASC");
                    else {
                        if ($_SERVER['SERVER_NAME'] == "www.motospot.ch")
                            $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='moto' ORDER BY priorite DESC, marque ASC");
                        else
                            $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='voiture' ORDER BY priorite DESC, marque ASC");
                    }
                    $marqueslist = "";
                    while ($marques = mysqli_fetch_object($requetetr)) {
                        $quotedmarques = preg_quote($marques->marque);
                        if ((!preg_match("/\|$quotedmarques\|/ui", $marqueslist)) && ($marques->marque != ""))
                            $marqueslist = $marqueslist . "|$marques->marque|";
                    }
                    $marqueslist = preg_replace("/^\|/", "", $marqueslist);
                    $marqueslist = preg_replace("/\|$/", "", $marqueslist);
                    ?>
                    <select class="form-control input-sm" id="marque" name="marque" onchange="changemarque2();">
                        <option value="Toutes" <?PHP if (!isset($_GET["marque"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                        <?PHP
                        $i = 0;
                        $explode = explode("||", $marqueslist);
                        while (isset($explode[$i])) {
                            $marque = $explode[$i];
                            if (preg_match("/$marque/i", $getq))
                                $getmarque = $marque;
                            ?><option value="<?PHP echo $marque; ?>" <?PHP if (((isset($_GET["marque"])) && ($_GET["marque"] == $marque)) || (preg_match("/$marque/i", $getq))) echo "selected"; ?>><?PHP echo $marque; ?></option><?PHP
                            $i++;
                        }
                        ?>
                    </select>
                    <div class="clear"></div>

                    <label for="modele" class="small"><?PHP echo $LANG[34]; ?></label>

                    <?PHP
                        $requetetr = mysqli_query($connect1, "SELECT * FROM formulaires_recherche WHERE modele != '' AND marque != '' ORDER BY modele ASC");
                        $i = 0;
                        while($modeles = mysqli_fetch_object($requetetr))
                        {
                            $modeleslist[$i]['marque'] = $modeles->marque;
                            $modeleslist[$i]['modele'] = $modeles->modele;
                            $i++;
                        }
                    ?>

                    <div class="hidden" id="modeleslist">
                        <?php 
                            $string = '';
                            foreach ($modeleslist as $key => $modele_infos) {
                                $string .= $modele_infos['marque'] .','.$modele_infos['modele'].';';
                            }
                            echo rtrim($string, ';');
                        ?>
                    </div>

                    <?php  
                    if (isset($_GET["modele"])) {
                        echo "<div class='hidden' id='modeleselected'>".$_GET['modele']."</div>";
                    }
                    ?>

                    <select class="form-control input-sm" id="modele" name="modele" required>
                            <option value=""></option>
                    </select>
                    <div class="clear"></div>

                    <label for="version" class="small">Version</label>
                    <input type="text" class="form-control" id="version" name="version" placeholder="Ex: Turbo, GTI, Sport" value="<?PHP if ((isset($_GET["version"])) && ($_GET["version"] != "")) echo $_GET["version"];?>">
                    <div class="clear"></div>

                    <label for="reference" class="small">Numéro d'annonce</label>
                    <input type="text" class="form-control" name="reference" placeholder="Ex: 678453" value="<?PHP if ((isset($_GET["reference"])) && ($_GET["reference"] != "")) echo $_GET["reference"];?>">
                    <div class="clear"></div>

                    <label for="prixMin" class="small leftSelect">Prix</label><br/>
                    <select class="form-control input-sm leftSelect" id="prixMin" name="prixMin">
                        <option value="" <?PHP if (!isset($_GET["prixMin"])) echo "selected"; ?>>De</option>
                        <option value="1000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "1000")) echo "selected"; ?>>CHF 1'000.-</option>
                        <option value="2000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "2000")) echo "selected"; ?>>CHF 2'000.-</option>
                        <option value="3000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "3000")) echo "selected"; ?>>CHF 3'000.-</option>
                        <option value="4000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "4000")) echo "selected"; ?>>CHF 4'000.-</option>
                        <option value="5000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "5000")) echo "selected"; ?>>CHF 5'000.-</option>
                        <option value="7500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "7500")) echo "selected"; ?>>CHF 7'500.-</option>
                        <option value="10000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "10000")) echo "selected"; ?>>CHF 10'000.-</option>
                        <option value="12500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "12500")) echo "selected"; ?>>CHF 12'500.-</option>
                        <option value="15000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "15000")) echo "selected"; ?>>CHF 15'000.-</option>
                        <option value="17500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "17500")) echo "selected"; ?>>CHF 17'500.-</option>
                        <option value="20000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "20000")) echo "selected"; ?>>CHF 20'000.-</option>
                        <option value="22500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "22500")) echo "selected"; ?>>CHF 22'500.-</option>
                        <option value="25000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "25000")) echo "selected"; ?>>CHF 25'000.-</option>
                        <option value="30000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "30000")) echo "selected"; ?>>CHF 30'000.-</option>
                        <option value="35000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "35000")) echo "selected"; ?>>CHF 35'000.-</option>
                        <option value="40000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "40000")) echo "selected"; ?>>CHF 40'000.-</option>
                        <option value="45000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "45000")) echo "selected"; ?>>CHF 45'000.-</option>
                        <option value="50000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "50000")) echo "selected"; ?>>CHF 50'000.-</option>
                        <option value="60000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "60000")) echo "selected"; ?>>CHF 60'000.-</option>
                        <option value="70000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "70000")) echo "selected"; ?>>CHF 70'000.-</option>
                        <option value="80000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "80000")) echo "selected"; ?>>CHF 80'000.-</option>
                        <option value="90000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "90000")) echo "selected"; ?>>CHF 90'000.-</option>
                        <option value="100000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "100000")) echo "selected"; ?>>CHF 100'000.-</option>
                        <option value="150000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "150000")) echo "selected"; ?>>CHF 150'000.-</option>
                        <option value="200000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "200000")) echo "selected"; ?>>CHF 200'000.-</option>
                        <option value="300000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "300000")) echo "selected"; ?>>CHF 300'000.-</option>
                    </select>

                    <select class="form-control input-sm leftSelect" id="prixMax" name="prixMax">
                        <option value="" <?PHP if (!isset($_GET["prixMax"])) echo "selected"; ?>>à</option>
                        <option value="1000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "1000")) echo "selected"; ?>>CHF 1'000.-</option>
                        <option value="2000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "2000")) echo "selected"; ?>>CHF 2'000.-</option>
                        <option value="3000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "3000")) echo "selected"; ?>>CHF 3'000.-</option>
                        <option value="4000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "4000")) echo "selected"; ?>>CHF 4'000.-</option>
                        <option value="5000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "5000")) echo "selected"; ?>>CHF 5'000.-</option>
                        <option value="7500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "7500")) echo "selected"; ?>>CHF 7'500.-</option>
                        <option value="10000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "10000")) echo "selected"; ?>>CHF 10'000.-</option>
                        <option value="12500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "12500")) echo "selected"; ?>>CHF 12'500.-</option>
                        <option value="15000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "15000")) echo "selected"; ?>>CHF 15'000.-</option>
                        <option value="17500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "17500")) echo "selected"; ?>>CHF 17'500.-</option>
                        <option value="20000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "20000")) echo "selected"; ?>>CHF 20'000.-</option>
                        <option value="22500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "22500")) echo "selected"; ?>>CHF 22'500.-</option>
                        <option value="25000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "25000")) echo "selected"; ?>>CHF 25'000.-</option>
                        <option value="30000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "30000")) echo "selected"; ?>>CHF 30'000.-</option>
                        <option value="35000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "35000")) echo "selected"; ?>>CHF 35'000.-</option>
                        <option value="40000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "40000")) echo "selected"; ?>>CHF 40'000.-</option>
                        <option value="45000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "45000")) echo "selected"; ?>>CHF 45'000.-</option>
                        <option value="50000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "50000")) echo "selected"; ?>>CHF 50'000.-</option>
                        <option value="60000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "60000")) echo "selected"; ?>>CHF 60'000.-</option>
                        <option value="70000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "70000")) echo "selected"; ?>>CHF 70'000.-</option>
                        <option value="80000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "80000")) echo "selected"; ?>>CHF 80'000.-</option>
                        <option value="90000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "90000")) echo "selected"; ?>>CHF 90'000.-</option>
                        <option value="100000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "100000")) echo "selected"; ?>>CHF 100'000.-</option>
                        <option value="150000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "150000")) echo "selected"; ?>>CHF 150'000.-</option>
                        <option value="200000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "200000")) echo "selected"; ?>>CHF 200'000.-</option>
                        <option value="300000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "300000")) echo "selected"; ?>>CHF 300'000.-</option>
                    </select>
                    <div class="clear"></div>

                    <label for="annee" class="small leftSelect">Année</label><br />
                    <select class="form-control input-sm leftSelect" id="anneeMin" name="anneeMin">
                        <option value="" <?PHP if (!isset($_GET["anneeMin"])) echo "selected"; ?>>De</option>
                        <option value="2017" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2017")) echo "selected"; ?>>2017</option>
                        <option value="2016" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2016")) echo "selected"; ?>>2016</option>
                        <option value="2015" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2015")) echo "selected"; ?>>2015</option>
                        <option value="2014" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2014")) echo "selected"; ?>>2014</option>
                        <option value="2013" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2013")) echo "selected"; ?>>2013</option>
                        <option value="2012" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2012")) echo "selected"; ?>>2012</option>
                        <option value="2011" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2011")) echo "selected"; ?>>2011</option>
                        <option value="2010" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2010")) echo "selected"; ?>>2010</option>
                        <option value="2009" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2009")) echo "selected"; ?>>2009</option>
                        <option value="2008" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2008")) echo "selected"; ?>>2008</option>
                        <option value="2007" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2007")) echo "selected"; ?>>2007</option>
                        <option value="2006" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2006")) echo "selected"; ?>>2006</option>
                        <option value="2005" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2005")) echo "selected"; ?>>2005</option>
                        <option value="2004" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2004")) echo "selected"; ?>>2004</option>
                        <option value="2003" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2003")) echo "selected"; ?>>2003</option>
                        <option value="2002" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2002")) echo "selected"; ?>>2002</option>
                        <option value="2001" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2001")) echo "selected"; ?>>2001</option>
                        <option value="2000" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2000")) echo "selected"; ?>>2000</option>
                        <option value="1999" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1999")) echo "selected"; ?>>1999</option>
                        <option value="1998" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1998")) echo "selected"; ?>>1998</option>
                        <option value="1997" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1997")) echo "selected"; ?>>1997</option>
                        <option value="1996" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1996")) echo "selected"; ?>>1996</option>
                        <option value="1995" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1995")) echo "selected"; ?>>1995</option>
                        <option value="1994" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1994")) echo "selected"; ?>>1994</option>
                        <option value="1993" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1993")) echo "selected"; ?>>1993</option>
                        <option value="1992" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1992")) echo "selected"; ?>>1992</option>
                        <option value="1991" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1991")) echo "selected"; ?>>1991</option>
                        <option value="1990" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1990")) echo "selected"; ?>>1990</option>
                        <option value="1989" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1989")) echo "selected"; ?>>1989</option>
                        <option value="1988" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1988")) echo "selected"; ?>>1988</option>
                        <option value="1987" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1987")) echo "selected"; ?>>1987</option>
                        <option value="1986" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1986")) echo "selected"; ?>>1986</option>
                        <option value="1985" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1985")) echo "selected"; ?>>1985</option>
                        <option value="1984" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1984")) echo "selected"; ?>>1984</option>
                        <option value="1983" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1983")) echo "selected"; ?>>1983</option>
                        <option value="1982" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1982")) echo "selected"; ?>>1982</option>
                        <option value="1981" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1981")) echo "selected"; ?>>1981</option>
                        <option value="1980" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1980")) echo "selected"; ?>>1980</option>
                        <option value="1975" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1975")) echo "selected"; ?>>1975</option>
                        <option value="1970" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1970")) echo "selected"; ?>>1970</option>
                        <option value="1960" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1960")) echo "selected"; ?>>1960</option>
                        <option value="1950" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1950")) echo "selected"; ?>>1950</option>
                        <option value="1940" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1940")) echo "selected"; ?>>1940</option>
                        <option value="1930" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1930")) echo "selected"; ?>>1930</option>
                    </select>
                    <select class="form-control input-sm leftSelect" id="anneeMax" name="anneeMax">
                        <option value="" <?PHP if (!isset($_GET["anneeMax"])) echo "selected"; ?>>à</option>
                        <option value="2017" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2017")) echo "selected"; ?>>2017</option>
                        <option value="2016" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2016")) echo "selected"; ?>>2016</option>
                        <option value="2015" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2015")) echo "selected"; ?>>2015</option>
                        <option value="2014" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2014")) echo "selected"; ?>>2014</option>
                        <option value="2013" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2013")) echo "selected"; ?>>2013</option>
                        <option value="2012" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2012")) echo "selected"; ?>>2012</option>
                        <option value="2011" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2011")) echo "selected"; ?>>2011</option>
                        <option value="2010" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2010")) echo "selected"; ?>>2010</option>
                        <option value="2009" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2009")) echo "selected"; ?>>2009</option>
                        <option value="2008" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2008")) echo "selected"; ?>>2008</option>
                        <option value="2007" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2007")) echo "selected"; ?>>2007</option>
                        <option value="2006" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2006")) echo "selected"; ?>>2006</option>
                        <option value="2005" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2005")) echo "selected"; ?>>2005</option>
                        <option value="2004" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2004")) echo "selected"; ?>>2004</option>
                        <option value="2003" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2003")) echo "selected"; ?>>2003</option>
                        <option value="2002" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2002")) echo "selected"; ?>>2002</option>
                        <option value="2001" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2001")) echo "selected"; ?>>2001</option>
                        <option value="2000" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2000")) echo "selected"; ?>>2000</option>
                        <option value="1999" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1999")) echo "selected"; ?>>1999</option>
                        <option value="1998" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1998")) echo "selected"; ?>>1998</option>
                        <option value="1997" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1997")) echo "selected"; ?>>1997</option>
                        <option value="1996" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1996")) echo "selected"; ?>>1996</option>
                        <option value="1995" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1995")) echo "selected"; ?>>1995</option>
                        <option value="1994" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1994")) echo "selected"; ?>>1994</option>
                        <option value="1993" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1993")) echo "selected"; ?>>1993</option>
                        <option value="1992" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1992")) echo "selected"; ?>>1992</option>
                        <option value="1991" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1991")) echo "selected"; ?>>1991</option>
                        <option value="1990" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1990")) echo "selected"; ?>>1990</option>
                        <option value="1989" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1989")) echo "selected"; ?>>1989</option>
                        <option value="1988" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1988")) echo "selected"; ?>>1988</option>
                        <option value="1987" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1987")) echo "selected"; ?>>1987</option>
                        <option value="1986" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1986")) echo "selected"; ?>>1986</option>
                        <option value="1985" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1985")) echo "selected"; ?>>1985</option>
                        <option value="1984" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1984")) echo "selected"; ?>>1984</option>
                        <option value="1983" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1983")) echo "selected"; ?>>1983</option>
                        <option value="1982" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1982")) echo "selected"; ?>>1982</option>
                        <option value="1981" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1981")) echo "selected"; ?>>1981</option>
                        <option value="1980" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1980")) echo "selected"; ?>>1980</option>
                        <option value="1975" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1975")) echo "selected"; ?>>1975</option>
                        <option value="1970" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1970")) echo "selected"; ?>>1970</option>
                        <option value="1960" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1960")) echo "selected"; ?>>1960</option>
                        <option value="1950" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1950")) echo "selected"; ?>>1950</option>
                        <option value="1940" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1940")) echo "selected"; ?>>1940</option>
                        <option value="1930" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1930")) echo "selected"; ?>>1930</option>
                    </select>
                    <div class="clear"></div>

                    <label for="kilometres" class="small leftSelect">Kilomètres</label><br />
                    <select class="form-control input-sm leftSelect" id="kilometresMin" name="kilometresMin">
                        <option value="" <?PHP if (!isset($_GET["kilometresMin"])) echo "selected"; ?>>De</option>
                        <option value="1000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "1000")) echo "selected"; ?>>1'000</option>
                        <option value="5000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "5000")) echo "selected"; ?>>5'000</option>
                        <option value="7500" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "7500")) echo "selected"; ?>>7'500</option>
                        <option value="10000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "10000")) echo "selected"; ?>>10'000</option>
                        <option value="20000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "20000")) echo "selected"; ?>>20'000</option>
                        <option value="30000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "30000")) echo "selected"; ?>>30'000</option>
                        <option value="40000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "40000")) echo "selected"; ?>>40'000</option>
                        <option value="50000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "50000")) echo "selected"; ?>>50'000</option>
                        <option value="60000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "60000")) echo "selected"; ?>>60'000</option>
                        <option value="70000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "70000")) echo "selected"; ?>>70'000</option>
                        <option value="80000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "80000")) echo "selected"; ?>>80'000</option>
                        <option value="90000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "90000")) echo "selected"; ?>>90'000</option>
                        <option value="100000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "100000")) echo "selected"; ?>>100'000</option>
                        <option value="110000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "110000")) echo "selected"; ?>>110'000</option>
                        <option value="120000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "120000")) echo "selected"; ?>>120'000</option>
                        <option value="130000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "130000")) echo "selected"; ?>>130'000</option>
                        <option value="140000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "140000")) echo "selected"; ?>>140'000</option>
                        <option value="150000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "150000")) echo "selected"; ?>>150'000</option>
                        <option value="160000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "160000")) echo "selected"; ?>>160'000</option>
                        <option value="170000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "170000")) echo "selected"; ?>>170'000</option>
                        <option value="180000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "180000")) echo "selected"; ?>>180'000</option>
                        <option value="190000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "190000")) echo "selected"; ?>>190'000</option>
                        <option value="200000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "200000")) echo "selected"; ?>>200'000</option>
                    </select>
                    <select class="form-control input-sm leftSelect" id="kilometresMax" name="kilometresMax" onchange="this.form.submit();">
                        <option value="" <?PHP if (!isset($_GET["kilometresMax"])) echo "selected"; ?>>à</option>
                        <option value="1000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "1000")) echo "selected"; ?>>1'000</option>
                        <option value="5000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "5000")) echo "selected"; ?>>5'000</option>
                        <option value="7500" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "7500")) echo "selected"; ?>>7'500</option>
                        <option value="10000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "10000")) echo "selected"; ?>>10'000</option>
                        <option value="20000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "20000")) echo "selected"; ?>>20'000</option>
                        <option value="30000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "30000")) echo "selected"; ?>>30'000</option>
                        <option value="40000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "40000")) echo "selected"; ?>>40'000</option>
                        <option value="50000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "50000")) echo "selected"; ?>>50'000</option>
                        <option value="60000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "60000")) echo "selected"; ?>>60'000</option>
                        <option value="70000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "70000")) echo "selected"; ?>>70'000</option>
                        <option value="80000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "80000")) echo "selected"; ?>>80'000</option>
                        <option value="90000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "90000")) echo "selected"; ?>>90'000</option>
                        <option value="100000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "100000")) echo "selected"; ?>>100'000</option>
                        <option value="110000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "110000")) echo "selected"; ?>>110'000</option>
                        <option value="120000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "120000")) echo "selected"; ?>>120'000</option>
                        <option value="130000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "130000")) echo "selected"; ?>>130'000</option>
                        <option value="140000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "140000")) echo "selected"; ?>>140'000</option>
                        <option value="150000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "150000")) echo "selected"; ?>>150'000</option>
                        <option value="160000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "160000")) echo "selected"; ?>>160'000</option>
                        <option value="170000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "170000")) echo "selected"; ?>>170'000</option>
                        <option value="180000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "180000")) echo "selected"; ?>>180'000</option>
                        <option value="190000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "190000")) echo "selected"; ?>>190'000</option>
                        <option value="200000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "200000")) echo "selected"; ?>>200'000</option>
                    </select>
                    <div class="clear"></div>

                    <label for="chevaux" class="small leftSelect">Chevaux</label><br />
                    <select class="form-control input-sm leftSelect" id="chevauxMin" name="chevauxMin">
                        <option value="" <?PHP if (!isset($_GET["chevauxMin"])) echo "selected"; ?>>De</option>
                        <option value="40" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "40")) echo "selected"; ?>>40</option>
                        <option value="50" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "50")) echo "selected"; ?>>50</option>
                        <option value="60" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "60")) echo "selected"; ?>>60</option>
                        <option value="70" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "70")) echo "selected"; ?>>70</option>
                        <option value="80" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "80")) echo "selected"; ?>>80</option>
                        <option value="90" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "90")) echo "selected"; ?>>90</option>
                        <option value="100" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "100")) echo "selected"; ?>>100</option>
                        <option value="110" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "110")) echo "selected"; ?>>110</option>
                        <option value="120" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "120")) echo "selected"; ?>>120</option>
                        <option value="130" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "130")) echo "selected"; ?>>130</option>
                        <option value="140" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "140")) echo "selected"; ?>>140</option>
                        <option value="150" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "150")) echo "selected"; ?>>150</option>
                        <option value="160" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "160")) echo "selected"; ?>>160</option>
                        <option value="170" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "170")) echo "selected"; ?>>170</option>
                        <option value="180" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "180")) echo "selected"; ?>>180</option>
                        <option value="190" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "190")) echo "selected"; ?>>190</option>
                        <option value="200" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "200")) echo "selected"; ?>>200</option>
                        <option value="210" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "210")) echo "selected"; ?>>210</option>
                        <option value="220" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "220")) echo "selected"; ?>>220</option>
                        <option value="230" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "230")) echo "selected"; ?>>230</option>
                        <option value="240" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "240")) echo "selected"; ?>>240</option>
                        <option value="250" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "250")) echo "selected"; ?>>250</option>
                        <option value="260" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "260")) echo "selected"; ?>>260</option>
                        <option value="270" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "270")) echo "selected"; ?>>270</option>
                        <option value="280" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "280")) echo "selected"; ?>>280</option>
                        <option value="290" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "290")) echo "selected"; ?>>290</option>
                        <option value="300" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "300")) echo "selected"; ?>>300</option>
                        <option value="320" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "320")) echo "selected"; ?>>320</option>
                        <option value="340" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "340")) echo "selected"; ?>>340</option>
                        <option value="360" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "360")) echo "selected"; ?>>360</option>
                        <option value="380" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "380")) echo "selected"; ?>>380</option>
                        <option value="400" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "400")) echo "selected"; ?>>400</option>
                        <option value="450" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "450")) echo "selected"; ?>>450</option>
                        <option value="500" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "500")) echo "selected"; ?>>500</option>
                        <option value="550" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "550")) echo "selected"; ?>>550</option>
                        <option value="600" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "600")) echo "selected"; ?>>600</option>
                        <option value="650" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "650")) echo "selected"; ?>>650</option>
                        <option value="700" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "700")) echo "selected"; ?>>700</option>
                    </select>
                    <select class="form-control input-sm leftSelect" id="chevauxMax" name="chevauxMax" onchange="this.form.submit();">
                        <option value="" <?PHP if (!isset($_GET["chevauxMax"])) echo "selected"; ?>>à</option>
                        <option value="40" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "40")) echo "selected"; ?>>40</option>
                        <option value="50" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "50")) echo "selected"; ?>>50</option>
                        <option value="60" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "60")) echo "selected"; ?>>60</option>
                        <option value="70" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "70")) echo "selected"; ?>>70</option>
                        <option value="80" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "80")) echo "selected"; ?>>80</option>
                        <option value="90" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "90")) echo "selected"; ?>>90</option>
                        <option value="100" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "100")) echo "selected"; ?>>100</option>
                        <option value="110" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "110")) echo "selected"; ?>>110</option>
                        <option value="120" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "120")) echo "selected"; ?>>120</option>
                        <option value="130" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "130")) echo "selected"; ?>>130</option>
                        <option value="140" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "140")) echo "selected"; ?>>140</option>
                        <option value="150" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "150")) echo "selected"; ?>>150</option>
                        <option value="160" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "160")) echo "selected"; ?>>160</option>
                        <option value="170" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "170")) echo "selected"; ?>>170</option>
                        <option value="180" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "180")) echo "selected"; ?>>180</option>
                        <option value="190" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "190")) echo "selected"; ?>>190</option>
                        <option value="200" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "200")) echo "selected"; ?>>200</option>
                        <option value="210" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "210")) echo "selected"; ?>>210</option>
                        <option value="220" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "220")) echo "selected"; ?>>220</option>
                        <option value="230" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "230")) echo "selected"; ?>>230</option>
                        <option value="240" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "240")) echo "selected"; ?>>240</option>
                        <option value="250" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "250")) echo "selected"; ?>>250</option>
                        <option value="260" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "260")) echo "selected"; ?>>260</option>
                        <option value="270" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "270")) echo "selected"; ?>>270</option>
                        <option value="280" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "280")) echo "selected"; ?>>280</option>
                        <option value="290" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "290")) echo "selected"; ?>>290</option>
                        <option value="300" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "300")) echo "selected"; ?>>300</option>
                        <option value="320" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "320")) echo "selected"; ?>>320</option>
                        <option value="340" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "340")) echo "selected"; ?>>340</option>
                        <option value="360" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "360")) echo "selected"; ?>>360</option>
                        <option value="380" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "380")) echo "selected"; ?>>380</option>
                        <option value="400" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "400")) echo "selected"; ?>>400</option>
                        <option value="450" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "450")) echo "selected"; ?>>450</option>
                        <option value="500" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "500")) echo "selected"; ?>>500</option>
                        <option value="550" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "550")) echo "selected"; ?>>550</option>
                        <option value="600" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "600")) echo "selected"; ?>>600</option>
                        <option value="650" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "650")) echo "selected"; ?>>650</option>
                        <option value="700" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "700")) echo "selected"; ?>>700</option>
                    </select>
                    <div class="clear"></div>

<?PHP
if (preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
    ?>
                        <label for="cylindree" class="small leftSelect"><?PHP echo $LANG[39]; ?></label><br />
                        <select class="form-control input-sm leftSelect" id="cylindreeMin" name="cylindreeMin">
                            <option value="Tous" <?PHP if (!isset($_GET["cylindreeMin"])) echo "selected"; ?>>à</option>
                            <option value="600" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "600")) echo "selected"; ?>>600 cm3</option>
                            <option value="800" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "800")) echo "selected"; ?>>800 cm3</option>
                            <option value="1000" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "1000")) echo "selected"; ?>>1000 cm3</option>
                            <option value="1200" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "1200")) echo "selected"; ?>>1200 cm3</option>
                            <option value="1400" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "1400")) echo "selected"; ?>>1400 cm3</option>
                            <option value="1600" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "1600")) echo "selected"; ?>>1600 cm3</option>
                            <option value="1800" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "1800")) echo "selected"; ?>>1800 cm3</option>
                            <option value="2000" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "2000")) echo "selected"; ?>>2000 cm3</option>
                            <option value="2500" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "2500")) echo "selected"; ?>>2500 cm3</option>
                            <option value="3000" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "3000")) echo "selected"; ?>>3000 cm3</option>
                            <option value="3500" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "3500")) echo "selected"; ?>>3500 cm3</option>
                            <option value="4000" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "4000")) echo "selected"; ?>>4000 cm3</option>
                            <option value="4500" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "4500")) echo "selected"; ?>>4500 cm3</option>
                            <option value="5000" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "5000")) echo "selected"; ?>>5000 cm3</option>
                            <option value="5500" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "5500")) echo "selected"; ?>>5500 cm3</option>
                            <option value="6000" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "6000")) echo "selected"; ?>>6000 cm3</option>
                            <option value="6500" <?PHP if ((isset($_GET["cylindreeMin"])) && ($_GET["cylindreeMin"] == "6500")) echo "selected"; ?>>6500 cm3</option>
                        </select>
                        <select class="form-control input-sm leftSelect" id="cylindreeMax" name="cylindreeMax">
                            <option value="Tous" <?PHP if (!isset($_GET["cylindreeMax"])) echo "selected"; ?>>à</option>
                            <option value="600" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "600")) echo "selected"; ?>>600 cm3</option>
                            <option value="800" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "800")) echo "selected"; ?>>800 cm3</option>
                            <option value="1000" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "1000")) echo "selected"; ?>>1000 cm3</option>
                            <option value="1200" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "1200")) echo "selected"; ?>>1200 cm3</option>
                            <option value="1400" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "1400")) echo "selected"; ?>>1400 cm3</option>
                            <option value="1600" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "1600")) echo "selected"; ?>>1600 cm3</option>
                            <option value="1800" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "1800")) echo "selected"; ?>>1800 cm3</option>
                            <option value="2000" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "2000")) echo "selected"; ?>>2000 cm3</option>
                            <option value="2500" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "2500")) echo "selected"; ?>>2500 cm3</option>
                            <option value="3000" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "3000")) echo "selected"; ?>>3000 cm3</option>
                            <option value="3500" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "3500")) echo "selected"; ?>>3500 cm3</option>
                            <option value="4000" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "4000")) echo "selected"; ?>>4000 cm3</option>
                            <option value="4500" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "4500")) echo "selected"; ?>>4500 cm3</option>
                            <option value="5000" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "5000")) echo "selected"; ?>>5000 cm3</option>
                            <option value="5500" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "5500")) echo "selected"; ?>>5500 cm3</option>
                            <option value="6000" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "6000")) echo "selected"; ?>>6000 cm3</option>
                            <option value="6500" <?PHP if ((isset($_GET["cylindreeMax"])) && ($_GET["cylindreeMax"] == "6500")) echo "selected"; ?>>6500 cm3</option>
                        </select>
                        <div class="clear"></div>
    <?PHP
}
else {
    ?><input type="hidden" id="cylindrees" name="cylindrees" value=""><?PHP
}
?>

                    <label for="typevehicule" class="small"><?PHP echo $LANG[40]; ?></label>
                    <select class="form-control input-sm" id="typevehicule" name="typevehicule">
                        <option value="Tous" <?PHP if (!isset($_GET["typevehicule"])) echo "selected"; ?>><?PHP echo $LANG[35]; ?></option>
												<option value="neuf" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "neuf")) echo "selected"; ?>><?PHP echo $LANG[41]; ?></option>
                        <option value="occasion" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "occasion")) echo "selected"; ?>><?PHP echo $LANG[42]; ?></option>
                        <option value="demonstration" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "demonstration")) echo "selected"; ?>><?PHP echo $LANG[43]; ?></option>
                        <option value="collection" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "collection")) echo "selected"; ?>><?PHP echo $LANG[44]; ?></option>
                    </select>
                    <div class="clear"></div>

                    <label for="transmission" class="small"><?PHP echo $LANG[45]; ?></label>
                    <select class="form-control input-sm" id="transmission" name="transmission">
                        <option value="Toutes" <?PHP if (!isset($_GET["transmission"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                        <option value="manuel" <?PHP if ((isset($_GET["transmission"])) && ($_GET["transmission"] == "manuel")) echo "selected"; ?>><?PHP echo $LANG[46]; ?></option>
                        <option value="automatique" <?PHP if ((isset($_GET["transmission"])) && ($_GET["transmission"] == "automatique")) echo "selected"; ?>><?PHP echo $LANG[47]; ?></option>
                        <option value="sequentielle" <?PHP if ((isset($_GET["transmission"])) && ($_GET["transmission"] == "sequentielle")) echo "selected"; ?>><?PHP echo $LANG[48]; ?></option>
                    </select>
                    <div class="clear"></div>

                    <label for="carburant" class="small"><?PHP echo $LANG[49]; ?></label>
                    <select class="form-control input-sm" id="carburant" name="carburant">
                        <option value="Tous" <?PHP if (!isset($_GET["carburant"])) echo "selected"; ?>><?PHP echo $LANG[35]; ?></option>
                        <option value="essence" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "essence")) echo "selected"; ?>><?PHP echo $LANG[50]; ?></option>
                        <option value="diesel" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "diesel")) echo "selected"; ?>><?PHP echo $LANG[51]; ?></option>
                        <option value="electrique" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "electrique")) echo "selected"; ?>><?PHP echo $LANG[52]; ?></option>
                        <option value="hybride" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "hybride")) echo "selected"; ?>><?PHP echo $LANG[53]; ?></option>
<?PHP
if (!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
    ?>
                            <option value="gaz" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "gaz")) echo "selected"; ?>><?PHP echo $LANG[54]; ?></option>
                            <option value="bioethanol" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "bioethanol")) echo "selected"; ?>><?PHP echo $LANG[55]; ?></option>
    <?PHP
}
else {
    ?>
                            <option value="mÃ©lange 2 temps" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "mÃ©lange 2 temps")) echo "selected"; ?>><?PHP echo $LANG[56]; ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                    <div class="clear"></div>

                        <?PHP
                        if (!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
                            ?>
                        <label for="carrosserie" class="small"><?PHP echo $LANG[57]; ?></label>
                        <select class="form-control input-sm selectpicker" id="carrosserie" name="carrosserie">
                            <option data-content="<i class='fa fa-car'></i>" value="Toutes" <?PHP if (!isset($_GET["carrosserie"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                            <option data-content="<img src='images/Berline.png'> Berline" value="berline" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "berline")) echo "selected"; ?>><?PHP echo $LANG[58]; ?></option>
                            <option data-content="<img src='images/Break.png'> Break" value="break" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "break")) echo "selected"; ?>><?PHP echo $LANG[59]; ?></option>
                            <option data-content="<img src='images/Cabriolet.png'> Cabriolet" value="cabriolet" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "cabriolet")) echo "selected"; ?>><?PHP echo $LANG[60]; ?></option>
                            <option data-content="<img src='images/Coupe.png'> Coupé" value="coupe" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "coupe")) echo "selected"; ?>><?PHP echo $LANG[61]; ?></option>
                            <option data-content="<img src='images/Monospace.png'> Monospace" value="monospace" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "monospace")) echo "selected"; ?>><?PHP echo $LANG[62]; ?></option>
                            <option data-content="<img src='images/Petite-voiture-citadine.png'> Petite citadine" value="petite voiture" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "petite voiture")) echo "selected"; ?>><?PHP echo $LANG[63]; ?></option>
                            <option data-content="<img src='images/SUV-tout-terrain.png'> SUV tout-terrain" value="suv" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "suv/tout-terrain")) echo "selected"; ?>><?PHP echo $LANG[65]; ?></option>
                            <option data-content="<img src='images/Pick-up.png'> Pick-up" value="pickup" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "pick-up")) echo "selected"; ?>><?PHP echo $LANG[64]; ?></option>  
                        </select>
                        <div class="clear"></div>
    <?PHP
}
else {
    ?><input type="hidden" id="carrosserie" name="carrosserie" value="Toutes"><?PHP
}
?>

                    <label for="couleurexterne" class="small"><?PHP echo $LANG[66]; ?></label>
                    <select class="form-control input-sm" id="couleurexterne" name="couleurexterne">
                        <option value="Toutes" <?PHP if (!isset($_GET["couleurexterne"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                        <option value="anthracite" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "anthracite")) echo "selected"; ?>><?PHP echo $LANG[67]; ?></option>
                        <option value="argent" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "argent")) echo "selected"; ?>><?PHP echo $LANG[68]; ?></option>
                        <option value="beige" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "beige")) echo "selected"; ?>><?PHP echo $LANG[69]; ?></option>
                        <option value="blanc" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "blanc")) echo "selected"; ?>><?PHP echo $LANG[70]; ?></option>
                        <option value="bleu" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "bleu")) echo "selected"; ?>><?PHP echo $LANG[71]; ?></option>
                        <option value="bordeaux" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "bordeaux")) echo "selected"; ?>><?PHP echo $LANG[72]; ?></option>
                        <option value="brun" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "brun")) echo "selected"; ?>><?PHP echo $LANG[73]; ?></option>
                        <option value="gris" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "gris")) echo "selected"; ?>><?PHP echo $LANG[74]; ?></option>
                        <option value="jaune" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "jaune")) echo "selected"; ?>><?PHP echo $LANG[75]; ?></option>
                        <option value="noir" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "noir")) echo "selected"; ?>><?PHP echo $LANG[76]; ?></option>
                        <option value="or" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "or")) echo "selected"; ?>><?PHP echo $LANG[77]; ?></option>
                        <option value="orange" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "orange")) echo "selected"; ?>><?PHP echo $LANG[78]; ?></option>
                        <option value="rose" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "rose")) echo "selected"; ?>><?PHP echo $LANG[79]; ?></option>
                        <option value="rouge" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "rouge")) echo "selected"; ?>><?PHP echo $LANG[80]; ?></option>
                        <option value="turquoise" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "turquoise")) echo "selected"; ?>><?PHP echo $LANG[81]; ?></option>
                        <option value="vert" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "vert")) echo "selected"; ?>><?PHP echo $LANG[82]; ?></option>
                        <option value="violet" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "violet")) echo "selected"; ?>><?PHP echo $LANG[83]; ?></option>
                    </select>
                    <div class="clear"></div>

<?PHP
if (!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
    ?>
                        <label for="rouesmotrices" class="small"><?PHP echo $LANG[84]; ?></label>
                        <select class="form-control input-sm" id="rouesmotrices" name="rouesmotrices">
                            <option value="Toutes" <?PHP if (!isset($_GET["rouesmotrices"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                            <option value="4roues" <?PHP if ((isset($_GET["rouesmotrices"])) && ($_GET["rouesmotrices"] == "4roues")) echo "selected"; ?>><?PHP echo $LANG[85]; ?></option>
                            <option value="tractionarriere" <?PHP if ((isset($_GET["rouesmotrices"])) && ($_GET["rouesmotrices"] == "tractionarriere")) echo "selected"; ?>><?PHP echo $LANG[86]; ?></option>
                            <option value="tractionavant" <?PHP if ((isset($_GET["rouesmotrices"])) && ($_GET["rouesmotrices"] == "tractionavant")) echo "selected"; ?>><?PHP echo $LANG[87]; ?></option>
                        </select>
                        <div class="clear"></div>
                        <?PHP
                    }
                    else {
                        ?><input type="hidden" id="rouesmotrices" name="rouesmotrices" value="Toutes"><?PHP
                    }
                    ?>
					
					<input type="hidden" id="expertisee" name="expertisee" value="">
					
					<input type="hidden" id="expertisee" name="photo" value="">
					

                    <div class="clear"></div>
                    <button type="button" class="btn btn-primary" onclick="this.form.submit();"><?PHP echo $LANG[90]; ?></button>
                    <button type="button" class="btn btn-primary effacer" onclick="resetFormFilter(this.form);
        document.getElementById('q').value = '<?PHP echo $LANG[4]; ?>';"><?PHP echo $LANG[91]; ?></button>

                </div>
            </div>
    </div>
    <!-- FIN FILTRES VERSION BUREAU -->

    <!--FIXED MENU MOBILE 18/02/16-->
    <div id="fixedMenu">
    <div class="mk_count_one">
        <!--SUISSE--><div class="mobileSelect">
            <select id="region" name="region" class="form-control suisse select-icon" onchange="this.form.submit();">
                <option value="suisse" selected=""></option>
                <option value="suisse"><?PHP echo $LANG[28]; ?></option>
                <option value="suisseallemande" <?PHP if ((isset($_GET["region"])) && ($_GET["region"] == "suisseallemande")) echo "selected"; ?>><?PHP echo $LANG[29]; ?></option>
                <option value="suisseitalienne" <?PHP if ((isset($_GET["region"])) && ($_GET["region"] == "suisseitalienne")) echo "selected"; ?>><?PHP echo $LANG[30]; ?></option>
                <option value="suisseromande" <?PHP if ((isset($_GET["region"])) && ($_GET["region"] == "suisseromande")) echo "selected"; ?>><?PHP echo $LANG[31]; ?></option>
            </select>
        </div>
        <!--FILTRES--><div class="mobileSelect iconFiltres">

            <!--lien pour la position fixed-->
            <a id="posFix" href="javascript:void(0);" onclick="$('#indexFooter').toggle();" data-toggle="collapse" data-target="#fixedFiltres, #myResult" class="hidden cls-toggle-class"><img src="images/spacer.gif" class="openFiltre"></a>
            <!-- <a id="posFix" href="#aFiltres" data-toggle="collapse" data-target="#fixedFiltres, #myResult" class="hidden"><img src="images/spacer.gif" class="openFiltre"></a> -->
            <!--lien pour la position relative--><a id="posRel" onclick="$('#indexFooter').toggle();" data-toggle="collapse" data-target="#fixedFiltres, #myResult" class="cls-toggle-class"><img src="images/spacer.gif" class="openFiltre"></a>
            <div id="fixedFiltres" class="collapse">
                <div id="pushFiltres" class="noPush"><a name="aFiltres"></a></div>
                <label for="marque" class="small"><?PHP echo $LANG[32]; ?></label>
<?PHP
if ((isset($_GET['r'])) && ($_GET['r'] == "Toutes"))
    $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche ORDER BY priorite DESC, marque ASC");
else if (isset($_GET['r']))
    $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='$_GET[r]' ORDER BY priorite DESC, marque ASC");
else {
    if ($_SERVER['SERVER_NAME'] == "www.autospot.ch")
        $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='voiture' ORDER BY priorite DESC, marque ASC");
    else
        $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='moto' ORDER BY priorite DESC, marque ASC");
}
$marqueslist = "";
while ($marques = mysqli_fetch_object($requetetr)) {
    $quotedmarques = preg_quote($marques->marque);
    if ((!preg_match("/\|$quotedmarques\|/ui", $marqueslist)) && ($marques->marque != ""))
        $marqueslist = $marqueslist . "|$marques->marque|";
}
$marqueslist = preg_replace("/^\|/", "", $marqueslist);
$marqueslist = preg_replace("/\|$/", "", $marqueslist);
?>
                <select class="form-control input-sm" id="marquebis" onchange="document.getElementById('marque').value = document.getElementById('marquebis').value;
                  changemarque2();">
                    <option value="Toutes" <?PHP if (!isset($_GET["marque"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                <?PHP
                $i = 0;
                $explode = explode("||", $marqueslist);
                while (isset($explode[$i])) {
                    $marque = $explode[$i];
                    if (preg_match("/$marque/i", $getq))
                        $getmarque = $marque;
                    ?><option value="<?PHP echo $marque; ?>" <?PHP if (((isset($_GET["marque"])) && ($_GET["marque"] == $marque)) || (preg_match("/$marque/i", $getq))) echo "selected"; ?>><?PHP echo $marque; ?></option><?PHP
                    $i++;
                }
                ?>
                </select>
                <label for="modele" class="small"><?PHP echo $LANG[34]; ?></label>

                

                    <select class="form-control input-sm" id="modelebis" name="modele" required>
                            <option value=""></option>
                    </select>

                    <script type="text/javascript">
                        function changemarque2() {
                            var modeleslist = $('#modeleslist').text().split(";"),
                                marque_selected = $('#marque').val(),
                                modeleselected = $('#modeleselected').text(),
                                options = ['<option value=""><?= $LANG[35] ;?></option>'];
                            for (var i = 0; i < modeleslist.length; i++) {
                                var split = modeleslist[i].split(','),
                                    marque = split[0],
                                    modele = split[1];

                                if (marque == marque_selected) {
                                    if (modele == modeleselected) { var selected = 'selected'; }else{ var selected = ''; }
                                    option = '<option value='+modele+' '+selected+'>'+modele+'</option>';
                                    options.push(option);
                                }
                            }
                            $('#modele').html(options.join(''));
                            $('#modelebis').html(options.join(''));
                        }
                        changemarque2();
                    </script>





                <?PHP
                // if ((isset($_GET["marque"])) || (isset($getmarque))) {
                //     if (isset($getmarque))
                //         $marque = $getmarque;
                //     else
                //         $marque = $_GET["marque"];
                //     if ($_GET['r'] == "Toutes")
                //         $requetetr = mysqli_query($connect1, "SELECT modele FROM formulaires_recherche WHERE marque='$marque' ORDER BY modele ASC");
                //     else
                //         $requetetr = mysqli_query($connect1, "SELECT modele FROM formulaires_recherche WHERE marque='$marque' && choix='$_GET[r]' ORDER BY modele ASC");
                //     $modeleslist = "";
                //     while ($modeles = mysqli_fetch_object($requetetr)) {
                //         if ((!preg_match("/\|$modeles->modele\|/", $modeleslist)) && ($modeles->modele != ""))
                //             $modeleslist = $modeleslist . "|$modeles->modele|";
                //     }
                //     $modeleslist = preg_replace("/^\|/", "", $modeleslist);
                //     $modeleslist = preg_replace("/\|$/", "", $modeleslist);
                // }
                ?>
                <!-- <select class="form-control input-sm" id="modelebis" onchange="document.getElementById('modele').value = document.getElementById('modelebis').value;"> -->
                    <!-- <option value="Tous" <?PHP if (!isset($_GET["modele"])) echo "selected"; ?>><?PHP echo $LANG[35]; ?></option> -->
                <?PHP
                // $i = 0;
                // if ($modeleslist != "") {
                    // $explode = explode("||", $modeleslist);
                    // while (isset($explode[$i])) {
                        // $modele = $explode[$i];
                        ?>
                        <!-- <option value="<?PHP echo $modele; ?>" <?PHP if (((isset($_GET["modele"])) && ($_GET["modele"] == $modele)) || (preg_match("/$modele/i", $getq))) echo "selected"; ?>><?PHP echo $modele; ?></option> -->
                        <?PHP
                        // $i++;
                    // }
                // }
                ?>
				<!-- </select> -->
				
				<label for="version" class="small">Version</label>
				<input type="text" class="form-control" id="version" name="version" placeholder="Ex: Turbo, GTI, Sport" value="<?PHP if ((isset($_GET["version"])) && ($_GET["version"] != "")) echo $_GET["version"];?>">
				
				

                <label for="prixMin" class="small leftSelect">Prix</label><br/>
                <select class="form-control input-sm leftSelect" id="prixMin" name="prixMin">
                    <option value="" <?PHP if (!isset($_GET["prixMin"])) echo "selected"; ?>>De</option>
                    <option value="1000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "1000")) echo "selected"; ?>>CHF 1'000.-</option>
                    <option value="2000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "2000")) echo "selected"; ?>>CHF 2'000.-</option>
                    <option value="3000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "3000")) echo "selected"; ?>>CHF 3'000.-</option>
                    <option value="4000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "4000")) echo "selected"; ?>>CHF 4'000.-</option>
                    <option value="5000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "5000")) echo "selected"; ?>>CHF 5'000.-</option>
                    <option value="7500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "7500")) echo "selected"; ?>>CHF 7'500.-</option>
                    <option value="10000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "10000")) echo "selected"; ?>>CHF 10'000.-</option>
                    <option value="12500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "12500")) echo "selected"; ?>>CHF 12'500.-</option>
                    <option value="15000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "15000")) echo "selected"; ?>>CHF 15'000.-</option>
                    <option value="17500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "17500")) echo "selected"; ?>>CHF 17'500.-</option>
                    <option value="20000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "20000")) echo "selected"; ?>>CHF 20'000.-</option>
                    <option value="22500" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "22500")) echo "selected"; ?>>CHF 22'500.-</option>
                    <option value="25000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "25000")) echo "selected"; ?>>CHF 25'000.-</option>
                    <option value="30000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "30000")) echo "selected"; ?>>CHF 30'000.-</option>
                    <option value="35000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "35000")) echo "selected"; ?>>CHF 35'000.-</option>
                    <option value="40000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "40000")) echo "selected"; ?>>CHF 40'000.-</option>
                    <option value="45000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "45000")) echo "selected"; ?>>CHF 45'000.-</option>
                    <option value="50000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "50000")) echo "selected"; ?>>CHF 50'000.-</option>
                    <option value="60000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "60000")) echo "selected"; ?>>CHF 60'000.-</option>
                    <option value="70000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "70000")) echo "selected"; ?>>CHF 70'000.-</option>
                    <option value="80000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "80000")) echo "selected"; ?>>CHF 80'000.-</option>
                    <option value="90000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "90000")) echo "selected"; ?>>CHF 90'000.-</option>
                    <option value="100000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "100000")) echo "selected"; ?>>CHF 100'000.-</option>
                    <option value="150000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "150000")) echo "selected"; ?>>CHF 150'000.-</option>
                    <option value="200000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "200000")) echo "selected"; ?>>CHF 200'000.-</option>
                    <option value="300000" <?PHP if ((isset($_GET["prixMin"])) && ($_GET["prixMin"] == "300000")) echo "selected"; ?>>CHF 300'000.-</option>
                </select>

                <select class="form-control input-sm leftSelect" id="prixMax" name="prixMax">
                    <option value="" <?PHP if (!isset($_GET["prixMax"])) echo "selected"; ?>>à</option>
                    <option value="1000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "1000")) echo "selected"; ?>>CHF 1'000.-</option>
                    <option value="2000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "2000")) echo "selected"; ?>>CHF 2'000.-</option>
                    <option value="3000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "3000")) echo "selected"; ?>>CHF 3'000.-</option>
                    <option value="4000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "4000")) echo "selected"; ?>>CHF 4'000.-</option>
                    <option value="5000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "5000")) echo "selected"; ?>>CHF 5'000.-</option>
                    <option value="7500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "7500")) echo "selected"; ?>>CHF 7'500.-</option>
                    <option value="10000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "10000")) echo "selected"; ?>>CHF 10'000.-</option>
                    <option value="12500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "12500")) echo "selected"; ?>>CHF 12'500.-</option>
                    <option value="15000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "15000")) echo "selected"; ?>>CHF 15'000.-</option>
                    <option value="17500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "17500")) echo "selected"; ?>>CHF 17'500.-</option>
                    <option value="20000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "20000")) echo "selected"; ?>>CHF 20'000.-</option>
                    <option value="22500" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "22500")) echo "selected"; ?>>CHF 22'500.-</option>
                    <option value="25000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "25000")) echo "selected"; ?>>CHF 25'000.-</option>
                    <option value="30000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "30000")) echo "selected"; ?>>CHF 30'000.-</option>
                    <option value="35000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "35000")) echo "selected"; ?>>CHF 35'000.-</option>
                    <option value="40000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "40000")) echo "selected"; ?>>CHF 40'000.-</option>
                    <option value="45000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "45000")) echo "selected"; ?>>CHF 45'000.-</option>
                    <option value="50000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "50000")) echo "selected"; ?>>CHF 50'000.-</option>
                    <option value="60000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "60000")) echo "selected"; ?>>CHF 60'000.-</option>
                    <option value="70000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "70000")) echo "selected"; ?>>CHF 70'000.-</option>
                    <option value="80000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "80000")) echo "selected"; ?>>CHF 80'000.-</option>
                    <option value="90000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "90000")) echo "selected"; ?>>CHF 90'000.-</option>
                    <option value="100000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "100000")) echo "selected"; ?>>CHF 100'000.-</option>
                    <option value="150000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "150000")) echo "selected"; ?>>CHF 150'000.-</option>
                    <option value="200000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "200000")) echo "selected"; ?>>CHF 200'000.-</option>
                    <option value="300000" <?PHP if ((isset($_GET["prixMax"])) && ($_GET["prixMax"] == "300000")) echo "selected"; ?>>CHF 300'000.-</option>
                </select>
                <div class="clearfix"></div>

                <label for="annee" class="small leftSelect">Année</label><br />
                <select class="form-control input-sm leftSelect" id="anneeMin" name="anneeMin">
                    <option value="" <?PHP if (!isset($_GET["anneeMin"])) echo "selected"; ?>>De</option>
                    <option value="2017" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2017")) echo "selected"; ?>>2017</option>
                    <option value="2016" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2016")) echo "selected"; ?>>2016</option>
                    <option value="2015" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2015")) echo "selected"; ?>>2015</option>
                    <option value="2014" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2014")) echo "selected"; ?>>2014</option>
                    <option value="2013" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2013")) echo "selected"; ?>>2013</option>
                    <option value="2012" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2012")) echo "selected"; ?>>2012</option>
                    <option value="2011" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2011")) echo "selected"; ?>>2011</option>
                    <option value="2010" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2010")) echo "selected"; ?>>2010</option>
                    <option value="2009" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2009")) echo "selected"; ?>>2009</option>
                    <option value="2008" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2008")) echo "selected"; ?>>2008</option>
                    <option value="2007" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2007")) echo "selected"; ?>>2007</option>
                    <option value="2006" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2006")) echo "selected"; ?>>2006</option>
                    <option value="2005" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2005")) echo "selected"; ?>>2005</option>
                    <option value="2004" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2004")) echo "selected"; ?>>2004</option>
                    <option value="2003" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2003")) echo "selected"; ?>>2003</option>
                    <option value="2002" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2002")) echo "selected"; ?>>2002</option>
                    <option value="2001" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2001")) echo "selected"; ?>>2001</option>
                    <option value="2000" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "2000")) echo "selected"; ?>>2000</option>
                    <option value="1999" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1999")) echo "selected"; ?>>1999</option>
                    <option value="1998" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1998")) echo "selected"; ?>>1998</option>
                    <option value="1997" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1997")) echo "selected"; ?>>1997</option>
                    <option value="1996" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1996")) echo "selected"; ?>>1996</option>
                    <option value="1995" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1995")) echo "selected"; ?>>1995</option>
                    <option value="1994" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1994")) echo "selected"; ?>>1994</option>
                    <option value="1993" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1993")) echo "selected"; ?>>1993</option>
                    <option value="1992" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1992")) echo "selected"; ?>>1992</option>
                    <option value="1991" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1991")) echo "selected"; ?>>1991</option>
                    <option value="1990" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1990")) echo "selected"; ?>>1990</option>
                    <option value="1989" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1989")) echo "selected"; ?>>1989</option>
                    <option value="1988" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1988")) echo "selected"; ?>>1988</option>
                    <option value="1987" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1987")) echo "selected"; ?>>1987</option>
                    <option value="1986" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1986")) echo "selected"; ?>>1986</option>
                    <option value="1985" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1985")) echo "selected"; ?>>1985</option>
                    <option value="1984" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1984")) echo "selected"; ?>>1984</option>
                    <option value="1983" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1983")) echo "selected"; ?>>1983</option>
                    <option value="1982" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1982")) echo "selected"; ?>>1982</option>
                    <option value="1981" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1981")) echo "selected"; ?>>1981</option>
                    <option value="1980" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1980")) echo "selected"; ?>>1980</option>
                    <option value="1975" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1975")) echo "selected"; ?>>1975</option>
                    <option value="1970" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1970")) echo "selected"; ?>>1970</option>
                    <option value="1960" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1960")) echo "selected"; ?>>1960</option>
                    <option value="1950" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1950")) echo "selected"; ?>>1950</option>
                    <option value="1940" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1940")) echo "selected"; ?>>1940</option>
                    <option value="1930" <?PHP if ((isset($_GET["anneeMin"])) && ($_GET["anneeMin"] == "1930")) echo "selected"; ?>>1930</option>
                </select>
                <select class="form-control input-sm leftSelect" id="anneeMax" name="anneeMax">
                    <option value="" <?PHP if (!isset($_GET["anneeMax"])) echo "selected"; ?>>à</option>
                    <option value="2017" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2017")) echo "selected"; ?>>2017</option>
                    <option value="2016" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2016")) echo "selected"; ?>>2016</option>
                    <option value="2015" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2015")) echo "selected"; ?>>2015</option>
                    <option value="2014" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2014")) echo "selected"; ?>>2014</option>
                    <option value="2013" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2013")) echo "selected"; ?>>2013</option>
                    <option value="2012" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2012")) echo "selected"; ?>>2012</option>
                    <option value="2011" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2011")) echo "selected"; ?>>2011</option>
                    <option value="2010" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2010")) echo "selected"; ?>>2010</option>
                    <option value="2009" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2009")) echo "selected"; ?>>2009</option>
                    <option value="2008" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2008")) echo "selected"; ?>>2008</option>
                    <option value="2007" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2007")) echo "selected"; ?>>2007</option>
                    <option value="2006" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2006")) echo "selected"; ?>>2006</option>
                    <option value="2005" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2005")) echo "selected"; ?>>2005</option>
                    <option value="2004" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2004")) echo "selected"; ?>>2004</option>
                    <option value="2003" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2003")) echo "selected"; ?>>2003</option>
                    <option value="2002" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2002")) echo "selected"; ?>>2002</option>
                    <option value="2001" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2001")) echo "selected"; ?>>2001</option>
                    <option value="2000" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "2000")) echo "selected"; ?>>2000</option>
                    <option value="1999" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1999")) echo "selected"; ?>>1999</option>
                    <option value="1998" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1998")) echo "selected"; ?>>1998</option>
                    <option value="1997" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1997")) echo "selected"; ?>>1997</option>
                    <option value="1996" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1996")) echo "selected"; ?>>1996</option>
                    <option value="1995" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1995")) echo "selected"; ?>>1995</option>
                    <option value="1994" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1994")) echo "selected"; ?>>1994</option>
                    <option value="1993" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1993")) echo "selected"; ?>>1993</option>
                    <option value="1992" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1992")) echo "selected"; ?>>1992</option>
                    <option value="1991" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1991")) echo "selected"; ?>>1991</option>
                    <option value="1990" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1990")) echo "selected"; ?>>1990</option>
                    <option value="1989" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1989")) echo "selected"; ?>>1989</option>
                    <option value="1988" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1988")) echo "selected"; ?>>1988</option>
                    <option value="1987" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1987")) echo "selected"; ?>>1987</option>
                    <option value="1986" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1986")) echo "selected"; ?>>1986</option>
                    <option value="1985" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1985")) echo "selected"; ?>>1985</option>
                    <option value="1984" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1984")) echo "selected"; ?>>1984</option>
                    <option value="1983" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1983")) echo "selected"; ?>>1983</option>
                    <option value="1982" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1982")) echo "selected"; ?>>1982</option>
                    <option value="1981" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1981")) echo "selected"; ?>>1981</option>
                    <option value="1980" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1980")) echo "selected"; ?>>1980</option>
                    <option value="1975" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1975")) echo "selected"; ?>>1975</option>
                    <option value="1970" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1970")) echo "selected"; ?>>1970</option>
                    <option value="1960" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1960")) echo "selected"; ?>>1960</option>
                    <option value="1950" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1950")) echo "selected"; ?>>1950</option>
                    <option value="1940" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1940")) echo "selected"; ?>>1940</option>
                    <option value="1930" <?PHP if ((isset($_GET["anneeMax"])) && ($_GET["anneeMax"] == "1930")) echo "selected"; ?>>1930</option>
                </select>
                <div class="clearfix"></div>


                <label for="kilometres" class="small leftSelect">Kilomètres</label><br />
                <select class="form-control input-sm leftSelect" id="kilometresMin" name="kilometresMin">
                    <option value="" <?PHP if (!isset($_GET["kilometresMin"])) echo "selected"; ?>>De</option>
                    <option value="1000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "1000")) echo "selected"; ?>>1'000</option>
                    <option value="5000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "5000")) echo "selected"; ?>>5'000</option>
                    <option value="7500" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "7500")) echo "selected"; ?>>7'500</option>
                    <option value="10000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "10000")) echo "selected"; ?>>10'000</option>
                    <option value="20000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "20000")) echo "selected"; ?>>20'000</option>
                    <option value="30000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "30000")) echo "selected"; ?>>30'000</option>
                    <option value="40000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "40000")) echo "selected"; ?>>40'000</option>
                    <option value="50000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "50000")) echo "selected"; ?>>50'000</option>
                    <option value="60000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "60000")) echo "selected"; ?>>60'000</option>
                    <option value="70000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "70000")) echo "selected"; ?>>70'000</option>
                    <option value="80000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "80000")) echo "selected"; ?>>80'000</option>
                    <option value="90000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "90000")) echo "selected"; ?>>90'000</option>
                    <option value="100000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "100000")) echo "selected"; ?>>100'000</option>
                    <option value="110000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "110000")) echo "selected"; ?>>110'000</option>
                    <option value="120000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "120000")) echo "selected"; ?>>120'000</option>
                    <option value="130000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "130000")) echo "selected"; ?>>130'000</option>
                    <option value="140000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "140000")) echo "selected"; ?>>140'000</option>
                    <option value="150000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "150000")) echo "selected"; ?>>150'000</option>
                    <option value="160000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "160000")) echo "selected"; ?>>160'000</option>
                    <option value="170000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "170000")) echo "selected"; ?>>170'000</option>
                    <option value="180000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "180000")) echo "selected"; ?>>180'000</option>
                    <option value="190000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "190000")) echo "selected"; ?>>190'000</option>
                    <option value="200000" <?PHP if ((isset($_GET["kilometresMin"])) && ($_GET["kilometresMin"] == "200000")) echo "selected"; ?>>200'000</option>
                </select>
                <select class="form-control input-sm leftSelect" id="kilometresMax" name="kilometresMax">
                    <option value="" <?PHP if (!isset($_GET["kilometresMax"])) echo "selected"; ?>>à</option>
                    <option value="1000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "1000")) echo "selected"; ?>>1'000</option>
                    <option value="5000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "5000")) echo "selected"; ?>>5'000</option>
                    <option value="7500" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "7500")) echo "selected"; ?>>7'500</option>
                    <option value="10000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "10000")) echo "selected"; ?>>10'000</option>
                    <option value="20000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "20000")) echo "selected"; ?>>20'000</option>
                    <option value="30000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "30000")) echo "selected"; ?>>30'000</option>
                    <option value="40000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "40000")) echo "selected"; ?>>40'000</option>
                    <option value="50000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "50000")) echo "selected"; ?>>50'000</option>
                    <option value="60000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "60000")) echo "selected"; ?>>60'000</option>
                    <option value="70000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "70000")) echo "selected"; ?>>70'000</option>
                    <option value="80000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "80000")) echo "selected"; ?>>80'000</option>
                    <option value="90000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "90000")) echo "selected"; ?>>90'000</option>
                    <option value="100000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "100000")) echo "selected"; ?>>100'000</option>
                    <option value="110000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "110000")) echo "selected"; ?>>110'000</option>
                    <option value="120000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "120000")) echo "selected"; ?>>120'000</option>
                    <option value="130000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "130000")) echo "selected"; ?>>130'000</option>
                    <option value="140000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "140000")) echo "selected"; ?>>140'000</option>
                    <option value="150000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "150000")) echo "selected"; ?>>150'000</option>
                    <option value="160000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "160000")) echo "selected"; ?>>160'000</option>
                    <option value="170000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "170000")) echo "selected"; ?>>170'000</option>
                    <option value="180000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "180000")) echo "selected"; ?>>180'000</option>
                    <option value="190000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "190000")) echo "selected"; ?>>190'000</option>
                    <option value="200000" <?PHP if ((isset($_GET["kilometresMax"])) && ($_GET["kilometresMax"] == "200000")) echo "selected"; ?>>200'000</option>
                </select>
                <div class="clearfix"></div>

                <?php if (preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])): ?>
                    <label for="cylindrees" class="small"><?PHP echo $LANG[39]; ?></label>
                    <select class="form-control input-sm" id="cylindreesbis" onchange="document.getElementById('cylindrees').value = document.getElementById('cylindreesbis').value;">
                        <option value="Toutes" <?PHP if (!isset($_GET["cylindrees"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                        <option value="50" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "50")) echo "selected"; ?>>50</option>
                        <option value="80" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "80")) echo "selected"; ?>>80</option>
                        <option value="125" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "125")) echo "selected"; ?>>125</option>
                        <option value="250" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "250")) echo "selected"; ?>>250</option>
                        <option value="500" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "500")) echo "selected"; ?>>500</option>
                        <option value="600" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "600")) echo "selected"; ?>>600</option>
                        <option value="750" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "750")) echo "selected"; ?>>750</option>
                        <option value="1000" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "1000")) echo "selected"; ?>>1000</option>
                        <option value="1100" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "1100")) echo "selected"; ?>>1100</option>
                        <option value="1200" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "1200")) echo "selected"; ?>>1200</option>
                        <option value="1500" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "1500")) echo "selected"; ?>>1500</option>
                        <option value="2000" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "2000")) echo "selected"; ?>>2000</option>
                        <option value="2500" <?PHP if ((isset($_GET["cylindrees"])) && ($_GET["cylindrees"] == "2500")) echo "selected"; ?>>2500</option>
                    </select>
                    <div class="clearfix"></div>
                <?PHP else: ?>
                    <label for="chevaux" class="small leftSelect">Chevaux</label><br />
                    <select class="form-control input-sm leftSelect" id="chevauxMin" name="chevauxMin">
                        <option value="" <?PHP if (!isset($_GET["chevauxMin"])) echo "selected"; ?>>De</option>
                        <option value="40" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "40")) echo "selected"; ?>>40</option>
                        <option value="50" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "50")) echo "selected"; ?>>50</option>
                        <option value="60" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "60")) echo "selected"; ?>>60</option>
                        <option value="70" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "70")) echo "selected"; ?>>70</option>
                        <option value="80" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "80")) echo "selected"; ?>>80</option>
                        <option value="90" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "90")) echo "selected"; ?>>90</option>
                        <option value="100" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "100")) echo "selected"; ?>>100</option>
                        <option value="110" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "110")) echo "selected"; ?>>110</option>
                        <option value="120" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "120")) echo "selected"; ?>>120</option>
                        <option value="130" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "130")) echo "selected"; ?>>130</option>
                        <option value="140" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "140")) echo "selected"; ?>>140</option>
                        <option value="150" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "150")) echo "selected"; ?>>150</option>
                        <option value="160" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "160")) echo "selected"; ?>>160</option>
                        <option value="170" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "170")) echo "selected"; ?>>170</option>
                        <option value="180" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "180")) echo "selected"; ?>>180</option>
                        <option value="190" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "190")) echo "selected"; ?>>190</option>
                        <option value="200" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "200")) echo "selected"; ?>>200</option>
                        <option value="210" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "210")) echo "selected"; ?>>210</option>
                        <option value="220" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "220")) echo "selected"; ?>>220</option>
                        <option value="230" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "230")) echo "selected"; ?>>230</option>
                        <option value="240" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "240")) echo "selected"; ?>>240</option>
                        <option value="250" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "250")) echo "selected"; ?>>250</option>
                        <option value="260" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "260")) echo "selected"; ?>>260</option>
                        <option value="270" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "270")) echo "selected"; ?>>270</option>
                        <option value="280" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "280")) echo "selected"; ?>>280</option>
                        <option value="290" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "290")) echo "selected"; ?>>290</option>
                        <option value="300" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "300")) echo "selected"; ?>>300</option>
                        <option value="320" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "320")) echo "selected"; ?>>320</option>
                        <option value="340" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "340")) echo "selected"; ?>>340</option>
                        <option value="360" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "360")) echo "selected"; ?>>360</option>
                        <option value="380" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "380")) echo "selected"; ?>>380</option>
                        <option value="400" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "400")) echo "selected"; ?>>400</option>
                        <option value="450" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "450")) echo "selected"; ?>>450</option>
                        <option value="500" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "500")) echo "selected"; ?>>500</option>
                        <option value="550" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "550")) echo "selected"; ?>>550</option>
                        <option value="600" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "600")) echo "selected"; ?>>600</option>
                        <option value="650" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "650")) echo "selected"; ?>>650</option>
                        <option value="700" <?PHP if ((isset($_GET["chevauxMin"])) && ($_GET["chevauxMin"] == "700")) echo "selected"; ?>>700</option>
                    </select>
                    <select class="form-control input-sm leftSelect" id="chevauxMax" name="chevauxMax" onchange="this.form.submit();">
                        <option value="" <?PHP if (!isset($_GET["chevauxMax"])) echo "selected"; ?>>à</option>
                        <option value="40" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "40")) echo "selected"; ?>>40</option>
                        <option value="50" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "50")) echo "selected"; ?>>50</option>
                        <option value="60" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "60")) echo "selected"; ?>>60</option>
                        <option value="70" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "70")) echo "selected"; ?>>70</option>
                        <option value="80" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "80")) echo "selected"; ?>>80</option>
                        <option value="90" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "90")) echo "selected"; ?>>90</option>
                        <option value="100" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "100")) echo "selected"; ?>>100</option>
                        <option value="110" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "110")) echo "selected"; ?>>110</option>
                        <option value="120" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "120")) echo "selected"; ?>>120</option>
                        <option value="130" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "130")) echo "selected"; ?>>130</option>
                        <option value="140" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "140")) echo "selected"; ?>>140</option>
                        <option value="150" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "150")) echo "selected"; ?>>150</option>
                        <option value="160" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "160")) echo "selected"; ?>>160</option>
                        <option value="170" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "170")) echo "selected"; ?>>170</option>
                        <option value="180" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "180")) echo "selected"; ?>>180</option>
                        <option value="190" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "190")) echo "selected"; ?>>190</option>
                        <option value="200" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "200")) echo "selected"; ?>>200</option>
                        <option value="210" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "210")) echo "selected"; ?>>210</option>
                        <option value="220" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "220")) echo "selected"; ?>>220</option>
                        <option value="230" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "230")) echo "selected"; ?>>230</option>
                        <option value="240" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "240")) echo "selected"; ?>>240</option>
                        <option value="250" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "250")) echo "selected"; ?>>250</option>
                        <option value="260" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "260")) echo "selected"; ?>>260</option>
                        <option value="270" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "270")) echo "selected"; ?>>270</option>
                        <option value="280" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "280")) echo "selected"; ?>>280</option>
                        <option value="290" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "290")) echo "selected"; ?>>290</option>
                        <option value="300" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "300")) echo "selected"; ?>>300</option>
                        <option value="320" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "320")) echo "selected"; ?>>320</option>
                        <option value="340" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "340")) echo "selected"; ?>>340</option>
                        <option value="360" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "360")) echo "selected"; ?>>360</option>
                        <option value="380" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "380")) echo "selected"; ?>>380</option>
                        <option value="400" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "400")) echo "selected"; ?>>400</option>
                        <option value="450" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "450")) echo "selected"; ?>>450</option>
                        <option value="500" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "500")) echo "selected"; ?>>500</option>
                        <option value="550" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "550")) echo "selected"; ?>>550</option>
                        <option value="600" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "600")) echo "selected"; ?>>600</option>
                        <option value="650" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "650")) echo "selected"; ?>>650</option>
                        <option value="700" <?PHP if ((isset($_GET["chevauxMax"])) && ($_GET["chevauxMax"] == "700")) echo "selected"; ?>>700</option>
                    </select>
                    <div class="clearfix"></div>
                <?php endif; ?>

                <label for="prix" class="small"><?PHP echo $LANG[40]; ?></label>
                <select class="form-control input-sm" id="typevehiculebis" onchange="document.getElementById('typevehicule').value = document.getElementById('typevehiculebis').value;">
                    <option value="Tous" <?PHP if (!isset($_GET["typevehicule"])) echo "selected"; ?>><?PHP echo $LANG[35]; ?></option>
                    <option value="neuf" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "neuf")) echo "selected"; ?>><?PHP echo $LANG[41]; ?></option>
                    <option value="occasion" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "occasion")) echo "selected"; ?>><?PHP echo $LANG[42]; ?></option>
                    <option value="demonstration" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "demonstration")) echo "selected"; ?>><?PHP echo $LANG[43]; ?></option>
                    <option value="collection" <?PHP if ((isset($_GET["typevehicule"])) && ($_GET["typevehicule"] == "collection")) echo "selected"; ?>><?PHP echo $LANG[44]; ?></option>
                </select>

                <label for="transmission" class="small"><?PHP echo $LANG[45]; ?></label>
                <select class="form-control input-sm" id="transmissionbis" onchange="document.getElementById('transmission').value = document.getElementById('transmissionbis').value;">
                    <option value="Toutes" <?PHP if (!isset($_GET["transmission"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                    <option value="manuel" <?PHP if ((isset($_GET["transmission"])) && ($_GET["transmission"] == "manuel")) echo "selected"; ?>><?PHP echo $LANG[46]; ?></option>
                    <option value="automatique" <?PHP if ((isset($_GET["transmission"])) && ($_GET["transmission"] == "automatique")) echo "selected"; ?>><?PHP echo $LANG[47]; ?></option>
                    <option value="sequentielle" <?PHP if ((isset($_GET["transmission"])) && ($_GET["transmission"] == "sequentielle")) echo "selected"; ?>><?PHP echo $LANG[48]; ?></option>
                </select>

                <label for="carburant" class="small"><?PHP echo $LANG[49]; ?></label>
                <select class="form-control input-sm" id="carburantbis" onchange="document.getElementById('carburant').value = document.getElementById('carburantbis').value;">
                    <option value="Tous" <?PHP if (!isset($_GET["carburant"])) echo "selected"; ?>><?PHP echo $LANG[35]; ?></option>
                    <option value="essence" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "essence")) echo "selected"; ?>><?PHP echo $LANG[50]; ?></option>
                    <option value="diesel" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "diesel")) echo "selected"; ?>><?PHP echo $LANG[51]; ?></option>
                    <option value="electrique" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "electrique")) echo "selected"; ?>><?PHP echo $LANG[52]; ?></option>
                    <option value="hybride" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "hybride")) echo "selected"; ?>><?PHP echo $LANG[53]; ?></option>
<?PHP
if (!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
    ?>
                        <option value="gaz" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "gaz")) echo "selected"; ?>><?PHP echo $LANG[54]; ?></option>
                        <option value="bioethanol" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "bioethanol")) echo "selected"; ?>><?PHP echo $LANG[55]; ?></option>
    <?PHP
}
else {
    ?>
                        <option value="mÃ©lange 2 temps" <?PHP if ((isset($_GET["carburant"])) && ($_GET["carburant"] == "mÃ©lange 2 temps")) echo "selected"; ?>><?PHP echo $LANG[56]; ?></option>
    <?PHP
}
?>
                </select>

<?PHP
if (!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
    ?>
                    <label for="carrosserie" class="small"><?PHP echo $LANG[57]; ?></label>
                    <select class="form-control input-sm" id="carrosseriebis" onchange="document.getElementById('carrosserie').value = document.getElementById('carrosseriebis').value;">
                        <option value="Toutes" <?PHP if (!isset($_GET["carrosserie"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                        <option value="berline" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "berline")) echo "selected"; ?>><?PHP echo $LANG[58]; ?></option>
                        <option value="break" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "break")) echo "selected"; ?>><?PHP echo $LANG[59]; ?></option>
                        <option value="cabriolet" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "cabriolet")) echo "selected"; ?>><?PHP echo $LANG[60]; ?></option>
                        <option value="coupe" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "coupe")) echo "selected"; ?>><?PHP echo $LANG[61]; ?></option>
                        <option value="monospace" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "monospace")) echo "selected"; ?>><?PHP echo $LANG[62]; ?></option>
                        <option value="petite voiture" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "petite voiture")) echo "selected"; ?>><?PHP echo $LANG[63]; ?></option>
                        <option value="pick-up" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "pick-up")) echo "selected"; ?>><?PHP echo $LANG[64]; ?></option>
                        <option value="suv/tout-terrain" <?PHP if ((isset($_GET["carrosserie"])) && ($_GET["carrosserie"] == "suv/tout-terrain")) echo "selected"; ?>><?PHP echo $LANG[65]; ?></option>
                    </select>
                        <?PHP
                    }
                    ?>

                <label for="couleurexterne" class="small"><?PHP echo $LANG[66]; ?></label>
                <select class="form-control input-sm" id="couleurexternebis" onchange="document.getElementById('couleurexterne').value = document.getElementById('couleurexternebis').value;">
                    <option value="Toutes" <?PHP if (!isset($_GET["couleurexterne"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                    <option value="anthracite" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "anthracite")) echo "selected"; ?>><?PHP echo $LANG[67]; ?></option>
                    <option value="argent" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "argent")) echo "selected"; ?>><?PHP echo $LANG[68]; ?></option>
                    <option value="beige" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "beige")) echo "selected"; ?>><?PHP echo $LANG[69]; ?></option>
                    <option value="blanc" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "blanc")) echo "selected"; ?>><?PHP echo $LANG[70]; ?></option>
                    <option value="bleu" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "bleu")) echo "selected"; ?>><?PHP echo $LANG[71]; ?></option>
                    <option value="bordeaux" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "bordeaux")) echo "selected"; ?>><?PHP echo $LANG[72]; ?></option>
                    <option value="brun" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "brun")) echo "selected"; ?>><?PHP echo $LANG[73]; ?></option>
                    <option value="gris" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "gris")) echo "selected"; ?>><?PHP echo $LANG[74]; ?></option>
                    <option value="jaune" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "jaune")) echo "selected"; ?>><?PHP echo $LANG[75]; ?></option>
                    <option value="noir" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "noir")) echo "selected"; ?>><?PHP echo $LANG[76]; ?></option>
                    <option value="or" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "or")) echo "selected"; ?>><?PHP echo $LANG[77]; ?></option>
                    <option value="orange" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "orange")) echo "selected"; ?>><?PHP echo $LANG[78]; ?></option>
                    <option value="rose" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "rose")) echo "selected"; ?>><?PHP echo $LANG[79]; ?></option>
                    <option value="rouge" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "rouge")) echo "selected"; ?>><?PHP echo $LANG[80]; ?></option>
                    <option value="turquoise" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "turquoise")) echo "selected"; ?>><?PHP echo $LANG[81]; ?></option>
                    <option value="vert" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "vert")) echo "selected"; ?>><?PHP echo $LANG[82]; ?></option>
                    <option value="violet" <?PHP if ((isset($_GET["couleurexterne"])) && ($_GET["couleurexterne"] == "violet")) echo "selected"; ?>><?PHP echo $LANG[83]; ?></option>
                </select>

                <?PHP
                if (!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
                    ?>
                    <label for="rouesmotricesbis" class="small"><?PHP echo $LANG[84]; ?></label>
                    <select class="form-control input-sm" id="rouesmotricesbis" onchange="document.getElementById('rouesmotrices').value = document.getElementById('rouesmotricesbis').value;">
                        <option value="Toutes" <?PHP if (!isset($_GET["rouesmotrices"])) echo "selected"; ?>><?PHP echo $LANG[33]; ?></option>
                        <option value="4roues" <?PHP if ((isset($_GET["rouesmotrices"])) && ($_GET["rouesmotrices"] == "4roues")) echo "selected"; ?>><?PHP echo $LANG[85]; ?></option>
                        <option value="tractionarriere" <?PHP if ((isset($_GET["rouesmotrices"])) && ($_GET["rouesmotrices"] == "tractionarriere")) echo "selected"; ?>><?PHP echo $LANG[86]; ?></option>
                        <option value="tractionavant" <?PHP if ((isset($_GET["rouesmotrices"])) && ($_GET["rouesmotrices"] == "tractionavant")) echo "selected"; ?>><?PHP echo $LANG[87]; ?></option>
                    </select>
                    <div class="clear"></div>
    <?PHP
}
?>

                <div>
                    <input id="checkexpertbis" type="hidden" value="" name="expertisee" onchange="document.getElementById('checkexpert').value = document.getElementById('checkexpertbis').value;
                        this.form.submit();" <?PHP if ((isset($_GET["expertisee"])) && ($_GET["expertisee"] == "oui")) echo " checked"; ?>>
                    
					<input id="checkphotobis" type="hidden" value="oui" name="photo" onchange="document.getElementById('checkphoto').value = document.getElementById('checkphotobis').value;
                this.form.submit();" <?PHP if ((isset($_GET["photo"])) && ($_GET["photo"] == "oui")) echo " checked"; ?>>
                    
                </div>

                <div class="checkbox checkbox-primary">
                    <button type="button" class="btn btn-primary appliquer" onclick="this.form.submit();"><?PHP echo $LANG[90]; ?></button>
                    <button type="button" class="btn btn-primary effacer" onclick="$('#indexFooter').toggle();" data-toggle="collapse" data-target="#fixedFiltres, #myResult"><?PHP echo $LANG[92]; ?></button>
                    <button type="button" class="btn btn-primary effacer" onclick="resetFormFilter(this.form);
                document.getElementById('q').value = '<?PHP echo $LANG[4]; ?>';"><?PHP echo $LANG[91]; ?></button>

                </div>

                <div class="checkbox checkbox-primary">

                </div>

            </div>
        </div>
        <!--TRIER-->
				<div class="mobileSelect">
            <select id="trier" name="trier" class="form-control trier" onchange="this.form.submit();">
                <option value="" selected=""></option>
                <option value=""><?PHP echo $LANG[18]; ?></option>
                <option value="prixcroissant" <?PHP if ((isset($_GET["trier"])) && ($_GET["trier"] == "prixcroissant")) echo "selected"; ?>><?PHP echo $LANG[19]; ?></option>
                <option value="prixdecroissant" <?PHP if ((isset($_GET["trier"])) && ($_GET["trier"] == "prixdecroissant")) echo "selected"; ?>><?PHP echo $LANG[20]; ?></option>
                <option value="kmcroissant" <?PHP if ((isset($_GET["trier"])) && ($_GET["trier"] == "kmcroissant")) echo "selected"; ?>><?PHP echo $LANG[21]; ?></option>
                <option value="kmdecroissant" <?PHP if ((isset($_GET["trier"])) && ($_GET["trier"] == "kmdecroissant")) echo "selected"; ?>><?PHP echo $LANG[22]; ?></option>
                <option value="anneecroissant" <?PHP if ((isset($_GET["trier"])) && ($_GET["trier"] == "anneecroissant")) echo "selected"; ?>><?PHP echo $LANG[23]; ?></option>
                <option value="anneedecroissant" <?PHP if ((isset($_GET["trier"])) && ($_GET["trier"] == "anneedecroissant")) echo "selected"; ?>><?PHP echo $LANG[24]; ?></option>
            </select>
        </div>
				
				<!-- TYPE VENDEUR-->
				
				<div class="mobileSelect">
            <select id="type_vendeur" name="type_vendeur" class="form-control type_vendeur select-icon" onchange="this.form.submit();">
							<option value="" <?PHP if((!isset($_GET["type_vendeur"])) || ($_GET["type_vendeur"] == "")) echo "selected";?>><?PHP echo "Tous les vendeurs";?></option>
							<option value="0" <?PHP if((isset($_GET["type_vendeur"])) && ($_GET["type_vendeur"] == "0")) echo "selected";?>><?PHP echo "Vendeurs particuliers";?></option>
							<option value="2" <?PHP if((isset($_GET["type_vendeur"])) && ($_GET["type_vendeur"] == "2")) echo "selected";?>><?PHP echo "Vendeurs professionnels";?></option>
							
            </select>
        </div>
				
				<!-- fin type vendeur-->
        </div>
    </div>
    <!--END FIXED MENU MOBILE 18/02/16-->

    <!--FILTRES MOBILE-->


</form>
<!--FILTRES