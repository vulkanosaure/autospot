<?php
require_once('vendor/autoload.php');
include("db_connexion.php");

// On augmente la limite maximale du script, pour les rapports très longs
ini_set('max_execution_time', 60*10); // 10min
ini_set('default_socket_timeout', 60*10);
ini_set('memory_limit','2048M'); // On augmente la mémoire disponible pour un script

use Dompdf\Dompdf;
// use Symfony\Component\Yaml\Yaml; // Pour les traductions dans le futur

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

// Pour les évaluations page 2
function makeNote($max, $count) {
	$double = ($count / $max) * 5 * 10;
	$round = round($double, 0);
	return $round / 10;
}
function makeNoteImg($note) {
	return 'images/eval/eval-'. ($note * 10) .'.png';
}

if (ENVIRONMENT == 'dev') { $rapport_id = $_GET['id']; }

// IF POST ACTION
if ($_SERVER['REQUEST_METHOD'] === 'POST' || ENVIRONMENT == 'dev') {
	if (isset($_POST) || ENVIRONMENT == 'dev') {

		// PROD
		if (ENVIRONMENT == 'prod') {
			if (!isset($_POST['demand_id']) || empty($_POST['demand_id'])){
		    	echo 'Please Enter demand_id';exit;
			}
			if (!isset($_POST['filename']) || empty($_POST['filename'])){
		    	echo 'Please Enter filename';exit;
			}
		}

		/*-- demand exam --*/
		$demand_id = mysqli_real_escape_string($connect1, $_POST['demand_id']);
		$filename = mysqli_real_escape_string($connect1, $_POST['filename']);
		if (ENVIRONMENT == 'dev') {
			$demand_id = $rapport_id;
			$filename = "appbackend/pdf/rapports/rapport".date('Ymd-H-i-s').".pdf";
		}
		$demand = mysqli_query($connect1, "SELECT * FROM demandes_exam WHERE id='$demand_id'");
		$keyValueArr = [];
		$trans_key = mysqli_query($connect1, "SELECT * FROM online_translation2 WHERE 1");
		if(mysqli_num_rows($trans_key) > 0){
			$trans_key_obj = mysqli_fetch_all($trans_key,MYSQLI_ASSOC);
			foreach ($trans_key_obj as $key => $value) {
				$keyValueArr[$value['key']] = $value['value'];
			}
		}

		if (mysqli_num_rows($demand) == 1) {
			$demand_obj = mysqli_fetch_object($demand);
			/*-- client --*/
			$client_query = mysqli_query($connect1, "SELECT * FROM clients WHERE id='$demand_obj->id_client_acheteur'");
			if(mysqli_num_rows($client_query) == 1){
				$client = mysqli_fetch_object($client_query);
			}else {
				echo "Error: Client not Found!!";exit;
			}
			/*-- Seller --*/
			$seller_query = mysqli_query($connect1, "SELECT * FROM clients WHERE id='$demand_obj->id_client_vendeur'");
			if(mysqli_num_rows($seller_query) == 1){
				$seller = mysqli_fetch_object($seller_query);
			}else {
				echo "Error: Seller not Found!!";exit;
			}
			/*-- garage --*/
			$garage_query = mysqli_query($connect1, "SELECT * FROM garages WHERE id='$demand_obj->id_garage'");
			if(mysqli_num_rows($garage_query) == 1){
				$garage = mysqli_fetch_object($garage_query);
			}else {
				echo "Error: Garage not Found!!";exit;
			}
			/*-- annonces --*/
			$annonces_query = mysqli_query($connect1, "SELECT * FROM annonces_clients WHERE id='$demand_obj->id_annonces_clients'");
			if(mysqli_num_rows($annonces_query) == 1){
				$annonces = mysqli_fetch_object($annonces_query);
			}else {
				echo "Error: Annonces not Found!!";exit;
			}
			/*-- demand question --*/
			$data_exam = json_decode($demand_obj->data_exam);
			$report_data = [];
			if(!isset($data_exam) || empty($data_exam)){
				echo "Empty exam report data !!";exit;
			}

			$report_data['user'] = $client;
			$report_data['seller'] = $seller;
			$report_data['garage'] = $garage;
			$annonces->premiereimm = date('d-m-Y', strtotime($annonces->premiereimm));
			$annonces->parution = date('d-m-Y', strtotime($annonces->parution));
			$report_data['annonces'] = $annonces;

			$report_data['id'] = $demand_obj->id;
			$report_data['date'] = date('d/m/Y', strtotime($demand_obj->demande_ts));
			$report_data['time'] = date('H:i', strtotime($demand_obj->demande_ts));
			$report_data['report_type'] = $demand_obj->type_forfait;

			/*-- inspection date --*/
			$inspection = json_decode($demand_obj->json_suivi);
			foreach ($inspection as $key => $inspect) {
				if ($inspect->key == 'exam_complete') {					
					if(isset($inspect->ts) && !empty($inspect->ts)){
						$report_data['inspect_date'] = date('d/m/Y', $inspect->ts);
						$report_data['inspect_time'] = date('H:i', $inspect->ts);
					}
				}
			}
			/*-- Days difference from demande created to today --*/
			$today = date('Y-m-d');
			// $demamd_date = date('Y-m-d', strtotime($demand_obj->demande_ts));
			// $datetime1 = date_create($demamd_date);
			// $datetime2 = date_create($today);
			// $interval = date_diff($datetime1, $datetime2);
			// $report_data['report_days'] = $interval->format('%a');

			foreach ($data_exam as $key => $value) {
				if($value->title == 'examchapter_photos') $exam_data['photos'] = $value->questions;
				if($value->title == 'examchapter_docs') $exam_data['docs'] = $value->questions;
				if($value->title == 'examchapter_entretien') $exam_data['entretien'] = $value->questions;
				if($value->title == 'examchapter_infodiverses') $exam_data['infodiverses'] = $value->questions;
				if($value->title == 'examchapter_light') $exam_data['light'] = $value->questions;
				if($value->title == 'examchapter_inner') $exam_data['inner'] = $value->questions;
				if($value->title == 'examchapter_startengine') $exam_data['startengine'] = $value->questions;
				if($value->title == 'examchapter_essais_routier') $exam_data['essais_routier'] = $value->questions;
				if($value->title == 'examchapter_engine') $exam_data['engine'] = $value->questions;
				if($value->title == 'examchapter_wheel') $exam_data['wheel'] = $value->questions;
				if($value->title == 'examchapter_infra') $exam_data['infra'] = $value->questions;
				if($value->title == 'examchapter_ajustements') $exam_data['ajustements'] = $value->questions;
				if($value->title == 'examchapter_carrosserie') $exam_data['carrosserie'] = $value->questions;
				if($value->title == 'examchapter_vitrage') $exam_data['vitrage'] = $value->questions;
				if($value->title == 'examchapter_parebrise') $exam_data['parebrise'] = $value->questions;
				if($value->title == 'examchapter_sumup_essai') $exam_data['sumup_essai'] = $value->questions;
			}
			/*-- Vehicle Images (Front, Rear, Left & Right) --*/
				foreach ($exam_data['photos'] as $k => $photo) {
					if(isset($photo->items) && !empty($photo->items)){
						foreach ($photo->items as $pic) {
							if(isset($pic->value) && !empty($pic->value)){
								$report_data['photos'][$pic->text] = 'appbackend/img/' . $pic->value[0];
							}
						}
					}
				}

			/*-- DOCUMENTS PRÉSENTÉS LORS DE L’EXAMEN --*/
				$report_data['docs']['user_manual'] = 0;
				$report_data['docs']['license'] = 0;
				$report_data['docs']['plaque'] = 0;
				$report_data['docs']['vin'] = 0;
				foreach ($exam_data['docs'] as $d => $docs) {
					/*-- Notice d’utilisation --*/
					if($docs->text == 'question_docs_0'){ 
						if(isset($docs->items) && !empty($docs->items)){
							foreach ($docs->items as $item) {
								if($item->value && $item->value == 1){
									if ($item->text == 'examglobal_yes') {
										$report_data['docs']['user_manual'] = 1;
										if(isset($item->items) && !empty($item->items)){
											foreach ($item->items as $lang) {
												if($lang->value && $lang->value == 1){
													if($lang->text == 'ec_fr') $manual_lang = 'français';
													if($lang->text == 'ec_de') $manual_lang = 'allemand';
													if($lang->text == 'ec_it') $manual_lang = 'italien';
													if($lang->text == 'examglobal_other') $manual_lang = 'Autre';
													$report_data['docs']['user_manual_lang'] = $manual_lang;
												}
											}
										}									
									}
								}
							}
						}
					}
					/*-- Permis de circulation --*/
					if($docs->text == 'question_docs_1'){ 
						if(isset($docs->items) && !empty($docs->items)){
							foreach ($docs->items as $circulation) {
								if($circulation->value && $circulation->value == 1){
									if ($circulation->text == 'examglobal_yes') {
										$report_data['docs']['license'] = 1;
										if(isset($circulation->items) && !empty($circulation->items)){
											foreach ($circulation->items as $circ_items) {
												if($circ_items->text == 'ec_6B54E'){
													if(isset($circ_items->value) && !empty($circ_items->value)){
														$report_data['docs']['license_pic'] = 'appbackend/img/' . $circ_items->value[0];
													}
												}
											}
										}									
									}
								}
							}
						}
					}
					/*--  Le numéro de plaque d’immatriculation inscrit sur le permis --*/
					if($docs->text == 'question_docs_3'){ 
						if(isset($docs->items) && !empty($docs->items)){
							foreach ($docs->items as $plaque) {
								if(isset($plaque->value) && $plaque->value && $plaque->value == 1){
									if ($plaque->text == 'examglobal_yes') {
										$report_data['docs']['plaque'] = 1;									
									}
								}
							}
						}
					}
					/*--   Le numéro de série de la voiture (VIN) correspond-t-il avec  --*/
					if($docs->text == 'question_docs_4'){ 
						if(isset($docs->items) && !empty($docs->items)){
							foreach ($docs->items as $vin) {
								if(isset($vin->value) && $vin->value && $vin->value == 1){
									if ($vin->text == 'examglobal_yes') {
										$report_data['docs']['vin'] = 1;									
									}
								}
							}
						}
					}

					/*-- PT. Demandez au vendeur si la voiture possède une/des pièces/accessoires qui ne sont pas d’origine et qui nécessite une homologation--*/
					if($docs->text == 'question_docs_2'){ 
						// print_r($docs);
						if(isset($docs->items) && !empty($docs->items)){
							foreach ($docs->items as $originals) {
								if ($originals->text == 'ec_49zx0') {
									if($originals->value && $originals->value == 1){
										$report_data['docs']['originals'] = 1;
									}
								}
								if ($originals->text == 'ec_v5ars') {
									if($originals->value && $originals->value == 1){
										$report_data['docs']['no_non_originals'] = 1;

										// On va chercher les pièces qui manquent
										$array = array();
										foreach ($originals->items as $k_items => $items) {
											if ($items->value == 1) {
												$text = $keyValueArr[$items->text];
												if (isset($items->items)) {
													foreach ($items->items as $k_item => $item) {
														if ($item->value == 1) {
															$text .= " (" . $keyValueArr[$item->text].")";
														}
													}
												}
												$array[] = $text;
											}
										}
										$report_data['docs']['no_non_originals_text'] = $array;

										/*-- Relevant Parts*/
										/*foreach ($originals->items as $parts) {
											if(isset($parts->value) && !empty($parts->value)){
												$report_data['docs'][$parts->text] = $parts->value;
											}else $report_data['docs'][$parts->text] = '-';
										}*/
									}else {
										$report_data['docs']['no_non_originals'] = 0;									
									}
								}
								if ($originals->text == 'ec_69ph6') {
									if($originals->value && $originals->value == 1){
										$report_data['docs']['swiss_certificate'] = 1;
										foreach ($originals->items as $parts) {
											if ($parts->text == 'ec_5mtis') {
												if(isset($parts->value) && !empty($parts->value)){
													$report_data['docs']['swiss_certificate_pic'] = 'appbackend/img/' . $parts->value[0];
												}
											}else {
												if ($parts->value == 1) {
													$report_data['docs']['swiss_certificate_text'][] = $keyValueArr[$parts->text];
												}
											}
										}
									}
								}
								if ($originals->text == 'ec_2qyoe') {
									if($originals->value && $originals->value == 1){
										$report_data['docs']['non_originals_in_dl'] = 1;
										foreach ($originals->items as $parts) {
											if ($parts->text == 'ec_5mtis') {
												if(isset($parts->value) && !empty($parts->value)){
													$report_data['docs']['non_originals_in_dl_pic'] = 'appbackend/img/' . $parts->value[0];
												}
											}else{
												if ($parts->value == 1) {
													$report_data['docs']['non_originals_in_dl_text'][] = $keyValueArr[$parts->text];
												}
											}
										}
									}
								}
							}
						}
					}

					if($docs->text == 'question_docs_1b'){
						if(isset($docs->items) && !empty($docs->items)){
							foreach ($docs->items as $disk_minimal_wear) {
								if(isset($disk_minimal_wear->items) && !empty($disk_minimal_wear->items)){
									foreach ($disk_minimal_wear->items as $key => $value) {
										if(isset($value->value) && $value->value && $value->value == 1){
											$report_data['docs']['disk_minimal_wear'][$disk_minimal_wear->text] = $value->text;
											if (isset($value->items) && !empty($value->items)) {
												foreach ($value->items as $k => $val) {
													if($val->type == 'number' && isset($val->value) && !empty($val->value)){
														$report_data['docs']['disk_minimal_wear_amount'][$disk_minimal_wear->text] = $val->value;
													}
												}
											}
										}									
									}
								}
							}
						}
					}
					// /*-- TYPE DE DISTRIBUTION --*/
					if($docs->text == 'question_entretien_3'){
						if(isset($docs->items) && !empty($docs->items)) {
							foreach ($docs->items as $distribution) {
								if ($distribution->value == 1) {
									if ($distribution->text == 'ec06lhgi') { $report_data['docs']['distributions_type'] = 0; } // Courroie de distribution
									elseif ($distribution->text == 'ec061wm7') { $report_data['docs']['distributions_type'] = 1; } // Chaine de distribution
									elseif ($distribution->text == 'ec01m138') { $report_data['docs']['distributions_type'] = 2; } // Cascade de pignons
								}
							}
						}

						/* if(isset($docs->items) && !empty($docs->items)){
							foreach ($docs->items as $distributions) {
								if(isset($distributions->value) && $distributions->value && $distributions->value == 1){
									if ($distributions->text == 'ec04975g') {
										if(isset($distributions->items) && !empty($distributions->items)){
											foreach ($distributions->items as $s => $distribution) {
												if(isset($distribution->value) && $distribution->value && $distribution->value == 1){
													if($distribution->text == 'ec061wm7'){ // Chaine de distribution
														$report_data['docs']['distributions_type'] = 1;
													}elseif ($distribution->text == 'ec01m138') { // Cascade de pignons
														$report_data['docs']['distributions_type'] = 2;
													}
												}
											}
										}
									}else if($distributions->text == 'ec02idyj'){ // Courroie de distribution
										$report_data['docs']['distributions_type'] = 0;
										if(isset($distributions->items) && !empty($distributions->items)){
											foreach ($distributions->items as $s => $distribution) {
												if(isset($distribution->value) && !empty($distribution->value)){
													$report_data['docs']['distribution'][$distribution->text] = $distribution->value;
												}
											}
										}
									}
								}
							}
						} */
					}
				}

			/*engine start*/
				$report_data['startengine']['main_indicator_lights'] = 0;
				$report_data['startengine']['warning_indicator_lights'] = 0;
				$report_data['startengine']['steering_operation'] = 0;
				foreach ($exam_data['startengine'] as $i => $engine) {
					if($engine->text == 'ec01s0le'){ 
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $indicator_lights) {
								if($indicator_lights->value && $indicator_lights->value == 1){
									if ($indicator_lights->text == 'ec01ty19') {
										$report_data['startengine']['main_indicator_lights'] = 1;								
									}else if($indicator_lights->text == 'ec010pyl'){
										if(isset($indicator_lights->items) && !empty($indicator_lights->items)){
											foreach ($indicator_lights->items as $k => $indicator) {
												if($indicator->value && $indicator->value == 1){
													$report_data['startengine']['main_indicator_blinkings'][$k] = $indicator->text;
												}
											}
										}
									}
								}
							}
						}
					}

					if($engine->text == 'question_startengine_2'){ 
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $warning_indicator) {
								if($warning_indicator->value && $warning_indicator->value == 1){
									if ($warning_indicator->text == 'ec38zr7z') {
										$report_data['startengine']['warning_indicator_lights'] = 1;								
									}else if($warning_indicator->text == 'ec38290x'){
										if(isset($warning_indicator->items) && !empty($warning_indicator->items)){
											foreach ($warning_indicator->items as $k => $warning_light) {
												if($warning_light->value && $warning_light->value !== false){
													if($warning_light->text == 'ec04escq'){
														if(isset($warning_light->value) && !empty($warning_light->value)){
															$report_data['startengine']['warning_indicator_pic'] = 'appbackend/img/' . $warning_light->value[0];
														}
													}else {
														$precision = '';
														if (isset($warning_light->items)) {
															foreach ($warning_light->items as $k_ite => $ite) {
																if ($ite->value == 1) {
																	$precision = ': ' . strtolower($keyValueArr[$ite->text]);
																}
															}
														}
														$report_data['startengine']['warning_indicator_blinkings'][$k] = $keyValueArr[$warning_light->text] . $precision;
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if($engine->text == 'question_startengine_7'){ 
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $steering_operation) {
								if($steering_operation->value && $steering_operation->value == 1){
									if ($steering_operation->text == 'ec04mkre') {
										$report_data['startengine']['steering_operation'] = 1;								
									}else if($steering_operation->text == 'ec04365g'){
										if(isset($steering_operation->items) && !empty($steering_operation->items)){
											foreach ($steering_operation->items as $k => $steering_operation_defect) {
												if(isset($steering_operation_defect->value) && $steering_operation_defect->value && $steering_operation_defect->value == 1){
													$report_data['startengine']['steering_operation_defects'][$k] = $steering_operation_defect->text;
													if(array_key_exists($steering_operation_defect->text, $keyValueArr))
													$report_data['startengine']['steering_operation_defects1'][$k] = $keyValueArr[$steering_operation_defect->text];
												}
											}
										}
									}
								}
							}
						}
					}

					if($engine->text == 'question_startengine_7b'){ 
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $parking_brake) {
								if($parking_brake->value && $parking_brake->value == 1){
									$report_data['startengine']['parking_brake_lever'] = $parking_brake->text;
								}
							}
						}
					}
				}

				if(isset($report_data['startengine']['warning_indicator_blinkings']) && ! empty($report_data['startengine']['warning_indicator_blinkings'])){
					foreach ($report_data['startengine']['warning_indicator_blinkings'] as $k => $warn_blink) {
						if($warn_blink == 'ec38a4sf'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Ampoule défaillante';
						}
						if($warn_blink == 'ec38vv8j'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Niveau d’huile moteur bas';
						}
						if($warn_blink == 'ec38bi8l'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = '--';
						}
						if($warn_blink == 'ec38f1fb'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Plaquette de frein usées';
						}
						if($warn_blink == 'ec38kwux'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Défaillance fermeture centralisée';
						}
						if($warn_blink == 'ec38k255'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Dysfonctionnement des airbags';
						}
						if($warn_blink == 'ec38s0d7'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Dysfonctionnement de l’antiblocage des roues';
						}
						if($warn_blink == 'ec382p4s'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Défaillance de l’ESP';
						}
						if($warn_blink == 'ec38pmj5'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Pression des pneumatiques';
						}
						if($warn_blink == 'ec38mq5z'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Défaillance des freins';
						}
						if($warn_blink == 'ec383h3i'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Témoin moteur';
						}
						if($warn_blink == 'ec380vxh'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = '--';
						}
						if($warn_blink == 'ec38pcs2'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Batterie faible';
						}
						if($warn_blink == 'ec380v81'){
							$report_data['startengine']['warning_indicator_blinkings'][$k] = 'Température anormale du liquide de refroidissement';
						}						
					}
				}
				if(isset($report_data['startengine']['main_indicator_blinkings']) && ! empty($report_data['startengine']['main_indicator_blinkings'])){
					foreach ($report_data['startengine']['main_indicator_blinkings'] as $k => $blinking) {
						if($blinking == 'ec01ja32'){
							$report_data['startengine']['main_indicator_blinkings'][$k] = 'Moteur';
						}
						if($blinking == 'ec01vmt6'){
							$report_data['startengine']['main_indicator_blinkings'][$k] = 'Batterie';
						}
						if($blinking == 'ec01of9u'){
							$report_data['startengine']['main_indicator_blinkings'][$k] = 'ABS';
						}
						if($blinking == 'ec01g5g8'){
							$report_data['startengine']['main_indicator_blinkings'][$k] = 'Airbag';
						}
						if($blinking == 'ec01mu7b'){
							$report_data['startengine']['main_indicator_blinkings'][$k] = 'ESP';
						}
						if($blinking == 'ec01bcgp'){
							$report_data['startengine']['main_indicator_blinkings'][$k] = 'Pression d’huile';
						}
						if($blinking == 'ec01c0og'){
							$report_data['startengine']['main_indicator_blinkings'][$k] = 'Anti-démarrage';
						}						
					}
				}
			
			/*infodiverses */
				$report_data['infodiverses']['double_keys'] = 0;
				$report_data['infodiverses']['nb_de_props'] = 0;
				$report_data['infodiverses']['fuel_consumption'] = 0;
				$report_data['infodiverses']['sheltered_parking'] = 0;
				foreach ($exam_data['infodiverses'] as $i => $infodiverses) {
					/*--  Double des clés --*/
					if($infodiverses->text == 'question_infodiverses_4'){ 
						if(isset($infodiverses->items) && !empty($infodiverses->items)){
							foreach ($infodiverses->items as $double_keys) {
								if($double_keys->value && $double_keys->value == 1){
									if ($double_keys->text == 'examglobal_yes') {
										$report_data['infodiverses']['double_keys'] = 1;									
									}
								}
							}
						}
					}
					/*-- Nb de Propertier --*/
					if($infodiverses->text == 'question_infodiverses_0'){ 
						if(isset($infodiverses->items) && !empty($infodiverses->items)){
							foreach ($infodiverses->items as $nbDeProp) {
								if($nbDeProp->value && $nbDeProp->value == 1){
									if ($nbDeProp->text == 'examglobal_yes') {
										$report_data['infodiverses']['nb_de_props'] = 1;
										if(isset($nbDeProp->items) && !empty($nbDeProp->items)){
											foreach ($nbDeProp->items as $propCount) {
												if($propCount->value && $propCount->value == 1){
													$report_data['infodiverses']['nb_de_props_count'] = $propCount->text;
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- Current Car usage --*/
					if($infodiverses->text == 'question_infodiverses_1'){ 
						if(isset($infodiverses->items) && !empty($infodiverses->items)){
							foreach ($infodiverses->items as $carUsage) {
								if($carUsage->value && $carUsage->value == 1){
									if ($carUsage->text == 'ec03nwsh') {
										$report_data['infodiverses']['car_usage'] = 'ponctuelle';
										if(isset($carUsage->items) && !empty($carUsage->items)){
											foreach ($carUsage->items as $usage) {
												if($usage->value && $usage->value == 1){
													$usageRegion = $usage->text;
												}
											}
										}
									}
									elseif ($carUsage->text == 'ec03h23t') {
										$report_data['infodiverses']['car_usage'] = 'regular';
										if(isset($carUsage->items) && !empty($carUsage->items)){
											foreach ($carUsage->items as $usage) {
												if($usage->value && $usage->value == 1){
													$usageRegion = $usage->text;
												}
											}
										}
									}
								}
							}
							if($usageRegion == 'ec03w0lw') $region = 'urbain';
							if($usageRegion == 'ec03rd69') $region = 'extra-urbain';
							if($usageRegion == 'ec03fri5') $region = 'mixte';
							$usagetype = $report_data['infodiverses']['car_usage'];
							if($usagetype == 'regular') $usagetype = 'régulière';
							$report_data['infodiverses']['car_usage_text'] = $usagetype . ' en milieu ' . $region;
						}
					}
					/*-- Actual fuel consumption --*/
					if($infodiverses->text == 'question_infodiverses_2'){ 
						if(isset($infodiverses->items) && !empty($infodiverses->items)){
							foreach ($infodiverses->items as $fuel_consumption) {
								if($fuel_consumption->value && $fuel_consumption->value == 1){
									if ($fuel_consumption->text == 'examglobal_yes') {
										$report_data['infodiverses']['fuel_consumption'] = 1;
										if(isset($fuel_consumption->items) && !empty($fuel_consumption->items)){
											foreach ($fuel_consumption->items as $consumption) {
												if ($consumption->text == 'ec03nj1n') {
													$report_data['infodiverses']['fuel_consumption_value'] = $consumption->value;
												}
											}
										}									
									}
								}
							}
						}
					}
					/*--  Parking under shelter place --*/
					if($infodiverses->text == 'question_infodiverses_3'){ 
						if(isset($infodiverses->items) && !empty($infodiverses->items)){
							foreach ($infodiverses->items as $sheltered_parking) {
								if($sheltered_parking->value && $sheltered_parking->value == 1){
									if ($sheltered_parking->text == 'examglobal_yes') {
										$report_data['infodiverses']['sheltered_parking'] = 1;
									}
								}
							}
						}
					}
					/*--  Spare Wheel / puncture kit --*/
					if($infodiverses->text == 'question_coffre_8'){ 
						if(isset($infodiverses->items) && !empty($infodiverses->items)){
							foreach ($infodiverses->items as $spare_wheel) {
								if(isset($spare_wheel->value) && $spare_wheel->value && $spare_wheel->value == 1){
									$report_data['infodiverses']['spare_wheel'] = $spare_wheel->text;								
								}
							}
						}
					}
				}

			/*-- Kilométrage --*/
				$report_data['inner']['driver_seats_damage'] = 1;
				$report_data['inner']['side_mirrors_damage'] = 1;
				$report_data['inner']['central_mirrors_damage'] = 1;
				$report_data['inner']['sprinklers_damage'] = 1;
				$report_data['inner']['sprinkler_funcs_damage'] = 1;
				$report_data['inner']['driver_seat_belts_damage'] = 1;
				$report_data['inner']['passanger_seat_belts_damage'] = 1;
				$report_data['inner']['rear_seat_belts_damage']['ec209xek'] = 1;
				$report_data['inner']['rear_seat_belts_damage']['ec20bhue'] = 1;
				$report_data['inner']['rear_seat_belts_damage']['ec20j0nq'] = 1;
				$report_data['inner']['elec_diagnostics_damage'] = 1;
				foreach ($exam_data['inner'] as $i => $inner) {
					if($inner->text == 'examchapter_inner_5'){ 
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $mileage) {
								if ($mileage->text == 'ec05qigl') {
									$report_data['inner']['mileage_displayed'] = $mileage->value;
								}
								if ($mileage->text == 'ec05sere') {
									$report_data['inner']['mileage_seller_inserted'] = $mileage->value;
								}
								if ($mileage->text == 'ec05zfmc') {
									$report_data['inner']['mileage_diff'] = $mileage->value;
								}
								if ($mileage->text == 'ec0527zk') {
									if(isset($mileage->value) && !empty($mileage->value)){
										$report_data['inner']['mileage_pic'] = 'appbackend/img/' . $mileage->value[0];
									}
								}
							}
						}
					}
					/*-- Par rapport à l’usure du pédalier et de la sellerie conducteur, estimez-vous que le kilométrage du véhicule semble cohérent  --*/
					if($inner->text == 'examchapter_inner_6'){ 
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $miles_change) {
								if($miles_change->value && $miles_change->value == 1){
									if ($miles_change->text == 'examglobal_yes') {
										$report_data['inner']['miles_change'] = 1;
										if ($miles_change->text == 'ec07x62m') {
											$report_data['inner']['changed_mileage'] = $miles_change->value;
										}
									}elseif ($miles_change->text == 'examglobal_no') {
										$report_data['inner']['miles_change'] = 0;
										$report_data['inner']['changed_mileage'] = $miles_change->value;
									}
								}
							}
						}
					}
					/*-- PT. Le compteur kilométrique affiche-t-il une valeur supérieure à celle indiquée sur l’étiquette/autocollant de la dernière vidange ou du dernier service ?  --*/
					if($inner->text == 'examchapter_inner_7'){ 
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $mile_change) {
								if($mile_change->value && $mile_change->value == 1 && $mile_change->text == 'ec051wjg'){
								// print_r($mile_change);echo "next loop\n";
									$report_data['inner']['service_mileage'] = 2;									
								}elseif ($mile_change->value && $mile_change->value == 1 && $mile_change->text == 'examglobal_yes') {
									$report_data['inner']['service_mileage'] = 1;
									if ($mile_change->text == 'ec07x62m') {
										$report_data['inner']['service_changed_mileage'] = $mile_change->value;
									}
								}elseif ($mile_change->value && $mile_change->value == 1 && $mile_change->text == 'examglobal_no') {
									$report_data['inner']['service_mileage'] = 0;
								}
							}
						}
					}
					if($inner->text == 'examchapter_inner_8'){
						// echo "<pre>"; print_r($inner); echo "</pre>";
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $driver_seats) {
								if($driver_seats->value == 1){
									if ($driver_seats->text == 'ec04mkre') { /*-- no driver_seats damage --*/
										$report_data['inner']['driver_seats_damage'] = 0;
									}else if($driver_seats->text == 'ec04365g'){ /*-- one or more damage --*/
										// echo "<pre>"; print_r($driver_seats); echo "</pre>";
										if(isset($driver_seats->items) && !empty($driver_seats->items)){
											foreach ($driver_seats->items as $s => $driver_seat) {
												// echo "<pre>"; print_r($driver_seat); echo "</pre>";
												// if(isset($driver_seat->value) && $driver_seat->value && $driver_seat->value == 1){
													if(isset($driver_seat->items) && !empty($driver_seat->items)){
														foreach ($driver_seat->items as $key => $lf_driver_seat) {
															if(isset($lf_driver_seat->value) && $lf_driver_seat->value && $lf_driver_seat->value == 1){
																if(isset($lf_driver_seat->items) && !empty($lf_driver_seat->items)){
																	foreach ($lf_driver_seat->items as $k => $val) {
																		if(isset($val->value) && $val->value && !empty($val->value)){
																			$report_data['inner']['drivers_seat'][$keyValueArr[$driver_seat->text]][$lf_driver_seat->text] = $val->text;
																			if(array_key_exists($lf_driver_seat->text, $keyValueArr) && array_key_exists($val->text, $keyValueArr)){
																				$report_data['inner']['drivers_seat1'][$keyValueArr[$driver_seat->text]][$keyValueArr[$lf_driver_seat->text]] = $keyValueArr[$val->text];
																			}
																		}
																	}
																}else{
																	$report_data['inner']['drivers_seat'][$driver_seat->text][$lf_driver_seat->text] = 'true';
																	if(array_key_exists($lf_driver_seat->text, $keyValueArr)){
																		$report_data['inner']['drivers_seat1'][$keyValueArr[$driver_seat->text]][$keyValueArr[$lf_driver_seat->text]] = 'true';
																	}
																}
															}
														}
													}
												// }
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'examchapter_inner_12'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $side_mirrors) {
								if(isset($side_mirrors->value) && $side_mirrors->value && $side_mirrors->value == 1){
									if ($side_mirrors->text == 'ec04mkre') { /*-- no side_mirrors damage --*/
										$report_data['inner']['side_mirrors_damage'] = 0;
									}else if($side_mirrors->text == 'ec04365g'){ /*-- one or more damage --*/
										if(isset($side_mirrors->items) && !empty($side_mirrors->items)){
											foreach ($side_mirrors->items as $s => $side_mirror) {
												if(isset($side_mirror->value) && $side_mirror->value && $side_mirror->value == 1){
													if(isset($side_mirror->items) && !empty($side_mirror->items)){
														foreach ($side_mirror->items as $key => $side_lf_mirror) {
															if(isset($side_lf_mirror->value) && $side_lf_mirror->value && $side_lf_mirror->value == 1){
																if(isset($side_lf_mirror->items) && !empty($side_lf_mirror->items)){
																	foreach ($side_lf_mirror->items as $i => $val) {
																		if(isset($val->value) && $val->value && !empty($val->value)){
																			$report_data['inner']['side_mirror'][$side_mirror->text][$side_lf_mirror->text] = $val->text;
																			if(array_key_exists($side_mirror->text, $keyValueArr) && array_key_exists($side_lf_mirror->text, $keyValueArr) && array_key_exists($val->text, $keyValueArr)){
																				$report_data['inner']['side_mirror1'][$keyValueArr[$side_mirror->text]][$keyValueArr[$side_lf_mirror->text]] = $keyValueArr[$val->text];
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'examchapter_inner_16'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $central_mirrors) {
								if(isset($central_mirrors->value) && $central_mirrors->value && $central_mirrors->value == 1){
									if ($central_mirrors->text == 'ec058oaw') { /*-- no central_mirrors damage --*/
										$report_data['inner']['central_mirrors_damage'] = 0;
									}else if($central_mirrors->text == 'ec05dvli'){ /*-- one or more damage --*/
										if(isset($central_mirrors->items) && !empty($central_mirrors->items)){
											foreach ($central_mirrors->items as $s => $central_mirror) {
												if(isset($central_mirror->value) && $central_mirror->value && $central_mirror->value == 1){
													if(isset($central_mirror->items) && !empty($central_mirror->items)){
														foreach ($central_mirror->items as $key => $central_lf_mirror) {
															if(isset($central_lf_mirror->value) && $central_lf_mirror->value && $central_lf_mirror->value == 1){
																$report_data['inner']['central_mirror'][$central_mirror->text][] =$central_lf_mirror->text;
																if(array_key_exists($central_mirror->text, $keyValueArr) && array_key_exists($central_lf_mirror->text, $keyValueArr)){
																	$report_data['inner']['central_mirror1'][$keyValueArr[$central_mirror->text]][] = $keyValueArr[$central_lf_mirror->text];
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'examchapter_inner_15'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $sprinkler_funcs) {
								if(isset($sprinkler_funcs->value) && $sprinkler_funcs->value && $sprinkler_funcs->value == 1){
									if ($sprinkler_funcs->text == 'ec04mkre') { /*-- no sprinkler_funcs damage --*/
										$report_data['inner']['sprinkler_funcs_damage'] = 0;
									}else if($sprinkler_funcs->text == 'ec04365g'){ /*-- one or more damage --*/
										if(isset($sprinkler_funcs->items) && !empty($sprinkler_funcs->items)){
											foreach ($sprinkler_funcs->items as $s => $sprinkler_func) {
												if(isset($sprinkler_func->value) && $sprinkler_func->value && $sprinkler_func->value == 1){
													if(isset($sprinkler_func->items) && !empty($sprinkler_func->items)){
														foreach ($sprinkler_func->items as $key => $lf_sprinkler_func) {
															if(isset($lf_sprinkler_func->value) && $lf_sprinkler_func->value && $lf_sprinkler_func->value == 1){
																$report_data['inner']['sprinkler_func'][$sprinkler_func->text][$lf_sprinkler_func->text] = 'true';
																if(array_key_exists($sprinkler_func->text, $keyValueArr) && array_key_exists($lf_sprinkler_func->text, $keyValueArr)){
																	$report_data['inner']['sprinkler_func1'][$sprinkler_func->text][$keyValueArr[$lf_sprinkler_func->text]] = 'true';
																}
																if(isset($lf_sprinkler_func->items) && !empty($lf_sprinkler_func->items)){
																	foreach ($lf_sprinkler_func->items as $i => $val) {
																		if(isset($val->value) && $val->value && $val->value == 1){
																			$report_data['inner']['sprinkler_func'][$sprinkler_func->text][$lf_sprinkler_func->text] = $val->text;
																			$report_data['inner']['sprinkler_func1'][$sprinkler_func->text][$keyValueArr[$lf_sprinkler_func->text]] = $keyValueArr[$val->text];
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'examchapter_inner_15b'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $sprinklers) {
								if(isset($sprinklers->value) && $sprinklers->value && $sprinklers->value == 1){
									if ($sprinklers->text == 'ec04mkre') { /*-- no sprinklers damage --*/
										$report_data['inner']['sprinklers_damage'] = 0;
									}else if($sprinklers->text == 'ec04365g'){ /*-- one or more damage --*/
										if(isset($sprinklers->items) && !empty($sprinklers->items)){
											foreach ($sprinklers->items as $s => $sprinkler) {
												// if(isset($sprinkler->value) && $sprinkler->value && $sprinkler->value == 1){
													if(isset($sprinkler->items) && !empty($sprinkler->items)){
														foreach ($sprinkler->items as $key => $lf_sprinkler) {
															// if(isset($lf_sprinkler->value) && $lf_sprinkler->value && $lf_sprinkler->value == 1){
															if ($sprinkler->text != "ec041k9d") { // On s'occupe du gicleur arrière juste en dessous
																// $report_data['inner']['sprinkler'][$sprinkler->text][$lf_sprinkler->text] = 'true';
																// if(array_key_exists($sprinkler->text, $keyValueArr) && array_key_exists($lf_sprinkler->text, $keyValueArr)){
																// 	$report_data['inner']['sprinkler1'][$sprinkler->text][$keyValueArr[$lf_sprinkler->text]] = 'true';
																// }
																if(isset($lf_sprinkler->items) && !empty($lf_sprinkler->items)){
																	$ii = 0;
																	foreach ($lf_sprinkler->items as $i => $val) {
																		if(isset($val->value) && $val->value && $val->value == 1){
																			$report_data['inner']['sprinkler'][$sprinkler->text][$lf_sprinkler->text][$ii] = $val->text;
																			$report_data['inner']['sprinkler1'][$sprinkler->text][$keyValueArr[$lf_sprinkler->text]][$ii] = $keyValueArr[$val->text];
																			if(isset($val->items) && !empty($val->items)){
																				foreach ($val->items as $subitem) {
																					if(isset($subitem->value) && $subitem->value && $subitem->value == 1){
																						$report_data['inner']['sprinkler'][$sprinkler->text][$lf_sprinkler->text][$ii] = $val->text .", ". $subitem->text;
																						$report_data['inner']['sprinkler1'][$sprinkler->text][$keyValueArr[$lf_sprinkler->text]][$ii] = $keyValueArr[$val->text] .", ". $keyValueArr[$subitem->text];
																					}
																				}
																			}
																		}
																		$ii++;
																	}
																}
															// }
															}else {
																// Gicleur arrière
																foreach ($sprinkler->items as $key => $item) {
																	if ($item->value == 1) {
																		if ($item->text == "ec05vn1d") {
																			$report_data['inner']['sprinkler'][$sprinkler->text]['ec041k9d'] = $item->text;
																			$report_data['inner']['sprinkler1'][$sprinkler->text][$keyValueArr['ec041k9d']] = $keyValueArr[$item->text];
																		}
																		if ($item->text == "ec050mb5") {
																			foreach ($item->items as $k => $sousitems) {
																				if ($sousitems->value == 1) {
																					$report_data['inner']['sprinkler'][$sprinkler->text]['ec041k9d'] = $item->text.$sousitems->text;
																					$report_data['inner']['sprinkler1'][$sprinkler->text][$keyValueArr['ec041k9d']] = $keyValueArr[$item->text].', '.$keyValueArr[$sousitems->text];
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												// }
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'examchapter_inner_17'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $driver_seat_belts) {
								if(isset($driver_seat_belts->value) && $driver_seat_belts->value && $driver_seat_belts->value == 1){
									if ($driver_seat_belts->text == 'ec058oaw') { /*-- no driver_seat_belts damage --*/
										$report_data['inner']['driver_seat_belts_damage'] = 0;
									}else if($driver_seat_belts->text == 'ec05r6hu'){ /*-- one or more damage --*/
										if(isset($driver_seat_belts->items) && !empty($driver_seat_belts->items)){
											foreach ($driver_seat_belts->items as $s => $driver_seat_belt) {
												// echo "<pre>"; print_r($driver_seat_belt); echo "</pre>";
												if(isset($driver_seat_belt->value) && $driver_seat_belt->value && $driver_seat_belt->value == 1){
													$pb = $keyValueArr[$driver_seat_belt->text];
													if(isset($driver_seat_belt->items) && !empty($driver_seat_belt->items)){
														foreach ($driver_seat_belt->items as $key => $lf_driver_seat_belt) {
															if($lf_driver_seat_belt->type == 'picture' && isset($lf_driver_seat_belt->value) && is_array($lf_driver_seat_belt->value) && !empty($lf_driver_seat_belt->value)){
																$report_data['inner']['driver_seat_belt1'][$keyValueArr[$driver_seat_belt->text]] = $keyValueArr[$lf_driver_seat_belt->text];
																$report_data['inner']['seat_belt_pics'][] = 'appbackend/img/' . $lf_driver_seat_belt->value[0];
															}elseif(isset($lf_driver_seat_belt->value) && $lf_driver_seat_belt->value && $lf_driver_seat_belt->value == 1){
																$report_data['inner']['driver_seat_belt'][$driver_seat_belt->text] = $lf_driver_seat_belt->text;
																if(array_key_exists($driver_seat_belt->text, $keyValueArr) && array_key_exists($lf_driver_seat_belt->text, $keyValueArr)){
																	$report_data['inner']['driver_seat_belt1'][$keyValueArr[$driver_seat_belt->text]] = $keyValueArr[$lf_driver_seat_belt->text];
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'question_hab_pass_2'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $passanger_seat_belts) {
								if(isset($passanger_seat_belts->value) && $passanger_seat_belts->value && $passanger_seat_belts->value == 1){
									if ($passanger_seat_belts->text == 'ec058oaw') { /*-- no passanger_seat_belts damage --*/
										$report_data['inner']['passanger_seat_belts_damage'] = 0;
									}else if($passanger_seat_belts->text == 'ec05r6hu'){ /*-- one or more damage --*/
										if(isset($passanger_seat_belts->items) && !empty($passanger_seat_belts->items)){
											foreach ($passanger_seat_belts->items as $s => $passanger_seat_belt) {
												if(isset($passanger_seat_belt->value) && $passanger_seat_belt->value && $passanger_seat_belt->value == 1){
													$choix1 = $keyValueArr[$passanger_seat_belt->text];
													if(isset($passanger_seat_belt->items) && !empty($passanger_seat_belt->items)){
														foreach ($passanger_seat_belt->items as $key => $lf_passanger_seat_belt) {
															if($lf_passanger_seat_belt->type == 'picture' && isset($lf_passanger_seat_belt->value) && is_array($lf_passanger_seat_belt->value) && !empty($lf_passanger_seat_belt->value)){
																$report_data['inner']['seat_belt_pics'][] = 'appbackend/img/' . $lf_passanger_seat_belt->value[0];
															}elseif(isset($lf_passanger_seat_belt->value) && $lf_passanger_seat_belt->value && $lf_passanger_seat_belt->value == 1){
																// $report_data['inner']['passanger_seat_belt'][$passanger_seat_belt->text] = $lf_passanger_seat_belt->text;
																// if(array_key_exists($passanger_seat_belt->text, $keyValueArr) && array_key_exists($lf_passanger_seat_belt->text, $keyValueArr)){
																// 	$report_data['inner']['passanger_seat_belt1'][$keyValueArr[$passanger_seat_belt->text]] = $keyValueArr[$lf_passanger_seat_belt->text];
																// }
																$choix2 = $keyValueArr[$lf_passanger_seat_belt->text];
															}
														}
													}
													//victor
													if ($choix2) { $report_data['inner']['passanger_seat_belt1'][$choix1] = $choix2; }
													else { $report_data['inner']['passanger_seat_belt1'][' '] = $choix1; }
												}
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'question_hab_back_2b'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $seats) {
								foreach ($seats->items as $rear_seat_belts) {
									if(isset($rear_seat_belts->value) && $rear_seat_belts->value && $rear_seat_belts->value == 1){
										if ($rear_seat_belts->text == 'ec058oaw') { /*-- no rear_seat_belts damage --*/
											$report_data['inner']['rear_seat_belts_damage'][$seats->text] = 0;
										}elseif ($rear_seat_belts->text == 'ec04975g') { /*-- NA --*/
											$report_data['inner']['rear_seat_belts_damage'][$seats->text] = 2;
										}else if($rear_seat_belts->text == 'ec05r6hu'){ /*-- one or more damage --*/
											if(isset($rear_seat_belts->items) && !empty($rear_seat_belts->items)){
												foreach ($rear_seat_belts->items as $s => $rear_seat_belt) {
													if(isset($rear_seat_belt->value) && $rear_seat_belt->value && $rear_seat_belt->value == 1){
														if(isset($rear_seat_belt->items) && !empty($rear_seat_belt->items)){
															foreach ($rear_seat_belt->items as $key => $lf_rear_seat_belt) {
																if($lf_rear_seat_belt->type == 'picture' && isset($lf_rear_seat_belt->value) && is_array($lf_rear_seat_belt->value) && !empty($lf_rear_seat_belt->value)){
																	
																	$report_data['inner']['rear_seat_belt'][$seats->text][' '] = $rear_seat_belt->text;
																	if(array_key_exists($rear_seat_belt->text, $keyValueArr) && array_key_exists($lf_rear_seat_belt->text, $keyValueArr)){
																		$report_data['inner']['rear_seat_belt1'][$seats->text][' '] = $keyValueArr[$rear_seat_belt->text];
																	}

																	$report_data['inner']['seat_belt_pics'][] = 'appbackend/img/' . $lf_rear_seat_belt->value[0];
																}elseif(isset($lf_rear_seat_belt->value) && $lf_rear_seat_belt->value && $lf_rear_seat_belt->value == 1){
																	$report_data['inner']['rear_seat_belt'][$seats->text][$rear_seat_belt->text] = $lf_rear_seat_belt->text;
																	if(array_key_exists($rear_seat_belt->text, $keyValueArr) && array_key_exists($lf_rear_seat_belt->text, $keyValueArr)){
																		$report_data['inner']['rear_seat_belt1'][$seats->text][$keyValueArr[$rear_seat_belt->text]] = $keyValueArr[$lf_rear_seat_belt->text];
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if($inner->text == 'question_engine_1'){
						if(isset($inner->items) && !empty($inner->items)){
							foreach ($inner->items as $elec_diagnostics) {
								if(isset($elec_diagnostics->value) && $elec_diagnostics->value && $elec_diagnostics->value == 1){
									if ($elec_diagnostics->text == 'ec06qn1h') { /*-- no elec_diagnostics damage --*/
										$report_data['inner']['elec_diagnostics_damage'] = 0;
									}else if($elec_diagnostics->text == 'ec06qnm6'){ /*-- one or more damage --*/
										if(isset($elec_diagnostics->items) && !empty($elec_diagnostics->items)){
											foreach ($elec_diagnostics->items as $s => $elec_diagnostic) {
												if($elec_diagnostic->type == 'picture' && isset($elec_diagnostic->value) && is_array($elec_diagnostic->value) && !empty($elec_diagnostic->value)){
													foreach ($elec_diagnostic->value as $pics) {
														$report_data['inner']['elec_diagnostic_pics'][] = 'appbackend/img/' . $pics;
													}
												// }elseif(isset($elec_diagnostic->value) && $elec_diagnostic->value && $elec_diagnostic->value == 1){
												}else{
													if(isset($elec_diagnostic->items) && !empty($elec_diagnostic->items)){
														foreach ($elec_diagnostic->items as $key => $lf_elec_diagnostic) {
															if($lf_elec_diagnostic->type == 'number' && isset($lf_elec_diagnostic->value) && !empty($lf_elec_diagnostic->value)){
																$report_data['inner']['elec_diagnostic'][$elec_diagnostic->text] = $lf_elec_diagnostic->value;
																if(array_key_exists($elec_diagnostic->text, $keyValueArr)){
																	$report_data['inner']['elec_diagnostic1'][$keyValueArr[$elec_diagnostic->text]] = $lf_elec_diagnostic->value;
																}
															}
														}
													}
												}
											}
										}
									}elseif ($elec_diagnostics->text == 'ec06oxtd') {
										$report_data['inner']['elec_diagnostics_damage'] = 2;									
									}
								}
							}
						}
					}
				}

			/*-- ENTRETIEN DU VÉHICULE --*/
				$report_data['entretien']['carnet'] = 0;
				$report_data['entretien']['service_bill'] = 0;
				$report_data['entretien']['claim_bill'] = 0;
				$report_data['entretien']['last_expertise_sheet'] = 0;
				foreach ($exam_data['entretien'] as $e => $entretien) {
					/*-- CARNET & SERVICES D’ENTRETIEN  --*/
					if($entretien->text == 'question_entretien_0'){ 
						if(isset($entretien->items) && !empty($entretien->items)){
							foreach ($entretien->items as $carnet) {
								if($carnet->value && $carnet->value == 1){
									if ($carnet->text == 'examglobal_yes') {
										$report_data['entretien']['carnet'] = 1;
										if(isset($carnet->items) && !empty($carnet->items)){
											foreach ($carnet->items as $carnettype) {
												if($carnettype->value && $carnettype->value == 1){
													if ($carnettype->text == 'ec02esus') {
														$report_data['entretien']['carnettype'] = 'standard';
													}else if ($carnettype->text == 'ec02mvmb') {
														$report_data['entretien']['carnettype'] = 'electronic';
													}
												}
											}
										}

									}
								}
							}
						}
					}
					/*-- CARNET & SERVICES D’ENTRETIEN  --*/
					if($entretien->text == 'question_entretien_1'){ 
						if(isset($entretien->items) && !empty($entretien->items)){
							foreach ($entretien->items as $maintenance) {
								if ($report_data['entretien']['carnet'] == 1) {
									if($maintenance->value && $maintenance->value == 1){
										if ($maintenance->text == 'ec_vdo3h') {
											$report_data['entretien']['maintenance'] = 'all';
										} else if ($maintenance->text == 'ec02zpd0') {
											$report_data['entretien']['maintenance'] = 'empty';
										} else if ($maintenance->text == 'ec_34b3f') {
											$report_data['entretien']['maintenance'] = 'missed';
											foreach ($maintenance->items as $missed) {
												if($missed->text == 'ec026la5'){
													foreach ($missed->items as $missedCounts) {
														if($missedCounts->value && $missedCounts->value == 1){
															$report_data['entretien']['missed_maintenance'] = $missedCounts->text;
														}
													}
												}
											}
										}
									}
									/*-- Standard booklet image --*/
									if ($maintenance->text == 'ec02yzy3') {
										if(isset($maintenance->value) && !empty($maintenance->value)){
											foreach ($maintenance->value as $k_image => $image) {
												$report_data['entretien']['std_booklet_pic'][] = 'appbackend/img/' . $image;
											}
										}
									}
								}
							}
						}
					}

					if($entretien->text == '*question_entretien_1b'){ 
						if(isset($entretien->items) && !empty($entretien->items)){
							foreach ($entretien->items as $lastService) {
								if ($lastService->text == 'ec023fye') {
									if(isset($lastService->value) && !empty($lastService->value)){
										$report_data['entretien']['last_service_date'] = $lastService->value;
									}
								}
								if ($lastService->text == 'ec02816d') {
									if(isset($lastService->value) && !empty($lastService->value)){
										$report_data['entretien']['last_service_mileage'] = $lastService->value;
									}
								}
								/*-- Electronic booklet image --*/
								if ($lastService->text == 'ec02wbn4') {
									if(isset($lastService->value) && !empty($lastService->value)){
										foreach ($lastService->value as $k_image => $image) {
											$report_data['entretien']['electronic_booklet_pic'][] = 'appbackend/img/' . $image;
										}
									}
								}							
							}
						}
					}
					/*-- Service bills --*/
					if($entretien->text == 'question_entretien_2'){ 
						if(isset($entretien->items) && !empty($entretien->items)){
							foreach ($entretien->items as $factures) {
								if($factures->value && $factures->value == 1){
									if ($factures->text == 'examglobal_yes') {
										$report_data['entretien']['service_bill'] = 1;
										if(isset($factures->items) && !empty($factures->items)){
											foreach ($factures->items as $serviceBill) {
												if ($serviceBill->text == 'ec026t37') {
													if(isset($serviceBill->value) && !empty($serviceBill->value)){
														foreach ($serviceBill->value as $k_image => $image) {
															$report_data['entretien']['service_bill_pic'][] = 'appbackend/img/' . $image;
														}
													}
												}
												if($serviceBill->value && $serviceBill->value == 1){
													$report_data['entretien']['service_bill_count'] = $serviceBill->text;
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- Claim bills*/
					if($entretien->text == 'question_entretien_4'){ 
						if(isset($entretien->items) && !empty($entretien->items)){
							foreach ($entretien->items as $claims) {
								if($claims->value && $claims->value == 1){
									if ($claims->text == 'examglobal_yes') {
										$report_data['entretien']['claim_bill'] = 1;
										if(isset($claims->items) && !empty($claims->items)){
											foreach ($claims->items as $claimBill) {
												if ($claimBill->text == 'ec02nwwu') {
													if(isset($claimBill->value) && !empty($claimBill->value)){
														foreach ($claimBill->value as $k_image => $image) {
															$report_data['entretien']['claim_bill_pic'][] = 'appbackend/img/' . $image;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- Last expertise --*/
					if($entretien->text == 'question_entretien_5'){ 
						if(isset($entretien->items) && !empty($entretien->items)){
							foreach ($entretien->items as $last_expertise) {
								if(isset($last_expertise->value) && $last_expertise->value && $last_expertise->value == 1){
									if ($last_expertise->text == 'examglobal_yes') {
										$report_data['entretien']['last_expertise_sheet'] = 1;
										if(isset($last_expertise->items) && !empty($last_expertise->items)){
											foreach ($last_expertise->items as $last_expertise_date) {
												if ($last_expertise_date->text == 'ec029b7p') {
													if(isset($last_expertise_date->value) && !empty($last_expertise_date->value)){
														$report_data['entretien']['last_expertise_date'] = $last_expertise_date->value;
													}
												}
												if ($last_expertise_date->text == 'ec02pdbw' and isset($last_expertise_date->value)) {
													if (count($last_expertise_date->value) > 0) {
														$report_data['entretien']['last_expertise_pic'] = 'appbackend/img/' . $last_expertise_date->value[0];
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				/*-- Days difference from last expertise to today --*/
				if(isset($report_data['entretien']['last_expertise_date']) && !empty($report_data['entretien']['last_expertise_date'])){
					$last_expertise_date = date('Y-m-d', strtotime($report_data['entretien']['last_expertise_date']));
					$datetime1 = date_create($last_expertise_date);
					$datetime2 = date_create($today);
					$interval = date_diff($datetime1, $datetime2);
					$report_data['report_days'] = $interval->format('%a');
				}

				// On détermine si le vendeur ne possède pas le dernier revelé d'expertise du service des automobiles.
				if ($annonces->premiereimm) {
					$dateArray = explode('-', $annonces->premiereimm);
					$date1 = $dateArray[2].'-'.$dateArray[1].'-'.$dateArray[0];

					$diff = abs(strtotime($today) - strtotime($date1));
					$years = floor($diff / (365*60*60*24));

					if ($years < 5) { $report_data['entretien']['car_has_less_than_5y'] = 1; }
				}


			// Essais routiers
				$report_data['essais_routier']['brake_efficiency'] = 0;
				$report_data['essais_routier']['drift_control'] = 0;
				foreach ($exam_data['essais_routier'] as $k => $essais_routier) {
					if($essais_routier->text == 'question_essais_routier_10'){
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $brake_efficiency) {
								if($brake_efficiency->value && $brake_efficiency->value == 1){
									if ($brake_efficiency->text == 'ec07t05z') {
										$report_data['essais_routier']['brake_efficiency'] = 1;
									}else if($brake_efficiency->text == 'ec07bh8b'){
										if(isset($brake_efficiency->items) && !empty($brake_efficiency->items)){
											foreach ($brake_efficiency->items as $b => $brake_defect) {
												if($brake_defect->value && $brake_defect->value == 1){
													$report_data['essais_routier']['brake_defect'][$b] = $keyValueArr[$brake_defect->text];
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- suspension efficiency --*/
					if($essais_routier->text == 'question_essais_routier_11'){
						foreach ($essais_routier->items as $key => $suspensions) {
							foreach ($suspensions->items as $k => $suspension) {
								if(!empty($suspension->value) && $suspension->value && $suspension->value == 1){
									if($suspension->text == 'ec04mkre'){
										$report_data['essais_routier']['suspension_efficiency'][$suspensions->text] = 1;
									}elseif ($suspension->text == 'ec04365g') {
										foreach ($suspension->items as $suspItem) {
											if(isset($suspItem->value) && $suspItem->value && $suspItem->value == 1){
												$report_data['essais_routier']['suspension_efficiency'][$suspensions->text][] = $suspItem->text;
											}
										}
									}
								}
							}
						}
					}

					/*-- Décrivez l’efficacité ressentie de l’embrayage --*/
					if($essais_routier->text == 'question_essais_routier_12'){
						foreach ($essais_routier->items as $essais_keys => $essais) {
							if ($essais->value == 1) {
								if ($essais->text == 'ec04mkre') {
									$report_data['essais_routier']['embrayage']['value'] = 'zero';
								}elseif ($essi->text == 'ec04365g') {
									$report_data['essais_routier']['embrayage']['value'] = 'many';
								}else{
									$report_data['essais_routier']['embrayage']['value'] = 'other';
								}
								$report_data['essais_routier']['embrayage']['text'] = $keyValueArr[ $essais->text ];
							}
						}
					}

					/*-- drift control --*/
					if($essais_routier->text == 'question_essais_routier_4'){ 
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $drift_control) {
								if($drift_control->value && $drift_control->value == 1){
									if ($drift_control->text == 'ec38fxoe') {
										$report_data['essais_routier']['drift_control'] = 1;
									}else if($drift_control->text == 'ec07bh8b'){
										if(isset($drift_control->items) && !empty($drift_control->items)){
											foreach ($drift_control->items as $k => $drift_control_defect) {
												if(isset($drift_control_defect->value) && $drift_control_defect->value && $drift_control_defect->value == 1){
													$report_data['essais_routier']['drift_control_defects'][$k] = $drift_control_defect->text;
													if(array_key_exists($drift_control_defect->text, $keyValueArr))
													$report_data['essais_routier']['drift_control_defects1'][$k] = $keyValueArr[$drift_control_defect->text];
												}
											}
										}
									}
								}
							}
						}
					}
				}
			
			/*-- suspension efficiency --*/
				if(!empty($report_data['essais_routier']['suspension_efficiency'])){
					foreach ($report_data['essais_routier']['suspension_efficiency'] as $susKey => $suspension_efficiency) {
						if(is_array($suspension_efficiency)){
							foreach ($suspension_efficiency as $k => $value) {
								if($value == 'ec38ffco'){
									$report_data['essais_routier']['suspension_efficiency'][$susKey][$k] = 'Bruits anormaux';
								}
								if($value == 'ec398lqz'){
									$report_data['essais_routier']['suspension_efficiency'][$susKey][$k] = 'Oscillations inhabituelles lors du freinage ou de l\'accélération';								
								}
								if($value == 'ec399cac'){								
									$report_data['essais_routier']['suspension_efficiency'][$susKey][$k] = 'Roulis';
								}
							}
						}
						/*-- left front suspension --*/
						if($susKey == 'ec39oooo'){
							$report_data['essais_routier']['suspension_efficiency1']['left_front'] = $report_data['essais_routier']['suspension_efficiency']['ec39oooo'];
						}
						/*-- left rear suspension --*/
						if($susKey == 'ec390wv7'){
							$report_data['essais_routier']['suspension_efficiency1']['left_rear'] = $report_data['essais_routier']['suspension_efficiency']['ec390wv7'];
						}
						/*-- right rear suspension --*/
						if($susKey == 'ec39bq28'){
							$report_data['essais_routier']['suspension_efficiency1']['right_rear'] = $report_data['essais_routier']['suspension_efficiency']['ec39bq28'];
						}
						/*-- right front suspension --*/
						if($susKey == 'ec39altb'){
							$report_data['essais_routier']['suspension_efficiency1']['right_front'] = $report_data['essais_routier']['suspension_efficiency']['ec39altb'];
						}

						foreach ($report_data['essais_routier']['suspension_efficiency1']['left_front'] as $key => $value) {
							if ($value == "Roulis") { $report_data['essais_routier']['suspension_efficiency1']['left_front'][$key] = "Présence de roulis"; }
							if ($value == "Bruits anormaux") { $report_data['essais_routier']['suspension_efficiency1']['left_front'][$key] = "Présence de bruits anormaux"; }
							if ($value == "Tangage") { $report_data['essais_routier']['suspension_efficiency1']['left_front'][$key] = "Présence de tangage"; }
						}
						foreach ($report_data['essais_routier']['suspension_efficiency1']['left_rear'] as $key => $value) {
							if ($value == "Roulis") { $report_data['essais_routier']['suspension_efficiency1']['left_rear'][$key] = "Présence de roulis"; }
							if ($value == "Bruits anormaux") { $report_data['essais_routier']['suspension_efficiency1']['left_rear'][$key] = "Présence de bruits anormaux"; }
							if ($value == "Tangage") { $report_data['essais_routier']['suspension_efficiency1']['left_rear'][$key] = "Présence de tangage"; }
						}
						foreach ($report_data['essais_routier']['suspension_efficiency1']['right_rear'] as $key => $value) {
							if ($value == "Roulis") { $report_data['essais_routier']['suspension_efficiency1']['right_rear'][$key] = "Présence de roulis"; }
							if ($value == "Bruits anormaux") { $report_data['essais_routier']['suspension_efficiency1']['right_rear'][$key] = "Présence de bruits anormaux"; }
							if ($value == "Tangage") { $report_data['essais_routier']['suspension_efficiency1']['right_rear'][$key] = "Présence de tangage"; }
						}
						foreach ($report_data['essais_routier']['suspension_efficiency1']['right_front'] as $key => $value) {
							if ($value == "Roulis") { $report_data['essais_routier']['suspension_efficiency1']['right_front'][$key] = "Présence de roulis"; }
							if ($value == "Bruits anormaux") { $report_data['essais_routier']['suspension_efficiency1']['right_front'][$key] = "Présence de bruits anormaux"; }
							if ($value == "Tangage") { $report_data['essais_routier']['suspension_efficiency1']['right_front'][$key] = "Présence de tangage"; }
						}

					}
				}

				if(isset($report_data['essais_routier']['brake_defect']) && ! empty($report_data['essais_routier']['brake_defect'])){
					foreach ($report_data['essais_routier']['brake_defect'] as $b => $defect) {
						if($defect == 'ec39o9rx'){
							$report_data['essais_routier']['brake_defect'][$b] = 'Présence d\'à-coups';
						}
						if($defect == 'ec39mht6'){
							$report_data['essais_routier']['brake_defect'][$b] = 'Bruit sourd';
						}
						if($defect == 'ec39c9eu'){
							$report_data['essais_routier']['brake_defect'][$b] = 'Bruits d\'arrachement métallique';
						}
						if($defect == 'ec399nrq'){
							$report_data['essais_routier']['brake_defect'][$b] = 'Pédale de frein molle';
						}
						if($defect == 'ec384d7k'){
							$report_data['essais_routier']['brake_defect'][$b] = 'Vibration du volant';
						}
						if($defect == 'ec142b4g'){
							$report_data['essais_routier']['brake_defect'][$b] = 'Autre défaut';
						}						
					}
				}

				$report_data['wheel']['suspensions_damage'] = 1;
				$report_data['wheel']['steerings_damage'] = 1;
				$report_data['wheel']['cardan_damage'] = 1;
				$report_data['wheel']['rims_damage'] = 1;
				$report_data['wheel']['tires_damage'] = 1;
				$report_data['wheel']['wheel_operations_damage'] = 1;
			
			/*---- WHEEL START ----*/
				foreach ($exam_data['wheel'] as $k => $wheel) {
					/*-- Wheel Suspension start --*/
					if($wheel->text == 'ec075638'){ 
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $suspensions) {
								if(isset($suspensions->value) && $suspensions->value && $suspensions->value == 1){
									if ($suspensions->text == 'ec06s20g') { /*-- no suspensions damage --*/
										$report_data['wheel']['suspensions_damage'] = 0;
									}else if($suspensions->text == 'ec06j9rm'){ /*-- one or more damage --*/
										if(isset($suspensions->items) && !empty($suspensions->items)){
											foreach ($suspensions->items as $s => $suspension) {
												if(isset($suspension->value) && $suspension->value && $suspension->value == 1){
													if(isset($suspension->items) && !empty($suspension->items)){
														foreach ($suspension->items as $key => $lf_suspension) {
															if(isset($lf_suspension->value) && $lf_suspension->value && $lf_suspension->value == 1){
																foreach ($lf_suspension->items as $k => $val) {
																	if(isset($val->value) && $val->value && $val->value == 1){
																		$report_data['wheel']['suspension'][$suspension->text][$lf_suspension->text][] = $val->text;

																		// Images -- victor
																		if (isset($val->items)) {
																			if ($val->items[0]->type == "picture" && count($val->items[0]->value) > 0) {
																				foreach ($val->items[0]->value as $myKey => $img) {
																					$myKey2 = count($report_data['wheel']['suspension_pic']);
																					$report_data['wheel']['suspension_pic'][$myKey2]['soustitre'] = $keyValueArr[$lf_suspension->text];
																					$report_data['wheel']['suspension_pic'][$myKey2]['image'] = 'appbackend/img/' . $img;
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- Wheel Suspension end --*/
					/*-- ETAT DES ORGANES DE LA DIRECTION --*/
					if($wheel->text == 'ec04tz4a'){
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $steerings) {
								if(isset($steerings->value) && $steerings->value && $steerings->value == 1){
									if ($steerings->text == 'ec06s20g') { /*-- no steering component damage --*/
										$report_data['wheel']['steerings_damage'] = 0;
									}else if($steerings->text == 'ec06j9rm'){ /*-- one or more damage --*/
										#damages start
										if(isset($steerings->items) && !empty($steerings->items)){
											foreach ($steerings->items as $s => $steering) {
												if(isset($steering->value) && $steering->value && $steering->value == 1){
													if(isset($steering->items) && !empty($steering->items)){
														foreach ($steering->items as $key => $lf_steering) {
															if(isset($lf_steering->value) && $lf_steering->value && $lf_steering->value == 1){
																if(isset($lf_steering->items) && !empty($lf_steering->items)){
																	foreach ($lf_steering->items as $k => $val) {
																		/*-- picture or text --*/
																		if((is_array($val->value) && !empty($val->value))){
																			$report_data['wheel']['steering'][$steering->text][$lf_steering->text][$val->text] = 'appbackend/img/' . $val->value[0];
																			$report_data['wheel']['steering_pic'][] = 'appbackend/img/' . $val->value[0];
																			if(array_key_exists($lf_steering->text, $keyValueArr)){
																				$report_data['wheel']['steering1'][$steering->text][$keyValueArr[$lf_steering->text]] = $keyValueArr[$lf_steering->text];
																			}
																		}elseif((isset($val->value) && $val->value && $val->value == 1)){
																			if(isset($val->items) && !empty($val->items)){ /*-- subitems --*/
																				foreach ($val->items as $pics) {
																					if($pics->type == 'picture' && (is_array($pics->value) && !empty($pics->value))){
																						$report_data['wheel']['steering'][$steering->text][$lf_steering->text][$val->text][$pics->text] = 'appbackend/img/' . $pics->value[0];
																						$report_data['wheel']['steering_pic'][] = 'appbackend/img/' . $pics->value[0];
																					}
																				}
																			}
																			$report_data['wheel']['steering'][$steering->text][$lf_steering->text] = $val->text;
																			if(array_key_exists($val->text, $keyValueArr) && array_key_exists($lf_steering->text, $keyValueArr)){
																				$report_data['wheel']['steering1'][$steering->text][$keyValueArr[$lf_steering->text]] = $keyValueArr[$val->text];
																			}
																		}
																	}
																}else{
																	$report_data['wheel']['steering'][$steering->text] = $lf_steering->text;
																	$report_data['wheel']['steering1'][$steering->text][$keyValueArr[$lf_steering->text]] = $keyValueArr[$lf_steering->text];
																}
															}
														}
													}
												}
											}
										}
										#end
									}
								}
							}
						}
					}

					/*-- ETAT DES SOUFFLETS DE CARDAN --*/
					if($wheel->text == 'ec04e1vi') {
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $cardans) {
								if(isset($cardans->value) && $cardans->value && $cardans->value == 1){
									if ($cardans->text == 'ec06s20g') { /*-- no cardan component damage --*/
										$report_data['wheel']['cardan_damage'] = 0;
									}else if($cardans->text == 'ec06j9rm'){ /*-- one or more damage --*/
										#damages start
										if(isset($cardans->items) && !empty($cardans->items)){
											foreach ($cardans->items as $s => $cardan) {
												if(isset($cardan->value) && $cardan->value && $cardan->value == 1){
													if(isset($cardan->items) && !empty($cardan->items)){
														foreach ($cardan->items as $key => $lf_cardan) {
															if(isset($lf_cardan->value) && $lf_cardan->value && $lf_cardan->value == 1){
																if(isset($lf_cardan->items) && !empty($lf_cardan->items)){
																	foreach ($lf_cardan->items as $k => $val) {
																		/*-- picture or text --*/
																		if((is_array($val->value) && !empty($val->value))){
																			$report_data['wheel']['cardan'][$cardan->text][$lf_cardan->text][$val->text] = 'appbackend/img/' . $val->value[0];
																			$report_data['wheel']['cardan_pic'][] = 'appbackend/img/' . $val->value[0];
																			if(array_key_exists($lf_cardan->text, $keyValueArr)){
																				$report_data['wheel']['cardan1'][$keyValueArr[$cardan->text]] = $keyValueArr[$lf_cardan->text];
																			}
																		}elseif((isset($val->value) && $val->value && $val->value == 1)){
																			if(isset($val->items) && !empty($val->items)){ /*-- subitems --*/
																				foreach ($val->items as $pics) {
																					if($pics->type == 'picture' && (is_array($pics->value) && !empty($pics->value))){
																						$report_data['wheel']['cardan'][$cardan->text][$lf_cardan->text][$val->text][$pics->text] = 'appbackend/img/' . $pics->value[0];
																						$report_data['wheel']['cardan_pic'][] = 'appbackend/img/' . $pics->value[0];
																					}
																				}
																			}
																			$report_data['wheel']['cardan'][$cardan->text][$lf_cardan->text] = $val->text;
																			if(array_key_exists($val->text, $keyValueArr) && array_key_exists($lf_cardan->text, $keyValueArr)){
																				$report_data['wheel']['cardan1'][$keyValueArr[$cardan->text]][$keyValueArr[$lf_cardan->text]] = $keyValueArr[$val->text];
																			}
																		}
																	}
																}else{
																	$report_data['wheel']['cardan'][$cardan->text] = $lf_cardan->text;
																	if(array_key_exists($lf_cardan->text, $keyValueArr)){
																		$report_data['wheel']['cardan1'][$keyValueArr[$cardan->text]] = $keyValueArr[$lf_cardan->text];
																	}
																}
															}
														}
													}
												}
											}
										}
										#end
									}
								}
							}
						}
					}

					/*-- RIM Condition --*/
					if($wheel->text == 'ec07dqmi'){
						foreach ($wheel->items as $items_key => $items) {
							foreach ($items->items as $item_key => $item) {
								if ($items->text == 'question_wheel_fl_0') { $k = 0; }
								elseif ($items->text == 'question_wheel_fr_0') { $k = 1; }
								elseif ($items->text == 'question_wheel_bl_0') { $k = 2; }
								elseif ($items->text == 'question_wheel_br_0') { $k = 3; }
								else { $k = 9; }

								$report_data['wheel']['rim1'][$k]['text'] = $keyValueArr[$items->text];
								foreach ($item->value as $img_key => $img) {
									$report_data['wheel']['rim1'][$k]['img'][] = 'appbackend/img/' . $img;
								}
							}
						}
						ksort($report_data['wheel']['rim1']);
						// echo "<pre>"; print_r($report_data['wheel']['rim1']); echo "</pre>";
						 // die();
					}

					/*-- Tires condition --*/
					if($wheel->text == 'ec07ts1e'){
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $tires) {
								if(isset($tires->value) && $tires->value && $tires->value == 1){
									if ($tires->text == 'ec06s20g') { /*-- no tires damage --*/
										$report_data['wheel']['tires_damage'] = 0;
									}else if($tires->text == 'ec06j9rm'){ /*-- one or more damage --*/
										if(isset($tires->items) && !empty($tires->items)){
											foreach ($tires->items as $s => $tire) {
												if(isset($tire->value) && $tire->value && $tire->value == 1){
													if(isset($tire->items) && !empty($tire->items)){
														foreach ($tire->items as $key => $lf_tire) {
															if(isset($lf_tire->value) && $lf_tire->value && $lf_tire->value == 1){
																$report_data['wheel']['tire'][$tire->text][] = $lf_tire->text;
																if(array_key_exists($lf_tire->text, $keyValueArr)){																
																	$report_data['wheel']['tire1'][$tire->text][] = $keyValueArr[$lf_tire->text];
																}
																if(isset($lf_tire->items) && !empty($lf_tire->items)){
																	foreach ($lf_tire->items as $k => $val) {
																		if($val->type == 'picture' && (isset($val->value) && is_array($val->value) && !empty($val->value))){
																			$report_data['wheel']['tire_pic'][] = 'appbackend/img/' . $val->value[0];
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					/*-- Tires condition --*/
					if($wheel->text == 'ec07ahg3'){
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $tire_sculptures) {
								if(isset($tire_sculptures->items) && !empty($tire_sculptures->items)){
									foreach ($tire_sculptures->items as $s => $tire_sculpture) {
										if(isset($tire_sculpture->value) && $tire_sculpture->value && $tire_sculpture->value == 1){
											$report_data['wheel']['tire_sculpture'][$tire_sculptures->text] = $tire_sculpture->text;										
											if(array_key_exists($tire_sculpture->text, $keyValueArr)){
												if($tire_sculpture->text == 'ec07dl5l'){
													$report_data['wheel']['tire_sculpture1'][$tire_sculptures->text] = $tire_sculpture->text;
												}else{
													$report_data['wheel']['tire_sculpture1'][$tire_sculptures->text] = $keyValueArr[$tire_sculpture->text];
												}
											}

											if(isset($tire_sculpture->items) && !empty($tire_sculpture->items)){
												foreach ($tire_sculpture->items as $key => $lf_tire_sculpture) {
													if(isset($lf_tire_sculpture->value) && !empty($lf_tire_sculpture->value) && $lf_tire_sculpture->type == 'number'){
														$report_data['wheel']['sculpture_depth'][$tire_sculptures->text] = $lf_tire_sculpture->value;
													}
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- brake pad visual condition --*/
					if($wheel->text == 'ec076h4g'){
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $brake_pads) {
								if(isset($brake_pads->items) && !empty($brake_pads->items)){
									foreach ($brake_pads->items as $s => $brake_pad) {
										if(isset($brake_pad->value) && $brake_pad->value && $brake_pad->value == 1){
											$report_data['wheel']['brake_pad'][$brake_pads->text] = $brake_pad->text;										
											if(array_key_exists($brake_pad->text, $keyValueArr)){
												$report_data['wheel']['brake_pad1'][$brake_pads->text][] = $keyValueArr[$brake_pad->text];
											}
										}
									}
								}
							}
						}
					}

					/*-- Brake disks condition --*/
					if($wheel->text == 'ec07yfde'){
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $brake_disks) {
								if(isset($brake_disks->type) && $brake_disks->type == 'label'){
									if(isset($brake_disks->items) && !empty($brake_disks->items)){
										foreach ($brake_disks->items as $s => $brake_disk_roue) {
											$roue = $brake_disk_roue->text;
											foreach ($brake_disk_roue->items as $key => $brake_disk) {
												if($brake_disk->value == 1){
													if ($brake_disk->text == 'ec06s20g') { /*-- no brake_disks damage --*/
														$report_data['wheel']['brake_disks_damage'][$roue] = 0;
													}else if($brake_disk->text == 'ec06j9rm'){ /*-- one or more damage --*/
														$report_data['wheel']['brake_disks_damage'][$roue] = 1;
														if(isset($brake_disk->items) && !empty($brake_disk->items)){
															foreach ($brake_disk->items as $key => $lf_brake_disk) {
																if($lf_brake_disk->value == 1){
																	$report_data['wheel']['brake_disk1'][$roue][] = $keyValueArr[$lf_brake_disk->text];
																	if (isset($lf_brake_disk->items)) {
																		if ($lf_brake_disk->items[0]->type == 'picture' && count($lf_brake_disk->items[0]->value) > 0) {
																			$report_data['wheel']['brake_disk_pic'][] = 'appbackend/img/' . $lf_brake_disk->items[0]->value[0];
																		}
																	}


																	// if(isset($lf_brake_disk->items) && !empty($lf_brake_disk->items)){
																	// 	// echo $keyValueArr[$lf_brake_disk->text] . '<br>';
																	// 	foreach ($lf_brake_disk->items as $k => $val) {
																	// 	// echo "<pre>"; print_r($val); echo "</pre>";
																	// 		if(isset($val->value) && $val->value && $val->value == 1){
																	// 			$report_data['wheel']['brake_disk'][$lf_brake_disk->text][$k] = $val->text;
																	// 			if(array_key_exists($val->text, $keyValueArr)){																
																	// 				$report_data['wheel']['brake_disk1'][$lf_brake_disk->text][$k] = $keyValueArr[$val->text];
																	// 			}
																	// 			if(isset($val->items) && !empty($val->items)){
																	// 				foreach ($val->items as $pic) {
																	// 					if($pic->type == 'picture' && (isset($pic->value) && is_array($pic->value) && !empty($pic->value))){
																	// 						$report_data['wheel']['brake_disk_pic'][] = 'appbackend/img/' . $pic->value[0];
																	// 					}
																	// 				}
																	// 			}
																	// 		}
																	// 	}
																	// }
																}
															}
														}
													}
												}
											}







										}
									}
								}elseif(isset($brake_disks->type) && $brake_disks->type == 'number'){
									if(isset($brake_disks->value) && !empty($brake_disks->value)){
										$report_data['wheel']['brake_disk_thickness'][$brake_disks->text] = $brake_disks->value;
									}
								}
							}
						}
					}
					/*-- Wheel operations --*/
					if($wheel->text == 'question_wheel_2b'){
						if(isset($wheel->items) && !empty($wheel->items)){
							foreach ($wheel->items as $wheel_operations) {
								if(isset($wheel_operations->value) && $wheel_operations->value && $wheel_operations->value == 1){
									if ($wheel_operations->text == 'ec07de9o') { /*-- no wheel_operations damage --*/
										$report_data['wheel']['wheel_operations_damage'] = 0;
									}else if($wheel_operations->text == 'ec07y47t'){ /*-- one or more damage --*/
										if(isset($wheel_operations->items) && !empty($wheel_operations->items)){
											foreach ($wheel_operations->items as $s => $wheel_operation) {
												if(isset($wheel_operation->value) && $wheel_operation->value && $wheel_operation->value == 1){
													if(isset($wheel_operation->items) && !empty($wheel_operation->items)){
														foreach ($wheel_operation->items as $key => $lf_wheel_operation) {
															if(isset($lf_wheel_operation->value) && $lf_wheel_operation->value && $lf_wheel_operation->value == 1){
																/*$report_data['wheel']['wheel_operation'][$wheel_operation->text][] = $lf_wheel_operation->text;
																if(array_key_exists($lf_wheel_operation->text, $keyValueArr)){
																	$report_data['wheel']['wheel_operation1'][$wheel_operation->text][] = $keyValueArr[$lf_wheel_operation->text];
																}*/
																if(isset($lf_wheel_operation->items) && !empty($lf_wheel_operation->items)){
																	foreach ($lf_wheel_operation->items as $k => $val) {
																		if(isset($val->value) && $val->value && !empty($val->value)){
																			$report_data['wheel']['wheels_operation'][$wheel_operation->text][$lf_wheel_operation->text] = $val->text;
																			if(array_key_exists($lf_wheel_operation->text, $keyValueArr) && array_key_exists($val->text, $keyValueArr)){
																				$report_data['wheel']['wheels_operation1'][$wheel_operation->text][$keyValueArr[$lf_wheel_operation->text]] = $keyValueArr[$val->text];
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				/*---- WHEEL END ----*/

			/*-- LIGHTS --*/
				$report_data['light']['buzzer_damage'] = 1;
				$report_data['light']['hazard_warning_damage'] = 1;
				$report_data['light']['front_rear_lights_damage'] = 1;
				$report_data['light']['front_rear_indicators_damage'] = 1;
				$report_data['light']['optics_damage'] = 1;
				$report_data['light']['wipers_damage'] = 1;
				foreach ($exam_data['light'] as $k => $light) {
					/*-- BUZZER --*/
					if($light->text == 'question_light_1'){
						if(isset($light->items) && !empty($light->items)){
							foreach ($light->items as $key => $buzzers) {
								if(isset($buzzers->value) && $buzzers->value && $buzzers->value == 1){
									if($buzzers->text == 'ec04mkre'){
										$report_data['light']['buzzer_damage'] = 0;
									}elseif ($buzzers->text == 'ec04365g') {/*-- one or more damage --*/
										if(isset($buzzers->items) && !empty($buzzers->items)){
											foreach ($buzzers->items as $s => $buzzer) {
												if(isset($buzzer->value) && $buzzer->value && $buzzer->value == 1){
													$report_data['light']['buzzer'][] = $buzzer->text;
													if(array_key_exists($buzzer->text, $keyValueArr)){
														$report_data['light']['buzzer1'][] = $keyValueArr[$buzzer->text];
													}
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- hazard warning light --*/
					if($light->text == 'question_light_18'){
						if(isset($light->items) && !empty($light->items)){
							foreach ($light->items as $key => $hazard_warnings) {
								if(isset($hazard_warnings->value) && $hazard_warnings->value && $hazard_warnings->value == 1){
									if($hazard_warnings->text == 'ec04mkre'){
										$report_data['light']['hazard_warning_damage'] = 0;
									}elseif ($hazard_warnings->text == 'ec04365g') {/*-- one or more damage --*/
										if(isset($hazard_warnings->items) && !empty($hazard_warnings->items)){
											foreach ($hazard_warnings->items as $s => $hazard_warning) {
												if(isset($hazard_warning->value) && $hazard_warning->value && $hazard_warning->value == 1){
													$report_data['light']['hazard_warning'][] = $hazard_warning->text;
													if(array_key_exists($hazard_warning->text, $keyValueArr)){
														$report_data['light']['hazard_warning1'][] = $keyValueArr[$hazard_warning->text];
													}
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- light operations --*/
					if($light->text == 'question_light_2_big'){
						if(isset($light->items) && !empty($light->items)){
							foreach ($light->items as $front_rear_lights) {
								if(isset($front_rear_lights->value) && $front_rear_lights->value && $front_rear_lights->value == 1){
									if ($front_rear_lights->text == 'ec04mkre') { /*-- no front_rear_lights damage --*/
										$report_data['light']['front_rear_lights_damage'] = 0;
									}else if($front_rear_lights->text == 'ec04365g'){ /*-- one or more damage --*/
										if(isset($front_rear_lights->items) && !empty($front_rear_lights->items)){
											foreach ($front_rear_lights->items as $s => $front_rear_light) {
												if(isset($front_rear_light->value) && $front_rear_light->value && $front_rear_light->value == 1){
													if(isset($front_rear_light->items) && !empty($front_rear_light->items)){
														foreach ($front_rear_light->items as $key => $lf_front_rear_light) {
															if(isset($lf_front_rear_light->value) && $lf_front_rear_light->value && $lf_front_rear_light->value == 1){
																$report_data['light']['front_rear_light'][$front_rear_light->text] = $lf_front_rear_light->text;
																if(array_key_exists($lf_front_rear_light->text, $keyValueArr)){
																	$report_data['light']['front_rear_light1'][$front_rear_light->text] = $keyValueArr[$lf_front_rear_light->text];
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					/*-- indicators operations --*/
					if($light->text == 'question_light_3_big'){
						if(isset($light->items) && !empty($light->items)){
							foreach ($light->items as $front_rear_indicators) {
								if(isset($front_rear_indicators->value) && $front_rear_indicators->value && $front_rear_indicators->value == 1){
									if ($front_rear_indicators->text == 'ec04mkre') { /*-- no front_rear_indicators damage --*/
										$report_data['light']['front_rear_indicators_damage'] = 0;
									}else if($front_rear_indicators->text == 'ec04365g'){ /*-- one or more damage --*/
										if(isset($front_rear_indicators->items) && !empty($front_rear_indicators->items)){
											foreach ($front_rear_indicators->items as $s => $front_rear_indicator) {
												if(isset($front_rear_indicator->value) && $front_rear_indicator->value && $front_rear_indicator->value == 1){
													$report_data['light']['front_rear_indicator'][$front_rear_indicator->text] = true;
													$report_data['light']['front_rear_indicator1'][$front_rear_indicator->text] = true;
													if(isset($front_rear_indicator->items) && !empty($front_rear_indicator->items)){
														foreach ($front_rear_indicator->items as $key => $lf_front_rear_indicator) {
															if(isset($lf_front_rear_indicator->value) && $lf_front_rear_indicator->value && $lf_front_rear_indicator->value == 1){
																$report_data['light']['front_rear_indicator'][$front_rear_indicator->text] = $lf_front_rear_indicator->text;
																if(array_key_exists($lf_front_rear_indicator->text, $keyValueArr)){
																	$report_data['light']['front_rear_indicator1'][$front_rear_indicator->text] = $keyValueArr[$lf_front_rear_indicator->text];
																}
															}
														}
													}
																	// $report_data['light']['front_rear_indicator2'][$front_rear_indicator->text] = $keyValueArr[$front_rear_indicator->text];
												}
											}
										}
									}
								}
							}
						}
					}
					if($light->text == 'question_light_4_big'){
						if(isset($light->items) && !empty($light->items)){
							foreach ($light->items as $optics) {
								if(isset($optics->value) && $optics->value && $optics->value == 1){
									if ($optics->text == 'ec06s20g') { /*-- no optics damage --*/
										$report_data['light']['optics_damage'] = 0;
									}else if($optics->text == 'ec06j9rm'){ /*-- one or more damage --*/
										if(isset($optics->items) && !empty($optics->items)){
											foreach ($optics->items as $s => $optic) {
												if(isset($optic->value) && $optic->value && $optic->value == 1){
													// $report_data['light']['optic'][$optic->text] = true;
													// $report_data['light']['optic1'][$optic->text] = true;
													if(isset($optic->items) && !empty($optic->items)){
														foreach ($optic->items as $key => $lf_optic) {
															if(isset($lf_optic->value) && is_array($lf_optic->value) && !empty($lf_optic->value) && $lf_optic->type == 'picture'){
																$report_data['light']['optic'][$optic->text][$lf_optic->text][] = 'appbackend/img/' . $lf_optic->value[0];
																$report_data['light']['optic_pic'][] = 'appbackend/img/' . $lf_optic->value[0];
															}elseif(isset($lf_optic->value) && $lf_optic->value && $lf_optic->value == 1){
																$report_data['light']['optic'][$optic->text][] = $lf_optic->text;
																if(array_key_exists($lf_optic->text, $keyValueArr)){
																	$report_data['light']['optic1'][$optic->text][] = $keyValueArr[$lf_optic->text];
																}
																if(isset($lf_optic->items) && !empty($lf_optic->items)){
																	foreach ($lf_optic->items as $pic) {
																		if($pic->type == 'picture' && (isset($pic->value) && is_array($pic->value) && !empty($pic->value))){
																			$report_data['light']['optic_pic'][] = 'appbackend/img/' . $pic->value[0];
																		}
																	}
																}
															}
														}
													}
													// $report_data['light']['optic2'][$optic->text] = $keyValueArr[$optic->text];
												}
											}
										}
									}
								}
							}
						}
					}
					if($light->text == 'ec04s2s9'){
						if(isset($light->items) && !empty($light->items)){
							foreach ($light->items as $wipers) {
								if(isset($wipers->value) && $wipers->value && $wipers->value == 1){
									if ($wipers->text == 'ec06s20g') { /*-- no wipers damage --*/
										$report_data['light']['wipers_damage'] = 0;
									}else if($wipers->text == 'ec06j9rm'){ /*-- one or more damage --*/
										if(isset($wipers->items) && !empty($wipers->items)){
											foreach ($wipers->items as $s => $wiper) {
												// if(isset($wiper->value) && $wiper->value && $wiper->value == 1){
													if(isset($wiper->items) && !empty($wiper->items)){
														foreach ($wiper->items as $key => $lf_wiper) {
															if(isset($lf_wiper->value) && $lf_wiper->value && $lf_wiper->value == 1){
																$report_data['light']['wiper'][$wiper->text][] = $lf_wiper->text;
																if(array_key_exists($lf_wiper->text, $keyValueArr)){
																	$report_data['light']['wiper1'][$wiper->text][] = $keyValueArr[$lf_wiper->text];
																}
															}
														}
													}
												// }
											}
										}
									}
								}
							}
						}
					}
				}

			// Wheel - suite
				if(isset($report_data['wheel']['sculpture_depth']) && !empty($report_data['wheel']['sculpture_depth'])){
					if(isset($report_data['wheel']['sculpture_depth']['question_wheel_fl_1']) && isset($report_data['wheel']['sculpture_depth']['question_wheel_fr_1'])){
						$report_data['wheel']['sculpture_diff']['fl_fr'] = $report_data['wheel']['sculpture_depth']['question_wheel_fl_1'] - $report_data['wheel']['sculpture_depth']['question_wheel_fr_1'];
						$report_data['wheel']['sculpture_diff']['fl_fr'] = abs($report_data['wheel']['sculpture_diff']['fl_fr']);
					}
					if(isset($report_data['wheel']['sculpture_depth']['question_wheel_bl_1']) && isset($report_data['wheel']['sculpture_depth']['question_wheel_br_1'])){
						$report_data['wheel']['sculpture_diff']['bl_br'] = $report_data['wheel']['sculpture_depth']['question_wheel_bl_1'] - $report_data['wheel']['sculpture_depth']['question_wheel_br_1'];
						$report_data['wheel']['sculpture_diff']['bl_br'] = abs($report_data['wheel']['sculpture_diff']['bl_br']);
					}
				}
				$susp = array();
				if(isset($report_data['wheel']['suspension']) && !empty($report_data['wheel']['suspension'])){
					foreach ($report_data['wheel']['suspension'] as $k => $value) {
						foreach ($value as $key => $val) {
							if($key == 'ec07n8h3'){ //Damper
								$susp[$k]['Amortisseur'] = $val;
								foreach ($val as $i => $item) {
									// if($item == 'ec06nc4g'){
									// 	$susp[$k]['Amortisseur'][$i] = 'Fuites';
									// }
									// if($item == 'ec06kang'){
									// 	$susp[$k]['Amortisseur'][$i] = 'Fissure';
									// }
									// if($item == 'ec06w5yz'){
									// 	$susp[$k]['Amortisseur'][$i] = 'Rouille';
									// }
									// if($item == 'ec0729ia'){
									// 	$susp[$k]['Amortisseur'][$i] = 'Corrosion perforante';
									// }
									// if($item == 'ec11nsgh'){
									// 	$susp[$k]['Amortisseur'][$i] = 'Mauvaise fixation';
									// }
									$susp[$k]['Amortisseur'][$i] = $keyValueArr[$item];
								}
							}
							if($key == 'ec07gls7'){ //Spring
								$susp[$k]['Ressort'] = $val;
								foreach ($val as $i => $item) {
									// if($item == 'ec06w5yz'){
									// 	$susp[$k]['Ressort'][$i] = 'Rouille';
									// }
									// if($item == 'ec0729ia'){
									// 	$susp[$k]['Ressort'][$i] = 'Corrosion perforante';
									// }
									// if($item == 'ec11nsgh'){
									// 	$susp[$k]['Ressort'][$i] = 'Mauvaise fixation';
									// }
									// if($item == 'ec07obeb'){
									// 	$susp[$k]['Ressort'][$i] = 'Fissure(s)';
									// }
									$susp[$k]['Ressort'][$i] = $keyValueArr[$item];
								}
							}
							if($key == 'ec076mml'){ // Ball joint (s) of suspension arms
								$susp[$k]['Rotule(s) de bras de suspensions'] = $val;
								foreach ($val as $i => $item) {
									// if($item == 'ec07k01m'){
									// 	$susp[$k]['Rotule(s) de bras de suspensions'][$i] = 'Détérioration du soufflet en caoutchouc';
									// }
									$susp[$k]['Rotule(s) de bras de suspensions'][$i] = $keyValueArr[$item];
								}
							}
							if($key == 'ec07karo'){ //Triangle of suspensions
								$susp[$k]['Triangle de suspensions'] = $val;
								foreach ($val as $i => $item) {
									// if($item == 'ec06w5yz'){
									// 	$susp[$k]['Triangle de suspensions'][$i] = 'Rouille';
									// }
									// if($item == 'ec0729ia'){
									// 	$susp[$k]['Triangle de suspensions'][$i] = 'Corrosion perforante';
									// }
									// if($item == 'ec05hw4k'){
									// 	$susp[$k]['Triangle de suspensions'][$i] = 'Fissure';
									// }
									// if($item == 'ec11u5b6'){
									// 	$susp[$k]['Triangle de suspensions'][$i] = 'Choc/déformation mineure';
									// }
									// if($item == 'ec07ma8c'){
									// 	$susp[$k]['Triangle de suspensions'][$i] = 'Choc/déformation majeure';
									// }
									$susp[$k]['Triangle de suspensions'][$i] = $keyValueArr[$item];
								}
							}
							if($key == 'ec07854a'){ //Suspension triangle silencer (s)
								$susp[$k]['Silentbloc(s) de triangle de suspensions'] = $val;
								foreach ($val as $i => $item) {
									// if($item == 'ec06w5yz'){
									// 	$susp[$k]['Silentbloc(s) de triangle de suspensions'][$i] = 'Rouille';
									// }
									// if($item == 'ec0729ia'){
									// 	$susp[$k]['Silentbloc(s) de triangle de suspensions'][$i] = 'Corrosion Corrosion perforante';
									// }
									// if($item == 'ec05hw4k'){
									// 	$susp[$k]['Silentbloc(s) de triangle de suspensions'][$i] = 'Fissure';
									// }
									// if($item == 'ec11u5b6'){
									// 	$susp[$k]['Silentbloc(s) de triangle de suspensions'][$i] = 'Choc/déformation mineure';
									// }
									// if($item == 'ec07ma8c'){
									// 	$susp[$k]['Silentbloc(s) de triangle de suspensions'][$i] = 'Choc/déformation majeure';
									// }
									// if($item == 'ec04z3a3'){
									// 	$susp[$k]['Silentbloc(s) de triangle de suspensions'][$i] = 'Détérioration du caoutchouc';
									// }
									$susp[$k]['Silentbloc(s) de triangle de suspensions'][$i] = $keyValueArr[$item];
								}
							}
							if($key == 'ec07gntk'){ //Suspension arms
								$susp[$k]['Bras de suspensions'] = $val;
								foreach ($val as $i => $item) {
									// if($item == 'ec06w5yz'){
									// 	$susp[$k]['Bras de suspensions'][$i] = 'Rouille';
									// }
									// if($item == 'ec0729ia'){
									// 	$susp[$k]['Bras de suspensions'][$i] = 'Corrosion perforante';
									// }
									// if($item == 'ec05hw4k'){
									// 	$susp[$k]['Bras de suspensions'][$i] = 'Fissure';
									// }
									// if($item == 'ec11u5b6'){
									// 	$susp[$k]['Bras de suspensions'][$i] = 'Choc/déformation mineure';
									// }
									// if($item == 'ec07ma8c'){
									// 	$susp[$k]['Bras de suspensions'][$i] = 'Choc/déformation majeure';
									// }
									$susp[$k]['Bras de suspensions'][$i] = $keyValueArr[$item];
								}
							}
							if($key == 'ec074sfa'){ //Stabilizer link
								$susp[$k]['Biellette de barre stabilisatrice'] = $val;
								foreach ($val as $i => $item) {
									// if($item == 'ec06w5yz'){
									// 	$susp[$k]['Biellette de barre stabilisatrice'][$i] = 'Rouille';
									// }
									// if($item == 'ec0729ia'){
									// 	$susp[$k]['Biellette de barre stabilisatrice'][$i] = 'Corrosion perforante';
									// }
									// if($item == 'ec05hw4k'){
									// 	$susp[$k]['Biellette de barre stabilisatrice'][$i] = 'Fissure';
									// }
									// if($item == 'ec11u5b6'){
									// 	$susp[$k]['Biellette de barre stabilisatrice'][$i] = 'Choc/déformation mineure';
									// }
									// if($item == 'ec07ma8c'){
									// 	$susp[$k]['Biellette de barre stabilisatrice'][$i] = 'Choc/déformation majeure';
									// }
									$susp[$k]['Biellette de barre stabilisatrice'][$i] = $keyValueArr[$item];
								}
							}
							if($key == 'ec046bjv'){ // Rotule(s) de triangle de suspensions
								foreach ($val as $i => $item) {
									$susp[$k]['Rotule(s) de triangle de suspensions'][] = $keyValueArr[$item];
								}
							}
							if($key == 'ec04d8zu'){ // Silentbloc(s) de bras de suspensions
								foreach ($val as $i => $item) {
									$susp[$k]['Silentbloc(s) de bras de suspensions'][] = $keyValueArr[$item];
								}
							}
							if($key == 'ec04gyh3'){ // Barre stabilisatrice
								foreach ($val as $i => $item) {
									if ($keyValueArr[$item] == 'Choc/déformation majeure') {
										$susp[$k]['Barre stabilisatrice'][] = 'Choc/déformation importante';
									}else {
										$susp[$k]['Barre stabilisatrice'][] = $keyValueArr[$item];
									}
								}
							}
						}
					}

					/*-- left front suspension --*/
					if(isset($susp['ec39oooo'])){
						$report_data['wheel']['suspension1']['left_front'] = $susp['ec39oooo'];
					}
					/*-- left rear suspension --*/
					if(isset($susp['ec390wv7'])){
						$report_data['wheel']['suspension1']['left_rear'] = $susp['ec390wv7'];
					}
					/*-- right rear suspension --*/
					if(isset($susp['ec39bq28'])){
						$report_data['wheel']['suspension1']['right_rear'] = $susp['ec39bq28'];
					}
					/*-- right front suspension --*/
					if(isset($susp['ec39altb'])){
						$report_data['wheel']['suspension1']['right_front'] = $susp['ec39altb'];
					}
				}

			/*-- ENGINE --*/
				$report_data['ext_engine']['compartments_damage'] = 1;
				$report_data['ext_engine']['engine_sealings_damage'] = 1;
				$report_data['ext_engine']['fluid_levels_ok'] = 0;
				foreach ($exam_data['engine'] as $k => $ext_engine) {
					if($ext_engine->text == 'question_engine_2'){
						if(isset($ext_engine->items) && !empty($ext_engine->items)){
							foreach ($ext_engine->items as $key => $engine_compartments) {
								/*-- ETAT DU COMPARTIMENT MOTEUR* --*/
								if($engine_compartments->text == 'ec06fxdn') {
									if(isset($engine_compartments->items) && !empty($engine_compartments->items)){
										foreach ($engine_compartments->items as $compartments) {
											if(isset($compartments->value) && $compartments->value && $compartments->value == 1){
												if ($compartments->text == 'ec06s20g') { /*-- no compartments damage --*/
													$report_data['ext_engine']['compartments_damage'] = 0;
												}else if($compartments->text == 'ec07p6jc'){ /*-- one or more damage --*/
													if(isset($compartments->items) && !empty($compartments->items)){
														foreach ($compartments->items as $s => $compartment) {
															if(isset($compartment->value) && $compartment->value && $compartment->value == 1){
																$report_data['ext_engine']['compartment'][] = $compartment->text;
																if(array_key_exists($compartment->text, $keyValueArr)){
																	$report_data['ext_engine']['compartment1'][] = $keyValueArr[$compartment->text];
																}
																if(isset($compartment->items) && !empty($compartment->items)){
																	foreach ($compartment->items as $pic) {
																		if($pic->type == 'picture' && (isset($pic->value) && is_array($pic->value) && !empty($pic->value))){
																			$report_data['ext_engine']['compartment_pic'][] = 'appbackend/img/' . $pic->value[0];
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}								
								}
								/*-- ETANCHÉITÉ MOTEUR* --*/
								if($engine_compartments->text == 'ec062zyl'){
									if(isset($engine_compartments->items) && !empty($engine_compartments->items)){
										foreach ($engine_compartments->items as $engine_sealings) {
											if(isset($engine_sealings->value) && $engine_sealings->value && $engine_sealings->value == 1){
												if ($engine_sealings->text == 'ec07xfhg') { /*-- no engine_sealings damage --*/
													$report_data['ext_engine']['engine_sealings_damage'] = 0;
												}else if($engine_sealings->text == 'ec11v9cm'){ /*-- one or more damage --*/
													if(isset($engine_sealings->items) && !empty($engine_sealings->items)){
														foreach ($engine_sealings->items as $s => $engine_sealing) {
															if(isset($engine_sealing->value) && $engine_sealing->value && $engine_sealing->value == 1){
																if(isset($engine_sealing->items) && !empty($engine_sealing->items)){
																	foreach ($engine_sealing->items as $key => $lf_engine_sealing) {
																		if(isset($lf_engine_sealing->value) && $lf_engine_sealing->value && $lf_engine_sealing->value == 1){
																			$report_data['ext_engine']['engine_sealing1'][$keyValueArr[$engine_sealing->text]] = $keyValueArr[$lf_engine_sealing->text];

																			// if(isset($lf_engine_sealing->items) && !empty($lf_engine_sealing->items)){
																			// 	foreach ($lf_engine_sealing->items as $i => $val) {
																			// 		if(isset($val->value) && $val->value && !empty($val->value)){
																			// 			$report_data['ext_engine']['engine_sealing'][$engine_sealing->text][$lf_engine_sealing->text] = $val->text;
																			// 			if(array_key_exists($engine_sealing->text, $keyValueArr) && array_key_exists($lf_engine_sealing->text, $keyValueArr) && array_key_exists($val->text, $keyValueArr)){
																			// 				$report_data['ext_engine']['engine_sealing1'][$keyValueArr[$engine_sealing->text]][$keyValueArr[$lf_engine_sealing->text]] = $keyValueArr[$val->text];
																			// 			}
																			// 		}
																			// 	}
																			// }
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if($ext_engine->text == 'question_engine_0'){
						if(isset($ext_engine->value) && !empty($ext_engine->value)){
							$report_data['ext_engine']['engine_compartment_pic'] = 'appbackend/img/' . $ext_engine->value[0];
						}
					}				
					/*-- NIVEAUX DES FLUIDES --*/
					if($ext_engine->text == 'question_engine_4') {
						if(isset($ext_engine->items) && !empty($ext_engine->items)){
							foreach ($ext_engine->items as $fluid_levels) {
								if(isset($fluid_levels->value) && $fluid_levels->value && $fluid_levels->value == 1){
									if ($fluid_levels->text == 'ec06ru3s') { /*-- no fluid_levels damage --*/
										$report_data['ext_engine']['fluid_levels_ok'] = 1;
									}else if($fluid_levels->text == 'ec06qfft'){ /*-- one or more damage --*/
										if(isset($fluid_levels->items) && !empty($fluid_levels->items)){
											foreach ($fluid_levels->items as $s => $fluid_level) {
												if(isset($fluid_level->value) && $fluid_level->value && $fluid_level->value == 1){
													if(isset($fluid_level->items) && !empty($fluid_level->items)){
														foreach ($fluid_level->items as $val) {
															if(isset($val->value) && $val->value && $val->value == 1){
																$report_data['ext_engine']['fluid_level'][$fluid_level->text] = $val->text;
																if(array_key_exists($val->text, $keyValueArr)){
																	$report_data['ext_engine']['fluid_level1'][$fluid_level->text] = $keyValueArr[$val->text];
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}

			/*-- ENGINE --*/
				//newly added
				// PT. Sans démonter des caches, décrivez l'état du bas moteur
				foreach ($exam_data['infra'] as $k => $infra) {
					/*-- low engines --*/
					if($infra->text == 'question_infra_1') {
						if(isset($infra->items) && !empty($infra->items)){
							foreach ($infra->items as $low_engines) {
								if(isset($low_engines->value) && $low_engines->value && $low_engines->value == 1){
									if ($low_engines->text == 'ec06s20g') {
										$report_data['infra']['low_engine_damages'] = 0;
										
									}else if($low_engines->text == 'ec07p6jc'){ 
										if(isset($low_engines->items) && !empty($low_engines->items)){
											foreach ($low_engines->items as $s => $low_engine) {
												if(isset($low_engine->value) && $low_engine->value && $low_engine->value == 1){
													if(isset($low_engine->items) && !empty($low_engine->items)){
														foreach ($low_engine->items as $val) {
															if(isset($val->value) && $val->value && $val->value == 1){
																if(array_key_exists($low_engine->text, $keyValueArr)){
																	$report_data['infra']['low_engine_damage1'][$keyValueArr[$low_engine->text]][$val->text] = $keyValueArr[$val->text];
																}
															}
															if($val->type == 'picture'){
																if(isset($val->value) && !empty($val->value)){
																	$report_data['infra']['low_engine_pic'][] = 'appbackend/img/' . $val->value[0];
																}
															}
														}
													}
												}
											}
										}
									}
									else{
										if ($low_engines->text == 'ec051wjg') { 
											if(isset($low_engines->value) && $low_engines->value && $low_engines->value == 1){
												$report_data['infra']['low_engine_damages'] = 2;
											}
										}
									}
								}
							}
						}
					}

				    // damages left side
					if($infra->text == 'question_bascaisse_left_0') {
						if(isset($infra->items) && !empty($infra->items)){
							if(isset($infra->items) && !empty($infra->items)){
								$hasPhoto = $infra->items[0]->value;
								if (is_array($hasPhoto)) {
									if (count($hasPhoto) > 0) {
										foreach ($hasPhoto as $key => $img) {
											$report_data['infra']['left_damage_pic'][] = 'appbackend/img/' . $img;
										}
									}
								}
							}
						}
					}

					// damages right side
					if($infra->text == 'question_bascaisse_right_0') {
						if(isset($infra->items) && !empty($infra->items)){
							$hasPhoto = $infra->items[0]->value;
							if (is_array($hasPhoto)) {
								if (count($hasPhoto) > 0) {
									foreach ($hasPhoto as $key => $img) {
										$report_data['infra']['right_damage_pic'][] = 'appbackend/img/' . $img;
									}
								}
							}
						}
					}

					//PT. Décrivez l’état général du châssis
					// describe the condition the chase
					if($infra->text == 'question_infra_5') {
						if(isset($infra->items) && !empty($infra->items)){
							foreach ($infra->items as $chassis) {
								// if(isset($chassis->value) && $chassis->value && $chassis->value == 1){
									if ($chassis->text == 'ec11kz4z') {
										if(isset($chassis->items) && !empty($chassis->items)){
											foreach ($chassis->items as $s => $chase) {
												if(isset($chase->value) && $chase->value && $chase->value == 1){
													if($chase->text == 'ec06j9rm'){
														if(isset($chase->items) && !empty($chase->items)){
															foreach ($chase->items as $val) {
																if(isset($val->value) && $val->value && $val->value == 1){
																	$report_data['infra']['state_cradle'][$chase->text] = $val->text;
																	if(array_key_exists($val->text, $keyValueArr)){
																		$report_data['infra']['state_cradle1'][] = $keyValueArr[$val->text];
																	}

																	if(isset($val->items) && !empty($val->items))
																	{
																		foreach ($val->items as $pic) {
		                        											if($pic->type == "picture"){
																				if(isset($pic->value) && !empty($pic->value)){

																					$report_data['infra']['chassis_pic'][] = 'appbackend/img/' . $pic->value[0];
																				}
																			}
																		}
																	}
																}
															}
														}
													}
													else if($chase->text == 'ec06s20g'){
													  	$report_data['infra']['cradle'] = 1;
													}
													else if($chase->text == 'ec051wjg') {
														$report_data['infra']['cradle'] = 2;
													}
													else if($chase->text == 'ec04uiwu') {
														$report_data['infra']['cradle'] = 3;
													}
												}
											}
										}
									}
									
									if($chassis->text == 'ec11fnyc'){ 
										if(isset($chassis->items) && !empty($chassis->items)){
											foreach ($chassis->items as $s => $chase) {
												if(isset($chase->value) && $chase->value && $chase->value == 1){
													if($chase->text == 'ec06s20g'){
													  	$report_data['infra']['spar'] = 1;
													}
													else if($chase->text == 'ec06j9rm'){
														if(isset($chase->items) && !empty($chase->items)){
															foreach ($chase->items as $val) {
																if(isset($val->value) && $val->value && $val->value == 1){
																	$report_data['infra']['state_spar'][$chase->text] = $val->text;
																	if(array_key_exists($val->text, $keyValueArr)){
																		$report_data['infra']['state_spar1'][] = $keyValueArr[$val->text];
																	}

																	if(isset($val->items) && !empty($val->items))
																	{
																		foreach ($val->items as $pic) {
		                        											if($pic->type == "picture"){
																				if(isset($pic->value) && !empty($pic->value)){

																					$report_data['infra']['chassis_pic'][] = 'appbackend/img/' . $pic->value[0];
																				}
																			}
																		}
																	}
																}
															}
														}
													}else{
														$report_data['infra']['spar'] = 2;
													}
												}
											}
										}
									}

									if($chassis->text == 'ec114152'){ 
										//echo "string";exit();
										if(isset($chassis->items) && !empty($chassis->items)){
											foreach ($chassis->items as $s => $chase) {
												if(isset($chase->value) && $chase->value && $chase->value == 1){
													if($chase->text == 'ec06s20g'){
														$report_data['infra']['floor'] = 1;
													}
													else if($chase->text == 'ec06j9rm'){
														if(isset($chase->items) && !empty($chase->items)){
															foreach ($chase->items as $val) {
																if(isset($val->value) && $val->value && $val->value == 1){
																	$report_data['infra']['state_floor'][$chase->text] = $val->text;
																	if(array_key_exists($val->text, $keyValueArr)){
																		$report_data['infra']['state_floor1'][] = $keyValueArr[$val->text];
																	}
																}

																if(isset($val->items) && !empty($val->items))
																	{
																		foreach ($val->items as $pic) {
		                        											if($pic->type == "picture"){
																				if(isset($pic->value) && !empty($pic->value)){

																					$report_data['infra']['chassis_pic'][] = 'appbackend/img/' . $pic->value[0];
																				}
																			}
																		}
																	}
															}
														}
													}else{
														$report_data['infra']['floor'] = 2;
													}
												}
											}
										}
									}
								// }
							}
						}
					}

					if($infra->text == 'question_infra_2') {
						if(isset($infra->items) && !empty($infra->items)){
							foreach ($infra->items as $exhausts) {
								if(isset($exhausts->value) && $exhausts->value && $exhausts->value == 1){
									if ($exhausts->text == 'ec07xfhg') {
										$report_data['infra']['oil_leaks'] = 0;
									}else if($exhausts->text == 'ec11v9cm'){ 
										if(isset($exhausts->items) && !empty($exhausts->items)){
											foreach ($exhausts->items as $s => $exhaust) {
												if(isset($exhaust->value) && $exhaust->value && $exhaust->value == 1){
													if(isset($exhaust->items) && !empty($exhaust->items)){
														foreach ($exhaust->items as $val) {
															if(isset($val->value) && $val->value && $val->value == 1){
																$report_data['infra']['oil_leak1'][$keyValueArr[$exhaust->text]][] = $keyValueArr[$val->text];

																// if(isset($val->items) && !empty($val->items)){
																// 	foreach ($val->items as $v) {
																// 		if(isset($v->value) && $v->value && $v->value == 1){
																// 			$precision = '';
																// 			if (isset($v->items)) {
																// 				foreach ($v->items as $k_vv => $vv) {
																// 					if ($vv->value == 1) {
																// 						$precision = ' ('.$keyValueArr[$vv->text].') ';
																// 					}
																// 				}
																// 			}

																// 			$report_data['infra']['oil_leak'][$exhaust->text] = $val->text;
																// 			if(array_key_exists($val->text, $keyValueArr)){
																// 				$report_data['infra']['oil_leak1'][$exhaust->text][$keyValueArr[$val->text]][] = $keyValueArr[$v->text].$precision;
																// 			}

																// 		}
																// 	}	
																// }	
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}

						// On compte le nombre de fuites pour voir si le tableau tient dans une page
						$i = 0;
						foreach ($report_data['infra']['oil_leak1'] as $key => $ol) {
							foreach ($ol as $k => $o) {
								$i = $i + 2;
							}
						}
						$report_data['infra']['oil_leak1_count'] = $i;
						// -- fin du compteur --
					}

					//PT. Décrivez l’état et la fixation du système d’échappement &amp; du pot catalytique
					// exhaust system
					// echo "<pre>"; print_r($infra); echo "</pre>";
					if($infra->text == 'question_infra_8') {
						if(isset($infra->items) && !empty($infra->items)){
							foreach ($infra->items as $exhausts) {
								// if(isset($exhausts->value) && $exhausts->value && $exhausts->value == 1){
									if($exhausts->text == 'ec11iqbv'){ 
										if(isset($exhausts->items) && !empty($exhausts->items)){
											foreach ($exhausts->items as $s => $exhaust) {
												if(isset($exhaust->value) && $exhaust->value && $exhaust->value == 1){
													if($exhaust->text == 'ec06s20g'){
													  	$report_data['infra']['manifolds'] = 1;
													}
													else if($exhaust->text == 'ec06j9rm'){
														if(isset($exhaust->items) && !empty($exhaust->items)){
															foreach ($exhaust->items as $myKey => $val) {
																if(isset($val->value) && $val->value && $val->value == 1){

																	if (isset($val->items)) {
																		$report_data['infra']['manifold1'][$myKey]['titre'] = $keyValueArr[$val->text];
																		foreach ($val->items as $myKey2 => $sousitems) {
																			if ($sousitems->value == 1) {
																				$report_data['infra']['manifold1'][$myKey]['values'][] = $keyValueArr[$sousitems->text];
																			}
																			if ($sousitems->type == 'picture' && count($sousitems->value) > 0) {
																				foreach ($sousitems->value as $myKey3 => $img) {
																					$report_data['infra']['manifold1_pic'][] = 'appbackend/img/' . $img;
																					$report_data['infra']['manifold1_pic_soustitre'] = $keyValueArr[$val->text];
																				}
																			}
																		}
																	}

																	// $report_data['infra']['manifold'][$exhaust->text][] = $val->text;
																	// if(array_key_exists($val->text, $keyValueArr)){
																	// 	$report_data['infra']['manifold1'][] = $keyValueArr[$val->text];
																	// }
																}
															}
														}
													}
												}
											}
										}
									}

									if($exhausts->text == 'ec110mbx'){
									    if(isset($exhausts->items) && !empty($exhausts->items)){
											foreach ($exhausts->items as $s => $exhaust) {
												if(isset($exhaust->value) && $exhaust->value && $exhaust->value == 1){
													if($exhaust->text == 'ec06s20g'){
														$report_data['infra']['lines'] = 1;
													}
													else if($exhaust->text == 'ec06j9rm'){
														if(isset($exhaust->items) && !empty($exhaust->items)){
															foreach ($exhaust->items as $val) {
																if(isset($val->value) && $val->value && $val->value == 1){
																	$report_data['infra']['line'][$exhaust->text] = $val->text;
																	if(array_key_exists($val->text, $keyValueArr)){
																		$report_data['infra']['line1'][$exhaust->text][] = $keyValueArr[$val->text];
																	}

																	if (isset($val->items)) {
																		foreach ($val->items as $picture_k => $picture) {
																			if (isset($picture->value) && $picture->type == "picture") {
																				foreach ($picture->value as $img_k => $img) {
																					$report_data['infra']['line1_pic'][] = "appbackend/img/" . $img;
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}

									if($exhausts->text == 'ec11971z'){ 
										if(isset($exhausts->items) && !empty($exhausts->items)){
											foreach ($exhausts->items as $s => $exhaust) {
												if(isset($exhaust->value) && $exhaust->value && $exhaust->value == 1){
													if($exhaust->text == 'ec06s20g'){
													  	$report_data['infra']['quiets'] = 1;
													}
													else if($exhaust->text == 'ec06j9rm'){
														if(isset($exhaust->items) && !empty($exhaust->items)){
															foreach ($exhaust->items as $val) {
																if(isset($val->value) && $val->value && $val->value == 1){
																	$report_data['infra']['quiet'][$exhaust->text] = $val->text;
																	if(array_key_exists($val->text, $keyValueArr)){
																		$report_data['infra']['quiet1'][] = $keyValueArr[$val->text];
																	}

																	if (isset($val->items)) {
																		foreach ($val->items as $picture_k => $picture) {
																			if (isset($picture->value) && $picture->type == "picture") {
																				foreach ($picture->value as $img_k => $img) {
																					$report_data['infra']['quiet1_pic'][] = "appbackend/img/" . $img;
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								// }
							}
						}
					}
					
					if(isset($infra->items) && !empty($infra->items)){
						foreach ($infra->items as $front) {
							if($front->text == 'question_infra_0') {
								if($front->type == 'picture') {
									if(isset($front->value) && !empty($front->value)){
										$report_data['infra']['front_pic'] = 'appbackend/img/' . $front->value[0];
									}
								}
							}

							if($front->text == 'question_infra_0b') {
								if($front->type == 'picture') {
									if(isset($front->value) && !empty($front->value)){
										$report_data['infra']['rear_pic'] = 'appbackend/img/' . $front->value[0];
									}
								}
							}
						}
					}
				}

				// victor
				foreach ($report_data['infra']['manifold1'] as $key => $item) {
					if (isset($item['titre']) && !isset($item['values'])) {
						$report_data['infra']['manifold1'][$key]['titre'] = '-';
						$report_data['infra']['manifold1'][$key]['values'][] = $item['titre'];
					}
				}

				// -- autre partie

				$report_data['victor']['infra']['chassis']['pic'] = array();
				if ($report_data['infra']['front_pic']) {
					$report_data['victor']['infra']['chassis']['pic'][] = array(
						"image"	=> $report_data['infra']['front_pic'],
						"soustitre" => "front_pic"
					);
				}
				if ($report_data['infra']['rear_pic']) {
					$report_data['victor']['infra']['chassis']['pic'][] = array(
						"image"	=> $report_data['infra']['rear_pic'],
						"soustitre" => "rear_pic"
					);
				}
				if ($report_data['infra']['chassis_pic']) {
					foreach ($report_data['infra']['chassis_pic'] as $key => $value) {
						$report_data['victor']['infra']['chassis']['pic'][] = array(
							"image"	=> $value,
							"soustitre" => "chassis_pic"
						);
					}
				}

			// MÉCANIQUE DU VÉHICULE (3)
				foreach ($exam_data['startengine'] as $engines) {
					if($engines->text == 'question_startengine_1'){ 
						if(isset($engines->items) && !empty($engines->items)){
							foreach ($engines->items as $engine) {
								if($engine->value && $engine->value == 1){
									if ($engine->text == 'ec26sdg6') {
										$report_data['startengine']['engine_start'] = 0;

									}
									if ($engine->text == 'ec38rf32') {
										$report_data['startengine']['intensity_good'] = 0;

									}
									if(isset($engine->items) && !empty($engine->items)){
										foreach ($engine->items as  $val) {
											if(isset($val->value) && $val->value && $val->value == 1){
												$report_data['startengine']['intensity_low_value'][$engine->text] = $val->text;
												if(array_key_exists($val->text, $keyValueArr)){
													$report_data['startengine']['intensity_low_value1'][$engine->text][$keyValueArr[$engine->text]] = $keyValueArr[$val->text];
												}
											}
										}
									}
									
								}
							}
						}
					}

					if($engines->text == 'question_startengine_3'){
						if(isset($engines->items) && !empty($engines->items)){
							foreach ($engines->items as $engine) {
								if($engine->value && $engine->value == 1){
									if($engine->text == 'ec388lys'){ 
										$report_data['startengine']['engine_abnormal_noise'] = 0;
									}
									if($engine->text == 'ec38q52u'){ 	
										if(isset($engine->items) && !empty($engine->items)){
											foreach ($engine->items as $noise) {
												if(isset($noise->value) && $noise->value && $noise->value == 1){
													$report_data['startengine']['engine_noise_value'][$engine->text] = $noise->text;
													if(array_key_exists($noise->text, $keyValueArr)){
														$report_data['startengine']['engine_noise_value1'][$engine->text][] = $keyValueArr[$noise->text];
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if($engines->text == 'question_startengine_9'){ 
						if(isset($engines->items) && !empty($engines->items)){
							foreach ($engines->items as $engine) {
								if($engine->value && $engine->value == 1){
									if($engine->text == 'ec38htk1'){ 
										$report_data['startengine']['exhaust_abnormal_noise'] = 0;
									}
									if($engine->text == 'ec381w5c'){ 	
										if(isset($engine->items) && !empty($engine->items)){
											foreach ($engine->items as $noise) {
												if(isset($noise->value) && $noise->value && $noise->value == 1){
													$report_data['startengine']['exhaust_noise_value'][$engine->text] = $noise->text;
													if(array_key_exists($noise->text, $keyValueArr)){
														$report_data['startengine']['exhaust_noise_value1'][$engine->text][] = $keyValueArr[$noise->text];
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if($engines->text == 'question_startengine_10'){ 
						if(isset($engines->items) && !empty($engines->items)){
							foreach ($engines->items as $engine) {
								if($engine->value && $engine->value == 1){
									if($engine->text == 'ec38oeny'){ 
										$report_data['startengine']['exhaust_abnormal_smoke'] = 0;
									}
									if($engine->text == 'ec38yf9n'){ 
										$report_data['startengine']['exhaust_abnormal_smoke'] = 1;	
										if(isset($engine->items) && !empty($engine->items)){
											foreach ($engine->items as $smoke) {
												if(isset($smoke->value) && $smoke->value && $smoke->value == 1) {
													$complement = '';
													if(isset($smoke->items) && !empty($smoke->items)){
														foreach ($smoke->items as $smk) {
															if(isset($smk->value) && $smk->value && $smk->value == 1){
																$complement = '. '.$keyValueArr[$smk->text].'.';

																// $report_data['startengine']['abnormal_smoke_value'][$engine->text] = $smk->text;
																// if(array_key_exists($smk->text, $keyValueArr)){
																// 	$report_data['startengine']['abnormal_smoke_value1'][$keyValueArr[$smoke->text]][] = $keyValueArr[$smk->text];
																// }
															}
														}
													}
													$report_data['startengine']['abnormal_smoke_value1'][] = $keyValueArr[$smoke->text].$complement;
												}
											}
										}
									}
								}
							}
						}
					}

					
					// if($engines->text == 'question_startengine_8'){ 
					// 	if(isset($engines->items) && !empty($engines->items)){
					// 		foreach ($engines->items as $engine) {
					// 			if(isset($engine->items) && !empty($engine->items)){
					// 				foreach ($engine->items as $stalls) {
					// 					// if(isset($smoke->value) && $smoke->value && $smoke->value == 1){
					// 						if ($stalls->text == 'ec384j7n') {
					// 							foreach ($stalls->items as $stall) {
					// 								if(isset($stall->value) && $stall->value && $stall->value == 1){
					// 									if(array_key_exists($stall->text, $keyValueArr)){
					// 										$report_data['startengine']['car_stalls1'][$stall->text] = $keyValueArr[$stall->text];
															
					// 									}
					// 								}
					// 							}
					// 						}	
					// 				   //} 
					// 				}
					// 			}
					// 		}	
					// 	}
					// }
				}

			// Essais routiers - suite
				foreach ($exam_data['essais_routier'] as $k => $essais_routier) {
					// PT. En mouvement, comment se déroule le passage des rapports (transmission manuelle)
					if($essais_routier->text == 'question_essais_routier_8'){
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $defects) {
								if ($defects->text == 'ec07t05z') {
									if(isset($defects->value) && !empty($defects->value)){
										$report_data['essais_routier']['manual_defects'] = 0;
									}
								}else if($defects->text == 'ec07bh8b'){
									if(isset($defects->value) && !empty($defects->value)){
										if($defects->value && $defects->value == 1){
											if(isset($defects->items) && !empty($defects->items)){
												foreach ($defects->items as $b => $brake_defect) {
													if($brake_defect->value && $brake_defect->value == 1){
														$precision = '';
														if (isset($brake_defect->items)) {
															foreach ($brake_defect->items as $k_ite => $ite) {
																if ($ite->value == 1) {
																	$precision = ' ('. $keyValueArr[$ite->text] .')';
																}
															}
														}
														$report_data['essais_routier']['manual_defects1'][$b] = $keyValueArr[$brake_defect->text] . $precision;
													}
												}
											}
										}
									}
								}
							}
						}
					}
					// PT. En mouvement, comment se déroule le passage des rapports (transmission automatique)
					if($essais_routier->text == 'question_essais_routier_9'){
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $defects) {
								if($defects->value && $defects->value == 1){
									if ($defects->text == 'ec07t05z') {
										$report_data['essais_routier']['automatic_defects'] = 0;
									}else if($defects->text == 'ec07bh8b'){
										//echo "string";exit();
										if(isset($defects->items) && !empty($defects->items)){
											foreach ($defects->items as $b => $brake_defect) {
												if($brake_defect->value && $brake_defect->value == 1){
													$report_data['essais_routier']['automatic_defects1'][$b] = 
													$keyValueArr[$brake_defect->text];
												}
											}
										}
									}
								}
							}
						}
					}
					// PT. « Décrivez le comportement à l’accélération »
					if($essais_routier->text == 'question_essais_routier_0'){
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $acceleration) {
								if($acceleration->value && $acceleration->value == 1){
									if ($acceleration->text == 'ec04krl6') {
										$report_data['essais_routier']['acceleration_behavior'] = 0;
									}else if($acceleration->text == 'ec04imdk'){
										if(isset($acceleration->items) && !empty($acceleration->items)){
											foreach ($acceleration->items as $b => $val) {
												if($val->value && $val->value == 1){
													$report_data['essais_routier']['acceleration_behavior1'][$acceleration->text] = $val->text;
													if(array_key_exists($val->text, $keyValueArr)){
														$report_data['essais_routier']['acceleration_behavior2'][$val->text][] = $keyValueArr[$val->text];
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}

			// Carrosserie
				foreach ($exam_data['carrosserie'] as $k => $carrosserie) {
					//PT. Décrivez l’état de la carrosserie face avant
					if($carrosserie->text == 'ec12nldd') {
						$hasPhoto = $carrosserie->items[0]->value;
						if (is_array($hasPhoto)) {
							if (count($hasPhoto) > 0) {
								foreach ($hasPhoto as $key => $img) {
									$report_data['carrosserie']['bumper_pic'][] = 'appbackend/img/' . $img;
								}
							}
						}
					}

					// PT. Décrivez l’état de la carrosserie côté gauche
					if($carrosserie->text == 'ec12g4u2') {
						$hasPhoto = $carrosserie->items[0]->value;
						if (is_array($hasPhoto)) {
							if (count($hasPhoto) > 0) {
								foreach ($hasPhoto as $key => $img) {
									$report_data['carrosserie']['left_front_wing_pic'][] = 'appbackend/img/' . $img;
								}
							}
						}
					}

					//PT. Décrivez l’état de la carrosserie face arrière
					if($carrosserie->text == 'ec12s83p') {
						$hasPhoto = $carrosserie->items[0]->value;
						if (is_array($hasPhoto)) {
							if (count($hasPhoto) > 0) {
								foreach ($hasPhoto as $key => $img) {
									$report_data['carrosserie']['rear_body_damages_pic'][] = 'appbackend/img/' . $img;
								}
							}
						}
					}

					//PT. Décrivez l’état de la carrosserie côté droit
					if($carrosserie->text == 'ec12lxr8') {
						$hasPhoto = $carrosserie->items[0]->value;
						if (is_array($hasPhoto)) {
							if (count($hasPhoto) > 0) {
								foreach ($hasPhoto as $key => $img) {
									$report_data['carrosserie']['right_body_pic'][] = 'appbackend/img/' . $img;
								}
							}
						}
					}

					//PT. Décrivez l’état du toit (pavillon)
					if($carrosserie->text == 'question_roof_1') {
						$hasPhoto = $carrosserie->items[1]->value;
						if (is_array($hasPhoto)) {
							if (count($hasPhoto) > 0) {
								$report_data['carrosserie']['roof_damage'] = 1;
								foreach ($hasPhoto as $key => $img) {
									$report_data['carrosserie']['roof_damage_pic'][] = 'appbackend/img/' . $img;
								}
							}
						}
						if ($carrosserie->items[0]->value) {
							// Si on a coché que c'est non concerné car décapotable, alors c'est non concerné !
							$report_data['carrosserie']['roof_damage'] = 0;
						}
					}

					//PT. Décrivez l’état de la capote
					if($carrosserie->text == 'question_roof_4') {
						if(isset($carrosserie->items) && !empty($carrosserie->items)){
							foreach ($carrosserie->items as $avants) {
								if(isset($avants->value) && $avants->value && $avants->value == 1){
									if ($avants->text == 'ec06s20g') { 
										$report_data['carrosserie']['hood_damage'] = 0;
									}else if($avants->text == 'ec06j9rm'){ /*-- one or more damage --*/
										if(isset($avants->items) && !empty($avants->items)){
											foreach ($avants->items as $s => $avant) {
												if(isset($avant->value) && $avant->value && $avant->value == 1){
													$report_data['carrosserie']['hood_damages'][$avant->text] = $value->text;
													if(array_key_exists($avant->text, $keyValueArr)){
														$report_data['carrosserie']['hood_damages1'][$avant->text] = $keyValueArr[$avant->text];
													}
												    
													if(isset($avant->items) && !empty($avant->items)){
												      foreach ($avant->items as $pic) {
												      		if($pic->type == "picture"){
																if(isset($pic->value) && !empty($pic->value)){
																	foreach ($pic->value as $img_k => $img) {
																		$report_data['carrosserie']['hood_damages_pic'][] = 'appbackend/img/' . $img;
																	}
																}
															}
														}
													}
													//print_r($report_data['carrosserie']['hood_damages_pic']);exit();
												}
											}
										}
									}else{
										$report_data['carrosserie']['hood_damage'] = 1;
									}
								}
							}
						}
					}
				}

			// ETAT DES ÉLÉMENTS DE CARROSSERIE
				foreach ($exam_data['inner'] as $inners) {
					// PT. Décrivez le fonctionnement de la capote
					if($inners->text == 'examchapter_inner_14') {
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $dysfunctions) {
								if(isset($dysfunctions->value) && $dysfunctions->value && $dysfunctions->value == 1){
									if ($dysfunctions->text == 'ec04mkre') { 
										$report_data['inners']['dysfunction'] = 0;
									}else if($dysfunctions->text == 'ec04365g'){ 
										//print_r($dysfunctions->items);exit;
										if(isset($dysfunctions->items) && !empty($dysfunctions->items)){
											foreach ($dysfunctions->items as $s => $dysfunction) {
												if(isset($dysfunction->value) && $dysfunction->value && $dysfunction->value == 1){
													if(isset($dysfunction->items) && !empty($dysfunction->items)){
														foreach ($dysfunction->items as $val) {
															if(isset($val->value) && $val->value && $val->value == 1){
																$report_data['inners']['dysfunction_damages'][$dysfunction->text] = $val->text;
																if(array_key_exists($val->text, $keyValueArr)){
																	$report_data['inners']['dysfunction_damages1'][$keyValueArr[$dysfunction->text]] = $keyValueArr[$val->text];
																}
															}
														}
													}
												}
											}
										}
									}else{
										$report_data['inners']['dysfunction'] = 1;
									}
								}
							}
						}
					}

					if($inners->text == 'question_ouvrants_0') {
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $dysfunctions) {
								if(isset($dysfunctions->value) && $dysfunctions->value && $dysfunctions->value == 1){
									if ($dysfunctions->text == 'ec04mkre') { 
										$report_data['inners']['malfunction'] = 0;
									}else if($dysfunctions->text == 'ec04365g'){ 
										if(isset($dysfunctions->items) && !empty($dysfunctions->items)){
											foreach ($dysfunctions->items as $s => $dysfunction) {
												if(isset($dysfunction->value) && $dysfunction->value && $dysfunction->value == 1){
													if(isset($dysfunction->items) && !empty($dysfunction->items)){
														foreach ($dysfunction->items as $val) {
															if(isset($val->value) && $val->value && $val->value == 1){
																$report_data['inners']['malfunctions'][$dysfunction->text] = $val->text;
																if(array_key_exists($val->text, $keyValueArr)){
																	$report_data['inners']['malfunction1'][$dysfunction->text][] = $keyValueArr[$val->text];
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					// PT. Décrivez le fonctionnement des vitres avant et arrières
					if($inners->text == 'examchapter_inner_13') {
						// echo "<pre>"; print_r($inners); echo "</pre>";
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $dysfunctions) {
								if(isset($dysfunctions->value) && $dysfunctions->value && $dysfunctions->value == 1){
									if ($dysfunctions->text == 'ec04mkre') { /*-- no dysfunctions damage --*/
										$report_data['inners']['rear_malfunction'] = 0;
									}else if($dysfunctions->text == 'ec04365g'){ /*-- one or more damage --*/
										if(isset($dysfunctions->items) && !empty($dysfunctions->items)){
											//print_r($dysfunctions->items);exit();
											foreach ($dysfunctions->items as $s => $dysfunction) {
												if(isset($dysfunction->value) && $dysfunction->value && $dysfunction->value == 1){
													if(isset($dysfunction->items) && !empty($dysfunction->items)){
														foreach ($dysfunction->items as $key => $dys) {
															if(isset($dys->value) && $dys->value && $dys->value == 1)
															{
															    //if(array_key_exists($dys->text, $keyValueArr))
																// {
																// 	$report_data['inners']['rear_malfunctions2'][$dys->text] = $keyValueArr[$dys->text];
																// }

																$complement = '';
																$ii = 0;
																foreach ($dys->items as $k => $val) {
																	if(isset($val->value) && $val->value && $val->value == 1){
																		// echo "<pre>"; print_r($val); echo "</pre>";
																		
																		$complement .= $keyValueArr[$val->text];

																	}
																}
																if ($complement != '') {
																	$complement = '. '.$complement.'.';
																}
																$report_data['inners']['rear_malfunctions1'][$keyValueArr[$dysfunction->text]][$keyValueArr[$dys->text]] = $keyValueArr[$dys->text].$complement;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}


					// PT. Décrivez l’état du levier de vitesse, de la console centrale et des plastiques de tableau de bord
					if($inners->text == 'examchapter_inner_19') {
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $stains) {
								if(isset($stains->value) && $stains->value && $stains->value == 1){
									if ($stains->text == 'ec05ec0o') { /*-- no stains damage --*/
										$report_data['inners']['stain'] = 0;
									}else if($stains->text == 'ec05sccc'){ /*-- one or more damage --*/
										if(isset($stains->items) && !empty($stains->items)){
											foreach ($stains->items as $s => $stain) {
												if(isset($stain->value) && $stain->value && $stain->value == 1){
													if(isset($stain->items) && !empty($stain->items)){
														foreach ($stain->items as $key => $dys) {
															if(isset($dys->value) && $dys->value && $dys->value == 1)
															{
															    foreach ($dys->items as $k => $val) {
																	//if(isset($val->value) && $val->value && $val->value == 1){
																		if(array_key_exists($stain->text, $keyValueArr) && array_key_exists($val->text, $keyValueArr) && array_key_exists($val->text, $keyValueArr))
																		{
																			$report_data['inners']['stain1'][$keyValueArr[$stain->text]][$keyValueArr[$dys->text]] = $keyValueArr[$dys->text];
																		}
																		
																		if($val->type == "picture"){
																			if(isset($val->value) && !empty($val->value)){
																				foreach ($val->value as $img_k => $img) {
																					$report_data['inners']['stain_pic'][] = 'appbackend/img/' . $img;
																				}
																		    }
																		}
																		
																	//}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}


					if($inners->text == 'examchapter_inner_0') {
						if($inners->type == 'picture') {
							if(isset($inners->value) && !empty($inners->value)){
								$report_data['inners']['compartment_pic'][] = 'appbackend/img/' . $inners->value[0];
							}
						}
					}

					if($inners->text == 'question_hab_back_0') {
						if($inners->type == 'picture') {
							if(isset($inners->value) && !empty($inners->value)){
								$report_data['inners']['rear_cabin_pic'][] = 'appbackend/img/' . $inners->value[0];
							}
						}
					}

					//PT. Dans l’habitacle y-a- t-il une odeur marquée désagréable
					if($inners->text == 'examchapter_inner_3') {
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $compartments) {
								if(isset($compartments->value) && $compartments->value && $compartments->value == 1){
									if ($compartments->text == 'examglobal_no') { 
										$report_data['inners']['compartment'] = 0;
									}else if($compartments->text == 'examglobal_yes'){ 
									    //print_r($compartments->items);exit();
										if(isset($compartments->items) && !empty($compartments->items)){
											foreach ($compartments->items as $s => $compartment) {
												if(isset($compartment->value) && $compartment->value && $compartment->value == 1){
													$report_data['inners']['compartment'][$compartment->text] = $value->text;
													if(array_key_exists($compartment->text, $keyValueArr)){
														$report_data['inners']['compartment1'][$compartment->text] = $keyValueArr[$compartment->text];
													}
												}
											}
										}
									}
								}
							}
						}
					}

					//PT. Décrivez le fonctionnement de la fermeture centralisée des portières et du coffre/hayon
					if($inners->text == 'examchapter_inner_1') {
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $doors) {
								if(isset($doors->value) && $doors->value && $doors->value == 1){
									if ($doors->text == 'ec04mkre') { 
										$report_data['inners']['dys_function'] = 0;
									}else if($doors->text == 'ec04365g'){ 
									   if(isset($doors->items) && !empty($doors->items)){
									   	    foreach ($doors->items as $s => $door) {
												if(isset($door->value) && $door->value && $door->value == 1){
													if ($door->text == 'ec0461m3'){ 
														$report_data['inners']['no_opening'] = 0;
													}
													if(isset($door->items) && !empty($door->items)){
														foreach ($door->items as $key => $dr) {
															if(isset($dr->value) && $dr->value && $dr->value == 1)
															{
															    if(array_key_exists($door->text, $keyValueArr) && array_key_exists($dr->text, $keyValueArr))
																{
																	$report_data['inners']['lock_doors1'][$keyValueArr[$door->text]] = $keyValueArr[$dr->text];
																}
																	
															}
														}
													}
												}
											}
										}
									}
									else if($doors->text == 'ec04975g') { 
										$report_data['inners']['concerned'] = 0;
									}
									else{ 
										$report_data['inners']['observation'] = 0;
									}
								}
							}
						}
					}
					//PT. Décrivez le fonctionnement des sièges avant chauffants
					if($inners->text == 'examchapter_inner_9') {
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $seats) {
								if(isset($seats->value) && $seats->value && $seats->value == 1){
									if ($seats->text == 'ec04mkre') {
										// Aucun dysfonctionnement constaté 
										$report_data['inners']['seats_malfunction'] = 0;
									}else if($seats->text == 'ec04365g'){ 
										// Un ou plusieurs dysfonctionnements constatés
									   if(isset($seats->items) && !empty($seats->items)){
									   	    foreach ($seats->items as $s => $seat) {
												// if(isset($seat->value) && $seat->value && $seat->value == 1){
													if(isset($seat->items) && !empty($seat->items)){
														foreach ($seat->items as $key => $st) {
															if(isset($st->value) && $st->value && $st->value == 1)
															{
															    if(array_key_exists($seat->text, $keyValueArr) && array_key_exists($st->text, $keyValueArr))
																{
																	$report_data['inners']['seats_malfunction1'][$keyValueArr[$seat->text]] = $keyValueArr[$st->text];
																}
																	
															}
														}
													}
												// }
											}
										}
									}
									else if($seats->text == 'ec04975g') { 
										// Non concerné
										$report_data['inners']['seats_concerned'] = 0;
									}
								}
							}
						}
					}

					//PT. Décrivez le fonctionnement du volant chauffant
					if($inners->text == 'examchapter_inner_10') {
						if(isset($inners->items) && !empty($inners->items)){
							foreach ($inners->items as $wheels) {
								if(isset($wheels->value) && $wheels->value && $wheels->value == 1){
									if ($wheels->text == 'ec04mkre') { 
										$report_data['inners']['wheels_malfunction'] = 0;
									}else if($wheels->text == 'ec04365g'){ 
										if(isset($wheels->items) && !empty($wheels->items)){
									   	    foreach ($wheels->items as $s => $wheel) {
												if(isset($wheel->value) && $wheel->value && $wheel->value == 1){
													if(array_key_exists($wheel->text, $keyValueArr))
													{
														$report_data['inners']['wheels_malfunction1'] = $keyValueArr[$wheel->text];
													}
												}
											}
										}
									}
									else { 
										$report_data['inners']['wheels_concerned'] = 0;
									}
								}
							}
						}
					}
				}

				// PT. Décrivez l’ajustement/alignement des portières, du coffre/hayon etc
				foreach ($exam_data['ajustements'] as $ajustements) {
					if($ajustements->text == 'question_ajustements_0') {
						if(isset($ajustements->items) && !empty($ajustements->items)){
							foreach ($ajustements->items as $adjust) {
								if(isset($adjust->value) && $adjust->value && $adjust->value == 1){
									if ($adjust->text == 'ec04w6a1') { 
										$report_data['ajustements']['alignement'] = 0;
									}else if($adjust->text == 'ec06t3lq'){ /*-- one or more damage --*/
										if(isset($adjust->items) && !empty($adjust->items)){
											foreach ($adjust->items as $s => $align) {
												if(isset($align->value) && $align->value && $align->value == 1){
													if(isset($align->items) && !empty($align->items)){
														foreach ($align->items as $val) {
														
															$report_data['ajustements']['alignements'][$align->text] = $val->text;
															if(array_key_exists($val->text, $keyValueArr)){
																$report_data['ajustements']['alignements1'][$align->text] = $keyValueArr[$val->text];
															}
															if($val->type == "picture"){
																if(isset($val->value) && !empty($val->value)){

																	$report_data['ajustements']['alignements_pic'][] = 'appbackend/img/' . $val->value[0];
																}
															}
														    //print_r($report_data['ajustements']['alignements_pic']);exit();
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}				

				// PT. Décrivez l’état de l’ensemble du vitrage
				foreach ($exam_data['vitrage'] as $vitrage) {
					if($vitrage->text == 'question_vitrage_0') {
						if(isset($vitrage->items) && !empty($vitrage->items)){
							foreach ($vitrage->items as $shields) {
								if(isset($shields->value) && $shields->value && $shields->value == 1){
									if ($shields->text == 'ec06s20g') { 
										$report_data['vitrage']['windshield_damage'] = 0;
									}else if($shields->text == 'ec06j9rm'){ /*-- one or more damage --*/
										//print_r($shields->items);exit;
										if(isset($shields->items) && !empty($shields->items)){
											foreach ($shields->items as $s => $shield) {
												if(isset($shield->value) && $shield->value && $shield->value == 1){
													if(isset($shield->items) && !empty($shield->items)){
														foreach ($shield->items as $val) {															
															if(isset($val->value) && $val->value && $val->value == 1){
																$report_data['vitrage']['windshield_damages'][$shield->text] = $val->text;
																if(array_key_exists($val->text, $keyValueArr)){
																	$report_data['vitrage']['windshield_damages1'][$shield->text][] = $keyValueArr[$val->text];
																}
																if(isset($val->items) && !empty($val->items)){
																	foreach ($val->items as $pic) {
	                        											if($pic->type == "picture"){
																			if(isset($pic->value) && !empty($pic->value)){
																				$report_data['vitrage']['windshield_pic'][] = 'appbackend/img/' . $pic->value[0];
																			}
																		}
																	}
																}

																// Ligne pare-brise . Si Impact ou Fissure, alors on a 3 choix supplémentaires.
																if ($shield->text == "ec042z95" && ($val->text == "ec06kang" || $val->text == "ec12wyy8")) {
																	if (isset($val->items)) {
																		foreach ($val->items as $precision_item_k => $precision_item) {
																			foreach ($precision_item->items as $precision_k => $precision) {
																				if ($precision->value == 1) {
																					$report_data['vitrage']['windshield_damages1_par_brise_precision'][$shield->text][ $keyValueArr[$val->text] ][$precision_item->text] = $precision->text;
																				}
																			}
																		}
																	}
																}

															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}

				foreach ($exam_data['startengine'] as $i => $engine) {
					// PT. Décrivez le fonctionnement de l’ordinateur de bord
					if($engine->text == 'question_startengine_4'){ 
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $boards) {
								if($boards->value && $boards->value == 1){
									if ($boards->text == 'ec04mkre') {
										$report_data['startengine']['mal_function'] = 0;								
									}else if($boards->text == 'ec04365g'){ 
										if(isset($boards->items) && !empty($boards->items)){
									   	    foreach ($boards->items as $s => $brd) {
												if(isset($brd->value) && $brd->value && $brd->value == 1){
													if(array_key_exists($brd->text, $keyValueArr))
													{
														$report_data['startengine']['boards_malfunction'][] = $keyValueArr[$brd->text];
													}
												}
											}
										}
									}
									else { 
										$report_data['startengine']['boards_concerned'] = 0;
									}
								}
							}
						}
					}

					//  PT. Décrivez le fonctionnement du chauffage
					if($engine->text == 'question_startengine_5'){ 
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $heats) {
								if($heats->value && $heats->value == 1){
									if ($heats->text == 'ec04mkre') {
										$report_data['startengine']['heat_function'] = 0;								
									}else if($heats->text == 'ec38hx51'){ 
										$report_data['startengine']['heats_malfunction'] = $keyValueArr[$heats->text];
									}else { 
										$report_data['startengine']['heats_concerned'] = 0;
									}
								}
							}
						}
					}

					//  PT. Décrivez le fonctionnement de la climatisation
					if($engine->text == 'question_startengine_6'){ 
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $climates) {
								if($climates->value && $climates->value == 1){
									if ($climates->text == 'ec04mkre') {
										$report_data['startengine']['climate_function'] = 0;								
									}else if($climates->text == 'ec04365g'){ 
										if(isset($climates->items) && !empty($climates->items)){
									   	    foreach ($climates->items as $s => $cli) {
												if(isset($cli->value) && $cli->value && $cli->value == 1){
													if(array_key_exists($cli->text, $keyValueArr))
													{
														$report_data['startengine']['climate_function1'][] = $keyValueArr[$cli->text];
													}
												}
											}
										}
									}
									else { 
										$report_data['startengine']['climate_concerned'] = 0;
									}
								}
							}
						}
					}

					if($engine->text == 'question_essais_routier_16'){
						if(isset($engine->items) && !empty($engine->items)){
							foreach ($engine->items as $cameras) {
								if($cameras->value && $cameras->value == 1){
									if ($cameras->text == 'ec04mkre') {
										$report_data['engine']['camera_malfunction'] = 0;								
									}else if($cameras->text == 'ec04365g'){ 
										if(isset($cameras->items) && !empty($cameras->items)){
									   	    foreach ($cameras->items as $s => $cam) {
												if(isset($cam->value) && $cam->value && $cam->value == 1){
													if(array_key_exists($cam->text, $keyValueArr))
													{
														$report_data['engine']['camera_malfunction1'][] = $keyValueArr[$cam->text];
													}
												}
											}
										}
									}
									else { 
										$report_data['engine']['camera_concerned'] = 0;
									}
								}
							}
						}
					}							
				}

			// Essais routiers - suite
				foreach ($exam_data['essais_routier'] as $k => $essais_routier) {

					//  PT. Vérifiez l’absence de bruits anormaux lors de virages serrés et lors de décélérations et accélérations en ligne droite.
					if($essais_routier->text == 'question_essais_routier_7'){ 
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $routier) {
								if($routier->value && $routier->value == 1){
									if($routier->text == 'ec388lys'){ 
										$report_data['essais_routier']['acceleration_abnormal_noise'] = 0;
									}
									if($routier->text == 'ec38q52u'){ 	
										if(isset($routier->items) && !empty($routier->items)){
											foreach ($routier->items as $noise) {
												if(isset($noise->value) && $noise->value && $noise->value == 1){
													
													$report_data['essais_routier']['acceleration_noise_value'][$routier->text] = $noise->text;
													if(array_key_exists($noise->text, $keyValueArr)){
														$report_data['essais_routier']['acceleration_noise_value1'][$routier->text][] = $keyValueArr[$noise->text];
													}
												}
											}
										}
									}
								}
							}
						}
					}


					if($essais_routier->text == 'question_essais_routier_1'){
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $turbo) {
								if($turbo->value && $turbo->value == 1){
									if(array_key_exists($turbo->text, $keyValueArr))
									{
										$report_data['essais_routier']['turbo1'][] = $keyValueArr[$turbo->text];
									}
								}
							}
						}
					}

					// PT. décrivez le fonctionnement du régulateur de vitesse
					if($essais_routier->text == 'question_essais_routier_2'){
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $regulator) {
								if($regulator->value && $regulator->value == 1){
									if(array_key_exists($regulator->text, $keyValueArr))
									{
										$report_data['essais_routier']['regulator'][] = $keyValueArr[$regulator->text];
									}
								}
							}
						}
					}

					if($essais_routier->text == 'question_essais_routier_3'){
						if(isset($essais_routier->items) && !empty($essais_routier->items)){
							foreach ($essais_routier->items as $compteur) {
								if($compteur->value && $compteur->value == 1){
									if(array_key_exists($compteur->text, $keyValueArr))
									{
										$report_data['essais_routier']['compteur'][] = $keyValueArr[$compteur->text];
									}
								}
							}
						}
					}
				}


			// Notes - page 2
				$eval = [];
				$r = $report_data;
				$maxTotal = 0;
				$pointsTotal = 0;

				// Conformité administrative / 5
					$max = $count = 5;
					$maxTotal = $maxTotal + $max;
					if ($r['docs']['user_manual'] != 1) { $count--; }
					if ($r['docs']['license'] != 1) { $count--; }
					if ($r['docs']['plaque'] != 1) { $count--; }
					if ($r['docs']['vin'] != 1) { $count--; }
					if ($r['infodiverses']['double_keys'] != 1) { $count--; }
					$eval['conf_admin'] = makeNote($max, $count);
					$pointsTotal = $pointsTotal + $count;
				// echo 'Conformité administrative: '.$count.' / '.$max.' => '.makeNote($max, $count).'<br>';

				// Confirmité du véhicule / 3
					$max = $count = 3;
					$maxTotal = $maxTotal + $max;
					if ($r['inner']['miles_change'] != 1) { $count--; } // COHÉRENCE DU KILOMÉTRAGE
					if ($r['inner']['service_mileage'] != 1) { $count--; } // COHÉRENCE DU KILOMÉTRAGE
					if ($r['docs']['no_non_originals'] == 1) { $count--; } // PIÈCES/ACCESSOIRES NON D’ORIGINE
					$eval['conf_vehicule'] = makeNote($max, $count);
					$pointsTotal = $pointsTotal + $count;
				// echo 'Confirmité du véhicule: '.$count.' / '.$max.' => '.makeNote($max, $count).'<br>';

				// Entretien / 5
					$max = $count = 5;
					$maxTotal = $maxTotal + $max;
					if (!$r['entretien']['carnettype']) { $count--; } // CARNET D’ENTRETIEN
					if ($r['entretien']['service_bill'] != 1) { $count--; } // FACTURE(S) DE SERVICE D’ENTRETIEN
					if ($r['entretien']['last_expertise_sheet'] == 1) { }else{ if ($r['entretien']['car_has_less_than_5y'] != 1) { $count--; } } // EXPERTISE DU SERVICE DES AUTOMOBILES
					if ($r['infodiverses']['nb_de_props'] != 1) { $count--; } // NB DE PROPRIÉTAIRES
					if ($r['infodiverses']['sheltered_parking'] != 1) { $count--; } // STATIONNEMENT
					$eval['entretien'] = makeNote($max, $count);
					$pointsTotal = $pointsTotal + $count;
				// echo 'Entretien: '.$count.' / '.$max.' => '.makeNote($max, $count).'<br>';

				// Sécurité / 91
					$max = $count = 91;
					$maxTotal = $maxTotal + $max;
					if ($r['startengine']['main_indicator_lights'] != 1) { $count--; } // VÉRIFICATION DU FONCTIONNEMENT DES TÉMOINS PRINCIPAUX AU TABLEAU DE BORD / 1
					if ($r['startengine']['warning_indicator_lights'] != 1) { $count--; } // VÉRIFICATION DE L’ABSENCE DE TÉMOINS D’ALERTE/ALARME AU TABLEAU DE BORD / 1
					if (!$r['startengine']['parking_brake_lever'] == 'ec38d9lf' || !$r['startengine']['parking_brake_lever'] == 'ec38mgq5') { $count--; } // EFFICACITÉ DU LEVIER DE FREIN DE STATIONNEMENT / 1
					if ($r['essais_routier']['brake_efficiency'] != 1) { $count--; } // EFFICACITÉ DU FREINAGE / 1
					foreach ($r['wheel']['suspension1'] as $key => $value) { if ($value !== 1) { $count--; } } // ETAT DES SUSPENSIONS / 4
					foreach ($r['essais_routier']['suspension_efficiency1'] as $key => $value) { if ($value !== 1) { $count--; } } // EFFICACITÉ DES SUSPENSIONS / 4
					foreach ($r['wheel']['steering1'] as $key => $value) { if ($value !== 0) { $count--; } } // ETAT DES ORGANES DE LA DIRECTION / 7
					if ($r['startengine']['steering_operation'] != 1) { $count--; } // FONCTIONNEMENT DU VOLANT DE DIRECTION / 1
					if ($r['essais_routier']['drift_control'] != 1) { $count--; } // CONTRÔLE DE LA DÉRIVE / TENUE DE CAP / 1
					foreach ($r['wheel']['rim1'] as $key => $value) { if ($value !== 0) { $count--; } } // ETAT DES JANTES / 4
					foreach ($r['wheel']['tire1'] as $key => $value) { if ($value !== 0) { $count--; } } // ETAT DES PNEUMATIQUES / 4
					foreach ($r['wheel']['tire_sculpture1'] as $key => $value) { if ($value == 'Pneu à changer') { $count--; } } // PROFONDEUR EN MILLIMÈTRES DES SCULPTURES DES PNEUMATIQUES / 4
					foreach ($r['wheel']['brake_pad1'] as $key => $value) { if ($value[0] != '> 4mm (ok)') { $count--; } } // VÉRIFICATION VISUELLE DE L’EPAISSEUR DES GARNITURES DES PLAQUETTES DE FREIN / 4
					foreach ($r['wheel']['brake_disks_damage'] as $key => $value) { if ($value == 1) { $count--; } } // ETAT DE SURFACE DES DISQUES DE FREIN / 4
					if ($r['wheel']['brake_disk_thickness']['ec07xmr2'] < $r['docs']['disk_minimal_wear_amount']['ec010o2k']) { $count--; } if ($r['wheel']['brake_disk_thickness']['ec07ksfk'] < $r['docs']['disk_minimal_wear_amount']['ec010o2k']) { $count--; } if ($r['wheel']['brake_disk_thickness']['ec07lbkd'] < $r['docs']['disk_minimal_wear_amount']['ec01k3du']) { $count--; } if ($r['wheel']['brake_disk_thickness']['ec07b77k'] < $r['docs']['disk_minimal_wear_amount']['ec01k3du']) { $count--; } // USURE DES DISQUES DE FREIN / 4
					foreach ($r['wheel']['wheels_operation1'] as $key => $value) { if (isset($value)) { $count--; } } // VÉRIFICATION DE L’ABSENCE DE JEUX DE FONCTIONNEMENTS AUX ROUES / 4
					if ($r['infodiverses']['spare_wheel'] != 'ec26ae05' && $r['infodiverses']['spare_wheel'] != 'ec26dsna') { $count--; } // ROUE DE SECOURS OU NÉCESSAIRE ANTI-CREVAISON / 1
					foreach ($r['inner']['drivers_seat1'] as $key => $value) { if (isset($value)) { $count--; break;  } } // FONCTIONNEMENT DU SIÈGE CONDUCTEUR / 1
					// FONCTIONNEMENT DU SYSTÈME D’ÉCLAIRAGE ET DE SIGNALISATION / 20
						if (isset($r['light']['buzzer1'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec04umyy'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec04mv3r'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec04xjqb'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec0414ys'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec04ulby'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec04jqtp']) && isset($r['light']['front_rear_light1']['ec04ulby'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec0400bn'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec040v8t'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec04ohd6'])) { $count--; }
						if (isset($r['light']['hazard_warning1'])) { $count--; }
						if (isset($r['light']['front_rear_light1']['ec04jk46'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04vtfy']) || isset($r['light']['front_rear_indicator1']['ec04qcat'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04vtfy']) || isset($r['light']['front_rear_indicator1']['ec04tk8j'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04vtfy']) || isset($r['light']['front_rear_indicator1']['ec04xerb'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04vtfy']) || isset($r['light']['front_rear_indicator1']['ec04nmam'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04e57i']) || isset($r['light']['front_rear_indicator1']['ec041elf'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04e57i']) || isset($r['light']['front_rear_indicator1']['ec04qh17'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04e57i']) || isset($r['light']['front_rear_indicator1']['ec04s2ix'])) { $count--; }
						if (isset($r['light']['front_rear_indicator1']['ec043ptm']) || isset($r['light']['front_rear_indicator1']['ec04e57i']) || isset($r['light']['front_rear_indicator1']['ec04twa1'])) { $count--; }
					// ETAT DES OPTIQUES AVANT ET ARRIÈRE / 7
						if (isset($r['light']['optic1']['ec04x58i'])) { $count--; }
						if (isset($r['light']['optic1']['ec04plqu'])) { $count--; }
						if (isset($r['light']['optic1']['ec048fkz'])) { $count--; }
						if (isset($r['light']['optic1']['ec044d4f'])) { $count--; }
						if (isset($r['light']['optic1']['ec04umyy'])) { $count--; }
						if (isset($r['light']['optic1']['ec04fn83'])) { $count--; }
						if (isset($r['light']['optic1']['ec04sy8g'])) { $count--; }
					if (isset($r['inner']['side_mirror1'])) { $count--; } if (isset($r['inner']['central_mirror1'])) { $count--; } // VERRES DE RÉTROVISEURS LATÉRAUX ET CENTRAL / 2
					if ($r['light']['wiper1']['ec04hv4o']) { $count--; } if ($r['light']['wiper1']['ec04s1v5']) { $count--; } if ($r['inner']['sprinkler_func1']['ec056rdd']) { $count--; } if ($r['inner']['sprinkler_func1']['ec058kjd']) { $count--; } if ($r['inner']['sprinkler1']['ec04m809']) { $count--; } if ($r['inner']['sprinkler1']['ec041k9d']) { $count--; } // ESSUIE-GLACES / 6
					if ($r['inner']['driver_seat_belt1']) { $count--; } if ($r['inner']['passanger_seat_belt1']) { $count--; } if ($r['inner']['rear_seat_belt1']['ec209xek']) { $count--; } if ($r['inner']['rear_seat_belt1']['ec20bhue']) { $count--; } if ($r['inner']['rear_seat_belt1']['ec20j0nq']) { $count--; } // ETAT ET FONCTIONNEMENT DES CEINTURES DE SÉCURITÉ / 5
					$eval['securite'] = makeNote($max, $count);
					$pointsTotal = $pointsTotal + $count;
				// echo 'Sécurité: '.$count.' / '.$max.' => '.makeNote($max, $count).'<br>';

				// Mécanique / manu: 32 / auto: 31
					if ($annonces->transmission == 'manuelle') { $max = 32; }else{ $max = 31; }
					$count = $max;
					$maxTotal = $maxTotal + $max;
					if ($r['inner']['elec_diagnostic1']) { $count--; } // DIAGNOSTIC ÉLECTRONIQUE / 1
					if ($r['ext_engine']['compartments_damage'] != 0) { $count--; } // ETAT DU COMPARTIMENT MOTEUR / 1
					if ($r['ext_engine']['engine_sealing1'] != 0) { $count--; } // ETANCHÉITÉ MOTEUR / 1
					if ($r['ext_engine']['fluid_level1']['ec064ttv'] == 'Niveau insuffisant (en dessous du min.)') { $count--; } if ($r['ext_engine']['fluid_level1']['ec066sai'] == 'Niveau insuffisant (en dessous du min.)') { $count--; } if ($r['ext_engine']['fluid_level1']['ec06kqau'] == 'Niveau insuffisant (en dessous du min.)') { $count--; } if ($r['ext_engine']['fluid_level1']['ec06jhsg'] == 'Niveau insuffisant (en dessous du min.)') { $count--; } // NIVEAUX DES FLUIDES / 4
					// ETAT DU BAS MOTEUR / 2
						$err_cart_huile_mot = $err_bloc_mot = null;
						foreach ($r['infra']['low_engine_damage1'] as $key => $value) {
							if (array_key_exists('ec04yb4k', $value)) { $err_cart_huile_mot = true; }
							if (array_key_exists('ec04ctvo', $value)) { $err_bloc_mot = true; }
							foreach ($value as $k => $val) {
								if (array_key_exists('ec04yb4k', $value)) { $err_cart_huile_mot = true; }
								if (array_key_exists('ec04ctvo', $value)) { $err_bloc_mot = true; }
							}
						}
						if ($err_cart_huile_mot) { $count--; }
						if ($err_bloc_mot) { $count--; }
					if ($r['infra']['oil_leak1']) { $count--; } // VÉRIFICATION DE L’ABSENCE DE FUITES DE FLUIDES / 7
					if ($r['infra']['left_damage1']) { $count--; } if ($r['infra']['right_damage1']) { $count--; } // ETAT DES BAS DE CAISSE / JUPES LATÉRALES / 2
					if ($r['infra']['state_cradle1']) { $count--; } if ($r['infra']['state_spar1']) { $count--; } if ($r['infra']['state_floor1']) { $count--; } // ETAT DU CHASSIS / 3
					if ($r['infra']['manifold1']) { $count--; } if ($r['infra']['line1']) { $count--; } if ($r['infra']['quiet1']) { $count--; } // ETAT, ÉTANCHÉITÉ ET FIXATION DU SYSTÈME D’ÉCHAPPEMENT & POT CATALYTIQUE / 3
					if (isset($r['startengine']['intensity_low_value1']) || $r['startengine']['intensity_good'] == 0) { $count--; } // DÉMARRAGE DU MOTEUR / 1
					if ($r['startengine']['engine_noise_value1']) { $count--; } if ($r['startengine']['exhaust_noise_value1']) { $count--; } if ($r['essais_routier']['acceleration_noise_value1']) { $count--; } // VÉRIFICATION DE L’ABSENCE DE BRUITS ANORMAUX / 3
					if (isset($r['startengine']['abnormal_smoke_value1'])) { $count--; } // VÉRIFICATION DE L’ABSENCE DE FUMÉE ANORMALE À L’ÉCHAPPEMENT / 1
					if ($annonces->transmission == 'manuelle' && isset($r['startengine']['car_stalls1']['ec38fztc'])) { $count--; } // TEST DE L’EMBRAYAGE À L’ARRÊT / 1
					if ($annonces->transmission == 'manuelle' && isset($r['essais_routier']['manual_defects1'])) { $count--; } // PASSAGE DES RAPPORTS (TRANSMISSION MANUELLE) / 1
					if ($annonces->transmission == 'automatique' && isset($r['essais_routier']['automatic_defects1'])) { $count--; } // PASSAGE DES RAPPORTS (TRANSMISSION AUTOMATIQUE) / 1
					if (isset($r['essais_routier']['acceleration_behavior2']['ec399ozq']) || isset($r['essais_routier']['acceleration_behavior2']['ec39o9rx'])) { $count--; } // COMPORTEMENT À L’ACCÉLÉRATION / 1
					$eval['mecanique'] = makeNote($max, $count);
					$pointsTotal = $pointsTotal + $count;
				// echo 'Mécanique: '.$count.' / '.$max.' => '.makeNote($max, $count).' <br>';

				// Carrosserie et Vitrage / 40
					$max = $count = 40;
					$maxTotal = $maxTotal + $max;
					foreach ($r['carrosserie']['bumper1'] as $key => $value) { $count--; } // Face avant
					foreach ($r['carrosserie']['left_front_wing1'] as $key => $value) { $count--; } // Côté gauche
					foreach ($r['carrosserie']['rear_body_damages1'] as $key => $value) { $count--; } // Côté gauche
					foreach ($r['carrosserie']['right_body_damage2'] as $key => $value) { $count--; } // Côté droit
					if ($r['carrosserie']['roof_damage1']) { $count--; } if ($r['carrosserie']['hood_damages1']) { $count--; } if ($r['inners']['dysfunction_damages1']) { $count--; } // Toit / capote
					foreach ($r['ajustements']['alignements1'] as $key => $value) { $count--; } // AJUSTEMENT/ALIGNEMENT DES ÉLÉMENTS DE CARROSSERIE
					foreach ($r['inners']['malfunction1'] as $key => $value) { $count--; } // OUVERTURE/FERMETURE DES OUVRANTS
					foreach ($r['vitrage']['windshield_damages1'] as $key => $value) { $count--; } // ETAT DU VITRAGE
					foreach ($r['inners']['rear_malfunctions1'] as $key => $value) { $count--; break; } // FONCTIONNEMENT DU VITRAGE
					$eval['carrosserie_vitrage'] = makeNote($max, $count);
					$pointsTotal = $pointsTotal + $count;
				// echo 'Carrosserie et Vitrage: '.$count.' / '.$max.' => '.makeNote($max, $count).'<br>';

				// Equipement et Intérieur / 12
					$max = $count = 12;
					$maxTotal = $maxTotal + $max;
					if(isset($r['inners']['stain1'])) { $count--; } // Etat du levier de vitesse et de la console centrale
					if(isset($r['inners']['compartment1'])) { $count--; } // Présence d’odeur marquée(fumée, carburant, gaz d’échappement)
					if(isset($r['inners']['lock_doors1']) || $r['inners']['no_opening'] === 0) { $count--; } // Fermeture centralisée des portières et du coffre/hayon
					if(isset($r['inners']['seats_malfunction1'])) { $count--; } // Fonctionnement des sièges avant chauffants
					if(isset($r['inners']['wheels_malfunction1'])) { $count--; } // Fonctionnement du volant chauffant
					if(isset($r['startengine']['boards_malfunction'])) { $count--; } // Fonctionnement de l’ordinateur de bord
					if(isset($r['startengine']['heats_malfunction'])) { $count--; } // Fonctionnement du chauffage
					if(isset($r['startengine']['climate_function1'])) { $count--; } // Fonctionnement de la climatisation
					if(isset($r['essais_routier']['turbo1'])) { $count--; } // Fonctionnement du turbo
					if(isset($r['essais_routier']['regulator']) && $r['essais_routier']['regulator'][0] != 'Aucun dysfonctionnement constaté') { $count--; } // Régulateur de vitesse
					if(isset($r['essais_routier']['compteur']) && $r['essais_routier']['compteur'][0] != 'Aucun dysfonctionnement constaté') { $count--; } // Compteur kilométrique
					if(isset($r['engine']['camera_malfunction1'])) { $count--; } // Caméra de recul
					$eval['equipemnt_interieur'] = makeNote($max, $count);
					$pointsTotal = $pointsTotal + $count;
				// echo 'Equipement et Intérieur: '.$count.' / '.$max.' => '.makeNote($max, $count).'<br>';

				// Make notes
					$eval['total'] = makeNote($maxTotal, $pointsTotal); // Total

					foreach ($eval as $key => $value) {
						$eval[$key] = array(
							'key' => $key,
							'note' => $value,
							'img' => makeNoteImg($value)
						);

						if ($key == 'conf_admin') { $eval[$key]['titre'] = 'Conformité administrative'; }
						if ($key == 'conf_vehicule') { $eval[$key]['titre'] = 'Conformité du véhicule'; }
						if ($key == 'entretien') { $eval[$key]['titre'] = 'Entretien'; }
						if ($key == 'securite') { $eval[$key]['titre'] = 'Sécurité'; }
						if ($key == 'mecanique') { $eval[$key]['titre'] = 'Mécanique'; }
						if ($key == 'carrosserie_vitrage') { $eval[$key]['titre'] = 'Carrosserie & Vitrage'; }
						if ($key == 'equipemnt_interieur') { $eval[$key]['titre'] = 'Équipement & Intérieur'; }
						if ($key == 'total') { $eval[$key]['titre'] = 'Évaluation moyenne'; }
					}

					$report_data['evals'] = $eval;

		}else {
			echo "Invalid Request!!";exit;
		}
		// echo "\n\nEOF";exit;

	    /*if (!isset($_POST['nom']) || empty($_POST['nom'])){
	    	echo 'no name';exit;
		} elseif (!isset($_POST['prenom']) || empty($_POST['prenom'])){
			echo 'no surname';exit;
		} elseif (!isset($_POST['email']) || empty($_POST['email'])){
			echo 'no email';exit;
		} elseif (!isset($_POST['message']) || empty($_POST['message'])){
			echo 'no message';exit;
		} else { */

			if (ENVIRONMENT == 'dev') { $client->email = "victor.weiss.be@gmail.com"; }
			if ( ! filter_var($client->email, FILTER_VALIDATE_EMAIL)) {
				echo 'not a valid email';exit;
			} else {

				$loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
				$twig = new \Twig_Environment($loader);
				$html = $twig->render('report_pdf.html.twig', $report_data);

				$dompdf = new Dompdf();
				$dompdf->loadHtml($html);

				$dompdf->setPaper('A4', 'portrait');
				$dompdf->set_option('defaultMediaType', 'all');
				$dompdf->set_option('isFontSubsettingEnabled', true);
				$dompdf->set_option("isPhpEnabled", true);
				$dompdf->render();

				if (ENVIRONMENT == 'dev') {
					// Show PDF on page
					$dompdf->stream('pdf.pdf', array("Attachment" => false));
				}else{
					// Download PDF
					$output = $dompdf->output();
					$filename = __DIR__.'/'.$filename;
					file_put_contents($filename, $output);
					echo "success";
				}

			}
		/*}*/
	}else {
		echo 'no data';
	}
}
else {
	echo "not a post action";
}

// On remet la limite d'execution du script à 30 sec et l'autre à 60
ini_set('max_execution_time', 30);
ini_set('default_socket_timeout', 60);
ini_set('memory_limit','128M');