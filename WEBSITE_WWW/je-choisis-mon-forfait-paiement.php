<?php
session_start();
include('db_connexion.php');

if (!count($_POST)) {
    header('Location: index.php');
    exit;
}

// Get POST parameters
$id_client = array_key_exists('id_client', $_POST) && intval($_POST['id_client']) != 0 ? intval($_POST['id_client']) : null;
$id_annonce = array_key_exists('id_annonce', $_POST) && intval($_POST['id_annonce']) != 0 ? intval($_POST['id_annonce']) : null;
$present = array_key_exists('present', $_POST) ? intval($_POST['present']) : 0;
$type_forfait = array_key_exists('type_forfait', $_POST) && in_array($_POST['type_forfait'], ['ScanExpert']) ? $_POST['type_forfait'] : 'ScanExpert';

if ($id_client === null || $id_annonce === null) {
    header('Location: index.php');
    exit;
}

$sql = mysqli_query($connect1, 'SELECT * FROM clients WHERE id="' . $id_client .'";');
$client = mysqli_fetch_object($sql);

$sql = mysqli_query($connect1, 'SELECT * FROM annonces_clients WHERE id="' . $id_annonce .'";');
$annonce = mysqli_fetch_object($sql);

if ($client === false || $annonce === false) {
    header('Location: index.php');
    exit;
}

// Check if a exam is already registered for this annonce
/*$sql = mysqli_query($connect1, 'SELECT * FROM demandes_exam WHERE id_annonces_clients="' . $id_annonce .'";');
$annonce = mysqli_fetch_object($requete);
if ($annonce !== false) {
    header('Location: je-choisis-mon-forfait.php?id='.$id_annonce.'&errorCode=1');
    exit;
}*/

// Insert row in demandes_exam table
require_once('appbackend/admin/include/db_connect.php');
require_once('appbackend/events.php');


//timestamp local (+1)
//
$now = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
$id_demande = insert_new_demande($dbh, $now->getTimestamp(), $annonce->id_client, $id_client, $id_annonce, $present, $type_forfait);





if (!$id_demande) {
    // Unexpected error
    header('Location: je-choisis-mon-forfait.php?id='.$id_annonce.'&errorCode=2');
    exit;
}

// Send form to klikandpay
?>

<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Autospot.ch</title>
    </head>
    <body>
        <p>Vous allez être rediriger vers notre partenaire bancaire.</p>
        <p>Si la page ne se charge pas dans quelques secondes, vous pouvez cliquer sur le bouton "Valider" ci-dessous.</p>
        <form method="post" action="https://www.klikandpay.com/paiement/order1.pl" accept-charset="UTF8" name="prepayment">
            <input type="hidden" name="NOM" size="24" value="<?php echo $client->nom; ?>" />
            <input type="hidden" name="PRENOM" size="24" value="<?php echo $client->prenom; ?>" />
            <input type="hidden" name="ADRESSE" size="24" value="<?php echo $client->adresse_num . ' ' . $client->adresse; ?>" />
            <input type="hidden" name="CODEPOSTAL" size="24" value="<?php echo $client->code_postal; ?>" />
            <input type="hidden" name="VILLE" size="24" value="<?php echo $client->ville; ?>" />
            <input type="hidden" name="PAYS" size="24" value="CH" />
            <input type="hidden" name="TEL" size="24" value="<?php echo $client->telephone; ?>" />
            <input type="hidden" name="EMAIL" size="24" value="<?php echo $client->email; ?>" />
            <input type="hidden" name="ID" value="1504875514" />
            <input type="hidden" name="MONTANT" value="199" />
            <input type="hidden" name="RETOURVOK" value="?id=<?php echo $id_demande; ?>&type=forfait" />
            <input type="hidden" name="RETOURVHS" value="?id=<?php echo $id_demande; ?>&type=forfait" />
            <input type="hidden" name="RETOUR" value="<?php echo $id_demande; ?>" />
            <input type="hidden" name="DIFFERE" value="1" />
            <input type="submit" name="valider" value="Valider" />
        </form>
        <script type="text/javascript">
            window.onload = function() {
                window.setTimeout('document.prepayment.submit()', 3000)
            }
        </script>
    </body>
</html>
