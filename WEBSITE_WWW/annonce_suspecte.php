<?php
session_start();

require_once('db_connexion.php');
require_once('vendor/autoload.php');
require_once('classes/PHPMailer2.php');

$tableannonces = "annonces_clients";

$id = array_key_exists('annonce', $_POST) ? intval($_POST['annonce']) : $_GET['id'];
$id = mysqli_real_escape_string($connect1, $id);
$requete1 = mysqli_query($connect1, "SELECT * FROM $tableannonces WHERE id='$id'");
$annonce = mysqli_fetch_object($requete1);

if ($annonce === null) {
    header('Location: index.php');
    exit;
}

include("header.php");
include("body.php");

$ok = 0;

if (sizeof($_POST) > 0) {
	
	
	$secret = "6LfUhlUUAAAAAArLszhTtTYcHQTHSKfMIT5d1kmi";
	$gRecaptchaResponse = (isset($_POST['g-recaptcha-response'])) ? $_POST['g-recaptcha-response'] : '';
	$recaptcha = new \ReCaptcha\ReCaptcha($secret);
	$resp = $recaptcha->verify($gRecaptchaResponse, $_SERVER['REMOTE_ADDR']);
	
	if (!$resp->isSuccess()){
		$ok = -1;
	}
    else if (!isset($_POST['nom']) || empty($_POST['nom']))
        $ok = -1;
    elseif (!isset($_POST['prenom']) || empty($_POST['prenom']))
        $ok = -1;
    elseif (!isset($_POST['email']) || empty($_POST['email']))
        $ok = -1;
    elseif (!isset($_POST['message']) || empty($_POST['message']))
		$ok = -1;
		
    else {
        $sent = 1;

        $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
        $twig = new \Twig_Environment($loader);
        $body = $twig->render('annonce_suspecte.html.twig', [
            'id' => $annonce->id, 
            'prenom' => $_POST['prenom'],
            'nom' => $_POST['nom'],
            'message' => $_POST['message'],
        ]);

        $mail = new PHPMailer2();

        $mail->setFrom('contact@autospot.ch', 'AutoSpot');
        // $mail->addAddress('contact@autospot.ch');
        $mail->addAddress('contact@autospot.ch');
        $mail->addReplyTo($_POST['email'], $_POST['prenom'].' '.$_POST['nom']);
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Annonce suspecte';
        $mail->Body = $body;
        $mail->send();
    }
}
?>

<!-- Annonce suspecte -->
<div class="container-fluid">
    <div class="contact">
        <h3 class="center">Signaler une annonce</h3>
  
        <form class="form-horizontal" role="form" action="//<?PHP echo $_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];?>" method="post">

            <div class="form-group">
                <label class="control-label col-sm-2"></label>
                <div class="col-sm-10">
                    <?php if ($ok == -1): ?>
                        <div class="alert alert-danger" role="alert">
                            Afin que votre déclaration soit enregistrée, vous devez renseigner tous les champs du formulaire ci-dessous.
                        </div>
                    <?php elseif (isset($sent)): ?>
                        <div class="alert alert-success" role="alert">
                            Votre message a bien été envoyé !
                        </div>
                    <?php endif; ?>  
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2"></label>
                <div class="col-sm-10">
                    Si vous avez trouvé un ou plusieurs éléments suspects dans cette annonce, veuillez-nous le faire part le plus précisément possible.<br />
                    Nous entreprendrons les démarches nécessaires auprès du vendeur
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2"></label>
                <div class="col-sm-10">
                    <strong>Annonce N°<?php echo $annonce->id; ?></strong>
                    <input type="hidden" name="annonce" value="<?php echo $annonce->id; ?>" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="nom">Nom :</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="Entrez votre nom" value="<?php if((isset($_POST['nom'])) && ($ok == 0)) echo $_POST['nom'];?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="prenom">Prénom :</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Entrez votre prénom" value="<?php if((isset($_POST['prenom'])) && ($ok == 0)) echo $_POST['prenom'];?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email :</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Entrez votre email" value="<?php if((isset($_POST['email'])) && ($ok == 0)) echo $_POST['email'];?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="message">Anomalies constatées :</label>
                <div class="col-sm-10">
                    <textarea id="message" name="message" class="form-control" rows="10" placeholder="Entrez votre message"><?php if((isset($_POST['message'])) && ($ok == 0)) echo $_POST['message'];?></textarea>
                </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-sm-2" for="message"></label>
                <div class="col-sm-10">
				<div class="g-recaptcha" data-sitekey="6LfUhlUUAAAAADFtz6TSa-0HVxBQYmxATuHJLlNt"></div>
                </div>
            </div>
			
			
            <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10">
                    <a href="/annonce.php?id=<?php echo $annonce->id; ?>" class="btn btn-cancel">Retour</a>
                    <button type="submit" class="btn btn-default">Envoyer</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php include("footer.php"); ?>