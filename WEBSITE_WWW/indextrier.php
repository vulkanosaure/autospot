<!-- TRIER -->
<?PHP
if(isset($_GET["trier"]))
	$query_string = preg_replace("/\&trier=$_GET[trier]/", "", $_SERVER["QUERY_STRING"]);
else
	$query_string = $_SERVER["QUERY_STRING"];
?>
<div class="collapse in myFilters-1">
<div class="col-sm-12 form-inline">
<select name="trier" class="form-control input-sm tri autotri col-sm-3" onchange="document.getElementById('trier').value=this.value;<?PHP if(isset($_GET['q'])) {?>this.form.submit();<?PHP } else {?>formaffiner.submit();<?PHP }?>">
<option value="" <?PHP if(!isset($_GET["trier"])) echo "selected";?>><?PHP echo $LANG[18];?></option>
<option value="prixcroissant" <?PHP if((isset($_GET["trier"])) && ($_GET["trier"] == "prixcroissant")) echo "selected";?>><?PHP echo $LANG[19];?></option>
<option value="prixdecroissant" <?PHP if((isset($_GET["trier"])) && ($_GET["trier"] == "prixdecroissant")) echo "selected";?>><?PHP echo $LANG[20];?></option>
<option value="kmcroissant" <?PHP if((isset($_GET["trier"])) && ($_GET["trier"] == "kmcroissant")) echo "selected";?>><?PHP echo $LANG[21];?></option>
<option value="kmdecroissant" <?PHP if((isset($_GET["trier"])) && ($_GET["trier"] == "kmdecroissant")) echo "selected";?>><?PHP echo $LANG[22];?></option>
<option value="anneecroissant" <?PHP if((isset($_GET["trier"])) && ($_GET["trier"] == "anneecroissant")) echo "selected";?>><?PHP echo $LANG[23];?></option>
<option value="anneedecroissant" <?PHP if((isset($_GET["trier"])) && ($_GET["trier"] == "anneedecroissant")) echo "selected";?>><?PHP echo $LANG[24];?></option>
</select>
<span class="col-sm-1 trispace hide480"></span>
<select name="region" class="form-control input-sm tri col-sm-3" onchange="document.getElementById('region').value=this.value;<?PHP if(isset($_GET['q'])) {?>this.form.submit();<?PHP } else {?>formaffiner.submit();<?PHP }?>">
<option value="suisse" <?PHP if((!isset($_GET["region"])) || ($_GET["region"] == "suisse")) echo "selected";?>><?PHP echo $LANG[28];?></option>
<option value="suisseallemande" <?PHP if((isset($_GET["region"])) && ($_GET["region"] == "suisseallemande")) echo "selected";?>><?PHP echo $LANG[29];?></option>
<option value="suisseitalienne" <?PHP if((isset($_GET["region"])) && ($_GET["region"] == "suisseitalienne")) echo "selected";?>><?PHP echo $LANG[30];?></option>
<option value="suisseromande" <?PHP if((isset($_GET["region"])) && ($_GET["region"] == "suisseromande")) echo "selected";?>><?PHP echo $LANG[31];?></option>
</select>
<span class="col-sm-1 hide767"></span>

<select name="type_vendeur" class="form-control input-sm tri col-sm-3" onchange="document.getElementById('type_vendeur').value=this.value;<?PHP if(isset($_GET['q'])) {?>this.form.submit();<?PHP } else {?>formaffiner.submit();<?PHP }?>">
<option value="" <?PHP if((!isset($_GET["type_vendeur"])) || ($_GET["type_vendeur"] == "")) echo "selected";?>><?PHP echo "Tous les vendeurs";?></option>
<option value="0" <?PHP if((isset($_GET["type_vendeur"])) && ($_GET["type_vendeur"] == "0")) echo "selected";?>><?PHP echo "Vendeurs particuliers";?></option>
<option value="2" <?PHP if((isset($_GET["type_vendeur"])) && ($_GET["type_vendeur"] == "2")) echo "selected";?>><?PHP echo "Vendeurs professionnels";?></option>
</select>

</div>

</div>
<!-- FIN TRIER -->
</form>