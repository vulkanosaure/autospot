<?php
session_start();
require_once('vendor/autoload.php');
require_once('db_connexion.php');
require_once('classes/PHPMailer2.php');




if (isset($_POST['email'])) {
    $sent = false;
    $sql = sprintf('SELECT id,email FROM clients WHERE email = "%s"', mysqli_real_escape_string($connect1, $_POST['email']));
    $query = mysqli_query($connect1, $sql);
    $client = mysqli_fetch_object($query);

    if (null === $client) {
        $error = 'Aucun compte n\'est rattaché à cette adresse email !';
    } else {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
        $twig = new \Twig_Environment($loader);
        $body = $twig->render('mot_de_passe_oublie.html.twig', ['url' => sprintf('http://www.autospot.ch/mot_de_passe_oublie_confirmation.php?key=%s', md5($client->email))]);
        
		$mail = new PHPMailer2();
		
        $mail->setFrom('contact@autospot.ch', 'AutoSpot');
        $mail->addAddress($client->email);
        $mail->addReplyTo('no-reply@autospot.ch', 'Ne pas répondre');
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Mot de passe oublié';
        $mail->Body = $body;
		$mail->send();
		
		$sent = true;
		/* 
		echo("send");
		echo "error info : ".$mail->ErrorInfo.';';
		 */
		
	}
	
	// exit("test");

    if (isset($_POST['referer'])) {
        $msg = new \Plasticbrain\FlashMessages\FlashMessages();

        if ($sent) {
            $msg->success('Email envoyé avec succès ! Veuillez consulter votre boite mail.');
        } else {
            $msg->error($error);
        }

        header('Location: '.$_POST['referer']);
        exit;
    }
}
include("header.php");
include("body.php");
?>

<div class="container-fluid">  
    <h3 class="center">Mot de passe oublié</h3>

    <p align="center">Pour réinitialiser votre mot de passe, introduisez votre adresse email.</p>
    <p align="center">Nous vous enverrons un lien dans les prochaines minutes.</p>

    <form class="form-horizontal" action="/mot_de_passe_oublie.php" method="post">
        <?php if (isset($sent)): ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="email"></label>
                <div class="col-sm-8">
                    <div class="alert alert-success" role="alert">
                        Email envoyé avec succès ! Veuillez consulter votre boite mail.
                    </div>
                </div>
            </div>
        <?php elseif (isset($error)): ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="email"></label>
                <div class="col-sm-8">
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">Email : <span class="requis">*</span></label>
            <div class="col-sm-8">
                <input type="email" class="form-control" name="email" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3"></label>
            <div class="col-sm-8">
                <button type="button" class="btn btn-inline btn-default">Annuler</button>
                <button type="submit" class="btn btn-register">Envoyer</button>
            </div>
        </div>
    </form>
</div>

<?php if (isset($sent)): ?>
    <script type="text/javascript">
        window.setTimeout(function() {
            window.location.href = '/inscription.php';
        }, 5000);
    </script>
<?php endif; ?>

<?php include("footer.php"); ?>
