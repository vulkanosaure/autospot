<?php
// ETAPE 3: SECURSPOT [DEBUT]
// TABLE ANNONCES_SECURSPOT
$nombreProprio = 2;
$pourcentageVille = mysqli_real_escape_string($connect1, $_POST['pourcentageVille']);
$circulation1Mois = mysqli_real_escape_string($connect1, $_POST['circulation1Mois']);
$circulation1Annee = mysqli_real_escape_string($connect1, $_POST['circulation1Annee']);
$dateAchatMois1 = mysqli_real_escape_string($connect1, $_POST['dateAchatMois1']);
$dateAchatAnnee1 = mysqli_real_escape_string($connect1, $_POST['dateAchatAnnee1']);
$prenom1 = mysqli_real_escape_string($connect1, $_POST['prenom1']);
$cp1 = mysqli_real_escape_string($connect1, $_POST['cp1']);
$rapportSuisse = mysqli_real_escape_string($connect1, $_POST['rapportSuisse']);
$garantConstructeur1 = mysqli_real_escape_string($connect1, $_POST['garantConstructeur1']);
$etablissementAchat = mysqli_real_escape_string($connect1, $_POST['etablissementAchat']);
$tranche = mysqli_real_escape_string($connect1, $_POST['tranche']);
mysqli_query($connect1, "INSERT INTO annonces_securspot (id_annonce, pourcentageVille, circulation1Mois, circulation1Annee, dateAchatMois1, dateAchatAnnee1, prenom1, cp1, rapportSuisse, garantConstructeur1, etablissementAchat, tranche) VALUES ('$id_annonce', '$pourcentageVille', '$circulation1Mois', '$circulation1Annee', '$dateAchatMois1', '$dateAchatAnnee1', '$prenom1', '$cp1', '$rapportSuisse', '$garantConstructeur1', '$etablissementAchat', '$tranche')") or die(mysqli_error($connect1));
$id_securspot_sql = mysqli_query($connect1, "SELECT id FROM annonces_securspot WHERE id_annonce='$id_annonce' ORDER BY id DESC LIMIT 1");
$id_securspot_array = mysqli_fetch_array($id_securspot_sql);
$id_securspot = $id_securspot_array['id'];
$_SESSION['id_securspot'] = $id_securspot;
// TABLE ANNONCES_SECURSPOT_DETAIL
// 20Mil Kilometres [DEBUT]
$releve20MilNature = mysqli_real_escape_string($connect1, $_POST['releve20MilNature']);
$km20Mil = mysqli_real_escape_string($connect1, $_POST['km20Mil']);
$mois20Mil = mysqli_real_escape_string($connect1, $_POST['mois20Mil']);
$annee20Mil = mysqli_real_escape_string($connect1, $_POST['annee20Mil']);
$etablissement20Mil = mysqli_real_escape_string($connect1, $_POST['etablissement20Mil']);
$facture20Mil = mysqli_real_escape_string($connect1, $_POST['facture20Mil']);
$serviceEffectue20Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue20Mil']);
$pieceChangee20Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee20Mil']);
$nomDetenteur20Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur20Mil']);
$prenom20Mil = mysqli_real_escape_string($connect1, $_POST['prenom20Mil']);
$cp20Mil = mysqli_real_escape_string($connect1, $_POST['cp20Mil']);
$com20Mil = mysqli_real_escape_string($connect1, $_POST['com20Mil']);
if($prenom20Mil != $prenom1)
    $nombreProprio = $nombreProprio + 1;
mysqli_query($connect1, "INSERT INTO annonces_securspot_detail (id_securspot, releve20MilNature, km20Mil, mois20Mil, annee20Mil, etablissement20Mil, facture20Mil, serviceEffectue20Mil, pieceChangee20Mil, nomDetenteur20Mil, prenom20Mil, cp20Mil, com20Mil) VALUES ('$id_securspot', '$releve20MilNature', '$km20Mil', '$mois20Mil', '$annee20Mil', '$etablissement20Mil', '$facture20Mil', '$serviceEffectue20Mil', '$pieceChangee20Mil', '$nomDetenteur20Mil', '$prenom20Mil', '$cp20Mil', '$com20Mil')") or die(mysqli_error($connect1));
// 20Mil Kilometres [FIN]
// 40Mil Kilometres [DEBUT]
if($_POST['mois40Mil'] != "")
{
    $releve40MilNature = mysqli_real_escape_string($connect1, $_POST['releve40MilNature']);
    $km40Mil = mysqli_real_escape_string($connect1, $_POST['km40Mil']);
    $mois40Mil = mysqli_real_escape_string($connect1, $_POST['mois40Mil']);
    $annee40Mil = mysqli_real_escape_string($connect1, $_POST['annee40Mil']);
    $etablissement40Mil = mysqli_real_escape_string($connect1, $_POST['etablissement40Mil']);
    $facture40Mil = mysqli_real_escape_string($connect1, $_POST['facture40Mil']);
    $serviceEffectue40Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue40Mil']);
    $pieceChangee40Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee40Mil']);
    $nomDetenteur40Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur40Mil']);
    $prenom40Mil = mysqli_real_escape_string($connect1, $_POST['prenom40Mil']);
    $cp40Mil = mysqli_real_escape_string($connect1, $_POST['cp40Mil']);
    $com40Mil = mysqli_real_escape_string($connect1, $_POST['com40Mil']);
    if($prenom40Mil != $prenom20Mil)
        $nombreProprio = $nombreProprio + 1;
    mysqli_query($connect1, "UPDATE annonces_securspot_detail SET releve40MilNature='$releve40MilNature', km40Mil='$km40Mil', mois40Mil='$mois40Mil', annee40Mil='$annee40Mil', etablissement40Mil='$etablissement40Mil', facture40Mil='$facture40Mil', serviceEffectue40Mil='$serviceEffectue40Mil', pieceChangee40Mil='$pieceChangee40Mil', nomDetenteur40Mil='$nomDetenteur40Mil', prenom40Mil='$prenom40Mil', cp40Mil='$cp40Mil', com40Mil='$com40Mil' WHERE id_securspot='$id_securspot'") or die(mysqli_error($connect1));
}
// 40Mil Kilometres [FIN]
// 60Mil Kilometres [DEBUT]
$releve60MilNature = mysqli_real_escape_string($connect1, $_POST['releve60MilNature']);
$km60Mil = mysqli_real_escape_string($connect1, $_POST['km60Mil']);
$mois60Mil = mysqli_real_escape_string($connect1, $_POST['mois60Mil']);
$annee60Mil = mysqli_real_escape_string($connect1, $_POST['annee60Mil']);
$etablissement60Mil = mysqli_real_escape_string($connect1, $_POST['etablissement60Mil']);
$facture60Mil = mysqli_real_escape_string($connect1, $_POST['facture60Mil']);
$serviceEffectue60Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue60Mil']);
$pieceChangee60Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee60Mil']);
$nomDetenteur60Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur60Mil']);
$prenom60Mil = mysqli_real_escape_string($connect1, $_POST['prenom60Mil']);
$cp60Mil = mysqli_real_escape_string($connect1, $_POST['cp60Mil']);
$com60Mil = mysqli_real_escape_string($connect1, $_POST['com60Mil']);
// 60Mil Kilometres [FIN]
// 80Mil Kilometres [DEBUT]
$releve80MilNature = mysqli_real_escape_string($connect1, $_POST['releve80MilNature']);
$km80Mil = mysqli_real_escape_string($connect1, $_POST['km80Mil']);
$mois80Mil = mysqli_real_escape_string($connect1, $_POST['mois80Mil']);
$annee80Mil = mysqli_real_escape_string($connect1, $_POST['annee80Mil']);
$etablissement80Mil = mysqli_real_escape_string($connect1, $_POST['etablissement80Mil']);
$facture80Mil = mysqli_real_escape_string($connect1, $_POST['facture80Mil']);
$serviceEffectue80Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue80Mil']);
$pieceChangee80Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee80Mil']);
$nomDetenteur80Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur80Mil']);
$prenom80Mil = mysqli_real_escape_string($connect1, $_POST['prenom80Mil']);
$cp80Mil = mysqli_real_escape_string($connect1, $_POST['cp80Mil']);
$com80Mil = mysqli_real_escape_string($connect1, $_POST['com80Mil']);
// 80Mil Kilometres [FIN]
// 100Mil Kilometres [DEBUT]
$releve100MilNature = mysqli_real_escape_string($connect1, $_POST['releve100MilNature']);
$km100Mil = mysqli_real_escape_string($connect1, $_POST['km100Mil']);
$mois100Mil = mysqli_real_escape_string($connect1, $_POST['mois100Mil']);
$annee100Mil = mysqli_real_escape_string($connect1, $_POST['annee100Mil']);
$etablissement100Mil = mysqli_real_escape_string($connect1, $_POST['etablissement100Mil']);
$facture100Mil = mysqli_real_escape_string($connect1, $_POST['facture100Mil']);
$serviceEffectue100Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue100Mil']);
$pieceChangee100Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee100Mil']);
$nomDetenteur100Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur100Mil']);
$prenom100Mil = mysqli_real_escape_string($connect1, $_POST['prenom100Mil']);
$cp100Mil = mysqli_real_escape_string($connect1, $_POST['cp100Mil']);
$com100Mil = mysqli_real_escape_string($connect1, $_POST['com100Mil']);
// 100Mil Kilometres [FIN]
// 120Mil Kilometres [DEBUT]
$releve120MilNature = mysqli_real_escape_string($connect1, $_POST['releve120MilNature']);
$km120Mil = mysqli_real_escape_string($connect1, $_POST['km120Mil']);
$mois120Mil = mysqli_real_escape_string($connect1, $_POST['mois120Mil']);
$annee120Mil = mysqli_real_escape_string($connect1, $_POST['annee120Mil']);
$etablissement120Mil = mysqli_real_escape_string($connect1, $_POST['etablissement120Mil']);
$facture120Mil = mysqli_real_escape_string($connect1, $_POST['facture120Mil']);
$serviceEffectue120Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue120Mil']);
$pieceChangee120Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee120Mil']);
$nomDetenteur120Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur120Mil']);
$prenom120Mil = mysqli_real_escape_string($connect1, $_POST['prenom120Mil']);
$cp120Mil = mysqli_real_escape_string($connect1, $_POST['cp120Mil']);
$com120Mil = mysqli_real_escape_string($connect1, $_POST['com120Mil']);
// 120Mil Kilometres [FIN]
// 140Mil Kilometres [DEBUT]
$releve140MilNature = mysqli_real_escape_string($connect1, $_POST['releve140MilNature']);
$km140Mil = mysqli_real_escape_string($connect1, $_POST['km140Mil']);
$mois140Mil = mysqli_real_escape_string($connect1, $_POST['mois140Mil']);
$annee140Mil = mysqli_real_escape_string($connect1, $_POST['annee140Mil']);
$etablissement140Mil = mysqli_real_escape_string($connect1, $_POST['etablissement140Mil']);
$facture140Mil = mysqli_real_escape_string($connect1, $_POST['facture140Mil']);
$serviceEffectue140Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue140Mil']);
$pieceChangee140Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee140Mil']);
$nomDetenteur140Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur140Mil']);
$prenom140Mil = mysqli_real_escape_string($connect1, $_POST['prenom140Mil']);
$cp140Mil = mysqli_real_escape_string($connect1, $_POST['cp140Mil']);
$com140Mil = mysqli_real_escape_string($connect1, $_POST['com140Mil']);
// 140Mil Kilometres [FIN]
// 160Mil Kilometres [DEBUT]
$releve160MilNature = mysqli_real_escape_string($connect1, $_POST['releve160MilNature']);
$km160Mil = mysqli_real_escape_string($connect1, $_POST['km160Mil']);
$mois160Mil = mysqli_real_escape_string($connect1, $_POST['mois160Mil']);
$annee160Mil = mysqli_real_escape_string($connect1, $_POST['annee160Mil']);
$etablissement160Mil = mysqli_real_escape_string($connect1, $_POST['etablissement160Mil']);
$facture160Mil = mysqli_real_escape_string($connect1, $_POST['facture160Mil']);
$serviceEffectue160Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue160Mil']);
$pieceChangee160Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee160Mil']);
$nomDetenteur160Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur160Mil']);
$prenom160Mil = mysqli_real_escape_string($connect1, $_POST['prenom160Mil']);
$cp160Mil = mysqli_real_escape_string($connect1, $_POST['cp160Mil']);
$com160Mil = mysqli_real_escape_string($connect1, $_POST['com160Mil']);
// 160Mil Kilometres [FIN]
// 180Mil Kilometres [DEBUT]
$releve180MilNature = mysqli_real_escape_string($connect1, $_POST['releve180MilNature']);
$km180Mil = mysqli_real_escape_string($connect1, $_POST['km180Mil']);
$mois180Mil = mysqli_real_escape_string($connect1, $_POST['mois180Mil']);
$annee180Mil = mysqli_real_escape_string($connect1, $_POST['annee180Mil']);
$etablissement180Mil = mysqli_real_escape_string($connect1, $_POST['etablissement180Mil']);
$facture180Mil = mysqli_real_escape_string($connect1, $_POST['facture180Mil']);
$serviceEffectue180Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue180Mil']);
$pieceChangee180Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee180Mil']);
$nomDetenteur180Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur180Mil']);
$prenom180Mil = mysqli_real_escape_string($connect1, $_POST['prenom180Mil']);
$cp180Mil = mysqli_real_escape_string($connect1, $_POST['cp180Mil']);
$com180Mil = mysqli_real_escape_string($connect1, $_POST['com180Mil']);
// 180Mil Kilometres [FIN]
// 200Mil Kilometres [DEBUT]
$releve200MilNature = mysqli_real_escape_string($connect1, $_POST['releve200MilNature']);
$km200Mil = mysqli_real_escape_string($connect1, $_POST['km200Mil']);
$mois200Mil = mysqli_real_escape_string($connect1, $_POST['mois200Mil']);
$annee200Mil = mysqli_real_escape_string($connect1, $_POST['annee200Mil']);
$etablissement200Mil = mysqli_real_escape_string($connect1, $_POST['etablissement200Mil']);
$facture200Mil = mysqli_real_escape_string($connect1, $_POST['facture200Mil']);
$serviceEffectue200Mil = mysqli_real_escape_string($connect1, $_POST['serviceEffectue200Mil']);
$pieceChangee200Mil = mysqli_real_escape_string($connect1, $_POST['pieceChangee200Mil']);
$nomDetenteur200Mil = mysqli_real_escape_string($connect1, $_POST['nomDetenteur200Mil']);
$prenom200Mil = mysqli_real_escape_string($connect1, $_POST['prenom200Mil']);
$cp200Mil = mysqli_real_escape_string($connect1, $_POST['cp200Mil']);
$com200Mil = mysqli_real_escape_string($connect1, $_POST['com200Mil']);
// 200Mil Kilometres [FIN]
// ETAPE 3: SECURSPOT [FIN]
mysqli_query($connect1, "UPDATE annonces_clients SET nombreProprio='$nombreProprio' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
?>