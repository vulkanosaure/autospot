<?php

include_once(__DIR__.'/../db_connexion.php');

class demande_disponibilite
{
    /**
     * @param $id
     * @return bool|null|object
     */
    public static function getByAnnonceAndClient($id_annonce, $id_client)
    {
        global $connect1;

        $sql = sprintf(
            'SELECT * FROM annonces_demandes_disponibilite WHERE id_annonce="%s" AND id_client="%s";', 
            mysqli_real_escape_string($connect1, $id_annonce),
            mysqli_real_escape_string($connect1, $id_client)
        );

        $query = mysqli_query($connect1, $sql);

        $annonce = mysqli_fetch_object($query);

        if ($annonce === null) {
            return false;
        }

        return $annonce;
    }

    /**
     * Insert new record
     * 
     * @param $id_annonce
     * @param $id_client
     */
    public static function insert($id_annonce, $id_client)
    {
        global $connect1;

        $sql = sprintf(
            'INSERT INTO annonces_demandes_disponibilite (id_client, id_annonce, created_at) VALUES ("%s", "%s", NOW());',
            mysqli_real_escape_string($connect1, $id_client),
            mysqli_real_escape_string($connect1, $id_annonce)
        );

        if (!mysqli_query($connect1, $sql)) {
            return false;
        }

        return mysqli_insert_id($connect1);
    }
}