<?php
include_once(__DIR__.'/../db_connexion.php');

class client
{
    /**
     * @param $id_client
     * @return bool|object
     */
    public static function getById($id_client)
    {
        global $connect1;

        $sql = sprintf(
            'SELECT * FROM clients WHERE id="%s" LIMIT 1;',
            mysqli_real_escape_string($connect1, $id_client)
        );
        $query = mysqli_query($connect1, $sql);

        return mysqli_fetch_object($query);
    }

    public static function isLoggedIn()
    {
        return isset($_SESSION['id_client']);
    }

    public static function getCurrent()
    {
        if (self::isLoggedIn()) {
            return self::getById($_SESSION['id_client']);
        }

        return null;
		}
		
		
		
		
		
}