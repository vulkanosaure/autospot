<?php
require_once(__DIR__ . '/../vendor/autoload.php');
require_once(__DIR__ . '/../classes/PHPMailer2.php');

class email
{
    /**
     * @return PHPMailer
     */
    public static function init()
    {
        $mail = new PHPMailer2();
        return $mail;
    }
}