<?php
include_once(__DIR__.'/../db_connexion.php');
require_once(__DIR__ . '/../appbackend/admin/include/constantes.php');
require_once(__DIR__.'/demande.php');

class annonce
{
	static $debug_mode = DEBUG_MODE; 
	// static $debug_mode = true;
	
    static $availableStatus = [
        'garage_unavailable',
        'seller_iddle_complete',
    ];
		
    static $unavailableBrands = [
        'AC', 'ACREA', 'ADLER', 'ADR', 'AIXAM', 'ALLARD', 'ALVIS', 'AMC', 'AMILCAR', 'ARIEL', 'ARMSTRONG SIDELEY', 'ARTEGA', 'ASTON MARTIN', 
        'AUBURN', 'AUSTIN', 'AUSTIN HEALEY', 'AUTOBIANCHI', 'AVANTI', 'AXR', 'BERKELEY', 'BENTLEY', 'BERTONE', 'BOATTAIL', 'BORGWARD', 'BRABHAM', 'BRASIER', 
        'BRISTOL', 'BUGATTI', 'BUGGI', 'CARBODIE', 'CARVER', 'CATERHAM', 'CHEVRON', 'CLENET', 'COBRA', 'CORD', 'CROSSLE', 'DAF', 'DAREN', 'DATSUN', 
        'DAX', 'DE TOMASO', 'DELAGE', 'DELOREAN', 'DESOTO', 'DESTINY', 'DFSK', 'DIAVOLINO', 'DKW', 'DONKERVOORT', 'DURANT', 'EDSEL', 'ESSEX', 'EXCALIBUR', 
        'FACEL VEGA', 'FERRARI', 'FISKER', 'FRAMO', 'FREEWIEL', 'GINETTA', 'GUMPERT', 'HANOMAG', 'HEALEY', 'HILLMAN', 'HOTCHKISS', 'HRG', 'HS', 'HUDSON', 
        'HUMMER', 'HWM', 'IMPERIA', 'INNOCENTI', 'INTERMECCANICA', 'INVICTA', 'ISO', 'JOWETT', 'KAISER', 'KOENIGSEGG', 'KOUGAR', 'KTM', 'LAGONDA', 
        'LAMBORGHINI', 'LEA FRANCIS', 'LOCOMOBILE', 'LOLA', 'LTI', 'MALLOCK', 'MARCOS', 'MARQUIS', 'MARTIN', 'MASERATI', 'MATHIS', 'MARTA', 'MAXIMAG',
        'MAYBACH', 'MCLAREN', 'MEGA', 'MERCURY', 'MESSERSCHMITT', 'MEV HUMMER', 'MINELLI', 'MONTERVERDI', 'MORGAN', 'MORRIS', 'MORS', 'MOWAG', 'NASH', 'NSU', 
        'OLDSMOBILE', 'OVERLAND', 'PACKARD', 'PAGANI', 'PANHARD', 'PANTHER', 'PGO', 'PHANTER', 'PIAGGIO', 'PIERCE ARROW', 'PININFARINA', 'PLYMOUTH', 
        'PUCH', 'RADICAL', 'RAMOO', 'RELIANT', 'REVA', 'RILEY', 'RINSPEED', 'ROLLS-ROYCE', 'ROSSION', 'RUF', 'RUSKA', 'SALMSON', 'SANTANA', 'SAPOROSHEZ', 
        'SBARRO', 'SECMA', 'SENECHAL', 'SIMCA', 'SINGER', 'SOKON', 'SPYKER', 'STEPHENS', 'STEYR', 'STUDEBAKER', 'STUTZ VICTORIA', 'SUNBEAM', 'SYLVA', 
        'TALBOT', 'TATRA', 'TAZZARI', 'TESLA', 'THINK', 'TRABANT', 'TRIUMPH', 'TVR', 'ULTIMA', 'UMM', 'VEM', 'VENTURI', 'VESPA', 'VOLTEIS', 'WARTBURG', 
        'WESTFIELD', 'WIESMANN', 'WILLYS', 'WOLSELEY', 'YES !', 'ZAGATO', 'ZARP', 'ZBR', 'ZIMMER',
		];
		
		
		static $unavailableModel = [
			"CITROEN C 15",
			"CITROEN C 25",
			"CITROEN JUMPER",
			"FIAT DUCATO",
			"FIAT SCUDO",
			"FORD TRANSIT",
			"OPEL VIVARO",
			"OPEL MOVANO",
			"PEUGEOT BOXER",
			"PEUGEOT EXPERT",
			"RENAULT TRAFFIC",
			"VW CRAFTER",
			"VW T3",
			"VW T4",
			"VW T5",
		];

    /**
     * @param $annonce
     * @return bool
     */
    public static function isAvailable($annonce)
    {
			$demande = demande::getByAnnonce($annonce->id);
			
			if (is_object($demande) === false
					|| !in_array($demande->payment_status, array(demande::PAYMENT_ACCEPTED, demande::PAYMENT_PROCESSING))
			) {
					return true;
			}
	

			if (empty($demande->json_suivi) || ($suivi = json_decode($demande->json_suivi, true)) === false) {
					return true;
			}

			$date_controle = demande::getCompletedDate($demande);
			if ($date_controle instanceof \DateTime) {
							$today = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
							$date_nouvelle_inspection = annonce::getNextExamAvailabilityDate($date_controle);

							if ($today > $date_nouvelle_inspection) {
									return true;
							}
			}
		
			
			if($demande != null && in_array($demande->status, ['garage_error'])){
				return true;
			}
			
			
			$output = demande::jsonKeyExists($suivi, self::$availableStatus);
			
			return $output;
	}
	
	
	public static function getGarageByEmail($email)
	{
		global $connect1;
		
		$sql = sprintf(
			'SELECT * FROM garages WHERE email="%s" LIMIT 1;',
			mysqli_real_escape_string($connect1, $email)
		);
		$query = mysqli_query($connect1, $sql);
		$garage = mysqli_fetch_object($query);
		return $garage;
	}
	
	
	public static function getCantonByCP($cp)
	{
		global $connect1;
		
		$sql = "SELECT * FROM liste_npa WHERE code_postal=:cp";
		$sql = sprintf(
			'SELECT * FROM liste_npa WHERE code_postal="%s"',
			mysqli_real_escape_string($connect1, $cp)
		);
		$query = mysqli_query($connect1, $sql);
		$npa = mysqli_fetch_array($query);
		if($npa) return $npa['canton'];
		else return '';
	}
	

    public static function isReportQueryable($annonce)
    {
			//marques interdites
      if (in_array(strtoupper($annonce->marque), self::$unavailableBrands)) {
    		return false;
			}
			
			//modeles interdits
			$strmodel = strtoupper($annonce->marque.' '.$annonce->modele);
			// if(self::$debug_mode) var_dump($strmodel);
			if(in_array($strmodel, self::$unavailableModel)){
				return false;
			}
			// if(self::$debug_mode) return true;
			
			
			if($annonce->demarre == 0) return false;
			if($annonce->deplace == 0) return false;
			
			if($annonce->inspection_impossible == 1) return false;
			
			if(in_array($annonce->carburant, ['électrique', 'gaz', 'bioethanol', 'hybride'])) return false;
			
			$cat = $annonce->categorie;
			if(in_array($cat, ['collection', 'demonstration'])) return false;
			
			
			
      return self::isAvailable($annonce);
    }

    /**
     * @param $annonce
     * @return bool
	 * 
	 * si pas de demande => false
	 * si date complete exist, +3 mois, si dépassé => false
	 * 
     */
    public static function isReportAvailable($annonce)
    {
		$demande = demande::getByAnnonce($annonce->id);

        if ($demande === false) {
            return false;
		}
		

        $date_controle = demande::getCompletedDate($demande);
        if ($date_controle instanceof \DateTime) {
            $today = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
			$date_nouvelle_inspection = annonce::getNextExamAvailabilityDate($date_controle);
			/* 
			echo "today : ".$today."<br />";
			echo "date_nouvelle_inspection : ".$date_nouvelle_inspection."<br />";
 			*/
            if ($today > $date_nouvelle_inspection) {
                return false;
            }
        }

        return demande::isCompleted($demande);
    }

    /**
     * @param $annonce
     * @return bool
     */
    public static function isReportInProgress($annonce)
    {
        return !self::isAvailable($annonce);
	}
	
	public static function isSellerUnavailable($demande)
	{
		if($demande != null && in_array($demande->status, ['seller_absent', 'seller_iddle'])){
			return true;
		}
		return false;
	}

    /**
     * @param $id
     * @return bool|null|object
     */
    public static function getById($id)
    {
        global $connect1;

        $sql = sprintf('SELECT * FROM annonces_clients WHERE id="%s"', mysqli_real_escape_string($connect1, $id));
        $query = mysqli_query($connect1, $sql);

        $annonce = mysqli_fetch_object($query);
        if ($annonce === false) return false;
        return $annonce;
    }

    /**
     * @param $demande_exam
     * @return bool|DateTime
     */
    public static function getReportAvailabilityDate($demande_exam)
    {
        $date_controle = demande::getCompletedDate($demande_exam);

        if ($date_controle === false) {
            return false;
        }

        $date_rapport = clone $date_controle;
        $secondsToAdd = 60*60*24*15;
        $date_rapport->add(new \DateInterval('PT'.$secondsToAdd.'S'));

        return $date_rapport;
    }

    /**
     * @param \DateTime $date_controle
     * @return \DateTime
     */
    public static function getNextExamAvailabilityDate($date_controle)
    {
        $date_nouvelle_inspection = clone $date_controle;
        $date_nouvelle_inspection->add(new \DateInterval('P90D'));

        return $date_nouvelle_inspection;
    }

    public static function pluralize( $count, $text ) 
    { 
        return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
    }

    public static function in( $datetime )
    {
        $string = '';
        $now = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
        $interval = $now->diff( $datetime, true );

        $string.= sprintf(' %s', self::pluralize( $interval->d, 'jour' ));
        $string.= sprintf(' %s', self::pluralize( $interval->h, 'heure' ));
        $string.= sprintf(' %s', self::pluralize( $interval->i, 'minute' ));
        $string.= sprintf(' %s', self::pluralize( $interval->s, 'seconde' ));
    
        return $string;
	}
	
	
	private static function getGarage($_id)
	{
		global $connect1;
		$sql = sprintf('SELECT * FROM garages WHERE id="%s"', mysqli_real_escape_string($connect1, $_id));
		$query = mysqli_query($connect1, $sql);
		
		$output = mysqli_fetch_array($query);
        if ($output === false) return false;
        return $output;
	}
	
	

    /**
     * @param stdClass $annonce
     * @param bool $withIcons
     * @param bool $buttons
     * @return string
     */
    public static function displayLink(stdClass $annonce, $withIcons = false, $buttons = false)
    {
		$id_client = $annonce->id_client;
		
		global $connect1;
		$sql = sprintf(
			'SELECT * FROM clients WHERE id="%s" LIMIT 1;',
			mysqli_real_escape_string($connect1, $id_client)
		);
		$query = mysqli_query($connect1, $sql);
		$client = mysqli_fetch_object($query);
		
		// var_dump($client);
		$garage = annonce::getGarageByEmail($client->email);
		$demande = demande::getByAnnonce($annonce->id);
		
		
    if (annonce::isReportAvailable($annonce) && !self::$debug_mode) {
			
			$html = '<a href="#" onclick="return false;" style="cursor: not-allowed; color:black;" data-toggle="tooltip" data-placement="top"';
			
			
			if ($demande != false) {
				$date_controle = demande::getCompletedDate($demande);
				
				if ($date_controle instanceof \DateTime) {
					
					$date_nouvelle_inspection = annonce::getNextExamAvailabilityDate($date_controle);
					
					//infobulle
					$date_controle_str = $date_controle->format('d/m/Y');
					$time_controle_str = $date_controle->format('H:i');
					$date_new_controle_str = $date_nouvelle_inspection->format('d/m/Y');
					$str = "Un utilisateur a déjà effectué le ".$date_controle_str." à ".$time_controle_str." une demande d'inspection technique . Si ce véhicule n'est pas vendu dans l'intervalle, la prochaine demande d'inspection technique ne sera possible qu'à partir du ".$date_new_controle_str;
					$html .= ' title="'.$str.'"';
				}
			}
			$html .= ">Demande d'inspection technique impossible</a>";
	
			
		}
		else if(annonce::isSellerUnavailable($demande) && !self::$debug_mode){
			
			$html = 'Demande d\'inspection technique impossible';
			if(self::$debug_mode) $html .= '(seller unavailable)';
		}
		elseif (annonce::isReportInProgress($annonce)) {
            $html = '<a href="#" onclick="return false;" style="cursor: not-allowed;" data-toggle="tooltip" data-placement="top" title="Un utilisateur a effectué une demande d\'inspection technique sur ce véhicule. Aucune nouvelle demande d\'inspection technique n\'est possible pour ce véhicule.">';

            if ($withIcons) {
                $html.= '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ';
            }
			
            $html.= 'Demande d\'inspection technique en cours';
            $html.= '</a>';
		} 
		elseif (!annonce::isReportQueryable($annonce)) {
						$html = 'Demande d\'inspection technique impossible';
						if(self::$debug_mode) $html .= '(queryable)';
		} 
		elseif (($annonce->categorie != 'collection'
            && $annonce->accident != 1
			&& $annonce->plaques != 0
			&& !$garage)
        ) {
			
			$cp = $annonce->codepostal;
			global $connect1;
			
			
			// echo "cp : ".$cp;
			
			$sql = sprintf('SELECT * FROM cp2garage WHERE cp="%s"', mysqli_real_escape_string($connect1, $cp));
			$query = mysqli_query($connect1, $sql);
			$num_rows = mysqli_num_rows($query);
			
			if($num_rows > 0){
				
				//si les 2 garages associés sont inactif => lien impossible
				$obj_cp = mysqli_fetch_array($query);
				
				$garage1 = annonce::getGarage($obj_cp['id_garages__1']);
				$garage2 = annonce::getGarage($obj_cp['id_garages__2']);
				
				if($garage1['actif'] != 1 && $garage2['actif'] != 1) $link_available = false;
				else $link_available = true;
				
			}
			//si ligne inexistantes dans cp2garage
			else{
				$link_available = false;
			}
			
			// if(self::$debug_mode) $link_available = true;
			
			
			if($link_available){
				$html = '<a href="je-choisis-mon-forfait.php?id='.$annonce->id.'"'.($buttons ? ' role="button" class="btn btn-autospot"' : '').'>';

				if ($withIcons) {
					$html.= '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ';
				}

				$html.= 'Je choisis mon forfait';
				if(self::$debug_mode) $html .= ' (test)';
				$html.= '</a>';
			}
			else{
				$html = 'Demande d\'inspection technique impossible';
				if(self::$debug_mode) $html .= '(!link_available)';
			}
			
			
		} 
		else {
				$html = 'Demande d\'inspection technique impossible';
				if(self::$debug_mode) $html .= '(conditions)';
		}
        return $html;
    }
}