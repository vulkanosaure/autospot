<?php

class Display
{
	public function __construct()
	{
		
	}
	
	public static function formatMonth($value)
	{
		$suffix = '';
		
		if($value < 12){
			$suffix = 'mois';
			$value = $value;
		}
		else{
			$suffix = 'an';
			$value = round($value / 12);
			if($value > 1) $suffix .= 's';
		}
		
		return $value.' '.$suffix.'';
	}
	
	
	public static function formatChevaux($value)
	{
		return $value;
	}
	
}