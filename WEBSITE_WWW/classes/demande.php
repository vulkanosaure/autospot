<?php
include_once(__DIR__.'/../db_connexion.php');
require_once(__DIR__.'/annonce.php');

class demande
{
    const PAYMENT_ACCEPTED = 'accepted';
    const PAYMENT_PROCESSING = 'processing';
    const PAYMENT_REJECTED = 'rejected';

    const EVENT_EXAM_COMPLETE = 'exam_complete';

    /**
     * @param $demande
     * @return bool
     */
    public static function isCompleted($demande)
    {
        if (empty($demande->json_suivi) || ($suivi = json_decode($demande->json_suivi, true)) === false) {
            return false;
        }

        if (!($ts = self::jsonKeyExists($suivi, self::EVENT_EXAM_COMPLETE))) {
            return false;
        }

        return true;
    }

    public static function getCompletedDate($demande)
    {
        if (self::isCompleted($demande)) {
            $suivi = json_decode($demande->json_suivi, true);
            $timestamp = self::jsonKeyExists($suivi, self::EVENT_EXAM_COMPLETE);

            $datetime = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
            $datetime->setTimestamp($timestamp);

            return $datetime;
        }

        return false;
    }

    /**
     * @param $id_annonce
     * @return bool|null|object
     */
    public static function getByAnnonce($id_annonce)
    {
        global $connect1;

        $sql = sprintf(
            'SELECT * FROM demandes_exam WHERE id_annonces_clients="%s" ORDER BY demande_ts DESC LIMIT 1;',
            mysqli_real_escape_string($connect1, $id_annonce)
        );
        $query = mysqli_query($connect1, $sql);

        return mysqli_fetch_object($query);
    }

    public static function getById($id_demande)
    {
        global $connect1;
        
        $sql = sprintf(
            'SELECT * FROM demandes_exam WHERE id="%s" ORDER BY demande_ts DESC LIMIT 1;',
            mysqli_real_escape_string($connect1, $id_demande)
        );
        $query = mysqli_query($connect1, $sql);

        return mysqli_fetch_object($query);
    }

    /**
     * @param $json
     * @param $key
     * @return bool
     */
    public static function jsonKeyExists($json, $key)
    {
        foreach ($json as $event) {
            if ((is_array($key) && in_array($event['key'], $key))
                || $event['key'] == $key
            ) {
                return $event['ts'];
            }
        }

        return false;
    }
}