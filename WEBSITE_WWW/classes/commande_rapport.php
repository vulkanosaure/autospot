<?php
include_once(__DIR__.'/../db_connexion.php');

class commande_rapport
{
    const PAIEMENT_ACCEPTED = 'accepted';
    const PAIEMENT_PROCESSING = 'processing';
    const PAIEMENT_REJECTED = 'rejected';
    
    public static function getById($id_commande_rapport)
    {
        global $connect1;
        
        $sql = sprintf(
            'SELECT * FROM commandes_rapports_inspection WHERE id="%s" ORDER BY created_at DESC LIMIT 1;',
            mysqli_real_escape_string($connect1, $id_commande_rapport)
        );
        $query = mysqli_query($connect1, $sql);

        return mysqli_fetch_object($query);
    }

    public static function getByDemandeAndClient($id_demande, $id_client, $status=self::PAIEMENT_ACCEPTED)
    {
        global $connect1;
        
        $sql = sprintf(
            'SELECT * FROM commandes_rapports_inspection WHERE id_demande_exam="%s" AND id_client="%s" AND paiement_status="%s" ORDER BY created_at DESC LIMIT 1;',
            mysqli_real_escape_string($connect1, $id_demande),
            mysqli_real_escape_string($connect1, $id_client),
            mysqli_real_escape_string($connect1, $status)
        );
        $query = mysqli_query($connect1, $sql);

        return mysqli_fetch_object($query);
    }
}