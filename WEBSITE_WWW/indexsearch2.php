    <!-- HEADER JUMBOTRON -->
    <div class="jumbotron jumboresult">

    <div class="row">

      <div class="col-sm-2 resultlogo">
      <a href="//<?PHP echo $_SERVER["HTTP_HOST"];?>">
	  <?PHP
      if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"]))
      {
      	?><img src="images/logomini-motospot.png" class="img-responsive"/><?PHP
      }
      else
      {
      	?><img src="images/logomini.png" class="img-responsive"/><?PHP
      }?></a>
      </div>
      <div class="col-sm-10">
      <form class="form-inline" role="form" action="//<?PHP echo $_SERVER["HTTP_HOST"];?>" method="GET">

        <div class="col-sm-8 bottom5">
          <div class="col-sm-12">
      <input type="text" class="form-control resultsearch" id="q" name="q" value="<?PHP if((isset($_GET['q'])) && ($_GET['q'] != "")) echo $_GET['q']; else echo $LANG[4];?>" onclick="if(document.getElementById('q').value=='<?PHP echo $LANG[4];?>') document.getElementById('q').value='';">
      </div>
      </div>
          <div class="col-sm-3 bottom5">
            <div class="col-sm-12">
    <select class="form-control resultselect" id="r" name="r"/>
  <?PHP
      if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"]))
      {
      	?>
  	<option value="voiture" <?PHP if((!isset($_GET["r"])) || ($_GET["r"] == "voiture")) echo "selected";?>><?PHP echo $LANG[1];?></option>
  	<option value="moto" <?PHP if((!isset($_GET["r"])) || ($_GET["r"] == "moto")) echo "selected";?>><?PHP echo $LANG[2];?></option>
	<?PHP
      }
      else
      {
      	?>
  	<option value="voiture" <?PHP if((!isset($_GET["r"])) || ($_GET["r"] == "voiture")) echo "selected";?>><?PHP echo $LANG[1];?></option>
	<option value="moto" <?PHP if((isset($_GET["r"])) && ($_GET["r"] == "moto")) echo "selected";?>><?PHP echo $LANG[2];?></option>
	<?PHP
      }?>
      </select>
            </div>
          </div>

    <div class="col-sm-1 bottom5">
    <div style="padding-left:5px;padding-right:5px;">
    <button type="submit" class="form-control btn btn-autospot"><span class="fa fa-search"></span></button>
    </div>
    </div>



  </div>

<div class="clear"></div>
  <kbd><?PHP echo $annoncestotal." ".$LANG[3];?></kbd>

</div>
</div>
<!-- END HEADER JUMBOTRON -->