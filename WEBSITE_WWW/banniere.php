<?PHP
session_start();
include("header.php");
include("body.php");
?>
<!-- BANNIERE -->
<div class="container">
<h3 class="center">Notre bannière auto</h3>

<div id="banniere" class="container center-block">


<p>Nous avons crée cette bannière de 70 cm sur 24 cm pour aider nos utilisateurs à vendre leur voiture plus rapidement. Toutes les personnes qui croiseront votre voiture sauront qu'elle est en vente.</p>
<p>Il vous suffit de la fixer facilement avec ses deux ventouses sur une vitre de votre voiture puis d'écrire le prix, l'année, les kilomètres et votre numéro de téléphone.</p>
<p>En exclusivité pour les 250 premières insertions, nous vous offrons gratuitement :</p>

<div class="col-sm-6">
	<p>
		1. La bannière auto<br/>
		2. Un feutre indélébile noir<br/>
		3. L'expédition à votre domicile Suisse en courrier A
	</p>
	<p>Lorsque vous êtes en déplacement, pensez à la fixer à l'intérieur de la fenêtre.</p> 
	<p>Voici en exemple la photographie d'une bannière fixée sur une Mazda.</p>
	<p>Pour recevoir votre bannière, il vous suffit d'insérer gratuitement votre voiture sur autospot en cliquant sur ce bouton : </p>
	<a href="publier.php" class="btn btn-autofoot">Publier une annonce</a>
</div>

<div class="col-sm-6"><img src="images/1-1.png" class="img-responsive"></div>
            
</div>    
<div class="clear"></div>

<!-- BANNIERE -->
</div>
<?php include("footer.php"); ?>