<?PHP
session_start();
if(isset($_SESSION['id_client']))
{
    header("Location: compte.php");
    exit();
}
if(isset($_POST['sunom']))
{
    include("db_connexion.php");
    $sunom = mysqli_real_escape_string($connect1, $_POST['sunom']);
    $suprenom = mysqli_real_escape_string($connect1, $_POST['suprenom']);
    $suemail = mysqli_real_escape_string($connect1, $_POST['suemail']);
    $adresse = mysqli_real_escape_string($connect1, $_POST['adresse']);
    $numero = mysqli_real_escape_string($connect1, $_POST['numero']);
    $codePostal = mysqli_real_escape_string($connect1, $_POST['codePostal']);
    $ville = mysqli_real_escape_string($connect1, $_POST['ville']);
    $sid = uniqid('', true);
    $supwd = md5($_POST['supwd']);
    $date = date("Y-m-d H:i:s");
    mysqli_query($connect1, "INSERT INTO clients (sid, lang, date, nom, prenom, adresse, adresse_num, code_postal, ville, email, mot_de_passe) VALUES ('$sid', 'fr', '$date', '$sunom', '$suprenom', '$adresse', '$numero', '$codePostal', '$ville', '$suemail', '$supwd')");
    $requete = mysqli_query($connect1, "SELECT id FROM clients WHERE email='$suemail' && mot_de_passe='$supwd'");
    $clients = mysqli_fetch_assoc($requete);
    $_SESSION['id_client'] = $clients['id'];
    $email_subject = "Confirmez votre inscription";
    $email_message = "Cher utilisateur d’Autospot,\n
    Bienvenu sur AutoSpot. Nous vous remercions de votre inscription.\n\n
    Pour des raisons de sécurité, merci de confirmer votre inscription en cliquant sur le lien suivant:\n
    Si ce lien ne fonctionne pas, copiez le lien et collez le dans votre barre d’adresse.\n\n
    Si vous n’êtes pas à l’origine de votre inscription sur AutoSpot, merci d’ignorer cet email.\n\n
    Cet e-mail a été généré automatiquement, merci de ne pas y répondre.\n
    Si vous souhaitez nous contacter, n’hésitez pas à le faire au travers de notre formulaire de contact.\n\n
    Avec nos meilleures salutations\n
    L’équipe d’AutoSpot";
    mail($suemail, $email_subject, $email_message);
    if((isset($_SESSION['redirectpublier'])) && ($_SESSION['redirectpublier'] == 1))
        header("Location: publier.php");
    else
        header("Location: compte.php");
    exit();
}
else if(isset($_POST['sipseudo']))
{
    include("db_connexion.php");
    $sipseudo = mysqli_real_escape_string($connect1, $_POST['sipseudo']);
    $sipwd = md5($_POST['sipwd']);
    $requete = mysqli_query($connect1, "SELECT id FROM clients WHERE email='$sipseudo' && mot_de_passe='$sipwd'");
    $clients = mysqli_fetch_assoc($requete);
    if(isset($clients['id']))
    {
        $_SESSION['id_client'] = $clients['id'];
        if((isset($_SESSION['redirectpublier'])) && ($_SESSION['redirectpublier'] == 1))
            header("Location: publier.php");
        else
            header("Location: compte.php");
        exit();
    }
}
include("header.php");
include("body.php");
?>
    
<!-- REGISTER -->
<div class="container">


<div class="col-sm-12">

<!-- SIGN IN -->
<div class="col-sm-6" id="signin">
    <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> S'IDENTIFIER</h4>
    <hr class="blue">
    <div id="connexion">
        
     <!-- formulaire de login -->  
    <form class="form-horizontal" action="http://www.autospot.ch/inscription.php?error=true" role="form" data-toggle="validator" method="post">
     
          
  <div class="form-group">
    <label class="control-label col-sm-3" for="sipseudo">Email : <span class="requis">*</span></label>
    <div class="col-sm-8">
      <input class="form-control" id="sipseudo" name="sipseudo" placeholder="Votre email" required>
    </div>
  </div>
        <div class="clear"></div>
        
  <div class="form-group">
    <label class="control-label col-sm-3" for="sipwd">Mot de passe : <span class="requis">*</span></label>
    <div class="col-sm-8"> 
      <input type="password" class="form-control" id="sipwd" name="sipwd" placeholder="Entrez votre mot de passe" required>
    </div>
  </div>
        <div class="clear"></div>
   
      <div class="col-sm-offset-3 col-sm-4 checkbox checkbox-primary">
        <input id="checksouvenir" type="checkbox" value="oui" name="souvenir"> <label for="checksouvenir">Se souvenir de moi</label>
      </div>
    <div class="col-sm-4 checkbox checkbox-primary">
        <a data-toggle="modal" data-target="#passeOublie">Mot de passe oublié</a>
      </div>
        <div class="clear"></div>
        
         <!-- POP UP mot de passe oublié  -->
<div id="passeOublie" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Vous avez oublié votre mot de passe ?</h4>
      </div>
      <div class="modal-body">
        <p align="center" class="bold">Pour réinitialiser votre mot de passe, introduisez votre<br/>adresse email.</p>
             <p align="center" class="bold">Nous vous enverrons un lien dans les prochaines minutes.</p>
        <div class="clear"></div>
        <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
    <p align="center">Email :</p>
      <input type="email" class="form-control col-sm-12" id="oublipass"><br/>
    <button type="button" class="btn btn-default left" data-dismiss="modal">Annuler</button><button type="submit" class="btn btn-register pull-right">Envoyer</button>
    </div>
  </div>
      </div>
     
    </div>

  </div>
</div>
<!-- /POP UP mot de passe oublié  -->
      
        
  <div class="form-group">
      <label class="col-sm-3"></label>
    <div class="col-sm-8">
      <button type="submit" name="seconnecter" class="btn btn-register noMargin">Se connecter</button>
    </div>
  </div>
        
</form>
    <!-- fin du formulaire de login -->
        </div>
</div>
<!-- END SIGN IN -->

<!-- SIGN UP -->
<div class="col-sm-6" id="signup">
    <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> S'INSCRIRE</h4>
    <div id="introsignup" class="collapse in">
        <hr class="blue">
    <p><i>Vous n’avez pas encore de compte utilisateur ? Inscrivez-vous rapidement et profiter des avantages suivants :</i>
    <ul>
        <li>Autospot.ch, le 1er portail automobile suisse « pour vendre sa voiture plus rapidement »</li>
        <li>Insérez votre annonce devient facile et rapide.</li>
        <li>Recevez gratuitement notre BannerKit (en savoir +) pour vendre votre voiture plus rapidement.</li>
        <li>Accéder gratuitement au service innovant SecureSpot</li>
        <li>Accéder à des annonces de qualité et mémoriser les dans vos favoris.</li>
        <li>Créer des alertes e-mails et recevoir les annonces qui correspondent à votre recherche (à venir)</li>
        <li>Gestion de votre profil</li>	
    </ul>
    </p>
    </div>
    <hr class="blue">
    
    <div class="col-sm-offset-4 col-sm-8">
    <button data-toggle="collapse" data-target="#introsignup, #inscription" class="btn btn-register noMargin">Inscription gratuite</button>
    </div>
    
    <div id="inscription" class="collapse">
        <div class="clear"></div>
        <!-- formulaire d'inscription -->
    <form id="signupform" class="form-horizontal" role="form" data-toggle="validator" method="post">
        
        <div class="form-group">
      <label class="control-label col-sm-4" for="sunom">Nom : <span class="requis">*</span></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sunom" name="sunom" placeholder="Votre nom" required>
      </div>
    </div>
        
        <div class="form-group">
      <label class="control-label col-sm-4" for="suprenom">Prénom : <span class="requis">*</span></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="suprenom" name="suprenom" placeholder="Votre prénom" required>
      </div>
    </div>   
        
    <div class="form-group">
      <label class="control-label col-sm-4" for="suemail">Email : <span class="requis">*</span></label>
      <div class="col-sm-8">
        <input type="email" class="form-control" id="suemail" name="suemail" placeholder="Votre email" required>
      </div>
    </div>
        
        <div class="form-group">
      <label class="control-label col-sm-4" for="adresse">Adresse & N° :  <span class="requis">*</span></label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Votre adresse" required>
      </div>
            <div class="col-sm-2">
        <input type="text" class="form-control" id="numero" name="numero" placeholder="N°" required>
      </div>
    </div>
        
        <div class="form-group">
      <label class="control-label col-sm-4" for="codePostal">Code postal & Ville :  <span class="requis">*</span></label>
      <div class="col-sm-3">
        <input type="text" class="form-control" id="codePostal" name="codePostal" placeholder="Votre NPA" required>
      </div>
            <div class="col-sm-5">
        <input type="text" class="form-control" id="ville" name="ville" placeholder="Votre ville" required>
      </div>
    </div>
        
    <div class="form-group">
      <label class="control-label col-sm-4" for="supwd">Mot de passe : <span class="requis">*</span></label>
      <div class="col-sm-8">          
        <input type="password" class="form-control" id="supwd" name="supwd" placeholder="Choisissez un mot de passe" required>
      </div>
    </div>
        
        <div class="form-group">
      <label class="control-label col-sm-4" for="confirmpwd">Confirmez mot de passe : <span class="requis">*</span></label>
      <div class="col-sm-8">          
        <input type="password" class="form-control" id="confirmpwd" name="confirmpwd" placeholder="Confirmez votre mot de passe" required>
      </div>
    </div>
        
    <div class="form-group">        
      <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" class="btn btn-register noMargin">S'inscrire</button>
      </div>
    </div>
  </form>
    <!-- fin du formulaire d'inscription -->
    </div>
</div>
<!-- END SIGN UP -->
     
</div>
    <div class="clear"></div>
    <hr class="blue">
    <div class="clear"></div>
   

<!-- END REGISTER -->
</div>
    
<?php include("footer.php"); ?>