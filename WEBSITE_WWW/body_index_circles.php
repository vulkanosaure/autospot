<div id="indexCirclesParent">
    <div class="title">
        6 raisons de choisir AutoSpot
        <hr class="blue">
    </div>
    <div class="clear"></div>
    <div id="indexCircles">
    <div class="circle">
        <a data-toggle="modal" data-target="#circle1"><img src="images/index/suisse.png" class="img-responsive"></a>
        <h4>Un service unique en Suisse</h4>
        <p class="hideSmart">Bénéficiez d'une solution simple et sûre pour éviter toute mauvaise surprise lors de l'achat d’une voiture d'occasion à un particulier.</P></div>
    <div class="circle">
        <a data-toggle="modal" data-target="#circle2"><img src="images/index/charte_qualite.png" class="img-responsive"></a>
        <h4>Une charte qualité</h4>
        <p class="hideSmart">Pour insérer une annonce sur autospot, les vendeurs particuliers doivent adhérer à notre charte qualité.</p></div>
    <div class="clear1280"></div>
    <div class="circle">
        <a data-toggle="modal" data-target="#circle3"><img src="images/index/points_controle.png" class="img-responsive"></a>
        <h4>Un examen approfondi</h4>
        <p class="hideSmart">En choisissant l'un de nos forfaits, nos garages partenaires effectuent 82 points de contrôle sur la voiture que vous envisagez d'acheter.</p></div>
				<div class="clear1920"></div>
				<!-- update -->
    <div class="circle">
        <a data-toggle="modal" data-target="#circle4"><img src="images/index/rapport_expertise.png" class="img-responsive"></a>
        <h4>Un rapport d’inspection</h4>
        <p class="hideSmart">Aussitôt l’examen de la voiture terminée, un rapport d’inspection détaillé vous est transmis par email et dans votre espace client.</p></div>
    <div class="clear1280"></div>
    <div class="circle">
        <a data-toggle="modal" data-target="#circle5"><img src="images/index/garantie-mecanique.png" class="img-responsive"></a>
        <h4>Une garantie mécanique</h4>
        <p class="hideSmart">Grâce à notre forfait ScanExpert Protect, bénéficiez d'une plus grande sécurité en optant pour la garantie mécanique.</p></div>
    <div class="circle">
        <a data-toggle="modal" data-target="#circle6"><img src="images/index/achat_serenite.png" class="img-responsive"></a>
        <h4>Un achat en toute sérénité</h4>
        <p class="hideSmart">Fini les soucis ! Autospot est désormais votre passeport sérénité pour l'achat d'une voiture d'occasion à un particulier.</p></div>
    <div class="clear1920"></div>
        
    <!-- Modal Circle 1 -->
<div id="circle1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title popTitle">Un service unique en Suisse</h2>
          <!--<img src="images/index/suisse.png" width="100" height="100" class="img-responsive popCercle">-->
          <div class="clear50"></div>
      </div>
      <div class="modal-body">
            <p>En tant qu'acheteur potentiel d'une voiture d’occasion à un particulier, vous vous êtes surement déjà posé certaines de ces questions simples mais primordiales :</p>
            
            <p>
                <ul>
                    <li>L'annonce représente-t-elle correctement le véhicule ?
                    <li>Comment vais-je communiquer dans une langue étrangère (suisse-allemand, suisse-italien) avec le vendeur ?</li>
                    <li>Quel est l'état réel du véhicule ?</li>
                </ul>
            </p>
            
            <p>Autospot est la 1ère plateforme en Suisse qui permet de répondre à ces questions sans que vous ayez besoin de vous déplacer de votre domicile.</p>
            <p>Aujourd'hui, grâce à AutoSpot, vous bénéficiez d&#39;une solution simple, sûre et rapide pour éviter toute mauvaise surprise lors de l’achat de votre voiture d’occasion à un particulier.</p>
            <p>Connaitre les points forts et les points faibles de la voiture, C'est ça la transparence !</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>
</div>
    
    <!-- Modal Circle 2 -->
<div id="circle2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title popTitle">Une charte qualité</h2>
          <!--<img src="images/index/charte_qualite.png" width="100" height="100" class="img-responsive popCercle">-->
      <div class="clear50"></div></div>
        
      <div class="modal-body">
        <p>Grâce à notre charte qualité, nous souhaitons proposer des annonces de qualité et à fortiori des vendeurs particuliers qui visent la transparence vis-à-vis des acheteurs.</p>

        <p>Selon notre charte qualité, lorsqu'un vendeur dépose son annonce sur autospot il accepte la possibilité qu'un acheteur potentiel demande à ce que la voiture du vendeur soit vérifié chez l’un de nos garages partenaires. Ce service est gratuit pour le vendeur.</p> 

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>
</div>
    
    <!-- Modal Circle 3 -->
<div id="circle3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title popTitle">Un examen approfondi</h2>
          <!--<img src="images/index/points_controle.png" width="100" height="100" class="img-responsive popCercle">-->
          <div class="clear50"></div>
      </div>
      <div class="modal-body">
        <p>En effectuant une demande d'inspection technique sur le véhicule que vous envisagez d'acheter, deux types de forfaits sont disponibles :</p>
        
        <ul>
            <li>Le forfait ScanExpert dure 60 minutes et comprend un examen statique et un essai routier de la voiture.</li>
            <li>Le forfait ScanExpert Protect (à venir) correspond au forfait ScanExpert rajouté d'une garantie mécanique.</li>
        </ul>

        <p>Le forfait ScanExpert, comprenant 82 points de contrôle, est structuré en 9 catégories :</p>
        
        <ul>
            <li>Photographies (int. et ext.) de la voiture, des documents et, le cas échéant, des éléments endommagés.</li>
            <li>Vérification de la conformité du véhicule.</li>
            <li>Vérification de la conformité des pièces administratives.</li>
            <li>Vérification de l&#39;entretien du véhicule.</li>
            <li>Examen détaillé des organes de sécurité (voiture au sol et levée)</li>
            <li>Examen détaillé de la mécanique et du châssis (voiture au sol et levée)</li>
            <li>Examen détaillé du vitrage et état de la carrosserie selon photos</li>
            <li>Examen détaillé de certains équipements.</li>
        </ul>
        
        <p>+ essai routier</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>
</div>
</div>    
    <!-- Modal Circle 5 -->
<div id="circle5" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title popTitle">Une garantie mécanique</h2>
          <!--<img src="images/index/garantie-mecanique.png" width="100" height="100" class="img-responsive popCercle">-->
          <div class="clear50"></div>
      </div>
      <div class="modal-body">
			Bientôt disponible
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>
</div>
    
    <!-- Modal Circle 4 -->
<div id="circle4" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title popTitle">Un rapport d'inspection</h2>
          <!--<img src="images/index/rapport_expertise.png" width="100" height="100" class="img-responsive popCercle">-->
          <div class="clear50"></div>
      </div>
      <div class="modal-body">
        <p>L’utilisateur qui a effectué la demande d’inspection technique reçoit le rapport d’inspection par email et dans son espace utilisateur immédiatement à la fin de l’inspection technique.</p>

        <p>En tant qu'acheteur potentiel, le rapport d’inspection, que vous recevez dans votre langue, vous permet de prendre rapidement connaissance de l'état réel de la voiture que vous envisagez d'acquérir <strong>sans vous déplacer de votre domicile</strong>.</p>

        <p>Plus de mauvaise surprise, plus d’arnaque possible !</p>

        <p>AutoSpot vous offre, désormais, une tranquillité d'esprit et vous procure l'assurance dont vous avez besoin lorsque vous achetez un véhicule d'occasion à un particulier.</p>

        <p>Nos rapports d'inspections normés ont été conçus et élaborés par nos services techniques et sont communs à tous les garages partenaires de notre réseau.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>
</div>
    
    <!-- Modal Circle 6 -->
<div id="circle6" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title popTitle">Un achat en toute sérénité</h2>
          <!--<img src="images/index/achat_serenite.png" width="100" height="100" class="img-responsive popCercle">-->
          <div class="clear50"></div>
      </div>
      <div class="modal-body">
            <p>L'achat d’un véhicule d’occasion à un vendeur particulier peut devenir une situation stressante et compliquée lorsque les acheteurs sont dépourvus de connaissances en mécaniques.</p>

            <p>Si à cela s'ajoute la barrière linguistique, fixer le rendez-vous avec le vendeur et, le cas échéant, négocier le prix, deviendra compliqué.</p>

            <p>Laissez à notre réseau de garages partenaires le soin de fixer le rendez-vous avec le vendeur et d'examiner avec attention le véhicule qui vous intéresse.</p>

            <p>Dès la fin de l'inspection technique, vous recevez votre rapport d'inspection en fichier PDF dans votre boite mail et dans votre espace utilisateur.</p>

            <p>Afin de communiquer avec un vendeur de langue étrangère, nous mettrons bientôt à votre disposition une plateforme d'échange dans votre espace utilisateur.</p>

            <p>Vous pourrez aisément transmettre votre décision et si nécessaire entamer les négociations dans votre langue avec le vendeur. Celui-ci les recevra dans sa propre langue.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>
</div>

</div>
</div>
    
<div class="clearIndex"></div>
