<?PHP
session_start();
include("header.php");
include("body.php");
?>
<!-- FORFAIT ScanExpert Protect -->
<div class="clear"></div>
<div class="container">
<div class="container center-block">
    
<div class="carteForfait maxW">
<div class="carteHead scan">
<p>ScanExpert <span class="protect_mk2">Protect</span></p>
</div>
<div class="carteBody" style="text-align:justify; text-justify:inter-word;">
<p>Forfait ScanExpert + garantie mécanique (à venir)</p><br />
<h3>Forfait ScanExpert</h3>
<?php include('scanexpert_controles.php'); ?>
</div>
<div class="carteFoot scan"></div>
</div>
     
</div>    
<div class="clear"></div>
</div>

<script type="text/javascript">
    jQuery('.scanexpert-controles-toggle').on('click', function(e) {
        $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle')
    });
    jQuery('.testRoutierControl').off('click');
    jQuery('.testRoutierControl').on('click', function(e) {
        $(this).toggleClass('fa-plus-circle fa-minus-circle')
    });
</script>

<?php include("footer.php"); ?>