<?php
if (!isset($media_query)) {
    $media_query = '1920';
}

if ((isset($_GET['r'])) && ($_GET['r'] == "Toutes")) {
    $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche ORDER BY priorite DESC, marque ASC");
} elseif (isset($_GET['r'])) {
    $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='$_GET[r]' ORDER BY priorite DESC, marque ASC");
} else {
    if ($_SERVER['SERVER_NAME'] == "www.motospot.ch") {
        $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='moto' ORDER BY priorite DESC, marque ASC");
    } else {
        $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='voiture' ORDER BY priorite DESC, marque ASC");
    }
}

$marqueslist = "";
while($marques = mysqli_fetch_object($requetetr)) {
    $quotedmarques = preg_quote($marques->marque);

    if ((!preg_match("/\|$quotedmarques\|/ui", $marqueslist)) && ($marques->marque != "")) {
        $marqueslist = $marqueslist."|$marques->marque|";
    }
}

$marqueslist = preg_replace("/^\|/", "", $marqueslist);
$marqueslist = preg_replace("/\|$/", "", $marqueslist);

if ((isset($_GET["marque"])) || (isset($getmarque))) {
    if (isset($getmarque)) {
        $marque = $getmarque;
    } else {
        $marque = $_GET["marque"];
    }

    if ($_GET['r'] == "Toutes") {
        $requetetr = mysqli_query($connect1, "SELECT modele FROM formulaires_recherche WHERE marque='$marque' ORDER BY modele ASC");
    } else {
        $requetetr = mysqli_query($connect1, "SELECT modele FROM formulaires_recherche WHERE marque='$marque' && choix='$_GET[r]' ORDER BY modele ASC");
    }

    $modeleslist = "";
    while ($modeles = mysqli_fetch_object($requetetr)) {
        if ((!preg_match("/\|$modeles->modele\|/", $modeleslist)) && ($modeles->modele != "")) {
            $modeleslist = $modeleslist . "|$modeles->modele|";
        }
    }

    $modeleslist = preg_replace("/^\|/", "", $modeleslist);
    $modeleslist = preg_replace("/\|$/", "", $modeleslist);
}
?>

<form class="recherche form-inline" role="form" action="index.php" method="get" style="display:inline">
    <input type="hidden" id="r" name="r" value="<?php if((preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) || ((isset($_GET['r'])) && ($_GET["r"] == "moto"))) echo "moto"; else echo "voiture";?>">

    <div class="form480<?php echo ($media_query == 480 ? ' criteres-mobile' : ''); ?>">

        <div class="form-group">
            <div class="formBG">
                <select class="form-control btn40" id="marque<?php echo $media_query; ?>" name="marque" onchange="changemarque(<?php echo $media_query; ?>);">
                    <option value="Toutes" <?php if(!isset($_GET["marque"])) echo "selected";?>><?PHP echo $LANG[32];?></option>
                    <?php
                    $i = 0;
                    $explode = explode("||", $marqueslist);

                    while (isset($explode[$i])) {
                        $marque = $explode[$i];

                        if (isset($getq) && preg_match("/$marque/i", $getq)) {
                            $getmarque = $marque;
                        }
                        ?>
                        <option value="<?php echo $marque; ?>" <?php if(((isset($_GET["marque"])) && ($_GET["marque"] == $marque)) || (isset($getq) && preg_match("/$marque/i", $getq))) echo "selected"; ?>><?php echo $marque; ?></option><?php
                        $i++;
                    }
                    ?>
                </select>

                <select class="form-control btn40" id="modele<?php echo $media_query; ?>" name="modele">
                    <option value="Tous" <?php if (!isset($_GET["modele"])) echo "selected";?>><?php echo $LANG[34];?></option>
                    <?php
                    $i = 0;
                    if ($modeleslist != "") {
                        $explode = explode("||", $modeleslist);

                        while(isset($explode[$i])) {
                            $modele = $explode[$i];
                            ?><option value="<?php echo $modele;?>" <?php if(((isset($_GET["modele"])) && ($_GET["modele"] == $modele)) || (preg_match("/$modele/i", $getq))) echo "selected";?>><?PHP echo $modele;?></option><?php
                            $i++;
                        }
                    }
                    ?>
                </select>

                <div style="display:none">
                    <select class="form-control btn40" id="modelebis<?php echo $media_query; ?>" onchange="document.getElementById('modele').value=document.getElementById('modelebis').value;">
                        <option value="Tous" <?php if(!isset($_GET["modele"])) echo "selected";?>><?php echo $LANG[34]; ?></option>
                        <?php
                        $i = 0;
                        if ($modeleslist != "") {
                            $explode = explode("||", $modeleslist);

                            while(isset($explode[$i])) {
                                $modele = $explode[$i];
                                ?><option value="<?php echo $modele; ?>" <?php if (((isset($_GET["modele"])) && ($_GET["modele"] == $modele)) || (preg_match("/$modele/i", $getq))) echo "selected";?>><?PHP echo $modele;?></option><?php
                                $i++;
                            }
                        }
                        ?>
                    </select>
                </div>

                <?php
                if ($media_query == 480) {
                    $onclick = "document.getElementById('marque1280').disabled=true;document.getElementById('modele1280').disabled=true;document.getElementById('marque1920').disabled=true;document.getElementById('modele1920').disabled=true;submit();";
                } elseif ($media_query == 1280) {
                    $onclick = "document.getElementById('marque480').disabled=true;document.getElementById('modele480').disabled=true;document.getElementById('marque1920').disabled=true;document.getElementById('modele1920').disabled=true;submit();";
                } else {
                    $onclick = "document.getElementById('marque1280').disabled=true;document.getElementById('modele1280').disabled=true;document.getElementById('marque480').disabled=true;document.getElementById('modele480').disabled=true;submit();";
                }
                ?>
                <button class="btn btn-primary btn20" type="button" onclick="<?php echo $onclick; ?>"><i class="fa fa-search"></i></button>
            </div>

            <p class="formPlus"><a href="index.php?r=voiture&marque=Toutes&modele=Tous">+ de critères de recherche</a></p>
            <p class="formPlus"><a href="index.php?r=voiture&marque=Toutes&modele=Tous">Visualiser toutes les annonces</a></p>

            <div class="search_by_reference visible-xs hidden-sm visible-md visible-lg">
                <br />
                <input type="text" class="form-control btn80" placeholder="Numéro d'annonce" name="reference" />
                <button class="btn btn-primary btn20" type="submit" disabled="disabled"><i class="fa fa-search"></i></button>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</form>