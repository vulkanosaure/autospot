<!-- INDEX FOOTER -->

<div id="indexFooter">
    <div id="footContain">

        <!-- colonne 1 -->
        <div class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-3">
            <div id="footerVente">
                <img src="/images/index/image_vente.jpg" class="img-responsive" alt="" />
                <a href="/publier.php" class="btn btn-autofoot" role="button">Je publie mon annonce</a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clearfix visible-xs"></div>

        <!-- colonne 2 -->
        <div class="col-sm-3">
            <p>Service clients 7/7 - 7h00 - 22h00</p>
            <p><a href="/contact.php">Formulaire de contact</a></p>
            <div class="clear"></div>
        </div>

        <!-- colonne 3 -->
        <div class="col-sm-3">
            <p><a href="/qui_sommes_nous.php">Qui sommes-nous?</a></p>
            <p><a href="/cgu.php">CGU</a></p>
            <p><a href="/mentions_legales.php">Mentions légales</a></p>
            <p><a href="/regles_insertion.php">Règles d'insertion</a></p>
            <p><a href="/nos-garages-partenaires.php">Nos garages partenaires</a></p>
            <p><a href="/adhesion-reseau-partenaires.php">Adhérer à notre réseau de garages partenaires</a></p>
            <p><a href="/banniere.php">Offre promotionnelle</a></p>
            <div class="clear"></div>
        </div>

        <!-- colonne 4 -->
        <div class="col-sm-3 text-center">
            <div class="fb-page" data-href="https://www.facebook.com/Autospot-678599932238243/?fref=ts" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Autospot-678599932238243/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Autospot-678599932238243/?fref=ts">Autospot</a></blockquote></div>

        </div>
        <div class="clear"></div>
        <!-- foot -->
        <div class="col-sm-12 copyrights"> copyrights © <?= date('Y') ;?> - <a href="http://www.autospot.ch" target="_blank">Autospot.ch</a> by <a href="http://www.skaiweb.com" target="_blank"><img src="images/index/logo_skaiweb-78x45.png"></a> - Tous droits réservés
        </div>
        <div class="clear"></div>

    </div>
</div>
   <script src="js/nav1.js" data-minify="1"></script>
                        <script src="js/navsb1.js" data-minify="1"></script> 
<!-- Script back to top -->
<script>
    jQuery(document).ready(function ($) {

        $('.toTop').click(function () {
            $("html, body").animate({scrollTop: 0}, 600);
            return false;
        });
             $('a.mk_forfit').on('click',function (e) {
        // e.preventDefault();

        var target = this.hash;
        var $target = $(target);
          $('html, body').animate({
                scrollTop: $target.offset().top
            }, 2000);

                // $('html, body').stop().animate({
                //     'scrollTop': $target.offset().top
                // }, 900, 'swing', function () {
                //     window.location.hash = target;
                // });
            });
    });
</script> 

<script src="js/nav.js" data-minify="1"></script>
<script src="js/navsb.js" data-minify="1"></script>
<?php 
if(isset($_GET['annonces']))
{
    ?>
    <script>
      jQuery(document).ready(function($) {
        console.log("hi");
               $('html,body').animate({
        scrollTop: $("#annonces").offset().top-40},
        'slow');

       }); 
    </script>
    <?php

}
 ?>
<?php 
if(isset($_GET['demandes']))
{
    ?>
    <script>
      jQuery(document).ready(function($) {
        console.log("hi");
                   $('#demandes').addClass('in'); console.log('hieeeee');

               $('html,body').animate({
        scrollTop: $("#demandes").offset().top-40},
        'slow');

       }); 
    </script>
    <?php

}
 ?>

 <?php 
if(isset($_GET['favoris']))
{
    ?>
    <script>
      jQuery(document).ready(function($) {
        console.log("hi");
                   $('#favoris').addClass('in'); console.log('hieeeee');
        
               $('html,body').animate({
        scrollTop: $("#favoris").offset().top-40},
        'slow');

       }); 
    </script>
    <?php

}
 ?>
  <?php 
if(isset($_GET['profil']))
{
    ?>
    <script>
      jQuery(document).ready(function($) {
        console.log("hi");
                   $('#profil').addClass('in'); console.log('hieeeee');
        
               $('html,body').animate({
        scrollTop: $("#profil").offset().top-40},
        'slow');

       }); 
    </script>
    <?php

}
 ?>
<script src="js/bootstrap-notify.min.js"></script>  
<script src="js/custom_form.js"></script>
<script src="js/index.js"></script>
</body>
</html>