<?php
session_start();
require_once('vendor/autoload.php');
require_once('classes/PHPMailer2.php');

if (!isset($_SESSION['id_client']))
{
    header("Location: inscription.php");
    exit();
}

include("db_connexion.php");

$sql = sprintf('SELECT email FROM clients WHERE id = "%s"', mysqli_real_escape_string($connect1, $_SESSION['id_client']));
$query = mysqli_query($connect1, $sql);
$client = mysqli_fetch_object($query);

if (isset($_POST['sauvegarder']))
{
    if ($_POST['emailNew'] != $_POST['emailConfirm']) {
        $erreur = "La confirmation de votre nouvelle adresse e-mail ne correspond pas. Veuillez réessayer!";
    } else {
        $sql = sprintf(
            'UPDATE clients SET email_new = "%s" WHERE id = "%s"',
            mysqli_real_escape_string($connect1, $_POST['emailNew']),
            $_SESSION['id_client']
        );
        mysqli_query($connect1, $sql);

        $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
        $twig = new \Twig_Environment($loader);
        $body = $twig->render('modifier_mail.html.twig', ['newEmail' => $_POST['emailNew'], 'url' => sprintf('http://www.autospot.ch/modifier_mail_confirmation.php?key=%s', md5($client->email))]);
        
        $mail = new PHPMailer2();
        
        $mail->setFrom('contact@autospot.ch', 'AutoSpot');
        $mail->addAddress($client->email);
        $mail->addReplyTo('no-reply@autospot.ch', 'Ne pas répondre');
        $mail->isHTML(true);
        $mail->Subject = 'Confirmez votre nouvelle adresse email';
        $mail->Body = $body;
        $mail->send();

        $notice = 'Un email de confirmation a été envoyé à votre ancienne adresse';
    }
}

include("header.php");
include("body.php");
?>
<a id="haut"></a>

<!-- ESPACE UTILISATEUR -->
<div class="container-fluid">
    <h3 class="center">Modifier mon adresse email</h3>
    <hr class="blue">
    <div class="clear"></div>
    
    <div id="modifEmail" class="col-sm-12">
        <form id="modifMail" class="form-horizontal" method="POST">
            <?php if (isset($erreur)) : ?>
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <div class="alert alert-danger">
                        <?php echo $erreur; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php elseif (isset($notice)) : ?>
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <div class="alert alert-success">
                        <?php echo $notice; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php endif; ?>

  <div class="form-group">
    <label class="control-label col-sm-3" for="emailActu">Adresse e-mail actuelle <span class="requis">*</span></label>
    <div class="col-sm-6">
      <input type="email" class="form-control" id="emailActu" name="emailActu" placeholder="" required value="<?php echo $client->email; ?>" disabled />
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3" for="emailNew">Nouvelle adresse email <span class="requis">*</span></label>
    <div class="col-sm-6"> 
      <input type="email" class="form-control" id="emailNew" name="emailNew" placeholder="" required>
    </div>
  </div>
    <div class="form-group">
    <label class="control-label col-sm-3" for="emailConfirm">Confirmer la nouvelle adresse email <span class="requis">*</span></label>
    <div class="col-sm-6"> 
      <input type="email" class="form-control" id="emailConfirm" name="emailConfirm" placeholder="" required>
    </div>
  </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-3 col-sm-6">
      <a class="btn btn-default" href="/compte.php?profil">Annuler</a>
      <button type="submit" name="sauvegarder" class="btn btn-primary">Envoyer</button>
    </div>
  </div>
</form>
    </div>
    
    <div class="clear"></div>
</div>
<!-- FIN ESPACE UTILISATEUR -->
</div>
<?php include("footer.php"); ?>