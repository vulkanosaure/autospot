<?PHP session_start();
include("header.php");
include("body.php");
?>
<!-- QUI SOMMES NOUS -->
<div class="container">
<h3 class="center">Qui sommes-nous?</h3>

<div id="qui" class="container center-block">
<p>Le concept est né en 2018 à Oron-la-Ville (Vaud) suite à l’expérience vécue par son fondateur Valentin.</p> 

<p>La famille de Valentin s’agrandit avec l’arrivé du 3ème enfant. Pas le choix, il faut acheter une nouvelle voiture, cette fois-ci plus grande. Le choix se porte rapidement sur un véhicule d’occasion car disponible de suite et possédant un très bon rapport qualité/prix comparé à un véhicule neuf.</p>

<p>Premier reflex, se rendre sur un site de petites annonces bien connu en suisse romande. Mais c’est à ce moment là que le parcours du combattant commence car Valentin n’est ni mécanicien, ni carrossier et encore moins expert automobile.</p>

<p>Non seulement il se retrouve vite perdu parmi la jungle des annonces de voitures d’occasions mais remarque qu’il est surtout difficile de connaître à l’avance leur historique.<br/>
S’ensuit la corvée des appels téléphoniques aux vendeurs, la difficulté, lorsqu’on n’est pas bilingue, à discuter en suisse-allemand et les soupçons d’arnaques auxquelles il a du faire face.</p>

<p>Finalement, quelques cheveux blancs plus tard, Valentin se résigne à restreindre son champ de recherche à la suisse romande mais une nouvelle idée vient de germer dans son esprit, celle de proposer un service innovant permettant à tout et chacun d’acquérir une voiture d’occasion sans stress, partout en Suisse, et surtout sans aucunes connaissances particulières en automobile.<br/> 
<b>AutoSpot est né !</b></p>

<p>AutoSpot vous offre une tranquillité d'esprit et vous procure l'assurance dont vous avez besoin lorsque vous achetez un véhicule d'occasion.</p>
    
<!--<img src="images/index/image_voiture_quisommesnous.png" class="center img-responsive hideSmart">
<img src="images/index/image_voiture_quisommesnous-mob.png" class="center img-responsive showSmart">-->
    
<p>Si vous avez d’éventuelles questions, remarques ou suggestions, n’hésitez pas à nous les transmettre via le formulaire de contact. Notre équipe se fera un plaisir de vous répondre .</p>
</div>    
<div class="clear50"></div>

<!-- /QUI SOMMES NOUS -->
</div>
<?php include("footer.php"); ?>