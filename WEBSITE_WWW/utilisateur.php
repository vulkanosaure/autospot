<?php include("header.php"); ?>
<a id="haut"></a>

<!-- ESPACE UTILISATEUR -->
<div class="container">
    
<h3 class="center">Espace utilisateur</h3>

<div class="panel-group col-sm-12" id="utilisateur">
    
    <!-- *** AFFICHAGE DES MENUS *** -->
    
    <!-- MENU ANNONCES --> 
    <div class="panel panel-blank col-sm-3">
      <div class="panel-heading">
            <a href="#annonces" data-toggle="collapse" data-target="#annonces" data-parent="#utilisateur"><img src="images/utilisateur/picto_mes_annonces.png" alt="mes annonces" class="img-responsive center-block"></a>
           <a href="#annonces" data-toggle="collapse" data-target="#annonces" data-parent="#utilisateur" class="btn btn-autospot" role="button">Mes annonces</a>
          <div class="clear"></div>
      </div>  
    </div>
    
    <!-- MENU DEMANDES --> 
    <div class="panel panel-blank col-sm-3">
      <div class="panel-heading">
        <a href="#demandes" data-toggle="collapse" data-target="#demandes" data-parent="#utilisateur"><img src="images/utilisateur/picto_suivi_demandes.png" alt="mes demandes" class="img-responsive center-block"></a>
        <a href="#demandes" data-toggle="collapse" data-target="#demandes" data-parent="#utilisateur" class="btn btn-autospot" role="button">Suivi de mes demandes</a>
          <div class="clear"></div>
      </div>
    </div>
    
    <!-- MENU FAVORIS --> 
    <div class="panel panel-blank col-sm-3">
      <div class="panel-heading">
        <a href="#favoris" data-toggle="collapse" data-target="#favoris" data-parent="#utilisateur"><img src="images/utilisateur/picto_mes_favoris.png" alt="mes favoris" class="img-responsive center-block"></a>
        <a href="#favoris" data-toggle="collapse" data-target="#favoris" data-parent="#utilisateur" class="btn btn-autospot" role="button">Mes favoris</a>
          <div class="clear"></div>
      </div>   
    </div>
    
    <!-- MENU PROFIL --> 
    <div class="panel panel-blank col-sm-3">
      <div class="panel-heading">
        <a href="#profil" data-toggle="collapse" data-target="#profil" data-parent="#utilisateur"><img src="images/utilisateur/picto_mon_profil.png" alt="mon profil" class="img-responsive center-block"></a>
        <a href="#profil" data-toggle="collapse" data-target="#profil" data-parent="#utilisateur" class="btn btn-autospot" role="button">Mon profil</a>
          <div class="clear"></div>
      </div>
    </div>
    
    <!-- *** AFFICHAGE DES SECTIONS *** -->
    <div class="clear"></div>

    <!-- MES ANNONCES --> 
    <div class="panel panel-blank col-sm-12">
        <div id="annonces" class="panel-collapse collapse">
            <hr class="blue">
            <h3>Mes annonces <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#annonces" data-parent="#utilisateur">X</button></h3>
            <hr class="blue">
        <!-- pas d'annonces -->
    <div id="annoncesVide" class="col-sm-12">
        <p class="larger">Vous n'avez pour l'instant aucune annonce publiée</p>
        <a href="publier.php" class="btn btn-primary" role="button">Publier une annonce maintenant</a>
    
        <div class="clear"></div>
        <font color="red">ou selon la réponse serveur...</font>
        <div class="clear"></div>  
    </div>
    
    <!-- Liste des annonces -->
    <!-- annonce 1 -->
	<div id="annonce-101" class="utilTab">
		<div class="col-sm-3">
            <img src="images/cars/no_image.png" class="img-responsive">
		</div>
		<div class="col-sm-9">
			<div class="row bgBlue">
				<div class="col-sm-4">
                    Marque & modèle
				</div>
				<div class="col-sm-2">
                    Date
				</div>
				<div class="col-sm-2">
                    Km
				</div>
				<div class="col-sm-2">
                    Prix
				</div>
				<div class="col-sm-2">
                    <div class="checkbox checkbox-slider--b">
                        <label><input type="checkbox" id="aciveAnnonce-101"><span><span></span></span></label>
                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
                    Cylindrée
				</div>
				<div class="col-sm-3">
                    Puissance
				</div>
				<div class="col-sm-3">
                    Carburant
				</div>
				<div class="col-sm-3">
                    Expire le ...
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
                    Boîte
				</div>
				<div class="col-sm-3">
                    Expertise
				</div>
				<div class="col-sm-3">
                    Lieu
				</div>
				<div class="col-sm-3">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
                    <span class="picto numero">N° de l'annonce</span>
                    <span class="picto voir">Voir l'annonce</span>
                    <span class="picto modifier">Modifier</span>
                    <span class="picto visualiser">Visualiser le rapport Securspot</span>
                    <span class="picto supprimer">Supprimer</span>
				</div>
			</div>
		</div>
	</div>
    <div class="clear"></div>
    <!-- annonce 2 -->
	<div id="annonce-102" class="utilTab">
		<div class="col-sm-3">
            <img src="images/cars/no_image.png" class="img-responsive">
		</div>
		<div class="col-sm-9">
			<div class="row bgBlue">
				<div class="col-sm-4">
                    Marque & modèle
				</div>
				<div class="col-sm-2">
                    Date
				</div>
				<div class="col-sm-2">
                    Km
				</div>
				<div class="col-sm-2">
                    Prix
				</div>
				<div class="col-sm-2">
                    <div class="checkbox checkbox-slider--b">
                        <label><input type="checkbox" id="aciveAnnonce-102"><span><span></span></span></label>
                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
                    Cylindrée
				</div>
				<div class="col-sm-3">
                    Puissance
				</div>
				<div class="col-sm-3">
                    Carburant
				</div>
				<div class="col-sm-3">
                    Expire le ...
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
                    Boîte
				</div>
				<div class="col-sm-3">
                    Expertise
				</div>
				<div class="col-sm-3">
                    Lieu
				</div>
				<div class="col-sm-3">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
                    <span class="picto numero">N° de l'annonce</span>
                    <span class="picto voir">Voir l'annonce</span>
                    <span class="picto modifier">Modifier</span>
                    <span class="picto visualiser">Visualiser le rapport Securspot</span>
                    <span class="picto supprimer">Supprimer</span>
				</div>
			</div>
		</div>
	</div>
    <div class="clear"></div>
      </div>
    </div>
    <!-- FIN MES ANNONCES --> 
    
    <!-- MES DEMANDES -->
    <div class="panel panel-blank col-sm-12">
        <div id="demandes" class="panel-collapse collapse">
            <hr class="blue">
            <h3>Suivi de mes demandes <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#demandes" data-parent="#utilisateur">X</button></h3>
            <hr class="blue">
            
         <!-- pas de demandes -->
    <div id="demandesVide" class="col-sm-12">
        <p class="larger">Vous n'avez pour l'instant aucune demande d'expertise</p>
        <div class="clear"></div>
        <font color="red">ou selon la réponse serveur...</font>
        <div class="clear"></div>  
    </div>
    
    <!-- Liste des demandes -->
        
    <!-- demandes 1 -->
	<div id="demandes-101" class="utilTab">
        <p>Vous avez effectué une demande d'expertise le ... pour le véhicule ...</p>
		<div class="col-sm-3">
            <img src="images/cars/no_image.png" class="img-responsive">
		</div>
		<div class="col-sm-9">
			<div class="row bgBlue">
				<div class="col-sm-6">
                    Marque & modèle
				</div>
				<div class="col-sm-2">
                    Date
				</div>
				<div class="col-sm-2">
                    Km
				</div>
				<div class="col-sm-2">
                    Prix
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
                    Cylindrée
				</div>
				<div class="col-sm-2">
                    Puissance
				</div>
				<div class="col-sm-2">
                    Carburant
				</div>
				<div class="col-sm-6">
                    Ici l'emplacement de la description rapide étalée sur deux lignes, ici l'emplacement de la description rapide étalée sur deux lignes... 
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
                    Boîte
				</div>
				<div class="col-sm-2">
                    Expertise
				</div>
				<div class="col-sm-2">
                    Lieu
				</div>
				<div class="col-sm-6">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
                    <span class="picto numero">N° de l'annonce</span>
                    <span class="picto voir">Voir l'annonce</span>
                    <span class="picto visualiser">Visualiser le rapport Securspot</span>
				</div>
			</div>
		</div>
        
        <div class="clear"></div>
        
        <div class="suiviDemandes center-block">
        
        <div class="suiviA"><p>Votre demande d'expertise N° ..., du ... a bie été prise en compte</p></div>
        <div class="suiviB"><p>La date du contrôle technique a été fixée au ...</p></div>
        <div class="suiviC"><p>Le rapport d'expertise est désormais disponible dans votre espace utilisateur</p></div>
        <div class="suiviD"><p>Notre expert automobile aprtenaire a terminé le contrôle technique</p></div>
            
        </div>
        
    <div class="clear"></div>
        
    </div>
      </div>
    </div>
    <!-- FIN MES DEMANDES -->
    
    <!-- MES FAVORIS -->
    <div class="panel panel-blank col-sm-12">
        <div id="favoris" class="panel-collapse collapse">
            <hr class="blue">
            <h3>Mes favoris <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#favoris" data-parent="#utilisateur">X</button></h3>
            <hr class="blue">
            
        <!-- pas de favoris -->
    <div id="favorisVide" class="col-sm-12">
        <p class="larger">Vous n'avez pour l'instant aucune annonce enregistrée dans vos favoris</p>
        <div class="clear"></div>
        <font color="red">ou selon la réponse serveur...</font>
        <div class="clear"></div>  
    </div>
    
    <!-- Liste des favoris -->
        <p>X annonces classées dans vos favoris</p>
    <!-- favoris 1 -->
	<div id="favoris-101" class="utilTab">
		<div class="col-sm-3">
            <img src="images/cars/no_image.png" class="img-responsive">
		</div>
		<div class="col-sm-9">
			<div class="row bgBlue">
				<div class="col-sm-6">
                    Marque & modèle
				</div>
				<div class="col-sm-2">
                    Date
				</div>
				<div class="col-sm-2">
                    Km
				</div>
				<div class="col-sm-2">
                    Prix
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
                    Cylindrée
				</div>
				<div class="col-sm-2">
                    Puissance
				</div>
				<div class="col-sm-2">
                    Carburant
				</div>
				<div class="col-sm-6">
                    Ici l'emplacement de la description rapide étalée sur deux lignes, ici l'emplacement de la description rapide étalée sur deux lignes... 
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
                    Boîte
				</div>
				<div class="col-sm-2">
                    Expertise
				</div>
				<div class="col-sm-2">
                    Lieu
				</div>
				<div class="col-sm-6">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
                    <span class="picto supprimer">Supprimer</span>
                    <span class="picto voir">Voir l'annonce</span>
                    <span class="picto visualiser">Visualiser le rapport Securspot</span>
				</div>
			</div>
		</div>
	</div>
    <div class="clear"></div>
    <!-- favoris 2 -->
	<div id="favoris-102" class="utilTab">
		<div class="col-sm-3">
            <img src="images/cars/no_image.png" class="img-responsive">
		</div>
		<div class="col-sm-9">
			<div class="row bgBlue">
				<div class="col-sm-6">
                    Marque & modèle
				</div>
				<div class="col-sm-2">
                    Date
				</div>
				<div class="col-sm-2">
                    Km
				</div>
				<div class="col-sm-2">
                    Prix
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
                    Cylindrée
				</div>
				<div class="col-sm-2">
                    Puissance
				</div>
				<div class="col-sm-2">
                    Carburant
				</div>
				<div class="col-sm-6">
                    Ici l'emplacement de la description rapide étalée sur deux lignes, ici l'emplacement de la description rapide étalée sur deux lignes... 
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
                    Boîte
				</div>
				<div class="col-sm-2">
                    Expertise
				</div>
				<div class="col-sm-2">
                    Lieu
				</div>
				<div class="col-sm-6">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
                    <span class="picto supprimer">Supprimer</span>
                    <span class="picto voir">Voir l'annonce</span>
                    <span class="picto visualiser">Visualiser le rapport Securspot</span>
				</div>
			</div>
		</div>
	</div>
    <div class="clear"></div>
      </div>
    </div>
    <!-- FIN MES FAVORIS --> 
    
    <!-- MON PROFIL -->
    <div class="panel panel-blank col-sm-12">
        <div id="profil" class="panel-collapse collapse">
        <hr class="blue">
        <h3>Mon profil <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#profil" data-parent="#utilisateur">X</button></h3>
        <hr class="blue">
            <div class="clear"></div>

        <div class="col-sm-4">
            <div class="clear"></div>
        <a href="modifier_mail.php"><img src="images/utilisateur/picto_modifier_mail.png" alt="Modifier mon email" class="img-responsive center-block"></a>
        <a href="modifier_mail.php" class="btn btn-autospot" role="button">Modifier mon email</a>
        </div>
        
        <div class="col-sm-4">
            <div class="clear"></div>
        <a href="modifier_coordonnees.php"><img src="images/utilisateur/picto_modifier_coordonnees.png" alt="Modifier mes coordonnées" class="img-responsive center-block"></a>
        <a href="modifier_coordonnees.php" class="btn btn-autospot" role="button">Modifier mes coordonnées</a>
        </div>
        
        <div class="col-sm-4">
            <div class="clear"></div>
        <a href="modifier_passe.php"><img src="images/utilisateur/picto_modifier_passe.png" alt="Modifier mon mot de passe" class="img-responsive center-block"></a>
        <a href="modifier_passe.php" class="btn btn-autospot" role="button">Modifier mon mot de passe</a>
        </div>
      </div>
    </div>
    <!-- FIN MON PROFIL --> 
    
    
</div>
    
</div>

<div class="clear"></div>
<!-- FIN ESPACE UTILISATEUR -->


<div class="container">
<?php include("index_footer.php"); ?>