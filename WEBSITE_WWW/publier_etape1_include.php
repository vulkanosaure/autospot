<?php

function getPost($_key)
{
	if(isset($_POST[$_key])) return $_POST[$_key];
	return '';
}

$parution = date("Y-m-d"); // OK
$marque = mysqli_real_escape_string($connect1, $_POST['marque']); // OK
$modele = mysqli_real_escape_string($connect1, $_POST['modele']); // OK
$version = mysqli_real_escape_string($connect1, $_POST['version']); // OK
$premiereimm = mysqli_real_escape_string($connect1, $_POST['premiereimm'].'-01'); // OK A VOIR LE FORMAT DATE YYYY-MM-DD sur le formulaire
$premiereimm_explode = explode("-", $premiereimm);
$year = $premiereimm_explode[0]; // OK
$derniereimm = mysqli_real_escape_string($connect1, $_POST['derniereimm'].'-01'); // OK A VOIR LE FORMAT DATE YYYY-MM-DD sur le formulaire
$expertisee = $derniereimm;
$cylindrees = mysqli_real_escape_string($connect1, getPost('cylindrees')); // OK
$puissancechevaux = mysqli_real_escape_string($connect1, $_POST['puissancechevaux']); // OK
$couleurexterne = mysqli_real_escape_string($connect1, $_POST['couleurexterne']); // OK
$metallisee = mysqli_real_escape_string($connect1, getPost('metallisee')); // OK
$categorie = mysqli_real_escape_string($connect1, $_POST['categorie']); // OK
$carrosserie = mysqli_real_escape_string($connect1, $_POST['carrosserie']); // OK
$transmission = mysqli_real_escape_string($connect1, $_POST['transmission']); // OK
$rouesMotrices = mysqli_real_escape_string($connect1, $_POST['rouesMotrices']); // OK
$vitesses = mysqli_real_escape_string($connect1, $_POST['vitesses']); // OK
$portes = mysqli_real_escape_string($connect1, $_POST['portes']); // OK
$sieges = mysqli_real_escape_string($connect1, getPost('sieges')); // OK
$carburant = mysqli_real_escape_string($connect1, $_POST['carburant']); // OK
$kilometrage = mysqli_real_escape_string($connect1, $_POST['kilometrage']); // OK
$prix = mysqli_real_escape_string($connect1, $_POST['prix']); // OK
$prixneuf = mysqli_real_escape_string($connect1, $_POST['prixneuf']); // OK
$discuter = mysqli_real_escape_string($connect1, getPost('discuter'));
$desc1 = mysqli_real_escape_string($connect1, $_POST['desc1']); // OK
$desc2 = mysqli_real_escape_string($connect1, $_POST['desc2']); // OK
$equipement = "";

$demarre = isset($_POST['demarre']) ? mysqli_real_escape_string($connect1, $_POST['demarre']) : ''; // OK
$deplace = isset($_POST['deplace']) ? mysqli_real_escape_string($connect1, $_POST['deplace']) : ''; // OK

$discuter = ($discuter == 'oui') ? 1 : 0;


if(isset($_POST['airbag']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['airbag']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['airbagL']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['airbagL']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['isofix']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['isofix']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['feuxAv']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['feuxAv']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['feuxAr']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['feuxAr']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['phareDir']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['phareDir']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['xenon']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['xenon']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['verrCent']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['verrCent']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['verrCentDist']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['verrCentDist']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['x3feustop']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['x3feustop']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['ledRetro']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['ledRetro']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['abs']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['abs']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['asr']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['asr']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['esp']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['esp']);
    $equipement = $equipement.$var.",";
}
// EQUIPEMENT SUITE
if(isset($_POST['dirAss']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['dirAss']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['clim']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['clim']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['climAuto']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['climAuto']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['vitreTeinte']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['vitreTeinte']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['vitreElec']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['vitreElec']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['volantCuir']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['volantCuir']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['volantChauf']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['volantChauf']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['siegeCuir']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['siegeCuir']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['siegeChauf']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['siegeChauf']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['detecteurPluie']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['detecteurPluie']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['essuieInter']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['essuieInter']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['regVitesse']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['regVitesse']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['startstop']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['startstop']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['retroElec']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['retroElec']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['parcageAr']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['parcageAr']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['parcageAvAr']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['parcageAvAr']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['cameraRecul']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['cameraRecul']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['barreToit']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['barreToit']);
    $equipement = $equipement.$var.",";
}
// EQUIPEMENT SUITE
if(isset($_POST['ordinateur']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['ordinateur']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['gps']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['gps']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['radio']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['radio']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['usb']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['usb']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['volantMultiF']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['volantMultiF']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['bluetooth']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['bluetooth']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['toitPano']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['toitPano']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['toitOuvrant']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['toitOuvrant']);
    $equipement = $equipement.$var.",";
}
// EQUIPEMENT SUITE
if(isset($_POST['spoilerToit']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['spoilerToit']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['jupes']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['jupes']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['chassisSport']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['chassisSport']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['siegeSport']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['siegeSport']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['retroChrome']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['retroChrome']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['barreToitAlu']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['barreToitAlu']);
    $equipement = $equipement.$var.",";
}
// EQUIPEMENT SUITE
if(isset($_POST['roueSecours']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['roueSecours']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['x2PneusRoues']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['x2PneusRoues']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['antiCrevaison']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['antiCrevaison']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['hardTop']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['hardTop']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['tapisCaoutchouc']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['tapisCaoutchouc']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['tapisRevetus']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['tapisRevetus']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['crochetAttelage']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['crochetAttelage']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['grille']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['grille']);
    $equipement = $equipement.$var.",";
}
// EQUIPEMENT SUITE
if(isset($_POST['importDirectPara']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['importDirectPara']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['handicape']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['handicape']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['tuning']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['tuning']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['voitureCourse']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['voitureCourse']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['nonFumeur']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['nonFumeur']);
    $equipement = $equipement.$var.",";
}
if(isset($_POST['exportation']))
{
    $var = mysqli_real_escape_string($connect1, $_POST['exportation']);
    $equipement = $equipement.$var.",";
}
// ACCIDENT OK
$accident = "";
if(isset($_POST['accident']))
    $accident = mysqli_real_escape_string($connect1, $_POST['accident']);
// typeGarantie OK
$typeGarantie = "";
if(isset($_POST['typeGarantie']))
{
    $typeGarantie = mysqli_real_escape_string($connect1, $_POST['typeGarantie']);
    $dureeGarantie = mysqli_real_escape_string($connect1, $_POST['dureeGarantie']);
}
// plaques OK
$plaques = "";
if(isset($_POST['plaques'])) $plaques = mysqli_real_escape_string($connect1, $_POST['plaques']);
	
if($modifier == 0)
{
	/*///////////////////////////////////////////////////////////////*/
	$sql = "INSERT INTO annonces_clients (id_client, parution, choix, nombreProprio, typeGarantie, dureeGarantie, accident, desc1, desc2, marque, modele, version, prix, prixneuf, prixdiscuter, premiereimm, derniereimm, expertisee, year, kilometrage, transmission, carburant, carrosserie, couleurexterne, metallisee, rouesmotrices, puissancechevaux, cylindrees, categorie, portes, vitesses, sieges, plaques, equipement, demarre, deplace) VALUES ('$_SESSION[id_client]', '$parution', 'voiture', 0, '$typeGarantie', '$dureeGarantie', '$accident', '$desc1', '$desc2', '$marque', '$modele', '$version', '$prix', '$prixneuf', '$discuter', '$premiereimm', '$derniereimm', '$expertisee', '$year', '$kilometrage', '$transmission', '$carburant', '$carrosserie', '$couleurexterne', '$metallisee', '$rouesMotrices', '$puissancechevaux', '$cylindrees', '$categorie', '$portes', '$vitesses', '$sieges', '$plaques', '$equipement', '$demarre', '$deplace')";
	// echo $sql;
	mysqli_query($connect1, $sql) or die(mysqli_error($connect1));
    $id_annonce_sql = mysqli_query($connect1, "SELECT id FROM annonces_clients WHERE id_client='$_SESSION[id_client]' ORDER BY id DESC LIMIT 1");
    $id_annonce_array = mysqli_fetch_array($id_annonce_sql);
	$id_annonce = $id_annonce_array['id'];
	// exit("");
	
	
	
	//send emails
	
	require_once('classes/client.php');
	require_once('classes/annonce.php');
	
	require_once("appbackend/admin/include/debug.php");
	require_once("appbackend/admin/include/db_connect.php");
	require_once("appbackend/events_utils.php");
	require_once("appbackend/include/email_utils.php");
	
	/* 
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	*/
	
	$client = client::getById($_SESSION['id_client']);
	$key_email = ($client->type == 0) ? "seller_insert_announce" : "seller_insert_announce2";
	
	handleEmails($dbh, $key_email, "seller", "", "insertion_announce", "", $id_annonce, []);
	
}
else
{
    mysqli_query($connect1, "UPDATE annonces_clients SET parution='$parution', typeGarantie='$typeGarantie', dureeGarantie='$dureeGarantie', accident='$accident', desc1='$desc1', desc2='$desc2', marque='$marque', modele='$modele', version='$version', prix='$prix', prixneuf='$prixneuf', prixdiscuter='$discuter', premiereimm='$premiereimm', derniereimm='$derniereimm', year='$year', kilometrage='$kilometrage', transmission='$transmission', carburant='$carburant', carrosserie='$carrosserie', couleurexterne='$couleurexterne', metallisee='$metallisee', rouesmotrices='$rouesMotrices', puissancechevaux='$puissancechevaux', cylindrees='$cylindrees', categorie='$categorie', portes='$portes', vitesses='$vitesses', sieges='$sieges', plaques='$plaques', equipement='$equipement', demarre='$demarre', deplace='$deplace' WHERE id='$modifierid'") or die(mysqli_error($connect1));
    $id_annonce = $modifierid;
}
?>