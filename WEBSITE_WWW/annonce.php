<?PHP


require_once('vendor/autoload.php');

include("header.php");
include("body.php");
require_once('classes/annonce.php');
require_once('classes/Display.php');

require_once('appbackend/admin/include/debug.php');
require_once('appbackend/admin/include/db_connect.php');
require_once('appbackend/include/email_utils.php');
require_once('appbackend/events_utils.php');


$msg = new \Plasticbrain\FlashMessages\FlashMessages();


?>

<?php
$id = mysqli_real_escape_string($connect1, $_GET['id']);
$requete1 = mysqli_query($connect1, "SELECT * FROM $tableannonces WHERE id='$id'");
$annonces = mysqli_fetch_object($requete1);

if (isset($_SESSION['id_client'])) {
    $sql = sprintf(
        'SELECT id FROM clients_favoris WHERE client = "%s" AND annonce = "%s"',
        mysqli_real_escape_string($connect1, $_SESSION['id_client']),
        mysqli_real_escape_string($connect1, $annonces->id)
    );
    $requete_favoris = mysqli_query($connect1, $sql);
    $favoris = mysqli_num_rows($requete_favoris);
}


if(isset($_POST['message'])){

    $ok = 1;
    $secret = "6LfUhlUUAAAAAArLszhTtTYcHQTHSKfMIT5d1kmi";
    $gRecaptchaResponse = (isset($_POST['g-recaptcha-response'])) ? $_POST['g-recaptcha-response'] : '';
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->verify($gRecaptchaResponse, $_SERVER['REMOTE_ADDR']);
    if (!$resp->isSuccess()){
        $ok = 0;
    }

    if ($ok == 1) {
        $_params = array();
        $_params["author_name"] = $_POST['nom'];
        $_params["author_email"] = $_POST['email'];
        $_params["author_telephone"] = $_POST['telephone'];
        $_params["author_msg"] = $_POST['message'];
        
        // $_GLOBAL['TRACE_DISABLED'] = true;
        handleEmails($dbh, "seller_demande_info", "seller", "", "demande_info", "", $annonces->id, $_params);
        
        $msg->success('Votre message a bien été envoyé. Vous recevrez sa réponse dans votre boite mail à l’adresse '.$_POST['email']);
    }else{
        $msg->error('Votre message n\'a pas été envoyé. Vous devez valider le reCAPTCHA.');
    }
}




?>

<div class="container">
    <!-- ********** ANNONCE ********** -->
	
	<?php
	if ($msg->hasMessages()) {
		$msg->display();
	}
	?>
	
	
    <div id="annonce" class="col-sm-12">
        <!-- breadcrumb + navigation -->
        <div class="col-xs-6 col-sm-4 breadLeft">
				<a href="index.php?r=voiture&marque=Toutes&modele=Tous&start=0"><i class="fa fa-chevron-left" aria-hidden="true"></i><i class="fa fa-chevron-left" aria-hidden="true"></i> Retour</a>
        </div>
        <div class="col-sm-4 breadCenter">
        </div>
        <div class="col-sm-4 breadRight">
        <?php
        $requete2 = mysqli_query($connect1, "SELECT id FROM $tableannonces WHERE id<'$id' ORDER BY id DESC LIMIT 1");
        $precedent = mysqli_fetch_object($requete2);
        $requete2 = mysqli_query($connect1, "SELECT id FROM $tableannonces WHERE id>'$id' LIMIT 1");
        $suivant = mysqli_fetch_object($requete2);
        ?>
        <?php if (isset($precedent->id)) { ?><a href="<?PHP echo "annonce.php?id=$precedent->id"; ?>"><i class="fa fa-chevron-left" aria-hidden="true"></i> Précédent</a><?PHP } if ((isset($precedent->id)) && (isset($suivant->id))) echo " / ";
        if (isset($suivant->id)) { ?><a href="<?PHP echo "annonce.php?id=$suivant->id"; ?>"><a href="<?PHP echo "annonce.php?id=$suivant->id"; ?>">Suivant <i class="fa fa-chevron-right" aria-hidden="true"></i></a><?PHP } ?>
        </div>
        <!-- /breadcrumb + navigation -->

        <div class="clear"></div>

        <!-- Photo + extrait infos -->

        <!-- headAnnonce PC -->

        <div id="headAnnonce" class="col-sm-12">

            <div class="col-xs-10 spothide"><h4><?PHP echo $annonces->marque . " " . $annonces->modele . " " . $annonces->version; ?></h4></div>
            <div class="col-xs-2 fav">
                <?php if (!isset($_SESSION['id_client'])): ?>
                    <a href="inscription.php">
                <?php else: ?>
                    <a href="#" onclick="favoris('<?php echo $annonces->id;?>');">
                <?php endif; ?>
                    <?php $class = isset($favoris) && $favoris > 0 ? "desc-fav-active" : "desc-fav-inactive"; ?>
                    <div id="desc-fav<?php echo $annonces->id;?>" class="<?php echo $class; ?>"></div>
                </a>
            </div>
            <div class="clear"></div>

            <div id="photoVendeur" class="col-sm-6">

                <!--  Lightgallery -->
                <div class="carSlider">
                    <div class="prixannonce hidden-md hidden-lg"><?PHP echo number_format($annonces->prix, 0, ",", "'") . ".-"; ?></div>

                    <ul id="imageGallery">
                        <?PHP
                        $imagei = 1;
                        $imagetotal = 1;
                        $object = "image" . $imagetotal;
                        $image = $annonces->$object;
                        while (isset($image)) {
                            if ($image != "")
                                $imagetotal++;
                            $imagei++;
                            $object = "image" . $imagei;
                            if (isset($annonces->$object))
                                $image = $annonces->$object;
                            else
                                unset($image);
                        }
                        $imagetotal = $imagetotal - 1;
                        $imagei = 1;
                        $imagecompteur = 1;
                        $object = "image" . $imagei;
                        $image = $annonces->$object;
                        while (isset($image)) {
                            if ($image != "") {
                                $lienannonce = "annonce.php?id=" . $annonces->id;
                                ?>
                                <li data-img-num="<?php echo $imagei; ?>" data-thumb="img/thumbs/<?PHP echo $image; ?>" data-src="img/<?PHP echo $image; ?>">
                                <?PHP if ($imagei == 1) { ?><a href="<?PHP echo $lienannonce; ?>" class="spothide"><img src="img/<?PHP echo $image; ?>" /></a><?PHP } ?>
                                    <img src="img/<?PHP echo $image; ?>" <?PHP if ($imagei == 1) { ?>class="spotshow"<?PHP } ?>/>
                                </li>
                                    <?php
                                    $imagecompteur++;
                                }
                                $imagei++;
                                $object = "image" . $imagei;
                                if (isset($annonces->$object))
                                    $image = $annonces->$object;
                                else
                                    unset($image);
                            }
                            ?>
                    </ul>
                    <div class="imgNum-container" style="pointer-events: none;">
                        <span class="imgNum">
                            <p>
                                <span id="current-img-num">1</span> / <?php echo $imagetotal; ?>
                            </p>
                        </span>
                    </div>
                </div>
                <!-- /Lightgallery -->
                <style>
                    #photoVendeur{
                        opacity:0;
                    }
                </style>                          
                <script type="text/javascript">
                    var j = jQuery.noConflict();

                    j(document).ready(function () {
                        // Init slider
                        j('#imageGallery').lightSlider({
                            gallery: true,
                            item: 1,
                            loop: true,
                            thumbItem: 10,
                            slideMargin: 0,
                            enableDrag: false,
                            currentPagerPosition: 'left',
                            onSliderLoad: function (el) {
                                $("#photoVendeur").css("opacity", "1");
                                el.lightGallery({
                                    download: false,
                                    selector: '#imageGallery .lslide'
                                });

                                // init imgNum
                                var element = $('.imgNum-container');
                                $('.lSSlideOuter').append(element);
                            },
                            onBeforeNextSlide: function(el) {
                                // set page num
                                j('#current-img-num').html(j('#imageGallery > li.active').next().data('img-num'));
                            },
                            onBeforePrevSlide: function(el) {
                                // set page num
                                j('#current-img-num').html(j('#imageGallery > li.active').prev().data('img-num'));
                            }
                        });
                    });
                </script>
                <hr class="blue2">
                <div class="clear"></div>

            </div>

            <div class="col-sm-6 annoncePC">
                <div id="infosAnnonce1" class="col-sm-12">
                    <div class="col-sm-10"><h4><?PHP echo $annonces->marque . " " . $annonces->modele . " " . $annonces->version; ?></h4></div>
                    <div class="col-sm-2 desc-fav"><a <?PHP if(!isset($_SESSION['id_client'])) {?>href="inscription.php"<?PHP } else {?>onclick="favoris('<?PHP echo $annonces->id;?>');"<?PHP }?>><div id="desc-fav<?PHP echo $annonces->id;?>" class="<?PHP if($favoris == 0) echo "desc-fav-inactive"; else echo "desc-fav-active";?>"></div></a></div>
                    <div class="clear"></div>
                    <div class="col-sm-12">
                        <p><?PHP echo $annonces->desc1; ?></p>
                        <hr class="blue2">
                    </div>
                </div>

                <div class="clear"></div>
                <div id="infosAnnonce2" class="col-sm-12">
                    <div class="col-sm-6">
                        <p>N° de l'annonce: <?PHP echo $annonces->id; ?></p>
                        <div class="clear"></div>
<?PHP
$parution = explode("-", $annonces->parution);
$parution = $parution[2] . "." . $parution[1] . "." . $parution[0];
?>
                        <p>Date de parution: <?PHP echo $parution; ?></p>
                        <div class="clear"></div>
                        <p><b>Prix: <?PHP echo number_format($annonces->prix, 0, ",", "'") . ".-"; if($annonces->prixdiscuter == 1) echo " (à discuter)";?></b></p>
                        <div class="clear"></div>
<?PHP
$premiereimm = explode("-", $annonces->premiereimm);
$premiereimm = $premiereimm[0];
?>
                        <p><b>Année: <?PHP echo $premiereimm; ?></b></p>
                        <div class="clear"></div>
                        <p><b>Kilomètres: <?PHP echo $annonces->kilometrage; ?> km</b></p>
						<div class="clear"></div>
						

<?PHP
$requete_securspot = mysqli_query($connect1, "SELECT fichier FROM annonces_securspot WHERE id_annonce='$annonces->id'");
if (mysqli_num_rows($requete_securspot) == 1) {
    $securspot = mysqli_fetch_object($requete_securspot);
    ?><p><a href="/rapports/<?PHP echo $securspot->fichier; ?>" target="_blank"><img src="images/securspot.png" width="66" height="34"/></a></p><?PHP
                        }
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        $requete2 = mysqli_query($connect1, "SELECT id, lang, nom, prenom, code_postal, ville, telephone, typecontact, email, type FROM clients WHERE id='$annonces->id_client'");
												$annonceur = mysqli_fetch_object($requete2);
												// echo "id annonceur : ".$annonceur->id;
                        ?>
						
						<p>Annonceur :<br />
						<?php 
						$garage = annonce::getGarageByEmail($annonceur->email);
						
						
						if($annonceur->type == 0){ 
							// echo "Vendeur particulier<br />".$annonceur->nom . " " . $annonceur->prenom . "<br>" . $annonceur->code_postal . " " . $annonceur->ville; 
							echo "Vendeur particulier<br />".$annonceur->nom . " " . $annonceur->prenom . "<br>" . $annonces->codepostal . " " . $annonces->ville; 
						}
						else if($annonceur->type == 2){ 
							// echo "Vendeur professionnel<br />".$annonceur->nom . "<br>" . $annonceur->code_postal . " " . $annonceur->ville; 
							echo "Vendeur professionnel<br />".$annonceur->nom . "<br>" . $annonces->codepostal . " " . $annonces->ville; 
						} 
						else if($annonceur->type == 1 && $garage) {
							$name = $garage->name;
							echo "Garage partenaire<br />".$name . "<br>" . $garage->cp_unique . " " . $garage->city; 
						}
						?>
						</p>
						
						<br/>
                        <?php if ($annonceur != false && $annonceur->typecontact != 1) echo $annonceur->telephone; ?></p>
                        <div class="clear"></div>
                        <div class="col-sm-12">
                            <div class="clear"></div>

                            <?php if (is_object($annonceur) && $annonceur->typecontact != 2) { ?><a href="#modalContact" class="btn btn-autospot" data-toggle="modal" data-target="#modalContact">Contacter le vendeur</a><?PHP } ?>
                            <p class="center"><a href="#" class="center"><small>Toutes les annonces de ce vendeur</small></a></p>

                            <div class="clear"></div>
														<?php
														if($annonceur->type != 2) echo annonce::displayLink($annonces, false, true); 
														?>
                            <div class="clear"></div>

                        </div>
                    </div>

                </div>

            </div>

            <div class="col-sm-12 annonceMob">

                <div class="col-sm-12">
                    <div class="col-xs-8">
                        <p><?PHP echo $annonces->desc1; ?></p>

                    </div>
                    <div class="col-xs-4 blueLeft">
                        <p><b>Année: <?PHP echo $premiereimm; ?></b></p>
                        <p><b><?PHP echo $annonces->kilometrage; ?> km</b></p>
						
						<?php if($annonces->prixdiscuter == 1){ ?>
							<p><b>Prix à discuter</b></p>
							<div class="clear"></div>
						<?php } ?>
<?PHP
$requete_securspot = mysqli_query($connect1, "SELECT fichier FROM annonces_securspot WHERE id_annonce='$annonces->id'");
if (mysqli_num_rows($requete_securspot) == 1) {
    $securspot = mysqli_fetch_object($requete_securspot);
    ?><p><a href="/rapports/<?PHP echo $securspot->fichier; ?>" target="_blank"><img src="images/securspot.png" width="66" height="34"/></a></p><?PHP
                        }
                        ?>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="col-xs-10">
				
					<p>Annonceur :<br />
					<?php 
					$garage = annonce::getGarageByEmail($annonceur->email);
					
					if($annonceur->type == 0){ 
						// echo "Vendeur particulier<br />".$annonceur->nom . " " . $annonceur->prenom . "<br>" . $annonceur->code_postal . " " . $annonceur->ville; 
						echo "Vendeur particulier<br />".$annonceur->nom . " " . $annonceur->prenom . "<br>" . $annonces->codepostal . " " . $annonces->ville; 
					}
					else if($annonceur->type == 2){ 
						// echo "Vendeur professionnel<br />".$annonceur->nom . "<br>" . $annonceur->code_postal . " " . $annonceur->ville; 
						echo "Vendeur professionnel<br />".$annonceur->nom . "<br>" . $annonces->codepostal . " " . $annonces->ville;
					} 
					else if($annonceur->type == 1 && $garage) {
						$name = $garage->name;
						echo "Garage partenaire<br />".$name . "<br>" . $garage->cp_unique . " " . $garage->city; 
					}
					?>
					</p>

                </div>
                <div class="clear"></div>

                <div class="col-sm-12">
                        <?PHP if ($annonceur->typecontact != 2) { ?><a href="#modalContact" class="btn btn-autospot" data-toggle="modal" data-target="#modalContact">Contacter le vendeur</a><?PHP } ?>
                    <p class="center"><a href="#" class="center"><small>Toutes les annonces de ce vendeur</small></a></p>
                    <div class="clear"></div>
										<?php
										if($annonceur->type != 2) echo annonce::displayLink($annonces, false, true);
										?>
                </div>

            </div>

        </div>
        <!-- /headAnnonce PC-->

        <!-- POP UP CONTACT VENDEUR -->
        <!-- Modal -->
        <div id="modalContact" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><small>Fermer</small></button>
                        <h4 class="modal-title">Contacter le vendeur</h4><br />
                        Cette annonce a été inséré en <?php if ($annonceur->lang == "fr") echo "français"; else if($annonceur->lang == "de") echo "allemand"; else if($annonceur->lang == "it") echo "italien";?>
                    </div>
                    <div class="modal-body">
                        <!-- FORMULAIRE CONTACT ANNONCEUR -->
                        <form class="form" role="form" method="POST">

                            <div class="form-group">
                                <label class="control-label col-sm-12"><b>Votre message pour :</b> <?PHP echo $annonceur->nom." ".$annonceur->prenom;?></label>
                                <br/>
                                <label class="control-label col-sm-12"><b>Concernant :</b> <?PHP echo $annonces->marque . " " . $annonces->modele . " " . $annonces->version; ?></label>
                            </div>

                            <div class="clear"></div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="nom">Nom <span class="requis">*</span></label>
                                <div class="col-sm-10">
                                    <input name="nom" type="text" class="form-control has-warning has-error has-feedback" required id="nom" placeholder="Ex. Laurent dupuis">
                                </div>
                            </div>

                            <div class="clear"></div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="telephone">Téléphone</label>
                                <div class="col-sm-10">
                                    <input name="telephone" type="text" class="form-control" id="telephone" placeholder="Ex. 079/123.45.67">
                                </div>
                            </div>

                            <div class="clear"></div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">E-mail <span class="requis">*</span></label>
                                <div class="col-sm-10">
                                    <input name="email" type="text" class="form-control has-warning has-error has-feedback" required="true" id="email" placeholder="Ex. laurent@dupuis.ch">
                                </div>
                            </div>
                            <div class="clear"></div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="message">Message <span class="requis">*</span></label>
                                <div class="col-sm-10">
                                    <textarea name="message" class="form-control has-warning has-error has-feedback" rows="7" id="message" placeholder="Bonjour,Je suis intéressé(e) par votre véhicule. Veuillez me contacter svp. Cordialement," required>
                                    </textarea>
                                </div>
                            </div>
                            <div class="clear"></div>
							
							
							<div class="form-group">
                                <label class="control-label col-sm-2" for="message"></label>
                                <div class="col-sm-10">
									<div class="g-recaptcha" data-sitekey="6LfUhlUUAAAAADFtz6TSa-0HVxBQYmxATuHJLlNt"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
							
							

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="alert alert-danger">
                                        <strong>* Champs obligatoires</strong>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 my_button_btn">
                                    <button type="submit" class="btn btn-autospot">Envoyer</button>
                                </div>
                            </div>
                        </form>
                        <div class="clear"></div>
                        <!-- /FORMULAIRE CONTACT ANNONCEUR -->

                    </div>
                    <div class="modal-footer">
                    </div>
                </div>

            </div>
        </div>
        <!-- /POP UP CONTACT VENDEUR -->

        <!-- /Photo + extrait infos -->

        <div class="clear"></div>

        <!-- DETAILS VEHICULE -->
        <div id="collDetails" class="col-xs-12 col-sm-7">
            <h4>Détails du véhicule</h4>
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10">
                    <table class="table">
                        <tr class="visible-xs">
                            <td>N° de l'annonce:</td>
                            <td><?php echo $annonces->id; ?></td>
                        </tr>
                        <tr class="visible-xs">
                            <td>Date de parution:</td>
                            <td>
                                <?php
                                $parution = explode("-", $annonces->parution);
                                $parution = $parution[2] . "." . $parution[1] . "." . $parution[0];
                                ?>
                                <?PHP echo $parution; ?>
                            </td>
                        </tr>    
                        <tr>
                            <td>Catégorie:</td>
                            <td><?php echo $annonces->categorie; ?></td>
                        </tr>
                        <tr>
                            <td>État du véhicule:</td>
                            <td><?php echo ($annonces->accident ? "Véhicule accidenté" : "Véhicule non accidenté"); ?></td>
                        </tr>
                        <tr>
                            <td>Carrosserie:</td>
                            <td><?php echo $annonces->carrosserie; ?></td>
                        </tr>
                        <tr>
                            <?php
                            $expertisee = explode("-", $annonces->expertisee);
                            $expertisee = $expertisee[2] . "." . $expertisee[1] . "." . $expertisee[0];
                            ?>
                            <td>Dernière expertise:</td>
                            <td><?php echo ($annonces->expertisee == "0000-00-00" ? "N.D" : $annonces->expertisee); ?></td>
                        </tr>
                        <tr>
                            <td>Carburant:</td>
                            <td><?php echo $annonces->carburant; ?></td>
                        </tr>
                        <tr>
                            <td>Transmission:</td>
                            <td><?php echo $annonces->transmission; ?></td>
                        </tr>
                        <tr>
                            <td>Roues motrices:</td>
                            <td><?php echo ($annonces->rouesmotrices != '') ? $annonces->rouesmotrices : 'N.D'; ?></td>
                        </tr>
						
						<?php if($annonces->transmission != 'automatique'){ ?>
                        <tr>
                            <td>Nb de vitesses:</td>
                            <td><?php echo $annonces->vitesses; ?></td>
                        </tr>
						<?php } ?>
                        <tr>
                            <td>Cylindrée:</td>
                            <td><?php echo $annonces->cylindrees; ?></td>
                        </tr>
                        <tr>
                            <td>Puissance en cv :</td>
                            <td><?php echo $annonces->puissancechevaux ?></td>
                        </tr>
                        <tr>
                            <td>Nb de portes:</td>
                            <td><?php echo $annonces->portes; ?></td>
                        </tr>
                        <tr>
                            <td>Nb de places:</td>
                            <td><?php echo ($annonces->sieges != 0) ? $annonces->sieges : 'N.D'; ?><//td>
                        </tr>
                        <tr>
                            <td>Couleur extérieure:</td>
                            <td>
                                <?php echo $annonces->couleurexterne; ?>
                                <?php echo $annonces->metallisee ? sprintf(' %s', $LANG[154]) : ''; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Garantie:</td>
                            <td><?php echo $annonces->typeGarantie; ?></td>
                        </tr>
												
												<?php if($annonces->prixneuf){ ?>
                        <tr>
                            <td>Prix neuf:</td>
                            <td><?php echo $annonces->prixneuf.'.-'; ?></td>
                        </tr>
												<?php } ?>
						
						<?php if($annonces->typeGarantie != 'Sans garantie'){ ?>
						<tr>
							<td>Durée de la garantie</td>
							<td><?php echo Display::formatMonth($annonces->dureeGarantie); ?></td>
                        </tr>
						<?php } ?>
						
                    </table>
                </div>
            </div>
        </div>
        <!-- /DETAILS VEHICULE -->


        <!-- DESCRIPTION DETAILLEE -->
        <div id="collDescription" class="col-sm-5">
            <h4>Description détaillée</h4>

			<p><?PHP 
			if($annonces->desc2 != '') echo $annonces->desc2;
			else echo 'Aucune information supplémentaire disponible';
			 ?></p>

        </div>
        <!-- /DESCRIPTION DETAILLEE -->

        <div class="clear"></div>

        <!-- ACCORDEONS -->
        <div class="panel-group" id="accordion">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#collEquipements" class="black collapsed">
                        <h4 class="panel-title">Les équipements du véhicule
                            <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span></h4></a>
                </div>
            </div>

            <!-- EQUIPEMENTS -->
            <div id="collEquipements" class="panel-collapse collapse">
                <!-- Sécurité -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="securite">Sécurité :</label>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="airbag" type="checkbox" value="oui" name="airbag" <?PHP if (preg_match("/Airbag conducteur et passager/", $annonces->equipement)) echo "checked"; ?> disabled><label for="airbag">Airbag conducteur et passager</label><br/>
                            <input id="airbagL" type="checkbox" value="oui" name="airbagL" <?PHP if (preg_match("/Airbag latéraux pour conducteur et passager/", $annonces->equipement)) echo "checked"; ?> disabled><label for="airbagL">Airbag latéraux pour conducteur et passager</label><br/>
                            <input id="isofix" type="checkbox" value="oui" name="isofix" <?PHP if (preg_match("/Fixation isofix pour siège enfant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="isofix">Fixation isofix pour siège enfant</label><br/>
                            <input id="feuxAv" type="checkbox" value="oui" name="feuxAv" <?PHP if (preg_match("/Feux anti-brouillard avant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="feuxAv">Feux anti-brouillard avant</label><br/>
                            <input id="feuxAr" type="checkbox" value="oui" name="feuxAr" <?PHP if (preg_match("/Feux anti-brouillard arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="feuxAr">Feux anti-brouillard arrière</label><br/>
                            <input id="phareDir" type="checkbox" value="oui" name="phareDir" <?PHP if (preg_match("/Phares directionnels/", $annonces->equipement)) echo "checked"; ?> disabled><label for="phareDir">Phares directionnels</label><br/>
                            <input id="xenon" type="checkbox" value="oui" name="xenon" <?PHP if (preg_match("/Éclairage Xénon/", $annonces->equipement)) echo "checked"; ?> disabled><label for="xenon">Éclairage Xénon</label>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="verrCent" type="checkbox" value="oui" name="verrCent" <?PHP if (preg_match("/Verrouillage centralisé/", $annonces->equipement)) echo "checked"; ?> disabled><label for="verrCent">Verrouillage centralisé</label><br/>
                            <input id="verrCentDist" type="checkbox" value="oui" name="verrCentDist" <?PHP if (preg_match("/Verrouillage centralisé à distance/", $annonces->equipement)) echo "checked"; ?> disabled><label for="verrCentDist">Verrouillage centralisé à distance</label><br/>
                            <input id="x3feustop" type="checkbox" value="oui" name="x3feustop" <?PHP if (preg_match("/3ème feu de stop/", $annonces->equipement)) echo "checked"; ?> disabled><label for="x3feustop">3ème feu de stop</label><br/>
                            <input id="ledRetro" type="checkbox" value="oui" name="ledRetro" <?PHP if (preg_match("/Led clignotant placé dans les rétroviseurs extérieurs/", $annonces->equipement)) echo "checked"; ?> disabled><label for="ledRetro">Led clignotant placé dans les rétroviseurs extérieurs</label><br/>
                            <input id="abs" type="checkbox" value="oui" name="abs" <?PHP if (preg_match("/ABS \(système anti-blocage des roues\)/", $annonces->equipement)) echo "checked"; ?> disabled><label for="abs">ABS (système anti-blocage des roues)</label><br/>
                            <input id="asr" type="checkbox" value="oui" name="asr" <?PHP if (preg_match("/ASR \(anti-patinage des roues\)/", $annonces->equipement)) echo "checked"; ?> disabled><label for="asr">ASR (anti-patinage des roues)</label><br/>
                            <input id="esp" type="checkbox" value="oui" name="esp" <?PHP if (preg_match("/ESP \(contrôle de stabilité\)/", $annonces->equipement)) echo "checked"; ?> disabled><label for="esp">ESP (contrôle de stabilité)</label>
                        </div>
                    </div></div>

                <!-- Confort -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="confort">Confort :</label>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="dirAss" type="checkbox" value="oui" name="dirAss" <?PHP if (preg_match("/Direction assistée/", $annonces->equipement)) echo "checked"; ?> disabled><label for="dirAss">Direction assistée</label><br/>
                            <input id="clim" type="checkbox" value="oui" name="clim" <?PHP if (preg_match("/Climatisation/", $annonces->equipement)) echo "checked"; ?> disabled><label for="clim">Climatisation</label><br/>
                            <input id="climAuto" type="checkbox" value="oui" name="climAuto" <?PHP if (preg_match("/Climatisation automatique/", $annonces->equipement)) echo "checked"; ?> disabled><label for="climAuto">Climatisation automatique</label><br/>
                            <input id="vitreTeinte" type="checkbox" value="oui" name="vitreTeinte" <?PHP if (preg_match("/Vitres teintées/", $annonces->equipement)) echo "checked"; ?> disabled><label for="vitreTeinte">Vitres teintées</label><br/>
                            <input id="vitreElec" type="checkbox" value="oui" name="vitreElec" <?PHP if (preg_match("/Vitres avant et arrière électriques/", $annonces->equipement)) echo "checked"; ?> disabled><label for="vitreElec">Vitres avant et arrière électriques</label><br/>
                            <input id="volantCuir" type="checkbox" value="oui" name="volantCuir" <?PHP if (preg_match("/Volant en cuir/", $annonces->equipement)) echo "checked"; ?> disabled><label for="volantCuir">Volant en cuir</label><br/>
                            <input id="volantChauf" type="checkbox" value="oui" name="volantChauf" <?PHP if (preg_match("/Volant chauffant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="volantChauf">Volant chauffant</label><br/>
                            <input id="siegeCuir" type="checkbox" value="oui" name="siegeCuir" <?PHP if (preg_match("/Sièges en cuir/", $annonces->equipement)) echo "checked"; ?> disabled><label for="siegeCuir">Sièges en cuir</label><br/>
                            <input id="siegeChauf" type="checkbox" value="oui" name="siegeChauf" <?PHP if (preg_match("/Sièges avant chauffant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="siegeChauf">Sièges avant chauffant</label>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="detecteurPluie" type="checkbox" value="oui" name="detecteurPluie" <?PHP if (preg_match("/Détecteur de luminosité et de pluie/", $annonces->equipement)) echo "checked"; ?> disabled><label for="detecteurPluie">Détecteur de luminosité et de pluie</label><br/>
                            <input id="essuieInter" type="checkbox" value="oui" name="essuieInter" <?PHP if (preg_match("/Essuie-glaces avant avec intervalle/", $annonces->equipement)) echo "checked"; ?> disabled><label for="essuieInter">Essuie-glaces avant avec intervalle</label><br/>
                            <input id="regVitesse" type="checkbox" value="oui" name="regVitesse" <?PHP if (preg_match("/Régulateur de vitesse/", $annonces->equipement)) echo "checked"; ?> disabled><label for="regVitesse">Régulateur de vitesse</label><br/>
                            <input id="startstop" type="checkbox" value="oui" name="startstop" <?PHP if (preg_match("/Fonction Start\/Stop/", $annonces->equipement)) echo "checked"; ?> disabled><label for="startstop">Fonction Start/Stop</label><br/>
                            <input id="retroElec" type="checkbox" value="oui" name="retroElec" <?PHP if (preg_match("/Rétroviseurs extérieurs à dégivrage et réglage électrique/", $annonces->equipement)) echo "checked"; ?> disabled><label for="retroElec">Rétroviseurs extérieurs à dégivrage et réglage électrique</label><br/>
                            <input id="parcageAr" type="checkbox" value="oui" name="parcageAr" <?PHP if (preg_match("/Capteur de distance de parcage arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="parcageAr">Capteur de distance de parcage arrière</label><br/>
                            <input id="parcageAvAr" type="checkbox" value="oui" name="parcageAvAr" <?PHP if (preg_match("/Capteur de distance de parcage avant et arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="parcageAvAr">Capteur de distance de parcage avant et arrière</label><br/>
                            <input id="cameraRecul" type="checkbox" value="oui" name="cameraRecul" <?PHP if (preg_match("/Caméra de recul/", $annonces->equipement)) echo "checked"; ?> disabled><label for="cameraRecul">Caméra de recul</label><br/>
                            <input id="barreToit" type="checkbox" value="oui" name="barreToit" <?PHP if (preg_match("/Barres de toit/", $annonces->equipement)) echo "checked"; ?> disabled><label for="barreToit">Barres de toit</label>
                        </div>
                    </div></div>

                <!-- Divertissements -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="divertissements">Divertissements :</label>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="ordinateur" type="checkbox" value="oui" name="ordinateur" <?PHP if (preg_match("/Ordinateur de bord/", $annonces->equipement)) echo "checked"; ?> disabled><label for="ordinateur">Ordinateur de bord</label><br/>
                            <input id="gps" type="checkbox" value="oui" name="gps" <?PHP if (preg_match("/Système de navigation GPS/", $annonces->equipement)) echo "checked"; ?> disabled><label for="gps">Système de navigation GPS</label><br/>
                            <input id="radio" type="checkbox" value="oui" name="radio" <?PHP if (preg_match("/Radio \/ CD/", $annonces->equipement)) echo "checked"; ?> disabled><label for="radio">Radio / CD</label><br/>
                            <input id="usb" type="checkbox" value="oui" name="usb" <?PHP if (preg_match("/Connexion USB et AUX/", $annonces->equipement)) echo "checked"; ?> disabled><label for="usb">Connexion USB et AUX</label>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="volantMultiF" type="checkbox" value="oui" name="volantMultiF" <?PHP if (preg_match("/Volant multi-fonctions/", $annonces->equipement)) echo "checked"; ?> disabled><label for="volantMultiF">Volant multi-fonctions</label><br/>
                            <input id="bluetooth" type="checkbox" value="oui" name="bluetooth" <?PHP if (preg_match("/Interface Bluetooth et téléphone mobile/", $annonces->equipement)) echo "checked"; ?> disabled><label for="bluetooth">Interface Bluetooth et téléphone mobile</label><br/>
                            <input id="toitPano" type="checkbox" value="oui" name="toitPano" <?PHP if (preg_match("/Toit panoramique/", $annonces->equipement)) echo "checked"; ?> disabled><label for="toitPano">Toit panoramique</label><br/>
                            <input id="toitOuvrant" type="checkbox" value="oui" name="toitOuvrant" <?PHP if (preg_match("/Toit ouvrant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="toitOuvrant">Toit ouvrant</label>
                        </div>
                    </div></div>

                <!-- Équipements sport -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="sport">Équipements sport :</label>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="spoilerToit" type="checkbox" value="oui" name="spoilerToit" <?PHP if (preg_match("/Spoiler de toit/", $annonces->equipement)) echo "checked"; ?> disabled><label for="spoilerToit">Spoiler de toit</label><br/>
                            <input id="jupes" type="checkbox" value="oui" name="jupes" <?PHP if (preg_match("/Jupes latérales/", $annonces->equipement)) echo "checked"; ?> disabled><label for="jupes">Jupes latérales</label><br/>
                            <input id="chassisSport" type="checkbox" value="oui" name="chassisSport" <?PHP if (preg_match("/Châssis sportif/", $annonces->equipement)) echo "checked"; ?> disabled><label for="chassisSport">Châssis sportif</label>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="siegeSport" type="checkbox" value="oui" name="siegeSport" <?PHP if (preg_match("/Sièges sport/", $annonces->equipement)) echo "checked"; ?> disabled><label for="siegeSport">Sièges sport</label><br/>
                            <input id="retroChrome" type="checkbox" value="oui" name="retroChrome" <?PHP if (preg_match("/Rétrovieurs extérieurs chromés/", $annonces->equipement)) echo "checked"; ?> disabled><label for="retroChrome">Rétrovieurs extérieurs chromés</label><br/>
                            <input id="barreToitAlu" type="checkbox" value="oui" name="barreToitAlu" <?PHP if (preg_match("/Barrres de toit en aluminium/", $annonces->equipement)) echo "checked"; ?> disabled><label for="barreToitAlu">Barrres de toit en aluminium</label>
                        </div>
                    </div></div>

                <!-- Autres -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="autres">Autres :</label>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="roueSecours" type="checkbox" value="oui" name="roueSecours" <?PHP if (preg_match("/Roue de secours/", $annonces->equipement)) echo "checked"; ?> disabled><label for="roueSecours">Roue de secours</label><br/>
                            <input id="x2PneusRoues" type="checkbox" value="oui" name="x2PneusRoues" <?PHP if (preg_match("/2 jeux de pneus\/roues/", $annonces->equipement)) echo "checked"; ?> disabled><label for="x2PneusRoues">2 jeux de pneus/roues</label><br/>
                            <input id="antiCrevaison" type="checkbox" value="oui" name="antiCrevaison" <?PHP if (preg_match("/Kit anti-crevaison/", $annonces->equipement)) echo "checked"; ?> disabled><label for="antiCrevaison">Kit anti-crevaison</label><br/>
                            <input id="hardTop" type="checkbox" value="oui" name="hardTop" <?PHP if (preg_match("/Hard top/", $annonces->equipement)) echo "checked"; ?> disabled><label for="hardTop">Hard top</label>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="checkbox checkbox-primary">
                            <input id="tapisCaoutchouc" type="checkbox" value="oui" name="tapisCaoutchouc" <?PHP if (preg_match("/Tapis en caoutchouc avant et arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="tapisCaoutchouc">Tapis en caoutchouc avant et arrière</label><br/>
                            <input id="tapisRevetus" type="checkbox" value="oui" name="tapisRevetus" <?PHP if (preg_match("/Tapis revêtus avant et arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="tapisRevetus">Tapis revêtus avant et arrière</label><br/>
                            <input id="crochetAttelage" type="checkbox" value="oui" name="crochetAttelage" <?PHP if (preg_match("/Crochet d'attelage/", $annonces->equipement)) echo "checked"; ?> disabled><label for="crochetAttelage">Crochet d'attelage</label><br/>
                            <input id="grille" type="checkbox" value="oui" name="grille" <?PHP if (preg_match("/Grille de séparation/", $annonces->equipement)) echo "checked"; ?> disabled><label for="grille">Grille de séparation</label>
                        </div>
                    </div></div>
            </div>
            <!-- /EQUIPEMENTS -->

            <div class="clear"></div>

            <!-- Informations complémentaires -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" data-target="#collInfoComp" class="black collapsed">
                        <h4 class="panel-title">Informations complémentaires
                            <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span></h4></a>
                </div>
            </div>
			

            <div id="collInfoComp" class="panel-collapse collapse">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="complementaires">Informations complémentaires :</label>

                    <div class="col-sm-3">
                        <div class="checkbox checkbox-primary">
                            <input id="importDirectPara" type="checkbox" value="oui" name="importDirectPara" <?PHP if (preg_match("/Importation directe\/parallèle/", $annonces->equipement)) echo "checked"; ?> disabled><label for="importDirectPara">Importation directe/parallèle</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="checkbox checkbox-primary">
                            <input id="handicape" type="checkbox" value="oui" name="handicape" <?PHP if (preg_match("/Pour handicapé/", $annonces->equipement)) echo "checked"; ?> disabled><label for="handicape">Pour handicapé</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="checkbox checkbox-primary">
                            <input id="tuning" type="checkbox" value="oui" name="tuning" <?PHP if (preg_match("/Tuning/", $annonces->equipement)) echo "checked"; ?> disabled><label for="tuning">Tuning</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="checkbox checkbox-primary">
                            <input id="voitureCourse" type="checkbox" value="oui" name="voitureCourse" <?PHP if (preg_match("/Voiture de course/", $annonces->equipement)) echo "checked"; ?> disabled><label for="voitureCourse">Voiture de course</label>
                        </div>
                    </div>
					
                    <div class="col-sm-3">
                        <div class="checkbox checkbox-primary">
                            <input id="nonFumeur" type="checkbox" value="oui" name="nonFumeur" <?PHP if (preg_match("/Véhicule non-fumeur/", $annonces->equipement)) echo "checked"; ?> disabled><label for="nonFumeur">Véhicule non fumeur</label>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- /Informations complémentaires -->

        </div>
        <!-- END ACCORDEONS -->

        <div class="alert alert-danger" role="alert">
            Si vous suspectez une annonce trompeuse ou pas sérieuse, veuillez nous le transmettre en <a href="/annonce_suspecte.php?id=<?php echo $annonces->id; ?>">cliquant ICI</a>.<br />
        </div>

        <p><small>Les informations contenues dans cette annonce ne sont pas garanties.</small></p>

        <div class="clear"></div>

    </div>
</div>
<!-- ********** /ANNONCE ********** -->



<?php include("footer.php"); ?>
