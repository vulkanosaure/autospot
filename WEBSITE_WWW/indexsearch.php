<div id="switchL"><?PHP if(!preg_match("/www.autospot.ch/", $_SERVER["HTTP_HOST"])) echo "<a href=\"http://www.autospot.ch/\">";?>autospot.ch<?PHP if(!preg_match("/www.autospot.ch/", $_SERVER["HTTP_HOST"])) echo "</a>";?> | <?PHP if(!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) echo "<a href=\"http://www.motospot.ch/\">";?>motospot.ch<?PHP if(!preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) echo "</a>";?></div>
<div id="switchR"><a data-toggle="modal" href="#apropos">à propos</a></div>

<?PHP
include("apropos.php");
if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"]))
{
	$requete_annoncestotal = mysqli_query($connect1, "SELECT * FROM $tableannonces WHERE choix='moto'");
	$annoncestotal = mysqli_num_rows($requete_annoncestotal);
	$annoncestotal = number_format($annoncestotal, 0, ',', '\'');
}
else
{
	$requete_annoncestotal = mysqli_query($connect1, "SELECT * FROM $tableannonces WHERE choix='voiture'");
	$annoncestotal = mysqli_num_rows($requete_annoncestotal);
	$annoncestotal = number_format($annoncestotal, 0, ',', '\'');
}
?>
<div id="day">
<div class="col-sm-4 left"></div>
<div class="col-sm-4 center">Aujourd'hui avec <a href="<?PHP if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) echo "http://www.motospot.ch/index.php?q=Rechercher+par+marque+et%2Fou+mod%C3%A8le&r=moto"; else echo "http://www.autospot.ch/index.php?q=Rechercher+par+marque+et%2Fou+mod%C3%A8le&r=voiture"?>"><button type="button" class="btn btn-default"><?PHP echo $annoncestotal;?></button></a> annonces uniques</div>
<div class="col-sm-4 right">découvrez en un clic toutes les annonces <?PHP if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) echo "motos"; else echo "automobiles"?> de Suisse</div>
</div>

<div class="container">

<!-- HEADER -->
<div class="jumbotron jumboindex">
  <a href="//<?PHP echo $_SERVER["HTTP_HOST"];?>"><div class="logoindex">
  	<?PHP
  if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"]))
  {
  	?><img src="images/logo-motospot.png" class="img-responsive"/><?PHP
  }
  else
  {
  	?><img src="images/logo.png" class="img-responsive"/><?PHP
  }?>
  </div></a>
  <div class="row searchindex">
  <form class="form-inline" role="form" action="//<?PHP echo $_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"];?>" method="GET">

  <div class="col-sm-8">
  <div class="form-group col-sm-12">
    <input type="text" class="form-control resultsearch" id="q" name="q" value="<?PHP if((isset($_GET['q'])) && ($_GET['q'] != "")) echo $_GET['q']; else echo "Rechercher par marque et/ou modèle";?>" onclick="if(document.getElementById('q').value=='Rechercher par marque et/ou modèle') document.getElementById('q').value='';">
  </div>
  </div>

  <div class="col-sm-3">
  <div class="form-group col-sm-12">
  <select class="form-control resultselect" id="r" name="r"/>
  <?PHP
  if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"]))
  {
  	?>
  	<option value="voiture" <?PHP if((!isset($_GET["r"])) || ($_GET["r"] == "voiture")) echo "selected";?>>Voitures de tourisme</option>
  	<option value="moto" <?PHP if((!isset($_GET["r"])) || ($_GET["r"] == "moto")) echo "selected";?>>Motos & Scooters</option>
	<?PHP
  }
  else
  {
  	?>
  	<option value="voiture" <?PHP if((!isset($_GET["r"])) || ($_GET["r"] == "voiture")) echo "selected";?>>Voitures de tourisme</option>
	<option value="moto" <?PHP if((isset($_GET["r"])) && ($_GET["r"] == "moto")) echo "selected";?>>Motos & Scooters</option>
	<?PHP
  }?>
</select>
</div>
</div>

  <div class="col-sm-1">
  <div class="form-group col-sm-12">
  <button type="submit" class="btn btn-autospot"><span class="glyphicon glyphicon-search"></span></button>
  </div>
  </div>
</form>
</div>
</div>

<!-- AJOUT JUIN 2015 -->
<div class="clear"></div>

<!-- FOOTER -->
<div id="footer">


<div class="indexfooter">

<!-- SITES du FOOTER -->
  <div id="logosites" class="col-xs-12">
  <ul>
  <?PHP
  if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"]))
  {
  	?>
	<li class="site1"><img src="logos-sites/motoscout24.png" alt="motoscout24.ch"></li>
    <li class="site2"><img src="logos-sites/motoricardo.png" alt="motoricardo.ch"></li>
    <li class="site3"><img src="logos-sites/anibis.png" alt="anibis.ch"></li>
    <li class="site4"><img src="logos-sites/petitesannonces.png" alt="petitesannonces.ch"></li>
    <li class="site5"><img src="logos-sites/fahrzeugnet.png" alt="fahrzeugnet.ch"></li>
    <li class="site6"><img src="logos-sites/bikeweb.png" alt="bikeweb"></li>
    <li class="site7"><img src="logos-sites/topannonces.png" alt="topannonces.ch"></li>
    <li class="site8"><img src="logos-sites/motosportsuisse.png" alt="motosportsuisse.ch"></li>
    <li class="site9" class="site"><img src="logos-sites/commerce-moto.png" alt="commerce-moto.ch"></li>
    <li class="site10"></li>
    <li class="site11"></li>
    <li class="site12"></li>
  	<?PHP
  }
  else
  {
  	?>
	<li class="site1"><img src="logos-sites/autoscout24.png" alt="autoscout24.ch"></li>
    <li class="site2"><img src="logos-sites/autoricardo.png" alt="autoricardo.ch"></li>
    <li class="site3"><img src="logos-sites/anibis.png" alt="anibis.ch"></li>
    <li class="site4"><img src="logos-sites/petitesannonces.png" alt="petitesannonces.ch"></li>
    <li class="site5"><img src="logos-sites/tutti.png" alt="tutti.ch"></li>
    <!--<li class="site6"><img src="logos-sites/auto-online.png" alt="auto-online.ch"></li>-->
    <li class="site7"><img src="logos-sites/autolina.png" alt="autolina.ch"></li>
    <li class="site8"><img src="logos-sites/fahrzeugnet.png" alt="fahrzeugnet.ch"></li>
    <li class="site9" class="site"><img src="logos-sites/carweb.png" alt="carweb.ch"></li>
    <li class="site10"><img src="logos-sites/drive-in.png" alt="drive-in.ch"></li>
    <li class="site11"><img src="logos-sites/autobox.png" alt="autobox.ch"></li>
    <li class="site12"><img src="logos-sites/topannonces.png" alt="topannonces.ch"></li>
  	<?PHP
  }
  ?>
  </ul>
</div>
<!-- END SITES du FOOTER -->

</div>
</div>
<!-- END FOOTER -->
<!-- AJOUT JUIN 2015 -->