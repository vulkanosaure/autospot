<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

if (empty($_GET['exemple'])) {
	?><a href="rapport.php?exemple=1">Exemple 1 (à l'écran)</a><br/>
	<a href="rapport.php?exemple=2">Exemple 1 (en pdf)</a><br/>
	<a href="rapport.php?exemple=3">Exemple 2 (à l'écran)</a><br/>
	<a href="rapport.php?exemple=4">Exemple 2 (en pdf)</a><br/><?php
}
else {
	$data = array(
		'id'		=>	1,
		//tableau avec une liste d'url
		'photos'	=>	array('https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Porsche_detail_amk.jpg/800px-Porsche_detail_amk.jpg', 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Projet_T7.JPG/800px-Projet_T7.JPG', 'https://upload.wikimedia.org/wikipedia/commons/6/6e/1967_Porsche_911S.jpg', 'https://upload.wikimedia.org/wikipedia/commons/6/6e/1967_Porsche_911S.jpg', 'https://upload.wikimedia.org/wikipedia/commons/6/6e/1967_Porsche_911S.jpg'),
		'donnees'		=>	array(
			'marque'		=>	'porsche',
			'modele'		=>	'911',
			'prix'			=>	'15559€',
			'annee'			=>	'2013',
			'km'			=>	'100',
			'carrosserie'	=>	'Petite citadine',
			'categorie'		=>	'Neuf',
			'cylindree'		=>	'600 cm3',
			'chevaux'		=>	'40',
			'transmission'	=>	'Manuelle',
			'rouesmotrices'	=>	'4 roues',
			'nbvitesses'	=>	'10',
			'carburant'		=>	'Essence',
			'nbportes'		=>	'5',
			'nbplaces'		=>	'4',
			'couleur'		=>	'Argent',
			// true ou false pour la couleur métalisée
			'metallise'		=>	false
		),
		'description'	=>	array(
			'rapide'		=>	'description rapide',
			'detaillee'		=>	'description detaillee'
		),
		'synthese'		=>	array(
			// vous serez le Xeme proprio (ici 2)
			'nombreproprio'	=>	2,
			// true ou false pour si le véhicule est d'origine suisse
			'suisse'		=>	false,
			// nombre de mois restant de garantie (ou 0)
			'fingarantie'	=>	24,
			// 0 pour pas d'expertise, sinon nombre de mois d'expertise
			'moisexpertise'	=>	12,
			// nombre de services d'entretient manquant (0 pour aucun)
			'servicemanquant'=> 0,
			// service des X km qui n'a pas été fait
			'servicexkmmanq'=>	0,
			// barre de progression de 10 à 100
			'barreprogres'	=>	10
		),
		'distinctions'	=>	array(
			// true ou false
			'nonfumeur'		=>	true
		),
		// tableau de tableau
		'tableau'		=>	array(
			array(
				// mois et année
				'date'			=>	'08.2010',
				'km'			=>	124,
				'etablissement'	=>	'renault',
				// true ou false
				'facture'		=>	true,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Acheteur : détenteur N°1',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'07.2011',
				'km'			=>	17000,
				'etablissement'	=>	'Durant Pierre',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'clignotant droit',
				'detenteur'		=>	'Nouveau détenteur : détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'01.2013',
				'km'			=>	31570,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'06.2014',
				'km'			=>	40980,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'08.2015',
				'km'			=>	62520,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'02.2016',
				'km'			=>	75400,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			)
		)
	);
	
	
	
	$data2 = array(
		'id'		=>	2,
		//tableau avec une liste d'url
		'photos'	=>	array('http://i68.servimg.com/u/f68/14/88/53/89/autowp63.jpg', 'http://i68.servimg.com/u/f68/14/88/53/89/autowp63.jpg','http://i68.servimg.com/u/f68/14/88/53/89/autowp63.jpg', 'http://i68.servimg.com/u/f68/14/88/53/89/autowp63.jpg', 'http://i68.servimg.com/u/f68/14/88/53/89/autowp63.jpg'),
		'donnees'		=>	array(
			'marque'		=>	'Citroen',
			'modele'		=>	'2CV',
			'prix'			=>	'15559€',
			'annee'			=>	'2013',
			'km'			=>	'100000',
			'carrosserie'	=>	'Berline',
			'categorie'		=>	'Neuf',
			'cylindree'		=>	'600 cm3',
			'chevaux'		=>	'400',
			'transmission'	=>	'Automatique',
			'rouesmotrices'	=>	'4 roues',
			'nbvitesses'	=>	'10',
			'carburant'		=>	'Diesel',
			'nbportes'		=>	'2',
			'nbplaces'		=>	'4',
			'couleur'		=>	'Violet',
			// true ou false pour la couleur métalisée
			'metallise'		=>	true
		),
		'description'	=>	array(
			'rapide'		=>	'description rapide',
			'detaillee'		=>	'description detaillee'
		),
		'synthese'		=>	array(
			// vous serez le Xeme proprio (ici 2)
			'nombreproprio'	=>	20,
			// true ou false pour si le véhicule est d'origine suisse
			'suisse'		=>	true,
			// nombre de mois restant de garantie (ou 0)
			'fingarantie'	=>	0,
			// 0 pour pas d'expertise, sinon nombre de mois d'expertise
			'moisexpertise'	=>	0,
			// nombre de services d'entretient manquant (0 pour aucun)
			'servicemanquant'=> 1,
			// service des X km qui n'a pas été fait
			'servicexkmmanq'=>	10,
			// barre de progression de 10 à 100
			'barreprogres'	=>	100
		),
		'distinctions'	=>	array(
			// true ou false
			'nonfumeur'		=>	false
		),
		// tableau de tableau
		'tableau'		=>	array(
			array(
				// mois et année
				'date'			=>	'08.2010',
				'km'			=>	124,
				'etablissement'	=>	'renault',
				// true ou false
				'facture'		=>	true,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Acheteur : détenteur N°1',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'07.2011',
				'km'			=>	17000,
				'etablissement'	=>	'Durant Pierre',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'clignotant droit',
				'detenteur'		=>	'Nouveau détenteur : détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'01.2013',
				'km'			=>	31570,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'06.2014',
				'km'			=>	40980,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'08.2015',
				'km'			=>	62520,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'02.2016',
				'km'			=>	75400,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			)
		)
	);
	
	
	if ($_GET['exemple'] == 1) {
		afficher_rapport($data, false);
	}
	elseif ($_GET['exemple'] == 2) {
		afficher_rapport($data, true);
	}
	elseif ($_GET['exemple'] == 3) {
		afficher_rapport($data2, false);
	}
	elseif ($_GET['exemple'] == 4) {
		afficher_rapport($data2, true);
	}
}
// $data = tableau contenant toutes les données
// $ispdf = true pour pdf ou false pour à l'écran
function afficher_rapport($data, $ispdf) {
	if (!session_id())
		session_start();
	
	
	ob_start();
	$_SESSION['rapportdatas'] = serialize($data['tableau']);
	$mois = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
	?><html>
	<head>
	<title>Rapport</title>
	<link rel="stylesheet" type="text/css" href="static/style.css" media="screen">
	</head>
	<body id="pagerapport">
		
		<div class="sectiontitle">
			<img src="static/template photos.png" alt="Photos"/>
		</div>
		<div class="maincontent">
			<div class="photos">
			<?php
			foreach ($data['photos'] as $photo) {
				?><img class="photo" src="<?php echo $photo?>" alt="Photo"/><?php
			}
			?>
			</div>
		</div>
		<br/><br/>
			
			
			
			
		<div class="sectiontitle">
			<img src="static/template Donnees.png" alt="Données"/>
		</div>
		<div class="maincontent">
			<div class="threecolumn"><strong><?php echo htmlspecialchars($data['donnees']['prix'])?></strong></div>
			<div class="threecolumn"><strong><?php echo htmlspecialchars($data['donnees']['annee'])?></strong></div>
			<div class="threecolumn"><strong><?php echo htmlspecialchars($data['donnees']['km'])?> km</strong></div>
			<br/><br/><br/>
			
			<div style="page-break-after: always;">
			<div class="twocolumnheader">Carrosserie</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['carrosserie'])?></div><br/>
			<div class="twocolumnheader">Catégorie</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['categorie'])?></div><br/>
			<div class="twocolumnheader">Cylindrée</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['cylindree'])?></div><br/>
			<div class="twocolumnheader">Chevaux</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['chevaux'])?></div><br/>
			<div class="twocolumnheader">Transmission</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['transmission'])?></div><br/>
			<div class="twocolumnheader">Roues Motrices</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['rouesmotrices'])?></div><br/>
			<div class="twocolumnheader">Nb de vitesses</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['nbvitesses'])?></div><br/>
			<div class="twocolumnheader">Carburant</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['carburant'])?></div><br/>
			<div class="twocolumnheader">Nb de portes</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['nbportes'])?></div><br/>
			<div class="twocolumnheader">Nb de places</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['nbplaces'])?></div><br/>
			<div class="twocolumnheader">Couleur</div><div class="twocolumnright"><?php echo htmlspecialchars($data['donnees']['couleur']);
			if ($data['donnees']['metallise'])
				echo ' Peinture métallisée';
			?></div><br/>
			</div>
		</div>
		<br/><br/>
		
		
		<div class="sectiontitle">
			<img src="static/template Description.png" alt="Description"/>
		</div>
		<div class="maincontent">
			<h2><?php echo htmlspecialchars($data['description']['rapide'])?></h2>
			<br/>
			<h2><?php echo htmlspecialchars($data['description']['detaillee'])?></h2>
		</div>
		
		
		
		<br/><br/>
		<div class="sectiontitle">
			<img src="static/template Synthese.png" alt="Synthèse"/>
		</div>
		<div class="maincontent">
			<ul class="synthese">
				<li>En achetant ce véhicule, vous serez le <?php
				echo htmlspecialchars($data['synthese']['nombreproprio']);
				?><sup><?php
				if ($data['synthese']['nombreproprio'] == 1)
					echo 'er';
				else
					echo 'ème';
				?></sup> propriétaire.</li>
				
				<li>
				<?php
				if ($data['synthese']['suisse']) {
					?>Ce véhicule est d’origine Suisse.<?php
				}
				else {
					?>Ce véhicule n’est pas d’origine Suisse.<?php
				}
				?>
				</li>
				
				<li>
				<?php
				if ($data['synthese']['fingarantie']) {
					?>La garantie constructeur de ce véhicule est encore valable pendant <?php echo $data['synthese']['fingarantie']?> mois.<?php
				}
				else {
					?>La garantie constructeur n’est plus valable.<?php
				}
				?>
				</li>
				
				<li>
				<?php
				if ($data['synthese']['moisexpertise']) {
					?>Ce véhicule est expertisé par les services des automobiles et est encore valable pendant <?php echo $data['synthese']['moisexpertise']?> mois.<?php
				}
				else {
					?>Ce véhicule n’est pas expertisé par les services des automobiles.<?php
				}
				?>
				</li>
				
				<li>
				<?php
				if ($data['synthese']['servicemanquant']) {
					?>Il manque <?php echo $data['synthese']['servicemanquant']?> service(s) d'entretien. <?php
				}
				else {
					?>Carnet d’entretien à jour ! Tous les services d’entretien recommandés par le constructeur ont été réalisés. <?php
				}
				if ($data['synthese']['servicexkmmanq']) {
					?>Le service des <?php echo $data['synthese']['servicexkmmanq']?> km n’a pas été effectué.<?php
				}
				?>
				</li>
			</ul>
			
			
			<?php
			if (in_array($data['synthese']['barreprogres'], array(10, 20, 30, 40, 50, 60, 70, 80, 90, 100))) {
				?>Selon l'estimation du vendeur, l'utilisation de ce véhicule c'est déroulé en ville à hauteur de <?php echo $data['synthese']['barreprogres'] ?>%.<br/>
				<img class="barreprogression" src="static/Barres de progression - <?php echo $data['synthese']['barreprogres'] ?>.png"/><?php
			}
			
			?>
		</div>
		<br/><br/>
		
		
		
		
		<div class="sectiontitle">
			<img src="static/template Distinctions.png" alt="Distinctions"/>
		</div>
		<div class="maincontent"><?php
		
			if ($data['distinctions']['nonfumeur']) {
				?><img src="static/tick.png" style="width:2em;"/> Véhicule non fumeur<br/><?php
			}
			
			if ($data['synthese']['nombreproprio'] <= 1) {
				?><img src="static/tick.png" style="width:2em;"/> 1<sup>ère</sup> main<br/><?php
			}
			
			
			$differencielkm = $data['donnees']['km'] / (date('Y') - $data['donnees']['annee']);
			if (($data['donnees']['carburant'] == 'Essence' && $differencielkm < 15000)
				||
				($data['donnees']['carburant'] == 'Diesel' && $differencielkm < 20000)
				) {
					?><div class="distinctionkm">
						<img src="static/Distinction kilometrique.png" alt="Distinction"/>
						<div class="distinctionkminside">
						Ce véhicule obtient notre distinction kilométrique.<br/><br/>
						Il possède une moyenne kilométrique de <strong><?php echo round($differencielkm, 0);?></strong> km par an.</div>
					</div><?php
			}
			?><div style="clear:both"></div>
			<div class="cadreinfo" style="page-break-after: always;">
			La distinction kilométrique est attribuée aux véhicules qui possèdent une moyenne kilométrique inférieure à 15'000 km par an pour un moteur essence et 20'000 km par an pour un moteur Diesel.
			</div>
		</div>
		<?php
		
		
		
		
		
		
		
		?><div class="sectiontitle">
			<img src="static/template historique kilometrique.png" alt="Historique kilométrique"/>
		</div>
		<div class="maincontent">
			<div class="cadreinfo">
			Les points sur le graphique correspondent aux différents relevés kilométriques insérés par le vendeur. Le point 1 correspond à la 1ère mise en circulation du véhicule. <br/>
			La ligne <span style="color:blue">bleue</span> correspond à l’évolution kilométrique selon les interventions subies par le véhicule.<br/>
			Le dernier point (ici en <span style="color:red">rouge</span>) correspond au kilométrage du véhicule lors de son insertion sur autospot.
			</div>
		</div>
		<div class="maincontent">
		<?php
		
		if (!$ispdf) {
			?><img src="graph.php" /><?php
		}
		else {
			$opts = array(
			  'http'=>array(
				'method'=>"POST",
				'timeout'=>'10',
				'header'=>"Content-type: application/x-www-form-urlencoded\r\n" ,
				'content'=>http_build_query(array('passTheDatas' => $data['tableau']))
			  )
			);

			$context = stream_context_create($opts);

			$img = file_get_contents(dirname("http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}").'/graph.php', false, $context);
            $imgname = '_tmprapport_'.rand().rand().rand().rand().'.jpg';
			$tempfile3 = getcwd().'/'.$imgname;
			file_put_contents($tempfile3, $img);
			?><img src="<?php echo dirname("http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}")."/".$imgname;?>" /><?php
		}
		?>
		</div>


	
	
	
	
	<br/><br/><br/><br/>
		
		<table class="tableaukm">
			<tr>
				<th>Relevé</th>
				<th>Date</th>
				<th>Kilométrage</th>
				<th>Nature du relevé</th>
				<th>Nom de l'établissement</th>
				<th>Avec facture(s)</th>
				<th>Pièces changées</th>
				<th>Détenteur</th>
				<th>Commentaire</th>
			</tr>
			
			<?php
			$index = 1;
			foreach ($data['tableau'] as $line) {
				?><tr><td><?php echo $index?></td>
				<td class=""><?php 
				$thedate = date_parse_from_format('m\.Y', $line['date']);
				echo $mois[$thedate['month'] -1].' '.$thedate['year']; ?></td>
				<td class=""><?php echo htmlspecialchars($line['km'])?></td>
				<td class=""><?php 
				if ($index == 1) {
					?>1<sup>ère</sup> mise en circulation<?php
				}
				elseif ($index == count($data['tableau'])) {
					?>Insertion de l'annonce<?php
				}
				?></td>
				<td class=""><?php echo htmlspecialchars($line['etablissement'])?></td>
				<td class=""><?php 
				if ($index == 1 || $index == count($data['tableau'])) {
						?> - <?php
				}
				else {
					if ($line['facture']) {
						?>Oui<?php
					}
					else {
						?>Non<?php
					}
				}
				?></td>
				<td class=""><?php echo htmlspecialchars($line['piecechangee'])?></td>
				<td class=""><?php echo htmlspecialchars($line['detenteur'])?></td>
				<td class=""><?php echo htmlspecialchars($line['commentaire'])?></td>
				</tr><?php
				$index++;
			}
			?>
			
		</table>
		<br/>
		<br/>
		<div class="maincontent">
			<div class="cadreinfo">
			Les données fournies par le vendeur ne sont pas contrôlés par AutoSpot. <br/>
			Lors de la visite de ce véhicule, merci de contrôler le carnet d’entretien et, si disponible, les factures d’entretien correspondantes.
			</div>
		</div>
		
		<br/>
	</body>
	</html><?php
	
	$html = ob_get_clean();
	if (!$ispdf)
		echo $html;
	else {
		//$tempfile = tempnam(sys_get_temp_dir(), 'rapportpdfhtml');
		//chmod($tempfile, 0777);
		$tempfile2 = tempnam("/home/autospot/www/securspot/", 'rapportpdfhtml');
		chmod($tempfile2, 0777);
		
        $tempfilename = '/_tmprapport_'.rand().rand().rand().rand().'.htm';
		$tempfile = getcwd().$tempfilename;
        $tempfile_url = 'http://www.autospot.ch/securspot'.$tempfilename;
		//$tempfile2 = getcwd().'\bla.pdf';
		
		//file_put_contents($tempfile, mb_convert_encoding($html, 'ISO-8859-1', 'UTF-8'));
		file_put_contents($tempfile, iconv('UTF-8', 'WINDOWS-1252', $html));
		
		$theoutput = array();
		//echo '"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe" "'.escapeshellarg($tempfile).'" "'.$tempfile2.'"';
		//$theexec = exec('"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe" "'.escapeshellarg($tempfile).'" "'.escapeshellarg($tempfile2).'"', $theoutput);
		//$theexec = exec('"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe" "'.escapeshellarg($tempfile).'" "'.escapeshellarg($tempfile2).'"2>&1', $theoutput);
		$theexec = exec('"/usr/local/bin/wkhtmltox/bin/wkhtmltopdf" --replace thelogo '.(dirname("http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}").'/static/logo.png').' --replace rapportid '.escapeshellarg($data['id']).' --replace thedate '.escapeshellarg(date('d M Y')).' --replace subinfo '.escapeshellarg($data['donnees']['marque'].' '.$data['donnees']['modele']).' --footer-html "'.getcwd().'/static/footer.html"  --header-html "'.getcwd().'/static/header.html" "'.$tempfile_url.'" "'.$tempfile2.'"', $theoutput);
        
		$thecontent = file_get_contents($tempfile2);
		
		if ($thecontent) {
			header("Content-type:application/pdf");
			header("Content-Disposition:attachment;filename=rapport-autospot.pdf");
			echo $thecontent;
		}
		else {
			echo $theexec;
		}
		
		
		
		
		@unlink($tempfile);
		@unlink($tempfile2);
		@unlink($tempfile3);
	}
}


?>