<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
session_start();

$chd = '';
$chm = '';
$sizetext = 20;

$fmt = datefmt_create(
    'fr_FR',

    IntlDateFormatter::SHORT,
    IntlDateFormatter::NONE,
    NULL,
    IntlDateFormatter::GREGORIAN
);
$mois = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
$first = null;
$last = null;
$maxprice = 0;
$minprice = null;
$showedmaxprice = false;
$showedminprice = false;

$prices = array();

if (!empty($_POST['passTheDatas'])) {
	$datas = $_POST['passTheDatas'];
}
else
	$datas = unserialize($_SESSION['rapportdatas']);
/*$datas = array(
			array(
				// mois et année
				'date'			=>	'08.2010',
				'km'			=>	124,
				'etablissement'	=>	'renault',
				// true ou false
				'facture'		=>	true,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Acheteur : détenteur N°1',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'07.2011',
				'km'			=>	17000,
				'etablissement'	=>	'Durant Pierre',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'clignotant droit',
				'detenteur'		=>	'Nouveau détenteur : détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'01.2013',
				'km'			=>	31570,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'06.2014',
				'km'			=>	40980,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'08.2015',
				'km'			=>	62520,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			),
			array(
				// mois et année
				'date'			=>	'02.2016',
				'km'			=>	75400,
				'etablissement'	=>	'',
				// true ou false
				'facture'		=>	false,
				'piecechangee'	=>	'',
				'detenteur'		=>	'Détenteur N° 2',
				'commentaire'	=>	'Oui (cliquez ici ) ou Non'
			)
		);*/

foreach ($datas as $app) {
	$blatmp = date_create_from_format('m\.Y', $app['date']);
	$app['dateuptimestamp'] = $blatmp->getTimestamp();


	if (!$first) {
		$first = $app;
		$minprice = $app['km'];
	}
	$last = $app;
	
	$prices[] = $app;
	if ($maxprice < $app['km'])
		$maxprice = $app['km'];
	if ($minprice > $app['km'])
		$minprice = $app['km'];
}


if (count($prices) == 1) {
	$prices[] = $prices[0];
}

$percentprice = $maxprice?(100/$maxprice):0;
$firstdate = ($first['dateuptimestamp']);
$lastdate = /*time();//*/($last['dateuptimestamp']);
$diffdate = $lastdate - $firstdate;
$stepsdate = $diffdate / 200;
$numprices = count($prices);
$numlabel = 0;

$i = 0;
$prev = null;
$chcos = array();
foreach ($prices as $priceindex => $price) {
	
	$isokay = 0;
	do {
		if ($numlabel >= (count($prices) -2))
				$chcos[] = 'FF0000';
			else
				$chcos[] = '4f81bd';
		
		$extrakm = 0;
		
		
		
		if ($firstdate + $i * $stepsdate >= $price['dateuptimestamp']) {
			if (isset($prices[$priceindex +1])) {
				$difftime = $prices[$priceindex +1]['dateuptimestamp'] - $price['dateuptimestamp'];
				$diffval = $prices[$priceindex +1]['km'] - $price['km'];
				//$extrakm = $diffval * (($i * $stepsdate) / ($prices[$priceindex +1]['dateuptimestamp'] - $price['dateuptimestamp']));
				$extrakm = $diffval * ((($firstdate +$i * $stepsdate) - $prices[$priceindex +0]['dateuptimestamp']) / ($prices[$priceindex +1]['dateuptimestamp'] - $prices[$priceindex -0]['dateuptimestamp']));
			}
			
			$chd .= ($chd!=''?',':'').round(($price['km'] + $extrakm)*$percentprice,2);
			
			if ($numlabel == (count($prices) -1)) {
				$color = 'FF0000';
			}
			else
				$color = '4f81bd';
			
			/*if ($prev == null || $prev == $price['km'])
				$color = '555555';
			elseif ($prev > $price['km'])
				$color = '005500';
			else
				$color = 'FF0000';*/
			
			if ($numprices < 10 || ($numlabel > ($numprices - 7)) || ($maxprice == $price['price'] && !$showedmaxprice && ($showedmaxprice = true)) || ($minprice == $price['km'] && !$showedminprice && ($showedminprice = true))) {
				//$chm .= ($chm!=''?'|':'').('A'.($price['price']!=-0.01?$price['price']:'?').urlencode($currency).','.$color.',0,'.$i.','.$sizetext);
				$chm .= ($chm!=''?'|':'').('A'.($numlabel +1).','.$color.',0,'.$i.','.$sizetext);

			}

			$isokay = 1;
			$i++;
			$numlabel++;
		}
		else {
			if (isset($prices[$priceindex])) {
				//$difftime = $prices[$priceindex +1]['dateuptimestamp'] - $price['dateuptimestamp'];
				$diffval = $price['km'] - $prev;
				$extrakm = $diffval * ((($firstdate +$i * $stepsdate) - $prices[$priceindex -1]['dateuptimestamp']) / ($prices[$priceindex -0]['dateuptimestamp'] - $prices[$priceindex -1]['dateuptimestamp']));
				//echo ((($firstdate +$i * $stepsdate) - $prices[$priceindex -1]['dateuptimestamp']) / ($prices[$priceindex -0]['dateuptimestamp'] - $prices[$priceindex -1]['dateuptimestamp'])).'<br>';
			}
			$chd .= ($chd!=''?',':'').round(($prev+$extrakm)*$percentprice,2);
			$i++;
		}

	}
	while ($isokay != 1);

	$prev = $price['km'];
}
//exit;
/*while ($i < 200) {
	$chd .= ($chd!=''?',':'').round($prev*$percentprice,2);
	$chcos[] = 'FF0000';
	$i++;
}*/
if (count($prices) > 1 && $last['dateuptimestamp'] > (time() - 432000))
	$chd .= ','.round($prev*$percentprice,2);



// 5 jours
//if ($diffdate >= 432000) {
	//$chxl1 = date('j/n/y', $firstdate).'|'.date('j/n/y',($firstdate + $diffdate/4)).'|'.date('j/n/y',($firstdate + $diffdate/4*2)).'|'.date('j/n/y',($firstdate + $diffdate/4*3)).'|'.date('j/n/y', $lastdate);
	
	$thedates = array(
		$firstdate,
		($firstdate + $diffdate/4),
		($firstdate + $diffdate/4*2),
		($firstdate + $diffdate/4*3),
		$lastdate
	);
	$chxl1 = '';
	foreach ($thedates as $thedate) {
		if ($chxl1)
			$chxl1 .= '|';
		$chxl1 .= $mois[(date('n', $thedate) -1)].' '.date('Y', $thedate);
	}
	
	//$chxl1 = urlencode(datefmt_format($fmt, $firstdate)).'|'.urlencode(datefmt_format($fmt, ($firstdate + $diffdate/4))).'|'.urlencode(datefmt_format($fmt, ($firstdate + $diffdate/4*2))).'|'.urlencode(datefmt_format($fmt, ($firstdate + $diffdate/4*3))).'|'.urlencode(datefmt_format($fmt, $lastdate));
//}
//else {
	//$chxl1 = date('j/n/y', $firstdate).'|'.date('j/n/y', $lastdate);
//	$chxl1 = urlencode(datefmt_format($fmt, $firstdate)).'|'.urlencode(datefmt_format($fmt, $lastdate));
//}
//$chxl2 = '0'.urlencode($currency).'|'.round($maxprice/2,2).urlencode($currency).'|'.$maxprice.urlencode($currency);
$chxl2 = '|'.round(($maxprice/4)*1,0).' km'.'|'.round(($maxprice/4)*2,0).' km'.'|'.round(($maxprice/4)*3,0).' km'.'|'.$maxprice.' km';

if (1) {
	$context = stream_context_create(
    array('http' => array(
      'method' => 'POST',
      'content' => http_build_query(array('cht' => 'lc', 'chd' => 't:'.$chd, 'chco' => implode('|', $chcos), 'chls' => '2.0,4.0,1.0', 'chs' => '1000x300', 'chxt' => 'x,y', 'chxl' => '0:|'.urldecode($chxl1.'|1:|'.$chxl2), 'chm' => urldecode($chm), 'chma' => '30,40,30,0', 'chg' => '25,50', 'chxs' => '0,,'.$sizetext.'|1,,'.$sizetext)),
	  'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0\r\n",
	  )));
	$data = file_get_contents('https://chart.googleapis.com/chart', false, $context);
}
else {
	$url = 'https://chart.googleapis.com/chart?cht=lc&chd=t:'.$chd.'&chco=008000&chls=2.0,4.0,1.0&chs=1000x200&chxt=x,y&chxl=0:|'.$chxl1.'|1:|'.$chxl2.'&chm='.$chm.'&chma=30,40,30,0&chg=25,50&chxs=0,,'.$sizetext.'|1,,'.$sizetext;
	$data = file_get_contents($url);
}
header('Content-type:image/png');
echo $data;



exit;
require 'vendor/autoload.php';

 $piChart = new gchart\gLineChart();


 $piChart->addDataSet(array(112,315,66,40));
 //$piChart->setLabels(array("first", "second", "third","fourth"));
 $piChart->setLegend(array("first", "second", "third","fourth"));
 $piChart->setColors(array("ff3344", "11ff11", "22aacc", "3333aa"));
 echo $piChart->getImgCode();
 
?>