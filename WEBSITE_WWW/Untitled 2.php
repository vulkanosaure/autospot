            <!-- rapport 200Mil -->
            <div id="service200Mil" class="form-group collapse">
            <div class="rapportKm">0 - 200'000 km</div>
            <label class="control-label col-sm-1 small" for="releve200MilNature">Nature du relev�</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve200MilNature" name="releve200MilNature">
                <option value="1�re r�vision constructeur">1�re r�vision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="R�paration carrosserie">R�paration carrosserie</option>
                <option value="R�paration moteur">R�paration moteur</option>
                <option value="R�paration vitrage">R�paration vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km200Mil">Ins�rez le kilom�trage correspondant � ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km200Mil" name="km200Mil">
            </div>
        
            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois200Mil" name="mois200Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">F�vrier</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Ao�t</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">D�cembre</option>
            </select><br />
            <select class="form-control" id="annee200Mil" name="annee200Mil">
  		        <option value="" selected>ann�e</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>
                    
            <label class="control-label col-sm-1 small" for="etablissement200Mil">Ins�rez le nom de l'�tablissement o� ce service a �t� r�alis�</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement200Mil" name="etablissement200Mil">
            </div>
            
            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture200Mil">Poss�dez-vous la/les factures correspondantes � ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture200MilOui" type="radio" name="facture200Mil" value="1"><label for="facture200MilOui">Oui</label>
            <br/>
            <input id="facture200MilNon" type="radio" name="facture200Mil" value="0"><label for="facture200MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture200MilOui').change(function(){
                    if(this.checked)
                    $("#facture200Oui").css("display", "");  // show
                    else
                    $("#facture200Oui").css("display", "none");  // hide
                });
                $('#facture200MilNon').change(function(){
                    if(this.checked)
                    $("#facture200Oui").css("display", "none");
                    else
                    $("#facture200Oui").css("display", "");
                    });
                </script>
                
                <div id="facture200Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue200Mil">Ins�rez le/les services qui ont �t� effectu�s</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue200Mil" name="serviceEffectue200Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee200Mil">Ins�rez la/les pi�ces qui ont �t� chang�es</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee200Mil" name="pieceChangee200Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur200Mil">Est-ce que le nom du d�tenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur200MilOui" type="radio" name="nomDetenteur200Mil" value="1"><label for="nomDetenteur200MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur200MilNon" type="radio" name="nomDetenteur200Mil" value="0"><label for="nomDetenteur200MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur200MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur200Oui").css("display", "");  // show
                    else
                    $("#detenteur200Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur200MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur200Oui").css("display", "none");
                    else
                    $("#detenteur200Oui").css("display", "");
                    });
                </script>
                <div id="detenteur200Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom200Mil">Choisissez la 1�re lettre du pr�nom du d�tenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom200Mil" name="prenom200Mil">
  				<option value="" selected>1�re l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp200Mil" name="cp200Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>
                
                <textarea class="form-control" rows="5" id="com200Mil" name="com200Mil" placeholder="Inscrivez votre commentaire..."></textarea>
                
                <div class="clear"></div>
        
        </div>
        
        <div class="clear"></div>
            <!-- /rapport 200Mil -->