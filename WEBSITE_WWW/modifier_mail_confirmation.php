<?php
session_start();

include("db_connexion.php");

$id_client = null;
$key = array_key_exists('key', $_GET) && !empty($_GET['key']) ? trim($_GET['key']) : null;
if (null === $key) {
    header('Location: inscription.php');
}

$sql = sprintf('SELECT id FROM clients WHERE MD5(email) = "%s"', mysqli_real_escape_string($connect1, $key));
$query = mysqli_query($connect1, $sql);
$client = mysqli_fetch_object($query);

if (null === $client) {
    header('Location: inscription.php');
}

$sql = sprintf('UPDATE clients SET email=email_new,email_new=NULL WHERE id = "%s"', mysqli_real_escape_string($connect1, $client->id));
mysqli_query($connect1, $sql);

include("header.php");
include("body.php");
?>

<div class="container">
    <h3 class="center">Confirmez votre nouvelle adresse email</h3>

    <div class="container">
        <p class="center">Le changement d’adresse email a été effectué avec succès ! Veuillez <a class="link" href="/inscription.php">cliquer ici</a> pour vous connecter à votre nouveau compte utilisateur</p>
    </div>
</div>

<?php
include("footer.php");