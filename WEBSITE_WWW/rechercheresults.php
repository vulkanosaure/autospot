<?php
require_once('classes/annonce.php');
$nondefini = $LANG[5];
$annee = explode("-", $annonces->premiereimm);
$annee = $annee[1].".".$annee[0];




$expertisee = explode("-", $annonces->expertisee);
$expertisee = $expertisee[1].".".$expertisee[0];

$ville = "";
$canton = "";
if($annonces->ville == "")
{
	$requete_ville = mysqli_query($connect1, "SELECT ville FROM liste_npa WHERE code_postal='$annonces->codepostal'");
	$ville = mysqli_fetch_object($requete_ville);
    if(isset($ville->ville))
	   $ville = $ville->ville;
}
else
	$ville = $annonces->ville;
if($annonces->canton == "")
{
	$requete_canton = mysqli_query($connect1, "SELECT canton FROM liste_npa WHERE code_postal='$annonces->codepostal'");
	$canton = mysqli_fetch_object($requete_canton);
    if(isset($canton->canton))
	   $canton = $canton->canton;
}
else
	$canton = $annonces->canton;
$ville = trim($ville);
if(preg_match("/ $canton/", $ville))
	$ville = preg_replace("/ $canton/", "", $ville);
else if(preg_match("/\/ $canton/", $ville))
	$ville = preg_replace("/\/ $canton/", "", $ville);
else if(preg_match("/($canton)/", $ville))
	$ville = preg_replace("/($canton)/", "", $ville);

if (isset($_SESSION['id_client']))
{
    $sql = sprintf(
        'SELECT id FROM clients_favoris WHERE client = "%s" AND annonce = "%s"',
        mysqli_real_escape_string($connect1, $_SESSION['id_client']),
        mysqli_real_escape_string($connect1, $annonces->id)
    );
    $requete_favoris = mysqli_query($connect1, $sql);
    $favoris = mysqli_num_rows($requete_favoris);
}
$lienannonce = "annonce.php?id=".$annonces->id;
?>
<!-- LIGNE RESULTAT I -->
<div class="col-sm-12 cars">
<div class="col-sm-3">

<div class="prixmobile"><?PHP echo number_format($annonces->prix, 0, ",", "'").".-";?></div>


<!--  Lightgallery -->
                <div class="carSlider<?PHP echo $j;?>">
        <ul id="imageGallery<?PHP echo $j;?>">
        <?PHP
        $imagei = 1;
        $imagetotal = 1;
        $object = "image".$imagetotal;
        $image = $annonces->$object;
        $mk=0;

        while(isset($image))
        {
            if($image != "")
                $imagetotal++;
            $imagei++;
            $object = "image".$imagei;
            if(isset($annonces->$object))
                $image = $annonces->$object;
            else
                unset($image);
        }
        $imageok = 0;
        $imagetotal = $imagetotal - 1;
        $imagei = 1;
        $imagecompteur = 1;
        $object = "image".$imagei;
        $image = $annonces->$object;
        while(isset($image))
        {
            if($image != "")
            {
                $imageok = 1;
                ?>
            <li class='mini-slider-item' data-thumb="img/thumbs/<?PHP echo $image;?>" data-src="img/<?PHP echo $image;?>">
                <?PHP if($imagei == 1){?><a href="<?PHP echo $lienannonce;?>" class="spothide"><img src="img/<?PHP echo $image;?>" /></a><?PHP }?>
                <img src="img/<?PHP echo $image;?>" <?PHP if($imagei == 1){?>class="spotshow"<?PHP }?>/>
                <span class="imgNum mk<?php echo $imagecompteur; ?>"><p><?PHP echo $imagecompteur." / ".$imagetotal;?></p></span>
                </li>
                <?PHP
                $imagecompteur++;
            }
            $imagei++;
        $mk++;
            
            $object = "image".$imagei;
            if(isset($annonces->$object))
                $image = $annonces->$object;
            else
                unset($image);
        }
        if($imageok == 0)
        {
            ?>
            <li data-thumb="img/thumbs/no_image.png" data-src="img/no_image.png">
            <a href="<?PHP echo $lienannonce;?>" class="spothide"><img src="img/no_image.png" /></a>
            <img src="img/no_image.png" class="spotshow"/>
            <span class="imgNum"><p>1</p></span>
            </li>
            <?PHP
        }
        ?>
        </ul>
    </div>
                <!-- /Lightgallery -->
<script type="text/javascript">
    $('#imageGallery<?PHP echo $j;?>').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:10,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                download: false,
                selector: '#imageGallery<?PHP echo $j;?> .lslide'
								
            });
						
						$('ul[id^="imageGallery"]').css('height', 'unset');
        }
    });
</script>

</div>

<div class="col-sm-9">

<!-- LIGNE 1 -->
<div class="col-sm-12 cardesc1">
<!-- lien vers la page de l'annonce -->
    <a href="<?PHP echo $lienannonce;?>" class="black">
<div class="col-xs-5 desc-modele"><h5><?PHP echo $annonces->marque." ".$annonces->modele." ".$annonces->version;?></h5></div>
<div class="col-xs-1 desc-annee"><h5><?PHP if($annee != "00.0000") echo $annee; else echo $nondefini;?></h5></div>
<div class="col-xs-2 desc-km"><h5><?PHP echo $annonces->kilometrage;?> km</h5></div>
<div class="col-xs-2 desc-prix"><h5><b><?PHP echo number_format($annonces->prix, 0, ",", "'").".-";?></b></h5></div>
    </a>
<div class="col-xs-1 desc-fav">
    <?php if (!isset($_SESSION['id_client'])): ?>
        <a href="inscription.php">
    <?php else: ?>
        <a href="#" onclick="favoris('<?php echo $annonces->id;?>');">
    <?php endif; ?>
        <?php $class = isset($favoris) && $favoris > 0 ? "desc-fav-active" : "desc-fav-inactive"; ?>
        <div id="desc-fav<?php echo $annonces->id;?>" class="<?php echo $class; ?>"></div>
    </a>
</div>
</div>
<!-- FIN LIGNE 1 -->

<!-- LIGNE 2 -->
<div class="col-sm-12 cardesc2">

<!-- LIGNE 2 COLONNE 1-->
<div id="infos-<?PHP echo $j;?>" class="col-sm-9 collapse">
<div class="col-sm12 cardesc3">
<div class="col-xs-4 desc-cm"><?PHP if($annonces->cylindrees != 0) echo $annonces->cylindrees." cm3"; else echo "Cylindrée: ".$nondefini;?></div><div class="col-xs-4 desc-chevaux"><?PHP if($annonces->puissancechevaux != 0) echo $annonces->puissancechevaux." ".$LANG[6]; else echo $LANG[7].": ".$nondefini;?></div><div class="col-xs-4 desc-carbu"><?PHP if(($annonces->carburant != "") && (preg_match("/Essence/ui", $annonces->carburant))) echo $LANG[8]; else if(($annonces->carburant != "") && ($annonces->carburant != "Autres")) echo $annonces->carburant; else echo $LANG[9].": ".$nondefini;?></div>
</div>
<div class="col-sm12 cardesc4">
<div class="col-xs-4 desc-boite"><?PHP if($annonces->transmission != "") echo $LANG[10]." ".$annonces->transmission; else echo $LANG[11].": ".$nondefini;?></div>
<div class="col-xs-4 desc-expert">

<?PHP echo $LANG[12]; ?>: <?PHP if ($expertisee != "00.0000") echo $expertisee;
else if ($annonces->year >= date("Y") - 4) echo $LANG[13];
else echo 'N.D'; ?>

</div><div class="col-xs-4">
<?PHP
$requete_securspot = mysqli_query($connect1, "SELECT fichier FROM annonces_securspot WHERE id_annonce='$annonces->id'");
if(mysqli_num_rows($requete_securspot) == 1)
{
	$securspot = mysqli_fetch_object($requete_securspot);
	?><span style="widht:66px;height:34px;"><a href="/rapports/<?PHP echo $securspot->fichier;?>" target="_blank"><img class="autoWidth" src="images/securspot.png" width="66" height="34"/></a></span><?PHP
}
?>
</div><div class="col-xs-4 desc-lieu">
	<?PHP 
	
	if($annonces->codepostal != 0) echo $annonces->codepostal_a." ".$annonces->ville_a." (".$annonces->canton_a.")";
	?></div>
</div>
<div class="col-sm12 descrapide"><?PHP echo $annonces->desc1;?></div>
</div>
<!-- FIN LIGNE 2 COLONNE 1-->

<!-- LIGNE 2 COLONNE 2-->
<div class="col-sm-3">
<div class="col-sm12 cardesc5">
<div class="retour"></div>

<div class="visu">
		<?php 
		
		$sql = sprintf(
			'SELECT * FROM clients WHERE id="%s" LIMIT 1;',
			mysqli_real_escape_string($connect1, $annonces->id_client)
		);
		$query = mysqli_query($connect1, $sql);
		$client = mysqli_fetch_object($query);
		
		if($client->type != 2) echo annonce::displayLink($annonces, true);
		?>
</div>
<div class="fav">
    <?php if (!isset($_SESSION['id_client'])): ?>
        <a href="inscription.php">
    <?php else: ?>
        <a href="#" onclick="favoris('<?php echo $annonces->id;?>');">
    <?php endif; ?>
        <?php $class = isset($favoris) && $favoris > 0 ? "desc-fav-active" : "desc-fav-inactive"; ?>
        <div id="desc-fav<?php echo $annonces->id;?>" class="<?php echo $class; ?>"></div>
    </a>
</div>
<div class="plus">
<a href="#myModal-<?php echo $j;?>" data-toggle="collapse" data-target="#infos-<?PHP echo $j;?>" class="collapsed"><i class="fa fa-plus"></i><i class="fa fa-minus"></i> d'infos</a>
</div>
</div>
</div>
<!-- FIN LIGNE 2 COLONNE 2-->

</div>
<!-- FIN LIGNE 2 -->

</div>
</div>
  <!-- FIN LIGNE RESULTAT I -->
<div class="clearcars"></div>