<?php
if (!isset($occurence)) {
    $occurence = 1;
}
if (!isset($collapse)) {
    $collapse = false;
}
if (!isset($subtitle)) {
    $subtitle = false;
}
?>
<?php if ($subtitle) : ?>
    <h3>
        <?php if ($collapse): ?>
            <a class="scanexpert-controles-toggle" href="#scanexpert-details" data-toggle="collapse" role="button" aria-expanded="false">
                <i class="fa fa-plus-circle blue"></i>
        <?php endif; ?>
        Forfait ScanExpert
        <?php if ($collapse): ?>
            </a>
        <?php endif; ?>
    </h3>
<?php endif; ?>
<?php if ($subtitle && $collapse): ?>
    <div id="scanexpert-details" class="collapse">
<?php endif; ?>
<ul class="scanexpert-controls">
    <li>
        <a class="visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>0" role="button" aria-expanded="false">
            <i class="fa fa-check blue"></i>
            <strong>&nbsp;Photographies (int. et ext.) de la voiture, des documents et, le cas échéant, des éléments endommagés.</strong>
        </a>
        <strong class="hidden-xs">Photographies (int. et ext.) de la voiture, des documents et, le cas échéant, des éléments endommagés.</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>0" class="scanexpert-controles-details hidden-xs collapse">
        </div>
    </li>
    <li>
        <a class="scanexpert-controles-toggle visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>1" role="button" aria-expanded="false">
            <i class="fa fa-plus-circle blue"></i>
            <strong>&nbsp;Vérification de la conformité du véhicule</strong>
        </a>
        <strong class="hidden-xs">Vérification de la conformité du véhicule</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>1" class="scanexpert-controles-details hidden-xs collapse">
            <i class="fa fa-check blue"></i>&nbsp;Photographie du compteur kilométrique et insertion du kilométrage affiché sur le compteur kilométrique.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification et comparaison du kilométrage affiché sur le compteur kilométrique et celui affiché lors du dernier service/vidange.<br />
            <i class="fa fa-check blue"></i>&nbsp;Selon les informations fournies par le vendeur, vérification si la/les pièce(s)/accessoire(s) non d'origine sont inscrites sur le permis de circulation et si le certification d'homologation suisse est présent.
        </div>
    </li>
    <li>
        <a class="scanexpert-controles-toggle visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>2" role="button" aria-expanded="false">
            <i class="fa fa-plus-circle blue"></i>
            <strong>&nbsp;Vérification de la conformité des pièces administratives</strong>
        </a>
        <strong class="hidden-xs">Vérification de la conformité des pièces administratives</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>2" class="scanexpert-controles-details hidden-xs collapse">
            <i class="fa fa-check blue"></i>&nbsp;Vérification que le N° de série apposé sur le châssis correspondent à ce qui est inscrit sur le permis de circulation.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification des plaques d'immatriculation.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification du manuel d'utilisation.
        </div>
    </li>
    <li>
        <a class="scanexpert-controles-toggle visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>3" role="button" aria-expanded="false">
            <i class="fa fa-plus-circle blue"></i>
            <strong>&nbsp;Vérification de l'entretien du véhicule.</strong>
        </a>
        <strong class="hidden-xs">Vérification de l'entretien du véhicule.</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>3" class="scanexpert-controles-details hidden-xs collapse">
            <i class="fa fa-check blue"></i>&nbsp;Vérification de la présence du carnet d'entretien (standard ou électronique) et photographie.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification, si présent, du/des facture(s) d'entretien et photographie.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification, si présent, du/des facture(s) de sinistre.<br />
            <i class="fa fa-check blue"></i>&nbsp;Information sur le nombre de propriétaire(s) précédent(s).<br />
            <i class="fa fa-check blue"></i>&nbsp;Informations sur le type d’utilisation et de la consommation en carburant du véhicule.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de la présence du double des clés/télécommande
        </div>
    </li>
    <li>
        <a class="scanexpert-controles-toggle visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>6" role="button" aria-expanded="false">
            <i class="fa fa-plus-circle blue"></i>
            <strong>&nbsp;Examen détaillé des organes de sécurité (voiture au sol et levée)</strong>
        </a>
        <strong class="hidden-xs">Examen détaillé des organes de sécurité (voiture au sol et levée)</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>6" class="scanexpert-controles-details hidden-xs collapse">
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'absence de témoins d'alerte/alarme au tableau de bord.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification que les principaux témoins au tableau de bord s'allument puis s'éteignent.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état des suspensions.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l’état des organes de la direction.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'efficacité du levier de frein de stationnement.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification du fonctionnement du volant de direction.<br />
            <i class="fa fa-check blue"></i>&nbsp;État des jantes selon photo(s).<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état des pneumatiques et de la profondeur de leur sculpture.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'absence de jeux aux roues.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de la présence d'une roue de secours ou d'un nécessaire anti-crevaison.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification du fonctionnement du siège conducteur.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification du fonctionnement du système d'éclairage et de signalisation.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état des optiques avant et arrière.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état et du fonctionnement des verres de rétroviseurs.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état et du fonctionnement des essuie-glaces et des gicleurs d’essuie-glaces.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état et du fonctionnement des ceintures de sécurités conducteur, passager et arrière (si présent).
        </div>
    </li>
    <li>
        <a class="scanexpert-controles-toggle visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>5" role="button" aria-expanded="false">
            <i class="fa fa-plus-circle blue"></i>
            <strong>&nbsp;Examen détaillé de la mécanique et du châssis (voiture au sol et levée)</strong>
        </a>
        <strong class="hidden-xs">Examen détaillé de la mécanique et du châssis (voiture au sol et levée)</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>5" class="scanexpert-controles-details hidden-xs collapse">
            <i class="fa fa-check blue"></i>&nbsp;Branchement du lecteur de diagnostic électronique.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état du compartiment moteur et identification de fuite de fluide si celle-ci est immédiatement visible.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification du type de distribution (courroie, chaine ou cascade de pignons).<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l’état des soufflets de cardan.<br />
            <i class="fa fa-check blue"></i>&nbsp;Identification de fuite de fluide si celle-ci est immédiatement visible.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état du châssis (longeron, berceau, brancard) et des bas de caisse.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état, de l’étanchéité et de la fixation du système d'échappement et du pot catalytique.<br />
            <i class="fa fa-check blue"></i>&nbsp;A l’arrêt, vérification du fonctionnement du volant de direction.<br />
        </div>
    </li>
    <li>
        <a class="scanexpert-controles-toggle visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>4" role="button" aria-expanded="false">
            <i class="fa fa-plus-circle blue"></i>
            <strong>&nbsp;Examen détaillé du vitrage et état de la carrosserie selon photos</strong>
        </a>
        <strong class="hidden-xs">Examen détaillé du vitrage et état de la carrosserie selon photos</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>4" class="scanexpert-controles-details hidden-xs collapse">
            <i class="fa fa-check blue"></i>&nbsp;Etat de la carrosserie selon photos.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'ouverture/fermeture des ouvrants.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification de l'état et du fonctionnement du vitrage.
        </p>
    </li>
    <li>
        <a class="scanexpert-controles-toggle visible-xs" data-toggle="collapse" href="#scanexpert-controles-<?php echo $occurence; ?>8" role="button" aria-expanded="false">
            <i class="fa fa-plus-circle blue"></i>
            <strong>&nbsp;Examen détaillé de l'intérieur et de certains équipements</strong>
        </a>
        <strong class="hidden-xs">Examen détaillé de l'intérieur et de certains équipements</strong>
        <div id="scanexpert-controles-<?php echo $occurence; ?>8" class="scanexpert-controles-details hidden-xs collapse">
            <i class="fa fa-check blue"></i>&nbsp;Vérification du fonctionnement de la fermeture centralisée.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification du fonctionnement de la ventilation et de la climatisation.<br />
            <i class="fa fa-check blue"></i>&nbsp;Vérification du fonctionnement du compteur kilométrique et de l’avertisseur sonore.<br />
        </div>
    </li>
</ul>	<!-- modif -->
<p>
    <strong>
			
        <i class="testRoutierControl fa fa-plus-circle fa-2x blue" data-toggle="collapse" data-target="#testRoutier<?php echo $occurence; ?>"></i> Test routier (6 points de contrôle).
    </strong>
</p>
<div id="testRoutier<?php echo $occurence; ?>" class="collapse">
    <p><i class="fa fa-check blue"></i> Ecoute des bruits : roulement, échappement.</p>
    <p><i class="fa fa-check blue"></i> Contrôle de la puissance du moteur.</p> 
    <p><i class="fa fa-check blue"></i> Contrôle du passage des rapports (transmission manuelle/automatique).</p> 
    <p><i class="fa fa-check blue"></i> Contrôle du comportement et de l'efficacité du système de freinage.</p> 
    <p><i class="fa fa-check blue"></i> Contrôle ressentie de la liaison au sol : Suspensions.</p> 
    <p><i class="fa fa-check blue"></i> Contrôle du fonctionnement de la caméra de recul et du compteur kilométrique.</p> 
    <p><i class="fa fa-check blue"></i> Efficacité ressentie de l'embrayage.</p> 
    <p>&nbsp;</p>
</div>
<?php if ($subtitle && $collapse): ?>
    </div>
<?php endif; ?>