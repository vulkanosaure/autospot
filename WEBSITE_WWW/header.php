<?php

//TODO detection environment

/* 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); 
*/


if(!isset($_SESSION))
    session_start();
if (isset($_GET['lang']))
    $_SESSION['lang'] = $_GET['lang'];
else if (!isset($_SESSION['lang']))
    $_SESSION['lang'] = "fr";
include("langues/lang_$_SESSION[lang].php");
if (!isset($connect1))
    include("db_connexion.php");
$tableannonces = "annonces_clients";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr-FR" lang="fr-FR">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/custom_styles.css" />
        <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/steps.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/steps.js"></script>

        <!-- include summernote css/js-->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>

        <!-- Selectpicker pour les pictos carrosserie dans le select -->
        <script src="js/bootstrap-select.js"></script>
        <link rel="stylesheet" type="text/css" href="css/bootstrap-select.css" />
        <!-- lightslider -->
        <link rel="stylesheet" type="text/css" href="css/lightslider.css" />
        <script src="js/lightslider.js"></script>
        <link rel="stylesheet" type="text/css" href="css/lightgallery.css" />
        <script src="js/lightgallery.js"></script>
        <link rel="stylesheet" href="bower_components/swiper/dist/css/swiper.css" />
        <script src="bower_components/swiper/dist/js/swiper.js"></script>

        <?php if (preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) : ?>
            <title>MotoSpot.ch <?php echo $LANG[151]; ?></title>
            <link rel="stylesheet" href="css/style_motospot.css">
            <link rel="shortcut icon" type="image/x-icon" href="http://www.autospot.ch/motospot.ico">
        <?php else: ?>
            <title>AutoSpot.ch <?php echo $LANG[151]; ?></title>
            <link rel="stylesheet" href="css/style_autospot.css">
            <link rel="shortcut icon" type="image/x-icon" href="http://www.autospot.ch/autospot.ico">
        <?php endif; ?>

        <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.5/css/lightslider.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.5/js/lightslider.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/css/lightgallery.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lightgallery.js"></script>-->
        <!-- /lightslider  -->


        <!--    SCRIPT onSCROLL POUR FIXER les FILTRES version MOBILE-->
        <script type='text/javascript'>
            jQuery(function () {
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > 20) { //hauteur à laquelle on veut déclencher la fixation en haut du menu
                        jQuery('#fixedMenu').addClass("fixMenu");
                        jQuery('#posRel').addClass("hidden");
                        jQuery('#posFix').removeClass("hidden");
                        jQuery('#pushFiltres').removeClass("noPush");
                        jQuery('#pushFiltres').addClass("push");
                    } else {
                        jQuery('#fixedMenu').removeClass("fixMenu");
                        jQuery('#posRel').removeClass("hidden");
                        jQuery('#posFix').addClass("hidden");
                        jQuery('#pushFiltres').removeClass("push");
                        jQuery('#pushFiltres').addClass("noPush");
                    }
                });
                jQuery('body').on('click', '.cls-toggle-class', function () {
                    jQuery('body').toggleClass('noscroll');
                });
            });
            jQuery(document).ready(function ($) {
                $("#posFix").click(function (event) {
                    /* Act on the event */
                    event.preventdefault();
                });
            });
        </script>
        <!--    SCRIPT onSCROLL POUR FIXER les FILTRES version MOBILE-->

        <!-- Smartsupp Live Chat script -->
        <script type="text/javascript">
            var _smartsupp = _smartsupp || {};
            _smartsupp.key = '38e51d02f5226c6195dcdcd6289e5ae6a7fe02a0';

            window.smartsupp||(function(d) {
                var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
                s=d.getElementsByTagName('script')[0];c=d.createElement('script');
                c.type='text/javascript';c.charset='utf-8';c.async=true;
                c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
            })(document);
        </script>
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
    </head>