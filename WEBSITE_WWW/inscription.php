<?php
/* 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */
require_once('vendor/autoload.php');
session_start();

if (isset($_SESSION['id_client'])) {
	header("Location: compte.php");
	exit();
}

include('db_connexion.php');
include('header.php');
include('body.php');

$referer = array_key_exists('referer', $_GET) && !empty($_GET['referer']) ? trim($_GET['referer']) : null;
?>
    
<!-- REGISTER -->
<div class="container">
<?php
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
if ($msg->hasErrors()) {
	$msg->display();
}
?>

<div class="col-sm-12">

<!-- SIGN IN -->
<div class="col-sm-6" id="signin">
    <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> S'IDENTIFIER</h4>
    <hr class="blue">
    <div id="connexion">
        
     <!-- formulaire de login -->  
    <form class="form-horizontal" action="/connexion.php?referer=<?php echo urlencode($referer); ?>" role="form" data-toggle="validator" method="post">
        <input type="hidden" name="action" value="login" />
          
  <div class="form-group">
    <label class="control-label col-sm-3" for="sipseudo">Email : <span class="requis">*</span></label>
    <div class="col-sm-8">
      <input class="form-control" id="sipseudo" name="sipseudo" placeholder="Votre email" required>
    </div>
  </div>
	<div class="clear"></div>
        
  <div class="form-group">
    <label class="control-label col-sm-3" for="sipwd">Mot de passe : <span class="requis">*</span></label>
    <div class="col-sm-8"> 
      <input type="password" class="form-control" id="sipwd" name="sipwd" placeholder="Entrez votre mot de passe" required>
    </div>
  </div>
    <div class="clear"></div>
	
		
	
		
		
		
		
   <div class="mk_form_left">
      <div class="col-sm-offset-3 col-sm-4 checkbox checkbox-primary">
        <input id="checksouvenir" type="checkbox" value="oui" name="souvenir"> <label for="checksouvenir">Se souvenir de moi</label>
      </div>
    <div class="col-sm-4 checkbox checkbox-primary">
        <a href="/mot_de_passe_oublie.php">Mot de passe oublié</a>
      </div>
      </div>
        <div class="clear"></div>
        
  <div class="form-group">
      <label class="col-sm-3"></label>
    <div class="col-sm-8">
      <button type="submit" name="seconnecter" class="btn btn-register noMargin mk_form_right">Se connecter</button>
    </div>
  </div>
        
</form>
    <!-- fin du formulaire de login -->
        </div>
</div>
<!-- END SIGN IN -->

<!-- SIGN UP -->
<div class="col-sm-6" id="signup">
    <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> S'INSCRIRE</h4>
    <div id="introsignup" class="collapse <?php echo !isset($errorSignup) ? 'in' : ''; ?>">
        <hr class="blue">
    <p><i>Vous n’avez pas encore de compte utilisateur ? Inscrivez-vous rapidement et profitez des avantages suivants :</i>
    <ul>
        <li>Une plateforme innovante permettant de mandater une inspection technique de 60 minutes sur la voiture que vous souhaitez acheter (forfait ScanExpert)</li>
        <li>Un réseau de garages partenaires réparti dans toute la suisse romande.</li>
        <li>Profitez, grâce au forfait ScanExpert Protect, d'une sécurité supplémentaire avec la garantie mécanique (à venir).</li>
        <li>Insérez votre annonce et recevez gratuitement notre bannière auto (<a href="http://www.autospot.ch/banniere.php" target="_blank">en savoir +</a>). Pour les 250 premières insertions.</li>	
    </ul>
    </p>
    </div>
    <hr class="blue">
    
    <div class="col-sm-offset-4 col-sm-8">
    <button data-toggle="collapse" data-target="#introsignup, #inscription" class="btn btn-register noMargin">Inscription gratuite</button>
    </div>
    
    <div id="inscription" class="collapse <?php echo isset($errorSignup) ? 'in' : ''; ?>">
        <div class="clear"></div>
		
		
		
    <!-- formulaire d'inscription -->
    <form novalidate id="signupform" class="form-horizontal" role="form" data-toggle="validator" action="connexion.php?referer=<?php echo urlencode($referer); ?>" method="post">
        <input type="hidden" name="action" value="signup" />

        <?php if (isset($errorSignup)) : ?>
            <div class="form-group">
                <div class="alert alert-danger" role="alert">
                    <?php echo $errorSignup; ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label class="control-label col-sm-4" for="sunom">Type de compte :</label>
            <div class="col-sm-8">
              <!-- <input type="text" disabled="disabled" class="form-control" id="sutypecompte" name="sutypecompte" value="Particulier"> -->
							
							<select name="type_client" class="form-control" onchange="changeTypeInscription(this.value, 'Nom : ', 'Nom du garage : ');">
								<option value="0">Particulier</option>
								<option value="2">Professionnel</option>
								
							</select>
							
              <!-- <input type="checkbox" name="particulier" required value="0"> Je confirme être un particulier -->
            </div>
      </div>
        <div class="form-group">
      <label class="control-label col-sm-4" for="sunom"><span id="form-label-sunom">Nom : </span><span class="requis">*</span></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sunom" name="sunom" placeholder="Votre nom" required <?php echo isset($sunom) ? 'value="' . $sunom . '"' : ''; ?>>
      </div>
    </div>
        
    <div class="form-group" id="form-item-suprenom">
      <label class="control-label col-sm-4" for="suprenom">Prénom : <span class="requis">*</span></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="suprenom" name="suprenom" placeholder="Votre prénom" required <?php echo isset($suprenom) ? 'value="' . $suprenom . '"' : ''; ?>>
      </div>
    </div>   
        
    <div class="form-group">
      <label class="control-label col-sm-4" for="suemail">Email : <span class="requis">*</span></label>
      <div class="col-sm-8">
        <input type="email" class="form-control" id="suemail" name="suemail" placeholder="Votre email" required <?php echo isset($suemail) ? 'value="' . $suemail . '"' : ''; ?>>
      </div>
    </div>
        
        <div class="form-group">
      <label class="control-label col-xs-12 col-sm-4" for="adresse">Adresse & N° :  <span class="requis">*</span></label>
      <div class="col-xs-9 col-sm-6">
        <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Votre adresse" required <?php echo isset($adresse) ? 'value="' . $adresse . '"' : ''; ?>>
      </div>
            <div class="col-xs-3 col-sm-2">
        <input type="text" class="form-control" id="numero" name="numero" placeholder="N°" required <?php echo isset($numero) ? 'value="' . $numero . '"' : ''; ?>>
      </div>
    </div>
        
        <div class="form-group">
      <label class="control-label col-xs-12 col-sm-4" for="codePostal">Code postal & Ville :  <span class="requis">*</span></label>
      <div class="col-xs-3 col-sm-3">
        <input type="text" class="form-control" id="codePostal" name="codePostal" placeholder="Votre NPA" required <?php echo isset($codePostal) ? 'value="' . $codePostal . '"' : ''; ?>>
      </div>
            <div class="col-xs-9 col-sm-5">
        <input type="text" class="form-control" id="ville" name="ville" placeholder="Votre ville" required <?php echo isset($ville) ? 'value="' . $ville . '"' : ''; ?>>
      </div>
    </div>
        
    <div class="form-group">
      <label class="control-label col-sm-4" for="supwd">Mot de passe : <span class="requis">*</span></label>
      <div class="col-sm-8">          
        <input type="password" class="form-control" id="supwd" name="supwd" placeholder="Choisissez un mot de passe" required>
      </div>
    </div>
        
        <div class="form-group">
      <label class="control-label col-sm-4" for="confirmpwd">Confirmez mot de passe : <span class="requis">*</span></label>
      <div class="col-sm-8">          
        <input type="password" class="form-control" id="confirmpwd" name="confirmpwd" placeholder="Confirmez votre mot de passe" required>
      </div>
    </div>
	
		<div class="form-group">
		<label class="control-label col-sm-4"></label>
		<div class="col-sm-8"> 
		<div class="g-recaptcha" data-sitekey="6LfUhlUUAAAAADFtz6TSa-0HVxBQYmxATuHJLlNt"></div>
		</div>
		</div>
	
    <div class="clear"></div>
        
    <div class="form-group">        
      <div class="col-sm-offset-4 col-sm-8">
        <button type="submit" class="btn btn-register noMargin">S'inscrire</button>
      </div>
    </div>
  </form>
    <!-- fin du formulaire d'inscription -->
    </div>
</div>
<!-- END SIGN UP -->
     
</div>
    <div class="clear"></div>
    <hr class="blue">
    <div class="clear"></div>
   

<!-- END REGISTER -->
</div>
    
<?php include("footer.php"); ?>