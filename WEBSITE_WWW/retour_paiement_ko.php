<?php
include_once('db_connexion.php');
require_once('classes/demande.php');
require_once('classes/commande_rapport.php');

$type = array_key_exists('type', $_GET) && !empty($_GET['type']) ? trim($_GET['type']) : null;
$id = array_key_exists('id', $_GET) && intval($_GET['id']) != 0 ? intval($_GET['id']) : null;

if ($type == 'forfait' && $id !== null) {
    $sql = sprintf('SELECT * FROM demandes_exam WHERE id="%s";', mysqli_real_escape_string($connect1, $id));
    $query = mysqli_query($connect1, $sql);
    $demande = mysqli_fetch_object($query);

    if ($demande !== false) {
        if ($demande->payment_status == demande::PAYMENT_PROCESSING || $demande->payment_status == NULL) {
            $sql = sprintf(
                'UPDATE demandes_exam SET payment_status="%s" WHERE id = "%s"',
                mysqli_real_escape_string($connect1, demande::PAYMENT_REJECTED),
                mysqli_real_escape_string($connect1, $demande->id)
            );
            mysqli_query($connect1, $sql);   
        }

        header('Location: je-choisis-mon-forfait.php?id='.$demande->id_annonces_clients.'&step4=1');
        exit;
    }
} elseif ($type == 'rapport' && $id !== null) {
    $commande = commande_rapport::getById($id);

    if ($commande !== null) {
        if ($commande->paiement_status == commande_rapport::PAIEMENT_PROCESSING) {
            $sql = sprintf(
                'UPDATE commandes_rapports_inspection SET paiement_status="%s" WHERE id = "%s"',
                mysqli_real_escape_string($connect1, commande_rapport::PAIMENT_REJECTED),
                mysqli_real_escape_string($connect1, $commande->id)
            );
            mysqli_query($connect1, $sql);
        }

        header('Location: rapport_inspection.php?id='.$commande->id_demande_exam.'&step4=1');
        exit;
    }
}

header('Location: index.php');
exit;