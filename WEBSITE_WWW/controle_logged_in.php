<?php include("header.php"); ?>
<a id="haut"></a>

<!-- CONTAINER FORMULAIRE EN 5 étapes -->
<div class="container">
   
<div class="stepwizard col-sm-12">
    <div class="stepwizard-row setup-panel etapes-row">
      <div class="stepwizard-step">
        <p>Annonce</p>
          
          <a href="#step-1" type="button" class="btn btn-primary btn-circle etapeIcone">1</a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>
      <div class="stepwizard-step">
        <p>Coordonnées / Connexion</p>
          <a href="#step-2" type="button" class="btn btn-default btn-circle etapeIcone" disabled="disabled">2</a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>
      <div class="stepwizard-step">
        <p>Forfait</p>
          <a href="#step-3" type="button" class="btn btn-default btn-circle etapeIcone" disabled="disabled">3</a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>
        <div class="stepwizard-step">
        <p>Moyen de paiement</p>
            <a href="#step-4" type="button" class="btn btn-default btn-circle etapeIcone" disabled="disabled">4</a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>
        <div class="stepwizard-step">
        <p>Félicitations</p>
            <a href="#step-5" type="button" class="btn btn-default btn-circle etapeIcone" disabled="disabled">5</a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>
    </div>
  </div>
    <div class="clear"></div>
    <div id="redAlert" class="alert alert-danger">
        <span class="lettrine"><i class="fa fa-exclamation" aria-hidden="true"></i></span> <strong>Données manquantes :</strong> Veuillez remplir les champs marqués en rouges.
</div>
    
  <form role="form" name="controler" action="" method="post">
      
    <!-- ÉTAPE 1 : CONTRÔLEZ L'ANNONCE -->
    <div class="setup-content" id="step-1">
     
        <h3 class="blue">Contrôlez l'annonce</h3>
        
    <div class="col-sm-12">
        <div class="col-sm-6">
        
            <p class="larger">Félicitation, vous avez trouvé la voiture qui vous plaît.<br />
                Grâce à AutoSpot, vous pouvez connaître l'état réel de la ... que vous envisagez d'acheter.</p>
            <p class="larger">Pour poursuivre, merci de contrôler les données ci-après :</p>
            
            <h4>Données de base :</h4>
            <p>
            Prix :<br />
            Année :<br />
            Kilomètres :
            </p>
            <h4>Etat & garantie :</h4>
            <p>
            Catégorie :<br />
            Etat du véhicule :<br />
            Expertisé :<br />
            Catégorie :<br />
            Garantie :<br />
            </p>
            <h4>Motorisation:</h4>
            <p>
            Cylindrée :<br />
            Chevaux :<br />
            Transmission :<br />
            Roues motrices :<br />
            Nb de vitesse :<br />
            Carburant :
            </p>
            <h4>Carrosserie :</h4>
            <p>
            Type :<br />
            Couleur :<br />
            Nb de portes :<br />
            Nb de sièges :
            </p>
            <h4>Annonceur :</h4>
            <p>
            Type :<br />
            Lieu :
            </p>
        
        </div>
    
        <div class="col-sm-6">   
        <!--  Lightgallery -->
            <div class="carSlider">
                <div class="prixannonce">16'900.-</div>
                
                <ul id="imageGallery">
                    <li data-thumb="img/car_thumb_1.jpg" data-src="img/car_1.jpg"> 
                <img src="img/car_1.jpg" />
                <span class="imgNum">1 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_2.jpg" data-src="img/car_2.jpg"> 
                <img src="img/car_2.jpg" />
                <span class="imgNum">2 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_3.jpg" data-src="img/car_3.jpg"> 
                <img src="img/car_3.jpg" />
                <span class="imgNum">3 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_4.jpg" data-src="img/car_4.jpg"> 
                <img src="img/car_4.jpg" />
                <span class="imgNum">4 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_5.jpg" data-src="img/car_5.jpg"> 
                <img src="img/car_5.jpg" />
                <span class="imgNum">5 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_6.jpg" data-src="img/car_6.jpg"> 
                <img src="img/car_6.jpg" />
                <span class="imgNum">6 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_7.jpg" data-src="img/car_7.jpg"> 
                <img src="img/car_7.jpg" />
                <span class="imgNum">7 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_8.jpg" data-src="img/car_8.jpg"> 
                <img src="img/car_8.jpg" />
                <span class="imgNum">8 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_9.jpg" data-src="img/car_9.jpg"> 
                <img src="img/car_9.jpg" />
                <span class="imgNum">9 / 10</span>
                 </li>
                    <li data-thumb="img/car_thumb_10.jpg" data-src="img/car_10.jpg"> 
                <img src="img/car_10.jpg" />
                <span class="imgNum">10 / 10</span>
                 </li>
                </ul>
            </div>
                <!-- /Lightgallery -->
        </div>
    </div>
    
    <div class="col-sm-12">
        <hr class="blue">
        <div class="col-sm-6">
        <h4>Description rapide</h4>
        </div>
        
        <div class="col-sm-6">
        <h4>Description détaillée</h4>
        <p>
        Lorem ipsum dolor sit amel, consectetuer adipiscing elit. Aenean commodo ligula eget
        dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
        nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
        sem. Nulla consequat massa quis enim. Oonec pede justo, fringilla vel, aliquet nec,
        vulputate eget, arcu. ln enim justo, rhoncus ut, imperdiet a. venenatis vitae, justo. Nullam
        dictum felis eu pede mollis pretium.<br/>
        lnteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate
        eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam
        lorem ante. dapibus in, viverra quis, feugiat a, tenus. Phasellus viverra nulla ut metus
        varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue.
        Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus,
        tellus eget condimentum rhoncus, sem quam semper libero, sit amel adipiscing sem
        neque sed ipsum. Nam quam nunc, blandit vel, lucius pulvinar, hendrerit id, lorem.
        Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut Jibero venenatis
        faucibus. Nullam quis ante. Etiam sit amel orci eget eros faucibus tincidunt. Ouis Jeo. Sed
        fringilla mauris si! amel nibh. Donec sodales sagittis magna. Sed consequat, Jeo eget
        bibendum sodales, augue velit cursus nunc.
        </p>
        </div>
    </div>   
        
    <div class="col-sm-12">
        
        <!-- ACCORDEONS -->
        <div class="panel-group" id="accordion">
            
        <div class="panel panel-default">
        <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" data-target="#collEquipements" class="black collapsed">
                <h4 class="panel-title">Les équipements du véhicule
                    <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span></h4></a>
            </div>
            </div>
     
            <!-- EQUIPEMENTS --> 
    <div id="collEquipements" class="panel-collapse collapse">
        <!-- Sécurité -->
        <div class="form-group">
      <label class="control-label col-sm-2" for="securite">Sécurité :</label>
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="airbag" type="checkbox" value="oui" name="airbag"><label for="airbag">Airbag conducteur et passager</label><br/>
          <input id="airbagL" type="checkbox" value="oui" name="airbagL"><label for="airbagL">Airbag latéraux pour conducteur et passager</label><br/>
          <input id="isofix" type="checkbox" value="oui" name="isofix"><label for="isofix">Fixation isofix pour siège enfant</label><br/>
          <input id="feuxAv" type="checkbox" value="oui" name="feuxAv"><label for="feuxAv">Feux anti-brouillard avant</label><br/>
          <input id="feuxAr" type="checkbox" value="oui" name="feuxAr"><label for="feuxAr">Feux anti-brouillard arrière</label><br/>
          <input id="phareDir" type="checkbox" value="oui" name="phareDir"><label for="phareDir">Phares directionnels</label><br/>
          <input id="xenon" type="checkbox" value="oui" name="xenon"><label for="xenon">Éclairage Xénon</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="verrCent" type="checkbox" value="oui" name="verrCent"><label for="verrCent">Verrouillage centralisé</label><br/>
          <input id="verrCentDist" type="checkbox" value="oui" name="verrCentDist"><label for="verrCentDist">Verrouillage centralisé à distance</label><br/>
          <input id="x3feustop" type="checkbox" value="oui" name="x3feustop"><label for="x3feustop">3ème feu de stop</label><br/>
          <input id="ledRetro" type="checkbox" value="oui" name="ledRetro"><label for="ledRetro">Led clignotant placé dans les rétroviseurs extérieurs</label><br/>
          <input id="abs" type="checkbox" value="oui" name="abs"><label for="abs">ABS (système anti-blocage des roues)</label><br/>
          <input id="asr" type="checkbox" value="oui" name="asr"><label for="asr">ASR (anti-patinage des roues)</label><br/>
          <input id="esp" type="checkbox" value="oui" name="esp"><label for="esp">ESP (contrôle de stabilité)</label>
        </div>
    </div></div>
  
      <!-- Confort -->
      <div class="form-group">
      <label class="control-label col-sm-2" for="confort">Confort :</label>
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="dirAss" type="checkbox" value="oui" name="dirAss"><label for="dirAss">Direction assistée</label><br/>
          <input id="clim" type="checkbox" value="oui" name="clim"><label for="clim">Climatisation</label><br/>
          <input id="climAuto" type="checkbox" value="oui" name="climAuto"><label for="climAuto">Climatisation automatique</label><br/>
          <input id="vitreTeinte" type="checkbox" value="oui" name="vitreTeinte"><label for="vitreTeinte">Vitres teintées</label><br/>
          <input id="vitreElec" type="checkbox" value="oui" name="vitreElec"><label for="vitreElec">Vitres avant et arrière électriques</label><br/>
          <input id="volantCuir" type="checkbox" value="oui" name="volantCuir"><label for="volantCuir">Volant en cuir</label><br/>
          <input id="volantChauf" type="checkbox" value="oui" name="volantChauf"><label for="volantChauf">Volant chauffant</label><br/>
          <input id="siegeCuir" type="checkbox" value="oui" name="siegeCuir"><label for="siegeCuir">Sièges en cuir</label><br/>
          <input id="siegeChauf" type="checkbox" value="oui" name="siegeChauf"><label for="siegeChauf">Sièges avant chauffant</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="detecteurPluie" type="checkbox" value="oui" name="detecteurPluie"><label for="detecteurPluie">Détecteur de luminosité et de pluie</label><br/>
          <input id="essuieInter" type="checkbox" value="oui" name="essuieInter"><label for="essuieInter">Essuie-glaces avant avec intervalle</label><br/>
          <input id="regVitesse" type="checkbox" value="oui" name="regVitesse"><label for="regVitesse">Régulateur de vitesse</label><br/>
          <input id="startstop" type="checkbox" value="oui" name="startstop"><label for="startstop">Fonction Start/Stop</label><br/>
          <input id="retroElec" type="checkbox" value="oui" name="retroElec"><label for="retroElec">Rétroviseurs extérieurs à dégivrage et réglage électrique</label><br/>
          <input id="parcageAr" type="checkbox" value="oui" name="parcageAr"><label for="parcageAr">Capteur de distance de parcage arrière</label><br/>
          <input id="parcageAvAr" type="checkbox" value="oui" name="parcageAvAr"><label for="parcageAvAr">Capteur de distance de parcage avant et arrière</label><br/>
          <input id="cameraRecul" type="checkbox" value="oui" name="cameraRecul"><label for="cameraRecul">Caméra de recul</label><br/>
          <input id="barreToit" type="checkbox" value="oui" name="barreToit"><label for="barreToit">Barres de toit</label>
        </div>
    </div></div>

      <!-- Divertissements -->
      <div class="form-group">
      <label class="control-label col-sm-2" for="divertissements">Divertissements :</label>
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="ordinateur" type="checkbox" value="oui" name="ordinateur"><label for="ordinateur">Ordinateur de bord</label><br/>
          <input id="gps" type="checkbox" value="oui" name="gps"><label for="gps">Système de navigation GPS</label><br/>
          <input id="radio" type="checkbox" value="oui" name="radio"><label for="radio">Radio, CD</label><br/>
          <input id="usb" type="checkbox" value="oui" name="usb"><label for="usb">Connexion USB et AUX</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="volantMultiF" type="checkbox" value="oui" name="volantMultiF"><label for="volantMultiF">Volant multi-fonctions</label><br/>
          <input id="bluetooth" type="checkbox" value="oui" name="bluetooth"><label for="bluetooth">Interface Bluetooth et téléphone mobile</label><br/>
          <input id="toitPano" type="checkbox" value="oui" name="toitPano"><label for="toitPano">Toit panoramique</label><br/>
          <input id="toitOuvrant" type="checkbox" value="oui" name="toitOuvrant"><label for="toitOuvrant">Toit ouvrant</label>
        </div>
    </div></div>

      <!-- Équipements sport -->
      <div class="form-group">
      <label class="control-label col-sm-2" for="sport">Équipements sport :</label>
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="spoilerToit" type="checkbox" value="oui" name="spoilerToit"><label for="spoilerToit">Spoiler de toit</label><br/>
          <input id="jupes" type="checkbox" value="oui" name="jupes"><label for="jupes">Jupes latérales</label><br/>
          <input id="chassisSport" type="checkbox" value="oui" name="chassisSport"><label for="chassisSport">Châssis sportif</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="siegeSport" type="checkbox" value="oui" name="siegeSport"><label for="siegeSport">Sièges sport</label><br/>
          <input id="retroChrome" type="checkbox" value="oui" name="retroChrome"><label for="retroChrome">Rétrovieurs extérieurs chromés</label><br/>
          <input id="barreToitAlu" type="checkbox" value="oui" name="barreToitAlu"><label for="barreToitAlu">Barrres de toit en aluminium</label>
        </div>
    </div></div>

      <!-- Autres -->
      <div class="form-group">
      <label class="control-label col-sm-2" for="autres">Autres :</label>
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="roueSecours" type="checkbox" value="oui" name="roueSecours"><label for="roueSecours">Roue de secours</label><br/>
          <input id="x2PneusRoues" type="checkbox" value="oui" name="x2PneusRoues"><label for="x2PneusRoues">2 jeux de pneus/roues</label><br/>
          <input id="antiCrevaison" type="checkbox" value="oui" name="antiCrevaison"><label for="antiCrevaison">Kit anti-crevaison</label><br/>
          <input id="hardTop" type="checkbox" value="oui" name="hardTop"><label for="hardTop">Hard top</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="tapisCaoutchouc" type="checkbox" value="oui" name="tapisCaoutchouc"><label for="tapisCaoutchouc">Tapis en caoutchouc avant et arrière</label><br/>
          <input id="tapisRevetus" type="checkbox" value="oui" name="tapisRevetus"><label for="tapisRevetus">Tapis revêtus avant et arrière</label><br/>
          <input id="crochetAttelage" type="checkbox" value="oui" name="crochetAttelage"><label for="crochetAttelage">Crochet d'attelage</label><br/>
          <input id="grille" type="checkbox" value="oui" name="grille"><label for="grille">Grille de séparation</label>
        </div>
    </div></div>
    </div>
        <!-- /EQUIPEMENTS -->
            
            <div class="clear"></div>
            
            <!-- Informations complémentaires -->
            <div class="panel panel-default">
        <div class="panel-heading">
            <a data-toggle="collapse" data-parent="#accordion" data-target="#collInfoComp" class="black collapsed">
                <h4 class="panel-title">Informations complémentaires
                    <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span></h4></a>
            </div>
            </div>
            
      <div id="collInfoComp" class="panel-collapse collapse">
        <div class="form-group">
      <label class="control-label col-sm-2" for="complementaires">Informations complémentaires :</label>
            
        <div class="col-sm-3">
      <div class="checkbox checkbox-primary">
          <input id="importDirectPara" type="checkbox" value="oui" name="importDirectPara"><label for="importDirectPara">Importation directe/parallèle</label>
        </div>
    </div>
    <div class="col-sm-2">
      <div class="checkbox checkbox-primary">
          <input id="handicape" type="checkbox" value="oui" name="handicape"><label for="handicape">Pour handicapé</label>
        </div>
    </div>
    <div class="col-sm-2">
      <div class="checkbox checkbox-primary">
          <input id="tuning" type="checkbox" value="oui" name="tuning"><label for="tuning">Tuning</label>
        </div>
    </div>
    <div class="col-sm-2">
      <div class="checkbox checkbox-primary">
          <input id="voitureCourse" type="checkbox" value="oui" name="voitureCourse"><label for="voitureCourse">Voiture de course</label>
        </div>
    </div>
            <div class="col-sm-3">
      <div class="checkbox checkbox-primary">
          <input id="exportation" type="checkbox" value="oui" name="exportation"><label for="exportation">Pour l'exportation</label>
        </div>
    </div>
            <div class="col-sm-3">
      <div class="checkbox checkbox-primary">
          <input id="nonFumeur" type="checkbox" value="oui" name="nonFumeur"><label for="nonFumeur">Véhicule non fumeur</label>
        </div>
    </div>
        </div>
        </div>
        <!-- /Informations complémentaires -->
        
</div>
<!-- END ACCORDEONS -->
            
    </div>
        
        
        <div class="clear"></div>
        <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
        <div class="clear"></div>
    
      </div>
      <!-- FIN ÉTAPE 1 -->
      
     <!-- ÉTAPE 2 : CONTRÔLEZ VOS COORDONÉES --> 
    <div class="setup-content" id="step-2">
        
        <!-- 2A --> 
        <h3 class="blue">Contrôlez vos coordonnées</h3>
         <div class="col-md-12">
             <p class="larger">Pour poursuivre votre demande, il vous suffit de contrôler vos coordonnées ci-après :</p>
        <!-- CONTROL COORD FORM -->
        <form id="coordForm" class="form-horizontal" role="form" data-toggle="validator">
        
        <div class="form-group">
            <label class="control-label col-xs-6" for="coordNom">Nom : <span class="requis">*</span>
            <input type="text" class="form-control" id="coordNom" placeholder="Votre nom" required>
            </label>
            
            <label class="control-label col-xs-6" for="coordPrenom">Prénom : <span class="requis">*</span>
            <input type="text" class="form-control" id="coordPrenom" placeholder="Votre prénom" required>
            </label>
            
            <label class="control-label col-xs-9" for="coordAdresse">Adresse complète :  <span class="requis">*</span>
            <input type="text" class="form-control" id="coordAdresse" placeholder="Votre adresse" required>
            </label>
            
            <label class="control-label col-xs-3" for="coordNumero">N° :  <span class="requis">*</span>
            <input type="text" class="form-control" id="coordNumero" placeholder="N° de rue, voie etc." required>
            </label>
            
            <label class="control-label col-xs-3" for="coordCP">Code postal :  <span class="requis">*</span>
            <input type="text" class="form-control" id="coordCP" placeholder="Votre code postal" required>
            </label>
            
            <label class="control-label col-xs-9" for="coordVille">Ville :  <span class="requis">*</span>
            <input type="text" class="form-control" id="coordVille" placeholder="Votre ville" required>
            </label>
            
            <label class="control-label col-xs-12" for="coordTel">N° de téléphone :  <span class="requis">*</span>
            <input type="text" class="form-control" id="coordTel" placeholder="Votre n° de téléphone" required>
            </label>
            
            <label class="control-label col-xs-12" for="coordMail">Email :  <span class="requis">*</span>
            <input type="text" class="form-control" id="coordMail" placeholder="Votre email" required>
            </label>
          
    </div>
  </form>
        <!-- END CONTROL COORD FORM -->
        </div>
        
        <div class="clear"></div>
        <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
        <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
        <div class="clear"></div>
        <!-- FIN 2A --> 
    
      </div>
     <!-- FIN ÉTAPE 2 -->
      
     <!-- ÉTAPE 3 : CHOISISSEZ VOTRE FORFAIT -->
      <div class="setup-content" id="step-3">
          
        <h3 class="blue">Choisissez votre forfait</h3>
         <div class="col-md-12">
         <form id="forfaitForm" class="form-horizontal" role="form" data-toggle="validator">   
             <!-- VISIOEXPERT -->
             <div id="visio" class="col-md-6 carteForfait controlForfait">
<div class="carteHead visio">
<p>VisioExpert</p>
</div>
<div class="carteBody">
<p>Suivant un cahier des charges précis, chaque expert effectue <b class="blue">65</b> points de contrôles sur le véhicule, classés selon les 7 catégories suivantes :</p>
<p><b>1. Identification et vérification de la conformité du véhicule</b>
    <br/>L'expert vérifie que le N° de série apposé sur le châssis et le moteur correspondent bien à ce qui est inscrit sur le permis de circulation. Il contrôle également que le véhicule correspond bien à celui de l'annonce.</p>
<p><b>2. Vérification de la conformité des pièces administratives</b></p>
<p><b>3. Vérification de l'entretien du véhicule</b><br />
    L'expert examine en détail le carnet d'entretien et les factures afin de s'assurer que les préconisations de maintenance ont été réalisées.</p>
<p><b>4. Etat de la carrosserie selon photos</b><br />
    L'expert examine minutieusement la carrosserie du véhicule afin de détecter des traces de réparations antérieures, des différences de teintes ou la présence de corrosion.</p>
<p><b>5. Examen détaillé de la mécanique</b><br />
    L'expert examine le compartiment moteur dans son ensemble ainsi que les éléments tels que la batterie, le radiateur, les caoutchoucs, les durites ou les niveaux. Il contrôle l'absence de chocs, de déformations, de traces de corrosions, de fuites ainsi que l'absence de bruit ou vibration anormale.</p>
<p><b>6. Examen détaillé des organes de sécurité</b><br />
    L'expert teste les organes et équipements de sécurité tels que les pneumatiques, clignotants, freins, suspensions, rétroviseurs.</p>
<p>&nbsp;</p>   
<p>En complément à ces 65 points de contrôle, l'expert évaluera, le cas échéant, les frais de remise en état lors de dégâts apparents (exemple : carrosserie grêlée, bosselée ou rouillée, pare-brise fissuré).<br /> 
L'expert complète le rapport d'expertise par une photographie du flan avant gauche du véhicule, du flan avant droit, de l'arrière, de l'intérieur, du compteur kilométrique et, si cela est nécessaire, d'une photographie des éléments endommagés.<br />   
Il y rajoute une photographie des services présents dans le carnet d'entretien et termine son rapport d'expertise par une synthèse de l'état général et par ses remarques et conseils.</p>
</div>
<div class="carteFoot visio">
    <span class="radioForfait">
        <input id="forfaitVisio" type="radio" name="forfait" onclick="if(this.checked){visio()}" required>&nbsp;<label class="control-label" for="forfaitVisio">Choisir ce forfait</label> <span class="requis" required>*</span><br/>
    <span class="prixChoixForfait">PRIX</span>
</div>
</div>
            <!-- /VISIOEXPERT -->
             
             <!-- SCAN EXPERT -->
             <div id="scan" class="col-md-6 carteForfait controlForfait">
<div class="carteHead scan">
<p>ScanExpert</p>
</div>
<div class="carteBody">
<p class="bigger">Suivant un cahier des charges précis, chaque expert effectue <b class="blue">80</b> points de contrôles sur le véhicule.</p>
<p><b>Forfait VisioExpert</b> (<b class="blue">65</b> points de contrôle).</p>
<p><i class="fa fa-plus-circle fa-2x blue" style=" vertical-align: middle;"></i> Essai dynamique du véhicule</p>
<p><i class="fa fa-check blue"></i> Écoute des bruits: roulements, boîte de vitesse, échappement.</p>
<p><i class="fa fa-check blue"></i> Contrôle de la présence de fumées à l'échappement au ralenti et à l'accélération.</p>
<p><i class="fa fa-check blue"></i> Contrôle de la transmission manuelle : passage des rapports (murs, points durs, craquements).</p>
<p><i class="fa fa-check blue"></i> Contrôle de la transmission automatique : passage des rapports (glissement).</p>
<p><i class="fa fa-check blue"></i> Contrôle ressenti de l'efficacité de l'embrayage, des supensions, des freins, de la direction.</p>
<p><i class="fa fa-check blue"></i> Contrôle de le dérive à l'accélération.</p>
<p><i class="fa fa-check blue"></i> Contrôle du fonctionnement des instruments tels que le régulateur de vitesse, le compteur kilométrique.</p>
<p>Lors de l'insertion de l'annonce par le vendeur, celui-ci devra indiquer si son véhicule possède des plaques d'immatriculation.<br/>Si ce n'est pas le cas, un essai dynamique du véhicule par l'expert ne sera pas possible. Dans ce cas, seul le forfait de base VisioExpert vous sera disponible.</p>
</div>
<div class="carteFoot scan">
    <span class="radioForfait"><input id="forfaitScan" type="radio" name="forfait" onclick="if(this.checked){scan()}" required>&nbsp;<label class="control-label" for="forfaitScan">Choisir ce forfait</label> <span class="requis" required>*</span></span>
    <br/>
    <span class="prixChoixForfait">PRIX</span>
</div>
</div>
             <!-- /SCANEXPERT -->

<!-- Script pour mettre en arrière le forfait non choisi -->
<script>
function visio() {
document.getElementById("forfaitVisio").value = 
    $( "#visio" ).animate({
    opacity: 1,
  }, 500, function() {
    // Animation complete.
  });
    $( "#scan" ).animate({
    opacity: 0.5,
  }, 500, function() {
    // Animation complete.
  });
}
function scan() {
document.getElementById("forfaitScan").value = 
    $( "#scan" ).animate({
    opacity: 1,
  }, 500, function() {
    // Animation complete.
  });
    $( "#visio" ).animate({
    opacity: 0.5,
  }, 500, function() {
    // Animation complete.
  });
}
</script>
                 
         <div class="clear"></div>
             
             <div class="col-sm-12 checkbox checkbox-primary">
      <input id="presentControle" type="checkbox" value="oui" name="presentControle">
		<label for="presentControle" class="notReqBlack">Je souhaite être présent lors du contrôle technique</label>
          </div>
    <script type="text/javascript">
              $('#presentControle').change(function(){
   if(this.checked)
$(".showPresent").css("display", "");  // show
   else
      $(".showPresent").css("display", "none");  // hide
});
</script>
             <p class="larger blue showPresent" style="display:none;">Vous serez averti par email et dans votre espace client dès que la date du contrôle technique sera connue.</p>
                 </form>     
        </div>
      
      <div class="clear"></div>
        <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
        <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
        <div class="clear"></div>
          
      </div>
      <!-- FIN ÉTAPE 3 -->
    
      <!-- ÉTAPE 4 : CHOISISSEZ VOTRE MOYEN DE PAIEMENT -->
      <div class="setup-content" id="step-4">
          
      <h3 class="blue">Choisissez votre moyen de paiement</h3>
         <div class="col-md-12">
          <p class="larger">Veuillez choisir parmi les moyens de paiement suivants :</p>
             
             <label class="control-label col-sm-3" for="paiement"></label>
            <div class="col-sm-9 radio radio-primary">
            <input id="paiementPostfinance" type="radio" name="paiement" required><label for="paiementPostfinance">Paiement par Postfinance Card <span class="requis">*</span></label>
            <br/>
            <input id="paiementMastercard" type="radio" name="paiement" required><label for="paiementMastercard">Paiement par MasterCard <span class="requis">*</span></label>
            </div>
             
        </div>
      
      <div class="clear"></div>
        <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
        <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
        <div class="clear"></div>
          
      </div>
     <!-- FIN ÉTAPE 4 --> 

      <!-- ÉTAPE 5 : FÉLICITATIONS -->
      <div class="setup-content" id="step-5">
          
      <h3 class="blue">Félicitations, votre demande d'expertise N°... a bien été prise en compte</h3>
         <div class="col-md-12">
          <p class="larger">Merci pour votre demande d'expertise sur la ...</p>
<p class="larger">Notre expert automobile partenaire contactera le plus rapidement possible le vendeur afin de fixer un rendez-vous pour le contrôle technique.</p>
<p class="larger">Si vous avez choisis d'être présent lors du contrôle technique, vous serez informé de la date, de l'heure et du lieu de rendez-vous par e-mail et dans votre espace utilisateur.</p>
<p></p>
<p></p>
<p></p>
<p></p>
<p class="larger">Si le véhicule désiré est déjà vendu, vous seriez aussitôt averti par e-mail et dans votre espace utilisateur.<br />
Dans ce cas, votre demande d'expertise serait annulée et nous vous rembourserions intégralement, sans aucun frais.</p>   
        </div>
      
      <div class="clear"></div>
          <a href="/index.php?r=voiture&marque=Toutes&modele=Tous" role="button" class="btn btn-primary btn-lg pull-left toTop white">Retourner à la page des annonces</a>
        <div class="clear"></div>
          
      </div>
     <!-- FIN ÉTAPE 5 :  -->
    
    </form>
  

<!-- FIN CONTAINER FORMULAIRE EN 5 étapes -->



<?php include("index_footer.php"); ?>