<?php
session_start();
require_once('vendor/autoload.php');
require_once('classes/annonce.php');
require_once('classes/client.php');
require_once('classes/demande.php');
require_once('classes/commande_rapport.php');

$id_demande = array_key_exists('id', $_GET) && intval($_GET['id']) > 0 ? intval($_GET['id']) : null;
$id_commande = array_key_exists('id_commande', $_GET) && intval($_GET['id_commande']) > 0 ? intval($_GET['id_commande']) : null;

if ($id_demande === null) {
    header('Location: index.php');
    exit;
}

$demande_exam = demande::getById($id_demande);
$annonce = annonce::getById($demande_exam->id_annonces_clients);

if ($demande_exam === null || $annonce === false) {
    header('Location: index.php');
    exit;
}

$client = client::getCurrent();

$canDownloadReport = false;
if ($client !== null) {
    if ($id_commande !== null) {
        $commande = commande_rapport::getById($id_commande);
    } else {
        $commande = commande_rapport::getByDemandeAndClient($demande_exam->id, $client->id, commande_rapport::PAIEMENT_ACCEPTED);
    }

    if (is_object($commande) && $commande->paiement_status == commande_rapport::PAIEMENT_ACCEPTED && $commande->id_client == $client->id) {
        $canDownloadReport = true;
    }
}

$today = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
$date_demande = \DateTime::createFromFormat('Y-m-d H:i:s', $demande_exam->demande_ts, new \DateTimeZone('Europe/Paris'));

$date_controle = demande::getCompletedDate($demande_exam);
$date_rapport = annonce::getReportAvailabilityDate($demande_exam);

if ($date_rapport === false) {
    header('Location: index.php');
    exit;
}

$date_nouvelle_inspection = annonce::getNextExamAvailabilityDate($date_controle);

$available = true;
if ($date_rapport > $today) {
    $available = false;
    $countDown = annonce::in($date_rapport);
}

$step1style = ' style="display: none;"';
$step2style = ' style="display: none;"';
$step3style = ' style="display: none;"';
$step4style = ' style="display: none;"';
$step5style = ' style="display: none;"';

if ($available && isset($_GET['step2'])) {
    if (client::isLoggedIn()) {
        $step = 3;
        $step3style = ' style="display: block;"';
    } else {
        $step = 2;
        $step2style = ' style="display: block;"';
    }
} elseif ($available && isset($_GET['step3'])) {
    $step = 3;
    $step3style = ' style="display: block;"';
} elseif ($available && isset($_GET['step4'])) {
    $step = 4;
    $step4style = ' style="display: block;"';
} elseif ($canDownloadReport) {
    $step = 5;
    $step5style = ' style="display: block;"';
} else {
    $step = 1;
    $step1style = ' style="display: block;"';
}

$msg = new Plasticbrain\FlashMessages\FlashMessages();

include('header.php');
include('body.php');
?>

<!-- CONTAINER FORMULAIRE EN 5 étapes -->
<div class="container">
    <div class="stepwizard col-sm-12">
        <div class="stepwizard-row setup-panel etapes-row ignore">
            <div class="stepwizard-step">
                <p>Rapport d'inspection</p>
                <a href="#step-1" type="button" class="btn btn-primary btn-circle etapeIcone">1</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Connexion / inscription</p>
                <a href="#step-2" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 2 ? ' disabled="disabled"' : ''; ?>>2</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Confirmation</p>
                <a href="#step-3" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 3 ? ' disabled="disabled"' : ''; ?>>3</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Moyen de paiement</p>
                <a href="#step-4" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 4 ? ' disabled="disabled"' : ''; ?>>4</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Félicitations</p>
                <a href="#step-5" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 5 ? ' disabled="disabled"' : ''; ?>>5</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <!-- ÉTAPE 1 : CONTRÔLEZ L'ANNONCE -->
    <div class="setup-content" id="step-1"<?php echo $step1style; ?>>
        <h3 class="blue">Rapport d'inspection</h3>
        <div class="col-sm-12">
            <p class="larger">Félicitations, vous avez trouvé la voiture qui vous plaît.</p>
            <p class="larger">Grâce à AutoSpot, vous pouvez connaître l'état réel de la <?php echo "$annonce->marque $annonce->modele $annonce->version"; ?> que vous envisagez d'acheter.</p>
            <p class="larger">Un utilisateur a effectué le <?php echo $date_demande->format('d/m/Y'); ?> à <?php echo $date_demande->format('H:i'); ?> une demande d'inspection technique sur cette <?php echo "$annonce->marque $annonce->modele $annonce->version"; ?>.</p>
            <p class="larger">Le rapport d'inspection technique N° (afficher le numéro du rapport) a été réalisé le <?php echo $date_controle->format('d/m/Y'); ?> à <?php echo $date_controle->format('H:i'); ?>.</p>
            <p class="larger">
                Si vous souhaitez prendre connaissance du rapport d'inspection technique, vous pouvez l'obtenir en l'achetant au prix de frs 49.-
                <?php if (!$available) : ?>
                    dès le <?php echo $date_rapport->format('d/m/Y'); ?> à <?php echo $date_rapport->format('H:i'); ?>
                <?php else : ?>
                    en cliquant sur le bouton continuer.
                    <p class="larger">Dès l'achat de ce rapport d'inspection technique, celui-ci ne sera disponible à nouveau à la vente que dans 15 jours.</p>
                <?php endif; ?>
            </p>
            <p class="larger">Si ce véhicule n'est pas vendu dans l'intervalle, la prochaine demande d'inspection technique ne sera possible qu'à partir du <?php echo $date_nouvelle_inspection->format('d/m/Y'); ?>.</p>
            
            <div class="clear"></div>
            <?php if ($available) : ?>
                <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button"<?php echo (client::isLoggedIn() ? ' data-step="2"' : ''); ?>>Continuer</button>
            <?php endif; ?>
        </div>
    </div>

    <!-- ÉTAPE 2 : CONTRÔLEZ VOS COORDONNÉES -->
    <div class="setup-content" id="step-2"<?php echo $step2style; ?>>
        <?php if ($client !== null) : ?>
            <!-- 2A -->
            <h3 class="blue">Connexion / inscription</h3>
            <div class="col-md-12">
                <p class="larger">Pour poursuivre votre demande, il vous suffit de contrôler vos coordonnées ci-après :</p>
                <!-- CONTROL COORD FORM -->
                <form id="coordForm" class="form-horizontal" role="form" data-toggle="validator">
                    <div class="form-group">
                        <label class="control-label col-xs-6" for="coordNom">Nom : <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordNom" placeholder="Votre nom" value="<?PHP echo $client->nom; ?>" required>
                        </label>

                        <label class="control-label col-xs-6" for="coordPrenom">Prénom : <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordPrenom" placeholder="Votre prénom" value="<?PHP echo $client->prenom; ?>" required>
                        </label>

                        <label class="control-label col-xs-9" for="coordAdresse">Adresse complète :  <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordAdresse" placeholder="Votre adresse" value="<?PHP echo $client->adresse; ?>" required>
                        </label>

                        <label class="control-label col-xs-3" for="coordNumero">N° :  <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordNumero" placeholder="N° de rue, voie etc." value="<?PHP echo $client->adresse_num; ?>" required>
                        </label>

                        <label class="control-label col-xs-3" for="coordCP">Code postal :  <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordCP" placeholder="Votre code postal" value="<?PHP echo $client->code_postal; ?>" required>
                        </label>

                        <label class="control-label col-xs-9" for="coordVille">Ville :  <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordVille" placeholder="Votre ville" value="<?PHP echo $client->ville; ?>" required>
                        </label>

                        <label class="control-label col-xs-12" for="coordTel">N° de téléphone :  <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordTel" placeholder="Votre n° de téléphone" value="<?PHP echo $client->telephone; ?>" required>
                        </label>

                        <label class="control-label col-xs-12" for="coordMail">Email :  <span class="requis">*</span>
                            <input type="text" class="form-control" id="coordMail" placeholder="Votre email" value="<?PHP echo $client->email; ?>" required>
                        </label>

                    </div>
                </form>
                <!-- END CONTROL COORD FORM -->
            </div>
        <?php else : ?>
            <!-- 2B  -->
            <h3 class="blue">Connectez-vous à votre compte ou créez-en un gratuitement</h3>
            <div class="col-md-12">
                <p class="larger">Pour poursuivre votre demande, il vous suffit de vous connecter à votre compte utilisateur ou, si vous n'en possedez pas encore, d'en créer un gratuitement :</p>
                <!-- SIGN IN -->
                <div class="col-sm-6" id="signin">
                    <?php
                    if ($msg->hasErrors()) {
                        $msg->display();
                    }
                    ?>
                    <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> S'IDENTIFIER</h4>
                    <hr class="blue">
                    <div id="connexion">

                        <!-- formulaire de login -->
                        <form class="form-horizontal" role="form" data-toggle="validator" action="connexion.php?referer=<?php echo urlencode('rapport_inspection.php?id='.$id_demande.'&step2'); ?>" method="post">
                            <input type="hidden" name="action" value="login" />
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="sipseudo">Email : <span class="requis">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="sipseudo" name="sipseudo" placeholder="Votre email" required>
                                </div>
                            </div>
                            <div class="clear"></div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="sipwd">Mot de passe : <span class="requis">*</span></label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="sipwd" name="sipwd" placeholder="Entrez votre mot de passe" required>
                                </div>
                            </div>
                            <div class="clear"></div>

                            <div class="col-sm-offset-3 col-sm-4 checkbox checkbox-primary">
                                <input id="checksouvenir" type="checkbox" value="oui" name="souvenir"> <label for="checksouvenir">Se souvenir de moi</label>
                            </div>
                            <div class="col-sm-4 checkbox checkbox-primary">
                                <a href="/mot_de_passe_oublie.php">Mot de passe oublié</a>
                            </div>
                            <div class="clear"></div>

                            <div class="form-group">
                                <label class="col-sm-3"></label>
                                <div class="col-sm-8">
                                    <button type="submit" name="seconnecter" class="btn btn-register noMargin mk_form_right1">Se connecter</button>
                                </div>
                            </div>

                        </form>
                        <!-- fin du formulaire de login -->
                    </div>
                </div>
                <!-- END SIGN IN -->

                <!-- SIGN UP -->
                <div class="col-sm-6" id="signup">
                    <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> S'INSCRIRE</h4>
                    <div id="introsignup" class="collapse in">
                        <hr class="blue">
                        <p><i>Vous n’avez pas encore de compte utilisateur ? Inscrivez-vous rapidement et profitez des avantages suivants :</i>
                        <ul>
                            <li>Une plateforme innovante permettant de mandater une inspection technique de 60 minutes sur la voiture que vous souhaitez acheter (forfait ScanExpert)</li>
                            <li>Un réseau de garages partenaires réparti dans toute la suisse romande.</li>
                            <li>Profitez, grâce au forfait ScanExpert Protect, d'une sécurité supplémentaire avec la garantie mécanique (à venir).</li>
                            <li>Insérez votre annonce et recevez gratuitement notre bannière auto (en savoir +). Pour les 250 premières insertions.</li>
                        </ul>
                        </p>
                    </div>
                    <hr class="blue">

                    <div class="col-sm-offset-4 col-sm-8">
                        <button data-toggle="collapse" data-target="#introsignup, #inscription" class="btn btn-register noMargin">Inscription gratuite</button>
                    </div>

                    <div id="inscription" class="collapse">
                        <div class="clear"></div>
                        <?php
                        if ($msg->hasErrors()) {
                            $msg->display();
                        }
                        ?>
                        <!-- formulaire d'inscription -->
                        <form id="signupform" class="form-horizontal" role="form" data-toggle="validator" action="connexion.php?referer=<?php echo urlencode('rapport_inspection.php?id='.$id_demande.'&id_commande='.$id_commande.'&step3'); ?>" method="post">
                            <input type="hidden" name="action" value="signup" />
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="sunom">Nom : <span class="requis">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="sunom" name="sunom" placeholder="Votre nom" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="suprenom">Prénom : <span class="requis">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="suprenom" name="suprenom" placeholder="Votre prénom" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="suemail">Email : <span class="requis">*</span></label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" id="suemail" name="suemail" placeholder="Votre email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="adresse">Adresse & N° :  <span class="requis">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Votre adresse" required>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="numero" name="numero" placeholder="N°" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="codePostal">Code postal & Ville :  <span class="requis">*</span></label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="codePostal" name="codePostal" placeholder="Votre NPA" required>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="ville" name="ville" placeholder="Votre ville" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="supwd">Mot de passe : <span class="requis">*</span></label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="supwd" name="supwd" placeholder="Choisissez un mot de passe" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="confirmpwd">Confirmez mot de passe : <span class="requis">*</span></label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="confirmpwd" name="confirmpwd" placeholder="Confirmez votre mot de passe" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="avatar">Choisir un avatar :</label>
                                <div class="col-sm-8">
                                    Plugin à insérer (je laisse le choix le plus judicieux à faire à Guillaume exemple ici : <a href="http://plugins.krajee.com/file-avatar-upload-demo">http://plugins.krajee.com/file-avatar-upload-demo</a>)
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-register noMargin">S'inscrire</button>
                                </div>
                            </div>
                        </form>
                        <!-- fin du formulaire d'inscription -->
                    </div>
                </div>
                <!-- END SIGN UP -->
            </div>
        <?php endif;?>
        <div class="clear"></div>
        <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
        <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
        <div class="clear"></div>
        <!-- FIN 2B  -->    
    </div>

    <!-- ÉTAPE 3 :  -->
    <div class="setup-content" id="step-3"<?php echo $step3style; ?>>
        <h3 class="blue">Confirmation</h3>
        <div class="col-sm-12">
            <?php if (isset($client)) : ?>
                <?php if ($msg->hasMessages()) : ?>
                    <?php $msg->display(); ?>
                    <script>
                        window.setTimeout(function() {
                            window.location.href = '/annonce.php?id=<?php echo $annonce->id; ?>';
                        }, 5000);
                    </script>
                <?php endif; ?>
                <p class="larger">Avant de procéder à l'achat de ce rapport d'inspection technique, veuillez vous assurer, auprès du vendeur, que ce véhicule est encore en vente.</p>
                <p class="larger">Pour ce faire, veuillez cliquer sur le bouton envoyer pour que le vendeur reçoive un email lui demandant si sont véhicule est encore en vente. Vous recevrez sa réponse dans votre boite mail à l'adresse <?php echo $client->email; ?>.</p>
                <p class="larger">
                    <form method="post" action="send_availability_email.php?referer=<?php echo urlencode('rapport_inspection.php?id='.$id_demande.'&id_commande='.$id_commande.'&step3'); ?>">
                        <input type="hidden" name="id_annonce" value="<?php echo $annonce->id; ?>" />
                        <input type="hidden" name="id_client" value="<?php echo $client->id; ?>" />
                        <button type="submit" class="btn btn-primary">Envoyer</button>
                    </form>
                </p>
                <div class="clear"></div>
                <p class="larger">Si vous vous êtes assuré que le véhicule est encore en vente, veuillez cliquer sur Continuer.</p>
                <div class="clear"></div>
                <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
            <?php else : ?>
                <p class="larger">Veuillez vous connecter à l'étape précédente.</p>
                <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
            <?php endif; ?>
            <div class="clear"></div>
        </div>
    </div>

    <!-- ÉTAPE 4 : MOYEN DE PAIEMENT -->
    <div class="setup-content" id="step-4"<?php echo $step4style; ?>>
        <h3 class="blue">Moyen de paiement</h3>
        <div class="col-sm-12">
            <?php if (isset($client)) : ?>
                <?php if (isset($_GET['step4'])): ?>
                    <div class="col-md-12">
                        <p class="larger text-warning">Une erreur est survenue lors du paiement.</p>
                        <p class="larger text-warning">Veuillez réessayer en vérifiant vos informations de votre carte bancaire.</p>
                    </div>
                <?php endif; ?>
                <div class="col-md-12">
                    <p class="larger">En cliquant sur continuer vous serez redirigé vers la plateforme de paiement sécurisé klikandpay.</p>
                    <p class="larger">Vous effectuerez un paiement de CHF 49.- via MasterCard, Visa, Postfinance ou Postfinance Card.</p>
                </div>

                <div class="clear"></div>
                <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>

                <form method="post" id="prepayment" action="/rapport_inspection_paiement.php">
                    <input type="hidden" name="id_client" value="<?php echo $_SESSION['id_client']; ?>" />
                    <input type="hidden" name="id_demande" value="<?php echo $demande_exam->id; ?>" />
                    <button class="btn btn-primary btn-lg pull-right toTop" onclick="this.form.submit();">Continuer</button>
                </form>
            <?php else : ?>
                <p class="larger">Veuillez vous connecter à l'étape précédente.</p>
                <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
            <?php endif; ?>

            <div class="clear"></div>
        </div>
    </div>

    <!-- ÉTAPE 5 : FELICITATIONS -->
    <div class="setup-content" id="step-5"<?php echo $step5style; ?>>
        <h3 class="blue">Félicitations</h3>
        <div class="col-sm-12">
            <?php if ($canDownloadReport) : ?>
                <p class="larger">Félicitations, le rapport d'inspection technique vous a été envoyé dans votre boite mail à l'adresse <?php echo $client->email; ?>.</p>
                <p class="larger">Vous pouvez également le télécharger en cliquant sur le bouton Télécharger le rapport d'inspection technique.</p>
                <p class="larger"></p>
                <p class="larger text-center">
                    <button class="btn btn-primary">Télécharger le rapport d'inspection technique</button>
                </p>
            <?php else : ?>
                <p class="larger">Vous n'avez pas le droit d'afficher cette page.</p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>