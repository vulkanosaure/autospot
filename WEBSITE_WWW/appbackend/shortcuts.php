<?php


$library_shortcut = array(

	"type_radio" => array(
		"type" => "radio"
	),
	
	"type_label" => array(
		"type" => "label"
	),
	
	"type_text" => array(
		"type" => "text"
	),
	
	"type_number" => array(
		"type" => "number"
	),
	
	"type_cb" => array(
		"type" => "checkbox"
	),
	
	
	"dysf" => array(
		"items_tpl" => "type_radio",
		"items" => array("ec04u08f", "ec04unna")
	),
	"dysf2" => array(
		"type" => "checkbox",
		"items_tpl" => "type_radio",
		"items" => array("ec04htvz", "ec04unna")
	),
	"dysf3" => array(
		"type" => "radio",
		"items_tpl" => "type_radio",
		"items" => array("ec04htvz", "ec04unna")
	),

	
	
	"dysf_cligno" => array(
		"items_tpl" => "type_radio",
		"items" => array("ec04u08f", "ec04f2ne", "ec04ye96", "ec04m0ac")
	),
	
	"dysf_light2" => array(
		"items_tpl" => "type_radio",
		"items" => array("ec04u08f", "ec04wkg1", "ec04svpj")
	),
	
	
	"no_close" => array(
		"type" => "checkbox",
		"items_tpl" => "type_radio",
		"items" => array("ec07qstr", "ec04unna", "ec1789z1")
	),
	
	
	
	"precise" => array(
		"items" => array(
			array(
				"text" => "examglobal_precise",
				"type" => "text"
			)
		)
	),
	
	"precise_pic" => array(
		"items" => array(
			array(
				"text" => "examglobal_precise",
				"type" => "text"
			),
			array(
				"type" => "picture",
				"text" => "ec05ej78"
			)
		)
	),
	
	
	"cb_precise" => array(
		"type" => "checkbox",
		"items" => array(
			array(
				"type" => "text",
				"text" => "examglobal_precise"
			)
		)
	),
	
	"cb_precise_pic" => array(
		"type" => "checkbox",
		"items" => array(
			array(
				"type" => "text",
				"text" => "examglobal_precise"
			),
			array(
				"type" => "picture",
				"text" => "ec05ej78"
			)
		)
	),
	
	"type_cb_pic" => array(
		"type" => "checkbox",
		"items" => array(
			array(
				"type" => "picture",
				"text" => "ec05ej78"
			)
		)
	),

	"choose_side" => array(
		"items_tpl" => array(
			"type" => "checkbox",
			"items_tpl" => "type_radio",
			"items" => array("ec042lzo", "ec04zmyn")
		),
		"items" => array("ec04at15", "ec04ojb7", "ec04o1uf", "ec04xzyb")
	),

	"type_cb_fuite" => array(
		"type" => "checkbox",
		"items_tpl" => "type_radio",
		"items" => array("ec042lzo", "ec04zmyn")
			
	),

	"yesno_question" => array(
		"type" => "label",
		"items_tpl" => "type_radio",
		"items" => array("examglobal_yes", "examglobal_no")
	),

	
	"cb_precise_optional" => array(
		"type" => "checkbox",
		"items" => array(
			array(
				"type" => "text",
				"text" => "ec07ufcp",
				"req" => false
			)
		)
	),
	
	"cb_precise_optional_pic" => array(
		"type" => "checkbox",
		"items" => array(
			array(
				"type" => "text",
				"text" => "ec07ufcp",
				"req" => false
			),
			array(
				"type" => "picture",
				"text" => "ec05ej78"
			)
		)
	),
	
	
	"optique_nopic" => array(
		"type" => "checkbox",
		"items_tpl" => "type_cb",
		"items" => array("ec05fiaw", "ec06kang", "ec12wyy8", "ec12dg3u", "ec126prl", "ec12vjd0", array(
			"type" => "picture",
			"text" => "ec05ej78"
		))
	)
	

);


?>