<?php


require_once("include/email_utils.php");
require_once("events_utils.php");
require_once("include/facture_generation.php");
require_once("admin/include/debug.php");




function insert_new_demande($dbh, $ts, $id_vendeur, $id_client, $id_announce, $present, $type_forfait='ScanExpert', $data_exam='')
{
	trace("insert_new_demande(".$ts.", ".$id_vendeur.", ".$id_client.", ".$id_announce.")");
	
	$id = insertDemande($dbh, $ts, $id_vendeur, $id_client, $id_announce, $present,$type_forfait, $data_exam);
	handleDemandeAssociation($dbh, $id, 'init');
	return $id;
}


function new_demande($dbh, $id)
{
	trace("new_demande(".$id.")");
	
	handleEmails($dbh, "client_new_demande", "client", $id, "global");
	handleEmails($dbh, "garage_new_demande", "garage", $id, "new");
	handleEmails($dbh, "admin_new_demande", "admin", $id);
	
	registerEventDemande($dbh, $id, time(), "new_demande");
	
}



//0 : global, 1 : 1ere fois, 2 : 2eme fois
function garage_set_time($dbh, $_index, $_iddemande, $_ts)
{
	trace("evt::garage_set_time(".$_index.", ".$_iddemande.", ".$_ts.")");
	// $_ts -= 2 * 3600;
	
	
	//global
	if($_index == 0){
		
	}
	//specific
	else{
		setTimeDemande($dbh, $_iddemande, $_index, $_ts);
		setRelanceDemande($dbh, $_iddemande, 0);
		
		registerEventDemande($dbh, $_iddemande, time(), "garage_set_time".$_index, array("ts" => $_ts));
		
		if($_index == 1){
			handleEmails($dbh, "client_gar_set_time1", "client", $_iddemande);
			handleEmails($dbh, "seller_gar_set_time1", "seller", $_iddemande, "confirm");
			handleEmails($dbh, "admin_gar_set_time1", "admin", $_iddemande);
		}
		else{
			handleEmails($dbh, "client_gar_set_time2", "client", $_iddemande);
			handleEmails($dbh, "seller_gar_set_time2", "seller", $_iddemande, "confirm");
			handleEmails($dbh, "admin_gar_set_time2", "admin", $_iddemande);
		}
	}
	
}



function garage_unavailable($dbh, $_iddemande)
{
	trace("evt::garage_unavailable()");
	setStatusDemande($dbh, $_iddemande, "refused");
	
	handleEmails($dbh, "client_gar_unavailable", "client", $_iddemande, "canceled2");
	handleEmails($dbh, "admin_gar_unavailable", "admin", $_iddemande);
	
	registerEventDemande($dbh, $_iddemande, time(), "garage_unavailable");
}



function garage_refuse_demande($dbh, $_iddemande, $_idgarage)
{
	trace("garage_refuse_demande(".$_iddemande.")");
	
	registerEventDemande($dbh, $_iddemande, time(), "garage_refuse_demande", array("id_garage" => $_idgarage));
	setAcceptDemande($dbh, $_iddemande, 0);
	
	addRefusDemande($dbh, $_iddemande, 1);
	handleDemandeAssociation($dbh, $_iddemande, 'refuse');
	
}

function garage_accept_demande($dbh, $_iddemande, $_idgarage)
{
	trace("garage_accept_demande(".$_iddemande.")");
	setAcceptDemande($dbh, $_iddemande, 1);
	
	handleEmails($dbh, "garage_gar_accept_demande", "garage", $_iddemande, "gar_accept_demande");
	handleEmails($dbh, "seller_gar_accept_demande", "seller", $_iddemande, "global");
	handleEmails($dbh, "admin_gar_accept_demande", "admin", $_iddemande);
	
	registerEventDemande($dbh, $_iddemande, time(), "garage_accept_demande", array("id_garage" => $_idgarage));
}

function rappel_rdv($dbh, $_iddemande)
{
	trace("rappel_rdv()");
	handleEmails($dbh, "seller_rappel_rdv", "seller", $_iddemande, "rappel");
	registerEventDemande($dbh, $_iddemande, time(), "rappel_rdv");
}

function seller_iddle_relance($dbh, $_iddemande)
{
	trace("seller_iddle_relance(".$_iddemande.")");
	handleEmails($dbh, "seller_seller_iddle_relance", "seller", $_iddemande, "global");
	
	registerEventDemande($dbh, $_iddemande, time(), "seller_iddle_relance");
}

function seller_iddle_complete($dbh, $_iddemande)
{
	trace("seller_iddle_complete(".$_iddemande.")");
	setStatusDemande($dbh, $_iddemande, "seller_iddle");
	
	handleEmails($dbh, "client_seller_iddle_complete", "client", $_iddemande, "canceled2");
	handleEmails($dbh, "seller_seller_iddle_complete", "seller", $_iddemande, "canceled");
	handleEmails($dbh, "garage_seller_iddle_complete", "garage", $_iddemande, "canceled");
	handleEmails($dbh, "admin_seller_iddle_complete", "admin", $_iddemande, "canceled3");
	
	
	registerEventDemande($dbh, $_iddemande, time(), "seller_iddle_complete");
}

function garage_declare_absent($dbh, $_iddemande)
{
	trace("evt::garage_declare_absent(".$_iddemande.")");
	setStatusDemande($dbh, $_iddemande, "seller_absent");
	
	handleEmails($dbh, "client_gar_declare_absent", "client", $_iddemande, "canceled2");
	
	handleEmails($dbh, "admin_gar_declare_absent", "admin", $_iddemande, "canceled3");
	handleEmails($dbh, "garage_gar_declare_absent", "garage", $_iddemande, "canceled");
	handleEmails($dbh, "seller_gar_declare_absent", "seller", $_iddemande, "canceled");
	
	registerEventDemande($dbh, $_iddemande, time(), "garage_declare_absent");
}

function garage_declare_sold($dbh, $_iddemande)
{
	trace("evt::garage_declare_sold(".$_iddemande.")");
	setStatusDemande($dbh, $_iddemande, "vehicule_sold");
	
	handleEmails($dbh, "client_gar_declare_sold", "client", $_iddemande, "canceled2");
	handleEmails($dbh, "admin_gar_declare_sold", "admin", $_iddemande, "canceled3");
	handleEmails($dbh, "garage_gar_declare_sold", "garage", $_iddemande, "canceled");
	
	deleteAnnonceByDemande($dbh, $_iddemande);
	
	registerEventDemande($dbh, $_iddemande, time(), "garage_declare_sold");
}



//garage clique depuis app
function garage_declare_error($dbh, $_iddemande)
{
	setStatusDemande($dbh, $_iddemande, "garage_error");
	
	handleEmails($dbh, "client_gar_declare_error", "client", $_iddemande, "global");
	handleEmails($dbh, "garage_gar_declare_error", "garage", $_iddemande, "canceled");
	handleEmails($dbh, "admin_gar_declare_error", "admin", $_iddemande, "canceled3");
	
	registerEventDemande($dbh, $_iddemande, time(), "garage_declare_error");
}


function is_status_canceled($dbh, $_iddemande)
{
	$status_canceled = ['refused', 'seller_iddle', 'seller_absent', 'vehicule_sold', 'garage_error'];
	
	$sql = "SELECT * FROM demandes_exam WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	$status = $output['status'];
	return (in_array($status, $status_canceled));
}



function delete_announce($dbh, $_iddemande)
{
	trace("delete_announce()");
	
	//condition
	if(!isDemandeComplete($dbh, $_iddemande)){
		
		//pas si emails ont déja été envoyés
		if(!is_status_canceled($dbh, $_iddemande)){
		
			handleEmails($dbh, "client_delete_announce", "client", $_iddemande, "canceled2");
			handleEmails($dbh, "garage_delete_announce", "garage", $_iddemande, "canceled");
			handleEmails($dbh, "admin_gar_declare_sold", "admin", $_iddemande, "canceled3");
		}
		
		//influe sur compte user et admin/demandes
		registerEventDemande($dbh, $_iddemande, time(), "delete_announce");
		
	}
	
	//a voir si ce status est ok, delete_announce serait plus approprié, mais il n'existait pas 
	//je ne sais pas si en ajouter un comme ça aurait causé des problèmes
	setStatusDemande($dbh, $_iddemande, "vehicule_sold");
	
	
}



function exam_complete($dbh, $_iddemande)
{
	trace("exam_complete()");
	 
	$ts = time();
	if(DEBUG_MODE) $ts = '1536697659';
	
	//$filename_rapport = "pdf/rapports/rapport-".$ts.".pdf";
	$filename_facture_garage = "pdf/factures/facture-".$ts."-garage.pdf";
	$filename_facture_client = "pdf/factures/facture-".$ts."-client.pdf";
	$filename_rapport_client = "pdf/rapports/rapport-".$ts."-client.pdf";
	
	
	//change line, because this timestamp is used for report 
	registerEventDemande($dbh, $_iddemande, time(), "exam_complete", ["filename" => $filename_rapport_client]);
	
	
	insertDocumentDB($dbh, $_iddemande, $ts, $filename_facture_garage, "facture_garage");
	insertDocumentDB($dbh, $_iddemande, $ts, $filename_facture_client, "facture_client");
	insertDocumentDB($dbh, $_iddemande, $ts, $filename_rapport_client, "rapport_client");
	
	if(!DEBUG_MODE){ 
		generate_facture($filename_facture_garage, $dbh, $_iddemande, "garage");
		generate_facture($filename_facture_client, $dbh, $_iddemande, "client");
		
		//appel de l'api de génération du rapport PDF
		$post = [
			"demand_id" => $_iddemande,
			"filename" => 'appbackend/'.$filename_rapport_client,
		];
		
		$url_script = BASE_URL_HTTPS."/../report_api.php";
		trace("url_script : ".$url_script);
		
		$ch = curl_init($url_script);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		
		trace($response);
	}
	
	 
	 
	// sleep(60 * 5);
	sleep(5);
	
	handleEmails($dbh, "client_exam_complete", "client", $_iddemande, "complete", array($filename_facture_client, $filename_rapport_client), "", array('ts' => $ts));
	handleEmails($dbh, "garage_exam_complete", "garage", $_iddemande, "complete_garage", array($filename_facture_garage), "", array('ts' => $ts));
	
	email_inspection_technique_terminee_to_vendeur($dbh, $_iddemande);
}





?>