<?php


require_once('include/time_utils.php');



function tracerec($_msg, $_level)
{
	$_str = "";
	for($i=0; $i<$_level; $i++) $_str .= "&nbsp;&nbsp;&nbsp;&nbsp;";
	echo $_str.$_msg."<br />";
}



function getItem($_id)
{
	global $dbh;
	$output = array();



	$dbh->query("set names utf8");
	 
	 
	$sql = "SELECT ";

	$sql .= "demandes_exam.id";
	$sql .= ", demandes_exam.demande_ts as ts";
	$sql .= ", acheteur.nom as name";
	$sql .= ", acheteur.prenom as firstname";
	$sql .= ", demandes_exam.present";

	$sql .= ", vendeur.nom as seller_name";
	$sql .= ", vendeur.prenom as seller_firstname";
	$sql .= ", vendeur.adresse as seller_adresse";
	$sql .= ", vendeur.adresse_num as seller_num";
	$sql .= ", vendeur.code_postal as seller_cp";
	$sql .= ", vendeur.ville as seller_city";
	$sql .= ", vendeur.telephone as seller_tel";
	$sql .= ", vendeur.email as seller_email";

	$sql .= ", annonces_clients.marque as brand";
	$sql .= ", annonces_clients.modele as model";
	$sql .= ", annonces_clients.version as version";
	$sql .= ", annonces_clients.year as year";
	$sql .= ", annonces_clients.kilometrage as km";

	$sql .= ", annonces_clients.carburant";
	$sql .= ", annonces_clients.transmission as engine";
	$sql .= ", annonces_clients.portes as nbdoors";
	$sql .= ", annonces_clients.desc1 as shortdesc";
	$sql .= ", annonces_clients.desc2 as longdesc";
	$sql .= ", annonces_clients.sieges as sieges";
	$sql .= ", UNIX_TIMESTAMP(annonces_clients.premiereimm) as premiereimm";

	$sql .= ", demandes_exam.ct_ts";
	$sql .= ", demandes_exam.ct_ts2";
	$sql .= ", demandes_exam.status";

	$sql .= " FROM demandes_exam JOIN clients as acheteur ON id_client_acheteur=acheteur.id JOIN clients as vendeur ON id_client_vendeur=vendeur.id JOIN annonces_clients ON id_annonces_clients=annonces_clients.id WHERE demandes_exam.id='".$_id."'";

	$r = $dbh->query($sql);

	$output = $r->fetch(PDO::FETCH_ASSOC);
	
	
	$output["num"] = $output["id"];
	$output["ts"] = strtotime($output["ts"]);
	$output["ct_ts"] = strtotime($output["ct_ts"]);
	$output["ct_ts2"] = strtotime($output["ct_ts2"]);
	$output["present"] = ($output["present"] == "1");
	$output["seller_address"] = $output["seller_num"]." ".$output["seller_adresse"];
	$output["seller_cp_city"] = $output["seller_cp"]." ".$output["seller_city"];
	
	$output["ts"] += getUTCshift();
	//+2 si été 	(UTC+2)
	//+1 si hiver (UTC+1)
	
	
	
	return $output;
}


?>