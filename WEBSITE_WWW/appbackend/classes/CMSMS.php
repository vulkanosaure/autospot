<?php
  
/* 
  class CMSMS
  {
    static public function buildMessageXml($recipient, $from, $message) {
      $xml = new SimpleXMLElement('<MESSAGES/>');

      $authentication = $xml->addChild('AUTHENTICATION');
      $authentication->addChild('PRODUCTTOKEN', SMS_TOKEN);

      $msg = $xml->addChild('MSG');
      $msg->addChild('FROM', $from);
      $msg->addChild('TO', $recipient);
      $msg->addChild('BODY', $message);

      return $xml->asXML();
    }

    static public function sendMessage($recipient, $from, $message) {
      $xml = self::buildMessageXml($recipient, $from, $message);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://sgw01.cm.nl/gateway.ashx',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);

      curl_close($ch);

      return $response;
    }
  }
   */
  
  
  class CMSMS
  {
    static public function buildMessageXml($recipient, $from, $message) {
      
	  $data = array(
		"messages" => array(
			"authentication" => array(
				"producttoken" => SMS_TOKEN
			),
			"msg" => array(
				array(
					"from" => $from,
					"to" => array(
						array("number" => $recipient)
					),
					"body" => array("content" => $message)
				)
			)
		)
	  );
	  
	//   var_dump($data);
	  return json_encode($data, JSON_PRETTY_PRINT);
    }

    static public function sendMessage($recipient, $from, $message) {
		
		$message = utf8_encode($message);
		
      $xml = self::buildMessageXml($recipient, $from, $message);
	  //echo "json : ".$xml."<br />";
	  
	//   trace("xml : ".$xml);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://gw.cmtelecom.com/v1.0/message',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/json'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);

      curl_close($ch);

      return $response;
    }
  }
  
  
  
  
?>