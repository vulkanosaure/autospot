<?php




function isOneGarageAvailable($dbh, $_cp, $_ts)
{
	trace("isOneGarageAvailable(".$_cp.", ".$_ts.")");
	for($i=0; $i<2; $i++){
		trace("i : ".$i);
		$garage = getGarageByCP($dbh, $_cp, $i);
		//var_dump($garage);
		if($garage != null && isGarageDispo($dbh, $garage["id"], $_ts)){
			trace("garage ".$i." dispo");
			return true;
		}
	}
	return false;
}


function insertDocumentDB($dbh, $_iddemande, $_ts, $_filename, $_type)
{
	$sql = "INSERT INTO documents SET ts=FROM_UNIXTIME(:ts), filename=:filename, type=:type, id_demande=:id_demande";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":ts", $_ts);
	$smth->bindParam(":filename", $_filename);
	$smth->bindParam(":type", $_type);
	$smth->bindParam(":id_demande", $_iddemande);
	$smth->execute();
	/* 
	trace("error insert document : ");
	var_dump($smth->errorInfo());
	 */
}



/*
pour une demande donnée
si le garage n'est pas dispo :
	augmente ind_refus
	recommence

il faut que des mails soient envoyés depuis handle
	

email envoyé en double : 
	garage 1 off, garage 2 on
*/


function handleDemandeAssociation($dbh, $_iddemande, $_event)
{
	$demande = getDemande($dbh, $_iddemande);
	//var_dump($demande);
	$_indrefus = $demande["index_refus"];
	trace("ooo________________________".$_event);
	trace("_indrefus : ".$_indrefus);
	
	$ts_test = time();
	
	$garage = getGarageByDemande($dbh, $_iddemande, $_indrefus);
	
	
	//cas ou garage 1 == garage 2
	if($_indrefus == 1){
		$garage2 = getGarageByDemande($dbh, $_iddemande, $_indrefus - 1);
		if($garage['id'] == $garage2['id']){
			trace('same same garage');
			$_indrefus++;
			$garage = getGarageByDemande($dbh, $_iddemande, $_indrefus);
		}
	}
	
	
	
	if($garage != NULL && !isGarageDispo($dbh, $garage["id"], $ts_test)){
		addRefusDemande($dbh, $_iddemande, 1);
		trace("relaunch func (idgarage : ".$garage["id"].")");
		handleDemandeAssociation($dbh, $_iddemande, $_event);
		return;
	}
	
	
	if($garage == NULL){
		trace("if null");
		garage_unavailable($dbh, $_iddemande);
		
		//si index_refus vaut pas 2, c'est qu'il n'yavait pas de garage existant
		//event garage_refuse, 2
	}
	else{
		trace("if else ".$_indrefus);
		saveIdGarage($dbh, $_iddemande, $garage["id"]);
		
		
		
		//2 event : onInit et onRefuse
		//onRefuse : doit envoyer les mails
		//onInit : doit pas les envoyer
		
		//j'avais pensé que si _indrefus == 1
			//on est dans onRefuse
		//mais c'est faut dans le cas ou
			//garage 1 off / garage 2 on
		
		if($_event == 'refuse' && $_indrefus == 1){
			handleEmails($dbh, "garage_new_demande", "garage", $_iddemande, "new");
		}
	}
	
}




//en fonction de list_holidays et actif

function isGarageDispo($dbh, $id_garage, $_ts)
{
	$garage = getGarage($dbh, $id_garage);
	$holidays = $garage["list_holidays"];
	
	$actif = $garage['actif'];
	if($actif == '0') return false;
	
	if($holidays == "") return true;
	
	$tabdays = explode("\n", $holidays);
	// var_dump($tabdays);
	foreach($tabdays as $day){
		
		$tab = explode("-", $day);
		$tsday = mktime(0, 0, 0, intval($tab[1]), intval($tab[0]), intval($tab[2]));
		if($_ts > $tsday && $_ts < $tsday + 86400) return false;
	}
	
	return true;
}


function getGarage($dbh, $_id)
{
	$sql = "SELECT * FROM garages WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_id);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output;
}



//get le x eme garage associ� � une demande
//si $_indrefus >= 2, null

function getGarageByDemande($dbh, $_iddemande, $_indrefus)
{
	$cp = getCPbyDemande($dbh, $_iddemande);
	trace("cp : ".$cp);
	return getGarageByCP($dbh, $cp, $_indrefus);
}


function isDemandeComplete($dbh, $_iddemande)
{
	$sql = "SELECT * FROM demandes_exam WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	$result = ($output['complete'] == 1);
	return $result;
}



/* 
function getGarageByCP($dbh, $cp, $_indrefus)
{
	if($_indrefus >= 2) return null;
	
	$sql = "SELECT * FROM garages WHERE cp LIKE '%".$cp."%'";
	$smth = $dbh->prepare($sql);
	$smth->execute();
	
	$counter = 0;
	while($output = $smth->fetch(PDO::FETCH_ASSOC)){
		//var_dump($output);
		if($counter == $_indrefus) return $output;
		$counter++;
	}
	return null;
}
*/
 
 
/* 
function getGarageByCP($dbh, $cp, $_indrefus)
{
	if($_indrefus >= 2) return null;
	
	$sql = "SELECT * FROM garages";
	$smth = $dbh->prepare($sql);
	$smth->execute();
	
	$list = array();
	$max_len = 0;
	while($output = $smth->fetch(PDO::FETCH_ASSOC)){
		$output["tab_cp"] = ($output["cp"] == "") ? NULL : explode(",", $output["cp"]);
		
		if($output["tab_cp"] != NULL){
			$len = count($output["tab_cp"]);
			if($len > $max_len) $max_len = $len;
		}
		
		$list[] = $output;
	}
	
	trace("____________________________________________________________");
	var_dump($list);
	trace("____________________________________________________________");
	
	trace("max_len : ".$max_len);
	
	$index_col = 0;
	$index_test = 0;
	
	for($i=0; $i<$max_len; $i++){
		
		foreach($list as $tab){
			$tab_cp = $tab["tab_cp"];
			if($tab_cp != null && isset($tab_cp[$index_col])){
				$val = $tab_cp[$index_col];
				if($val == $cp){
					if($_indrefus == $index_test) return $tab;
					else $index_test++;
				}
			}
		}
		$index_col++;
	}
	
	return null;
}
*/


function getGarageByCP($dbh, $cp, $_indrefus)
{
	if($_indrefus >= 2) return null;
	$col = ($_indrefus == 0) ? "id_garages__1" : "id_garages__2";
	
	$sql = "SELECT ".$col." as id_garage FROM cp2garage WHERE cp=:cp";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":cp", $cp);
	$smth->execute();
	if($smth->rowCount() == 0){
		return null;
	}
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	$id_garage = $output["id_garage"];
	if($id_garage == null) return null;
	
	$sql = "SELECT * FROM garages WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $id_garage);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output;
}



function getCPbyDemande($dbh, $_iddemande)
{
	// $sql = "SELECT * FROM demandes_exam JOIN annonces_clients ON id_annonces_clients=annonces_clients.id WHERE demandes_exam.id=:id_demande";
	$sql = "SELECT *, code_postal as cp FROM demandes_exam JOIN clients ON id_client_vendeur=clients.id WHERE demandes_exam.id=:id_demande";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id_demande", $_iddemande);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output["cp"];
}


function setStatusDemande($dbh, $_iddemande, $_value)
{
	$sql = "UPDATE demandes_exam SET status=:value WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":value", $_value);
	$smth->execute();
}



function setAcceptDemande($dbh, $_iddemande, $_value)
{
	$sql = "UPDATE demandes_exam SET accepted=:value WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":value", $_value);
	$smth->execute();
}



function insertDemande($dbh, $ts, $id_vendeur, $id_client, $id_announce, $present,$type_forfait='ScanExpert', $data_exam='')
{
	trace("ts : ".$ts.", id_announce : ".$id_announce);
	$datestr = date('Y-m-d H:i:s', $ts);
	
	$sql = "INSERT INTO demandes_exam SET id_annonces_clients=:id_annonces_clients, id_client_acheteur=:id_client_acheteur, id_client_vendeur=:id_client_vendeur, present=:present, type_forfait=:type_forfait, demande_ts=:date, data_exam=:data_exam, json_suivi=''";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":date", $datestr);
	$smth->bindParam(":id_client_vendeur", $id_vendeur);
	$smth->bindParam(":id_client_acheteur", $id_client);
	$smth->bindParam(":id_annonces_clients", $id_announce);
	$smth->bindParam(":present", $present);
    $smth->bindParam(":type_forfait", $type_forfait);
    $smth->bindParam(":data_exam", $data_exam);
	
	if (!$smth->execute()) {
		trace("error : ".$smth->errorInfo());
		return false;
	}

	$last_id = $dbh->lastInsertId();
	trace("INSERTION OK");
	return $last_id;
}





function setTimeDemande($dbh, $_iddemande, $_index, $_ts)
{
	$column = ($_index == 1) ? "ct_ts" : "ct_ts2";
	
	//convert timestamp into date
	$_date = date('Y-m-d H:i:s', $_ts);
	
	$sql = "UPDATE demandes_exam SET ".$column."=:date WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":date", $_date);
	$smth->execute();
}

function setRelanceDemande($dbh, $_iddemande, $_value)
{
	$sql = "UPDATE demandes_exam SET relance=:value WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":value", $_value);
	$smth->execute();
}


function setPaymentInfo($dbh, $_iddemande, $_value)
{
	$sql = "UPDATE demandes_exam SET payment_info=:value WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":value", $_value);
	$smth->execute();
}



function getDemande($dbh, $_id)
{
	$sql = "SELECT * FROM demandes_exam WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_id);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output;
}


function getDemandeExtended($dbh, $_id, $_delay = 0)
{
	$sql = "SELECT *, garages.name as garage_name, garages.tel as garage_tel, garages.address as garage_address, garages.cp_unique as garage_cp, garages.city as garage_city, garages.id as garage_id, ";
	$sql .= "UNIX_TIMESTAMP(demande_ts) as demande_ts";
	$sql .= ", UNIX_TIMESTAMP(ct_ts) as ct_ts";
	$sql .= ", UNIX_TIMESTAMP(ct_ts2) as ct_ts2";
	$sql .= " FROM demandes_exam JOIN annonces_clients ON annonces_clients.id=demandes_exam.id_annonces_clients JOIN garages ON demandes_exam.id_garage=garages.id WHERE demandes_exam.id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_id);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	
	
	//que quand lancé depuis cron, datetime different... ?
	
	if($output["demande_ts"] != null) $output['demande_ts'] += $_delay;
	if($output["ct_ts"] != null) $output['ct_ts'] += $_delay;
	if($output["ct_ts2"] != null) $output['ct_ts2'] += $_delay;
	//_____________________________
	
	return $output;
}

function getDemandeExtended_clients($dbh, $_id, $_type)
{
	//_type = client / seller
	$col = ($_type == "client") ? "id_client_acheteur" : "id_client_vendeur";
	$sql = "SELECT * FROM demandes_exam JOIN clients ON demandes_exam.".$col."=clients.id WHERE demandes_exam.id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_id);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output;
}

function getDemandeExtended_clients_byAnnounce($dbh, $_id)
{
	$sql = "SELECT * FROM annonces_clients JOIN clients ON annonces_clients.id_client=clients.id WHERE annonces_clients.id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_id);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output;
}

function getAnnounceExtended($dbh, $_id)
{
	$sql = "SELECT *";
	$sql .= " FROM annonces_clients JOIN clients ON annonces_clients.id_client=clients.id WHERE annonces_clients.id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_id);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output;
}

function getAnnonceByDemande($dbh, $_iddemande)
{
	$sql = "SELECT *";
	
	//        from annonces_clients JOIN demandes_exam on annonces_clients.id=demandes_exam.id_annonces_clients
	$sql .= " FROM annonces_clients JOIN demandes_exam ON annonces_clients.id=demandes_exam.id_annonces_clients WHERE demandes_exam.id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->execute();
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	return $output;
}



function deleteAnnonceByDemande($dbh, $_iddemande)
{
	$sql = "SELECT * FROM demandes_exam WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->execute();
	$demande = $smth->fetch(PDO::FETCH_ASSOC);
	$id_annonce = $demande['id_annonces_clients'];
	
	// $sql = "DELETE FROM annonces_clients WHERE id=:id";
	$sql = "UPDATE annonces_clients SET visible=0 WHERE id=:id";
	
	
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $id_annonce);
	$smth->execute();
}






function deleteDemande($dbh, $_iddemande)
{
	$sql = "DELETE FROM demandes_exam WHERE id=:id";
	
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->execute();
}




function addRefusDemande($dbh, $_iddemande, $_delta)
{
	$sql = "UPDATE demandes_exam SET index_refus=index_refus+:delta WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":delta", $_delta);
	$smth->execute();
}
function setRefusDemande($dbh, $_iddemande, $_value)
{
	$sql = "UPDATE demandes_exam SET index_refus=:value WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":value", $_value);
	$smth->execute();
}



function saveIdGarage($dbh, $_iddemande, $_idgarage)
{
	$sql = "UPDATE demandes_exam SET id_garage=:id_garage WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":id_garage", $_idgarage);
	$smth->execute();
}



function registerEventDemande($dbh, $_iddemande, $_ts, $_idevent, $_params = NULL)
{
	$sql = "SELECT * FROM demandes_exam WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->execute();
	
	$output = $smth->fetch(PDO::FETCH_ASSOC);
	$json = $output["json_suivi"];
	
	if($json == "") $tab = array();
	else $tab = json_decode($json);
	
	$obj = array();
	$obj["key"] = $_idevent;
	$obj["ts"] = $_ts;
	$obj["params"] = $_params;
	$tab[] = $obj;
	
	
	
	$json = json_encode($tab);
	$sql = "UPDATE demandes_exam SET json_suivi=:json WHERE id=:id";
	$smth = $dbh->prepare($sql);
	$smth->bindParam(":id", $_iddemande);
	$smth->bindParam(":json", $json);
	$smth->execute();
	
}




?>