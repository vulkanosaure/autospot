<?php

// Connexion BDD et récupération des informations
require_once('../../admin/include/db_connect.php');
require_once("../../events_utils.php");


$d = getDemandeExtended($dbh, 149); // Demande
$v = getDemandeExtended_clients($dbh, 149, "seller"); // vendeur
$a = getDemandeExtended_clients($dbh, 149, "client"); // client
$json = json_decode($d['data_exam'], true); // FIchier JSON


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<STYLE TYPE="text/css">
		P { margin-bottom: 0.08in; direction: ltr; widows: 2; orphans: 2 }
		A:link { color: #0563c1; so-language: zxx }
        body{ font-size: 0.8em; width: 80%;margin: auto; font-family: "Helvetica";}
        table{width: 100%;margin: auto;}
        @media print {
        footer {page-break-after: always;}
        }
        .grad {
            background: red; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(left, red , goldenrod, forestgreen); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(right, red , goldenrod, forestgreen); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(right, red , goldenrod, forestgreen); /* For Firefox 3.6 to 15 */
            background: linear-gradient(to right, red , goldenrod, forestgreen); /* Standard syntax */
            width: 100%;
            margin: auto;
            background-color: red;
        }
        .grad-left, .grad-right{
            display: inline-block;
            font-size: 0.6em;
        }
        .grad-left{
            text-align: left;
            width: 48%;
        }
        .grad-right{
            text-align: right;
            width: 48%;
        }
        .col-left{
            width: 60%;
        }
        col-right{
            width: 40%;
        }
        .grad-1, .grad-2, .grad-3, .grad-4, .grad-5{width: 18%;display: inline-block;margin: 0;padding: 0;}
	</STYLE>
</HEAD>
<BODY LANG="fr-CH" LINK="#0563c1" DIR="LTR">
<table style="text-align:center;">
    <tr>
        <td style='width:25%;'>
            <img src="logo.png" style="width:100%;" >
        </td>
        <td td style='width:40%;'>
        <p>
            SkaiWeb<br>
            1610 Oron-la-ville
        </p>
        </td>
        <td td style='width:305;text-align:right;'>
        <p>
            Inspection technique N° : <?=$d['id']?><br>
            Date : <?=date('d/m/Y')?>
        </p>
        </td>
    </tr>
</table>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in;text-align:right;">
    <?=$a['prenom']." ".$a['nom']?>
</P>
<P STYLE="margin-bottom: 0in;text-align:right;">
    <?=$a['adresse']?>
</P>
<P STYLE="margin-bottom: 0in;text-align:right;">
    <?=$a['code_postal']." - ".$a['ville']?>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><FONT SIZE=4>
    <B>RAPPORT D’INSPECTION TECHNIQUE</B>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">
    <B>Forfait ScanExpert /Forfait ScanExpert Protect</B>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in;">
    Vous avez effectué, le <?=date('d/m/Y', $d['demande_ts'])?>, une demande d’inspection technique pour une <?= $d['marque']." ".$d['modele'];?>.
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
    Le rapport d’inspection détaillé ci-après vous informe des résultats obtenus à l’examen statique et à l’examen dynamique (essai routier).
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
    Ce rapport d’inspection vous est transmis immédiatement dès la fin de l’inspection technique. 
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0.11in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.11in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.11in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.11in"><BR><BR>
</P>
<table>
    <tr>
        <td><B>VENDEUR GARAGE PARTENAIRE INSPECTION TECHNIQUE</B></td>
        <td><B>GARAGE PARTENAIRE</B></td>
        <td><B>INSPECTION TECHNIQUE</B></td>
    </tr>
    <tr>
        <td><?= $v['nom']?> - <?= $v['prenom']?><BR><?= $v['code_postal']?> - <?= $v['ville']?><BR><? $v['telephone']?></td>
        <td> GARAGE<BR><?= $d['address']?><BR><?= $d['cp_unique']." - ".$d['city']?></td>
        <td>Durée : 60 minutes<BR>Date : <?=date('d/m/Y', $d['demande_ts'])?><BR>Heure : <?=date('H:i', $d['demande_ts']);?><BR>Condition : Au sol et levée</td>
    </tr>
</table>
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
            
            

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=5><B>evaluations</B></SPAN></P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">Afin
d’obtenir une vision claire et rapide du résultat de l’inspection
technique, les 115 points de contrôle de l’inspection technique
sont répartis en sept catégories, visibles dans le tableau
ci-dessous. 
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">Une note de 5 signifie qu’il n’y a aucun défaut, dégât ou dysfonctionnement constaté.</P>

            
<table style='width:60%;margin:auto;'>
    <tr>
        <td class="col-left">CONFORMITÉ ADMINISTRATIVE</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td>CONFORMITÉ DU VÉHICULE</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td>ENTRETIEN</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td>SÉCURITÉ</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td>MÉCANIQUE</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td>CARROSSERIE & VITRAGE</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td>ÉQUIPEMENTS & INTERIEUR</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td>ÉVALUTATION MOYENNE</td>
        <td class="col-right">
            <div class="grad-number">
                <div class="grad-left">0</div>
                <div class="grad-right">5</div>
            </div>
            <div class="grad">&nbsp;</div>
        </td>
    </tr>
</table>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>En
rouge pour le DEV.</B></P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">Les
points de contrôle associés à chaque partie (conformité
administrative, conformité du véhicule, entretien etc) sont
répartis dans le rapport ci-dessus.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in"><B>Exemple
pour la partie «&nbsp;Equipement &amp; intérieur&nbsp;»</B>&nbsp;:
dans cette partie, il y a 12 points de contrôle.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">Si
on coche Aucun défaut constaté à ces 12 points de contrôle alors
on obtient une note de <B>5</B>
(5/16*16)</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">Comme
vous pouvez le voir sur l’image ci-dessus la note maximale est de 5
et la note minimale est 0.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in">Le
calcul pour obtenir la note est&nbsp;: 5 (car évalué sur une
échelle sur 5) divisé par le nb de points de contrôle associé à
chaque partie (en l’occurrence 12 pour la partie «&nbsp;Equipement
&amp; intérieur&nbsp;») multiplié par le nb de points de contrôle
où il y a aucun dégât/défaut/dysfonctionnement constaté</P>
<P STYLE="margin-bottom: 0.11in"><BR><BR>
</P>

<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    
    
<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=5><B>Identification
du véhicule</B></SPAN></P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=202>
	<COL WIDTH=382>
	<TR>
		<TD WIDTH=202 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
            <P>Marque</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['marque']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Modèle</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['modele'];?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Version</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['version']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Date de mise en circulation</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['premiereimm']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Type</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Voiture particulière</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Catégorie</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['categorie']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Carrosserie</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['carrosserie']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Cylindrée</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['cylindrees']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Puissance en cv</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['puissancechevaux']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Carburant</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['carburant']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Transmission</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['transmission']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Roues motrices</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['rouesmotrices']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Nb de places</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['sieges']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Nb de portes</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['portes']?></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=202 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Couleur extérieure</P>
		</TD>
		<TD WIDTH=382 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P><?= $d['couleurexterne']?></P>
		</TD>
	</TR>
</TABLE>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        
 
<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=5><B>PHotographies du véhicule</B></SPAN></P>
<P STYLE="margin-bottom: 0in; line-height: 100%"><SPAN STYLE="text-transform: uppercase">Face
avant</SPAN></P>
        

<P STYLE="margin-bottom: 0in; line-height: 100%"><SPAN STYLE="text-transform: uppercase">Côté gauche</SPAN></P>
        
        
<P STYLE="margin-bottom: 0in; line-height: 100%"><SPAN STYLE="text-transform: uppercase">Face arrière</SPAN></P>
        

<P STYLE="margin-bottom: 0in; line-height: 100%"><SPAN STYLE="text-transform: uppercase">Côté
droit</SPAN></P>


    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=5><B>Conformité
administrative</B></SPAN></P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=268>
	<COL WIDTH=316>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Documents présentés lors de l’examen</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Notice d’utilisation</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2><B>PT.
			La notice d’utilisation est-elle présente&nbsp;?</B></P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
            
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Permis
			de circulation</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2><B>PT.
			Le permis de circulation est-il présent&nbsp;?</B></P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
            
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Le
			numéro de plaques d’immatriculation du véhicule est identique
			à celui inscrit sur le permis de circulation</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2><B>PT.
			Le numéro de plaque d’immatriculation inscrit sur le permis etc</B></P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
            
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Le
			numéro de série VIN du véhicule est identique à celui inscrit
			sur le permis de circulation</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2><B>PT.
			Le numéro de série de la voiture (VIN) correspond-t-il avec etc</B></P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Double
			des clés</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2><B>PT.
			Le double des clés/télécommande est-il présent&nbsp;?</B></P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
            
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Permis de circulation</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>conformité
du véhicule</B></SPAN></P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=201>
	<COL WIDTH=202>
	<COL WIDTH=167>
	<TR>
		<TD COLSPAN=3 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Conformité du kilométrage</B></SPAN><B>*</B></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=201 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Kilométrage inséré par le vendeur</B></P>
		</TD>
		<TD WIDTH=202 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Kilométrage affiché sur le compteur</B></P>
		</TD>
		<TD WIDTH=167 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Différence</B></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=201 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
		</TD>
		<TD WIDTH=202 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
		</TD>
		<TD WIDTH=167 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; text-transform: uppercase">
<BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Généralement,
les vendeurs continuent de rouler avec leur véhicule lorsque
celui-ci est en vente. Toutefois, une grande différence entre le
kilométrage inséré par le vendeur et celui affiché sur le
compteur (ex. plusieurs milliers de kilomètres) doit, selon les cas,
vous permettre une plus grande marge de négociation.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Compteur kilométrique</P>
    
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Cohérence du kilométrage</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
            
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			
		</TD>
	</TR>
</TABLE>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Pièces/accessoires
			non d’origine</B></SPAN><FONT SIZE=4>*</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;La/les pièces non d'origine ne sont PAS présentent sur
			le permis de circulation mais possèdent le certificat
			d’homologation suisse&nbsp;» si c’est coché. 
			</P>
			<P>Afficher
			«&nbsp;La/les pièces concernés sont&nbsp;: X&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;La/les pièces non d'origine sont présentent sur le
			permis de circulation&nbsp;». si c’est coché. 
			</P>
			<P>Afficher
			«&nbsp;La/les pièces concernés sont&nbsp;: X&nbsp;»</P>
		</TD>
	</TR>
</TABLE>

<P STYLE="margin-bottom: 0in"><FONT SIZE=4>*<FONT SIZE=2 STYLE="font-size: 9pt">Selon
les informations fournies par le vendeur.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Photographie(s)
du/des certificat(s) d’homologation suisse</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Si
le 3ème point s’affiche alors afficher la/les photographie(s)
du/des certificat(s) d’homologation suisse</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Photographie(s)
de/des annexe(s) du permis de circulation</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Si
le 4ème point s’affiche alors afficher la photographie de/des
annexes du permis de circulation</P>

    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>entretien
du véhicule</B></SPAN></P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=221>
	<COL WIDTH=363>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Carnet
			&amp; services d’entretien</B></SPAN></P>
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Le carnet d’entretien / exemplaire client du carnet d’entretien
			électronique est-il présent&nbsp;?</P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Selon le carnet d’entretien standard, les services d’entretien
			ont-ils été respectés&nbsp;?</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Le carnet d’entretien standard est présent&nbsp;» si
			carnet d’entretien standard est coché</P>
			<P>Afficher
			«&nbsp;Le carnet d’entretien standard n’est pas présent&nbsp;»
			si non est coché. Dans ce cas, ne pas afficher le reste du
			tableau</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=221 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			<B>«&nbsp;Carnet
			d’entretien à jour</B>»
			si «&nbsp;Tous les services d’entretien ont été effectué&nbsp;»
			est coché.</P>
		</TD>
		<TD WIDTH=363 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Tous les services d’entretien ont été effectués&nbsp;»
			si c’est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=221 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;<B>Services
			d’entretien</B>&nbsp;»si
			c’est coché</P>
		</TD>
		<TD WIDTH=363 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;X (afficher ce qui est coché) services d’entretien
			n’ont pas été effectués&nbsp;».</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=221 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;<B>Services
			d’entretien</B>&nbsp;»
			si c’est coché</P>
		</TD>
		<TD WIDTH=363 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Le carnet d’entretien est vide&nbsp;» si c’est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Carnet
d’entretien standard</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Si
on coche carnet d’entretien standard alors afficher la/les
photographies du carnet d’entretien standard</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=266>
	<COL WIDTH=318>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Exemplaire
			client du carnet d’entretien électronique</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Le carnet d’entretien / exemplaire client du carnet d’entretien
			électronique est-il présent&nbsp;?</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’exemplaire client du carnet d’entretien électronique
			est présent&nbsp;» si<B>
			</B>exemplaire
			client du carnet d’entretien électronique est coché.</P>
			<P>Afficher
			«&nbsp;L’exemplaire client du carnet d’entretien électronique
			n’est pas présent&nbsp;» si non est coché. Dans ce cas, ne
			pas afficher le reste du tableau</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=266 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Date du
			dernier service</B></P>
		</TD>
		<TD WIDTH=318 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Kilométrages
			du dernier service</B></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=266 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			ici la date du dernier service</P>
		</TD>
		<TD WIDTH=318 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			ici les kilométrages du dernier service</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Exemplaire
client du carnet d’entretien électronique</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Si
on coche exemplaire client du carnet d’entretien électronique
alors afficher la/les photographies de l’exemplaire client du
carnet d’entretien électronique.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Facture(s)
			de service d’entretien</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Des factures de service d’entretien sont-elles présentes&nbsp;?</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Aucune facture de service d’entretien est présente&nbsp;»
			si non est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;X (afficher le nombre coché) facture(s) de service
			d’entretien est/sont présente&nbsp;» si ou est coché</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Facture(s)
des services d’entretien</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
la/les photographies des factures des services d’entretien si
une/des factures de service d’entretien sont présente.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>expertise
			du service des automobiles</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>La
			dernière expertise du service des automobiles a été effectuée
			le (afficher ici la date selon la BDD)</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>La
			dernière expertise du service des automobiles date de (afficher
			ici le nombre de jours visible dans la BDD) jours</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Ce
			véhicule n’est pas expertisé (selon la BDD)</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Si
			la date de la 1<SUP>ère</SUP>
			mise en circulation est de moins de 5 ans alors afficher « Ce
			véhicule n’a pas encore passé la 1<SUP>ère</SUP>
			expertise auprès des services des automobiles car la 1<SUP>ère</SUP>
			mise en circulation de la voiture date de moins de 5 ans.</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">En
suisse, tous les véhicules de tourisme sont soumis à un contrôle
périodique obligatoire, appelé expertise. Cette expertise, auprès
des services des automobiles, détermine si le véhicule satisfait
aux prescriptions légales de sécurité.</P>
<P ALIGN=JUSTIFY STYLE="margin-top: 0.08in; margin-bottom: 0in">La
1<SUP>ère</SUP>
expertise se déroule 5 ans après la 1<SUP>ère</SUP>
mise en circulation. Puis la seconde expertise se déroule 3 ans
après, puis tous les 2 ans.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=14 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Historique
			du/des sinistres (facture(s) à l’appui)</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Le vendeur possède-t-il la/les factures correspondant à un/des
			sinistre(s)</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Facture(s)
correspondant à un/des sinistre(s)</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
la photographie de la/les facture(s) correspondant à un/des
sinistres si oui est coché</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=417>
	<COL WIDTH=167>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Nb
			de propriétaires</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>Connaissez-vous
			le nombre exact de propriétaires (vous-même y.c)&nbsp;?</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=417 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Selon le vendeur, le nombre exact de propriétaires
			différents est connu&nbsp;» si oui est coché</P>
		</TD>
		<TD WIDTH=167 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER>Ce
			véhicule a été possédé par X (Afficher ici ce qui est coché)
			propriétaire(s)</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Si
			le point précédent s’affiche alors afficher «&nbsp;En
			achetant ce véhicule, vous serez le Xème propriétaire (X=le
			nombre qui est coché +1)</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Selon le vendeur, le nombre exact de propriétaires
			différents n’est pas connu&nbsp;» si non est coché</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Utilisation
			du véhicule</B></SPAN><B>*</B></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Qu’elle est l’utilisation actuelle du véhicule&nbsp;?</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER>Afficher
			«&nbsp;Selon le vendeur, ce véhicule est utilisé de façon
			<B>ponctuelle</B>
			en milieu <B>urbain&nbsp;»
			</B>si
			c’est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER>Afficher
			«&nbsp;Selon le vendeur, ce véhicule était utilisé de façon
			<B>ponctuelle</B>
			en milieu <B>extra-urbain&nbsp;»
			</B>si
			c’est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER>Afficher
			«&nbsp;Selon le vendeur, ce véhicule est utilisé de façon
			<B>ponctuelle</B>
			en milieu <B>mixte&nbsp;»
			</B>si
			c’est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER>Afficher
			«&nbsp;Selon le vendeur, ce véhicule est utilisé de façon
			<B>régulière</B>
			en milieu <B>urbain&nbsp;»
			</B>si
			c’est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER>Afficher
			«&nbsp;Selon le vendeur, ce véhicule est utilisé de façon
			<B>régulière</B>
			en milieu <B>extra-urbain&nbsp;»
			</B>si
			c’est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER>Afficher
			«&nbsp;Selon le vendeur, ce véhicule est utilisé de façon
			<B>régulière</B>
			en milieu <B>mixte&nbsp;»
			</B>si
			c’est coché</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=2>*</SPAN><FONT SIZE=2>Correspond
uniquement à l’utilisation qui a été faite par le vendeur actuel
(ne prend pas en compte l’utilisation faite par le/les anciens
propriétaires, si existant).</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Consommation
			réelle</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Connaissez-vous sa consommation réelle en carburant</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Le vendeur ne connaît pas la consommation réelle de
			cette voiture» si non est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Selon le vendeur, la consommation réelle de la voiture
			est de X (afficher ce qui est coché) L/100km&nbsp;» si oui est
			coché</P>
		</TD>
	</TR>
</TABLE>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Stationnement*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Demandez au vendeur si le véhicule est stationné à l’abri des
			intempéries&nbsp;?</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Selon le vendeur, ce véhicule est stationné à l’abri
			des intempéries (parking sous-terrain, garage, porche)&nbsp;» si
			oui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Selon le vendeur, ce véhicule n’est <B>pas</B>
			stationné à l’abri des intempéries (parking sous-terrain,
			garage, porche)&nbsp;» si non est coché</P>
		</TD>
	</TR>
</TABLE>

<P STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=2>*</SPAN><FONT SIZE=2>Correspond
uniquement au stationnement effectué par le vendeur actuel (ne prend
pas en compte le stationnement réalisé par le/les ancien(s)
propriétaire(s), si existant).</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Vérification
			du fonctionnement des témoins principaux au tableau de bord</B></SPAN></P>
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			</SPAN><FONT COLOR="#0070c0"><FONT SIZE=2><SPAN LANG="fr-FR">A
			la mise en contact du moteur, vérifier que les témoins
			principaux du tableau de bord s’allument puis s’éteignent</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Tous les témoins principaux s’allument puis
			s’éteignent&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Un ou plusieurs témoin(s) ne s’est pas allumé » si
			c’est sélectionné puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Vérification
q<FONT SIZE=2><SPAN LANG="fr-FR">ue
les témoins principaux du tableau de bord, moteur, batterie, ABS,
airbag, ESP, pression d’huile et anti-démarrage, s’allument à
la mise en contact puis s’éteignent au démarrage du moteur. </SPAN>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><SPAN LANG="fr-FR">Ces
témoins doivent systématiquement s’allumer puis s’éteindre
afin d’indiquer au conducteur leur bon fonctionnement et qu’aucune
anomalie n’est détectée.</SPAN></P>
<P LANG="fr-FR" STYLE="margin-bottom: 0in; text-transform: uppercase; line-height: 100%">
<BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase; line-height: 100%">
<BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase; line-height: 100%">
<BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Vérification
			de l’absence de témoins d’alerte/alarme au tableau de bord</B></SPAN></P>
			<P ALIGN=JUSTIFY><FONT COLOR="#0070c0"><FONT SIZE=2><SPAN LANG="fr-FR">PT.
			Après l’allumage du moteur, est-ce que des témoins (voyants)
			d’alerte ou d’alarme du tableau de bord s’allument ou
			clignote&nbsp;?</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun témoin d’alerte ou d’alarme s’allume/clignote
			au tableau de bord&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Un ou plusieurs témoin(s) d’alerte ou d’alarme
			s’allume(nt)/clignote(nt) au tableau de bord&nbsp;» si c’est
			sélectionné puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Les
témoins d’alerte, généralement de couleur <FONT SIZE=2><B>orange</B><FONT SIZE=2>,
<FONT SIZE=2><SPAN LANG="fr-FR">informent
d'une panne ou d'un dysfonctionnement sur le véhicule mais vous
permet de continuer à rouler.</SPAN></P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><SPAN LANG="fr-FR">Selon
les cas, soit il vous suffira de contrôler par vous-même le niveau
d’huile ou la pression des pneus, soit il sera nécessaire que vous
contactiez votre garagiste pour faire inspecter le/les témoins
présent(s) au tableau de bord.</SPAN></P>
<P ALIGN=JUSTIFY STYLE="margin-top: 0.08in; margin-bottom: 0in"><FONT SIZE=2><SPAN LANG="fr-FR">Les
témoins d’alarme, généralement de couleur </SPAN><FONT SIZE=2><SPAN LANG="fr-FR"><B>rouge</B></SPAN><FONT SIZE=2><SPAN LANG="fr-FR">,
informent d’un danger ou d’une panne du moteur qui nécessitent
l'arrêt immédiat de la voiture. </SPAN>
</P>
<P LANG="fr-FR" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; text-transform: uppercase">
<BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
<SPAN LANG="fr-FR">d’un
ou plusieurs témoin(s) d’alerte et/ou d’alarme</SPAN></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici la photographie du tableau de bord lorsqu’un ou plusieurs
témoins d’alerte, d’alarme s’allume/clignote&nbsp;».</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Efficacité
			du levier de frein de stationnement*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’efficacité du levier de frein de stationnement</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Efficace (course courte)&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Course importante (un réglage est nécessaire)&nbsp;» si
			c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Ne fonctionne pas&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Non concerné, frein de stationnement électronique&nbsp;»
			si c’est sélectionné</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*La
course du levier de frein de stationnement correspond à la longueur
du déplacement du levier de frein de stationnement nécessaire pour
bloquer les roues.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Efficacité
			du freinage*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Efficacité du freinage</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut constaté&nbsp;» si c’est sélectionné</P>
			<P>Si
			Un ou plusieurs défaut constaté est sélectionné afficher ce
			qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*
Vérification de l’absence de bruits anormaux, d’à-coups, de
pédale de frein molle ou de vibration au volant de direction lors du
freinage</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=266>
	<COL WIDTH=317>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			des suspensions*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état des suspensions</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=266 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions avant gauche</P>
		</TD>
		<TD WIDTH=317 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			«&nbsp;Suspensions avant gauche&nbsp;» n’est PAS coché</P>
			<P>Si
			«&nbsp;Suspensions avant gauche&nbsp;» est coché afficher ce
			qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=266 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions avant droit</P>
		</TD>
		<TD WIDTH=317 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			«&nbsp;Suspensions avant droit&nbsp;» n’est PAS coché</P>
			<P>Si
			«&nbsp;Suspensions avant droit&nbsp;» est coché afficher ce qui
			est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=266 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions arrière
			gauche</P>
		</TD>
		<TD WIDTH=317 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			«&nbsp;Suspensions arrière gauche&nbsp;» n’est PAS coché</P>
			<P>Si
			«&nbsp;Suspensions arrière gauche&nbsp;» est coché afficher ce
			qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=266 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions arrière
			droit</P>
		</TD>
		<TD WIDTH=317 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			«&nbsp;Suspensions arrière droit&nbsp;» n’est PAS coché</P>
			<P>Si
			«&nbsp;Suspensions arrière droit&nbsp;» est coché afficher ce
			qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*
Vérification de l’étanchéité de l’amortisseur, de l’état
du ressort, du triangle de suspensions et de son/ses silentbloc(s) et
rotule(s), du bras de suspensions et de son/ses silentbloc(s) et
rotule(s), de la biellette de barre stabilisatrice et de la barre
stabilisatrice</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux suspensions</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état des suspensions</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=292>
	<COL WIDTH=292>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Efficacité
			des suspensions*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions avant gauche</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné</P>
			<P>Si
			Un ou plusieurs dysfonctionnement constatés est sélectionné
			afficher ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions avant droit</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné</P>
			<P>Si
			Un ou plusieurs dysfonctionnement(s) constatés est sélectionné
			afficher ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions arrière
			gauche</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné</P>
			<P>Si
			Un ou plusieurs dysfonctionnement constatés est sélectionné
			afficher ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Suspensions arrière
			droit</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné</P>
			<P>Si
			Un ou plusieurs dysfonctionnement constatés est sélectionné
			afficher ce qui est coché.</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification,
lors de l’essai routier, de l’absence de bruits anormaux, de
roulis et de tangage</P>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=283>
	<COL WIDTH=300>
	<TR>
		<TD COLSPAN=2 WIDTH=598 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			des organes de la direction*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état des organes de la direction et des soufflets
			de cardan<B>&nbsp;</B></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=283 HEIGHT=5 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Crémaillère de
			direction</P>
		</TD>
		<TD WIDTH=300 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si c’est
			sélectionné ou si PAS coché</P>
			<P>Si
			«&nbsp;Crémaillère de direction&nbsp;» est coché afficher ce
			qui est coché en 2<SUP>ème</SUP>
			indentation.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=283 HEIGHT=5 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Boitier de direction</P>
		</TD>
		<TD WIDTH=300 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si c’est
			sélectionné ou si PAS coché</P>
			<P>Si
			«&nbsp;Boitier de direction&nbsp;» est coché afficher ce qui
			est coché en 2<SUP>ème</SUP>
			indentation.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=283 HEIGHT=5 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Colonne de direction</P>
		</TD>
		<TD WIDTH=300 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			PAS coché</P>
			<P>Si
			«&nbsp;Colonne de direction&nbsp;» est coché afficher ce qui
			est coché en 2<SUP>ème</SUP>
			indentation.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=283 HEIGHT=5 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Biellettes de direction</P>
		</TD>
		<TD WIDTH=300 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			PAS coché</P>
			<P>Si
			«&nbsp;Biellettes de direction&nbsp;» est coché afficher ce qui
			est coché en 2<SUP>ème</SUP>
			indentation
			puis
			<FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=283 HEIGHT=5 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Rotules de direction
			intérieures (rotules axiales)</P>
		</TD>
		<TD WIDTH=300 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			PAS coché</P>
			<P>Si
			«&nbsp;Rotules de direction intérieures (rotules axiales)&nbsp;»
			est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=283 HEIGHT=5 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Rotules de direction
			extérieures</P>
		</TD>
		<TD WIDTH=300 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			PAS coché</P>
			<P>Si
			«&nbsp;Rotules de direction extérieures&nbsp;» est coché
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=283 HEIGHT=4 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Soufflets de cardans</P>
		</TD>
		<TD WIDTH=300 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			PAS coché</P>
			<P>Si
			« Soufflets de cardans » est coché afficher ce qui est coché
			en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase">*</SPAN><FONT SIZE=2>Vérification
de l’état des organes de la direction et des soufflets de cardans,
sans retirer le/les cache(s) présent(s) sous le véhicule.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux organes de la direction</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état des organes de la direction</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Fonctionnement
			du volant de direction*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement du volant de direction</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnements constatés&nbsp;» est
			sélectionné afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase">*</SPAN><FONT SIZE=2>Vérification
de l’absence de bruits anormaux, de point(s) dur(s), mur(s), volant
dur et volant non droit</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Contrôle
			de la dérive / tenue de cap*</B></SPAN></P>
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			</SPAN><FONT COLOR="#00b0f0"><FONT SIZE=2>Contrôle
			de la dérive / tenue de cap</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut constaté, la voiture maintient le cap&nbsp;»
			si c’est sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs défauts constatés&nbsp;» est
			sélectionné afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase">*</SPAN><FONT SIZE=2>
Vérification, lors de l’essai routier en ligne droite, de
l’absence déportation du véhicule et de vibration du volant.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; text-transform: uppercase">
<BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=186>
	<COL WIDTH=398>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			des jantes*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état des jantes</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=186 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Jante avant gauche</P>
		</TD>
		<TD WIDTH=398 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Jante avant gauche n’est PAS coché</P>
			<P>Si
			jante avant gauche est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=186 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Jante avant droit</P>
		</TD>
		<TD WIDTH=398 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Jante avant droit n’est PAS coché</P>
			<P>Si
			jante avant gauche est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=186 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Jante arrière gauche</P>
		</TD>
		<TD WIDTH=398 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Jante arrière gauche n’est PAS coché</P>
			<P>Si
			jante avant gauche est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=186 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Jante arrière droit</P>
		</TD>
		<TD WIDTH=398 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Jante arrière droit n’est PAS coché</P>
			<P>Si
			jante avant gauche est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*L’état
des jantes est un élément esthétique (en observant l’absence de
rayures, frottement de trottoir) et un élément de sécurité (en
évaluant l’absence de fissures, choc/déformation).</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>En
cas d’achat, vérifiez que ce véhicule est vendu avec les mêmes
roues que celles qui ont été présenté lors de cette inspection
technique.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux jantes</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état des jantes.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=210>
	<COL WIDTH=374>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			des pneumatiques*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état des pneumatiques</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique avant gauche</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Pneumatique avant gauche n’est PAS coché</P>
			<P>Si
			pneumatique avant gauche est coché afficher ce qui est
			sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique avant droit</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Pneumatique avant droit n’est PAS coché</P>
			<P>Si
			pneumatique avant droit est coché afficher ce qui est sélectionné
			en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique arrière
			gauche</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Pneumatique arrière gauche n’est PAS coché</P>
			<P>Si
			pneumatique arrière gauche est coché afficher ce qui est
			sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique arrière
			droit</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Pneumatique arrière droit n’est PAS coché</P>
			<P>Si
			pneumatique arrière droit est coché afficher ce qui est
			sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*L’état
des pneumatiques est un élément de sécurité important car ils
constituent l’unique contact de la voiture avec la route. 
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Vérification
de l’absence d’hernie(s), d’entaille(s), de
craquelures/fissures, d’usure irrégulière, d’usure importante,
de déformation de la bande de roulement, de corps étrangers dans la
bande de roulement.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux pneumatiques</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état des pneumatiques.</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=210>
	<COL WIDTH=374>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Profondeur
			en millimètres des sculptures des pneumatiques*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Inscrivez la profondeur des sculptures des pneumatiques</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique avant gauche</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			Pneu neuf si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Profondeur des sculptures de X millimètres&nbsp;»</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Pneu à changer&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Absence d’indicateur d’usure&nbsp;» si c’est
			sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique avant droit</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			Pneu neuf si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Profondeur des sculptures de X millimètres&nbsp;»</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Pneu à changer&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Absence d’indicateur d’usure&nbsp;» si c’est
			sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Différence de
			profondeur des sculptures, en millimètres, entre les deux
			pneumatiques avant</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			ici la différence en mm entre la 1<SUP>ère</SUP>
			ligne et la 2<SUP>ème</SUP>
			ligne de ce tableau.</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Différence de profondeur de sculpture de X mm</P>
			<P>Ce chiffre ne doit pas
			dépasser 5 millimètres</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique arrière
			gauche</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			Pneu neuf si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Profondeur des sculptures de X millimètres&nbsp;»</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Pneu à changer&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Absence d’indicateur d’usure&nbsp;» si c’est
			sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pneumatique arrière
			droit</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			Pneu neuf si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Profondeur des sculptures de X millimètres&nbsp;»</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Pneu à changer&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Absence d’indicateur d’usure&nbsp;» si c’est
			sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Différence de
			sculptures, en millimètres, entre les deux pneumatiques avant</P>
		</TD>
		<TD WIDTH=374 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			ici la différence en mm entre la 1<SUP>ère</SUP>
			ligne et la 2<SUP>ème</SUP>
			ligne de ce tableau.</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Différence de profondeur de sculpture de X mm</P>
			<P>Ce chiffre ne doit pas
			dépasser 5 millimètres</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*La
durée de vie d’un pneumatique dépend de nombreux facteurs tels le
nombre de
<FONT SIZE=2>kilomètres
parcourus par l’automobiliste, le style de conduite, la propriété
de la gomme du pneumatique ou encore les conditions climatiques.</P>
<P ALIGN=JUSTIFY STYLE="margin-top: 0.08in; margin-bottom: 0in"><FONT SIZE=2>Selon
la législation suisse, un pneumatique est à changer dès que la
profondeur des sculptures atteint 1.6 millimètres. Toutefois, le
risque d’aquaplanage et la distance de freinage sont augmentés,
notamment sur route mouillée, dès une profondeur de sculpture de
3.0 millimètres.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Un
pneumatique neuf possède une profondeur de sculpture d’environ 8
millimètres.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Selon
la législation suisse, les deux pneumatiques avant et les deux
pneumatiques arrière ne doivent pas avoir une différence de
profondeur des sculptures de plus de 5 millimètres. 
</P>

    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=257>
	<COL WIDTH=327>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Vérification
			visuelle de l’epaisseur des garnitures des plaquettes de frein*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez visuellement l’usure des garnitures des plaquettes de
			frein</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=257 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Plaquettes de frein
			avant gauche</P>
		</TD>
		<TD WIDTH=327 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;&lt;=2mm (à changer) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			3mm (proche de la limite d’usure) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			&gt;4mm (OK) si c’est coché</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’avant des
			freins à tambours&nbsp;» si Roue avant gauche n’est PAS coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=257 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Plaquettes de frein
			avant droit</P>
		</TD>
		<TD WIDTH=327 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;&lt;=2mm (à changer) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			3mm (proche de la limite d’usure) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;&gt;4mm (OK)&nbsp;» si c’est coché</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’avant des
			freins à tambours&nbsp;» si Roue avant droit n’est PAS coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=257 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Plaquettes de frein
			arrière gauche</P>
		</TD>
		<TD WIDTH=327 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;&lt;=2mm (à changer) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			3mm (proche de la limite d’usure) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			&gt;4mm (OK) si c’est coché</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’arrière des
			freins à tambours&nbsp;» si Roue arrière gauche n’est PAS
			coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=257 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Plaquettes de frein
			arrière droit</P>
		</TD>
		<TD WIDTH=327 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;&lt;=2mm (à changer) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			3mm (proche de la limite d’usure) si c’est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			&gt;4mm (OK) si c’est coché</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’arrière des
			freins à tambours&nbsp;» si Roue arrière droit n’est PAS
			coché.</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Les
plaquettes de frein sont considérées comme des consommables, au
même titre que les disques de frein, les pneumatiques ou les balais
d’essuie-glaces. Cela signifie qu’ils s’usent au fil du temps
et des kilomètres.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Les
plaquettes de frein se changent par paire (2 plaquettes par roue) dès
que le témoin s’allume sur le tableau de bord ou, si la voiture
est dépourvue de témoin, dès que la garniture de la plaquette de
frein mesure 2 millimètres.</P>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=228>
	<COL WIDTH=355>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			de surface des disques de frein*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de surface et l’usure de disques de frein</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein avant
			gauche</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Disque avant gauche n’est PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			Disque avant gauche est coché afficher ce qui est sélectionné
			en 2<SUP>ème</SUP>
			indentation</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’avant des
			freins à tambours&nbsp;» si cette phrase c’est affiché au
			point précédent</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein avant
			droit</P>
		</TD>
		<TD WIDTH=355 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Disque avant droit n’est PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			Disque avant droit est coché afficher ce qui est sélectionné en
			2<SUP>ème</SUP>
			indentation</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’avant des
			freins à tambours&nbsp;» si cette phrase c’est affiché au
			point précédent</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein arrière
			gauche</P>
		</TD>
		<TD WIDTH=355 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Disque arrière gauche n’est PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			Disque arrière gauche est coché afficher ce qui est sélectionné
			en 2<SUP>ème</SUP>
			indentation</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’arrière des
			freins à tambours&nbsp;» si cette phrase c’est affiché au
			point précédent</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein arrière
			droit</P>
		</TD>
		<TD WIDTH=355 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Disque arrière droit n’est PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			Disque arrière droit est coché afficher ce qui est sélectionné
			en 2<SUP>ème</SUP>
			indentation</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’arrière des
			freins à tambours&nbsp;» si cette phrase c’est affiché au
			point précédent</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de frottements/rayures, usure(s) irrégulière(s),
fissure(s), cassure(s) et de corps gras.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux disques de frein</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état de surface des disques de frein</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=155>
	<COL WIDTH=184>
	<COL WIDTH=230>
	<TR>
		<TD COLSPAN=3 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Usure
			des disques de frein*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=155 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein avant
			gauche</P>
		</TD>
		<TD WIDTH=184 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Epaisseur
			du disque</P>
			<P STYLE="margin-bottom: 0in">Afficher
			ici l’épaisseur du disque inséré au <FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de surface et l’usure des disques de frein</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in">Côte
			minimale d’usure</P>
			<P>Afficher
			ici la côte minimale d’usure inséré au <FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Inscrivez la côte minimale d’usure des disques de frein</P>
		</TD>
		<TD WIDTH=230 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			OK si l’épaisseur du disque est au-dessus de la cote minimale
			d’usure</P>
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			«&nbsp;A remplacer (hors côte)&nbsp;» si l’épaisseur du
			disque est au-dessous de la cote minimale d’usure</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’avant des
			freins à tambours&nbsp;» si c’est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=155 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein avant
			droit</P>
		</TD>
		<TD WIDTH=184 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Epaisseur
			du disque</P>
			<P STYLE="margin-bottom: 0in">Afficher
			ici l’épaisseur du disque au <FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de surface et l’usure des disques de frein</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in">Côte
			minimale d’usure</P>
			<P>Afficher
			ici la côte minimale d’usure au<FONT COLOR="#00b0f0"><FONT SIZE=2>
			PT. Inscrivez la côte minimale d’usure des disques de frein</P>
		</TD>
		<TD WIDTH=230 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			OK si l’épaisseur du disque est au-dessus de la cote minimale
			d’usure</P>
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			«&nbsp;A remplacer (hors côte)&nbsp;» si l’épaisseur du
			disque est au-dessous de la cote minimale d’usure</P>
			<P>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’avant des
			freins à tambours&nbsp;» si c’est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=155 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein arrière
			gauche</P>
		</TD>
		<TD WIDTH=184 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Epaisseur
			du disque</P>
			<P STYLE="margin-bottom: 0in">Afficher
			ici l’épaisseur du disque au <FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de surface et l’usure des disques de frein</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in">Côte
			minimale d’usure</P>
			<P>Afficher
			ici la côte minimale d’usure au<FONT COLOR="#00b0f0"><FONT SIZE=2>
			PT. Inscrivez la côte minimale d’usure des disques de frein</P>
		</TD>
		<TD WIDTH=230 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			OK si l’épaisseur du disque est au-dessus de la cote minimale
			d’usure</P>
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			«&nbsp;A remplacer (hors côte)» si l’épaisseur du disque est
			au-dessous de la cote minimale d’usure</P>
			<P ALIGN=JUSTIFY>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’arrière des
			freins à tambours&nbsp;» si c’est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=155 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Disque de frein arrière
			droit</P>
		</TD>
		<TD WIDTH=184 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Epaisseur
			du disque</P>
			<P STYLE="margin-bottom: 0in">Afficher
			ici l’épaisseur du disque au <FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de surface et l’usure des disques de frein</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in"><BR>
			</P>
			<P STYLE="margin-bottom: 0in">Côte
			minimale d’usure</P>
			<P>Afficher
			ici la côte minimale d’usure au<FONT COLOR="#00b0f0"><FONT SIZE=2>
			PT. Inscrivez la côte minimale d’usure des disques de frein</P>
		</TD>
		<TD WIDTH=230 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			OK si l’épaisseur du disque est au-dessus de la cote minimale
			d’usure</P>
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Afficher
			«&nbsp;A remplacer (hors côte)&nbsp;» si l’épaisseur du
			disque est au-dessous de la cote minimale d’usure</P>
			<P ALIGN=JUSTIFY>Afficher
			«&nbsp;Non concerné. Ce véhicule possède à l’arrière des
			freins à tambours&nbsp;» si c’est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Lors
de freinages, non seulement les plaquettes de frein s’usent mais
également les disques de frein. Afin de garder un freinage optimal,
chaque constructeur automobile établit des côtes minimales d’usure
pour les disques de frein avant et arrière.</P>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=153>
	<COL WIDTH=431>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Vérification
			de l’absence de jeux de fonctionnements aux roues*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Vérifiez l’absence de jeux de fonctionnements aux roues</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=153 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Roue avant gauche</P>
		</TD>
		<TD WIDTH=431 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Absence de jeux de fonctionnements » si c’est
			sélectionné ou si roue avant gauche n’est pas coché</P>
			<P>Si
			roue avant gauche est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=153 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Roue avant droit</P>
		</TD>
		<TD WIDTH=431 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Absence de jeux de fonctionnements » si c’est
			sélectionné ou si roue avant gauche n’est pas coché</P>
			<P>Si
			roue avant droit est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=153 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Roue arrière gauche</P>
		</TD>
		<TD WIDTH=431 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Absence de jeux de fonctionnements » si c’est
			sélectionné ou si roue avant gauche n’est pas coché</P>
			<P>Si
			roue arrière gauche est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=153 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Roue arrière droit</P>
		</TD>
		<TD WIDTH=431 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Absence de jeux de fonctionnements » si c’est
			sélectionné ou si roue avant gauche n’est pas coché</P>
			<P>Si
			roue arrière droit est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*
Vérification de l’absence de jeux de fonctionnements aux roulement
de roue, au bras de suspensions, au triangle de suspensions, à la
biellette de barre stabilisatrice et à l’assiette suspensions.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Roue
			de secours ou nécessaire anti-crevaison</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Le véhicule possède-t-il une roue de secours ou un nécessaire
			anti-crevaison&nbsp;?</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Ce véhicule possède une roue de secours&nbsp;» si c’est
			coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Ce véhicule possède un nécessaire anti-crevaison&nbsp;»
			si c’est coché</P>
			<P>Afficher
			«&nbsp;Ce véhicule ne possède ni une roue de secours, ni un
			nécessaire anti-crevaison&nbsp;» si aucun des deux est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=624 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=608>
	<TR>
		<TD WIDTH=608 HEIGHT=11 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Fonctionnement
			du siège conducteur</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement du siège conducteur</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=608 HEIGHT=11 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Afficher
			le titre en gras correspondant puis <FONT FACE="Wingdings, serif">
			«&nbsp;Déplacement du siège&nbsp;» si c’est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			le titre en gras correspondant puis <FONT FACE="Wingdings, serif">
			«&nbsp;Réglage de l’assise&nbsp;» si c’est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné</P>
			<P>Afficher
			le titre en gras correspondant puis <FONT FACE="Wingdings, serif">
			«&nbsp;Réglage de l’inclinaison du dossier bloqué&nbsp;» si
			c’est coché</P>
		</TD>
	</TR>
</TABLE>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>securité
du véhicule (2)</B></SPAN></P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=210>
	<COL WIDTH=374>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Fonctionnement
			du système d’éclairage et de signalisation*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Avertisseur
			sonore</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’avertisseur sonore</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché 
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Phare
			diurne</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché 
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feux
			de position</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feux
			de croisement</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feux
			de route</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feux
			antibrouillard avant</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché 
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feu
			antibrouillard arrière</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché 
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feux
			de stop et feu de stop additionnel</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feu
			de recul</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feux
			de détresse</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des feux de détresse</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Feu
			de plaque arrière</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’éclairage avant et arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Clignotant
			avant gauche</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;clignotant avant gauche&nbsp;» est coché alors afficher
			ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté gauche ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté gauche
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Clignotant
			de rétroviseur gauche</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou pas concerné » si
			PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;clignotant de rétroviseur&nbsp;» est coché alors
			afficher ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté gauche ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté gauche
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Répétiteurs
			latéraux de clignotant gauche</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;répétiteurs latéraux de clignotant gauche&nbsp;» est
			coché alors afficher ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté gauche ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté gauche
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Clignotant
			arrière gauche</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;clignotant arrière gauche&nbsp;» est coché alors
			afficher ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté gauche ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté gauche
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Clignotant
			avant droit</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;clignotant avant droit&nbsp;» est coché alors afficher
			ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté droit ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté droit
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Clignotant
			de rétroviseur droit</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;clignotant de rétroviseur&nbsp;» est coché alors
			afficher ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté droit ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté droit
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Répétiteurs
			latéraux de clignotant droit</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;répétiteurs latéraux de clignotant droit&nbsp;» est
			coché alors afficher ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté droit ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté droit
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Clignotant
			arrière droit<FONT COLOR="#00b0f0"><FONT SIZE=2>
			
			</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des clignotants avant, latéraux et
			arrière</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si PAS coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;clignotant arrière droit&nbsp;» est coché alors
			afficher ce qui est coché</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun clignotant ne s’allume&nbsp;» est coché alors
			afficher «&nbsp;Ne s’allume pas&nbsp;»</P>
			<P>Si
			«&nbsp;Les clignotants du côté droit ne s’allument pas&nbsp;»
			est coché alors afficher «&nbsp;Les clignotants du côté droit
			ne s’allument pas&nbsp;»</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Un
bon éclairage et une bonne signalisation sont des facteurs
déterminants pour votre sécurité et celle des autres
automobilistes.</P>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=228>
	<COL WIDTH=355>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			des optiques avant et arrière*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état des optiques avant et arrière</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Optique avant gauche</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si PAS coché</P>
			<P>Si
			«&nbsp;bloc optique avant gauche&nbsp;» est coché alors
			afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Optique avant droit</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si PAS coché</P>
			<P>Si
			«&nbsp;bloc optique avant droit&nbsp;» est coché alors afficher
			ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Phare anti brouillard
			avant gauche</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si PAS coché</P>
			<P>Si
			«&nbsp;phare antibrouillard avant gauche&nbsp;» est coché alors
			afficher ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Phare anti brouillard
			avant droit</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si PAS coché</P>
			<P>Si
			«&nbsp;phare antibrouillard avant droit&nbsp;» est coché alors
			afficher ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Phares diurnes</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si PAS coché</P>
			<P>Si
			«&nbsp;phares diurne» est coché alors afficher ce qui est
			coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Optique arrière gauche</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si PAS coché</P>
			<P>Si
			«&nbsp;bloc optique arrière gauche » est coché alors afficher
			ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Optique arrière droit</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si PAS coché</P>
			<P>Si
			«&nbsp;bloc optique arrière droit » est coché alors afficher
			ce qui est coché.</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><SPAN LANG="fr-FR">*</SPAN><FONT SIZE=2>
Vérification de<FONT SIZE=2><SPAN LANG="fr-FR">
l’absence de rayure(s), fissure(s), impact(s), optiques opaque(s)
ou embué ou de mauvaise fixation. Les optiques d'une automobile se
doivent de toujours être dans un parfait état</SPAN></P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux optiques</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état des optiques avant et arrière</P>

<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=247>
	<COL WIDTH=336>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>verres
			de rétroviseurs latéraux et central*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=247 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Etat
			&amp; fonctionnement des verres de rétroviseurs gauche et droite</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des verres de rétroviseurs gauche et
			droit</P>
		</TD>
		<TD WIDTH=336 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât/dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dégât/dysfonctionnement constaté&nbsp;»
			est sélectionné afficher ce qui est coché <FONT FACE="Wingdings, serif">
			puis afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation <FONT FACE="Wingdings, serif">
			puis afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=247 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Etat
			&amp; fonctionnement du rétroviseur central</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état du miroir et le fonctionnement du rétroviseur
			central (pivots)</P>
		</TD>
		<TD WIDTH=336 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut/dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dégât/dysfonctionnement constaté&nbsp;»
			est sélectionné afficher ce qui est coché <FONT FACE="Wingdings, serif">
			puis afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Les
verres de rétroviseurs sont un élément de sécurité important. Il
est primordial que ceux-ci soient en bon état pour que vous puissiez
effectuer les manœuvres nécessaire (dépasser un véhicule, se
rabattre ou se parquer).</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=228>
	<COL WIDTH=355>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Essuie-glaces</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Etat
			des balais d’essuie-glaces avant</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état des balais d’essuie-glace avant et arrière&nbsp;</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dégâts constatés&nbsp;» est
			sélectionné afficher ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Etat
			des balais d’essuie-glaces arrière</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état des balais d’essuie-glace avant et arrière&nbsp;</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dégâts constatés&nbsp;» est
			sélectionné afficher ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			des balais d’essuie-glaces avant</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des essuie-glaces avant et arrière</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné&nbsp;» afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			(pour ne fonctionne pas) ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			du balai d’essuie-glace arrière</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des essuie-glaces avant et arrière</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné&nbsp;» afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			(pour ne fonctionne pas) ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			des gicleurs avant d’essuie-glace</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des gicleurs avant et arrière
			d’essuie-glace</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné&nbsp;» afficher le titre en gras correspondant puis
			<FONT FACE="Wingdings, serif">
			ce qui est coché puis <FONT FACE="Wingdings, serif">
			(pour aucun débit) ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			du gicleur arrière d’essuie-glace</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des gicleurs avant et arrière
			d’essuie-glace</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné&nbsp;» afficher le titre en gras correspondant puis
			<FONT FACE="Wingdings, serif">
			ce qui est coché puis <FONT FACE="Wingdings, serif">
			(pour aucun débit) ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=228>
	<COL WIDTH=355>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>etat
			et fonctionnement des Ceintures de sécurité*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Ceinture
			de sécurité conducteur</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état et le fonctionnement de la ceinture conducteur</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut/dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs défaut/dysfonctionnement constaté&nbsp;»
			est sélectionné afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=228 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Ceinture
			de sécurité passager</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état et le fonctionnement de la ceinture passager</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut/dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs défaut/dysfonctionnement constaté&nbsp;»
			est sélectionné afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD ROWSPAN=3 WIDTH=228 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Ceintures
			de sécurité à l’arrière</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des ceintures de sécurité à
			l’arrière</P>
		</TD>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Ceinture
			de sécurité arrière (côté conducteur)</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut/dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs défaut/dysfonctionnement constaté&nbsp;»
			est sélectionné afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Ceinture
			de sécurité arrière (au centre)</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut/dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs défaut/dysfonctionnement constaté&nbsp;»
			est sélectionné afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=355 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Ceinture
			de sécurité arrière (côté passager)</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut/dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs défaut/dysfonctionnement constaté&nbsp;»
			est sélectionné afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’usure, des enrouleurs et des boucles de ceintures de sécurité.
L’état et le fonctionnement des ceintures de sécurité est un
élément trop souvent négligé lors de l’achat d’une voiture
d’occasion.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) liée(s) à l’usure des ceinture(s) de sécurité</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0.11in">Afficher
ici la/les photographie(s) de l’usure de la/des ceintures de
sécurité</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>Mécanique
du véhicule</B></SPAN></P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Diagnostic
			électronique*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Lecteur de diagnostic électronique</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Aucune panne/ défaut / dysfonctionnement affiché» si
			c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Une ou plusieurs panne(s) / défaut(s) /
			dysfonctionnement(s) affiché(s)&nbsp;» si c’est sélectionné
			puis <FONT FACE="Wingdings, serif">
			afficher le titre en gras puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher
			«&nbsp;Impossible de brancher le diagnostic électronique&nbsp;»
			si c’est sélectionné.</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Tous
nos garages partenaires possèdent un outil de diagnostic
électronique pour toutes les marques de voitures. Une fois branché,
l’outil de diagnostic permet de déterminer rapidement les
défauts/dysfonctionnements présents dans la voiture.

</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
de la mémoire de défaut</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici la photographie de l’écran affichant la mémoire de défaut</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			du compartiment moteur*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Sans démonter de caches, décrivez du dessus l’état du
			compartiment moteur et de l’étanchéité moteur</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dégât constaté» est sélectionné
			afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de choc(s)/déformation(s), corrosion, sans retirer
le/les cache(s) présent(s) sur le moteur.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) au compartiment moteur</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état du compartiment moteur</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>etanchéité
			moteur*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Sans démonter de caches, décrivez du dessus l’état du
			compartiment moteur et de l’étanchéité moteur</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée » si c’est sélectionné</P>
			<P>Si
			«&nbsp;Une ou plusieurs fuites constatées» est sélectionné
			afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de fuite(s) de fluide(s), sans retirer le/les cache(s)
présent(s) sur le moteur.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Photographie
du compartiment moteur</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici la photographie du compartiment moteur</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=276>
	<COL WIDTH=308>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Niveaux
			des fluides</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez le niveau des fluides</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=276 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Huile moteur</P>
		</TD>
		<TD WIDTH=308 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Niveau OK&nbsp;» si c’est sélectionné ou si Niveau
			huile moteur n’est PAS coché</P>
			<P>Si
			Niveau huile moteur est coché afficher ce qui est sélectionné.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=276 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide de frein</P>
		</TD>
		<TD WIDTH=308 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Niveau OK&nbsp;» si c’est sélectionné ou si Niveau
			liquide de frein n’est PAS coché</P>
			<P>Si
			Niveau liquide de frein est coché afficher ce qui est
			sélectionné.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=276 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide de
			refroidissement</P>
		</TD>
		<TD WIDTH=308 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Niveau OK&nbsp;» si c’est sélectionné ou si Niveau
			liquide de refroidissement n’est PAS coché</P>
			<P>Si
			Niveau liquide de refroidissement est coché afficher ce qui est
			sélectionné.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=276 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide de direction
			assistée</P>
		</TD>
		<TD WIDTH=308 VALIGN=TOP STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Niveau OK&nbsp;» si c’est sélectionné ou si Niveau
			liquide de direction assistée n’est PAS coché</P>
			<P>Si
			Niveau liquide de direction assistée est coché afficher ce qui
			est sélectionné.</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Type
			de distribution</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Selon le carnet d’entretien ou la notice d’utilisation,
			inscrivez la périodicité du remplacement de la distribution</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Ce véhicule possède une courroie de distribution&nbsp;»
			si Concerné est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Ce véhicule possède une chaine de distribution&nbsp;»
			si chaine de distribution est coché</P>
			<P>Afficher
			«&nbsp;Ce véhicule possède une cascade de pignons&nbsp;» si
			cascade de pignons est coché</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Si
la phrase «&nbsp;Ce véhicule possède une courroie de
distribution&nbsp;» est affiché alors afficher la phrase «&nbsp;La
courroie de distribution est une pièce d’usure importante du
moteur. Veuillez observer le point suivant «&nbsp;Périodicité du
changement de la courroie de distribution&nbsp;» afin de connaître
les préconisations constructeur&nbsp;».</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=190>
	<COL WIDTH=191>
	<COL WIDTH=190>
	<TR>
		<TD COLSPAN=3 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Périodicité
			du changement de la courroie de distribution*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Selon le carnet d’entretien ou la notice d’utilisation,
			inscrivez la périodicité du remplacement de la distribution</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=190 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Mois</B></P>
		</TD>
		<TD WIDTH=191 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Années</B></P>
		</TD>
		<TD WIDTH=190 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Kilomètres</B></P>
		</TD>
	</TR>
	<TR VALIGN=TOP>
		<TD WIDTH=190 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher ce qui
			est inséré dans l’App</P>
		</TD>
		<TD WIDTH=191 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher ce qui
			est inséré dans l’App</P>
		</TD>
		<TD WIDTH=190 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Afficher ce qui
			est inséré dans l’App</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Selon
les préconisations du constructeur. Au premier des deux termes échus
(Mois/année OU kilomètres</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Ne
pas afficher ce tableau si on a cliqué cascade de pignons ou chaine
de distribution</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>Mécanique
du véhicule (2)</B></SPAN></P>
<P ALIGN=CENTER STYLE="margin-top: 0.08in; margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>Voiture
levée</B></SPAN></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=292>
	<COL WIDTH=292>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			du bas moteur*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Sans démonter de caches, décrivez l’état du bas moteur</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Carter d’huile moteur</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dégâts constatés&nbsp;» est
			sélectionné afficher ce qui est coché</P>
			<P>Afficher
			«&nbsp;Observation impossible&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 HEIGHT=22 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Bloc moteur</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dégâts constatés&nbsp;» est
			sélectionné afficher ce qui est coché</P>
			<P>Afficher
			«&nbsp;Observation impossible&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de dommage(s), sans retirer le/les cache(s) présent(s)
sous le véhicule.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) liée(s) au bas moteur</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état du bas moteur</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Si
observation impossible est sélectionné afficher la phrase «&nbsp;Sur
ce modèle de voiture, il est impossible d’observation l’état du
bas moteur car celui-ci est parcouru de cache(s). Malheureusement, il
est impossible de retirer le/les cache(s) moteur dans le temps
imparti de l’inspection technique.&nbsp;»</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=210>
	<COL WIDTH=374>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Vérification
			de l’absence de fuites de fluides*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Vérifiez l’absence de fuite de fluide</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Huile moteur</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée&nbsp;» si c’est sélectionné
			ou si Fuite d’huile moteur n’est PAS coché</P>
			<P>Si
			«&nbsp;Fuite d’huile moteur&nbsp;» est coché afficher ce qui
			est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide de frein</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée&nbsp;» si c’est sélectionné
			ou si Fuite de liquide de frein n’est PAS coché</P>
			<P>Si
			«&nbsp;Fuite de liquide de frein&nbsp;» est coché afficher ce
			qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide de direction
			assistée</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée&nbsp;» si c’est sélectionné
			ou si Fuite du liquide de direction assistée n’est PAS coché</P>
			<P>Si
			«&nbsp;Fuite du liquide de direction assistée&nbsp;» est coché
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide de
			refroidissement</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée&nbsp;» si c’est sélectionné
			ou si Fuite du liquide de refroidissement n’est PAS coché</P>
			<P>Si
			«&nbsp;Fuite du liquide de refroidissement&nbsp;» est coché
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide de transmission</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée&nbsp;» si c’est sélectionné
			ou si Fuite du liquide de transmission n’est PAS coché</P>
			<P>Si
			«&nbsp;Fuite du liquide de transmission&nbsp;» est coché
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Liquide lave-glace</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée&nbsp;» si c’est sélectionné
			ou si Fuite du liquide lave-glace n’est PAS coché</P>
			<P>Si
			«&nbsp;Fuite du liquide lave-glace&nbsp;» est coché afficher ce
			qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=210 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Carburant</P>
		</TD>
		<TD WIDTH=374 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fuite constatée&nbsp;» si c’est sélectionné
			ou si Fuite de carburant n’est PAS coché</P>
			<P>Si
			«&nbsp;Fuite de carburant&nbsp;» est coché afficher ce qui est
			coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de fuite(s) de fluide(s) sans retirer le/les cache(s)
présent(s) sous le véhicule.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=219>
	<COL WIDTH=365>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Etat
			des bas de caisse / jupes latérales*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=219 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Bas
			de caisse / jupe latérale côté gauche</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état du bas de caisse/jupe latérale côté gauche</P>
		</TD>
		<TD WIDTH=365 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dégât constaté» est sélectionné
			afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=219 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Bas
			de caisse / jupe latérale côté droit</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état du bas de caisse/jupe latérale côté droit</P>
		</TD>
		<TD WIDTH=365 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dégât constaté» est sélectionné
			afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de corrosion, choc(s)/déformation(s), partie(s)
saillante(s), différence(s) de teinte et trace(s) de réparation.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux bas de caisse</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état des bas de caisse</P>
<P LANG="fr-FR" STYLE="margin-bottom: 0in"><BR>
</P>
<P LANG="fr-FR" STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=211>
	<COL WIDTH=372>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			du châssis*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état général du châssis</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=211 HEIGHT=11 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Berceau</P>
		</TD>
		<TD WIDTH=372 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dégâts constatés&nbsp;» est
			sélectionné afficher ce qui est coché.</P>
			<P>Afficher
			«&nbsp;Observation impossible si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=211 HEIGHT=11 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Longeron</P>
		</TD>
		<TD WIDTH=372 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dégâts constatés&nbsp;» est
			sélectionné afficher ce qui est coché.</P>
			<P>Afficher
			«&nbsp;Observation impossible si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=211 HEIGHT=10 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Plancher</P>
		</TD>
		<TD WIDTH=372 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dégâts constatés&nbsp;» est
			sélectionné afficher ce qui est coché.</P>
			<P>Afficher
			«&nbsp;Observation impossible si c’est sélectionné</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de corrosion perforante, fissure(s),
choc(s)/déformation(s), sans retirer le/les cache(s) présent(s)
sous le véhicule.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Si
observation impossible est coché alors afficher la phrase «&nbsp;Sur
ce modèle de voiture, il est impossible d’observer le châssis car
celui-ci est parcouru de caches. Malheureusement, il est impossible
de retirer le/les cache(s) concerné(s) dans le temps imparti de
l’inspection technique.&nbsp;»</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Photographie
du châssis avant</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici la photo du châssis avant</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Photographie
du châssis arrière</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici la photo du châssis arrière</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) liée(s) au châssis</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état général du châssis</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=292>
	<COL WIDTH=292>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat,
			étanchéité et fixation du système d’échappement &amp; pot
			catalytique*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état et la fixation du système d’échappement &amp;
			du pot catalytique</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 HEIGHT=4 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Collecteur d’échappement</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât/fuite constaté&nbsp;» si c’est coché</P>
			<P>Si
			Un ou plusieurs dégâts/fuites constaté(s) est sélectionné
			afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 HEIGHT=4 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Ligne d’échappement</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât/fuite constaté&nbsp;» si c’est coché</P>
			<P>Si
			Un ou plusieurs dégâts/fuites constaté(s) est sélectionné
			afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=292 HEIGHT=3 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Silencieux</P>
		</TD>
		<TD WIDTH=292 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât/fuite constaté&nbsp;» si c’est coché</P>
			<P>Si
			Un ou plusieurs dégâts/fuites constaté(s) est sélectionné
			afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de fuite(s), corrosion perforante, fissure(s),
choc(s)/déformation(s), et mauvaise fixation</P>
<P STYLE="margin-bottom: 0.11in"><BR><BR>
</P>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>Mécanique
du véhicule (3)</B></SPAN></P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Démarrage
			du moteur</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Comment se déroule le démarrage du moteur</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Démarrage aisé&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Démarrage à la 2<SUP>ème</SUP>
			tentative&nbsp;» si c’est sélectionné. 
			</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Démarrage à la 3<SUP>ème</SUP>
			tentative&nbsp;» si c’est sélectionné. 
			</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Ne démarre pas mais l’intensité lumineuse est
			bonne&nbsp;»si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Ne démarre pas et l’intensité lumineuse est faible ou
			inexistante&nbsp;» si c’est sélectionné puis <FONT FACE="Wingdings, serif">
			afficher ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=193>
	<COL WIDTH=391>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Vérification
			de l’absence de bruits anormaux</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Moteur</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Vérifiez l’absence de bruits anormaux dans le moteur</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun bruit anormal» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Un ou plusieurs bruits anormaux&nbsp;» si c’est
			sélectionné puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Echappement</P>
			<P ALIGN=JUSTIFY><FONT COLOR="#0070c0"><FONT SIZE=2><SPAN LANG="fr-FR">PT.
			Vérifiez l’absence de bruits anormaux à l’échappement</SPAN></P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun bruit anormal&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Présence de bruits anormaux&nbsp;» si c’est
			sélectionné puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Lors
			de virages serrés et lors d’accélérations et décélérations
			en ligne droite</P>
			<P ALIGN=JUSTIFY><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Vérifiez l’absence de bruits anormaux lors de virages serrés
			et lors de décélérations et accélérations en ligne droite. 
			</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun bruit anormal&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Présence de bruits anormaux&nbsp;» si c’est
			sélectionné puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché.</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Vérification
			de l’absence de fumée anormale à l’échappement</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			<FONT COLOR="#0070c0"><FONT SIZE=2><SPAN LANG="fr-FR">Vérifiez
			l’absence de fumée anormale à l’échappement</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune fumée anormale à l’échappement&nbsp;» si
			c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Présence de fumée(s) anormale(s) à l’échappement&nbsp;»
			si c’est sélectionné puis <FONT FACE="Wingdings, serif">
			affiché ce qui est sélectionné en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			(pour fumée noire) afficher ce qui est sélectionné en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Test
			de l’embrayage à l’arrêt*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Le test de l’embrayage a été réalisé avec succès.
			L’embrayage est en bon état&nbsp;» si «&nbsp;La voiture cale
			immédiatement&nbsp;» est coché</P>
			<P>Afficher
			«&nbsp;Le test de l’embrayage n’a pas été concluant. En
			effet, l’embrayage patine. Cela signifie qu’il est usé et
			qu’il faudra envisager de le changer.</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’état de l’embrayage en réalisant le test suivant&nbsp;:
«&nbsp;démarrer le moteur, enclencher le frein à main, la 3ème
vitesse et garder la pédale d'embrayage enfoncée. Puis relâcher
brusquement la pédale d'embrayage.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Passage
			des rapports (transmission MANUELLE) *</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			En mouvement, comment se déroule le passage des rapports
			(transmission manuelle)</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut constaté&nbsp;» si c’est sélectionné</P>
			<P>Si
			Un ou plusieurs défauts constatés est sélectionné afficher ce
			qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de bruits de craquements, de point(s) dur(s), de
mur(s), de boîte de vitesse dure.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Passage
			des rapports (transmission AUTOMATIQUE) *</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			En mouvement, comment se déroule le passage des rapports
			(transmission automatique)</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut constaté&nbsp;» si c’est sélectionné</P>
			<P>Si
			Un ou plusieurs défauts constatés est sélectionné afficher ce
			qui est coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de bruits de craquements, de vibrations anormales,
d’à-coups et de glissement de vitesse</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=598>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Comportement
			à l’accélération</B></SPAN></P>
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			</SPAN><FONT COLOR="#0070c0"><FONT SIZE=2><SPAN LANG="fr-FR">«&nbsp;Décrivez
			le comportement à l’accélération&nbsp;»</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Les accélérations sont franches et sans à-coups&nbsp;»
			si oui est coché</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Perte de puissance à l’accélération lors de l’essai
			routier&nbsp;» si Perte de puissance est coché</P>
			<P>Afficher
			«&nbsp;Présence d’à-coups à l’accélération lors de
			l’essai routier&nbsp;» si présence d’à-coups est coché</P>
		</TD>
	</TR>
</TABLE>
    
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
    

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>Carrosserie
&amp; vitrage</B></SPAN></P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=257>
	<COL WIDTH=327>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Etat
			des éléments de carrosserie*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><B>Face
			avant</B></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de la carrosserie face avant</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=257 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Capot</P>
		</TD>
		<TD WIDTH=327 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Capot n’est PAS coché</P>
			<P>Si
			«&nbsp;Capot » est coché afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=257 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pare-chocs avant</P>
		</TD>
		<TD WIDTH=327 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Pare-chocs avant n’est PAS coché</P>
			<P>Si
			«&nbsp;Pare-chocs avant » est coché afficher ce qui est coché
			en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de corrosion, choc(s)/déformation(s), partie(s)
saillante(s), dégâts de grêle, différence(s) de teinte et
trace(s) de réparation.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux éléments de carrosserie de la face
avant</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur les éléments de carrosserie de la face
avant</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=200>
	<COL WIDTH=384>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Etat
			des éléments de carrosserie*</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><B>Côté
			gauche</B></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de la carrosserie côté gauche</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Aile avant gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Aile avant gauche n’est PAS coché</P>
			<P>Si
			«&nbsp;Aile avant gauche » est coché afficher ce qui est coché
			en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière avant gauche
			(y.c poigné &amp; coque de rétroviseur)</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Portière avant gauche n’est PAS coché</P>
			<P>Si
			«&nbsp;Portière avant gauche » est coché afficher ce qui est
			coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=16 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière arrière
			gauche (y.c poigné &amp; coque de rétroviseur)</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si «&nbsp;Aucun
			dégât constaté&nbsp;» est sélectionné ou si Portière
			arrière gauche n’est PAS coché</P>
			<P>Si
			«&nbsp;Portière arrière gauche » est coché afficher ce qui
			est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=15 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Aile arrière gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Aile arrière gauche n’est PAS coché</P>
			<P>Si
			«&nbsp;Aile arrière gauche » est coché afficher ce qui est
			coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de corrosion, choc(s)/déformation(s), partie(s)
saillante(s), dégâts de grêle, différence(s) de teinte et
trace(s) de réparation.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux éléments de carrosserie du côté
gauche</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur les éléments de carrosserie du côté
gauche</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=200>
	<COL WIDTH=384>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=10 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			des éléments de carrosserie*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de la carrosserie face arrière</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=11 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Face
			arrière</B></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pare-choc arrière</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Pare-chocs arrière n’est PAS coché</P>
			<P>Si
			«&nbsp;Pare-chocs arrière » est coché afficher ce qui est
			coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Coffre / hayon</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Coffre/hayon n’est PAS coché</P>
			<P>Si
			«&nbsp;Coffre/hayon » est coché afficher ce qui est coché en
			2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de corrosion, choc(s)/déformation(s), partie(s)
saillante(s), dégâts de grêle, différence(s) de teinte et
trace(s) de réparation.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) liée(s) aux éléments de carrosserie de la face
arrière</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur les éléments de carrosserie de la face
arrière</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=200>
	<COL WIDTH=384>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=10 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			des éléments de carrosserie*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de la carrosserie côté droit</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=11 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Côté
			droit</B></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Aile arrière droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Aile arrière droit n’est PAS coché</P>
			<P>Si
			«&nbsp;Aile arrière droit » est coché afficher ce qui est
			coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière arrière droit
			(y.c poigné &amp; coque de rétroviseur)</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si c’est
			sélectionné ou si Portière arrière droit n’est PAS coché</P>
			<P>Si
			«&nbsp;Portière arrière droit » est coché afficher ce qui est
			coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière avant droit
			(y.c poigné &amp; coque de rétroviseur)</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Portière avant droit n’est PAS coché</P>
			<P>Si
			«&nbsp;Portière avant droit » est coché afficher ce qui est
			coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Aile avant droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné ou si
			Aile avant droit n’est PAS coché</P>
			<P>Si
			«&nbsp;Aile avant droit » est coché afficher ce qui est coché
			en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de corrosion, choc(s)/déformation(s), partie(s)
saillante(s), dégâts de grêle, différence(s) de teinte et
trace(s) de réparation.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) aux éléments de carrosserie du côté
droit</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur les éléments de carrosserie du côté droit</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=221>
	<COL WIDTH=363>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=10 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Etat
			des éléments de carrosserie</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=11 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><B>Toit /
			capote</B></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=221 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Etat
			du toit (pavillon)*</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état du toit (pavillon)</P>
		</TD>
		<TD WIDTH=363 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dégât constaté » est sélectionné
			afficher ce qui est coché</P>
			<P>Affiché
			«&nbsp;Non concerné, voiture décapotable&nbsp;» si c’est
			sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=221 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Etat
			de la capote**</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez l’état de la capote</P>
		</TD>
		<TD WIDTH=363 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dégât constaté » est sélectionné
			afficher ce qui est coché</P>
			<P>Affiché
			«&nbsp;Non concerné, voiture décapotable&nbsp;» si c’est
			sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=221 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			de la capote</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de la capote</P>
		</TD>
		<TD WIDTH=363 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné&nbsp;» afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Vérification
de l’absence de corrosion, choc(s)/déformation(s), partie(s)
saillante(s), dégâts de grêle, différence(s) de teinte et
trace(s) de réparation.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>**Vérification
de l’absence de tâche(s), déchirure(s).</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) à la capote</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur la capote</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=193>
	<COL WIDTH=391>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=20 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Ajustement/alignement
			des éléments de carrosserie*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’ajustement/alignement des portières, du
			coffre/hayon etc</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Capot</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait&nbsp;» si «&nbsp;Les
			ajustements/alignements sont parfaits&nbsp;» est sélectionné ou
			si Capot n’est PAS coché.</P>
			<P>Si
			capot est coché afficher «&nbsp;Un ou plusieurs défaut(s)
			d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pare-chocs avant</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait&nbsp;» si «&nbsp;Les
			ajustements/alignements sont parfaits&nbsp;» est sélectionné ou
			si Pare-chocs avant n’est PAS coché.</P>
			<P>Si
			Pare-chocs avant est coché afficher «&nbsp;Un ou plusieurs
			défaut(s) d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière avant gauche</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait&nbsp;» si «&nbsp;Les
			ajustements/alignements sont parfaits&nbsp;» est sélectionné ou
			si Portière avant gauche n’est PAS coché.</P>
			<P>Si
			Portière avant gauche est coché afficher «&nbsp;Un ou plusieurs
			défaut(s) d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière arrière
			gauche</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait ou non concerné »
			si «&nbsp;Les ajustements/alignements sont parfaits&nbsp;» est
			sélectionné ou si Portière arrière gauche n’est PAS coché.</P>
			<P>Si
			Portière arrière gauche est coché afficher «&nbsp;Un ou
			plusieurs défaut(s) d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pare-chocs arrière</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait » si «&nbsp;Les
			ajustements/alignements sont parfaits&nbsp;» est sélectionné ou
			si Pare-chocs arrière n’est PAS coché.</P>
			<P>Si
			Pare-chocs arrière est coché afficher «&nbsp;Un ou plusieurs
			défaut(s) d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Coffre / hayon</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait » si «&nbsp;Les
			ajustements/alignements sont parfaits&nbsp;» est sélectionné ou
			si Coffre/hayon n’est PAS coché.</P>
			<P>Si
			Coffre/hayon est coché afficher «&nbsp;Un ou plusieurs défaut(s)
			d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière arrière droit</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait ou non concerné »
			si «&nbsp;Les ajustements/alignements sont parfaits&nbsp;» est
			sélectionné ou si Portière arrière droit n’est PAS coché.</P>
			<P>Si
			Portière arrière droit est coché afficher «&nbsp;Un ou
			plusieurs défaut(s) d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=193 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière avant droit</P>
		</TD>
		<TD WIDTH=391 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;L’ajustement/alignement est parfait » si «&nbsp;Les
			ajustements/alignements sont parfaits&nbsp;» est sélectionné ou
			si Portière avant droit n’est PAS coché.</P>
			<P>Si
			Portière avant droit est coché afficher «&nbsp;Un ou plusieurs
			défaut(s) d’ajustement(s)/alignement(s) constaté(s)&nbsp;»</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>*Le
contrôle de l’ajustement/alignement des éléments de carrosserie
se réalise toujours entre deux éléments de carrosserie (exemple
capot et aile avant gauche) et se doit d’être parfait.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) à l’ajustement/alignement des éléments
de carrosserie</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
défaut d’ajustement/alignement constatés sur les éléments
ci-dessus</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=200>
	<COL WIDTH=384>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=19 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Ouverture/fermeture
			des ouvrants</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement du capot, des portières et du
			coffre/hayon</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Capot</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné ou si Capot n’est PAS coché</P>
			<P>Si
			Capot est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière avant gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné ou si portière avant gauche n’est PAS coché.</P>
			<P>Si
			Portière avant gauche est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière arrière
			gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» est sélectionné
			ou si portière arrière gauche n’est PAS coché.</P>
			<P>Si
			Portière arrière gauche est coché alors afficher ce qui est
			coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Coffre / hayon</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné ou si coffre/hayon n’est PAS coché</P>
			<P>Si
			coffre/hayon est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière arrière droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté ou non concerné » si
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» est sélectionné
			ou si portière arrière droit n’est PAS coché.</P>
			<P>Si
			Portière arrière droit est coché alors afficher ce qui est
			coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Portière avant droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun défaut constaté » si c’est coché</P>
			<P>Si
			Portière avant droit est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
</TABLE>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>Carrosserie
&amp; vitrage (2)</B></SPAN></P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=200>
	<COL WIDTH=384>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=13 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>Etat
			du vitrage*</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état de l’ensemble du vitrage</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Pare-brise</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si «&nbsp;Aucun dégât
			constaté&nbsp;» est sélectionné ou si pare-brise n’est PAS
			coché.</P>
			<P>Si
			pare-brise est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Glace de custode avant
			gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si «&nbsp;Aucun
			dégât constaté&nbsp;» est sélectionné ou si Glace de custode
			avant gauche n’est PAS coché.</P>
			<P>Si
			Glace de custode avant gauche est coché alors afficher ce qui est
			coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Fenêtre de la portière
			avant gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si «&nbsp;Aucun dégât
			constaté&nbsp;» est sélectionné ou si Fenêtre avant gauche
			n’est PAS coché.</P>
			<P>Si
			Fenêtre avant gauche est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Fenêtre de la portière
			arrière gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si «&nbsp;Aucun
			dégât constaté&nbsp;» est sélectionné ou si Fenêtre arrière
			gauche n’est PAS coché.</P>
			<P>Si
			Fenêtre arrière gauche est coché alors afficher ce qui est
			coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Glace de custode arrière
			gauche</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si «&nbsp;Aucun
			dégât constaté&nbsp;» est sélectionné ou si Glace de custode
			arrière gauche n’est PAS coché.</P>
			<P>Si
			Glace de custode arrière gauche est coché alors afficher ce qui
			est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Lunette arrière</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si «&nbsp;Aucun dégât
			constaté&nbsp;» est sélectionné ou si Lunette arrière n’est
			PAS coché.</P>
			<P>Si
			Lunette arrière est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Glace de custode arrière
			droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si «&nbsp;Aucun
			dégât constaté&nbsp;» est sélectionné ou si Glace de custode
			arrière droit n’est PAS coché.</P>
			<P>Si
			Glace de custode arrière droit est coché alors afficher ce qui
			est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Fenêtre de la portière
			arrière droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si «&nbsp;Aucun
			dégât constaté&nbsp;» est sélectionné ou si Fenêtre arrière
			droit n’est PAS coché.</P>
			<P>Si
			Fenêtre arrière droit est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Fenêtre de la portière
			avant droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté » si «&nbsp;Aucun dégât
			constaté&nbsp;» est sélectionné ou si Fenêtre avant droit
			n’est PAS coché.</P>
			<P>Si
			Fenêtre avant droit est coché alors afficher ce qui est coché</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Glace de custode avant
			droit</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dégât constaté ou non concerné » si «&nbsp;Aucun
			dégât constaté&nbsp;» est sélectionné ou si Glace de custode
			avant droit n’est PAS coché.</P>
			<P>Si
			Glace de custode avant droit est coché alors afficher ce qui est
			coché</P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><SPAN LANG="fr-FR">*Vérification
de l’absence de rayure(s), fissure(s), impact(s) </SPAN><FONT SIZE=2>ou
de visibilité insuffisante.</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Constatation(s)
du/des dégât(s) lié(s) au vitrage</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
défaut d’ajustement/alignement constatés sur les éléments
ci-dessus</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=200>
	<COL WIDTH=384>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=13 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><SPAN STYLE="text-transform: uppercase"><B>fonctionnement
			du vitrage</B></SPAN></P>
			<P ALIGN=CENTER><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des vitres avant et arrières</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=200 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P>Vitres avant et arrière</P>
		</TD>
		<TD WIDTH=384 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P>Si
			«&nbsp;Un ou plusieurs dysfonctionnement constaté&nbsp;» est
			sélectionné&nbsp;» afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			ce qui est coché en 2<SUP>ème</SUP>
			indentation puis <FONT FACE="Wingdings, serif">
			ce qui est coché en 3<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
</TABLE>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>éQUIPEMENTS
&amp; Intérieur</B></SPAN></P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Habitacle
conducteur</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici la photographie de l’habitacle conducteur</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Habitacle
arrière</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici la photographie de l’habitacle arrière. Ne pas afficher s’il
n’y a aucune photographie</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=238>
	<COL WIDTH=346>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Etat
			du tableau de bord et de la console centrale</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=238 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Etat
			du levier de vitesse et de la console centrale</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Décrivez l’état du levier de vitesse, de la console centrale
			et des plastiques de tableau de bord</P>
		</TD>
		<TD WIDTH=346 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucune tâche ou détérioration constatée&nbsp;» si
			c’est sélectionné.</P>
			<P>Si
			«&nbsp;Une ou plusieurs tâches/détériorations constatées&nbsp;»
			est sélectionné afficher ce qui est coché puis <FONT FACE="Wingdings, serif">
			afficher ce qui est coché en 2<SUP>ème</SUP>
			indentation</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=238 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Présence
			d’odeur marquée (fumée, carburant, gaz d’échappement)</P>
			<P><FONT COLOR="#00b0f0"><FONT SIZE=2>PT.
			Dans l’habitacle y-a-t-il une odeur marquée désagréable</P>
		</TD>
		<TD WIDTH=346 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Si
			Oui est sélectionné afficher ce qui est coché.</P>
			<P>Afficher
			Non si c’est sélectionné</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in"><A NAME="_GoBack"></A>Constatation(s)
du/des dégât(s) lié(s) au tableau de bord et de la console
centrale</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici les photographies liées à la présence d’un ou plusieurs
dégâts constatés sur l’état des éléments ci-dessus</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<P STYLE="margin-bottom: 0in; text-transform: uppercase"><BR>
</P>
<TABLE WIDTH=614 CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=268>
	<COL WIDTH=316>
	<TR>
		<TD COLSPAN=2 WIDTH=598 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P ALIGN=CENTER><SPAN STYLE="text-transform: uppercase"><B>Fonctionnement
			des équipements</B></SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fermeture
			centralisée des portières et du coffre/hayon</P>
			<P ALIGN=JUSTIFY><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de la fermeture centralisée des
			portières et du coffre/hayon</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté » si c’est
			sélectionné</P>
			<P STYLE="margin-bottom: 0in">Affiché
			«&nbsp;Observation impossible&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Affiché
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Aucun ouvrant ne se ferme&nbsp;» est coché alors
			afficher «&nbsp;Aucun ouvrant ne se ferme&nbsp;»</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;porte avant gauche » est coché alors afficher ce qui est
			coché</P>
			<P>Pareil
			pour porte arrière gauche, coffre etc</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			des sièges avant chauffants</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement des sièges avant chauffants</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dysfonctionnements constatés&nbsp;» est
			sélectionné <FONT FACE="Wingdings, serif">
			afficher le titre en gras puis ce qui est sélectionné 
			</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			du volant chauffant</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement du volant chauffant</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dysfonctionnements constatés&nbsp;» est
			sélectionné afficher ce qui est sélectionné 
			</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné.</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			de l’ordinateur de bord</P>
			<P ALIGN=JUSTIFY><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de l’ordinateur de bord</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dysfonctionnements constatés&nbsp;» est
			sélectionné afficher ce qui est coché 
			</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			du chauffage</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement du chauffage</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dysfonctionnements constatés&nbsp;» est
			sélectionné afficher ce qui est coché 
			</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			de la climatisation</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de la climatisation</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Si
			«&nbsp;Un ou plusieurs dysfonctionnements constatés&nbsp;» est
			sélectionné afficher ce qui est coché 
			</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Fonctionnement
			du turbo</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement du turbo</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Afficher
			Ne fonctionne pas si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			Bruit excessif si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Régulateur
			de vitesse</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			décrivez le fonctionnement du régulateur de vitesse</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Afficher
			Ne fonctionne pas si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=24 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Compteur
			kilométrique</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement du compteur kilométrique</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P>Afficher
			Ne fonctionne pas si c’est sélectionné</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=268 HEIGHT=23 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Caméra
			de recul</P>
			<P><FONT COLOR="#0070c0"><FONT SIZE=2>PT.
			Décrivez le fonctionnement de la caméra de recul</P>
		</TD>
		<TD WIDTH=316 STYLE="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Aucun dysfonctionnement constaté&nbsp;» si c’est
			sélectionné.</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Ne fonctionne pas&nbsp;» si c’est sélectionné</P>
			<P STYLE="margin-bottom: 0in">Afficher
			«&nbsp;Mauvais fonctionnement&nbsp;» si c’est sélectionné</P>
			<P>Afficher
			«&nbsp;Non concerné&nbsp;» si c’est sélectionné</P>
		</TD>
	</TR>
</TABLE>
        
<footer style='text-align:center;'>
    <p>
        Autospot.ch<br>
        [page]/[sitepages]
    </p>
</footer>
        

<P ALIGN=CENTER STYLE="margin-bottom: 0.11in"><SPAN STYLE="text-transform: uppercase"><FONT SIZE=4><B>remarques</B></SPAN></P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Grâce
à Autospot, vous pouvez désormais prendre une décision d’achat
réfléchie et, selon les résultats obtenus dans ce rapport
d’inspection, entamer les démarches de négociations avec le
vendeur.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Bien
entendu, si vous souhaitez débuter les démarches de négociations
avec le vendeur, nous vous conseillons d’argumenter vos remarques
avec les points de contrôle qui ont présenté des dégâts ou
dysfonctionnement.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Si
ce rapport d’inspection ne révèle aucun dégât ou
dysfonctionnement, vous avez aussi la possibilité de faire une offre
au vendeur en déduisant le coût de cette inspection technique au
prix de vente de ce véhicule.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Si
l’on compare le cout de cette inspection technique (199.-) au prix
de vente moyen des voitures sur le marché de l’occasion
(20'000.-), cela correspond à une baisse d’environ 0.8% du prix de
vente.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Ce
vendeur a inséré son annonce en (afficher
la langue dans laquelle le vendeur a inséré son annonce).</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Pour
contacter/négocier avec un vendeur en langue étrangère, nous vous
conseillons de profiter gratuitement de notre plateforme de
discussions, disponible dans votre espace utilisateur sous «&nbsp;Suivi
de mes demandes&nbsp;». (A venir)</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P STYLE="margin-bottom: 0in">Si,
au contraire, vous pensez pouvoir converser facilement avec le
vendeur, nous vous transmettons, ci-dessous, ces coordonnées&nbsp;:</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici le nom et prénom du vendeur</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici le code postal et la localité du vendeur</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0in">Afficher
ici le numéro de téléphone du vendeur</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Si,
malheureusement, ce rapport d’inspection a révélé des éléments
suspects ou a soulevé des dégâts/dysfonctionnements importants et
que vous ne souhaitez pas acquérir ce véhicule, nous vous
conseillons d’avertir le vendeur de votre décision.</P>
<P ALIGN=JUSTIFY STYLE="margin-top: 0.08in; margin-bottom: 0in">Dans
ce cas, vous avez tout de même eu raison de souscrire à notre
forfait (afficher ici le nom du forfait) car cela vous a permis,<U>
avant l’achat,</U>
de prendre connaissance de ces dégâts/dysfonctionnement et d’éviter
ainsi des frais de réparations sur ce véhicule.</P>
<P STYLE="margin-bottom: 0in"><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Si
vous avez d’éventuelles remarques ou suggestions, n’hésitez pas
à nous les transmettre par email à l’adresse <A HREF="mailto:contact@autospot.ch">contact@autospot.ch</A>
ou directement depuis notre live chat (discussions en temps réel)
disponible sur notre site internet en bas à droite de l’écran.</P>
<P STYLE="margin-bottom: 0.11in; text-transform: uppercase"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.11in; text-transform: uppercase"><BR><BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.11in"><FONT SIZE=2 STYLE="font-size: 9pt">Autospot
et le garage partenaire qui a effectué cette inspection technique ne
saurait être tenu pour responsable ni d’un incident ultérieur à
l’examen, ni d’un vice antérieur non décelable lors de
l’inspection technique. Pour de plus amples informations
veuillez-vous référer à nos CGU, disponibles à l’adresse
<A HREF="https://www.autospot.ch/cgu">https://www.autospot.ch/cgu</A>

</P>
<DIV TYPE=FOOTER>
	<CENTER>
		<TABLE WIDTH=100% CELLPADDING=8 CELLSPACING=0>
			<COL WIDTH=128*>
			<COL WIDTH=128*>
			<TR VALIGN=TOP>
				<TD WIDTH=50% BGCOLOR="#5b9bd5" STYLE="border: none; padding: 0in">
					<P STYLE="text-transform: uppercase"><BR>
					</P>
				</TD>
				<TD WIDTH=50% BGCOLOR="#5b9bd5" STYLE="border: none; padding: 0in">
					<P ALIGN=RIGHT STYLE="text-transform: uppercase"><BR>
					</P>
				</TD>
			</TR>
			<TR>
				<TD WIDTH=50% BGCOLOR="#ffffff" STYLE="border: none; padding: 0in">
					<P><SPAN STYLE="text-transform: uppercase"><FONT COLOR="#808080"><FONT SIZE=2 STYLE="font-size: 9pt">Autospot.ch</SPAN></P>
				</TD>
				<TD WIDTH=50% BGCOLOR="#ffffff" STYLE="border: none; padding: 0in">
					<P ALIGN=RIGHT><SDFIELD TYPE=PAGE SUBTYPE=RANDOM FORMAT=PAGE></SDFIELD></P>
				</TD>
			</TR>
		</TABLE>
	</CENTER>
	<P STYLE="margin-bottom: 0in; line-height: 100%"><BR>
	</P>
</DIV>
</BODY>
</HTML>