<?php

require_once(ROOT.'classes/fpdf/fpdf.php');

function generate_facture($_filename, $dbh, $_iddemande, $_type)
{
	$lang = "fr";	//todo
	
	$dbh->query("set names utf8");
	$r = $dbh->prepare("SELECT `key`, `value` FROM online_translation2 WHERE lang=:lang");
	$r->execute(array(":lang" => $lang));

	$data_translations = array();
	while($tab = $r->fetch(PDO::FETCH_ASSOC)){
		//$data_translations[$tab['key']] = utf8_decode($tab['value']);
		$data_translations[$tab['key']] = utf8_decode($tab['value']);
	}
	
	
	$data = getDemandeExtended($dbh, $_iddemande);
	trace("demande extended");
	//var_dump($data);
	
	
	$date_now = date("d-m-Y", time());
	if($_type == "garage") $num_client = $data["garage_id"];
	else $num_client = $data["id_client_acheteur"];
	$ref = $_iddemande;
	
	$admin_name = utf8_decode("Skaiweb, mendonça");
	$admin_address = "Route de Lausanne 17";
	$admin_cpcity = "1610 Oron-la-Ville";
	$admin_tel = "+41 79/1061313";
	$admin_email = "contact@autospot.ch";
	$admin_website = "www.autospot.ch";
	
	
	
	
	$pdf = new FPDF();
	$pdf->SetFillColor(127, 204, 231);
	
	$doc_margin_left = 19;
	
	$pdf->AddPage();
	/* 
	$pdf->SetLeftMargin(10);
	$pdf->SetTopMargin(10);
	 */
	$pagewidth = round($pdf->GetPageWidth());
	$pageheight = round($pdf->GetPageHeight());
	trace("pagewidth : ".$pagewidth);
	
	$margin_left = 10;
	$margin_top = 6;
	
	$pdf->setX($margin_left);
	$pdf->setY($margin_top);
	$logo_width = 27;
	$pdf->Image(ROOT.'admin/images/logo-skaiweb.png', $margin_left - 2, $margin_top, $logo_width);
	$pdf->Ln(12);
	
	$interline = 10;
	
	$pdf->SetFont('Arial', 'B', 14);
	$pdf->Cell(0, $interline, $admin_name, 0, 2);
	
	
	//coordonn�es
	
	$interline = 5;
	$pdf->SetFont('Arial', '', 12);
	
	$pdf->Cell(0, $interline, $admin_address, 0, 2);
	$pdf->Cell(0, $interline, $admin_cpcity, 0, 2);
	if($_type == "garage") $pdf->Cell(0, $interline, $admin_tel, 0, 2);
	$pdf->Cell(0, $interline, $admin_email, 0, 2);
	
	$pdf->SetFont('Arial', 'U', 12);
	$pdf->SetTextColor(5, 99, 193);
	$pdf->Cell(0, $interline, $admin_website, 0, 2, '', false, "http://".$admin_website);
	$pdf->SetTextColor(0, 0, 0);
	
	
	//header right (FACTURE)
	
	$pdf->SetFont('Arial', '', 12);
	$pdf->setY(21);
	$pdf->setX($pagewidth - 75);
	
	$pdf->SetFont('Arial', 'B', 16);
	
	if($_type == "garage"){
		$pdf->Cell(75, 15, getTranslation("facture_title", $data_translations), 0, 1, 'C', true);
	}
	else{
		$pdf->MultiCell(75, 9, getTranslation("facture_title_client", $data_translations), 0, 'C', true);
	}
	
	
	$data_client = getDemandeExtended_clients($dbh, $_iddemande, "client");
	trace("client ::::");
	//var_dump($data_client);
	
	if($_type == "garage"){
		$text_dest1 = $data["garage_name"];
		$text_dest2 = $data["garage_address"]."\n".$data["garage_cp"]." ".$data["garage_city"];
	}
	else{
		$text_dest1 = utf8_decode($data_client["prenom"]." ".$data_client["nom"]);
		$text_dest2 = utf8_decode($data_client["adresse_num"]." ".$data_client["adresse"]."\n".$data_client["code_postal"]." ".$data_client["ville"]);
	}
	
	
	
	
	//garage name
	$y = 62;
	$ytext = $y - 9;
	
	$x = $pagewidth - 92;
	$xtext = $x + 8;
	
	$pdf->setY($y);
	$pdf->setX($x);
	$pdf->Cell(72, 27, '', 1, 1, '', false);
	
	$pdf->setY($ytext);
	$pdf->setX($xtext);
	$pdf->SetFont('Arial', 'B', 14);
	$pdf->Cell(72 - 8, 27, $text_dest1, 0, 1, '', false);
	
	$pdf->setY($ytext + 18);
	$pdf->setX($xtext);
	$pdf->SetFont('Arial', '', 12);
	$pdf->MultiCell(72 - 8, 5, $text_dest2, 0);
	
	
	//zone ref
	$x = $doc_margin_left;
	$y = 68;
	$xtext = $x - 7;
	$interline = 6;
	$pdf->setX($x); $pdf->setY($y);
	$pdf->Cell(54, 24, '', 1);
	
	$pdf->SetFont('Arial', '', 11);
	$pdf->setY($y + 2);
	$pdf->setX($xtext); 
	$pdf->Cell(54, $interline, getTranslation("facture_ref", $data_translations), 0, 1);
	$pdf->setX($xtext); 
	$pdf->Cell(54, $interline, getTranslation("facture_date", $data_translations), 0, 1);
	$pdf->setX($xtext); 
	$pdf->Cell(54, $interline, getTranslation("facture_num_client", $data_translations), 0, 1);
	
	$xtext = $x + 14;
	$pdf->SetFont('Arial', '', 8);
	$pdf->setY($y + 2.4);
	$pdf->setX($xtext); 
	$pdf->Cell(54, $interline, ':  '.$ref, 0, 1);
	$pdf->setX($xtext); 
	$pdf->Cell(54, $interline, ':  '.$date_now, 0, 1);
	$pdf->setX($xtext); 
	$pdf->Cell(54, $interline, ':  '.$num_client, 0, 1);
	
	
	
	$x = $doc_margin_left;
	$pdf->SetFont('Arial', '', 12);
	$pdf->setX($x); 
	$pdf->setY(105);
	
	
	$str = getTranslation("facture_intitule", $data_translations);
	$str = str_replace("{{date}}", $date_now, $str);
	
	$pdf->Cell(0, $interline, $str, 0, 1);
	
	//getTranslation("", $data_translations)
	
	//tableau
	$y = 120;
	$xspace = 0;
	
	$x = $doc_margin_left - 8;
	makeCell($pdf, $x, $y, 22, getTranslation("facture_quantity", $data_translations), "1", false, "C");
	$x += 22;
	
	$length = 60;
	$str = getTranslation("facture_designation", $data_translations);
	$str2 = getTranslation("facture_designation_value", $data_translations);
	$str2 = str_replace("{{length}}", $length, $str2);
	makeCell($pdf, $x, $y, 46, $str, $str2);
	$x += 46;
	
	$year = $data["year"];
	$km = $data["kilometrage"];
	$str = getTranslation("facture_carac", $data_translations);
	
	$str2 = $data["marque"].", ".$data["modele"]."\n".getTranslation("facture_year", $data_translations)." : ".$year."\n".getTranslation("facture_km", $data_translations)." : ".$km;
	makeCell($pdf, $x, $y, 62, $str, $str2);
	$x += 62;
	
	
	if($_type == "garage"){
		$tarif_ht = $data["tarif_horaire_ht"];
		$str = getTranslation("facture_tarif_horaire", $data_translations);
		$str2 = $tarif_ht."";
	}
	else{
		$str = getTranslation("facture_type_forfait", $data_translations);
		$str2 = $data["type_forfait"];
	}
	
	makeCell($pdf, $x, $y, 28, $str, $str2, false, "C");
	$x += 28;
	
	if($_type == "garage"){
		$total_ht = $tarif_ht * 1.0;
		$str = getTranslation("facture_prix_ht", $data_translations);
	}
	else{
		$total_ht = 184.77;
		$str = getTranslation("facture_prix_ht_client", $data_translations);
	}
	$str2 = $total_ht;
	
	
	makeCell($pdf, $x, $y, 21, $str, $str2, false, "C");
	$x += 21;
	
	
	//totaux
	
	$y = 150;
	$x = 121; 
	$width1 = 34.5;
	
	$pdf->setY($y);
	$pdf->SetFont('Arial', '', 11);
	$pdf->setX($x);
	$pdf->Cell($width1, 9.5, getTranslation("facture_total_ht", $data_translations), 1, 1);
	$pdf->setX($x);
	$str = getTranslation("facture_tva", $data_translations);
	$str = str_replace("{{x}}", 7.7, $str);
	
	$pdf->Cell($width1, 9.5, $str, 1, 1);
	$pdf->SetFont('Arial', 'B', 11);
	$pdf->setX($x);
	$pdf->Cell($width1, 9.5, getTranslation("facture_total_ttc", $data_translations), 1, 1);
	
	$x = $x + $width1;
	
	$tva_value = round($total_ht * 0.077, 2);
	$total_ttc = round($total_ht + $tva_value, 2);
	
	$pdf->setY($y);
	$pdf->SetFont('Arial', '', 11);
	$pdf->setX($x);
	$pdf->Cell($width1, 9.5, $total_ht.'.-', 1, 1, 'R');
	$pdf->setX($x);
	$pdf->Cell($width1, 9.5, $tva_value.'.-', 1, 1, 'R');
	$pdf->SetFont('Arial', 'B', 11);
	$pdf->setX($x);
	$pdf->Cell($width1, 9.5, $total_ttc.'.-', 1, 1, 'R');
	
	
	
	//conditions de paiement
	
	$x = $doc_margin_left;
	$pdf->SetFont('Arial', '', 12);
	$pdf->setX($x); 
	$pdf->setY(203);
	
	if($_type == "garage"){
		$str = getTranslation("facture_condition", $data_translations);
		$str = str_replace("{{iban}}", $data["iban"], $str);
	}
	else{
		$str = getTranslation("facture_aquite", $data_translations);
		$str = str_replace("{{date}}", $date_now, $str);
	}
	
	
	$pdf->Cell(0, $interline, $str, 0, 1);
	$pdf->setY(222);
	$pdf->Cell(0, $interline, getTranslation("facture_greeting", $data_translations), 0, 1);
	if($_type == "client"){
		$pdf->Cell(0, $interline, getTranslation("facture_greeting2", $data_translations), 0, 1);
	}
	
	
	//footer
	$num_registre = utf8_decode("N° IDE CHE-446.329.949");
	
	$x = 18;
	$width = 183;
	$pdf->SetFont('Arial', '', 6);
	$pdf->setX($x); 
	$pdf->setY($pageheight - 25);
	$pdf->Cell($width, 2, '', 0, 1, '', true);
	$pdf->setY($pageheight - 29);
	$pdf->Cell($width, 2, $num_registre, 0, 1, 'C', false);
	
	
	//18, 25 (offset)
	//164, 2 (dims)
	
	
	$pdf->Output(ROOT.$_filename,'F');
	
}


function makeCell($pdf, $x, $y, $width, $_texttop, $_textbottom, $_isitalic = true, $_alignbottom = "L")
{
	$pdf->SetFont('Arial', 'B', 10);
	
	
	$pdf->setY($y);
	$pdf->setX($x); 
	$pdf->MultiCell($width, 9, "", 0, '', true);
	
	
	$tab = explode("\n", $_texttop);
	$_nblinetop = count($tab);
	
	$pdf->setY($y + 2.5 - ($_nblinetop - 1) * 1.7);
	$pdf->setX($x); 
	$pdf->MultiCell($width, 4.2, $_texttop, 0, 'C', false);
	
	$pdf->setY($y);
	$pdf->setX($x); 
	$pdf->Cell($width, 24, '', 1);
	
	$format = ($_isitalic) ? "I" : "";
	$pdf->SetFont('Arial', $format, 10);
	
	$tab = explode("\n", $_textbottom);
	$nbline = count($tab);
	
	$pdf->setY($y + 9 + 5.5 - ($nbline - 1) * 2.2);
	$pdf->setX($x); 
	$pdf->MultiCell($width, 4.5, $_textbottom, 0, $_alignbottom, false);
	
}



function getTranslation($_key, $data_translations)
{
	if(!isset($data_translations[$_key])) return $_key;
	return $data_translations[$_key];
}



?>