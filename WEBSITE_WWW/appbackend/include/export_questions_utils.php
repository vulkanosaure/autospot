<?php


/* 
ini_set('xdebug.var_display_max_depth', 20);
ini_set('xdebug.var_display_max_children', 512);
ini_set('xdebug.var_display_max_data', 1024);
 */


$GLOBALS["TRACE_ENABLED"] = true;




function getTranslation($_key)
{
	global $data_translations;
	if(!isset($data_translations[$_key])) return $_key;
	return $data_translations[$_key];
}

$listskip = array();








//trace("__________________________________________________________________________________________");
//var_dump($data);


$output = "";

function exportData($_data)
{
	global $listskip;
	$counter = 0;
	global $output;
	
	foreach($_data as $_chapter){
		
		$listskip = array();
		
		$_listquestions = $_chapter["questions"];
		$_len = count($_listquestions);
		
		$_title = getTranslation($_chapter["title"]);
		
		if(isset($_chapter["sk_c"]) && $_chapter["sk_c"] == 1) continue;
		
		
		
		$output .= "<h2 class='chapter-title'";
		if($counter > 0){
			$output .=  " style='margin-top:60px;'";
		}
		$output .=  ">".$_title."</h2>";
		
		for($i=0; $i<$_len; $i++){
			
			//remplacé par "skiped" plus global
			//if(in_array($i, $listskip)) continue;	
			
			$question_parent = null;
			$question = $_listquestions[$i];
			
			if(isset($question["inst"]) && $question["inst"]) continue;
			if(isset($question["sk"]) && $question["sk"] == 1) continue;
			
			
			rec($question, $question_parent, 0, "", 0, null, false);
			
			$output .=  "<br />";
			
		}
		$counter++;
		
	}
	
	return $output;
}






function rec(&$_object, &$_objectParent, $_levelrec, $_iditem, $_index, $_func, $_islast)
{
	global $listskip;
	global $output;
	
	$isroot = ($_objectParent == null);
	
	//tracerec($_object["text"], $_levelrec);
	$output .= generateHTML($_object, $_levelrec, $_index, $isroot, $_islast);
	
	$_type = $_object["type"];
	
	$_value = isset($_object["value"]) ? $_object["value"] : "";
	
	
	
	$_continueBrowsing = true;
	if(!$_value && ($_type == "radio" || $_object["type"] == "checkbox")) $_continueBrowsing = false;
	
	
	
	
	if($_value && $_type == "radio" && isset($_object["skip"])){
		foreach($_object["skip"] as $sk) $listskip[] = $sk;
		//trace("skip add");
		//var_dump($listskip);
	}
	
	
	if ($_continueBrowsing && isset($_object["items"])) {
		
		$_len = count($_object["items"]);
		$_counter = 0;
		
		$_nextlevel = $_levelrec + 1;

		for ($i = 0; $i < $_len; $i++) {

			$_obj =& $_object["items"][$i];
			//$_iditem = $_idparent . $_counter;
			$_islast = ($i == $_len - 1);

			rec($_obj, $_object, $_nextlevel, $_iditem, $i, $_func, $_islast);
			$_counter++;

		}
	}
	
}



function generateHTML($_obj, $_level, $_index, $_isroot, $_islast)
{
	global $dataItem;
	
	$_type = $_obj["type"];
	$_text = $_obj["text"];
	$_text = str_replace("todo:", "", $_text);
	
	$isarea = (isset($_obj["textarea"]) && $_obj["textarea"]);
	
	$_value = isset($_obj["value"]) ? $_obj["value"] : "";
	if($isarea) $_value = nl2br($_value);
	
	//no need, done in js, than value is transmitted through value prop
	/* 
	if(isset($_obj["dyndata"])){
		$_dyndata = $_obj["dyndata"];
		//$_value = $dataItem[$_dyndata];
	}
	*/
	
	if(isset($_obj["format"])){
		$format = $_obj["format"];
		if($format == "number"){
			$_value = number_format(floatval($_value), 0, ".", " ");
		}
		//trace("format : ".$format);
	}
	
	$_margint = 0;
	$_marginb = 0;
	
	if($_index == 0){
		$_margint += 10;
	}
	
	if($_type == "picture"){
		$_margint += 10;
		$_marginb += 10;
	}
	if($_isroot) $_margint += 10;
	if($_islast && $_type == "radio") $_marginb += 10;
	if($isarea) $_marginb += 14;
	
	
	$_text = getTranslation($_text);
	
	$_elmt = "";
	if($_type == "label") $_elmt = "p";
	else if($_type == "radio") $_elmt = "li";
	else $_elmt = "div";
	
	$_listclass = array();
	if($_value && ($_type == "radio" || $_type == "checkbox")) $_listclass[] = "selected";
	
	$_classbase = $_type;
	if($_classbase == "text" || $_classbase == "number") $_classbase = "text_key";
	$_listclass[] = $_classbase;
	if($isarea) $_listclass[] = "label_textarea";
	
	
	$_str = "";
	$_tabs = "";
	for($i=0; $i<$_level; $i++) $_tabs .= "\t";
	
	
	$_str .= $_tabs."<".$_elmt;
	$_str .= " class='";
	$_str .= "level".$_level;
	
	foreach($_listclass as $_classname) $_str .= " ".$_classname;
	
	$_str .= "'style='";
	if($_margint != 0) $_str .= "margin-top:".$_margint."px;";
	if($_marginb != 0) $_str .= "margin-bottom:".$_marginb."px";
	$_str .= "'";
	$_str .= ">";
	$_str .= $_text;
	$_str .= "</".$_elmt.">";
	$_str .= "\n";
	
	if($_type == "picture" && $_value){
		
		foreach($_value as $_urlpic){
			$_str .= $_tabs."<img class='level".$_level." img-questions' src='img/".$_urlpic."' />\n";
		}
	}
	else if($_type == "text" || $_type == "number"){
		$_str .= $_tabs."<p class='level".$_level." text_value'>".$_value."</p>\n";
	}
	
	
	
	
	return $_str;
	
}






?>