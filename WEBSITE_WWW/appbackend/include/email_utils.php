<?php


require_once(ROOT.'../vendor/autoload.php');
require_once(ROOT."admin/include/constantes.php");
//require_once(ROOT."classes/phpmailer/class.phpmailer.php");
require_once(ROOT."classes/phpmailer/PHPMailerAutoload.php");
require_once(ROOT."classes/class.DataMailer.php");
require_once(ROOT."classes/CMSMS.php");
require_once(ROOT."include/time_utils.php");
require_once(ROOT."../classes/PHPMailer2.php");



define("EMAIL_SENDER", "contact@autospot.ch");
define("NAME_SENDER", "Autospot");
//define("EMAIL_ADMIN", "vincent.huss@gmail.com");   
define("EMAIL_ADMIN", "contact@autospot.ch");
define("NAME_ADMIN", "Inspection Autospot");

// define("EMAIL_DST_DEBUG", "vincent.huss@gmail.com");
// define("EMAIL_DST_DEBUG", "valentin@mendonca.ch"); 
define("EMAIL_DST_DEBUG", "");



// define("SMS_DST_DEBUG", "0041791061313");			//valentin
//define("SMS_DST_DEBUG", "00841203321615");		//moi vietnam
define("SMS_DST_DEBUG", ""); 


//BASE_URL (to WWW/)









//desttype = seller, client, garage, admin
//key, telle que présente ds onlinetranslation (avec prefix : mails_content_)

function handleEmails($dbh, $_key, $_desttype, $_iddemande, $_keyobject = "", $_filenameattach = NULL, $_idannounce = "", $_params = NULL)
{
	trace("___________________________________________");
	trace("handleEmails(".$_key.", ".$_desttype.", ".$_iddemande.")");
	
	
	if($_desttype == "admin"){
		$_lang = "fr";
		$_email = EMAIL_ADMIN;
		$_tel = "";
		$_name = NAME_ADMIN;
	}
	else{
		
		
		if($_iddemande != ""){
			
			if($_desttype == "garage"){
				$sql = "SELECT lang, email, name, tel_sms as tel, receive_sms FROM garages JOIN demandes_exam ON garages.id=demandes_exam.id_garage WHERE demandes_exam.id=:id_demande";
			}
			if($_desttype == "client"){
				$sql = "SELECT lang2 as lang, email, prenom, nom, telephone as tel FROM clients JOIN demandes_exam ON clients.id=demandes_exam.id_client_acheteur WHERE demandes_exam.id=:id_demande";
			}
			if($_desttype == "seller"){
				$sql = "SELECT lang2 as lang, email, prenom, nom, telephone as tel FROM clients JOIN demandes_exam ON clients.id=demandes_exam.id_client_vendeur WHERE demandes_exam.id=:id_demande";
			}
			
			$smth = $dbh->prepare($sql);
			$smth->bindParam(":id_demande", $_iddemande);
		}
		else{
			
			//seller only actually
			if($_desttype == "seller"){
				$sql = "SELECT lang2 as lang, email, prenom, nom, telephone as tel FROM annonces_clients JOIN clients ON clients.id=annonces_clients.id_client WHERE annonces_clients.id=:id_announce";
			}
			trace("sql : ".$sql);
			trace("_idannounce : ".$_idannounce);
			$smth = $dbh->prepare($sql);
			$smth->bindParam(":id_announce", $_idannounce);
		}
		
		
		$smth->execute();
		$tab = $smth->fetch(PDO::FETCH_ASSOC);
		//var_dump($tab);
		$_lang = $tab["lang"];
		$_email = $tab["email"];
		$_tel = $tab["tel"];
		
		if($_desttype == "garage"){
			$_name = $tab["name"];
		}
		else{
			$_name = $tab["prenom"]." ".$tab["nom"];
		}
		
		if($_desttype == "garage" && $tab["receive_sms"] == 0) $_tel = "";
		
	}
	
	if(EMAIL_DST_DEBUG != ""){
		$_email = EMAIL_DST_DEBUG;
		$_lang = "fr";
	}
	
	trace("tel(0) : ".$_tel);
	if(SMS_DST_DEBUG != ""){
		$_tel = SMS_DST_DEBUG;
		$_lang = "fr";
	}
	
	trace("_lang : ".$_lang);
	trace("_email : ".$_email);
	
	if($_keyobject == "") $_keyobject = "mails_object_".$_key;
	else $_keyobject = "mails_object_".$_keyobject;
	
	
	$content = getOnlineTranslation($dbh, "mails_content_".$_key, $_lang);
	$content_sms = getOnlineTranslation($dbh, "sms_content_".$_key, $_lang);
	
	
	$object = getOnlineTranslation($dbh, $_keyobject, $_lang);
	
	if($_iddemande != ""){
		
		$_delayTS = 0;
		if($_key != 'seller_rappel_rdv') $_delayTS -= getUTCshift();
		
		
		$info_extended = getDemandeExtended($dbh, $_iddemande, $_delayTS);
		$info_extended_client = getDemandeExtended_clients($dbh, $_iddemande, "client");
		$info_extended_seller = getDemandeExtended_clients($dbh, $_iddemande, "seller");
	}
	else{
		$info_extended = getAnnounceExtended($dbh, $_idannounce);
		$info_extended_seller = getDemandeExtended_clients_byAnnounce($dbh, $_idannounce);
	}
	
	/* 
	trace("info_extended : ");
	var_dump($info_extended);
	var_dump($info_extended_seller);
	 */
	
	//translation
	for($i=0; $i<2; $i++){
		preg_match_all("{{{tr:(\w+)}}}", $content, $matches);
		foreach($matches[1] as $_k){
			$_value = getOnlineTranslation($dbh, $_k, $_lang);
			//trace("value : ".$_value);
			$content = str_replace("{{tr:".$_k."}}", $_value, $content);
		}
	}
	//var_dump($matches);
	//$content = str_replace("{{tr:mails_content_client_annulation_0}}", "yo2", $content);
	
	
	
	
	
	
	//_________________________________________________________________________
	//email
	
	
	$content = str_replace("{{id_demande}}", $_iddemande, $content);
	$content = str_replace("{{id_announce}}", $_idannounce, $content);
	$object = str_replace("{{id_demande}}", $_iddemande, $object);
	$content = str_replace("{{brand}}", $info_extended["marque"], $content);
	$content = str_replace("{{model}}", $info_extended["modele"], $content);
	$content = str_replace("{{version}}", $info_extended["version"], $content);
	
	//template 
	if($_iddemande != ""){
		$content = str_replace("{{garage_name}}", $info_extended["garage_name"], $content);
		$content = str_replace("{{garage_address}}", $info_extended["address"], $content);
		$content = str_replace("{{garage_cp}}", $info_extended["cp_unique"], $content);
		$content = str_replace("{{garage_city}}", $info_extended["city"], $content);
		$content = str_replace("{{garage_tel}}", $info_extended["garage_tel"], $content);
		$forfait = $info_extended["type_forfait"];
		$content = str_replace("{{forfait}}", $forfait, $content);
		
		
		//not used
		$content = str_replace("{{time_demande_formatted}}", formatDate("time", $info_extended["demande_ts"]), $content);
		$content = str_replace("{{date_ct_formatted}}", formatDate("date", $info_extended["ct_ts"]), $content);
		$content = str_replace("{{time_ct_formatted}}", formatDate("time", $info_extended["ct_ts"]), $content);
		$content = str_replace("{{date_ct2_formatted}}", formatDate("date", $info_extended["ct_ts2"]), $content);
		$content = str_replace("{{time_ct2_formatted}}", formatDate("time", $info_extended["ct_ts2"]), $content);
		
		//used
		$content = str_replace("{{date_demande_formatted}}", formatDate("date", $info_extended["demande_ts"]), $content);
		
		$ct_ts_real = ($info_extended["ct_ts2"] != NULL) ? $info_extended["ct_ts2"] : $info_extended["ct_ts"];
		$content = str_replace("{{date_ct_real_formatted}}", formatDate("date", $ct_ts_real), $content);
		$content = str_replace("{{time_ct_real_formatted}}", formatDate("time", $ct_ts_real), $content);
		
		//horaires
		$horaires = $info_extended["horaires"];
		$horaires_formatted = formatHoraireGarage($dbh, $horaires, $_lang);
		$content = str_replace("{{garage_horaires}}", $horaires_formatted, $content);
		
		//info bancaire
		
		$_value = $info_extended["tarif_horaire_ht"];
		$_value = str_replace(",", ".", $_value);
		$_value = floatval($_value);
		$_value *= 1.077;
		$_value = round($_value, 2);
		$_value = str_replace(".", ",", $_value);
		
		$content = str_replace("{{amount}}", $_value, $content);
		$content = str_replace("{{num_compte}}", $info_extended["iban"], $content);
		$content = str_replace("{{currency}}", "CHF", $content);
		
		
	}
	
	
	

	$content = str_replace("{{car_year}}", $info_extended["year"], $content);
	$content = str_replace("{{car_kilometrage}}", $info_extended["kilometrage"], $content);
	$content = str_replace("{{car_carburant}}", $info_extended["carburant"], $content);
	$content = str_replace("{{car_cylindrees}}", $info_extended["cylindrees"], $content);
	$content = str_replace("{{car_carrosserie}}", $info_extended["carrosserie"], $content);
	
	
	//arg additionnal
	if($_params != NULL){
		foreach($_params as $k=>$v){
			$content = str_replace("{{".$k."}}", $v, $content);
		}
	}
	
	
	
	
	if(isset($info_extended_client)){
		$content = str_replace("{{client_surname}}", $info_extended_client["prenom"], $content);
		$content = str_replace("{{client_name}}", $info_extended_client["nom"], $content);
		$content = str_replace("{{client_cp}}", $info_extended_client["code_postal"], $content);
		$content = str_replace("{{client_city}}", $info_extended_client["ville"], $content);
	}
	
	if(isset($info_extended_seller)){
		$content = str_replace("{{seller_surname}}", $info_extended_seller["prenom"], $content);
		$content = str_replace("{{seller_name}}", $info_extended_seller["nom"], $content);
		$content = str_replace("{{seller_cp}}", $info_extended_seller["code_postal"], $content);
		$content = str_replace("{{seller_city}}", $info_extended_seller["ville"], $content);
		
		$_lang_announce = $info_extended_seller["lang2"];
		$lang_formatted = getOnlineTranslation($dbh, "lang_".$_lang_announce, $_lang);
		$content = str_replace("{{lang_formatted}}", $lang_formatted, $content);
	}
	
	
	
	
	
	
	$email_convert = array("contact@autospot.ch", "contact@autospot.ch");
	foreach($email_convert as $email){
		$content = str_replace(" ".$email, ' <a href="mailto:'.$email.'">'.$email.'</a>', $content);
	}
	
	
	
	trace("tel : ".$_tel);
	
	//link forward 
	
	$rootURL = BASE_URL."../../";
	
	$links_convert = array(
		"contact" => $rootURL."contact.php",
		"suivi" => $rootURL."compte.php?demandes",
		"espace_seller" => $rootURL."compte.php",
		"rapport" => $rootURL."",			//todo
		"edit_annonce" => $rootURL."",		//todo
	);
	foreach($links_convert as $k=>$v){
		$content = str_replace("href='".$k."'", "href='".$v."'", $content);
	}
	
	
	$colorlnk = "#009ada";
	$colorkeywords = "#009ada";
	
	//formatage
	$content = nl2br($content);
	
	$content = str_replace("<labelcenter>", "<font color='".$colorkeywords."'><center><b>", $content);
	$content = str_replace("</labelcenter>", "</b></center></font>", $content);
	
	$content = str_replace("<value>", "<font color='".$colorkeywords."'><b>", $content);
	$content = str_replace("</value>", "</b></font>", $content);
	
	$content = str_replace("<a ", "<a style='color:".$colorlnk.";'", $content);
	
	$content = str_replace("’", "'", $content);
	$object = str_replace("’", "'", $object);
	
	
	trace("object : ".$object);
	trace("name : ".$_name);
	
	//_________________________________________________________________________
	//sms
	if($content_sms != ""){
		
		//translation
		for($i=0; $i<2; $i++){
			preg_match_all("{{{tr:(\w+)}}}", $content_sms, $matches);
			foreach($matches[1] as $_key){
				$_value = getOnlineTranslation($dbh, $_key, $_lang);
				//trace("value : ".$_value);
				$content_sms = str_replace("{{tr:".$_key."}}", $_value, $content_sms);
			}
		}
		
		//votre prépaiement n'a pas été executé.
		//http://www.autospot.ch/rapport?id=1234
		//goo.gl/516ctC (https)
		$_urlsuivi = "goo.gl/oSFQra";				//http://www.autospot.ch/compte.php?demandes
		
		// $_urlrapport = "goo.gl/todo";				//http://www.autospot.ch/rapports/ + table annonces_securspot
		$rootShortUrl = BASE_URL;
		$tab =explode('/', $rootShortUrl);
		array_pop($tab);
		$rootShortUrl = implode('/', $tab);
		$ts = (isset($_params['ts'])) ? $_params['ts'] : '';
		$_urlrapport = $rootShortUrl . "/rapport.php?id=" . $ts;
		

		
		$content_sms = str_replace("{{id_demande}}", $_iddemande, $content_sms);
		$content_sms = str_replace("{{id_announce}}", $_idannounce, $content_sms);
		$content_sms = str_replace("{{short_url_suivi}}", $_urlsuivi, $content_sms);
		$content_sms = str_replace("{{short_url_rapport}}", $_urlrapport, $content_sms);
		
		$content_sms = str_replace("{{date_ct_formatted}}", formatDate("date", $info_extended["ct_ts"]), $content_sms);
		$content_sms = str_replace("{{time_ct_formatted}}", formatDate("time", $info_extended["ct_ts"]), $content_sms);
		$content_sms = str_replace("{{date_ct2_formatted}}", formatDate("date", $info_extended["ct_ts2"]), $content_sms);
		$content_sms = str_replace("{{time_ct2_formatted}}", formatDate("time", $info_extended["ct_ts2"]), $content_sms);
		
		$content_sms = str_replace("{{date_ct_real_formatted}}", formatDate("date", $ct_ts_real), $content_sms);
		$content_sms = str_replace("{{time_ct_real_formatted}}", formatDate("time", $ct_ts_real), $content_sms);
		
		$content_sms = str_replace("{{garage_name}}", $info_extended["garage_name"], $content_sms);
		$content_sms = str_replace("{{garage_address}}", $info_extended["address"], $content_sms);
		$content_sms = str_replace("{{garage_cp}}", $info_extended["cp_unique"], $content_sms);
		$content_sms = str_replace("{{garage_city}}", $info_extended["city"], $content_sms);
		$content_sms = str_replace("{{garage_tel}}", $info_extended["garage_tel"], $content_sms);
		
		$content_sms = str_replace("’", "'", $content_sms);
	}
	
	
	
	
	
	//handleEmails("sms", $dbh, "client_new_demande", "client", $id);
	
	//email
	
	$listdest = array(
		array("email" => $_email, "name" => $_name)
	);
	
	
	$replyTo = "";
	if($_key == "seller_demande_info" && $_desttype == "seller"){
		trace('ok must add replyTo');
		$replyTo = $_params['author_email'];
	}
	
	trace('_key : '.$_key);
	trace('_desttype : '.$_desttype);
	
	trace('replyTo : '.$replyTo);
	
	sendMail($listdest, $object, $content, $_filenameattach, $replyTo);
	
	//sms
	if($_tel != "" && $content_sms != ""){
		sendSMS($_tel, $object, $content_sms);
	}
	
}


function getOnlineTranslation($dbh, $key, $lang)
{
	$dbh->query("set names utf8");
	$smth = $dbh->prepare("SELECT `key`, `value` FROM online_translation2 WHERE `key`=:key AND lang=:lang");
	$smth->bindParam(":key", $key);
	$smth->bindParam(":lang", $lang);
	$smth->execute();
	$tab = $smth->fetch(PDO::FETCH_ASSOC);
	//var_dump($tab);
	return $tab["value"];
}



function formatDate($_type, $_ts)
{
	if($_type == "date"){
		$output = date("d-m-Y", $_ts);
	}
	else{
		$output = date("H:i", $_ts);
	}
	return $output;
}




function sendMail($listdest, $object, $content, $_filenameattach, $replytTo = "")
{
	// Encodage Hotmail
		$content = htmlspecialchars_decode(htmlentities($content, ENT_NOQUOTES, 'UTF-8'));
	// fin encodage Hotmail

	$sd = new dataMailer();
	$sd->setSender_email(EMAIL_SENDER);
	$sd->setSender_name(NAME_SENDER);
	
	if($replytTo != ""){
		trace('add replay to '.$replytTo);
		$sd->mailer->AddReplyTo($replytTo);
	}
	
	
	//$sd->setTpl("../mails/send.tpl");
	$sd->setTpl(BASE_URL."/mails/send.tpl");
	
	if($_filenameattach != NULL){
		
		foreach($_filenameattach as $f){
			
			$filename = $f;
			$sd->addAttachment(BASE_URL . "/" . $f, $filename, "base64", "application/pdf");
			
			/* 
			if (file_exists(BASE_URL."/".$f)){
				$sd->addAttachment(BASE_URL . "/" . $f, $filename, "base64", "application/pdf");
			}
			*/
			
		}
		
	}
	
	
	//trace(BASE_URL."/mails/send.tpl");
	
	foreach($listdest as $tab){
		$name = utf8_decode($tab["name"]);
		$sd->addDest_email($tab["email"], $name);
	}
	
	if($object == "") $object = "todo object";
	$sd->setObject_email($object);
	
	$sd->addVariable("title", $object);
	$sd->addVariable("dyn_content", $content);
	$sd->addVariable("path_server", BASE_URL);
	
	if(SEND_EMAIL) $sd->send();
	else{
		$sd->show();
		
	}
	//trace("________________________________________________________________________");
	trace("get _error : ".$sd->getMsgError());
	
}


function sendSMS($_tel, $_object, $_content)
{
	trace("sendSMS(".$_tel.", ".$_object.")");
	//trace("SEND_SMS : ".SEND_SMS);
	trace('_content : '.$_content);
	trace('SEND_SMS : '.SEND_SMS);
	
	$_content = utf8_decode($_content);	//new
	$_tel = transformTel($_tel);
	
	trace('_tel : '.$_tel);
	
	$sender = "Autospot.ch";
	if(SEND_SMS) $response = CMSMS::sendMessage($_tel, $sender, $_content);
	else if(isset($GLOBALS["TRACE_ENABLED"])) echo "<span style='color:red;'>".$_content."</span><br />";
	
	// var_dump($response);
}



//
function transformTel($str)
{
	$prefix = '0041';
	//
	$substr = substr($str, 0, strlen($prefix));
	if($substr != $prefix){
		
		$str = preg_replace('`^0`', $prefix, $str);
	}	
	return $str;
}



function formatHoraireGarage($dbh, $_horaire, $_lang)
{
	$output = "";
	$tabdays = explode("\r\n", $_horaire);
	$keywdays = ["wdays_mon", "wdays_tue", "wdays_wed", "wdays_thu", "wdays_fri", "wdays_sat", "wdays_sun"];
	$count= 0;
	
	foreach($tabdays as $strday){
		
		$key = $keywdays[$count];
		
		$day = getOnlineTranslation($dbh, $key, $_lang);
		$output .= "<b>".$day."</b> : ";
		
		if($strday == "-") $strday = getOnlineTranslation($dbh, "wdays_closed", $_lang);
		$output .= $strday;
		
		$output .= "\n";
		$count++;
		if($count > 6) break;
	}
	
	return $output;
}


// 
// VICTOR 
// 
function email_inspection_technique_terminee_to_vendeur($dbh, $_iddemande) {

	// On récupère les informations de l'annonce
	$smth = $dbh->prepare("SELECT * FROM demandes_exam WHERE `id`=:id");
	$smth->bindParam(":id", $_iddemande);
	$smth->execute();
	$demande = $smth->fetch(PDO::FETCH_ASSOC);
	
	// On récupère les informations sur l'annonce
	$smth = $dbh->prepare("SELECT marque, modele FROM annonces_clients WHERE `id`=:id");
	$smth->bindParam(":id", $demande['id_annonces_clients']);
	$smth->execute();
	$annonce = $smth->fetch(PDO::FETCH_ASSOC);

	// On récupère les informations sur le vendeur
	$smth = $dbh->prepare("SELECT * FROM clients WHERE `id`=:id");
	$smth->bindParam(":id", $demande['id_client_vendeur']);
	$smth->execute();
	$vendeur = $smth->fetch(PDO::FETCH_ASSOC);

	$sujet['fr'] = "Inspection technique terminée";
	$sujet = $sujet[ $vendeur['lang'] ];

	// On envoie l'email
	$loader = new \Twig_Loader_Filesystem(ROOT.'../emails');
    $twig = new \Twig_Environment($loader);
    $body = $twig->render('inspection_technique_terminee_to_vendeur.html.twig', [
        'vendeur' => $vendeur,
        'annonce' => $annonce,
    ]);

	$mail = new PHPMailer2();
    $mail->setFrom('contact@autospot.ch', 'AutoSpot');
    $mail->addAddress($vendeur['email']);
    // $mail->addAddress('victor.weiss.be@gmail.com'); // victor
    $mail->addReplyTo('no-reply@autospot.ch', 'Ne pas répondre');
    $mail->isHTML(true);
    $mail->Subject = $sujet;
    $mail->Body = $body;
    $mail->send();

    // On récupère le SMS
    $smth = $dbh->prepare("SELECT * FROM online_translation2 WHERE `key` = 'sms_content_vendeur_inspection_terminee' AND `lang` = '".$vendeur['lang']."'");
	$smth->execute();
	$translation = $smth->fetch(PDO::FETCH_ASSOC);
	$content = $translation['value'];

    // On envoie le SMS
    $sender = "Autospot.ch";
    $content = utf8_decode($content);
    CMSMS::sendMessage(transformTel($vendeur['telephone']), $sender, $content);
    // CMSMS::sendMessage('0033782579911', $sender, $content); // victor

	return;
}





?>