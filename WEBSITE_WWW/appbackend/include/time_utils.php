<?php


function execCron($dbh, $_ts, $_filterid)
{
	$limitrelance = (DEBUG_MODE) ? 1 : 4;
	$limitcancel = (DEBUG_MODE) ? 4 : 7;
	handleIddle($dbh, $_ts, $limitrelance, $limitcancel, $_filterid);
	handleRappel($dbh, $_ts, $_filterid);
	
}




//rappelle du rdv

function handleRappel($dbh, $_currenttime, $_filterid)
{
	trace("handleRappel()");
	//traceDB($dbh, "handleRappel()");
	$nbseclimit = 24 * 3600;
	$intervallimit = 60 * 5;
	
	
	$r = $dbh->query("SELECT *, UNIX_TIMESTAMP(ct_ts) as ts1, UNIX_TIMESTAMP(ct_ts2) as ts2 FROM demandes_exam WHERE status='' AND ct_ts IS NOT NULL AND relance=0");
	while($output = $r->fetch(PDO::FETCH_ASSOC)){
		$ts = $output["ts1"];
		if($output["ts2"] != null) $ts = $output["ts2"];
		
		//var_dump($output);
		$id = $output["id"];
		trace("try id ".$id);
		
		if($_filterid != "" && $_filterid != $id) continue;
		
		$delta = $ts - $_currenttime;
		
		trace("id : ".$id.", ts : ".$ts.", ".date("Y-m-d H:m", $ts).", delta :".$delta);
		//traceDB($dbh, "id : ".$id.", ts : ".$ts.", ".date("Y-m-d H:m", $ts).", delta :".$delta);
		
		if($delta <= $nbseclimit && $delta >= $nbseclimit - $intervallimit){
			
			traceDB($dbh, "rappel_rdv :: id : ".$id.", ts : ".$ts.", ".date("Y-m-d H:m", $ts).", delta :".$delta);
			setRelanceDemande($dbh, $id, 1);
			
			rappel_rdv($dbh, $id);
		}
	}
	
}




//relance puis cancel si pas de nouvelle du vendeur (ne rappelle pas le garage)

function handleIddle($dbh, $_currenttime, $_limitrelance, $_limitcancel, $_filterid)
{
	trace("handleIddle(".$_limitrelance.", ".$_limitcancel.")");
	//traceDB($dbh, "handleIddle(".$_limitrelance.", ".$_limitcancel.")");
	/*
	status == ''
	ct_ts != null (attention, chopper le 2 si existant)
	*/
	
	$r = $dbh->query("SELECT *, UNIX_TIMESTAMP(demande_ts) as ts FROM demandes_exam WHERE status='' AND ct_ts IS NULL AND accepted=1");
	while($output = $r->fetch(PDO::FETCH_ASSOC)){
		
		//var_dump($output);
		
		$id_demande = $output["id"];
		trace("try id ".$id_demande);
		
		if($_filterid != "" && $_filterid != $id_demande) continue;
		
		
		//new take garage accept time as base
		$suivi = $output['json_suivi'];
		$suivi = json_decode($suivi, true);
		var_dump($suivi);
		
		$_ts1 = $output["ts"];
		$index = count($suivi);
		while($index) {
			$tab = $suivi[--$index];
			if($tab['key'] == 'garage_accept_demande'){
				$_ts1 = $tab['ts'];
				break;
			}
		}
		trace('_ts1 : '.$_ts1);
		
		$difftime = getDifftime($dbh, $_ts1, $_currenttime, true, true);
		$relance = $output["relance"];
		
		trace("id : ".$id_demande.", difftime : ".$difftime.", relance : ".$relance);
		
		
		if($difftime >= $_limitcancel){
			trace("cancel");
			
			traceDB($dbh, "cancel :: id : ".$id_demande.", difftime : ".$difftime.", relance : ".$relance);
			seller_iddle_complete($dbh, $id_demande);
		}
		else if($relance != 1 && $difftime >= $_limitrelance){
			trace("relance");
			
			traceDB($dbh, "relance :: id : ".$id_demande.", difftime : ".$difftime.", relance : ".$relance);
			setRelanceDemande($dbh, $id_demande, 1);
			
			seller_iddle_relance($dbh, $id_demande);
			//column relance sert à la fois pour le rappel 24h avant, et pour la relance apres 4 jours
			//pas de conflit car selon que ct_ts est set ou non, on bascule dans l'un ou l'autre
			
		}
	}
	
}








function getDifftime($dbh, $_ts1, $_ts2, $_sat, $_sun)
{
	if($_ts1 > $_ts2) throw new Exception("ts1 must be inferior to ts2");
	
	$_daysec = 24 * 3600;
	$_ts = $_ts1;
	$_count = 0;
	
	$tabholidays = array();
	$r = $dbh->query("SELECT * FROM holidays");
	while($output = $r->fetch(PDO::FETCH_ASSOC)) $tabholidays[] = $output["date1"];
	
	trace("date init : ".date("Y-m-d", $_ts));
	trace("date end : ".date("Y-m-d", $_ts2));
	
	while(true){
		$available = isDayAvailable($dbh, $_ts, $tabholidays, $_sat, $_sun);
		
		$_ts += $_daysec;
		if($_ts > $_ts2) break;
		
		//trace("available : ".$available);
		if($available) $_count++;
	}
	return $_count;
}



function isDayAvailable($dbh, $_ts, $_tabholidays, $_sat, $_sun)
{
	$weekday = date("w", $_ts);		//0 for sunday, 6 for saturday
	$datestr = date("Y-m-d", $_ts);
	//trace("weekday : ".$weekday.", datestr : ".$datestr);
	
	if($_sat && $weekday == 6) return false;
	if($_sun && $weekday == 0) return false;
	
	if(in_array($datestr, $_tabholidays)){
		trace("- holidays : ".$datestr, "", 1);
		return false;
	}
	return true;
}



function getUTCshift()
{
	$date = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
	$output = $date->getOffset();
	return $output;
	
}




?>