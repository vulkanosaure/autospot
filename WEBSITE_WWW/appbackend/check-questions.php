<?php

require_once("admin/include/routines.php");
require_once("admin/include/db_connect.php");
require_once("admin/include/utils.php");
require_once("utils.php");

/* 
ini_set('xdebug.var_display_max_depth', 20);
ini_set('xdebug.var_display_max_children', 512);
ini_set('xdebug.var_display_max_data', 1024);
 */


$GLOBALS["TRACE_ENABLED"] = true;

$lang = "fr";


$dbh->query("set names utf8");
$r = $dbh->prepare("SELECT `key`, `value` FROM online_translation2 WHERE lang=:lang");
$r->execute(array(":lang" => $lang));

$data_translations = array();
while($tab = $r->fetch(PDO::FETCH_ASSOC)){
	//$data_translations[$tab['key']] = utf8_decode($tab['value']);
	$data_translations[$tab['key']] = $tab['value'];
}
//var_dump($data_translations);


function getTranslation($_key)
{
	global $data_translations;
	if(!isset($data_translations[$_key])) return "";
	return $data_translations[$_key];
}



/* 
$r = $dbh->prepare("SELECT * FROM demandes_exam WHERE id=:id");
$r->execute(array(":id" => $_GET["id_demande"]));
$tab = $r->fetch(PDO::FETCH_ASSOC);

//var_dump($tab);
$content = $tab["data_exam"];
 */
//echo $content;


$content = file_get_contents("data-export.txt");
//echo $content;


$data = json_decode($content, true);

?>

<link href="css/export-questions.css" rel="stylesheet">
<?php

exportData($data);




//trace("__________________________________________________________________________________________");
//var_dump($data);




function exportData($_data)
{
	$counter = 0;
	
	foreach($_data as $_chapter){
		
		
		$_listquestions = $_chapter["questions"];
		$_len = count($_listquestions);
		
		$_title = getTranslation($_chapter["title"]);
		
		
		for($i=0; $i<$_len; $i++){
			
			
			$question_parent = null;
			$question = $_listquestions[$i];
			
			rec($question, $question_parent, 0, "", 0, null, false);
			
			
		}
		$counter++;
		
	}
}



function checkKey($_object, $_key)
{
	$_key = isset($_object[$_key]) ? $_object[$_key] : "";
	
	if($_key == "") return;
	$_translation = getTranslation($_key);
	if($_translation == "") trace("no translation for '".$_key."'");
}




function rec(&$_object, &$_objectParent, $_levelrec, $_iditem, $_index, $_func, $_islast)
{
	
	$isroot = ($_objectParent == null);
	
	
	checkKey($_object, "text");
	checkKey($_object, "sub");
	
	
	
	
	$_continueBrowsing = true;
	
	
	
	if ($_continueBrowsing && isset($_object["items"])) {
		
		$_len = count($_object["items"]);
		$_counter = 0;
		
		$_nextlevel = $_levelrec + 1;

		for ($i = 0; $i < $_len; $i++) {

			$_obj =& $_object["items"][$i];
			//$_iditem = $_idparent . $_counter;
			$_islast = ($i == $_len - 1);

			rec($_obj, $_object, $_nextlevel, $_iditem, $i, $_func, $_islast);
			$_counter++;

		}
	}
	
}






?>