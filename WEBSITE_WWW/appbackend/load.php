<?php
//echo phpversion();
 /* 
$_GET["iduser"] = "13";
$_GET["test_mode"] = "1";
 */
require_once("admin/include/routines.php");
require_once('include/time_utils.php');

require_once("admin/include/db_connect.php");

require_once("admin/include/utils.php");
require_once("admin/include/math.php");


$GLOBALS["TRACE_ENABLED"] = true;
$output = array();

$test_mode = (isset($_GET["test_mode"]) && $_GET["test_mode"] == "1");


$dbh->query("set names utf8");
 
 
$sql = "SELECT ";
$sql .= "demandes_exam.id";
$sql .= ", demandes_exam.demande_ts";
$sql .= ", demandes_exam.ct_ts";
$sql .= ", demandes_exam.ct_ts2";
$sql .= ", demandes_exam.accepted";
$sql .= ", demandes_exam.status";
$sql .= ", vendeur.nom";
$sql .= ", vendeur.prenom";
$sql .= ", annonces_clients.codepostal";
$sql .= " FROM demandes_exam JOIN clients as vendeur ON id_client_vendeur=vendeur.id JOIN annonces_clients ON id_annonces_clients=annonces_clients.id WHERE ";

$filters = array();

if(!$test_mode){
	$filters[] = "complete='0'";
	$filters[] = "status=''";
	$filters[] = "payment_status='accepted'";
}



$filters[] = "id_garage='".$_GET["iduser"]."'";

$sql .= implode(" AND ", $filters);

$r = $dbh->query($sql);

$data = array();
while($tab = $r->fetch(PDO::FETCH_ASSOC)) $data[] = $tab;


foreach($data as $k=>$v){
	$data[$k]["demande_ts"] = strtotime($data[$k]["demande_ts"]);
	$data[$k]["ct_ts"] = strtotime($data[$k]["ct_ts"]);
	$data[$k]["ct_ts2"] = strtotime($data[$k]["ct_ts2"]);
	
	$data[$k]["demande_ts"] += getUTCshift();
	// if($data[$k]["ct_ts"] != null) $data[$k]["ct_ts"] += 3600 * 2;
	// if($data[$k]["ct_ts2"] != null) $data[$k]["ct_ts2"] += 3600 * 2;
	
}
//var_dump($data);



$output["list"] = $data;

//sleep(1);


require_once("admin/include/db_close.php");

echo json_encode($output);

?>