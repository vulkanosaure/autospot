<?php

require_once("admin/include/routines.php");
require_once("admin/include/db_connect.php");
require_once("admin/include/utils.php");
require_once("utils.php");
require_once("include/export_questions_utils.php");




//lang / translations

$lang = "fr";


$dbh->query("set names utf8");
$r = $dbh->prepare("SELECT `key`, `value` FROM online_translation2 WHERE lang=:lang");
$r->execute(array(":lang" => $lang));

$data_translations = array();
while($tab = $r->fetch(PDO::FETCH_ASSOC)){
	//$data_translations[$tab['key']] = utf8_decode($tab['value']);
	$data_translations[$tab['key']] = $tab['value'];
}
//var_dump($data_translations);






//export

$r = $dbh->prepare("SELECT * FROM demandes_exam WHERE id=:id");
$r->execute(array(":id" => $_GET["id_demande"]));
$tab = $r->fetch(PDO::FETCH_ASSOC);

$dataItem = getItem($_GET["id_demande"]);

//var_dump($tab);
$content = $tab["data_exam"];

//echo $content;
//$content = file_get_contents("data-export.txt");

$data = json_decode($content, true);
//var_dump($data);

?>

<link href="css/export-questions.css" rel="stylesheet">
<?php

$contentHTML = exportData($data);
echo $contentHTML;


?>