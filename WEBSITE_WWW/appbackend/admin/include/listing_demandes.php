<?php

require_once("../events_utils.php");
require_once("../include/time_utils.php");


$id = "demandes_exam";
$title = "Demandes d'examen";


echo "<h2>".$title."</h2>";



echo "<div id='listing_line'>";


/* 
echo '<span class="btn_add" class="btn_clickable" onclick="javascript:onClickAdd(\''.$id.'\')"><img src="images/icon-new-file.png" /><span class="btnlink">Ajouter un garage</span></span>';
echo '<br />';
echo '<div class="break"></div>';
 */

/*
nom, rue et num�ro, CP, localit�, email, tel, mdp
*/

echo '<table class="listing" style="width:100%;">';

echo '<tr class="header">';

	echo '<td style="width:20px;">ID</td>';
	
	
	echo '<td style="width:40px;">Référence<br />Paiement</td>';
	echo '<td style="width:40px;">Numéro<br />Paiement</td>';

	echo '<td style="width:40px;">Date</td>';
	
	echo '<td style="width:40px;">Heure</td>';
	
	
	echo '<td></td>';
	/* 
	echo "<td style='width:40px;'></td>";
	echo "<td style='width:40px;'></td>";
	 */
	

echo '</tr>';


$tab_convert_event = array(
	"new_demande" => "",
	"garage_set_time1" => "Le garage a défini la date et l'heure du rendez-vous : <span class='value'>{{date}}</span> à <span class='value'>{{time}}</span>",
	"garage_set_time2" => "Le garage a reporté l'heure du rendez-vous : <span class='value'>{{date}}</span> à <span class='value'>{{time}}</span>",
	"garage_unavailable" => "Demande annulée : Aucun garage n'est disponible pour traiter la demande",
	"garage_refuse_demande" => "Le garage <span class='value'>{{garage_name}}</span> a refusé la demande",
	"garage_accept_demande" => "Le garage <span class='value'>{{garage_name}}</span> a accepté la demande",
	"rappel_rdv" => "",
	"seller_iddle_relance" => "Une relance à été envoyée au vendeur après 4 jours d'inactivité",
	"seller_iddle_complete" => "Demande annulée : Le vendeur n'a pas donné de nouvelles depuis 7 jours",
	"garage_declare_absent" => "Demande annulée : Le vendeur était absent le jour du rendez vous",
	"garage_declare_sold" => "Demande annulée : Le véhicule a été vendu entretemps",
	"garage_declare_error" => "Demande annulée : Problème technique (wifi ou tablette tactile)",
	"delete_announce" => "Demande annulée : L'annonce a été supprimée",
	"exam_complete" => "Inspection technique terminée : <a href='../{{filename}}' target='_blank'>Ouvrir le rapport</a>",
);





$r = $dbh->query("SELECT *, UNIX_TIMESTAMP(demande_ts) as demande_ts FROM ".$id." WHERE payment_status='accepted' ORDER BY demande_ts DESC");
//var_dump($dbh->errorInfo());

//while($obj = mysql_fetch_object($r)){
while($obj = $r->fetch(PDO::FETCH_OBJ)){
	$class_tr = "unpair";

	echo '<tr class="'.$class_tr.'" style="height:40px;">';
	
		$info = json_decode($obj->payment_info, true);
		// var_dump($info);
	
		$date = formatDate("date", $obj->demande_ts);
		$time = formatDate("time", $obj->demande_ts);
		
		echo '<td class="cell_demande_key">'.$obj->id.'</td>';
		
		
		echo '<td style="width:50px;" class="cell_demande_key">'.$info['NUMXKP'].'</td>';
		echo '<td style="width:50px;" class="cell_demande_key">'.$info['TRANSACTIONID'].'</td>';
		
		echo '<td style="width:90px;" class="cell_demande_key">'.$date.'</td>';
		echo '<td style="width:50px;" class="cell_demande_key">'.$time.'</td>';
		
		
		echo '<td class="cell_demande_key"></td>';
		
		$json = $obj->json_suivi;
		
		
		
		if($json != ""){
			$tab = json_decode($json, true);
			$counter_planning = 0;
			foreach($tab as $obj2){
				
				$ts = $obj2["ts"];
				// $ts -= getUTCshift();
				
				$key = $obj2["key"];
				$class_tr = ($counter_planning % 2 == 0) ? "pair" : "pair2";
				
				$date = formatDate("date", $ts);
				$time = formatDate("time", $ts);
				
				trace("key : ".$key);
				
				
				$labelkey = $tab_convert_event[$key];
				if($labelkey == "") continue;
				
				
				//19-06-2017 15:47
				if(isset($obj2["params"])){
					$p = $obj2["params"];
					if(isset($p["ts"])){
						
						$p["ts"] -= getUTCshift();
						$labelkey = str_replace("{{date}}", formatDate("date", $p["ts"]), $labelkey);
						$labelkey = str_replace("{{time}}", formatDate("time", $p["ts"]), $labelkey);
						
					}
					if(isset($p["id_garage"])){
						$info_garage = getGarage($dbh, $p["id_garage"]);
						$labelkey = str_replace("{{garage_name}}",$info_garage["name"], $labelkey);
						
					}
					if(isset($p["filename"])){
						$labelkey = str_replace("{{filename}}",$p["filename"], $labelkey);
						$labelkey = str_replace("{{id_vendeur}}",$obj->id_client_vendeur, $labelkey);
						
					}
					
				}
				
				
				echo "<tr class='".$class_tr."'>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo '<td style="width:90px;">'.$date.'</td>';
				echo '<td style="width:50px;">'.$time.'</td>';
				echo '<td style="text-align:left;">'.$labelkey.'</td>';
				
				$counter_planning++;
			}
			
		}

		/*
		echo '<td>'.$obj->horaires.'</td>';
		echo '<td>'.$obj->list_holidays.'</td>';
		*/
		
		/* 
		echo '<td><img class="btn_clickable" src="images/icon-modifier.png" onclick="javascript:onClickEdit(\''.$id.'\', '.$obj->id.')" /></td>';
		echo '<td><img class="btn_clickable" src="images/icon-supp.gif" onclick="javascript:onClickDelete(\''.$id.'\', '.$obj->id.')" /></td>';
		 */

	echo '</tr>';

	

}

echo '</table>';

echo "</div>";


function formatDate($_type, $_ts)
{
	if($_type == "date"){
		$output = date("d-m-Y", $_ts);
	}
	else{
		$output = date("H:i", $_ts);
	}
	return $output;
}

?>