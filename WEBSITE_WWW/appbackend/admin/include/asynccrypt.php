<?php


class AsyncCrypt{
	
	private static $ENCRYPTION_KEY = "kd3js9zK2pchW5kQ";
	
	
	
	static public function encrypt(&$_obj, $_tabkey)
	{
		$_tabvalues = array();
		$_len = count($_tabkey);
		for ($i = 0; $i < $_len; $i++) $_tabvalues[] = $_obj[$_tabkey[$i]];
		//foreach($_tabvalues as $val) trace("val : ".$val);
		//
		
		$_hash = hash("sha256", implode(":", $_tabvalues) . ":" . AsyncCrypt::$ENCRYPTION_KEY);
		$_obj["verifier"] = $_hash;
	}
	
	
	static public function isValid($_obj, $_tabkey)
	{
		$_keyencrypt = "verifier";
		if(!isset($_obj[$_keyencrypt])){
			return false;
		}
		else{
			$_tabvalues = array();
			$_len = count($_tabkey);
			for ($i = 0; $i < $_len; $i++) $_tabvalues[] = $_obj[$_tabkey[$i]];
			//foreach($_tabvalues as $val) trace("val : ".$val);
			
			$_hash = hash("sha256", implode(":", $_tabvalues) . ":" . AsyncCrypt::$ENCRYPTION_KEY);
			
			if($_obj[$_keyencrypt] == $_hash) return true;
			else return false;
		}
	}
	
}

?>