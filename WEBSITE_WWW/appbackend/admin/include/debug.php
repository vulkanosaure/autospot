<?php


function dirToArray($dir) {
   $result = array();
   
   $cdir = scandir($dir);
   foreach ($cdir as $key => $value)
   {
      if (!in_array($value,array(".","..")))
      {
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) { 
			$result[] = $value;
		 }
         else
         {
            $result[] = $value;
         }
      }
   }
  
   return $result;
} 


if(!function_exists("trace")){
	
	function trace($_msg, $_color="")
	{
		if(!isset($GLOBALS["TRACE_ENABLED"]) || !$GLOBALS["TRACE_ENABLED"]) return;
		
		echo "<font class='debug_trace'";
		if($_color != "") echo " color='".$_color."'";
		echo ">".$_msg."</font><br />";
	}
}



//pour tracer du server vers le client
$server_output = "";
function traceServer($_msg)
{
	global $server_output;
	$server_output .= " / ".$_msg;
}


function pt2str($_pt)
{
	return "[x : ".$_pt[0].", y : ".$_pt[1]."]";
}

?>