<?php

function random2($min, $max, $inc=1)
{
	$rand = rand(0, 99) / 100;
	$retour = floor2($rand * ($max-$min+$inc) + $min, $inc);
	return $retour;
}


function round2($nb, $inc=1)
{
	$retour = round($nb/$inc) * $inc;
	return $retour;
}

function floor2($nb, $inc=1)
{
	$retour = floor($nb/$inc) * $inc;
	return $retour;
}
function getRandProbability($_frequency)
{
	if ($_frequency <= 0) return false;
	$_denom = round2((1 / $_frequency), 0.1);
	$_rand = random2(0, $_denom, 0.1);
	return ($_rand < 1);
}


function getAverage($_valueA, $_valueB, $_coeffA, $_coeffB) 
{
	$_total = $_valueA * $_coeffA + $_valueB * $_coeffB;
	$_divide = $_coeffA + $_coeffB;
	return $_total / $_divide;
}

function getProgressionValue($_input, $_minsrc, $_maxsrc, $_mindst, $_maxdst)
{
	$_percentinput = ($_input - $_minsrc) / ($_maxsrc - $_minsrc);
	if ($_percentinput < 0) $_percentinput = 0;
	if ($_percentinput > 1) $_percentinput = 1;
	$_output = $_mindst + ($_maxdst - $_mindst) * $_percentinput;
	return $_output;
}







//___________________________________________________________________________________
//b�zier


function quadraticFunction($_p0, $_p1, $_p2, $_t) 
{
	$_output;
	$_output = $_p0 * pow(1 - $_t, 2) + 2 * $_p1 * $_t * (1 - $_t) + pow($_t, 2) * $_p2;
	return $_output;
}

function cubicFunction($_p0, $_p1, $_p2, $_p3, $_t) 
{
	$_output;
	$_output = $_p0 * pow(1 - $_t, 3) + 3 * $_p1 * $_t * pow(1 - $_t, 2) + 3 * $_p2 * pow($_t, 2) * (1 - $_t) + $_p3 * pow($_t, 3);
	return $_output;
}


?>