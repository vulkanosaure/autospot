﻿<?php

//require_once("../../php/constantes.php");

header('Content-Type: application/json; charset=UTF-8');

require_once("../../include/db_connect.php");
require_once("../../include/debug.php");
require_once("../../include/math.php");


$dbh->query("set names utf8");

$query = "SELECT * FROM _lang";
$r_lang = $dbh->query($query);


$DEBUG = false;


$data = array();
$lang_export = (isset($_GET["lang"])) ? $_GET["lang"] : "";


while($tab_langs = $r_lang->fetch(PDO::FETCH_ASSOC)){
	
	$code = $tab_langs["code"];
	$datalang = array();
	
	$r_src = $dbh->query("SELECT * FROM online_translation2 WHERE lang='".$code."' AND useapp=1");
	
	if($lang_export != "" && $code != $lang_export) continue;

	while($tab = $r_src->fetch(PDO::FETCH_ASSOC)){
		
		$key = $tab["key"];
		$datalang[$key] = $tab["value"];
		
		
		//echo $tab["key"]."<br />";
		$datalang[$key] = str_replace("\n", "\\n", $datalang[$key]);
		
		if(!in_array($key, ['question_essais_routier_0_a'])){
			$datalang[$key] = str_replace("<br />", "&lt;br /&gt;", $datalang[$key]);
		}
		
	}
	
	
	$data[$code] = $datalang;
}



if($DEBUG){
	
	$keyinit = "fr";
	$tab_init = $data[$keyinit];
	foreach($data as $lang=>$v){
		
		if($lang != $keyinit){
			foreach($tab_init as $k=>$v){
				$data[$lang][$k] = $v."-".$lang;
			}
			
		}
	}
	
}

//$displaySeparation = 

foreach($data as $k=>$v){
	
	
	echo json_encode($v);
}
//echo json_encode($data);


require_once("../../include/db_close.php");

?>