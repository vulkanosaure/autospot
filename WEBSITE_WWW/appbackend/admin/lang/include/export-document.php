<?php

//require_once("../../php/constantes.php");

require_once("../../../master/include/db_connect.php");
require_once("../../../master/include/debug.php");
require_once("../../../master/include/db_utils.php");
require_once("../../../master/include/math.php");
require_once("../classes/PHPRtfLite.php");




/* 
$_GET["lang_src"] = "en";
$_GET["lang_dst"] = "fr";
$_GET["groups"] = "0,2";
 */

$groups = explode(",", $_GET["groups"]);
$name_output = "rtf/citymonkey-".$_GET["lang_src"]."-".$_GET["lang_dst"]."_gr".implode("-", $groups).".rtf";



PHPRtfLite::registerAutoloader();

$rtf = new PHPRtfLite();
$rtf->setMarginLeft(0);
$rtf->setMarginRight(0);
$rtf->setMarginTop(0);
$rtf->setMarginBottom(0);
$rtf->setCharset('UTF-8');



// add section
$sect = $rtf->addSection();
$sect->setMarginLeft(0);
$sect->setMarginRight(0);
$sect->setMarginTop(0);
$sect->setMarginBottom(0);


$parformat = new PHPRtfLite_ParFormat();

$font_key = new PHPRtfLite_Font(6, 'Arial', '#999999', '#0000FF');
$font_key->setBold();

$font_desc = new PHPRtfLite_Font(8, 'Arial', '#67Ae59', '#0000FF');
$font_desc->setItalic();

$font_key_tr = new PHPRtfLite_Font(10, 'Arial', '#00a4ff', '#0000FF');
$font_key_tr->setBold();

$font_value = new PHPRtfLite_Font(10, 'Arial', '#000000', '#0000FF');




//__________________________________________________________________________________________________

mysql_query("set names utf8");

$r_en = mysql_query("SELECT * FROM online_translation2 WHERE lang='en'");
$content_en_all = array();
while($tab = mysql_fetch_array($r_en)){
	$key = $tab["key"];
	$content_en_all[$key] = $tab;
}



$r_src = mysql_query("SELECT * FROM online_translation2 WHERE lang='".$_GET["lang_src"]."' ORDER BY _group, `key` ASC");
$content_src = array();
while($tab = mysql_fetch_array($r_src)){
	$key = $tab["key"];
	$content_src[$key] = $tab;
}

$r_dst = mysql_query("SELECT * FROM online_translation2 WHERE lang='".$_GET["lang_dst"]."'");
$content_dst = array();
while($tab = mysql_fetch_array($r_dst)){
	$key = $tab["key"];
	$content_dst[$key] = $tab;
}





foreach($content_src as $k=>$obj_src){
	
	$obj_all = $content_en_all[$k];
	$group = $obj_all["_group"];
	
	$v = $obj_src["value"];
	
	$quality = (isset($content_dst[$k])) ? $content_dst[$k]["quality"] : 0;
	
	
	if(in_array($group, $groups) && $quality==0){
		
		//todo : faudra garder ou pas cette value en fonction du level de sa traduction
		
		
		$value = (isset($content_dst[$k])) ? $content_dst[$k]["value"] : "";
		$desc = $obj_src["_desc"];
		
		
		$v = str_replace ("///", " / ", $v);
		$value = str_replace ("///", " / ", $value);
		
		//replace les []
		$v = str_replace("[x]", "[5]", $v); $value = str_replace("[x]", "[5]", $value);
		$v = str_replace("[n]", "[name]", $v); $value = str_replace("[n]", "[name]", $value);
		$v = str_replace("[i]", "[object]", $v); $value = str_replace("[i]", "[object]", $value);
		$v = str_replace("[score1]", "10", $v); $value = str_replace("[score1]", "10", $value);
		$v = str_replace("[score2]", "5", $v); $value = str_replace("[score1]", "5", $value);
		$v = str_replace("[n1]", "[name 1]", $v); $value = str_replace("[n1]", "[name 1]", $value);
		$v = str_replace("[n2]", "[name 2]", $v); $value = str_replace("[n2]", "[name 2]", $value);
		
		//for screenshot
		$v = preg_replace("`<(/?)[woy]>`", "", $v);
		
		
		addContent($k, $font_key);
		
		//addContent($v, $font_key_tr);
		if($desc != "") addContent("(".$desc.")", $font_desc);
		
		addContent($value, $font_value);
		addContent("", $font_value);
		
	}
	
	
}
addContent("", $font_key);



// save rtf document to hello_world.rtf
$rtf->save($name_output);


header('Content-Description: File Transfer');
header('Content-type: application/rtf');
header("Content-Type: application/force-download");// some browsers need this
header("Content-Disposition: attachment; filename=\"".$name_output."\"");
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($name_output));
readfile($name_output);







//_____________________________________________________

function addContent($_str, $font)
{
	global $sect;
	global $parformat;
	$sect->writeText($_str, $font, $parformat);
}






require_once("../../../../master/include/db_close.php");

?>