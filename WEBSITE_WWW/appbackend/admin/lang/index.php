<?php

//require_once("../php/constantes.php");

require_once("../include/routines.php");
require_once("../include/db_connect.php");
require_once("../include/debug.php");
require_once("../include/math.php");

/* 
GROUPS
0 - le max serait les textes de share facebook (car courts en +)
1 - help
2 - la description store
3 - les captures d��cran
4 - les textes critiques du shop
5 - le reste
*/

define("DEFAULT_LANG", "fr");

$title = "Lang - AutoSpot";
$NB_GROUP = 7;
$TAB_FILTER_GROUP_LANG2 = array("mails_content_admin_", "mails_object_admin_");

$lang_filter = (isset($_GET["lang"])) ? $_GET["lang"] : DEFAULT_LANG;
//trace("lang_filter : ".$lang_filter);

$dbh->query("set names utf8");


?>

<!DOCTYPE html>
<html lang="en">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
	<title><?php echo $title;?></title>
	<link href="css/adming-lang.css" rel="stylesheet" type="text/css" />
	
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
	<!--<script src="../../js/jquery.js"></script>-->
	
	<script type="text/javascript">
	function copyToClipboard(text) {
		window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
	}
	</script>
</head>
<body>

<?php







//INSERT / UPDATE TRANSLATION

if(isset($_POST["id"])){
	
	$id = $_POST["id"];
	$lang = $_POST["lang"];
	$key = $_POST["key"];
	$value = addslashes($_POST["value"]);
	
	//$quality = $_POST["quality"];
	$quality = 0;
	$group = (isset($_POST["group"])) ? $_POST["group"] : '';
	$appcheck = (isset($_POST["appcheck"]) && $_POST["appcheck"] == 1) ? "1" : "0";
	$useapp = (isset($_POST["useapp"]) && $_POST["useapp"] == 1) ? "1" : "0";
	$desc = (isset($_POST["desc"])) ? $_POST["desc"] : "";
	
	//trace("appcheck : ".$appcheck);
	
	if($id == ""){
		$query = "INSERT INTO online_translation2 SET lang='".$lang."', `key`='".$key."', value='".$value."', quality='".$quality."', _group='".$group."', appcheck='".$appcheck."', useapp='".$useapp."', _desc='".$desc."'";
		$dbh->query($query);
	}else{
		$query = "UPDATE online_translation2 SET value='".$value."', quality='".$quality."', _group='".$group."', appcheck='".$appcheck."', _desc='".$desc."' WHERE id='".$id."'";
		//trace($query);
		$dbh->query($query);
	}
}

//INSERT NEW LANG

else if(isset($_POST["new_lang_code"])){
	
	$new_lang_code = (isset($_POST["new_lang_code"])) ? $_POST["new_lang_code"] : "";
	$new_lang_name = (isset($_POST["new_lang_name"])) ? $_POST["new_lang_name"] : "";
	
	if($new_lang_code == "" || $new_lang_name == ""){
		echo "<div class='error'>Fill all fields</div>";
	}
	else{
		$dbh->query("INSERT INTO _lang SET code='".$new_lang_code."', name='".$new_lang_name."'");
		echo "<div class='validation'>Lang added</div>";
	}
	
}

//INSERT NEW TRANSLATION

else if(isset($_POST["new_translation_key"])){
	
	$new_translation_key = (isset($_POST["new_translation_key"])) ? $_POST["new_translation_key"] : "";
	$new_translation_value = (isset($_POST["new_translation_value"])) ? $_POST["new_translation_value"] : "";
	$new_translation_group = (isset($_POST["new_translation_group"])) ? $_POST["new_translation_group"] : "";
	
	$new_translation_value = addslashes($new_translation_value);
	
	//new 
	if($new_translation_key == ""){
		$new_translation_key = "ec04".generateRandomKey(4);
		$copypaste = "\"".$new_translation_key."\"";
		echo "<h2>".$copypaste."</h2>";
		//"(todo:)?    "
		
		echo "<script type='text/javascript'>copyToClipboard('".$copypaste."');</script>";
	}
	
	if($new_translation_key == ""){
		echo "<div class='error'>Fill at least key</div>";
	}
	else{
		$r = $dbh->query("SELECT * FROM online_translation2 WHERE lang='".DEFAULT_LANG."' and `key`='".$new_translation_key."'");
		if($r->rowCount() > 0) echo "<div class='error'>This key already exists</div>";
		else{
			$sql = "INSERT INTO online_translation2 SET lang='".DEFAULT_LANG."', `key`='".$new_translation_key."', value='".$new_translation_value."', _group='".$new_translation_group."', useapp=1";
			trace($sql);
			if(!$dbh->query($sql)){
				print_r($dbh->errorInfo());
			}
			
		}
	}
	
}

//EXPORT LANG

else if(isset($_POST["export_lang"])){
	
	$export_lang = $_POST["export_lang"];
	trace("todo export lang ".$export_lang);
	
	$group_selected = array();
	for($i=0; $i<$NB_GROUP;$i++){
		if(isset($_POST["group_".$i])) $group_selected[] = $i;
	}
	//var_dump($group_selected);
	
	$lang_src = $_POST["lang_src"];
	$groups_str = implode(",", $group_selected);
	
	//add param get
	header("Location:include/export-document.php?lang_src=".$lang_src."&lang_dst=".$export_lang."&groups=".$groups_str);
	
}

//DELETE
else if(isset($_GET["delete_key"])){
	
	//trace("delete key : ".$_GET["delete_key"]);
	$dbh->query("DELETE from online_translation2 WHERE `key`='".$_GET["delete_key"]."'");
	echo "<div class='validation'>Key '".$_GET["delete_key"]."' deleted</div>";
	
	
}


function generateRandomKey($_len)
{
	$dict = "abcdefghijklmnopqrstuvwxyz0123456789";
	$lendict = strlen($dict);
	$output = "";
	for($i=0; $i<$_len; $i++){
		$index = rand(0, $lendict - 1);
		$output .= $dict[$index];
	}
	return $output;
}



//SELECTION DU CONTENU


$r_lang = $dbh->query("SELECT * FROM _lang");
$r_src = $dbh->query("SELECT * FROM online_translation2 WHERE lang='fr' ORDER BY _group, `key` ASC");


$list_keys = array();
$assoc_keys = array();

while($tab = $r_src->fetch(PDO::FETCH_ASSOC)){
	$list_keys[] = $tab;
	$assoc_keys[$tab["key"]] = $tab;
}

//WARNING FR
$r_fr_check = $dbh->query("SELECT * FROM online_translation2 WHERE lang='fr'");
$list_check_fr = array();
while($tab = $r_fr_check->fetch(PDO::FETCH_ASSOC)){
	$list_check_fr[$tab["key"]] = $tab;
}

$missing_keys_fr = array();
foreach($list_keys as $obj_src){
	$key = $obj_src["key"];
	if(!isset($list_check_fr[$key]) || $list_check_fr[$key]["value"] == "") $missing_keys_fr[] = $key;
}
if(count($missing_keys_fr) > 0) echo "<div class='error'>Missing fr keys : ".implode(", ", $missing_keys_fr)."</div>";





//_____________________________________________________________________________
//DISPLAY



//calculate ratio finished

$ratios = array();
$ratios_appcheck = array();
$r_lang2 = $dbh->query("SELECT * FROM _lang");


while($tab_lang = $r_lang2->fetch(PDO::FETCH_ASSOC)){
	$code = $tab_lang["code"];
	$ratios[$code] = array();
	$ratios_appcheck[$code] = array();
	for($i=0; $i<$NB_GROUP; $i++){
		$ratios[$code][] = 0;
		$ratios_appcheck[$code][] = 0;
	}
}

$query_all = $dbh->query("SELECT * FROM online_translation2 WHERE quality > 0");

while($obj = $query_all->fetch(PDO::FETCH_ASSOC)){
	
	if($obj["value"] != "" && $obj["quality"] > 0){
		
		$key = $obj["key"];
		$group = $assoc_keys[$key]["_group"];
		
		$code = $obj["lang"];
		$ratios[$code][$group]++;
		//trace("code : ".$code);
		
		if($obj["appcheck"]==1) $ratios_appcheck[$code][$group]++;
	}
	
}


function getRatio($_code, $_group, $_ratios)
{
	$_nbtotal = $_ratios[DEFAULT_LANG][$_group];
	$_nb = $_ratios[$_code][$_group];
	if($_nbtotal == 0) return 0;
	return $_nb / $_nbtotal;
}
function getRatioTotal($_code, $_ratios, $ratiomodel)
{
	global $NB_GROUP;
	$_nbtotal = 0;
	for($i=0; $i<$NB_GROUP; $i++) $_nbtotal += $ratiomodel[DEFAULT_LANG][$i];
	$_nb = 0;
	for($i=0; $i<$NB_GROUP; $i++) $_nb += $_ratios[$_code][$i];
	if($_nbtotal == 0) return 0;
	return $_nb / $_nbtotal;
}







//LISTING langs

echo "<h2>Liste des langues</h2>";

$r_listing_all = $dbh->query("SELECT * FROM online_translation2 WHERE lang='fr'");
$nb_total = $r_listing_all->rowCount();

$r_listing_lang = $dbh->query("SELECT * FROM _lang");
echo "<table style='border-spacing:0px;'>";
$count = 0;
while($tab = $r_listing_lang->fetch(PDO::FETCH_ASSOC)){
	
	$bgcolor = ($count%2==0) ? "333333" : "666666";
	
	echo "<form action='' method='POST'>";
	echo "<tr style='height:25px;background-color:#".$bgcolor."'>";
	
	$isLangSelected = ($lang_filter == $tab["code"]);
	
	echo "<td style='padding:10px;'>";
	if($isLangSelected) echo "<span class='percent'>";
	echo $tab["code"];
	if($isLangSelected) echo "</span>";
	echo "</td>";
	
	echo "<td style='padding:10px;'>";
	if($isLangSelected) echo "<span class='percent'>";
	echo $tab["name"];
	if($isLangSelected) echo "</span>";
	echo "</td>";
	echo "<td style='padding:10px;'><a href='?lang=".$tab["code"]."'>Afficher</a></td>";
	
	
	echo "<td>";
	if($tab["code"] != "fr"){
		$rtest = $dbh->query("SELECT * FROM online_translation2 WHERE lang='".$tab["code"]."' AND value != ''");
		$nbtr = $rtest->rowCount();
		echo $nbtr."/".$nb_total;
		
	}
	echo "</td>";
	
	
	echo "<td style='padding:10px;'>";
	echo "<a href='include/admin-lang-export-json.php?lang=".$tab["code"]."'>json</a>";
	echo "</td>";
	
	echo "</tr>";
	echo "</form>";
	
	$count++;
	
}
echo "</table>";
echo "<br />";



//FORM add language
/* 
echo "<form action='' method='POST'>";
echo "<h2>Add new lang</h2>";
echo "Code (minuscules) : <input name='new_lang_code' type='text'></input>&nbsp;&nbsp;&nbsp;&nbsp;";
echo "Name : <input name='new_lang_name' type='text'></input>&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<input type='submit' value='Add' />";
echo "</form>";

echo "<br /><br />";
 */


//FORM add new key

echo "<form action='' method='POST'>";
echo "<h2>Nouvelle traduction</h2>";
echo "key : <input name='new_translation_key' type='text'></input>&nbsp;&nbsp;&nbsp;&nbsp;";
echo "value (".DEFAULT_LANG.") : <input name='new_translation_value' type='text'></input>&nbsp;&nbsp;&nbsp;&nbsp;";
echo "group : <input name='new_translation_group' type='text' value='facture'></input>&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<input type='submit' value='Add' />";
echo "</form>";

echo "<br /><br />";



/* 
//btn get sync local
echo "<a href='include/admin-lang-export-json.php'>EXPORT JSON</a><br /><br />";
 */



/* 
//btn get sync local
echo "<form action='' method='post'>";
echo "<input type='hidden' name='export_servers' value='1' />";
echo "<input type='submit' value='Export servers' />";
//echo "<a href='?export_servers=1'>EXPORT ALL SERVER</a><br /><br />";
echo "</form>";
 */

while($tab_lang = $r_lang->fetch(PDO::FETCH_ASSOC)){
	//trace("_____________________________________");
	//trace("lang : ".$tab_lang["name"]);
	
	$lang_name = $tab_lang["name"];
	$lang = $tab_lang["code"];
	
	$tab_specific_lang = array();
	foreach($list_keys as $obj_src){
		$key = $obj_src["key"];
		$tab = array("key"=>$key);
		$tab_specific_lang[$key] = $tab;
	}
	
	$r = $dbh->query("SELECT * FROM online_translation2 WHERE lang='".$lang."'");
	
	while($tab = $r->fetch(PDO::FETCH_ASSOC)){
		$key = $tab["key"];
		$tab_specific_lang[$key] = $tab;
	}
	
	if($lang_filter == "" || $lang_filter == $lang){
		
		$isMainLang = ($lang_filter == DEFAULT_LANG);
		
		echo "<div style='width:100%;background-color:#00cdff'><h1>".$lang_name."</h1></div>";
		
		echo "<table>";
		
		$counter = 0;
		$savegroup = -1;
		foreach($list_keys as $obj_src){
			
			$key = $obj_src["key"];
			$pair = ($counter % 2 == 0);
			
			if(!$isMainLang){
				$_breakloop = false;
				foreach($TAB_FILTER_GROUP_LANG2 as $_f){
					$_len = strlen($_f);
					$_substr = substr($key, 0, $_len);
					//trace("_substr : ".$_substr);
					if($_substr == $_f){
						$_breakloop = true;
						continue;
					}
				}
				if($_breakloop){
					continue;
				}
			}
			
			
			
			$isMailContent = (substr($key, 0, 14) == "mails_content_");
			$isSMSContent = (substr($key, 0, 12) == "sms_content_");
			
			
			$obj = $tab_specific_lang[$key];
			$value = (isset($obj["value"])) ? $obj["value"] : "";
			$id = (isset($obj["id"])) ? $obj["id"] : "";
			$id_ancre = $lang."/".$key;
			
			$colorline = ($pair) ? "black" : "#333333";
			
			if($obj_src["_group"] != $savegroup){
				echo "<tr style='height:10px;background-color:#00cbdf;'><td colspan='10'><h3 style='color:white;'>".$obj_src["_group"]."</h3></td></tr>";
				$savegroup = $obj_src["_group"];
			}
			
			echo "<tr id='".$id_ancre."' style='background-color:".$colorline.";'>";
			echo "<form action='?lang=".$tab_lang["code"]."#".$id_ancre."' method='POST'>";
			
			//key
			echo "<td style='padding-right:10px;max-width:180px;word-wrap:break-word;'>";
			echo "<font color='#888888'>".$key."</font>";
			echo "</td>";
			
			//value src
			/* 
			echo "<td style='width:220px;'>";
			echo nl2br($obj_src["value"]);
			echo "</td>";
			 */
			
			if($isSMSContent) $heightTextArea = 100;
			else if($isMailContent) $heightTextArea = 300;
			else{
				$tabtest = explode("\n", $value);
				$nbline = count($tabtest);
				$heightTextArea = 16 * $nbline;
			}
			
			$bgcolor = ($value == "") ? "#FFAAAA" : "white";
			if($value == ""){
				$value = $obj_src["value"];
			}
			
			//value
			echo "<td width='460'>";
			echo "<textarea name='value' style='width:460px;height:".$heightTextArea."px;background-color:".$bgcolor.";' type='text'>";
			echo $value;
			echo "</textarea>";
			echo "</td>";
			
			/* 
			//desc
			if($lang=="en" || $lang=="fr"){
				
				$desc = (isset($obj["_desc"])) ? $obj["_desc"] : "";
				echo "<td width='150'>";
				echo "<textarea name='desc' style='width:150px;height:16px;' type='text'>";
				echo $desc;
				echo "</textarea>";
				echo "</td>";
			}
			 */
			//group
			if($lang==DEFAULT_LANG){
				echo "<td>";
				echo "Group ";
				/* 
				echo "<select name='group'>";
				for($i=0; $i<$NB_GROUP;$i++){
					$selected_str = ($obj_src["_group"] == $i) ? " selected" : "";
					echo "<option value='".$i."'".$selected_str.">".$i."</option>";	//todo selected
				}
				echo "</select>";
				 */
				$group = (isset($obj["_group"])) ? $obj["_group"] : "";
				echo "<textarea name='group' style='width:80px;height:16px;' type='text'>";
				echo $group;
				echo "</textarea>";
				echo "</td>";
			}
			
			$spacer = "&nbsp;&nbsp;";
			/* 
			//use app
			if($lang==DEFAULT_LANG){
				echo "<td>";
				echo $spacer."useapp ";
				$checked_str = ($obj["useapp"]==1) ? " checked" : "";
				echo "<input name='useapp' value='1'".$checked_str." type='checkbox' />";
				echo "</td>";
			}
			 */
			 
			 /* 
			//app check
			echo "<td>";
			echo $spacer."appcheck ";
			if(!isset($obj["appcheck"])) $obj["appcheck"] = 0;
			$checked_str = ($obj["appcheck"]==1) ? " checked" : "";
			echo "<input name='appcheck' value='1'".$checked_str." type='checkbox' />";
			echo "</td>";
			 */
			 
			 /* 
			//quality
			echo "<td width='100'>";
			echo $spacer."quality ";
			echo "<select name='quality'>";
			if(!isset($obj["quality"])) $obj["quality"] = 0;
			
			$nbquality = 2;
			for($i=0; $i<$nbquality;$i++){
				$selected_str = ($obj["quality"]==$i) ? " selected" : "";
				echo "<option value='".$i."' ".$selected_str.">".$i."</option>";
			}
			
			echo "</select>";
			echo "</td>";
			 */
			
			
			
			
			//hidden inputs
			echo "<input type='hidden' name='id' value='".$id."' />";
			echo "<input type='hidden' name='lang' value='".$lang."' />";
			echo "<input type='hidden' name='key' value='".$key."' />";
			
			
			//btn valid
			echo "<td width='100'>";
			$value_submit = "Update";
			echo "<input type='submit' value='".$value_submit."' />";
			echo "</td>";
			
			
			//delete
			if($lang==DEFAULT_LANG){
				echo "<td>";
				echo "<a href='?delete_key=".$key."' style='color:red;'>delete</a>";
				echo "</td>";
			}
			
			
			echo "</form>";
			echo "</tr>";
			
			$counter++;
			
		}
		echo "</table>";
	}
	
}





require_once("../include/db_close.php");
?>

</body>