<?php


require_once("include/routines.php");
require_once("include/db_connect.php");

require_once("include/utils.php");
require_once("include/auth.php");



?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"

 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>

	<title>Administration - AutoSpot</title>

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="initial-scale=0.7, maximum-scale=0.7, user-scalable=no, width=device-width">

	<link rel="shortcut icon" href="images/ico.ico" />

	

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

	

	

	<script type="text/javascript" src="js/main.js"></script>

	<link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />

	

	<script type='text/javascript' src='js/jquery.scrollTo-min.js'></script>

	<script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>

	<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

	<script type="text/javascript" src="js/JSON.js"></script>
	<script src="js/dateformat/dist/dateformat.min.js"></script>

	<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

	

	<script type="text/javascript">

		var noUpdateAfterCloseBox = false;

	</script>

</head>





<body>

<span id="up" class="btnlink"><img src="images/row-up.png"> <a href="#main">Haut de la page</a></span>



<?php



if(!isset($_GET["page"])) $_GET["page"] = "garages";




$action = (!isset($_GET["action"])) ? "" : $_GET["action"];
//echo("action : ".$action);

if($action == "delete"){
	
	$id = $_GET["id"];
	
	
	if($_GET["page"] == "garages"){
		
		$smth = $dbh->prepare("SELECT * FROM garages WHERE id=:id");
		$smth->bindParam(":id", $id);
		$smth->execute();
		$obj = $smth->fetch(PDO::FETCH_ASSOC);
		$id_client = $obj["id_client"];
		
		$smth = $dbh->prepare("DELETE FROM clients WHERE id=:id");
		$smth->bindParam(":id", $id_client);
		$smth->execute();
	}
	
	
	$smth = $dbh->prepare("DELETE FROM ".$_GET["page"]." WHERE id=:id");
	$smth->bindParam(":id", $id);
	$smth->execute();
	
	
	
}




echo "<div id='main'>";

echo "<img src='images/logo.png' style='float:left;margin-top:-10px;width:250px;margin-left:-130px;' />";

echo "<h1>Administration</h1>";



require_once("menu.php");

echo "<br /><br /><br />";







if($_GET["page"]=="garages") require_once("include/listing.php");
else if($_GET["page"]=="demandes_exam") require_once("include/listing_demandes.php");
else if($_GET["page"]=="holidays") require_once("include/listing_holidays.php");
else if($_GET["page"]=="annonces_clients") require_once("include/listing_annonces_clients.php");
else if($_GET["page"]=="cp2garage") require_once("include/listing_cp.php");
else if($_GET["page"]=="tests") require_once("include/tests.php");
else if($_GET["page"]=="factures_garages") require_once("include/listing_factures.php");
else if($_GET["page"]=="factures_clients") require_once("include/listing_factures.php");



?>

<br /><br /><br /><br />

</div>







<a id="hidden_link" style="display:none;">hidden link</a>

<script type="text/javascript" src="js/onload.js"></script>

</body>