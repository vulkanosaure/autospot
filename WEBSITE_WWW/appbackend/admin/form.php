<?php



//GET : labels, tablename.column

//array are separated with ___ (3)


$selectable_key = array();
$selectable_key["color_text"] = array("Blanc___B", "Noir___N");
$selectable_key["valide"] = array("Oui___1", "Non___0");
$selectable_key["receive_sms"] = array("Oui___1", "Non___0");
$selectable_key["actif"] = array("Oui___1", "Non___0");
$selectable_key["inspection_impossible"] = array("Oui___1", "Non___0");




$GLOBALS["TRACE_ENABLED"] = true;


foreach($_POST as $k=>$v) $_POST[$k] = stripslashes($v);

require_once("include/db_connect.php");
require_once("include/utils.php");



$mode = $_GET["mode"];


$values = array();
$values_display = array();
$columns = explode("___", $_GET["columns"]);
$labels = explode("___", $_GET["labels"]);
$types = explode("___", $_GET["types"]);

//trace("ount :".count($columns).", ".count($types));


if($mode=="create"){
	foreach($columns as $col){
		$values[$col] = "";

	}
}

else if($mode=="edit"){

	$id = $_GET["id"];
	//trace("id : ".$id);
	$query = "SELECT * FROM ".$_GET["tablename"]." WHERE id='".$id."'";

	//trace("query : ".$query);

	$r = $dbh->query($query);
	//$obj = mysql_fetch_array($r);
	$obj = $r->fetch(PDO::FETCH_ASSOC);

	

	foreach($columns as $col){
		$values[$col] = $obj[$col];
		$values[$col] = utf8_encode($values[$col]);
	}
	

}
else exit("wrong value for mode");







if(isset($_POST["valid"])){

	foreach($columns as $col){

		//echo "col : ".$col.", value : ".$_POST[$col]."<br />";
		$values[$col] = $_POST[$col];

		if(substr($col, 0, 4) == "date"){
			//convert from DD-MM-YYYY to YYYY-MM-DD
			$tab = explode("-", $values[$col]);
			$values[$col] = $tab[2]."-".$tab[1]."-".$tab[0];
		}
		
		$values[$col] = utf8_decode($values[$col]);
	}

	$bError = false;
	$bErrorMsg = "";

	//check si tout est rempli
	/* 
	foreach($columns as $col){
	
		if($_POST[$col]==""){
			$bError = true;
			$bErrorMsg = "Veuillez remplir tous les champs";
		}
	}
	*/
	
	if(!$bError){

		if($mode=="create"){
			
			$tablename = $_GET["tablename"];

			$query = "INSERT INTO ".$tablename." SET ";

			$columns2 = array();
			foreach($columns as $c) $columns2[] = $c."=:".$c;
			$query .= implode(", ", $columns2);
			trace($query);

			$smth = $dbh->prepare($query);
			foreach($columns as $c) $smth->bindParam(":".$c, $values[$c]);

			$smth->execute();
			var_dump($dbh->errorInfo());
			
			
			
			//else exit("todo tablename ".$tablename);
			
			//trace("query : ".$query);
			
			
			if($tablename == "garages"){
				
				$id_garage = $dbh->lastInsertId();
				
				
				$query = "INSERT INTO clients SET ";
				$query .= "sid=:sid";
				$query .= ", lang=:lang";
				$query .= ", lang2=:lang";
				$query .= ", date=NOW()";
				$query .= ", type=1";
				$query .= ", surnom=:name";
				$query .= ", nom=:name";
				$query .= ", adresse=:address";
				$query .= ", code_postal=:cp_unique";
				$query .= ", ville=:city";
				$query .= ", email=:email";
				$query .= ", telephone=:tel";
				$query .= ", mot_de_passe=:password";
				$query .= ", societe=:name";
				$query .= ", adresse_num=''";
				$query .= ", confirmed_email=1";
				
				trace('query client : ');
				trace($query);

				$sid = uniqid('', true);
				$pwd = md5($values["password"]);
				
				$smth = $dbh->prepare($query);
				$smth->bindParam(":sid", $sid);
				$smth->bindParam(":lang", $values["lang"]);
				$smth->bindParam(":name", $values["name"]);
				$smth->bindParam(":address", $values["address"]);
				$smth->bindParam(":cp_unique", $values["cp_unique"]);
				$smth->bindParam(":city", $values["city"]);
				$smth->bindParam(":email", $values["email"]);
				$smth->bindParam(":tel", $values["tel"]);
				$smth->bindParam(":password", $pwd);
				
				$smth->execute();
				var_dump($dbh->errorInfo());
				
				$id_client = $dbh->lastInsertId();
				
				
				$query = "UPDATE garages SET id_client=:id_client WHERE id=:id";
				$smth = $dbh->prepare($query);
				$smth->bindParam(":id_client", $id_client);
				$smth->bindParam(":id", $id_garage);
				$smth->execute();
				
				/*
				id	int(11) Incrément automatique	 
				name	text	 
				address	text	 
				cp_unique	varchar(99)	 
				city	text	 
				lang	varchar(9)	 
				email	text	 
				tel	text	 
				tel_sms	text	 
				receive_sms	int(11)	 
				password	text	 
				horaires	text	 
				list_holidays	text	 
				
				type? 
				typecontact ?
				adresse / adresse_num sont séparés dans table clients, pas dans garages
				nom / prenom sont séparés dans table clients, pas dans garages.
				telephone, mettre le telephone fixe ou portable ?
				
				 */
				
			}

		}

		else if($mode=="edit"){

			$id = $_GET["id"];
			$tablename = $_GET["tablename"];
			$nbcol = count($columns);
			$count = 0;
			
			$query = "UPDATE ".$_GET["tablename"]." SET ";
			
			foreach($columns as $col){
				$query .= $col."=:".$col;
				if($count < $nbcol-1) $query .= ", ";
				$count++;
			}
			$query .= " WHERE id=:id";
			
			$smth = $dbh->prepare($query);
			//$smth->bindParam(":tablename", $_GET["tablename"]);
			$smth->bindParam(":id", $id);
			
			foreach($columns as $col){
				$smth->bindParam(":".$col, $values[$col]);
			}
			$smth->execute();
			var_dump($dbh->errorInfo());
			
			//trace("query : ".$query);
			
			//mysql_query($query);
			
 			if($tablename == "garages"){
				$smth = $dbh->prepare("SELECT * FROM garages WHERE id=:id");
				$smth->bindParam(":id", $id);
				$smth->execute();
				$obj = $smth->fetch(PDO::FETCH_ASSOC);
				$id_client2 = $obj["id_client"];
				trace("id_client2 : ".$id_client2);
				
				
				$query = "UPDATE clients SET ";
				$query .= "sid=:sid";
				$query .= ", lang=:lang";
				$query .= ", lang2=:lang";
				$query .= ", date=NOW()";
				$query .= ", type=1";
				$query .= ", surnom=:name";
				$query .= ", nom=:name";
				$query .= ", adresse=:address";
				$query .= ", code_postal=:cp_unique";
				$query .= ", ville=:city";
				$query .= ", email=:email";
				$query .= ", telephone=:tel";
				$query .= ", mot_de_passe=:password";
				$query .= ", societe=:name";
				$query .= ", adresse_num=''";
				$query .= " WHERE id=:id";
				
				trace($query);
				
				$sid = uniqid('', true);
				$pwd = md5($values["password"]);
				
				$smth = $dbh->prepare($query);
				$smth->bindParam(":sid", $sid);
				$smth->bindParam(":lang", $values["lang"]);
				$smth->bindParam(":name", $values["name"]);
				$smth->bindParam(":address", $values["address"]);
				$smth->bindParam(":cp_unique", $values["cp_unique"]);
				$smth->bindParam(":city", $values["city"]);
				$smth->bindParam(":email", $values["email"]);
				$smth->bindParam(":tel", $values["tel"]);
				$smth->bindParam(":password", $pwd);
				
				$smth->bindParam(":id", $id_client2);
				$smth->execute();
				
			}

		}
	}
}





?>



<link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/pikaday.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/calendar.css" rel="stylesheet" type="text/css" media="screen" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/pikaday.js"></script>



<body style="background-color:#FFFFFF;">

<img id="btn_close_lightview" src="images/close_lightview.jpg" onclick="onClickCloseLightview()" />



<form id="form" method="POST">

	<?php

	$title = "";

	if($mode=="create") $title = "Ajout";
	else if($mode=="edit") $title = "Modification";

	?>

	<h2><?php echo $title;?></h2>
	

	<?php

	$nbcol = count($columns);

	for($i=0; $i<$nbcol; $i++){

		$col = $columns[$i];
		$label = stripslashes($labels[$i]);
		$value = $values[$col];
		$type = $types[$i];
		

		//$GLOBALS["TRACE_ENABLED"] = true;
		echo "<div style='margin-top:12px;'>";
		

		//liste d�roulante statique

		if(isset($selectable_key[$col])){

			echo "<span>".$label." : </span>";

			echo "<select name='".$col."'>";

			$tab = $selectable_key[$col];

			foreach($tab as $option){

				$option_tab = explode("___", $option);
				$option_label = $option_tab[0];
				$option_name = $option_tab[1];

				echo "<option ";

				if($value==$option_name) echo "selected ";
				echo "value='".$option_name."'>".$option_label."</option>";

			}

			echo "</select>";
		}

		

		//liste d�roulante dynamique

		else if(substr($col, 0, 3)=="id_"){

			$select_tablename = substr($col, 3, strlen($col)-3);
			$select_tablename = preg_replace('/__\d+/', "", $select_tablename);
			
			//trace("select_tablename : ".$select_tablename."/");

			echo "<span>".$label." : </span>";
			echo "<select name='".$col."'>";

			//$r_select = mysql_query("SELECT * FROM ".$select_tablename." ORDER BY name ASC");
			$smth = $dbh->prepare("SELECT * FROM ".$select_tablename." ORDER BY name ASC");
			$smth->execute();

			while($obj = $smth->fetch(PDO::FETCH_ASSOC)){
			//while($obj = mysql_fetch_object($r_select)){

				echo "<option ";
				if($value==$obj["id"]) echo "selected ";
				echo "value='".$obj["id"]."'>".$obj["name"]."</option>";

			}
			echo "</select>";
		}
		//date
		else if($type == "date"){
			echo '<span class="label">'.$label.'</span> : <input name="'.$col.'" type="text" id="datepicker">';
		}
		//text area
		else if($type == "textarea"){
			echo '<span class="label">'.$label.'</span> : <textarea style="" name="'.$col.'">'.$value.'</textarea>';
		}
		//champ texte standard
		else{

			//trace("tablename : ".$tablename.", col : ".$col.", label : ".$label.", value : ".$value);
			echo "<span>".$label.' : </span><input type="text" name="'.$col.'" value="'.$value.'" />';
		}

		echo "</div>";
		echo "<div style='clear:both;'>";
	}

	?>


	<input type="hidden" value="1" name="valid" />

	<br />
	<span id="btn_submit" style='float:right;' class='btn_clickable' onclick="javascript:onclickSubmitForm()"><img src="images/icon-save.gif" /> <span class='btnlink'>Enregistrer</span></span>


<?php




if(isset($_POST["valid"])){

	if($bError){

		echo "<div class='msg_error'>".$bErrorMsg."</div><br />";
	}

	else{

		if($mode=="create") echo "<div class='msg_valid'>Insertion OK</div><br />";
		else echo "<div class='msg_valid'>Modification OK</div><br />";
		
		echo '<script type="text/javascript">parent.$.fancybox.close();</script>';

	}

}

?>

</form>
<script>
	window.onload = function(){
		var _elmt = document.getElementById('datepicker');
		console.log("_elmt : "+_elmt);
		var picker = new Pikaday({ 
			field: _elmt,
			firstDay: 1,
			format: 'DD-MM-YYYY',

			i18n: {
				previousMonth : 'Mois précédent',
				nextMonth     : 'Mois suivant',
				months        : ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
				weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
				weekdaysShort : ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam']
			},

			onSelect:function(){
				console.log("onSelect");
				_elmt.value = this.getMoment().format('DD-MM-YYYY');
			}
		});

		/*
		for(var i in _listDates){
			var _date = _listDates[i];
			picker.setDate(_date);
		}
		*/
	};
    
</script>

</body>