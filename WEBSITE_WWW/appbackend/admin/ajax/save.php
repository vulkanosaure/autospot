<?php
$GLOBALS["TRACE_ENABLED"] = true;
require_once("../../../reseau/config_network_version.php");
require_once("../../../map/php/include/db_connect.php");
require_once("../../../map/php/include/utils.php");

/* 
$_POST["datajson"] = '[{"id":"ACHENHEIMTest","city":"whadup","station":"Bla bla","display_line":"1","accessibility":"0"},{"id":"SAVERNECFAJulesVerne","city":"SAVERNE","station":"C.F.A. Jules Verne","display_line":"0","accessibility":"0"},{"id":"SAVERNECollgeLesSources","city":"SAVERNE","station":"Collège Les Sources","display_line":"0","accessibility":"0"},{"id":"SAVERNEPlacedesDragons","city":"SAVERNE","station":"Place des Dragons","display_line":"0","accessibility":"0"},{"id":"SAVERNEGareRoutire","city":"SAVERNE","station":"Gare Routière","display_line":"1","accessibility":"0"},{"id":"SAVERNECAT","city":"SAVERNE","station":"C.A.T.","display_line":"1","accessibility":"0"},{"id":"WALDOLWISHEIMCentre","city":"WALDOLWISHEIM","station":"Centre","display_line":"1","accessibility":"0"},{"id":"FURCHHAUSENCentre","city":"FURCHHAUSEN","station":"Centre","display_line":"1","accessibility":"0"},{"id":"ALTENHEIMMairie","city":"ALTENHEIM","station":"Mairie","display_line":"1","accessibility":"0"},{"id":"FRIEDOLSHEIMMairie","city":"FRIEDOLSHEIM","station":"Mairie","display_line":"1","accessibility":"0"},{"id":"SAESSOLSHEIMMairie","city":"SAESSOLSHEIM","station":"Mairie","display_line":"1","accessibility":"0"},{"id":"SAESSOLSHEIMRoutedeSaverne","city":"SAESSOLSHEIM","station":"Route de Saverne","display_line":"1","accessibility":"0"},{"id":"DUNTZENHEIMCentre","city":"DUNTZENHEIM","station":"Centre","display_line":"1","accessibility":"0"},{"id":"ROHRPlacedelEglise","city":"ROHR","station":"Place de lEglise","display_line":"1","accessibility":"0"},{"id":"ROHRLotRohrbach","city":"ROHR","station":"Lot. Rohrbach","display_line":"1","accessibility":"0"},{"id":"GOUGENHEIMPlacedelaLibration","city":"GOUGENHEIM","station":"Place de la Libération","display_line":"1","accessibility":"0"},{"id":"GOUGENHEIMRoutedeMittelhausen","city":"GOUGENHEIM","station":"Route de Mittelhausen","display_line":"1","accessibility":"0"},{"id":"GIMBRETTCentre","city":"GIMBRETT","station":"Centre","display_line":"1","accessibility":"0"},{"id":"REITWILLERMairie","city":"REITWILLER","station":"Mairie","display_line":"1","accessibility":"0"},{"id":"TRUCHTERSHEIMChteaudEau","city":"TRUCHTERSHEIM","station":"Château dEau","display_line":"1","accessibility":"0"},{"id":"TRUCHTERSHEIMPlaceduMarch","city":"TRUCHTERSHEIM","station":"Place du Marché","display_line":"1","accessibility":"0"},{"id":"TRUCHTERSHEIMAncienneGare","city":"TRUCHTERSHEIM","station":"Ancienne Gare","display_line":"1","accessibility":"0"},{"id":"TRUCHTERSHEIMCollgeduKochersberg","city":"TRUCHTERSHEIM","station":"Collège du Kochersberg","display_line":"1","accessibility":"0"}]';
$_POST["idline"] = "405";
$_POST["sens"] = "A";
 */
 
$active = true;
 
 

$_POST["datajson"] = stripslashes($_POST["datajson"]);
trace("datajson : ".$_POST["datajson"].", idline : ".$_POST["idline"].", sens : ".$_POST["sens"]);

$data = json_decode($_POST["datajson"]);
$id_line = $_POST["idline"];
$sens = $_POST["sens"];


/*
A MODIFIER :

	station_details
	line2station
	route_details


station_details :
	fait une sauvegarde de location (where id_line and sens)
	delete where id_line and sens
	insert liste data
	
line2station :
	delete where id_line and sens
	insert liste data
	
route_details :
	fait une sauvagarde de list_location (where id_line and sens) par rapport aux cl� id_station_src + "_" + id_station_dst
	delete where id_line
	insert liste data
	
	
!todo :
	permettre d'inserer une location_moy pour les nouvelle stations
	route_details, on peut pas ins�rer comme �a, �a ne concerne que l'aller, ne le faire que si sens=="A" ?						OK
	list_location peut ne pas exister, (chaine vide si inexistante)																OK

*/



//________________________________________________________
//update table station_details

$locations = array();
$r = mysql_query("SELECT * FROM station_details WHERE id_line='".$id_line."' AND sens='".$sens."'");
while($obj = mysql_fetch_object($r)){
	$locations[$obj->id_station] = $obj->location;
	trace($obj->id_station." : ".$obj->location);
}

if($active) mysql_query("DELETE FROM station_details WHERE id_line='".$id_line."' AND sens='".$sens."'");

foreach($data as $obj){
	
	//3 possibilit�
		//cette "station_details" existait deja, on l'a donc en sauvegarde dans location
		//cette station existait, mais pas utilis�e, sur ce ligne/sens, on a donc une position moyenne de la station qu'on peut utiliser
	
	if(isset($locations[$obj->id])) $location = $locations[$obj->id];
	else{
		$r_location = mysql_query("SELECT * FROM station WHERE id='".$obj->id."'");
		$location = mysql_fetch_object($r_location)->location_moy;
	}
	$accessibility = ($obj->accessibility=="1") ? "true" : "false";
	$query = "INSERT INTO station_details VALUES ('', '".$id_line."', '".$obj->id."', '".$sens."', '".$location."', '".$accessibility."')";
	trace("query : ".$query);
	mysql_query($query);
}



//________________________________________________________
//update table line2station
trace("_______________________________________________________________________________________");

if($active) mysql_query("DELETE FROM line2station WHERE id_line='".$id_line."' AND sens='".$sens."'");

$position = 0;
foreach($data as $obj){
	$display_line = ($obj->display_line=="1") ? "1" : "0";
	$query = "INSERT INTO line2station VALUES ('".$id_line."', '".$sens."', '".$obj->id."', '".$position."', '".$display_line."')";
	trace("query : ".$query);
	if($active) mysql_query($query);
	$position++;
}




//________________________________________________________
//update table route_details


trace("_______________________________________________________________________________________");


if($sens=="A"){

	$list_locations = array();
	$r = mysql_query("SELECT * FROM route_details WHERE id_line='".$id_line."'");
	while($obj = mysql_fetch_object($r)){
		$key = $obj->id_station_src."_".$obj->id_station_dst;
		$list_locations[$key] = $obj->list_location;
		//trace($key." : ".$obj->list_location);
		
	}

	if($active) mysql_query("DELETE FROM route_details WHERE id_line='".$id_line."'");
	
	$position = 0;
	$id_station_prev = "";
	foreach($data as $obj){
		if($obj->display_line=="1"){
			if($id_station_prev != ""){
				$key = $id_station_prev."_".$obj->id;
				$list_location = (isset($list_locations[$key])) ? $list_locations[$key] : "";
				$query = "INSERT INTO route_details VALUES ('', '".$id_line."', '".$id_station_prev."', '".$obj->id."', '".$position."', '".$list_location."')";
				trace("query : ".$query);
				if($active) mysql_query($query);
				$position++;
			}
			$id_station_prev = $obj->id;
		}
	}
}


?>