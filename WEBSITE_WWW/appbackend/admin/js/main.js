var _listdata = {
	garages : {
		columns : ["name", "address", "cp_unique", "city", "lang", "email", "tel", "password", "receive_sms", "tel_sms", "iban", "tarif_horaire_ht", "horaires", "list_holidays", "actif"],
		labels : ["Nom", "Adresse", "Code Postal", "Localit&eacute;", "Langue", "Email", "T&eacute;l&eacute;phone", "Mot-de-passe", "Reçoit les SMS", "T&eacute;l&eacute;phone SMS", "IBAN", "Tarif horaire HT", "Horaires", "Vacances", "Actif"],
		types : ["", "", "", "", "", "", "", "", "", "", "", "", "textarea", "textarea", ""],
	},

	annonces_clients : {
		columns : ["marque", "modele", "version", "year", "kilometrage", "prix", "codepostal", "ville", "desc1", "valide", "inspection_impossible"],
		labels : ["Marque", "Modèle", "Version", "Année", "Km", "Prix", "CP", "Ville", "Desc rapide", "Publication", "Insp. tech. impossible"],
		types : ["", "", "", "", "", "", "", "", "", "", ""],
	},

	holidays : {
		columns : ["date1"],
		labels : ["Date"],
		types : ["date"],
		popupWidth : 500
	},
	
	cp2garage : {
		columns : ["cp", "id_garages__1", "id_garages__2"],
		labels : ["Code Postal", "Garage 1", "Garage 2"],
		types : ["", "", ""],
	},

};

var _minheightpopup = 340;


function onClickImageAnnonces(_tablename, _id)
{
	console.log("onClickImageAnnonces("+_tablename+", "+_id);


	var _url = "annonces/images.php?id="+_id;
	showLightview(_url, 700, 600);
}



function onClickEdit(_tablename, _id)
{
	console.log("onClickEdit("+_tablename+", "+_id+")");

	var _str = "form.php?mode=edit&";
	_str += "id="+_id+"&";
	_str += "tablename="+_tablename;

	var _data = _listdata[_tablename];

	_str += "&columns=" + _data.columns.join("___");
	_str += "&labels=" + _data.labels.join("___");
	_str += "&types=" + _data.types.join("___");

	console.log(_str);

	//showLightview("form.php?mode=edit&id="+_id+"&tablename=garages&columns=name___address___cp___city___email___tel___password&labels=Nom___Adresse___Code postal___Localit&eacute;___Email___T&eacute;l&eacute;phone___Mot-de-passe", 420, 400);
	var _height = 54 * _data.columns.length;
	if(_height < _minheightpopup) _height = _minheightpopup;

	var _width = (_data.popupWidth == undefined) ? 420 : _data.popupWidth;
	showLightview(_str, _width, _height);

}




function appendDate(e)
{
	var _href = e.href;
	
	var _d = new Date();
	var _str = formatNumber(_d.getDate()) + "-" + (formatNumber(_d.getMonth()+1)) + "-" + _d.getFullYear() + " "+formatNumber(_d.getHours()) + ":"+formatNumber(_d.getMinutes());
	
	var _date = window.prompt("Entrer une date (JJ-MM-AAAA HH:MM)", _str);
	if(_date == null) return;
	
	var _tab = _date.split(" ");
	var _tabdate = _tab[0].split("-");
	var _tabtime = _tab[1].split(":");
	
	var _d2 = new Date();
	_d2.setDate(_tabdate[0]);
	_d2.setMonth(_tabdate[1] - 1);
	_d2.setFullYear(_tabdate[2]);
	
	_d2.setHours(_tabtime[0]);
	_d2.setMinutes(_tabtime[1]);
	var _ts = _d2.getTime() / 1000;
	_ts = Math.round(_ts);
	
	_href += "&ts="+_ts;
	window.location.href =_href;
}

function formatNumber(_value){
	if(_value < 10) return "0"+_value;
	return _value;
}



function onClickAdd(_tablename)
{
	//todo : columns and stuff dynamic
	var _data = _listdata[_tablename];
		
	_columns = _data.columns.join("___");
	_labels = _data.labels.join("___");
	_types = _data.types.join("___");

	var _height = 54 * _data.columns.length;
	if(_height < _minheightpopup) _height = _minheightpopup;
	
	var _width = (_data.popupWidth == undefined) ? 420 : _data.popupWidth;
	showLightview("form.php?mode=create&tablename="+_tablename+"&columns="+_columns+"&labels="+_labels+"&types="+_types, _width, _height);

}





/* FORM */

function onclickSubmitForm()
{
	//alert("onclickSubmitForm "+$("#form_planning"));

	$("#form").submit();
}






function openDatePicker(){
	console.log("openDatePicker");


	return false;
}






/* LIGHTVIEW */



function showLightview(_url, _width, _height)
{
	$("a#hidden_link").fancybox({
		'height'			: _height,
		'width'				: _width,
		'showCloseButton'	: false,
		'titleShow'     	: false,
		'autoScale'			: false,
		'type'				: 'iframe',
		'padding' 			: 0,
		'margin' 			: 0,
		'onClosed'			: onFancyBoxClosed,
		'href'				: _url
	});

	$("a#hidden_link").click();

}



function onFancyBoxClosed()
{
	if(!noUpdateAfterCloseBox) window.location = window.location;
}



function onClickCloseLightview()
{
	parent.$.fancybox.close();

}










function onClickDelete(_page, _index)
{
	//alert("onClickDelete("+_index+")");
	if(!window.confirm("Supprimer cette ligne ?")) return;
	
	//todo, respecter la page en cours
	window.location.href = "index.php?page="+_page+"&action=delete&id="+_index;

}




 window.onbeforeunload = function()

{

	//alert("_page : "+_page+", _hasModif : "+_hasModif);
	/* 
	if(_page=="line2station" && _hasModif){

		return false;

	} 
	 */
	

}