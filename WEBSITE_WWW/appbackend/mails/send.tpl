<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>AutoSpot</title>

<style type="text/css">
    .ReadMsgBody {width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    body     {width: 100%; background-color: #ffffff; margin:0; padding:0;font-family: Arial, Helvetica, sans-serif;}
    body .onlyMobile {display: none !important;}
    @media only screen and (max-width: 650px)  {
                    body .deviceWidth {width:440px!important; padding:0;border:none !important;}    
                    body .center {text-align: center!important;}  
                    body .onlyDesktop {display: none !important;}
                    body .onlyMobile {display: block !important;}
                    body .nowidth {width:0!important;display:none !important;}
            }
            
    @media only screen and (max-width: 479px) {
                    body .deviceWidth {width:280px!important; padding:0;border:none !important;}    
                    body .center {text-align: center!important;}  
                    body .onlyDesktop {display: none !important;}
                    body .onlyMobile {display: block !important;}
                    body .nowidth {width:0!important;display:none !important;}
            }

</style>
</head>


<body style="margin:0;font-family:Verdana;background-color:white;">

<table style="font-family: Arial, Helvetica, sans-serif;padding-left:0;padding-right:0;padding-top:0;padding-bottom:0;margin-left:0;margin-bottom:0;margin-top:0;margin-right:0;width:100%;" border="0" border-spacing="0" border-collapse="collapse" cellpadding="0" cellspacing="0" align="center">


<tr style="height:200px;">
<td style="text-align:center;"><img style="width:500px;" src='{{path_server}}/mails/images/logo.png' /></td>
</tr>
<!-- 
<tr style="height:150px;">
<td style="text-align:center;"><h1 style='color:#009ada'>{{title}}</h1></td>
</tr>
 -->
<tr>
<td style="text-align:left;">{{dyn_content}}</td>
</tr>

</table>

</body>

</html>