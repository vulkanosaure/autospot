<?php

require_once("admin/include/routines.php");
//require_once("admin/include/db_connect.php");
require_once("admin/include/utils.php");
require_once("shortcuts.php");
require_once("utils.php");

ini_set('xdebug.var_display_max_depth', 20);
ini_set('xdebug.var_display_max_children', 512);
ini_set('xdebug.var_display_max_data', 1024);




$chapters = array(0, 1, 2, 3, 4, 5, 38, 39, 6, 10.1, 11, 12.3, 12.4);


$start = 0;
$end = count($chapters);
if(isset($_GET["debug"])){
	$start = $_GET["debug"];
	$end = $start + 1;
}





$output = array();

for($i=$start; $i<$end; $i++){
	
	if(isset($_GET["debug"])) $istr = $i;
	else{
		$istr = $chapters[$i];
	}
	
	while(strlen($istr) < 2) $istr = "0".$istr;
	
	$file = "questions/".$istr.".data.json";
	$content = file_get_contents($file);
	
	$content = preg_replace('`\/\*(.)*\*\/`', '', $content);
	
	
	//echo $content."<br /><br />";
	
	$obj = json_decode($content, true);
	
	
	$obj = processShortcut($obj);
	
	$output[] = $obj;
}


/* 
echo "___________________________________________<br />";
echo "final <br />";
var_dump($output);
echo "<br /><br /><br /><br /><br /><br />";
 */

echo json_encode($output);












function processShortcut($_data)
{
	$_listquestions = $_data["questions"];
	$_len = count($_listquestions);
	
	for($i=0; $i<$_len; $i++){
		
		$question_parent = null;
		$question = $_listquestions[$i];
		rec($question, $question_parent, 0, "", 0, null, false);
		
		$_listquestions[$i] = $question;
	}
	$_data["questions"] = $_listquestions;
	
	
	return $_data;
}



function rec(&$_object, &$_objectParent, $_levelrec, $_iditem, $_index, $_func, $_islast)
{
	
	if(is_string($_object)){
		$_object = array("text" => $_object);
	}
	
	
	//var_dump($_object);
	//tracerec($_object["text"].", islast : ".$_islast, $_levelrec);
	/* 
	$_object["text"].= "-mod1";
	$_objectParent["text"].= "-mod2";
	 */
	 
	//tpl
	
	if(isset($_object["tpl"])){
		
		$tpl = $_object["tpl"];
		
		if(is_string($tpl)){
			
			global $library_shortcut;
			if(!isset($library_shortcut[$tpl])) throw new Exception('tpl "'.$tpl.'" not found.');
			$tpl = $library_shortcut[$tpl];
			
		}
		
		foreach($_object as $k=>$v){
			$tpl[$k] = $v;
		}
		
		$_object = $tpl;
		
		unset($_object["tpl"]);
		
	}
	
	
	//items_tpl
	
	if($_objectParent != null){
		
		if(isset($_objectParent["items_tpl"]) && $_objectParent["items_tpl"] != ""){
			
			$parent_tpl = $_objectParent["items_tpl"];
			
			if(is_string($parent_tpl)){
				
				global $library_shortcut;
				if(!isset($library_shortcut[$parent_tpl])) throw new Exception('items_tpl "'.$parent_tpl.'" not found.');
				$parent_tpl = $library_shortcut[$parent_tpl];
				
			}
			
			foreach($_object as $k=>$v){
				$parent_tpl[$k] = $v;
			}
			
			//$_objectParent["items"][$_index] = $parent_tpl;
			
			foreach($parent_tpl as $k=>$v){
				$_objectParent["items"][$_index][$k] = $v;
			}
			
			if($_islast){
				unset($_objectParent["items_tpl"]);
			}
		}
		
	}
	
	
	
	
	
	
	
	if(!isset($_object["type"])){
		
		$_object["type"] = "label";
	}
	
	
	//$_idparent = ($_objectParent != null) ? $_objectParent["id"] : "";
	
	if (isset($_object["items"]) && $_object["items"] == "") {
		unset($_object["items"]);
	}
	
	if (isset($_object["items"])) {
		
		$_len = count($_object["items"]);
		$_counter = 0;
		
		$_nextlevel = $_levelrec + 1;

		for ($i = 0; $i < $_len; $i++) {

			$_obj =& $_object["items"][$i];
			//$_iditem = $_idparent . $_counter;
			$_islast = ($i == $_len - 1);

			rec($_obj, $_object, $_nextlevel, $_iditem, $i, $_func, $_islast);
			$_counter++;

		}
	}
	
}





?>