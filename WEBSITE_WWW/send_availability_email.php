<?php
session_start();

require_once('vendor/autoload.php');
include_once('db_connexion.php');
include_once('classes/client.php');
include_once('classes/annonce.php');
include_once('classes/email.php');
include_once('classes/demande_disponibilite.php');

$id_annonce = array_key_exists('id_annonce', $_POST) && !empty($_POST['id_annonce']) ? trim($_POST['id_annonce']) : null;
$id_client = array_key_exists('id_client', $_POST) && !empty($_POST['id_client']) ? trim($_POST['id_client']) : null;
$referer = array_key_exists('referer', $_GET) && !empty($_GET['referer']) ? trim($_GET['referer']) : 'index.php';

$msg = new \Plasticbrain\FlashMessages\FlashMessages();

if ($id_annonce !== null && $id_client !== null) {
    $annonce = annonce::getById($id_annonce);
    $client_acheteur = client::getById($id_client);

    if ($annonce !== null && $client_acheteur !== null) {
        $demande_disponibilite = demande_disponibilite::getByAnnonceAndClient($annonce->id, $client_acheteur->id);

        // Check if current user has rights to request email
        if ($demande_disponibilite === false) {
            $client_vendeur = client::getById($annonce->id_client);

            if ($client_vendeur !== false) {
                $mail = email::init();

                $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
                $twig = new \Twig_Environment($loader);
                $body = $twig->render('availability.html.twig', [
                    'id' => $annonce->id,
                    'prenom' => $client_vendeur->prenom,
                    'nom' => $client_vendeur->nom,
                    'marque' => $annonce->marque,
                    'modele' => $annonce->modele,
                    'url' => 'http://www.autospot.ch/compte.php?annonces',
                    'url_annonce' => 'http://www.autospot.ch/annonce.php?id='.$annonce->id,
                ]);

                $mail->setFrom('contact@autospot.ch', 'AutoSpot');
                $mail->addAddress($client_vendeur->email);
                $mail->addReplyTo($client_acheteur->email);
                $mail->isHTML(true);
                $mail->CharSet = 'UTF-8';
                $mail->Subject = 'Message concernant votre annonce sur Autospot';
                $mail->Body = $body;
                $mail->send();

                $msg->success('Votre demande pour savoir si ce véhicule est encore en vente a bien été envoyée au vendeur. Vous recevrez sa réponse dans votre boite mail.');

                demande_disponibilite::insert($annonce->id, $client_acheteur->id);
            }
        } else {
            $date_demande = \DateTime::createFromFormat('Y-m-d H:i:s', $demande_disponibilite->created_at);
            $msg->error(sprintf('Vous avez déjà effectué une demande le %s', $date_demande->format('d/m/Y')));
        }
    }
}

header('Location: '.$referer);
exit;

