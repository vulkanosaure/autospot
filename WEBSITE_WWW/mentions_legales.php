<?PHP
session_start();
// if(!isset($_SESSION['id_client']))
// {
//     header("Location: inscription.php");
//     exit();
// }
include("header.php");
include("body.php");
?>

<!-- MENTIONS LEGALES -->
<div class="container">
    <h3 class="center">Mentions légales</h3>

<div class="row cgu">
    <p class="center">Le site internet <a href="www.autospot.ch/">www.autospot.ch</a> est la propriété exclusive de la société <a href="http://skaiweb.ch/">SkaiWeb</a>, inscrite au registre du commerce du canton de Vaud sous le numéro d'identification CHE-446.329.949.<br />
    Le siège social se situe à 1610 Oron-la-ville (Vaud)<br/>
    Adresse email : contact@autospot.ch</p>

    <p class="center">Le site est hébergé par OVH, 2 rue kellermann, 59100 Roubaix.</p>

    <p class="center">Le directeur de publication est Valentin Mendonça en sa qualité de Président de la société <a href="http://skaiweb.ch/">SkaiWeb</a>.</p>
    
    </div>
</div>    
<div class="clear"></div>
<!-- END MENTIONS LEGALES -->
</div>
<?php include("footer.php"); ?>