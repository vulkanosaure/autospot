<aside id="side-nav" class="w-text-light">
    <div class="side-nav-wrapper">
        <nav id="vertical-nav">
            <ul class="vertical-menu">
               <li id="vertical-menu-item-6119" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2295 current_page_item menu-item-5602">
                    <a href="/">ACCUEIL</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page "> <a href="/?r=voiture&marque=Toutes&modele=Tous">Rechercher une annonce</a></li>
				<li id="vertical-menu-item-6119" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6119">
                    <a href="/publier.php">PUBLIER UNE ANNONCE</a>
                </li>
                <li id="vertical-menu-item-5602" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2295 current_page_item menu-item-5602">
                    <a href="/#etapesIndexTab">Comment ça marche ?</a>
                </li>
                <li id="vertical-menu-item-5602" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2295 current_page_item menu-item-5602">
                    <a href="/#forfaits">Nos forfaits</a>
                </li>
                <li id="vertical-menu-item-6119" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6119">
                    <a href="#">Mon spot<span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                    <ul class="sub-menu">
                        <li class="back-to-parent"><span><i class="fa fa-angle-left" aria-hidden="true"></i> RETOUR</span></li>
                        <?php
                        if (isset($_SESSION['id_client']))
                        {
                            $requete = mysqli_query($connect1, "SELECT prenom FROM clients WHERE id='$_SESSION[id_client]'");
                            $clients = mysqli_fetch_assoc($requete);
                            $prenom = $clients['prenom'];
                        }
                        ?>
                        <?php if (isset($_SESSION['id_client'])) : ?>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?profil">Bienvenue <?php echo $prenom;?></a></li>
                        <?php else : ?>
                            <li><a href="inscription.php">Se connecter / s’inscrire</a></li>
                        <?php endif; ?>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?annonces">Mes publications</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?demandes">Suivi de mes demandes</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?favoris">Mes favoris</button></a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?profil">Mon profil</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="/ajax_deconnecter.php" data-toggle="collapse" data-target="#logoffSuccess">Déconnexion</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "> &nbsp;</li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page "> &nbsp;</li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page ">
                    <a href="/qui_sommes_nous.php">Qui sommes-nous ?</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page ">
                    <a href="/nos-garages-partenaires.php">Nos garages partenaires</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page ">
                    <a href="/adhesion-reseau-partenaires.php">Adhérer à notre réseau de garages partenaires</a>
                </li>
            </ul>
        </nav>


    </div>
</aside>
<header id="header" class="w-dark w-sticky w-transparent w-text-light">
    <div class="cls-container"> 
        <span class="mobile-nav-icon"> <i class="menu-icon"></i> </span> 
        <span id="header-logo"> <a href="/"> 
                <img class="dark-logo" src="images/logomini.png" alt="" /> 
                <img class="dark-sticky" src="images/logomini.png" alt="" /> 
                <img class="light-logo" src="images/logomini.png" alt="" /> 
                <img class="light-sticky" src="images/logomini.png" alt="" /> </a> </span>
        <nav id="top-nav" class="dropdown-nav">
            <ul class="top-menu2">
                <li id="commentCaMarche" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2295 current_page_item menu-item-5602 ">
                    <a class="mk_forfit" href="/#etapesIndexTab">Comment ça marche ?</a>
                </li>
                <li id="nosForfaits" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2295 current_page_item menu-item-5602 ">
                    <a class="mk_forfit" href="/#forfaits">Nos forfaits</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2295 current_page_item menu-item-5602">
                    <a href="publier.php">Publier Une Annonce </a>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6119"><a href="#">Mon spot <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    <ul class="sub-menu">
                        <?php if (isset($_SESSION['id_client'])) : ?>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page ">
                                <a href="compte.php?profil">Bienvenue <?php echo $prenom;?></a>
                            </li>
                        <?php else : ?>
                            <li><a href="inscription.php">Se connecter / s'inscrire</a></li>
                        <?php endif; ?>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "> <a href="/?r=voiture&marque=Toutes&modele=Tous">Rechercher une annonce</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?annonces">Mes publications</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?demandes">Suivi de mes demandes</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?favoris">Mes favoris</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page "><a href="compte.php?profil">Mon profil</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page  spothide">  <a href="publier.php">Publier une annonce</a></li>
                        <!--<li class="menu-item menu-item-type-post_type menu-item-object-page "><a onClick="javascript:ajax('//' + window.location.hostname + '/ajax_deconnecter.php', '');" data-toggle="collapse" data-target="#logoffSuccess">Déconnexion</a></li>-->
                        <?php if (isset($_SESSION['id_client'])) : ?>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page ">
                                <a href="/ajax_deconnecter.php" data-toggle="collapse" data-target="#logoffSuccess">Déconnexion</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>

            </ul>
        </nav>
        <ul class="contact-info">
            <!-- <li> <a href="<?PHP echo $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; if($_SERVER['QUERY_STRING'] == "") echo '?lang=fr'; else echo $_SERVER['QUERY_STRING'].'&lang=fr';?>">fr</a></li> -->
            <!-- <li> <a href="<?PHP echo $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; if($_SERVER['QUERY_STRING'] == "") echo '?lang=it'; else echo $_SERVER['QUERY_STRING'].'&lang=it';?>">it</a></li> -->
            <!-- <li> <a href="<?PHP echo $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; if($_SERVER['QUERY_STRING'] == "") echo '?lang=fr'; else echo $_SERVER['QUERY_STRING'].'&lang=de';?>">de</a></li> -->
            <li>fr</li>
            <li>it</li>
            <li>de</li>
        </ul>
    </div>

</header>