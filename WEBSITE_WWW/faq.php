<div id="faq" class="modal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal">&times;</button><br />

<h2>Questions Fréquemment Posées</h2>
<br>
<div class="faq-page">
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_1" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>1. Qu’est-ce qu’autospot.ch ?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer1" class="col-md-12">
<p>Autospot.ch est le premier moteur de recherche Suisse dédié aux annonces de véhicules provenant des portails automobiles et sites de petites annonces Suisse.
<P>Sur la nouvelle plateforme autospot.ch, vous trouverez forcément le véhicule et/ou l'accessoire automobile de votre choix.
<p>Afin de satisfaire aux demandes de nos utilisateurs et d’être le plus exhaustif possible, nous répertorions les annonces des catégories suivantes:</P>
<UL><LI>Voitures de tourisme
<LI>Motos
<LI>Utilitaires
<LI>Camping-car
<LI>Caravanes
<LI>Camions
<LI>Pièces & accessoires
<LI>Jantes, Pneus, roues
</UL>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_2" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>2. D’où proviennent les annonces d’autospot.ch?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer2" class="col-md-12">
Autospot.ch est en mesure de vous fournir un large choix d’annonces de qualité qui proviennent des sites internet suivants:
<UL><LI><a href="http://www.topannonces.ch">topannonces.ch</a>
<LI><a href="http://www.petitesannonces.ch">petitesannonces.ch</a>
<LI><a href="http://www.car4you.ch">car4you.ch</a>
<LI><a href="http://www.anibis.ch">anibis.ch</a>
<LI><a href="http://www.fahrzeugnet.ch">fahrzeugnet.ch</a>
<LI><a href="http://www.drive-in.ch">drive-in.ch</a>
</UL>
<P>Si vous souhaitez plus d’informations sur chacun de ces sites, veuillez cliquer ICI.</P>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_3" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>3. Pourquoi avons-nous crée autospot.ch ?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer3" class="col-md-12">
<P>Depuis quelques années de nombreux portails automobiles et sites d’annonces se sont crées sur internet en Suisse.</P>
<P>En partant de cette constatation, nous avons souhaité vous faciliter la tâche et vous faire gagner du temps en créant un moteur de recherche qui regroupe et répertorie toute les annonces de véhicules de Suisse.</P>
<P>Simple d'utilisation, rapide et efficace, autospot.ch vous permet, en une seule recherche, d'obtenir des annonces de qualité provenant des portails automobiles Suisse.</P>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_4" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>4. Quels sont les avantages d’autospot.ch ?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer4" class="col-md-12">
<P>Grâce à autospot.ch, plus besoin d’effectuer de longues et fastidieuses recherche sur internet pour obtenir le véhicule adapté à vos envies.</P>
<P>Dorénavant, une seule requête sur notre moteur de recherche vous permet d’obtenir les annonces correspondant au véhicule recherché provenant des portails automobiles Suisse.</P>
<P>En tant qu’utilisateur de notre moteur de recherche, vous ne risquez pas d’obtenir des annonces périmées. En effet, afin de vous offrir des résultats de recherche à jour et de qualité, nous recevons quotidiennement les annonces de chaque portail automobile.</P>
<P>De plus, en effectuant une requete sur notre moteur de recherche, vous serez automatiquement localisé, grâce à votre adresse IP. Ce nouveau service à été réalisé afin de vous permettre de filtrer les annonces selon la distance en kilomètres qui vous sépare d'elle.</P>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_5" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>5. Que puis-je rechercher dans la barre de recherche de la page d’accueil?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer5" class="col-md-12">
Notre moteur de recherche recense les annonces Suisse des catégories suivantes:
<UL><LI>Voitures de tourisme
<LI>Motos
<LI>Utilitaires
<LI>Camping-car
<LI>Caravanes
<LI>Camions
<LI>Pièces & accessoires
<LI>Jantes, Pneus, roues
</UL>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_6" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>6. Comment utiliser la barre de recherche de la page d’accueil ?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer6" class="col-md-12">
<P>Comme tout moteur de recherche, vous pouvez effectuer votre recherche facilement et rapidement en inscrivant dans la barre de recherche le /les termes associés à votre requête.</P>
<P>Vous pouvez, simplement, inscrire la marque et/ou le modèle du véhicule recherché (Exemple: Audi, Audi A3, Ford, Ford Focus etc).</P>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_7" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>7. Quelle marques de véhicules puis-je inscrire dans la barre de recherche?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer7" class="col-md-12">
Vous pouvez effectuer une recherche pour toute les marques connues de véhicules, et plus précisément de voitures de tourisme, motos, utilitaires, camping-cars, caravanes et camions.
<P>Vous pouvez observer toutes les marques de véhicules sur la page de résultats de recherche dans la colonne de gauche intitulé “filtrer les résultats de recherche”, dans  le menu déroulant marques.</P>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_8" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>8. Qu’est-ce que “filtrer les résultats de recherche”?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer8" class="col-md-12">
Dès lors que vous avez inscrit un/des critère de recherche dans la barre de recherche de la page d’accueil, les annonces correspondant à votre requête s’affichent sur une nouvelle page. Cette nouvelle page s'appelle “page de résultats de recherche”
<P>A cet instant, deux possibilités s’offrent à vous:
<OL><LI>Ne pas filtrer les annonces: En effet, si les annonces obtenues vous sembles suffisamment précises, il n’est pas nécessaire de les filtrer. Il vous suffit de les consulter.
<LI>Filtrer les annonces: si vous estimez que les annonces obtenues ne sont pas assez précises ou qu’un trop grand nombre d’annonce sont obtenues, vous avez la possibilité de les filtrer.
</OL>
</P>
<P>Filtrer les annonces signifie donc de restreindre les annonces obtenus en ajoutant des critères de recherche.</P>
</div>
</div>
</div>
<div class="faq">
<div class="question">
<div class="col-md-1"><button id="btn-faq_9" class="float button">+</button></div>
<div class="col-md-11">
<h3><strong>9. Comment filtrer les résultats de recherche?</strong></h3>
</div>
</div>
<div class="answer">
<div id="answer9" class="col-md-12">
Pour ce faire, veuillez utiliser la colonne, “filtrer les résultats de recherche”,qui se situe sur la partie gauche du site.
<p>Grâce à nos outils de recherche innovants et performants, vous avez la possibilité de filtrer vos résultats de recherche selon les critères suivants:</P>
<UL><LI>Marque
<LI>Modèle
<LI>Prix
<LI>Année
<LI>Kilomètres
<LI>Type de carrosserie
<LI>Motorisation
<LI>Energie: (exemple: essence, diesel, hybride)
</UL></P>
<P>Filtrer les annonces signifie donc de restreindre les annonces obtenus en ajoutant des critères de recherche.</P>
</div>
</div>
</div>
</div>
<script type="text/javascript">// <![CDATA[
$(".faq-page .question").click(function() {

    if($(this).hasClass('active')){

        $(this).parent().children('.answer').hide('fast');
        $(this).children().children('.button').html('+');

        $(this).removeClass('active');
    } else {

        $(this).children().children('.button').html('-');
        $(this).parent().children('.answer').show('fast');

        $(this).addClass('active');
    }
});
// ]]></script>
<br>
	</div><div class="clear"></div>
</div>
</div>
</div>
</div>