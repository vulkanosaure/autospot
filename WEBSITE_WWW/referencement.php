<?PHP
include("db_connexion.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr-FR" lang="fr-FR">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AutoSpot.ch | Moteur de recherche d'annonces automobiles suisses</title>

    <link rel="stylesheet" href="bootstrap.css">
    <?PHP
if(preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"]))
{
	?><link rel="stylesheet" href="style-moto.css"><?PHP
}
else
{
	?><link rel="stylesheet" href="style.css"><?PHP
}
?>
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&appId=121801251173900&version=v2.0";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div class="container">
	<form action="//<?PHP echo $_SERVER["HTTP_HOST"];?>/index.php" method="GET">
	<?PHP include("indexsearch2.php");?>
	</form>
	<!-- RESULTATS -->
	<div class="col-sm-10 resultcars">
<h2>Référencement sur notre moteur de recherche autospot</h2>
<br>
<H3>Qui somme-nous ?</H3>
<P>De la même manière que Google pour l’ensemble du web, notre moteur de recherche, www.autospot.ch, a pour objectif de recenser de la manière la plus exhaustive les annonces automobiles de suisses.</P>
<br>
<br>
<H3>Autospot.ch… un moteur de recherche uniquement pour les voitures ?</H3>
Sur www.autospot.ch, vous trouvez, non seulement des voitures de tourisme mais également des véhicules utilitaires, des camping-cars, des caravanes ainsi que des camions.
<P>Nous recensons également les pièces et accessoires automobiles ainsi que les jantes & pneus.</P>
<P>Si votre site internet possède exclusivement des annonces de motos, alors celles-ci seront référencé directement sur notre moteur de recherche spécialisé pour les deux roues, à l'adresse www.motospot.ch</P>
<br>
<br>
<H3>Vous possédez un site internet d’annonce de véhicules, alors bienvenu sur autospot.ch</H3>
<P>Sur www.autospot.ch, tous les sites internet dédiés aux annonces de véhicules ou d’accessoires automobiles, tels que les portails automobiles et les sites de petites annonces, sont les bienvenus.</P>
<br>
<br>
<H3>Chaque annonce compte !</H3>
<P>Quel que soit le nombre de véhicule que possède votre site internet, 100'000, 10'000 ou même 10 annonces, pour www.autospot.ch, chaque annonce possède le même intérêt.</P>
<br>
<br>
<H3>Que cela me rapporte-t-il ?</H3> Un recensement de vos annonces sur notre moteur de recherche, apportera à votre site internet une visibilité accrue et le trafic internet engendré.
<P>En effet, dès qu'un utilisateur souhaite obtenir des informations supplémentaires sur l'annonce ou simplement les coordonnées du vendeur, il lui suffit de cliquer sur l'annonce pour être automatiquement redirigé vers votre site internet.</P>
De plus, sur chaque annonces, vous trouverez le logo de votre site internet.
<P>Comme vous avez pu le remarquer en naviguant sur notre moteur de recherche les utilisateurs ne peuvent pas y insérer d’annonce.</P>
<br>
<br>
<H3>Intéressé pour un partenariat ?</H3>
Dans ce cas, deux possibilitées s’offre à vous:
<UL>
<LI>1ère possibilité :
<P>Vous avez les moyens techniques pour nous transmettre vos annonces au travers d’un flux de donnée. </P>
<P>Nous pouvons aisément travailler avec tous les types de fluxs.</P>
<br>
<br>
<LI>2ème possibilité :
<P>Si, au contraire, vous ne possédez pas les moyens techniques pour nous transmettre vos annonces via un flux de donnée, nous pouvons créer un robot, appelé également spyder.</P>
<P>Celui-ci sera en charge de scanner individuellement vos annonces, selon les fréquences que vous souhaités. Par exemple 1x par jour, 2x par semaine, 1x par semaine ou même 1x par quinzaine.</P>
</P>
<P>Bien entendu, plus les fréquences sont rapprochées et plus il y aura d’annonces à jour.</P></UL>
<br>
<br>
Si vous êtes intéressé, n’hésitez pas à nous contacter en utilisant le formulaire de contact à votre disposition sur notre site internet ou directement par email à l’adresse:
<a href="mailto:contact@autospot.ch">contact@autospot.ch</a>
<br>
<br>
Nous restons à votre écoute si vous avez d’éventuelles remarques ou suggestions.
<br>
	</div><div class="clear"></div>
<?PHP
include("footer.php");
?>
</div>
</body>
</html>