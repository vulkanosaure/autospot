<?php
session_start();

include("header.php");
include("body.php");
?>

<div class="container">
    <h3 class="center">Nos garages partenaires</h3>

    <p>Autospot est le pionnier et le leader en suisse en proposant à nos utilisateurs un service inédit grâce à la collaboration de nos garages partenaires.</p>

    <p>En effet, Autospot souhaite démocratiser, simplifier et rendre plus sûr l'achat de voitures d'occasions aux particuliers.</p>

    <p>Grâce à nos deux forfaits, ScanExpert et ScanExpert Protect, nos utilisateurs peuvent désormais mandater une inspection technique de 60 minutes afin de connaître l'état réel et précis de la voiture qu'ils envisagent d’acquérir.</p>

    <p>Nos garages partenaires ont été sélectionné avec soins selon des critères précis et respectent un devoir de transparence.</p>

    <p>Notre équipe est en constante recherche de nouveaux garages partenaire en suisse. Si le concept vous intéresse et que vous souhaitez en savoir plus, cliquer <a href="adhesion-reseau-partenaires.php">ici</a></p>

    <p>De plus, tous nos garages partenaires effectuent le diagnostic, l'entretien et la réparation de véhicules toutes marques selon les normes des constructeurs automobiles.</p>

    <p>Notre réseau de garages partenaires se compose des garages suivants:</p>

    <p><a href="https://www.rmb-cars.ch" target="_blank">Garage RMB-Cars</a> à Bursinel (Vaud).</p>

    <p><a href="http://www.garagemecatech.ch" target="_blank">Garage Mecatech</a> à Dorénaz (Valais).</p>

    <p><a href="http://www.franic.ch/garage" target="_blank">Garage Didier Francey</a> à Cousset (Fribourg).</p>

    <p><a href="http://www.auto-rothschild.ch" target="_blank">Garage Auto-Rothschild</a> à Genève.</p>
    
    <p><a href="javascript:;">Garage des nations</a> à Sion (Valais).</p>

    <p><a href="https://www.garagebaechler.ch" target="_blank">Garage-Carrosserie du Centre Baechler & Fils SA</a> à Corminboeuf (Fribourg).</p>

    <p><a href="http://longauto.ch/" target="_blank">Garage Longauto Sàrl</a> à Loveresse (Berne).</p>

    <p><a href="http://www.ac-ne.ch/" target="_blank">Garage Autoconcept réparations</a> à Neuchâtel.</p>

    <br><br>
</div>

<?php
include("footer.php");