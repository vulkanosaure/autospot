<?php
session_start();

require_once('vendor/autoload.php');
require_once('db_connexion.php');
require_once('classes/PHPMailer2.php');

$id_client = null;
$key = array_key_exists('key', $_GET) && !empty($_GET['key']) ? trim($_GET['key']) : null;
if (null !== $key) {
    $sql = sprintf('SELECT id FROM clients WHERE MD5(email) = "%s"', mysqli_real_escape_string($connect1, $key));
    $query = mysqli_query($connect1, $sql);
    $client = mysqli_fetch_object($query);

    if (null !== $client) {
        $sql = sprintf('UPDATE clients SET confirmed_email=1 WHERE id = "%s"', mysqli_real_escape_string($connect1, $client->id));
        mysqli_query($connect1, $sql);

        $_SESSION['id_client'] = $client->id;

        // L'utilisateur a validé son compte, qu'il a crée depuis 'je-choisis-mon-forfait.php'. On le redirige donc sur cette page
        if ($_GET['ref'] == 'choisir-forfait' && $_GET['step'] && $_GET['annonce']) {
            header('Location: je-choisis-mon-forfait.php?id='. $_GET['annonce'] .'&step'. $_GET['step'] .'&email-confirmed=ok'); die();
        }

        include("header.php");
        include("body.php");
        ?>

        <div class="container">
            <h3 class="center">Confirmez votre inscription</h3>

            <div class="container">
                <p class="center">Votre adresse email est à présent confirmée !</p>
                <p class="center">
                    <a href="/publier.php" class="btn btn-inline btn-default">Publier votre première annonce</a>
                </p>
            </div>
        </div>

        <?php
        include("footer.php");
        exit;
    }
} else {
    $id_client = array_key_exists('id_client', $_SESSION) && !empty($_SESSION['id_client']) ? trim($_SESSION['id_client']) : null;
    if (null === $id_client) {
        $_SESSION['redirection'] = 'publier';
        header("Location: inscription.php");
        exit();
    }
}

$sql = sprintf('SELECT id, email, confirmed_email FROM clients WHERE id = %s', mysqli_real_escape_string($connect1, $id_client));
$query = mysqli_query($connect1, $sql);
$client = mysqli_fetch_object($query);

if (null === $client)
{
    unset($_SESSION['id_client']);
    $_SESSION['redirection'] = 'publier';
    header("Location: inscription.php");
    exit();
}

if ($client->confirmed_email == 1) {
    header("Location: publier.php");
    exit();
}

include("header.php");
include("body.php");

$action = array_key_exists('action', $_GET) && !empty($_GET['action']) ? trim($_GET['action']) : null;
if ($action == 'resend') {
    $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
    $twig = new \Twig_Environment($loader);
    $body = $twig->render('inscription.html.twig', ['url' => sprintf('http://www.autospot.ch/inscription_confirmation.php?key=%s', md5($client->email))]);

    $mail = new PHPMailer2();
	
    $mail->setFrom('contact@autospot.ch', 'AutoSpot');
    $mail->addAddress($client->email);
    $mail->addReplyTo('no-reply@autospot.ch', 'Ne pas répondre');
    $mail->isHTML(true);

    $mail->Subject = 'Confirmez votre inscription';
    $mail->Body = $body;
    $mail->send();

    // Si la demande de RESEND provient de la page je-choisis-mon-forfait.php, on renvoi l'utilisateur sur cette page
    if ($_GET['referer'] == 'choisir-forfait') {
        ?> <script type="text/javascript"> window.location.href = 'je-choisis-mon-forfait.php?id=<?= $_GET['annonce'] ;?>&step<?= $_GET['step'] ;?>&resend=ok'; </script> <?php
    }
} elseif ($action == 'update') {
    $formemail = array_key_exists('formemail', $_POST) && !empty($_POST['formemail']) ? trim($_POST['formemail']) : null;

    if (null !== $formemail) {
        $sql = sprintf(
            'SELECT id FROM clients WHERE email = "%s" and id != "%s"',
            mysqli_real_escape_string($connect1, $formemail),
            mysqli_real_escape_string($connect1, $client->id)
        );
        $query = mysqli_query($connect1, $sql);
        $email = mysqli_fetch_object($query);

        if ($email !== null) {
            $message = 'Cette adresse email est déjà utilisée !';
            $type = 'danger';
        } else {
            $sql = sprintf(
                'UPDATE clients SET email="%s" WHERE id = "%s"',
                mysqli_real_escape_string($connect1, $formemail),
                mysqli_real_escape_string($connect1, $client->id)
            );
            mysqli_query($connect1, $sql);

            $client->email = $formemail;
            $message = 'Votre adresse email a bien été mise à jour !';
            $type = 'success';
        }

        // Si la demande de RESEND provient de la page je-choisis-mon-forfait.php, on renvoi l'utilisateur sur cette page
        if ($_GET['referer'] == 'choisir-forfait') {
            ?> <script type="text/javascript"> window.location.href = 'je-choisis-mon-forfait.php?id=<?= $_GET['annonce'] ;?>&step<?= $_GET['step'] ;?>&update=ok&message=<?= $message ;?>&type=<?= $type ;?>'; </script> <?php
        }

    }
}
?>

<div class="container">
    <h3 class="center">Confirmez votre inscription</h3>

    <div class="container">
        <?php if ($action == 'resend') : ?>
            <div class="alert alert-success" role="alert">
                Un nouvel email vous a été envoyé !
            </div>
        <?php elseif ($action == 'update'): ?>
            <div class="alert alert-<?php echo $type; ?>" role="alert">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <p>Pour des raisons de sécurité, veuillez confirmer votre inscription avant de pouvoir publier une annonce.</p>
        <p>Un email de confirmation a été envoyé à l'adresse email <strong><?php echo $client->email; ?></strong>. Il vous suffit de suivre les instructions contenues dans cet email. Si vous ne le recevez pas, pensez à contrôler dans votre boite spam.</p>
        <p>Si l'adresse email est incorrecte, veuillez cliquer sur le bouton ci-contre <a class="btn btn-inline btn-small btn-default" data-toggle="modal" data-target="#modalEmailUpdate">Corriger l'adresse email</a></p>
        <p>Si dans les 15 prochaines minutes vous n'avez pas reçu d'email de confirmation, veuillez cliquer sur le bouton ci-contre <a class="btn btn-inline btn-small btn-default" href="/inscription_confirmation.php?action=resend">Renvoyer l'email de confirmation</a></p>
    </div>
</div>

<div id="modalEmailUpdate" class="modal fade" tabindex="-1" role="dialog" style="z-index: 999;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Corriger mon adresse email</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" action="/inscription_confirmation.php?action=update" method="post">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"
                                for="formemail">Adresse email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control"
                                   id="formemail" name="formemail" placeholder="Email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Enregistrer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include("footer.php"); ?>