<?php
session_start();
include("db_connexion.php");
require_once('vendor/autoload.php');
require_once('classes/email.php');
require_once('classes/Display.php');
require_once('./constantes.php');

if (!isset($_SESSION['id_client'])) {
    header('Location: inscription.php?referer=publier.php');
    exit();
}


$id_client = mysqli_real_escape_string($connect1, $_SESSION['id_client']);
$requete = mysqli_query($connect1, 'SELECT * FROM clients WHERE id="'.$id_client.'";');
$client = mysqli_fetch_object($requete);

if ($client->confirmed_email == 0) {
    header("Location: inscription_confirmation.php");
    exit();
}


$requeteAllBanniere = mysqli_query($connect1, "SELECT * FROM commandes_bannieres");
$nbBanniere = mysqli_num_rows($requeteAllBanniere);
// $nbBanniere = 300;


if (isset($_POST['marque']))
{
    if (isset($_POST['modifier']))
    {
        $modifierid = mysqli_real_escape_string($connect1, $_POST['modifier']);
        $requete = mysqli_query($connect1, "SELECT id, image1, image2, image3, image4, image5, image6 FROM annonces_clients WHERE id='$modifierid' && id_client='$_SESSION[id_client]'");
        if(mysqli_num_rows($requete) != 0)
        {
        	$annonce = mysqli_fetch_object($requete);
        	$modifier = 1;
        }
        else
            $modifier = 0;
    }
    else
        $modifier = 0;
    // ETAPE 1: DONNEES VOITURES [DEBUT]
    include("publier_etape1_include.php");
    // ETAPE 1: DONNEES VOITURES [FIN]
    // ETAPE 2: UPLOAD PHOTOS [DEBUT]
    function createThumbnail($image_name,$new_width,$new_height,$uploadDir,$moveToDir)
    {
        $path = $uploadDir . '/' . $image_name;

        $mime = getimagesize($path);

        if($mime['mime']=='image/png'){ $src_img = imagecreatefrompng($path); }
        if($mime['mime']=='image/jpg'){ $src_img = imagecreatefromjpeg($path); }
        if($mime['mime']=='image/jpeg'){ $src_img = imagecreatefromjpeg($path); }
        if($mime['mime']=='image/pjpeg'){ $src_img = imagecreatefromjpeg($path); }

        $old_x = imagesx($src_img);
        $old_y = imagesy($src_img);

        $ratio = $old_x/$old_y; // width/height

        if( $ratio > 1) {
            if($old_x >= $new_width)
            {
                $thumb_w = $new_width;
                $thumb_h = $new_width/$ratio;
            }
            else if($old_y >= $new_height)
            {
                $thumb_w = $new_height/$ratio;
                $thumb_h = $new_height;
            }
            else
            {
                $thumb_w = $old_x;
                $thumb_h = $old_y;
            }
        }
        else {
            if($old_y >= $new_height)
            {
                $thumb_w = $new_height*$ratio;
                $thumb_h = $new_height;
            }
            else if($old_x >= $new_width)
            {
                $thumb_w = $new_width;
                $thumb_h = $new_width*$ratio;
            }
            else
            {
                $thumb_w = $old_x;
                $thumb_h = $old_y;
            }
        }

        $dst_img = imagecreatetruecolor($thumb_w,$thumb_h);
        imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

        // New save location
        $new_thumb_loc = $moveToDir . $image_name;

        if($mime['mime']=='image/png'){ $result = imagepng($dst_img,$new_thumb_loc,8); }
        if($mime['mime']=='image/jpg'){ $result = imagejpeg($dst_img,$new_thumb_loc,80); }
        if($mime['mime']=='image/jpeg'){ $result = imagejpeg($dst_img,$new_thumb_loc,80); }
        if($mime['mime']=='image/pjpeg'){ $result = imagejpeg($dst_img,$new_thumb_loc,80); }

        imagedestroy($dst_img);
        imagedestroy($src_img);

        return $result;
    }

    $imagei = 1;
    while($imagei <= 6)
    {
        if($check = @getimagesize($_FILES['image'.$imagei]["tmp_name"])) {
            if ($_FILES['image'.$imagei]["size"] <= 10000000)
            {
                $image_name = md5(basename($_FILES['image'.$imagei]["name"]).date("Y-m-d H:i:s")).".jpg";
                if(move_uploaded_file($_FILES['image'.$imagei]["tmp_name"], "/home/autospot/www/img/tmp/$image_name"))
                {
                    createThumbnail("$image_name", 800, 600, __DIR__ . "/img/tmp", __DIR__ . "/img/");
                    createThumbnail("$image_name", 100, 75, __DIR__ . "/img", __DIR__ . "/img/thumbs/");
                    unlink("/home/autospot/www/img/tmp/$image_name");
                }
                $image[$imagei-1] = $image_name;
            }
        }
        else
            $image[$imagei-1] = "";
        $imagei++;
	}
	/* 
	echo "image ::::<br />";
	var_dump($image);
	exit("");
	 */
	if($modifier == 0) {
        mysqli_query($connect1, "UPDATE annonces_clients SET image1='$image[0]', image2='$image[1]', image3='$image[2]', image4='$image[3]', image5='$image[4]', image6='$image[5]' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
    }
    else {
		
		echo "test img upload modif";
		var_dump($image);
		
		if($image[0] != "") mysqli_query($connect1, "UPDATE annonces_clients SET image1='$image[0]' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
		if($image[1] != "") mysqli_query($connect1, "UPDATE annonces_clients SET image2='$image[1]' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
		if($image[2] != "") mysqli_query($connect1, "UPDATE annonces_clients SET image3='$image[2]' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
		if($image[3] != "") mysqli_query($connect1, "UPDATE annonces_clients SET image4='$image[3]' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
		if($image[4] != "") mysqli_query($connect1, "UPDATE annonces_clients SET image5='$image[4]' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
		if($image[5] != "") mysqli_query($connect1, "UPDATE annonces_clients SET image6='$image[5]' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
		
    	if($image[0] != "")
    	{
    		unlink(__DIR__ . "/img/".$annonce->image1);
    		unlink(__DIR__ . "/img/thumbs/".$annonce->image1);
    	}
    	if($image[1] != "")
    	{
    		unlink(__DIR__ . "/img/".$annonce->image2);
    		unlink(__DIR__ . "/img/thumbs/".$annonce->image2);
    	}
    	if($image[2] != "")
    	{
    		unlink(__DIR__ . "/img/".$annonce->image3);
    		unlink(__DIR__ . "/img/thumbs/".$annonce->image3);
    	}
    	if($image[3] != "")
    	{
    		unlink(__DIR__ . "/img/".$annonce->image4);
    		unlink(__DIR__ . "/img/thumbs/".$annonce->image4);
    	}
    	if($image[4] != "")
    	{
    		unlink(__DIR__ . "/img/".$annonce->image5);
    		unlink(__DIR__ . "/img/thumbs/".$annonce->image5);
    	}
    	if($image[5] != "")
    	{
    		unlink(__DIR__ . "/img/".$annonce->image6);
    		unlink(__DIR__ . "/img/thumbs/".$annonce->image6);
		}
		
		
    }
	// ETAPE 2: UPLOAD PHOTOS [FIN]
    // ETAPE 3: SECURSPOT [DEBUT]
    if ($_POST['circulation1Mois'] != "")
    {
		if ($modifier == 1)
		{
			$requete_securspot = mysqli_query($connect1, "SELECT id, fichier FROM annonces_securspot WHERE id_annonce='$id_annonce'");
			if(mysqli_num_rows($requete_securspot) == 1)
			{
				$securspot = mysqli_fetch_object($requete_securspot);
				$id_securspot = $securspot->id;
				if($securspot->fichier != "")
					unlink(__DIR__ . "/rapports/".$securspot->fichier);
				mysqli_query($connect1, "DELETE FROM annonces_securspot WHERE id_annonce='$id_annonce'");
				mysqli_query($connect1, "DELETE FROM annonces_securspot_detail WHERE id_securspot='$id_securspot'");
			}
		}
        include("publier_etape3_include.php");
        include("securspot/rapport.php");
    }
    // ETAPE 3: SECURSPOT [FIN]
    // ETAPE 4: COMMANDE DE LA BANNIERE PUBLICITAIRE [DEBUT]
	if($modifier == 0)
	{
		$requeteBanniere = mysqli_query($connect1, "SELECT * FROM commandes_bannieres WHERE id_client='$_SESSION[id_client]'");
		if(($_POST['banniere'] == "Oui"))
		{
			$nomBanniere = mysqli_real_escape_string($connect1, $_POST['nomBanniere']);
			$prenomBanniere = mysqli_real_escape_string($connect1, $_POST['prenomBanniere']);
			$adresseBanniere = mysqli_real_escape_string($connect1, $_POST['adresseBanniere']);
			$numeroBanniere = mysqli_real_escape_string($connect1, $_POST['numeroBanniere']);
			$cpBanniere = mysqli_real_escape_string($connect1, $_POST['cpBanniere']);
			$villeBanniere = mysqli_real_escape_string($connect1, $_POST['villeBanniere']);
			
			if(mysqli_num_rows($requeteBanniere) == 0){
				mysqli_query($connect1, "INSERT INTO commandes_bannieres (id_client, nom, prenom, adresse, adresse_num, code_postal, ville) VALUES ('$_SESSION[id_client]', '$nomBanniere', '$prenomBanniere', '$adresseBanniere', '$numeroBanniere', '$cpBanniere', '$villeBanniere')") or die(mysqli_error($connect1));
			}
			/* 
			else{
				$tab = mysqli_fetch_array($requeteBanniere);
				$nomBanniere = $tab['nom'];
				$prenomBanniere = $tab['prenom'];
				$adresseBanniere = $tab['adresse'];
				$numeroBanniere = $tab['adresse_num'];
				$cpBanniere = $tab['code_postal'];
				$villeBanniere = $tab['ville'];
				
			}
			*/
		}
	}
	
	
	
	function getPost2($_key)
	{
		if(isset($_POST[$_key])) return $_POST[$_key];
		return '';
	}
	
		require_once('./classes/annonce.php');
	
    // ETAPE 4: COMMANDE DE LA BANNIERE PUBLICITAIRE [FIN]
    // ETAPE 5: MISE A JOUR ADRESSE ET NUMERO DE MOBILE DANS TOUS LES CAS [DEBUT]
    $adresseC = mysqli_real_escape_string($connect1, getPost2('adresseC'));
    $numeroC = mysqli_real_escape_string($connect1, getPost2('numeroC'));
    $cpC = mysqli_real_escape_string($connect1, getPost2('cpC'));
    $villeC = mysqli_real_escape_string($connect1, getPost2('villeC'));
    $numeroTelC = mysqli_real_escape_string($connect1, getPost2('numeroTelC'));
	$typeContactC = mysqli_real_escape_string($connect1, getPost2('typeContactC'));
	$canton = annonce::getCantonByCP($cpC);
		
		
	mysqli_query($connect1, "UPDATE clients SET adresse='$adresseC', adresse_num='$numeroC', code_postal='$cpC', ville='$villeC', telephone='$numeroTelC', typecontact='$typeContactC' WHERE id='$_SESSION[id_client]'") or die(mysqli_error($connect1));
	mysqli_query($connect1, "UPDATE annonces_clients SET codepostal='$cpC', ville='$villeC', canton='$canton' WHERE id='$id_annonce'") or die(mysqli_error($connect1));
	
	
    // ETAPE 5: MISE A JOUR ADRESSE ET NUMERO DE MOBILE DANS TOUS LES CAS [FIN]
    unset($_SESSION['id_securspot']);

    if ($modifier == 0) {
        $loader = new \Twig_Loader_Filesystem(__DIR__.'/emails');
        $twig = new \Twig_Environment($loader);
        $body = $twig->render('nouvelle_publication.html.twig', [
            'url' => sprintf('http://www.autospot.ch/appbackend/admin/?pwd=tdxyeyUWbDZMY8VH'),
            'annonce' => $id_annonce,
            'adresse' => $_POST['banniere'] == 'Oui' ? $_POST['adresseBanniere'] . ' ' . $_POST['cpBanniere'] . ' ' . $_POST['villeBanniere'] : '',
            'banniere' => $_POST['banniere'],
        ]);

        $mail = email::init();

        // Generate PDF and attach it to email
        if ($_POST['banniere'] == 'Oui') {
					
						$firstname = $client->prenom;
						$lastname = $client->nom;
						$address = $numeroC . ' ' . $adresseC;
						$zipcode = $cpC;
						$city = $villeC;
						
						if(isset($prenomBanniere)) $firstname = $prenomBanniere;
						if(isset($nomBanniere)) $lastname = $nomBanniere;
						if(isset($adresseBanniere)) $address = $numeroBanniere . ' ' . $adresseBanniere;
						if(isset($cpBanniere)) $zipcode = $cpBanniere;
						if(isset($villeBanniere)) $city = $villeBanniere;
						
						
            $now = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
            $loader = new \Twig_Loader_Filesystem(__DIR__.'/documents');
            $twig = new \Twig_Environment($loader);
            $html = $twig->render('bulletin_livraison.html.twig', [
                'bootstrap_css' => __DIR__ . '/css/bootstrap.min.css',
                'logo' => __DIR__ . '/skaiweb.png',
                'annonce' => $id_annonce,
                'date' => $now->format('d/m/Y'),
                'firstname' => $firstname,
                'lastname' => $lastname,
                'address' => $address,
                'zipcode' => $zipcode,
                'city' => $city,
			]);
			
			

            $filePath = __DIR__ . '/tmp/' . $id_annonce . '.pdf';
            if (PHP_OS == 'Darwin') {
                $binary = '/usr/local/bin/wkhtmltopdf';
            } else {
                $binary = __DIR__ . '/vendor/bin/wkhtmltopdf-amd64';
            }

            $snappy = new \Knp\Snappy\Pdf($binary);
            $pdfContent = $snappy->generateFromHtml($html, $filePath);
            $mail->addAttachment($filePath, $name = 'Bulletin-de-livraison.pdf',  'base64', 'application/pdf');
        }

        $mail->setFrom('contact@autospot.ch', 'AutoSpot');
        $mail->addAddress('publications@autospot.ch');
        // $mail->addAddress('vincent.huss@gmail.com');
        $mail->addReplyTo($client->email);
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Nouvelle publication d\'annonce !';
        $mail->Body = $body;
        $mail->send();

        if (isset($filePath) && file_exists($filePath)) {
            unset($filePath);
        }
    }

    header("Location: compte.php?annonces");
    exit();
}
include("header.php");
include("body.php");
if(isset($_GET['modifier']))
{
    $id_annonce = mysqli_real_escape_string($connect1, $_GET['modifier']);
    $id_client = mysqli_real_escape_string($connect1, $_SESSION['id_client']);
    $requete_annonces = mysqli_query($connect1, "SELECT * FROM annonces_clients WHERE id='$id_annonce' && id_client='$id_client'");
    if(mysqli_num_rows($requete_annonces) == 1)
    {
        $annonces = mysqli_fetch_object($requete_annonces);
        $requete_securspot = mysqli_query($connect1, "SELECT * FROM annonces_securspot WHERE id_annonce='$annonces->id'");
        if(mysqli_num_rows($requete_securspot) == 1)
        {
            $securspot = mysqli_fetch_object($requete_securspot);
            $requete_securspot_details = mysqli_query($connect1, "SELECT * FROM annonces_securspot_detail WHERE id_securspot='$securspot->id'");
            $securspot_details = mysqli_fetch_object($requete_securspot_details);
        }
    }
}
?>
<a id="haut"></a>

<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<style type="text/css">
    .datepicker.dropdown-menu { min-width: 230px; }
    .datepicker th.prev, .datepicker th.next, .datepicker th.datepicker-switch { cursor: pointer; }
    .datepicker th.prev:hover, .datepicker th.next:hover, .datepicker th.datepicker-switch:hover { background-color: #eeeeee; }
</style>

<!-- CONTAINER FORMULAIRE EN 5 étapes -->
<div class="container">
	
<?php
$index_step =0;
function getStepNum()
{
	global $index_step;
	$index_step++;
	return $index_step;
}
?>

<div class="stepwizard col-sm-12">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <p>Données<br/>& description</p>
          <a href="#step-1" type="button" class="btn btn-primary btn-circle"><?php echo getStepNum();?></a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>
      <div class="stepwizard-step">
        <p>Photographies</p>
          <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"><?php echo getStepNum();?></a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
	  </div>
	  
	  <?php if($nbBanniere < 200){ ?>
	  
      <div class="stepwizard-step">
        <p>Bannière<br>auto</p>
          <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"><?php echo getStepNum();?></a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
	  </div>
	  <?php } ?>
	  
      <div class="stepwizard-step">
        <p>Contact<br/>&amp; confirmation</p>
            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"><?php echo getStepNum();?></a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>
        <!--<div class="stepwizard-step">
        <p>Contact</p>
            <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
        <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
      </div>-->
    </div>
  </div>
 <h3>Publier une annonce</h3>

    <div id="redAlert" class="alert alert-danger">
        <span class="lettrine"><i class="fa fa-exclamation" aria-hidden="true"></i></span> <strong>Données manquantes :</strong> Veuillez remplir les champs marqués en rouges.
</div>

  <form role="form" id="publier" name="publier" action="publier.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="modifier" value="<?PHP if(isset($id_annonce)) echo $id_annonce;?>">

    <!-- ÉTAPE 1 : DONNÉES & DESCRIPTION -->
    <div class="setup-content" id="step-1" data-page='publier'>

        <div class="col-md-12">
            <hr class="blue">
            <div class="col-sm-4"><h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Données de base :</h4></div>
            <div class="col-sm-8"></span></div>
            <hr class="clear blue">

            <div class="form-group">
    <label class="control-label col-sm-4" for="marque">Marque : <span class="requis">*</span></label>
    <div class="col-sm-7">
				<?PHP
				if((isset($_GET['r'])) && ($_GET['r'] == "Toutes"))
					$requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche ORDER BY priorite DESC, marque ASC");
				else if(isset($_GET['r']))
					$requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='$_GET[r]' ORDER BY priorite DESC, marque ASC");
				else
				{
					if($_SERVER['SERVER_NAME'] == "www.motospot.ch")
                        $requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='moto' ORDER BY priorite DESC, marque ASC");
					else
						$requetetr = mysqli_query($connect1, "SELECT marque FROM formulaires_recherche WHERE choix='voiture' ORDER BY priorite DESC, marque ASC");
				}
				$marqueslist = "";
				while($marques = mysqli_fetch_object($requetetr))
				{
					$quotedmarques = preg_quote($marques->marque);
					if((!preg_match("/\|$quotedmarques\|/ui", $marqueslist)) && ($marques->marque != ""))
						$marqueslist = $marqueslist."|$marques->marque|";
				}
				$marqueslist = preg_replace("/^\|/", "", $marqueslist);
				$marqueslist = preg_replace("/\|$/", "", $marqueslist);
				?>
  <select class="form-control input-sm" id="marque" name="marque" onchange="changemarque2();" required>
		<option value=""></option>
		<?php
		$i = 0;
		$explode = explode("||", $marqueslist);
		while (isset($explode[$i])) {
			$marque = $explode[$i];
			if (preg_match("/$marque/i", $getq)) {
                $getmarque = $marque;
			}
			$getq = null;
			?>
            <option value="<?php echo $marque; ?>" <?php if(((isset($_GET["marque"])) && ($_GET["marque"] == $marque)) || (preg_match("/$marque/i", $getq))) echo "selected"; else if((isset($id_annonce)) && ($annonces->marque == $marque)) { echo "selected"; $getmarque = $marque; }?>><?PHP echo $marque;?></option><?PHP
			$i++;
		}
		?>
    </select>
    </div>
  </div>
        <div class="clear"></div>

  <div class="form-group">
    <label class="control-label col-sm-4" for="modele">Modèle : <span class="requis">*</span></label>
    <div class="col-sm-7">
	<?PHP
        $requetetr = mysqli_query($connect1, "SELECT * FROM formulaires_recherche WHERE modele != '' AND marque != '' ORDER BY modele ASC");
        $i = 0;
        while($modeles = mysqli_fetch_object($requetetr))
        {
            $modeleslist[$i]['marque'] = $modeles->marque;
            $modeleslist[$i]['modele'] = $modeles->modele;
            $i++;
        }
	?>

    <div class="hidden" id="modeleslist">
        <?php 
            $string = '';
            foreach ($modeleslist as $key => $modele_infos) {
                $string .= $modele_infos['marque'] .','.$modele_infos['modele'].';';
            }
            echo rtrim($string, ';');
        ?>
    </div>

    <?php  
    if (isset($annonces->modele)) {
        echo "<div class='hidden' id='modeleselected'>$annonces->modele</div>";
    }
    ?>

    <select class="form-control input-sm" id="modele" name="modele" required>
			<option value=""></option>
  </select>
    </div>
  </div>
        <div class="clear"></div>


    <script type="text/javascript">
        function changemarque2() {
            var modeleslist = $('#modeleslist').text().split(";"),
                marque_selected = $('#marque').val(),
                modeleselected = $('#modeleselected').text(),
                options = ['<option value=""></option>'];
            for (var i = 0; i < modeleslist.length; i++) {
                var split = modeleslist[i].split(','),
                    marque = split[0],
                    modele = split[1];

                if (marque == marque_selected) {
                    if (modele == modeleselected) { var selected = 'selected'; }else{ var selected = ''; }
                    option = '<option value='+modele+' '+selected+'>'+modele+'</option>';
                    options.push(option);
                }
            }
            $('#modele').html(options.join(''));
        }
        changemarque2();
    </script>


  <div class="form-group">
      <label class="control-label col-sm-4" for="version">Version : </label>
    <div class="col-sm-7">
      <input type="text" class="form-control" id="version" name="version" value="<?PHP if(isset($id_annonce)) echo $annonces->version;?>">
    </div></div>
        <div class="clear"></div>

      <div class="form-group">
      <label class="control-label col-sm-4" for="sieges">Nombre de places :</label>
    <div class="col-sm-7">
    <select class="form-control input-sm" id="sieges" name="sieges">
        <option value="" disabled style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>></option>
        <option value="1" <?PHP if((isset($id_annonce)) && ($annonces->sieges == 1)) echo "selected";?>>1</option>
        <option value="2" <?PHP if((isset($id_annonce)) && ($annonces->sieges == 2)) echo "selected";?>>2</option>
        <option value="3" <?PHP if((isset($id_annonce)) && ($annonces->sieges == 3)) echo "selected";?>>3</option>
        <option value="4" <?PHP if((isset($id_annonce)) && ($annonces->sieges == 4)) echo "selected";?>>4</option>
        <option value="5" <?PHP if((isset($id_annonce)) && ($annonces->sieges == 5)) echo "selected";?>>5</option>
        <option value="6" <?PHP if((isset($id_annonce)) && ($annonces->sieges == 6)) echo "selected";?>>6</option>
        <option value="7" <?PHP if((isset($id_annonce)) && ($annonces->sieges == 7)) echo "selected";?>>7</option>
    </select>
    </div></div>
        <div class="clear"></div>

       <div class="form-group">
           <label class="control-label col-sm-4" for="premiereimm">Date de la 1ère immatriculation :</label>
    <div class="col-sm-7">
      <input type="text" class="form-control datepicker" id="premiereimm" name="premiereimm" placeholder="mm/yyyy" value="<?PHP if(isset($id_annonce) && $annonces->premiereimm != '0000-00-00') echo substr($annonces->premiereimm, 0, 7);?>">
    </div></div>
        <div class="clear"></div>
        
       <div class="form-group">
           <label class="control-label col-sm-4" for="derniereimm">Date de la dernière expertise :</label>
    <div class="col-sm-7">
      <input type="text" class="form-control datepicker" id="derniereimm" name="derniereimm" placeholder="mm/yyyy" value="<?PHP if(isset($id_annonce) && $annonces->derniereimm != '0000-00-00') echo substr($annonces->derniereimm, 0, 7);?>">
    </div></div>
        <div class="clear"></div>

        <div class="form-group">
           <label class="control-label col-sm-4" for="cylindrees">Cylindrée : <span class="requis">*</span></label>
    <div class="col-sm-7">
    <select class="form-control input-sm" id="cylindrees" name="cylindrees" required>
        <option value="" disabled style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>></option>
        <option value="600" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 600)) echo "selected";?>>600</option>
        <option value="800" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 800)) echo "selected";?>>800</option>
        <option value="1000" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 1000)) echo "selected";?>>1000</option>
        <option value="1200" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 1200)) echo "selected";?>>1200</option>
        <option value="1400" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 1400)) echo "selected";?>>1400</option>
        <option value="1600" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 1600)) echo "selected";?>>1600</option>
        <option value="1800" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 1800)) echo "selected";?>>1800</option>
        <option value="2000" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 2000)) echo "selected";?>>2000</option>
        <option value="2500" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 2500)) echo "selected";?>>2500</option>
        <option value="3000" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 3000)) echo "selected";?>>3000</option>
        <option value="3500" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 3500)) echo "selected";?>>3500</option>
        <option value="4000" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 4000)) echo "selected";?>>4000</option>
        <option value="5000" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 5000)) echo "selected";?>>5000</option>
        <option value="5500" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 5500)) echo "selected";?>>5500</option>
        <option value="6000" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 6000)) echo "selected";?>>6000</option>
        <option value="6500" <?PHP if((isset($id_annonce)) && ($annonces->cylindrees == 6500)) echo "selected";?>>6500</option>
    </select>
    </div></div>
        <div class="clear"></div>

        <div class="form-group">
           <label class="control-label col-sm-4" for="puissancechevaux">Puissance en CV : <span class="requis">*</span></label>
    <div class="col-sm-7">
      <input type="number" min="1" max="9999" step="1" class="form-control" id="puissancechevaux" name="puissancechevaux" value="<?PHP if(isset($id_annonce)) echo $annonces->puissancechevaux;?>" required>
    </div></div>
        <div class="clear"></div>

        <hr class="blue">
        <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Données à compléter :</h4>
        <hr class="clear blue">

      <div class="form-group">
           <label class="control-label col-sm-4 col-xs-12" for="couleurexterne">Couleur extérieure : <span class="requis">*</span></label>
    <div class="col-sm-4 col-xs-6">
      <select class="form-control input-sm" id="couleurexterne" name="couleurexterne" required>
          <option value="" style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>>Choisir la couleur extérieure</option>
          <option value="anthracite" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "anthracite")) echo "selected";?>>Anthracite</option>
          <option value="argent" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "argent")) echo "selected";?>>Argent</option>
          <option value="beige" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "beige")) echo "selected";?>>Beige</option>
          <option value="blanc" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "blanc")) echo "selected";?>>Blanc</option>
          <option value="bleu" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "bleu")) echo "selected";?>>Bleu</option>
          <option value="bordeaux" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "bordeaux")) echo "selected";?>>Bordeaux</option>
          <option value="brun" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "brun")) echo "selected";?>>Brun</option>
          <option value="gris" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "gris")) echo "selected";?>>Gris</option>
          <option value="jaune" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "jaune")) echo "selected";?>>Jaune</option>
          <option value="noir" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "noir")) echo "selected";?>>Noir</option>
          <option value="or" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "or")) echo "selected";?>>Or</option>
          <option value="orange" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "orange")) echo "selected";?>>Orange</option>
          <option value="rose" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "rose")) echo "selected";?>>Rose</option>
          <option value="rouge" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "rouge")) echo "selected";?>>Rouge</option>
          <option value="turquoise" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "turquoise")) echo "selected";?>>Truquoise</option>
          <option value="vert" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "vert")) echo "selected";?>>Vert</option>
          <option value="violet" <?PHP if((isset($id_annonce)) && ($annonces->couleurexterne == "violet")) echo "selected";?>>Violet</option>
  </select>
    </div>
        <div class="col-sm-3 col-xs-6 checkbox checkbox-primary" style="text-align: right; padding-top: 4px;">
      <input id="metallisee" type="checkbox" value="1" name="metallisee" <?PHP if((isset($id_annonce)) && ($annonces->metallisee == "1")) echo "checked";?>>
		<label for="metallisee" class="notReqBlack">Peinture métallisée</label>
          </div></div>
        <div class="clear"></div>

        <div class="form-group">
           <label class="control-label col-sm-4" for="categorie">Catégorie : <span class="requis">*</span></label>
    <div class="col-sm-7">
        <select class="form-control input-sm" id="categorie" name="categorie" required>
  				<option value="" style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>>Choisir la catégorie</option>
					<option value="occasion" <?PHP if((isset($id_annonce)) && ($annonces->categorie == "occasion")) echo "selected";?>>Occasion</option>
					<option value="demonstration" <?PHP if((isset($id_annonce)) && ($annonces->categorie == "demonstration")) echo "selected";?>>Démonstration</option>
          <option value="collection" <?PHP if((isset($id_annonce)) && ($annonces->categorie == "collection")) echo "selected";?>><?PHP echo $LANG[44]; ?></option>
					<?php
					if($client->type != CLIENT_TYPE_CLIENT){
						?>
						<option value="neuf" <?PHP if((isset($id_annonce)) && ($annonces->categorie == "neuf")) echo "selected";?>><?PHP echo "Neuf"; ?></option>
						<?php
					}
					?>
  			</select>
    </div></div>   
        <div class="clear"></div>

        <div class="form-group">
           <label class="control-label col-sm-4" for="carrosserie">Type de carrosserie : <span class="requis">*</span></label>
            <div class="col-sm-7">
                <select class="form-control input-sm selectpicker" id="carrosserie" name="carrosserie" required>
    <option data-content="<i class='fa fa-car'></i> Choisir le type de carrosserie" value="" <?PHP if(!isset($id_annonce)) echo "selected";?>></option>
    <option data-content="<img src='images/Berline.png'> Berline" value="berline" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "berline")) echo "selected";?>></option>
    <option data-content="<img src='images/Break.png'> Break" value="break" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "break")) echo "selected";?>></option>
    <option data-content="<img src='images/Cabriolet.png'> Cabriolet" value="cabriolet" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "cabriolet")) echo "selected";?>></option>
    <option data-content="<img src='images/Coupe.png'> Coupé" value="coupe" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "coupe")) echo "selected";?>></option>
    <option data-content="<img src='images/Monospace.png'> Monospace" value="monospace" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "monospace")) echo "selected";?>></option>
    <option data-content="<img src='images/Petite-voiture-citadine.png'> Petite citadine" value="citadine" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "citadine")) echo "selected";?>></option>
    <option data-content="<img src='images/SUV-tout-terrain.png'> SUV tout-terrain" value="suv" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "suv")) echo "selected";?>></option>
    <option data-content="<img src='images/Pick-up.png'> Pick-up" value="pickup" <?PHP if((isset($id_annonce)) && ($annonces->carrosserie == "pickup")) echo "selected";?>></option>

</select>
  <div class="clear"></div>
</div>

    </div>
        <div class="clear"></div>

        <div class="form-group">
           <label class="control-label col-sm-4" for="transmission">Transmission : <span class="requis">*</span></label>
    <div class="col-sm-7">
        <select class="form-control input-sm" id="transmission" name="transmission" required>
                <option value="" style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>>Choisir la transmission</option>
				<option value="manuelle" <?PHP if((isset($id_annonce)) && ($annonces->transmission == "manuelle")) echo "selected";?>>Boîte manuelle</option>
				<option value="automatique" <?PHP if((isset($id_annonce)) && ($annonces->transmission == "automatique")) echo "selected";?>>Boîte automatique</option>
				<option value="sequentielle" <?PHP if((isset($id_annonce)) && ($annonces->transmission == "sequentielle")) echo "selected";?>>Boîte séquentielle</option>
  </select>
    </div></div>
        <div class="clear"></div>
		
        <div class="form-group">
           <label class="control-label col-sm-4" for="rouesMotrices">Roues motrices :</label>
    <div class="col-sm-7">
        <select class="form-control input-sm" id="rouesMotrices" name="rouesMotrices">
                <option value="" style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>>Choisir les roues motrices</option>
				<option value="4 roues" <?PHP if((isset($id_annonce)) && ($annonces->rouesmotrices == "4 roues")) echo "selected";?>>4 roues</option>
				<option value="traction arrière" <?PHP if((isset($id_annonce)) && ($annonces->rouesmotrices == "traction arrière")) echo "selected";?>>Traction arrière</option>
				<option value="traction avant" <?PHP if((isset($id_annonce)) && ($annonces->rouesmotrices == "traction avant")) echo "selected";?>>Traction avant</option>
  </select>
    </div></div>
        <div class="clear"></div>

    <div class="form-group" id="group-vitesses">
           <label class="control-label col-sm-4" for="vitesses">Nombre de vitesse : <span class="requis">*</span></label>
    <div class="col-sm-7">
        <select class="form-control input-sm" id="vitesses" name="vitesses" required>
            <option value="" style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>>Choisir le nombre de vitesses</option>
            <option value="5" <?PHP if((isset($id_annonce)) && ($annonces->vitesses == "5")) echo "selected";?>>5</option>
            <option value="6" <?PHP if((isset($id_annonce)) && ($annonces->vitesses == "6")) echo "selected";?>>6</option>
            <option value="7" <?PHP if((isset($id_annonce)) && ($annonces->vitesses == "7")) echo "selected";?>>7</option>
            <option value="7" <?PHP if((isset($id_annonce)) && ($annonces->vitesses == "8")) echo "selected";?>>8</option>
            <option value="7" <?PHP if((isset($id_annonce)) && ($annonces->vitesses == "9")) echo "selected";?>>9</option>
  </select>
    </div></div>
        <div class="clear"></div>

        <div class="form-group">
           <label class="control-label col-sm-4" for="portes">Nombre de portes : <span class="requis">*</span></label>
    <div class="col-sm-7">
        <select class="form-control input-sm" id="portes" name="portes" required>
            <option value="" style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>>Choisir le nombre de portes</option>
            <option value="3" <?PHP if((isset($id_annonce)) && ($annonces->portes == "3")) echo "selected";?>>3 portes</option>
            <option value="5" <?PHP if((isset($id_annonce)) && ($annonces->portes == "5")) echo "selected";?>>5 portes</option>
  </select>
    </div></div>
        <div class="clear"></div>

        <div class="form-group">
           <label class="control-label col-sm-4" for="carburant">Carburant : <span class="requis">*</span></label>
    <div class="col-sm-7">
        <select class="form-control input-sm" id="carburant" name="carburant" required>
            <option value="" style="display: none;" <?PHP if(!isset($id_annonce)) { ?>selected<?PHP }?>>Choisir le carburant</option>
            <option value="essence" <?PHP if((isset($id_annonce)) && ($annonces->carburant == "essence")) echo "selected";?>>Essence</option>
            <option value="diesel" <?PHP if((isset($id_annonce)) && ($annonces->carburant == "diesel")) echo "selected";?>>Diesel</option>
            <option value="hybride" <?PHP if((isset($id_annonce)) && ($annonces->carburant == "hybride")) echo "selected";?>>Hybride</option>
            <option value="électrique" <?PHP if((isset($id_annonce)) && ($annonces->carburant == "électrique")) echo "selected";?>>Électrique</option>
            <option value="gaz" <?PHP if((isset($id_annonce)) && ($annonces->carburant == "gaz")) echo "selected";?>>Gaz</option>
            <option value="bioethanol" <?PHP if((isset($id_annonce)) && ($annonces->carburant == "bioethanol")) echo "selected";?>>Bioéthanol</option>
  </select>
    </div></div>
        <div class="clear"></div>

        <div class="form-group">
      <label class="control-label col-sm-4 col-xs-12" for="kilometrage">Kilométrage exact : <span class="requis">*</span></label>
    <div class="col-sm-7">
      <input type="number" min="0" max="300000" step="1" class="form-control" id="kilometrage" name="kilometrage" value="<?PHP if(isset($id_annonce)) echo $annonces->kilometrage;?>" required>
    </div></div>
        <div class="clear"></div>

        <div class="form-group">
      <label class="control-label col-sm-4 col-xs-12" for="prix">Prix en CHF : <span class="requis">*</span></label>
    <div class="col-sm-4 col-xs-6">
      <input type="number" min="0" max="2000000" step="1" class="form-control" id="prix" name="prix" value="<?PHP if(isset($id_annonce)) echo $annonces->prix;?>" required>
    </div>
        <div class="col-sm-2 col-xs-offset-1 col-xs-5 checkbox checkbox-primary" style="padding-top: 5px;">
      <input id="discuter" type="checkbox" value="oui" name="discuter" <?PHP if((isset($id_annonce)) && ($annonces->prixdiscuter == "1")) echo "checked";?>>
		<label for="discuter" class="notReqBlack">à discuter</label>
          </div></div>
        <div class="clear"></div>

        <div class="form-group">
      <label class="control-label col-sm-4 col-xs-12" for="prixneuf">Prix neuf (facultatif) : <br />
          <small>prix de base + options</small></label>
    <div class="col-sm-7">
      <input type="number" min="0" max="2000000" step="1" class="form-control" id="prixneuf" name="prixneuf" value="<?PHP if(isset($id_annonce)) echo $annonces->prixneuf;?>">
    </div></div>
        <div class="clear"></div>

        <!-- ACCORDEON EQUIPEMENTS -->
    <hr class="blue">
    <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Équipement à compléter :</h4>
        <hr class="clear blue">
    <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse1" class="black collapsed">
          <h4 class="panel-title">
          <i class="fa fa-hand-o-right" aria-hidden="true"></i> Sécurité
              <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span>
          </h4></a>
    </div>
    <div id="collapse1" class="panel-collapse collapse">

        <div class="panel-body form-group">

    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="airbag" type="checkbox" value="Airbag conducteur et passager" name="airbag" <?PHP if((isset($id_annonce)) && (preg_match("/Airbag conducteur et passager/", $annonces->equipement))) echo "checked";?>><label for="airbag">Airbag conducteur et passager</label><br/>
          <input id="airbagL" type="checkbox" value="Airbag latéraux pour conducteur et passager" name="airbagL" <?PHP if((isset($id_annonce)) && (preg_match("/Airbag latéraux pour conducteur et passager/", $annonces->equipement))) echo "checked";?>><label for="airbagL">Airbag latéraux pour conducteur et passager</label><br/>
          <input id="isofix" type="checkbox" value="Fixation isofix pour siège enfant" name="isofix" <?PHP if((isset($id_annonce)) && (preg_match("/Fixation isofix pour siège enfant/", $annonces->equipement))) echo "checked";?>><label for="isofix">Fixation isofix pour siège enfant</label><br/>
          <input id="feuxAv" type="checkbox" value="Feux anti-brouillard avant" name="feuxAv" <?PHP if((isset($id_annonce)) && (preg_match("/Feux anti-brouillard avant/", $annonces->equipement))) echo "checked";?>><label for="feuxAv">Feux anti-brouillard avant</label><br/>
          <input id="feuxAr" type="checkbox" value="Feux anti-brouillard arrière" name="feuxAr" <?PHP if((isset($id_annonce)) && (preg_match("/Feux anti-brouillard arrière/", $annonces->equipement))) echo "checked";?>><label for="feuxAr">Feux anti-brouillard arrière</label><br/>
          <input id="phareDir" type="checkbox" value="Phares directionnels" name="phareDir" <?PHP if((isset($id_annonce)) && (preg_match("/Phares directionnels/", $annonces->equipement))) echo "checked";?>><label for="phareDir">Phares directionnels</label><br/>
          <input id="xenon" type="checkbox" value="Éclairage Xénon" name="xenon" <?PHP if((isset($id_annonce)) && (preg_match("/Éclairage Xénon/", $annonces->equipement))) echo "checked";?>><label for="xenon">Éclairage Xénon</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="verrCent" type="checkbox" value="Verrouillage centralisé" name="verrCent" <?PHP if((isset($id_annonce)) && (preg_match("/Verrouillage centralisé/", $annonces->equipement))) echo "checked";?>><label for="verrCent">Verrouillage centralisé</label><br/>
          <input id="verrCentDist" type="checkbox" value="Verrouillage centralisé à distance" name="verrCentDist" <?PHP if((isset($id_annonce)) && (preg_match("/Verrouillage centralisé à distance/", $annonces->equipement))) echo "checked";?>><label for="verrCentDist">Verrouillage centralisé à distance</label><br/>
          <input id="x3feustop" type="checkbox" value="3ème feu de stop" name="x3feustop" <?PHP if((isset($id_annonce)) && (preg_match("/3ème feu de stop/", $annonces->equipement))) echo "checked";?>><label for="x3feustop">3ème feu de stop</label><br/>
          <input id="ledRetro" type="checkbox" value="Led clignotant placé dans les rétroviseurs extérieurs" name="ledRetro" <?PHP if((isset($id_annonce)) && (preg_match("/Led clignotant placé dans les rétroviseurs extérieurs/", $annonces->equipement))) echo "checked";?>><label for="ledRetro">Led clignotant placé dans les rétroviseurs extérieurs</label><br/>
          <input id="abs" type="checkbox" value="ABS (système anti-blocage des roues)" name="abs" <?PHP if((isset($id_annonce)) && (preg_match("/ABS \(système anti-blocage des roues\)/", $annonces->equipement))) echo "checked";?>><label for="abs">ABS (système anti-blocage des roues)</label><br/>
          <input id="asr" type="checkbox" value="ASR (anti-patinage des roues)" name="asr" <?PHP if((isset($id_annonce)) && (preg_match("/ASR \(anti-patinage des roues\)/", $annonces->equipement))) echo "checked";?>><label for="asr">ASR (anti-patinage des roues)</label><br/>
          <input id="esp" type="checkbox" value="ESP (contrôle de stabilité)" name="esp" <?PHP if((isset($id_annonce)) && (preg_match("/ESP \(contrôle de stabilité\)/", $annonces->equipement))) echo "checked";?>><label for="esp">ESP (contrôle de stabilité)</label>
        </div>
    </div></div>

    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse2" class="black collapsed">
          <h4 class="panel-title">
        <i class="fa fa-hand-o-right" aria-hidden="true"></i> Confort
          <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span>
          </h4></a>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body form-group">
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="dirAss" type="checkbox" value="Direction assistée" name="dirAss" <?PHP if((isset($id_annonce)) && (preg_match("/Direction assistée/", $annonces->equipement))) echo "checked";?>><label for="dirAss">Direction assistée</label><br/>
          <input id="clim" type="checkbox" value="Climatisation" name="clim" <?PHP if((isset($id_annonce)) && (preg_match("/Climatisation/", $annonces->equipement))) echo "checked";?>><label for="clim">Climatisation</label><br/>
          <input id="climAuto" type="checkbox" value="Climatisation automatique" name="climAuto" <?PHP if((isset($id_annonce)) && (preg_match("/Climatisation automatique/", $annonces->equipement))) echo "checked";?>><label for="climAuto">Climatisation automatique</label><br/>
          <input id="vitreTeinte" type="checkbox" value="Vitres teintées" name="vitreTeinte" <?PHP if((isset($id_annonce)) && (preg_match("/Vitres teintées/", $annonces->equipement))) echo "checked";?>><label for="vitreTeinte">Vitres teintées</label><br/>
          <input id="vitreElec" type="checkbox" value="Vitres avant et arrière électriques" name="vitreElec" <?PHP if((isset($id_annonce)) && (preg_match("/Vitres avant et arrière électriques/", $annonces->equipement))) echo "checked";?>><label for="vitreElec">Vitres avant et arrière électriques</label><br/>
          <input id="volantCuir" type="checkbox" value="Volant en cuir" name="volantCuir" <?PHP if((isset($id_annonce)) && (preg_match("/Volant en cuir/", $annonces->equipement))) echo "checked";?>><label for="volantCuir">Volant en cuir</label><br/>
          <input id="volantChauf" type="checkbox" value="Volant chauffant" name="volantChauf" <?PHP if((isset($id_annonce)) && (preg_match("/Volant chauffant/", $annonces->equipement))) echo "checked";?>><label for="volantChauf">Volant chauffant</label><br/>
          <input id="siegeCuir" type="checkbox" value="Sièges en cuir" name="siegeCuir" <?PHP if((isset($id_annonce)) && (preg_match("/Sièges en cuir/", $annonces->equipement))) echo "checked";?>><label for="siegeCuir">Sièges en cuir</label><br/>
          <input id="siegeChauf" type="checkbox" value="Sièges avant chauffant" name="siegeChauf" <?PHP if((isset($id_annonce)) && (preg_match("/Sièges avant chauffant/", $annonces->equipement))) echo "checked";?>><label for="siegeChauf">Sièges avant chauffant</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="detecteurPluie" type="checkbox" value="Détecteur de luminosité et de pluie" name="detecteurPluie" <?PHP if((isset($id_annonce)) && (preg_match("/Détecteur de luminosité et de pluie/", $annonces->equipement))) echo "checked";?>><label for="detecteurPluie">Détecteur de luminosité et de pluie</label><br/>
          <input id="essuieInter" type="checkbox" value="Essuie-glaces avant avec intervalle" name="essuieInter" <?PHP if((isset($id_annonce)) && (preg_match("/Essuie-glaces avant avec intervalle/", $annonces->equipement))) echo "checked";?>><label for="essuieInter">Essuie-glaces avant avec intervalle</label><br/>
          <input id="regVitesse" type="checkbox" value="Régulateur de vitesse" name="regVitesse" <?PHP if((isset($id_annonce)) && (preg_match("/Régulateur de vitesse/", $annonces->equipement))) echo "checked";?>><label for="regVitesse">Régulateur de vitesse</label><br/>
          <input id="startstop" type="checkbox" value="Fonction Start/Stop" name="startstop" <?PHP if((isset($id_annonce)) && (preg_match("/Fonction Start\/Stop/", $annonces->equipement))) echo "checked";?>><label for="startstop">Fonction Start/Stop</label><br/>
          <input id="retroElec" type="checkbox" value="Rétroviseurs extérieurs à dégivrage et réglage électrique" name="retroElec" <?PHP if((isset($id_annonce)) && (preg_match("/Rétroviseurs extérieurs à dégivrage et réglage électrique/", $annonces->equipement))) echo "checked";?>><label for="retroElec">Rétroviseurs extérieurs à dégivrage et réglage électrique</label><br/>
          <input id="parcageAr" type="checkbox" value="Capteur de distance de parcage arrière" name="parcageAr" <?PHP if((isset($id_annonce)) && (preg_match("/Capteur de distance de parcage arrière/", $annonces->equipement))) echo "checked";?>><label for="parcageAr">Capteur de distance de parcage arrière</label><br/>
          <input id="parcageAvAr" type="checkbox" value="Capteur de distance de parcage avant et arrière" name="parcageAvAr" <?PHP if((isset($id_annonce)) && (preg_match("/Capteur de distance de parcage avant et arrière/", $annonces->equipement))) echo "checked";?>><label for="parcageAvAr">Capteur de distance de parcage avant et arrière</label><br/>
          <input id="cameraRecul" type="checkbox" value="Caméra de recul" name="cameraRecul" <?PHP if((isset($id_annonce)) && (preg_match("/Caméra de recul/", $annonces->equipement))) echo "checked";?>><label for="cameraRecul">Caméra de recul</label><br/>
          <input id="barreToit" type="checkbox" value="Barres de toit" name="barreToit" <?PHP if((isset($id_annonce)) && (preg_match("/Barres de toit/", $annonces->equipement))) echo "checked";?>><label for="barreToit">Barres de toit</label>
        </div>
    </div></div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse3" class="black collapsed">
          <h4 class="panel-title">
        <i class="fa fa-hand-o-right" aria-hidden="true"></i> Divertissements
              <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span>
      </h4></a>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body form-group">
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="ordinateur" type="checkbox" value="Ordinateur de bord" name="ordinateur" <?PHP if((isset($id_annonce)) && (preg_match("/Ordinateur de bord/", $annonces->equipement))) echo "checked";?>><label for="ordinateur">Ordinateur de bord</label><br/>
          <input id="gps" type="checkbox" value="Système de navigation GPS" name="gps" <?PHP if((isset($id_annonce)) && (preg_match("/Système de navigation GPS/", $annonces->equipement))) echo "checked";?>><label for="gps">Système de navigation GPS</label><br/>
          <input id="radio" type="checkbox" value="Radio / CD" name="radio" <?PHP if((isset($id_annonce)) && (preg_match("/Radio \/ CD/", $annonces->equipement))) echo "checked";?>><label for="radio">Radio / CD</label><br/>
          <input id="usb" type="checkbox" value="Connexion USB et AUX" name="usb" <?PHP if((isset($id_annonce)) && (preg_match("/Connexion USB et AUX/", $annonces->equipement))) echo "checked";?>><label for="usb">Connexion USB et AUX</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="volantMultiF" type="checkbox" value="Volant multi-fonctions" name="volantMultiF" <?PHP if((isset($id_annonce)) && (preg_match("/Volant multi-fonctions/", $annonces->equipement))) echo "checked";?>><label for="volantMultiF">Volant multi-fonctions</label><br/>
          <input id="bluetooth" type="checkbox" value="Interface Bluetooth et téléphone mobile" name="bluetooth" <?PHP if((isset($id_annonce)) && (preg_match("/Interface Bluetooth et téléphone mobile/", $annonces->equipement))) echo "checked";?>><label for="bluetooth">Interface Bluetooth et téléphone mobile</label><br/>
          <input id="toitPano" type="checkbox" value="Toit panoramique" name="toitPano" <?PHP if((isset($id_annonce)) && (preg_match("/Toit panoramique/", $annonces->equipement))) echo "checked";?>><label for="toitPano">Toit panoramique</label><br/>
          <input id="toitOuvrant" type="checkbox" value="Toit ouvrant" name="toitOuvrant" <?PHP if((isset($id_annonce)) && (preg_match("/Toit ouvrant/", $annonces->equipement))) echo "checked";?>><label for="toitOuvrant">Toit ouvrant</label>
        </div>
    </div></div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse4" class="black collapsed">
          <h4 class="panel-title">
        <i class="fa fa-hand-o-right" aria-hidden="true"></i> Équipements sport
              <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span>
      </h4></a>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <div class="panel-body form-group">
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="spoilerToit" type="checkbox" value="Spoiler de toit" name="spoilerToit" <?PHP if((isset($id_annonce)) && (preg_match("/Spoiler de toit/", $annonces->equipement))) echo "checked";?>><label for="spoilerToit">Spoiler de toit</label><br/>
          <input id="jupes" type="checkbox" value="Jupes latérales" name="jupes" <?PHP if((isset($id_annonce)) && (preg_match("/Jupes latérales/", $annonces->equipement))) echo "checked";?>><label for="jupes">Jupes latérales</label><br/>
          <input id="chassisSport" type="checkbox" value="Châssis sportif" name="chassisSport" <?PHP if((isset($id_annonce)) && (preg_match("/Châssis sportif/", $annonces->equipement))) echo "checked";?>><label for="chassisSport">Châssis sportif</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="siegeSport" type="checkbox" value="Sièges sport" name="siegeSport" <?PHP if((isset($id_annonce)) && (preg_match("/Sièges sport/", $annonces->equipement))) echo "checked";?>><label for="siegeSport">Sièges sport</label><br/>
          <input id="retroChrome" type="checkbox" value="Rétrovieurs extérieurs chromés" name="retroChrome" <?PHP if((isset($id_annonce)) && (preg_match("/Rétrovieurs extérieurs chromés/", $annonces->equipement))) echo "checked";?>><label for="retroChrome">Rétrovieurs extérieurs chromés</label><br/>
          <input id="barreToitAlu" type="checkbox" value="Barrres de toit en aluminium" name="barreToitAlu" <?PHP if((isset($id_annonce)) && (preg_match("/Barrres de toit en aluminium/", $annonces->equipement))) echo "checked";?>><label for="barreToitAlu">Barrres de toit en aluminium</label>
        </div>
    </div></div>
    </div>
  </div>
    <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse5" class="black collapsed">
          <h4 class="panel-title">
        <i class="fa fa-hand-o-right" aria-hidden="true"></i> Autres
              <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span>
      </h4></a>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <div class="panel-body form-group">
    <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="roueSecours" type="checkbox" value="Roue de secours" name="roueSecours" <?PHP if((isset($id_annonce)) && (preg_match("/Roue de secours/", $annonces->equipement))) echo "checked";?>><label for="roueSecours">Roue de secours</label><br/>
          <input id="x2PneusRoues" type="checkbox" value="2 jeux de pneus/roues" name="x2PneusRoues" <?PHP if((isset($id_annonce)) && (preg_match("/2 jeux de pneus\/roues/", $annonces->equipement))) echo "checked";?>><label for="x2PneusRoues">2 jeux de pneus/roues</label><br/>
          <input id="antiCrevaison" type="checkbox" value="Kit anti-crevaison" name="antiCrevaison" <?PHP if((isset($id_annonce)) && (preg_match("/Kit anti-crevaison/", $annonces->equipement))) echo "checked";?>><label for="antiCrevaison">Kit anti-crevaison</label><br/>
          <input id="hardTop" type="checkbox" value="Hard top" name="hardTop" <?PHP if((isset($id_annonce)) && (preg_match("/Hard top/", $annonces->equipement))) echo "checked";?>><label for="hardTop">Hard top</label>
        </div>
    </div>
        <div class="col-sm-5">
      <div class="checkbox checkbox-primary">
          <input id="tapisCaoutchouc" type="checkbox" value="Tapis en caoutchouc avant et arrière" name="tapisCaoutchouc" <?PHP if((isset($id_annonce)) && (preg_match("/Tapis en caoutchouc avant et arrière/", $annonces->equipement))) echo "checked";?>><label for="tapisCaoutchouc">Tapis en caoutchouc avant et arrière</label><br/>
          <input id="tapisRevetus" type="checkbox" value="Tapis revêtus avant et arrière" name="tapisRevetus" <?PHP if((isset($id_annonce)) && (preg_match("/Tapis revêtus avant et arrière/", $annonces->equipement))) echo "checked";?>><label for="tapisRevetus">Tapis revêtus avant et arrière</label><br/>
          <input id="crochetAttelage" type="checkbox" value="Crochet d'attelage" name="crochetAttelage" <?PHP if((isset($id_annonce)) && (preg_match("/Crochet d\'attelage/", $annonces->equipement))) echo "checked";?>><label for="crochetAttelage">Crochet d'attelage</label><br/>
          <input id="grille" type="checkbox" value="Grille de séparation" name="grille" <?PHP if((isset($id_annonce)) && (preg_match("/Grille de séparation/", $annonces->equipement))) echo "checked";?>><label for="grille">Grille de séparation</label>
        </div>
    </div></div>
    </div>
  </div>
    <div class="panel panel-default">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse6" class="black collapsed">
          <h4 class="panel-title">
        <i class="fa fa-hand-o-right" aria-hidden="true"></i> Informations complémentaires
          <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span>
          </h4></a>
    </div>
    <div id="collapse6" class="panel-collapse collapse">
      <div class="panel-body form-group">
    <div class="col-sm-2">
      <div class="checkbox checkbox-primary">
          <input id="handicape" type="checkbox" value="Pour handicapé" name="handicape" <?PHP if((isset($id_annonce)) && (preg_match("/Pour handicapé/", $annonces->equipement))) echo "checked";?>><label for="handicape">Pour handicapé</label>
        </div>
    </div>
    <div class="col-sm-2">
      <div class="checkbox checkbox-primary">
          <input id="tuning" type="checkbox" value="Tuning" name="tuning" <?PHP if((isset($id_annonce)) && (preg_match("/Tuning/", $annonces->equipement))) echo "checked";?>><label for="tuning">Tuning</label>
        </div>
    </div>
    <div class="col-sm-2">
      <div class="checkbox checkbox-primary">
          <input id="voitureCourse" type="checkbox" value="Voiture de course" name="voitureCourse" <?PHP if((isset($id_annonce)) && (preg_match("/Voiture de course/", $annonces->equipement))) echo "checked";?>><label for="voitureCourse">Voiture de course</label>
        </div>
    </div>
        </div>
    </div>
  </div>
</div>
<!-- END ACCORDEON EQUIPEMENTS -->

        <div class="clear"></div>

        <hr class="blue">
        <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Décrivez votre véhicule :</h4>
        <hr class="clear blue">
        <div class="form-group">
      <label class="control-label col-sm-4" for="desc1">Description rapide : <br />
          <small>(max. 100 caractères)</small></label>
    <div class="col-sm-7">
        <textarea class="form-control" rows="2" id="desc1" name="desc1" maxlength="100" style="display:block;"><?PHP if(isset($id_annonce)) echo $annonces->desc1;?></textarea>
    </div></div>
        <div class="clear"></div>

        <div class="form-group">
      <label class="control-label col-sm-4" for="desc2">Description détaillée : <br />
          <small>(max. 4000 caractères)</small></label>
    <div class="col-sm-7">
        <textarea class="form-control" id="desc2" name="desc2" maxlength="4000" rows="6"><?PHP if(isset($id_annonce)) echo $annonces->desc2;?></textarea>
    </div></div>
        <div class="clear"></div>

        <hr class="blue">
        <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> État du véhicule et garantie :</h4>
        <hr class="clear blue">
        <div class="form-group">
      <label class="col-sm-4 control-label" for="etatGarantie">État & garantie :</label>
    <div class="col-sm-3">
      <div class="checkbox checkbox-primary">
          <input id="nonFumeur" type="checkbox" value="Véhicule non-fumeur" name="nonFumeur" <?PHP if((isset($id_annonce)) && (preg_match("/Véhicule non-fumeur/", $annonces->equipement))) echo "checked";?>><label for="nonFumeur" class="notReqBlack">Véhicule non-fumeur</label><br/>
        </div>
    </div>
	
	<!-- 
	
	 -->
	

    <div class="col-sm-3">
         <div class="radio radio-primary">
          <input id="nonAccidente" type="radio" name="accident" value="0" <?PHP if((isset($id_annonce)) && ($annonces->accident == 0)) echo "checked";?> required>
          <label class="control-label" for="nonAccidente">Véhicule non accidenté</label> <span class="requis">*</span><br/>
          <?php /*<input id="accidente" type="radio" name="accident" value="1" <?PHP if((isset($id_annonce)) && ($annonces->accident == 1)) echo "checked";?>
          ><label class="control-label" for="accidente" >Véhicule accidenté</label> <span class="requis">*</span>*/ ?>
        </div>
    </div>
    </div>
    <div class="clear"></div>
	
	
	
	
	<?php if($client->type == CLIENT_TYPE_CLIENT){ ?>
	
	<div class="form-group">
		<label class="col-sm-4 control-label" for="demarre">&nbsp;</label>
		<div class="col-sm-4"><p>Le moteur de votre véhicule démarre-t-il ?</p></div>
		<div class="col-sm-4">
		<div class="radio radio-primary">
		<input id="demarreOui" type="radio" name="demarre" required value="1"<?php if((isset($id_annonce)) && ($annonces->demarre == 1)) echo " checked";?>><label class="control-label" for="demarreOui">Oui</label> <span class="requis">*</span><br/>
		<input id="demarreNon" type="radio" name="demarre" required value="0"<?php if((isset($id_annonce)) && ($annonces->demarre == 0)) echo " checked";?>><label class="control-label" for="demarreNon">Non</label> <span class="requis">*</span>
		</div>
		</div>
	</div>
	<div class="clear"></div>
	
	<div class="form-group">
		<label class="col-sm-4 control-label" for="deplace">&nbsp;</label>
		<div class="col-sm-4"><p>Votre véhicule est-il en état de se déplacer ?</p></div>
		<div class="col-sm-4">
		<div class="radio radio-primary">
		<input id="deplaceOui" type="radio" name="deplace" required value="1"<?php if((isset($id_annonce)) && ($annonces->deplace == 1)) echo " checked";?>><label class="control-label" for="deplaceOui">Oui</label> <span class="requis">*</span><br/>
		<input id="deplaceNon" type="radio" name="deplace" required value="0"<?php if((isset($id_annonce)) && ($annonces->deplace == 0)) echo " checked";?>><label class="control-label" for="deplaceNon">Non</label> <span class="requis">*</span>
		</div>
		</div>
	</div>
	<div class="clear"></div>
	
	<?php } ?>



  <div class="form-group">
   	 <label class="control-label col-sm-4" for="typeGarantie">Type de garantie :</label>
      <div class="col-sm-7">
      <select class="form-control" id="typeGarantie" name="typeGarantie">
				<option value="Sans garantie" selected>Sans garantie</option>
				<option value="À la livraison"<?PHP if((isset($id_annonce)) && ($annonces->typeGarantie == "À la livraison")) echo " selected";?>>À la livraison</option>
        <option value="À la première mise en circulation"<?PHP if((isset($id_annonce)) && ($annonces->typeGarantie == "À la première mise en circulation")) echo " selected";?>>À la première mise en circulation</option>
  		</select>
    	</div>
      <script type="text/javascript">
		
			function updateDureeGarantie(val)
			{
				if(val === "Sans garantie") {
                    $(".dureeGarantie").css("display", "none");  // hide
                }
                else {
                    $(".dureeGarantie").css("display", "block");  // show
                }
			}
			
			jQuery(document).ready(function () {
				var val = $('#typeGarantie').val();
				updateDureeGarantie(val);
			});

            $(function () {
              $("#typeGarantie").change(function() {
				var val = $(this).val();
                updateDureeGarantie(val);
              });
            });
        </script>

    </div>





        
	<div class="clear"></div>

  <div class="form-group dureeGarantie" style="display:none;">
        <label class="control-label col-sm-4" for="dureeGarantie">Durée de la garantie : <span class="requis">*</span></label>
            <div class="col-sm-7">
      <select class="form-control" id="dureeGarantie" name="dureeGarantie" required>
          <option value="" selected>Choisir une durée</option>
		  
			<?php
			$values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 24, 36, 48, 60, 72];
			foreach($values as $value){
				?>
				<option value="<?php echo $value;?>"<?PHP if((isset($id_annonce)) && ($annonces->dureeGarantie == $value)) echo " selected";?>><?php echo Display::formatMonth($value);?></option>
				<?php
			}
			?>
    	</select>
    </div>
        <div class="clear"></div>
    </div>


			
		<?php if($client->type == CLIENT_TYPE_CLIENT){ ?>
			
		<div class="form-group">
				<label class="col-sm-4 control-label" for="plaques">Plaques d'immatriculation :</label>
				<div class="col-sm-4">
					<p>Votre véhicule possède-t-il des plaques d'immatriculation pour pouvoir circuler sur la voie publique ?</p>
					<small>Cochez NON si votre véhicule possède des plaques de garage.</small>
				</div>
				
				
				<div class="col-sm-4">
				<div class="radio radio-primary">
				<input id="plaquesOui" type="radio" name="plaques" required value="1"<?PHP if((isset($id_annonce)) && ($annonces->plaques == 1)) echo " checked";?>><label class="control-label" for="plaquesOui">Oui</label> <span class="requis">*</span><br/>
				<input id="plaquesNon" type="radio" name="plaques" required value="0"<?PHP if((isset($id_annonce)) && ($annonces->plaques == 0)) echo " checked";?>><label class="control-label" for="plaquesNon">Non</label> <span class="requis">*</span>
				</div>
				</div>
			</div>
      <div class="clear"></div>
			   
			<?php } ?>
			  
		   
			 
      <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button" >Sauvegarder & continuer</button>
      <div class="clear"></div>
        </div>
      </div>
      <!-- FIN ÉTAPE 1 : DONNÉES & DESCRIPTION -->

     <!-- ÉTAPE 2 : INSÉRER DES PHOTOS -->
    <div class="setup-content" id="step-2" data-page='publier'>
        <div class="col-md-12">
          <hr class="blue">
            <div class="col-sm-4"><h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Insérer les photos de votre voiture :</h4></div>
            <div class="col-sm-8"><i class="fa fa-exclamation blue" aria-hidden="true"></i> <span class="anoter">Selon notre charte qualité, veuillez insérer au moins 1 photo de votre véhicule</span></div>
            <hr class="clear blue">

            <div class="form-group">
      <label class="control-label col-sm-4" for="photos">Télécharger vos photos :</label>
    <div class="col-sm-7">
      <ul>
          <li>Prenez 2 min. pour laver votre véhicule.</li>
          <li>Prenez les photos en plein jour ou dans un endroit éclairé.</li>
          <li>Prenez au minimum 4 photos extérieures et 2 photos de l’intérieur.</li>
        </ul>
    </div>
      <div class="clear"></div>
<script type="text/javascript">
$(function () {
	
	nbUpload = 0;
	
    // A chaque sélection de fichier
    $('#publier').find('input[name="image1"]').on('change', function (e) {
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#image_preview1');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
        }
        else {
            $image_preview = $('#image_preview1');
            $image_preview.find('img').attr('src', 'images/telecharger-les-photos.png');
        }
    });

    // A chaque sélection de fichier
    $('#publier').find('input[name="image2"]').on('change', function (e) {
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#image_preview2');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
        }
        else {
            $image_preview = $('#image_preview2');
            $image_preview.find('img').attr('src', 'images/telecharger-les-photos.png');
        }
    });


    // A chaque sélection de fichier
    $('#publier').find('input[name="image3"]').on('change', function (e) {
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#image_preview3');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
        }
        else {
            $image_preview = $('#image_preview3');
            $image_preview.find('img').attr('src', 'images/telecharger-les-photos.png');
        }
    });

    // A chaque sélection de fichier
    $('#publier').find('input[name="image4"]').on('change', function (e) {
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#image_preview4');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
        }
        else {
            $image_preview = $('#image_preview4');
            $image_preview.find('img').attr('src', 'images/telecharger-les-photos.png');
        }
    });

    // A chaque sélection de fichier
    $('#publier').find('input[name="image5"]').on('change', function (e) {
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#image_preview5');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
        }
        else {
            $image_preview = $('#image_preview5');
            $image_preview.find('img').attr('src', 'images/telecharger-les-photos.png');
        }
    });

    // A chaque sélection de fichier
    $('#publier').find('input[name="image6"]').on('change', function (e) {
        var files = $(this)[0].files;

        if (files.length > 0) {
            // On part du principe qu'il n'y qu'un seul fichier
            // étant donné que l'on a pas renseigné l'attribut "multiple"
            var file = files[0],
                $image_preview = $('#image_preview6');

            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
        }
        else {
            $image_preview = $('#image_preview6');
            $image_preview.find('img').attr('src', 'images/telecharger-les-photos.png');
        }
    });
});
</script>
    <div class="col-sm-3 col-sm-offset-1 form-group" id="module-upload-1">
		<div id="image_preview1">
			<div class="thumbnail">
				<img src="<?PHP if((isset($id_annonce)) && ($annonces->image1 != "")) echo "http://www.autospot.ch/img/$annonces->image1"; else echo "images/telecharger-les-photos.png";?>" class="img-responsive">
			</div>
		</div>
		<input id="input-file1" onchange='onChangeInput(0);' type="file" name="image1" accept="image/*">
		<a id="link-delete1" class='delete-file' onclick="deleteUpload(0);" style="visibility:<?PHP if((isset($id_annonce)) && ($annonces->image1 != "")) echo "visible"; else echo "hidden";?>;">Supprimer</a>
		
	</div>
	
    <div class="col-sm-3 col-sm-offset-1" id="module-upload-2">
		<div id="image_preview2">
			<div class="thumbnail">
				<img src="<?PHP if((isset($id_annonce)) && ($annonces->image2 != "")) echo "http://www.autospot.ch/img/$annonces->image2"; else echo "images/telecharger-les-photos.png";?>" class="img-responsive">
			</div>
		</div>
    	<input id="input-file2" onchange='onChangeInput(1);' type="file" name="image2" accept="image/*">
		<a id="link-delete2" class='delete-file' onclick="deleteUpload(1);" style="visibility:<?PHP if((isset($id_annonce)) && ($annonces->image2 != "")) echo "visible"; else echo "hidden";?>;">Supprimer</a>
	</div>
	
	<div class="col-sm-3 col-sm-offset-1" id="module-upload-3">
		<div id="image_preview3">
			<div class="thumbnail">
				<img src="<?PHP if((isset($id_annonce)) && ($annonces->image3 != "")) echo "http://www.autospot.ch/img/$annonces->image3"; else echo "images/telecharger-les-photos.png";?>" class="img-responsive">
			</div>
		</div>
   		<input id="input-file3" onchange='onChangeInput(2);' type="file" name="image3" accept="image/*">
		<a id="link-delete3" class='delete-file' onclick="deleteUpload(2);" style="visibility:<?PHP if((isset($id_annonce)) && ($annonces->image3 != "")) echo "visible"; else echo "hidden";?>;">Supprimer</a>
	</div>
	
    <div class="clear"></div>
	
	<div class="col-sm-3 col-sm-offset-1" id="module-upload-4">
		<div id="image_preview4">
			<div class="thumbnail">
				<img src="<?PHP if((isset($id_annonce)) && ($annonces->image4 != "")) echo "http://www.autospot.ch/img/$annonces->image4"; else echo "images/telecharger-les-photos.png";?>" class="img-responsive">
				<div class="caption"></div>
			</div>
		</div>
   		<input id="input-file4" onchange='onChangeInput(3);' type="file" name="image4" accept="image/*">
		<a id="link-delete4" class='delete-file' onclick="deleteUpload(3);" style="visibility:<?PHP if((isset($id_annonce)) && ($annonces->image4 != "")) echo "visible"; else echo "hidden";?>;">Supprimer</a>
	</div>
	
    <div class="col-sm-3 col-sm-offset-1" id="module-upload-5">
		<div id="image_preview5">
			<div class="thumbnail">
				<img src="<?PHP if((isset($id_annonce)) && ($annonces->image5 != "")) echo "http://www.autospot.ch/img/$annonces->image5"; else echo "images/telecharger-les-photos.png";?>" class="img-responsive">
				<div class="caption"></div>
			</div>
		</div>
    	<input id="input-file5" onchange='onChangeInput(4);' type="file" name="image5" accept="image/*">
		<a id="link-delete5" class='delete-file' onclick="deleteUpload(4);" style="visibility:<?PHP if((isset($id_annonce)) && ($annonces->image5 != "")) echo "visible"; else echo "hidden";?>;">Supprimer</a>
	</div>
	
    <div class="col-sm-3 col-sm-offset-1" id="module-upload-6">
		<div id="image_preview6">
			<div class="thumbnail">
				<img src="<?PHP if((isset($id_annonce)) && ($annonces->image6 != "")) echo "http://www.autospot.ch/img/$annonces->image6"; else echo "images/telecharger-les-photos.png";?>" class="img-responsive">
				<div class="caption"></div>
			</div>
		</div>
   	 	<input id="input-file6" onchange='onChangeInput(5);' type="file" name="image6" accept="image/*">
		<a id="link-delete6" class='delete-file' onclick="deleteUpload(5);" style="visibility:<?PHP if((isset($id_annonce)) && ($annonces->image6 != "")) echo "visible"; else echo "hidden";?>;">Supprimer</a>
	</div>
	
		   
</div>
            <div class="clear"></div>
            
            <div class="row">
                <div class="col-xs-6">
                    <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Sauvegarder <span class="hidden-xs">& continuer</span></button>
                </div>
            </div>
            
            <div class="clear"></div>
        </div>
    </div>
     <!-- FIN ÉTAPE 2 : INSÉRER DES PHOTOS -->

     <!-- ÉTAPE 3 : SECURSPOT -->
    <!--<div class="setup-content" id="step-3">-->
    <div class="setup-content">
        <div class="col-md-12">
          <hr class="blue">
            <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> SecurSpot - Afficher l'historique de votre véhicule d'occasion :</h4><span ><img src="images/picto_securspot.png" class="pull-right" style="margin-top:-10px;" alt="securspot"/></span>
            <hr class="clear blue">
        <h4 style="color:#009ada;">Service SecurSpot</h4>
        <div class="col-sm-8">
            <p>
            Permettez aux acheteurs potentiels de connaître à l'avance l'historique du véhicule que vous vendez.
            </p>
            <p>
            Totalement gratuit, le rapport SecureSpot vous permet, si vous possédez le carnet d'entretien, de noter tous les services qui ont été effectués sur le véhicule.
            </p>
            <p>
            Si vous possédez également les factures liées aux différents services effectués, vous pourrez noter le/les pièces qui ont été changées ainsi que le/les changements de détenteur.
            </p>
            <p>
            L'achat d'un véhicule d'occasion présente toujours des risques pour les acheteurs potentiels.<br/>
            Profitez gratuitement de se service et le label SecureSpot sera visible sur votre annonce.</p>
                            </div>
            <div class="col-sm-4" style="text-align:center;padding-top:5%;"><a href="securspot.php" target="_blank"><b>En savoir +</b></a></div>
            <div class="clear"></div>
            <button type="button" class="btn btn-autospot" data-toggle="collapse" data-target="#rapportSecurSpot">Je rédige mon rapport SecurSpot</button>
            <div class="clear"></div>

            <!-- rapport securspot -->

            <div id="rapportSecurSpot" class="collapse col-sm-12" style="<?PHP if(isset($securspot->id)) echo "display:block;";?>>

            <!-- rapport 1 -->
            <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Veuillez remplir votre rapport SecurSpot :</h4>
            <div class="form-group">

            <label class="control-label col-sm-1 small" for="circulation1Mois">1ère mise en circulation</label>
            <div class="col-sm-1">
            <select class="form-control" id="circulation1Mois" name="circulation1Mois">
  				<option value="" selected>mois</option>
                <option value="01"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "01")) echo " selected";?>>Janvier</option>
                <option value="02"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "02")) echo " selected";?>>Février</option>
                <option value="03"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "03")) echo " selected";?>>Mars</option>
                <option value="04"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "04")) echo " selected";?>>Avril</option>
                <option value="05"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "05")) echo " selected";?>>Mai</option>
                <option value="06"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "06")) echo " selected";?>>Juin</option>
                <option value="07"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "07")) echo " selected";?>>Juillet</option>
                <option value="08"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "08")) echo " selected";?>>Août</option>
                <option value="09"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "09")) echo " selected";?>>Septembre</option>
                <option value="10"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "10")) echo " selected";?>>Octobre</option>
                <option value="11"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "11")) echo " selected";?>>Novembre</option>
                <option value="12"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Mois == "12")) echo " selected";?>>Décembre</option>
            </select>
                </div>
            <div class="col-sm-1">
            <select class="form-control" id="circulation1Annee" name="circulation1Annee">
  		        <option value="" selected>année</option>
                <option value="2016"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2016)) echo " selected";?>>2016</option>
                <option value="2015"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2015)) echo " selected";?>>2015</option>
                <option value="2014"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2014)) echo " selected";?>>2014</option>
                <option value="2013"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2013)) echo " selected";?>>2013</option>
                <option value="2012"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2012)) echo " selected";?>>2012</option>
                <option value="2011"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2011)) echo " selected";?>>2011</option>
                <option value="2010"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2010)) echo " selected";?>>2010</option>
                <option value="2009"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2009)) echo " selected";?>>2009</option>
                <option value="2008"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2008)) echo " selected";?>>2008</option>
                <option value="2007"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2007)) echo " selected";?>>2007</option>
                <option value="2006"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2006)) echo " selected";?>>2006</option>
                <option value="2005"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2005)) echo " selected";?>>2005</option>
                <option value="2004"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2004)) echo " selected";?>>2004</option>
                <option value="2003"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2003)) echo " selected";?>>2003</option>
                <option value="2002"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2002)) echo " selected";?>>2002</option>
                <option value="2001"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2001)) echo " selected";?>>2001</option>
                <option value="2000"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 2000)) echo " selected";?>>2000</option>
                <option value="1999"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1999)) echo " selected";?>>1999</option>
                <option value="1998"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1998)) echo " selected";?>>1998</option>
                <option value="1997"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1997)) echo " selected";?>>1997</option>
                <option value="1996"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1996)) echo " selected";?>>1996</option>
                <option value="1995"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1995)) echo " selected";?>>1995</option>
                <option value="1994"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1994)) echo " selected";?>>1994</option>
                <option value="1993"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1993)) echo " selected";?>>1993</option>
                <option value="1992"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1992)) echo " selected";?>>1992</option>
                <option value="1991"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1991)) echo " selected";?>>1991</option>
                <option value="1990"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1990)) echo " selected";?>>1990</option>
                <option value="1989"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1989)) echo " selected";?>>1989</option>
                <option value="1988"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1988)) echo " selected";?>>1988</option>
                <option value="1987"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1987)) echo " selected";?>>1987</option>
                <option value="1986"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1986)) echo " selected";?>>1986</option>
                <option value="1985"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1985)) echo " selected";?>>1985</option>
                <option value="1984"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1984)) echo " selected";?>>1984</option>
                <option value="1983"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1983)) echo " selected";?>>1983</option>
                <option value="1982"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1982)) echo " selected";?>>1982</option>
                <option value="1981"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1981)) echo " selected";?>>1981</option>
                <option value="1980"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1980)) echo " selected";?>>1980</option>
                <option value="1975"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1975)) echo " selected";?>>1975</option>
                <option value="1970"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1970)) echo " selected";?>>1970</option>
                <option value="1960"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1960)) echo " selected";?>>1960</option>
                <option value="1950"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1950)) echo " selected";?>>1950</option>
                <option value="1940"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1940)) echo " selected";?>>1940</option>
                <option value="1930"<?PHP if((isset($securspot->id)) && ($securspot->circulation1Annee == 1930)) echo " selected";?>>1930</option>
            </select>
            </div>
            <label class="control-label col-sm-2 col-sm-offset-1 small" for="dateAchat1">À quelle date avez-vous acheté ce véhicule</label>
            <div class="col-sm-1">
            <select class="form-control" id="dateAchatMois1" name="dateAchatMois1">
  				<option value="" selected>mois</option>
                <option value="01"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "01")) echo " selected";?>>Janvier</option>
                <option value="02"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "02")) echo " selected";?>>Février</option>
                <option value="03"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "03")) echo " selected";?>>Mars</option>
                <option value="04"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "04")) echo " selected";?>>Avril</option>
                <option value="05"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "05")) echo " selected";?>>Mai</option>
                <option value="06"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "06")) echo " selected";?>>Juin</option>
                <option value="07"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "07")) echo " selected";?>>Juillet</option>
                <option value="08"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "08")) echo " selected";?>>Août</option>
                <option value="09"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "09")) echo " selected";?>>Septembre</option>
                <option value="10"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "10")) echo " selected";?>>Octobre</option>
                <option value="11"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "11")) echo " selected";?>>Novembre</option>
                <option value="12"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatMois1 == "12")) echo " selected";?>>Décembre</option>
            </select>
                </div>
            <div class="col-sm-1">
            <select class="form-control" id="dateAchatAnnee1" name="dateAchatAnnee1">
  		        <option value="" selected>année</option>
                <option value="2016"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2016)) echo " selected";?>>2016</option>
                <option value="2015"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2015)) echo " selected";?>>2015</option>
                <option value="2014"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2014)) echo " selected";?>>2014</option>
                <option value="2013"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2013)) echo " selected";?>>2013</option>
                <option value="2012"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2012)) echo " selected";?>>2012</option>
                <option value="2011"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2011)) echo " selected";?>>2011</option>
                <option value="2010"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2010)) echo " selected";?>>2010</option>
                <option value="2009"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2009)) echo " selected";?>>2009</option>
                <option value="2008"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2008)) echo " selected";?>>2008</option>
                <option value="2007"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2007)) echo " selected";?>>2007</option>
                <option value="2006"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2006)) echo " selected";?>>2006</option>
                <option value="2005"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2005)) echo " selected";?>>2005</option>
                <option value="2004"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2004)) echo " selected";?>>2004</option>
                <option value="2003"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2003)) echo " selected";?>>2003</option>
                <option value="2002"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2002)) echo " selected";?>>2002</option>
                <option value="2001"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2001)) echo " selected";?>>2001</option>
                <option value="2000"<?PHP if((isset($securspot->id)) && ($securspot->dateAchatAnnee1 == 2000)) echo " selected";?>>2000</option>
            </select>
            </div>
            <label class="control-label col-sm-2 col-sm-offset-1 small" for="pourcentageVille">À quel pourcentage utilisez-vous votre véhicule en ville?</label>
            <div class="col-sm-1">
            <select class="form-control" id="pourcentageVille" name="pourcentageVille">
  				<option value="" selected></option>
                <option value="10"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 10)) echo " selected";?>>10</option>
                <option value="20"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 20)) echo " selected";?>>20</option>
                <option value="30"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 30)) echo " selected";?>>30</option>
                <option value="40"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 40)) echo " selected";?>>40</option>
                <option value="50"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 50)) echo " selected";?>>50</option>
                <option value="60"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 60)) echo " selected";?>>60</option>
                <option value="70"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 70)) echo " selected";?>>70</option>
                <option value="80"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 80)) echo " selected";?>>80</option>
                <option value="90"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 90)) echo " selected";?>>90</option>
                <option value="100"<?PHP if((isset($securspot->id)) && ($securspot->pourcentageVille == 100)) echo " selected";?>>100</option>
            </select>
                </div>

        </div>
                <div class="clear"></div>
            <div class="form-group">

            <label class="control-label col-sm-1 small" for="prenom1">Choisissez la 1ère lettre du prénom de l'acheteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom1" name="prenom1">
  				<option value="" selected>1ère l.</option>
                <option value="A"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "A")) echo " selected";?>>A</option>
                <option value="B"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "B")) echo " selected";?>>B</option>
                <option value="C"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "C")) echo " selected";?>>C</option>
                <option value="D"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "D")) echo " selected";?>>D</option>
                <option value="E"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "E")) echo " selected";?>>E</option>
                <option value="F"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "F")) echo " selected";?>>F</option>
                <option value="G"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "G")) echo " selected";?>>G</option>
                <option value="H"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "H")) echo " selected";?>>H</option>
                <option value="I"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "I")) echo " selected";?>>I</option>
                <option value="J"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "J")) echo " selected";?>>J</option>
                <option value="K"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "K")) echo " selected";?>>K</option>
                <option value="L"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "L")) echo " selected";?>>L</option>
                <option value="M"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "M")) echo " selected";?>>M</option>
                <option value="N"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "N")) echo " selected";?>>N</option>
                <option value="O"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "O")) echo " selected";?>>O</option>
                <option value="P"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "P")) echo " selected";?>>P</option>
                <option value="Q"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "Q")) echo " selected";?>>Q</option>
                <option value="R"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "R")) echo " selected";?>>R</option>
                <option value="S"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "S")) echo " selected";?>>S</option>
                <option value="T"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "T")) echo " selected";?>>T</option>
                <option value="U"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "U")) echo " selected";?>>U</option>
                <option value="V"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "V")) echo " selected";?>>V</option>
                <option value="W"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "W")) echo " selected";?>>W</option>
                <option value="X"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "X")) echo " selected";?>>X</option>
                <option value="Y"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "Y")) echo " selected";?>>Y</option>
                <option value="Z"<?PHP if((isset($securspot->id)) && ($securspot->prenom1 == "Z")) echo " selected";?>>Z</option>
            </select><br/>
            <input class="form-control" id="cp1" name="cp1" type="number" min="1000" max="9999" maxlength="4" value="<?PHP if(isset($securspot->id)) echo $securspot->cp1;?>">
            </div>

            <label class="control-label col-sm-1 small" for="rapportSuisse">Ce véhicule est-il d'origine Suisse ?</label>
            <div class="col-sm-2 radio radio-primary">
            <input id="rapportSuisseOui" type="radio" name="rapportSuisse" value="1"<?PHP if((isset($securspot->id)) && ($securspot->rapportSuisse == 1)) echo " checked";?>><label for="rapportSuisseOui">Oui</label>
            <br/>
            <input id="rapportSuisseNon" type="radio" name="rapportSuisse" value="0"<?PHP if((isset($securspot->id)) && ($securspot->rapportSuisse == 0)) echo " checked";?>><label for="rapportSuisseNon">Non</label>
            </div>
            <label class="control-label col-sm-1 small" for="garantConstructeur1">Quelle est la durée de garantie constructeur ?</label>
            <div class="col-sm-2">
            <select class="form-control" id="garantConstructeur1" name="garantConstructeur1">
  		        <option value="" selected>année</option>
                <option value="1"<?PHP if((isset($securspot->id)) && ($securspot->garantConstructeur1 == 1)) echo " selected";?>>1</option>
                <option value="2"<?PHP if((isset($securspot->id)) && ($securspot->garantConstructeur1 == 2)) echo " selected";?>>2</option>
                <option value="3"<?PHP if((isset($securspot->id)) && ($securspot->garantConstructeur1 == 3)) echo " selected";?>>3</option>
                <option value="4"<?PHP if((isset($securspot->id)) && ($securspot->garantConstructeur1 == 4)) echo " selected";?>>4</option>
                <option value="5"<?PHP if((isset($securspot->id)) && ($securspot->garantConstructeur1 == 5)) echo " selected";?>>5</option>
                <option value="6"<?PHP if((isset($securspot->id)) && ($securspot->garantConstructeur1 == 6)) echo " selected";?>>6</option>
                <option value="7"<?PHP if((isset($securspot->id)) && ($securspot->garantConstructeur1 == 7)) echo " selected";?>>7</option>
            </select>

            </div>

            <label class="control-label col-sm-1 small" for="etablissementAchat">Insérez le nom de l'établissement où ce véhicule a été acheté</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissementAchat" name="etablissementAchat" value="<?PHP if(isset($securspot->id)) echo $securspot->etablissementAchat;?>">
            </div>

        </div>


            <div class="clear"></div>

            <h4>En suivant votre carnet d'entretien ET pour chacune de vos factures, notez chaque relevé kilométrique</h4>
            <div class="clear"></div>
            <div class="col-sm-9">
                <p class="blue">
<script>
function toggleTranche() {
    if(document.getElementById("tranche").value == '15') {
        document.getElementById("tranche").value = '20';
        document.getElementById("service20Mil").getElementsByClassName("rapportKm")[0].innerHTML = '0 - 20\'000 km';
        document.getElementById("service40Mil").getElementsByClassName("rapportKm")[0].innerHTML = '20\'000 - 40\'000 km';
        document.getElementById("service60Mil").getElementsByClassName("rapportKm")[0].innerHTML = '40\'000 - 60\'000 km';
    }
    else if(document.getElementById("tranche").value == '20') {
        document.getElementById("tranche").value = '30';
        document.getElementById("service20Mil").getElementsByClassName("rapportKm")[0].innerHTML = '0 - 30\'000 km';
        document.getElementById("service40Mil").getElementsByClassName("rapportKm")[0].innerHTML = '30\'000 - 60\'000 km';
        document.getElementById("service60Mil").getElementsByClassName("rapportKm")[0].innerHTML = '60\'000 - 90\'000 km';
    }
    else if(document.getElementById("tranche").value == '30') {
        document.getElementById("tranche").value = '15';
        document.getElementById("service20Mil").getElementsByClassName("rapportKm")[0].innerHTML = '0 - 15\'000 km';
        document.getElementById("service40Mil").getElementsByClassName("rapportKm")[0].innerHTML = '15\'000 - 30\'000 km';
        document.getElementById("service60Mil").getElementsByClassName("rapportKm")[0].innerHTML = '30\'000 - 45\'000 km';
    }
}
</script>
                <input type="hidden" id="tranche" name="tranche" value="20">
                <button id="btnMil" type="button" class="btn btn-default quinze"  style="margin-right:20px;" onclick="toggleTranche()"><img src="images/switch_35x35.png"></button>
               <i class="fa fa-exclamation"></i> Pour que les intervalles de révision soient les mêmes que celles de votre carnet d'entretien, cliquez sur le bouton ci-contre pour le modifier.</p>
            </div>
            <div class="clear"></div>
            <!-- rapport 20Mil -->
            <div id="service20Mil" class="form-group">
            <div class="rapportKm">0 - 20'000 km</div>
            <label class="control-label col-sm-1 small" for="releve20MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve20MilNature" name="releve20MilNature">
                <option value="1ère révision constructeur"<?PHP if((isset($securspot->id)) && ($securspot_details->releve20MilNature == "1ère révision constructeur")) echo " selected";?>>1ère révision constructeur</option>
                <option value="Expertise"<?PHP if((isset($securspot->id)) && ($securspot_details->releve20MilNature == "Expertise")) echo " selected";?>>Expertise</option>
                <option value="Réparation carrosserie"<?PHP if((isset($securspot->id)) && ($securspot_details->releve20MilNature == "Réparation carrosserie")) echo " selected";?>>Réparation carrosserie</option>
                <option value="Réparation moteur"<?PHP if((isset($securspot->id)) && ($securspot_details->releve20MilNature == "Réparation moteur")) echo " selected";?>>Réparation moteur</option>
                <option value="Réparation vitrage"<?PHP if((isset($securspot->id)) && ($securspot_details->releve20MilNature == "Réparation vitrage")) echo " selected";?>>Réparation vitrage</option>
                <option value="Changement d'assurance"<?PHP if((isset($securspot->id)) && ($securspot_details->releve20MilNature == "Changement d'assurance")) echo " selected";?>>Changement d'assurance</option>
                <option value="Autre"<?PHP if((isset($securspot->id)) && ($securspot_details->releve20MilNature == "Autre")) echo " selected";?>>Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km20Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km20Mil" name="km20Mil" value="<?PHP if(isset($securspot->id)) echo $securspot_details->km20Mil;?>">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois20Mil" name="mois20Mil">
  				<option value="" selected>mois</option>
                <option value="01"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "01")) echo " selected";?>>Janvier</option>
                <option value="02"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "02")) echo " selected";?>>Février</option>
                <option value="03"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "03")) echo " selected";?>>Mars</option>
                <option value="04"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "04")) echo " selected";?>>Avril</option>
                <option value="05"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "05")) echo " selected";?>>Mai</option>
                <option value="06"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "06")) echo " selected";?>>Juin</option>
                <option value="07"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "07")) echo " selected";?>>Juillet</option>
                <option value="08"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "08")) echo " selected";?>>Août</option>
                <option value="09"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "09")) echo " selected";?>>Septembre</option>
                <option value="10"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "10")) echo " selected";?>>Octobre</option>
                <option value="11"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "11")) echo " selected";?>>Novembre</option>
                <option value="12"<?PHP if((isset($securspot->id)) && ($securspot_details->mois20Mil == "12")) echo " selected";?>>Décembre</option>
            </select><br />
            <select class="form-control" id="annee20Mil" name="annee20Mil">
  		        <option value="" selected>année</option>
                <option value="2016"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2016)) echo " selected";?>>2016</option>
                <option value="2015"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2015)) echo " selected";?>>2015</option>
                <option value="2014"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2014)) echo " selected";?>>2014</option>
                <option value="2013"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2013)) echo " selected";?>>2013</option>
                <option value="2012"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2012)) echo " selected";?>>2012</option>
                <option value="2011"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2011)) echo " selected";?>>2011</option>
                <option value="2010"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2010)) echo " selected";?>>2010</option>
                <option value="2009"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2009)) echo " selected";?>>2009</option>
                <option value="2008"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2008)) echo " selected";?>>2008</option>
                <option value="2007"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2007)) echo " selected";?>>2007</option>
                <option value="2006"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2006)) echo " selected";?>>2006</option>
                <option value="2005"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2005)) echo " selected";?>>2005</option>
                <option value="2004"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2004)) echo " selected";?>>2004</option>
                <option value="2003"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2003)) echo " selected";?>>2003</option>
                <option value="2002"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2002)) echo " selected";?>>2002</option>
                <option value="2001"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2001)) echo " selected";?>>2001</option>
                <option value="2000"<?PHP if((isset($securspot->id)) && ($securspot_details->annee20Mil == 2000)) echo " selected";?>>2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement20Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement20Mil" name="etablissement20Mil" value="<?PHP if(isset($securspot->id)) echo $securspot_details->etablissement20Mil;?>">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture20Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture20MilOui" type="radio" name="facture20Mil" value="1"<?PHP if((isset($securspot->id)) && ($securspot_details->facture20Mil == 1)) echo " checked";?>><label for="facture20MilOui">Oui</label>
            <br/>
            <input id="facture20MilNon" type="radio" name="facture20Mil" value="0"<?PHP if((isset($securspot->id)) && ($securspot_details->facture20Mil == 0)) echo " checked";?>><label for="facture20MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture20MilOui').change(function(){
                    if(this.checked)
                    $("#facture20Oui").css("display", "");  // show
                    else
                    $("#facture20Oui").css("display", "none");  // hide
                });
                $('#facture20MilNon').change(function(){
                    if(this.checked)
                    $("#facture20Oui").css("display", "none");
                    else
                    $("#facture20Oui").css("display", "");
                    });
                </script>

                <div id="facture20Oui" class="form-group" style="<?PHP if((isset($securspot->id)) && ($securspot_details->facture20Mil == 1)) echo "display:block;"; else echo "display:none;";?>">
                <label class="control-label col-sm-1 small" for="serviceEffectue20Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue20Mil" name="serviceEffectue20Mil" value="<?PHP if(isset($securspot->id)) echo $securspot_details->serviceEffectue20Mil;?>">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee20Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee20Mil" name="pieceChangee20Mil" value="<?PHP if(isset($securspot->id)) echo $securspot_details->pieceChangee20Mil;?>">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur20Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur20MilOui" type="radio" name="nomDetenteur20Mil" value="1"<?PHP if((isset($securspot->id)) && ($securspot_details->nomDetenteur20Mil == 1)) echo " checked";?>><label for="nomDetenteur20MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur20MilNon" type="radio" name="nomDetenteur20Mil" value="0"<?PHP if((isset($securspot->id)) && ($securspot_details->nomDetenteur20Mil == 0)) echo " checked";?>><label for="nomDetenteur20MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur20MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur20Oui").css("display", "");  // show
                    else
                    $("#detenteur20Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur20MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur20Oui").css("display", "none");
                    else
                    $("#detenteur20Oui").css("display", "");
                    });
                </script>
                <div id="detenteur20Oui" class="form-group" style="<?PHP if((isset($securspot->id)) && ($securspot_details->nomDetenteur20Mil == 1)) echo "display:block;"; else echo "display:none;";?>">
                <label class="control-label col-sm-1 small" for="prenom20Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom20Mil" name="prenom20Mil">
  				<option value="" selected>1ère l.</option>
                <option value="A"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "A")) echo " selected";?>>A</option>
                <option value="B"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "B")) echo " selected";?>>B</option>
                <option value="C"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "C")) echo " selected";?>>C</option>
                <option value="D"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "D")) echo " selected";?>>D</option>
                <option value="E"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "E")) echo " selected";?>>E</option>
                <option value="F"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "F")) echo " selected";?>>F</option>
                <option value="G"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "G")) echo " selected";?>>G</option>
                <option value="H"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "H")) echo " selected";?>>H</option>
                <option value="I"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "I")) echo " selected";?>>I</option>
                <option value="J"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "J")) echo " selected";?>>J</option>
                <option value="K"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "K")) echo " selected";?>>K</option>
                <option value="L"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "L")) echo " selected";?>>L</option>
                <option value="M"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "M")) echo " selected";?>>M</option>
                <option value="N"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "N")) echo " selected";?>>N</option>
                <option value="O"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "O")) echo " selected";?>>O</option>
                <option value="P"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "P")) echo " selected";?>>P</option>
                <option value="Q"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "Q")) echo " selected";?>>Q</option>
                <option value="R"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "R")) echo " selected";?>>R</option>
                <option value="S"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "S")) echo " selected";?>>S</option>
                <option value="T"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "T")) echo " selected";?>>T</option>
                <option value="U"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "U")) echo " selected";?>>U</option>
                <option value="V"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "V")) echo " selected";?>>V</option>
                <option value="W"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "W")) echo " selected";?>>W</option>
                <option value="X"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "X")) echo " selected";?>>X</option>
                <option value="Y"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "Y")) echo " selected";?>>Y</option>
                <option value="Z"<?PHP if((isset($securspot->id)) && ($securspot_details->prenom20Mil == "Z")) echo " selected";?>>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp20Mil" name="cp20Mil" type="number" min="1000" max="9999" maxlength="4" value="<?PHP if(isset($securspot->id)) echo $securspot->cp20Mil;?>">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com20Mil" name="com20Mil" placeholder="Inscrivez votre commentaire..."><?PHP if(isset($securspot->id)) echo $securspot->com20Mil;?></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service40Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 20Mil -->

            <!-- rapport 40Mil -->
            <div id="service40Mil" class="form-group collapse">
            <div class="rapportKm">0 - 40'000 km</div>
            <label class="control-label col-sm-1 small" for="releve40MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve40MilNature" name="releve40MilNature">
                <option value="1ère révision constructeur">2ème révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km40Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km40Mil" name="km40Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois40Mil" name="mois40Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee40Mil" name="annee40Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement40Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement40Mil" name="etablissement40Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture40Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture40MilOui" type="radio" name="facture40Mil" value="1"><label for="facture40MilOui">Oui</label>
            <br/>
            <input id="facture40MilNon" type="radio" name="facture40Mil" value="0"><label for="facture40MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture40MilOui').change(function(){
                    if(this.checked)
                    $("#facture40Oui").css("display", "");  // show
                    else
                    $("#facture40Oui").css("display", "none");  // hide
                });
                $('#facture40MilNon').change(function(){
                    if(this.checked)
                    $("#facture40Oui").css("display", "none");
                    else
                    $("#facture40Oui").css("display", "");
                    });
                </script>

                <div id="facture40Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue40Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue40Mil" name="serviceEffectue40Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee40Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee40Mil" name="pieceChangee40Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur40Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur40MilOui" type="radio" name="nomDetenteur40Mil" value="1"><label for="nomDetenteur40MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur40MilNon" type="radio" name="nomDetenteur40Mil" value="0"><label for="nomDetenteur40MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur40MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur40Oui").css("display", "");  // show
                    else
                    $("#detenteur40Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur40MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur40Oui").css("display", "none");
                    else
                    $("#detenteur40Oui").css("display", "");
                    });
                </script>
                <div id="detenteur40Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom40Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom40Mil" name="prenom40Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp40Mil" name="cp40Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com40Mil" name="com40Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service60Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 40Mil -->

            <!-- rapport 60Mil -->
            <div id="service60Mil" class="form-group collapse">
            <div class="rapportKm">0 - 60'000 km</div>
            <label class="control-label col-sm-1 small" for="releve60MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve60MilNature" name="releve60MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km60Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km60Mil" name="km60Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois60Mil" name="mois60Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee60Mil" name="annee60Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement60Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement60Mil" name="etablissement60Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture60Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture60MilOui" type="radio" name="facture60Mil" value="1"><label for="facture60MilOui">Oui</label>
            <br/>
            <input id="facture60MilNon" type="radio" name="facture60Mil" value="0"><label for="facture60MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture60MilOui').change(function(){
                    if(this.checked)
                    $("#facture60Oui").css("display", "");  // show
                    else
                    $("#facture60Oui").css("display", "none");  // hide
                });
                $('#facture60MilNon').change(function(){
                    if(this.checked)
                    $("#facture60Oui").css("display", "none");
                    else
                    $("#facture60Oui").css("display", "");
                    });
                </script>

                <div id="facture60Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue60Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue60Mil" name="serviceEffectue60Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee60Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee60Mil" name="pieceChangee60Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur60Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur60MilOui" type="radio" name="nomDetenteur60Mil" value="1"><label for="nomDetenteur60MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur60MilNon" type="radio" name="nomDetenteur60Mil" value="0"><label for="nomDetenteur60MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur60MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur60Oui").css("display", "");  // show
                    else
                    $("#detenteur60Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur60MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur60Oui").css("display", "none");
                    else
                    $("#detenteur60Oui").css("display", "");
                    });
                </script>
                <div id="detenteur60Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom60Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom60Mil" name="prenom60Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp60Mil" name="cp60Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com60Mil" name="com60Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service80Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 60Mil -->

            <!-- rapport 80Mil -->
            <div id="service80Mil" class="form-group collapse">
            <div class="rapportKm">0 - 80'000 km</div>
            <label class="control-label col-sm-1 small" for="releve80MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve80MilNature" name="releve80MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km80Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km80Mil" name="km80Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois80Mil" name="mois80Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee80Mil" name="annee80Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement80Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement80Mil" name="etablissement80Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture80Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture80MilOui" type="radio" name="facture80Mil" value="1"><label for="facture80MilOui">Oui</label>
            <br/>
            <input id="facture80MilNon" type="radio" name="facture80Mil" value="0"><label for="facture80MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture80MilOui').change(function(){
                    if(this.checked)
                    $("#facture80Oui").css("display", "");  // show
                    else
                    $("#facture80Oui").css("display", "none");  // hide
                });
                $('#facture80MilNon').change(function(){
                    if(this.checked)
                    $("#facture80Oui").css("display", "none");
                    else
                    $("#facture80Oui").css("display", "");
                    });
                </script>

                <div id="facture80Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue80Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue80Mil" name="serviceEffectue80Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee80Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee80Mil" name="pieceChangee80Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur80Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur80MilOui" type="radio" name="nomDetenteur80Mil" value="1"><label for="nomDetenteur80MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur80MilNon" type="radio" name="nomDetenteur80Mil" value="0"><label for="nomDetenteur80MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur80MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur80Oui").css("display", "");  // show
                    else
                    $("#detenteur80Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur80MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur80Oui").css("display", "none");
                    else
                    $("#detenteur80Oui").css("display", "");
                    });
                </script>
                <div id="detenteur80Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom80Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom80Mil" name="prenom80Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp80Mil" name="cp80Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com80Mil" name="com80Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service100Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 80Mil -->

            <!-- rapport 100Mil -->
            <div id="service100Mil" class="form-group collapse">
            <div class="rapportKm">0 - 100'000 km</div>
            <label class="control-label col-sm-1 small" for="releve100MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve100MilNature" name="releve100MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km100Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km100Mil" name="km100Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois100Mil" name="mois100Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee100Mil" name="annee100Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement100Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement100Mil" name="etablissement100Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture100Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture100MilOui" type="radio" name="facture100Mil" value="1"><label for="facture100MilOui">Oui</label>
            <br/>
            <input id="facture100MilNon" type="radio" name="facture100Mil" value="0"><label for="facture100MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture100MilOui').change(function(){
                    if(this.checked)
                    $("#facture100Oui").css("display", "");  // show
                    else
                    $("#facture100Oui").css("display", "none");  // hide
                });
                $('#facture100MilNon').change(function(){
                    if(this.checked)
                    $("#facture100Oui").css("display", "none");
                    else
                    $("#facture100Oui").css("display", "");
                    });
                </script>

                <div id="facture100Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue100Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue100Mil" name="serviceEffectue100Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee100Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee100Mil" name="pieceChangee100Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur100Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur100MilOui" type="radio" name="nomDetenteur100Mil" value="1"><label for="nomDetenteur100MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur100MilNon" type="radio" name="nomDetenteur100Mil" value="0"><label for="nomDetenteur100MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur100MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur100Oui").css("display", "");  // show
                    else
                    $("#detenteur100Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur100MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur100Oui").css("display", "none");
                    else
                    $("#detenteur100Oui").css("display", "");
                    });
                </script>
                <div id="detenteur100Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom100Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom100Mil" name="prenom100Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp100Mil" name="cp100Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com100Mil" name="com100Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service120Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 100Mil -->

            <!-- rapport 120Mil -->
            <div id="service120Mil" class="form-group collapse">
            <div class="rapportKm">0 - 120'000 km</div>
            <label class="control-label col-sm-1 small" for="releve120MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve120MilNature" name="releve120MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km120Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km120Mil" name="km120Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois120Mil" name="mois120Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee120Mil" name="annee120Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement120Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement120Mil" name="etablissement120Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture120Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture120MilOui" type="radio" name="facture120Mil" value="1"><label for="facture120MilOui">Oui</label>
            <br/>
            <input id="facture120MilNon" type="radio" name="facture120Mil" value="0"><label for="facture120MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture120MilOui').change(function(){
                    if(this.checked)
                    $("#facture120Oui").css("display", "");  // show
                    else
                    $("#facture120Oui").css("display", "none");  // hide
                });
                $('#facture120MilNon').change(function(){
                    if(this.checked)
                    $("#facture120Oui").css("display", "none");
                    else
                    $("#facture120Oui").css("display", "");
                    });
                </script>

                <div id="facture120Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue120Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue120Mil" name="serviceEffectue120Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee120Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee120Mil" name="pieceChangee120Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur120Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur120MilOui" type="radio" name="nomDetenteur120Mil" value="1"><label for="nomDetenteur120MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur120MilNon" type="radio" name="nomDetenteur120Mil" value="0"><label for="nomDetenteur120MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur120MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur120Oui").css("display", "");  // show
                    else
                    $("#detenteur120Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur120MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur120Oui").css("display", "none");
                    else
                    $("#detenteur120Oui").css("display", "");
                    });
                </script>
                <div id="detenteur120Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom120Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom120Mil" name="prenom120Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp120Mil" name="cp120Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com120Mil" name="com120Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service140Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 120Mil -->

            <!-- rapport 140Mil -->
            <div id="service140Mil" class="form-group collapse">
            <div class="rapportKm">0 - 140'000 km</div>
            <label class="control-label col-sm-1 small" for="releve140MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve140MilNature" name="releve140MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km140Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km140Mil" name="km140Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois140Mil" name="mois140Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee140Mil" name="annee140Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement140Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement140Mil" name="etablissement140Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture140Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture140MilOui" type="radio" name="facture140Mil" value="1"><label for="facture140MilOui">Oui</label>
            <br/>
            <input id="facture140MilNon" type="radio" name="facture140Mil" value="0"><label for="facture140MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture140MilOui').change(function(){
                    if(this.checked)
                    $("#facture140Oui").css("display", "");  // show
                    else
                    $("#facture140Oui").css("display", "none");  // hide
                });
                $('#facture140MilNon').change(function(){
                    if(this.checked)
                    $("#facture140Oui").css("display", "none");
                    else
                    $("#facture140Oui").css("display", "");
                    });
                </script>

                <div id="facture140Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue140Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue140Mil" name="serviceEffectue140Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee140Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee140Mil" name="pieceChangee140Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur140Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur140MilOui" type="radio" name="nomDetenteur140Mil" value="1"><label for="nomDetenteur140MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur140MilNon" type="radio" name="nomDetenteur140Mil" value="0"><label for="nomDetenteur140MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur140MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur140Oui").css("display", "");  // show
                    else
                    $("#detenteur140Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur140MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur140Oui").css("display", "none");
                    else
                    $("#detenteur140Oui").css("display", "");
                    });
                </script>
                <div id="detenteur140Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom140Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom140Mil" name="prenom140Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp140Mil" name="cp140Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com140Mil" name="com140Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service160Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 140Mil -->

            <!-- rapport 160Mil -->
            <div id="service160Mil" class="form-group collapse">
            <div class="rapportKm">0 - 160'000 km</div>
            <label class="control-label col-sm-1 small" for="releve160MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve160MilNature" name="releve160MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km160Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km160Mil" name="km160Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois160Mil" name="mois160Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee160Mil" name="annee160Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement160Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement160Mil" name="etablissement160Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture160Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture160MilOui" type="radio" name="facture160Mil" value="1"><label for="facture160MilOui">Oui</label>
            <br/>
            <input id="facture160MilNon" type="radio" name="facture160Mil" value="0"><label for="facture160MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture160MilOui').change(function(){
                    if(this.checked)
                    $("#facture160Oui").css("display", "");  // show
                    else
                    $("#facture160Oui").css("display", "none");  // hide
                });
                $('#facture160MilNon').change(function(){
                    if(this.checked)
                    $("#facture160Oui").css("display", "none");
                    else
                    $("#facture160Oui").css("display", "");
                    });
                </script>

                <div id="facture160Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue160Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue160Mil" name="serviceEffectue160Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee160Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee160Mil" name="pieceChangee160Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur160Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur160MilOui" type="radio" name="nomDetenteur160Mil" value="1"><label for="nomDetenteur160MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur160MilNon" type="radio" name="nomDetenteur160Mil" value="0"><label for="nomDetenteur160MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur160MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur160Oui").css("display", "");  // show
                    else
                    $("#detenteur160Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur160MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur160Oui").css("display", "none");
                    else
                    $("#detenteur160Oui").css("display", "");
                    });
                </script>
                <div id="detenteur160Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom160Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom160Mil" name="prenom160Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp160Mil" name="cp160Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com160Mil" name="com160Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service180Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 160Mil -->

            <!-- rapport 180Mil -->
            <div id="service180Mil" class="form-group collapse">
            <div class="rapportKm">0 - 180'000 km</div>
            <label class="control-label col-sm-1 small" for="releve180MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve180MilNature" name="releve180MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km180Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km180Mil" name="km180Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois180Mil" name="mois180Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee180Mil" name="annee180Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement180Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement180Mil" name="etablissement180Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture180Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture180MilOui" type="radio" name="facture180Mil" value="1"><label for="facture180MilOui">Oui</label>
            <br/>
            <input id="facture180MilNon" type="radio" name="facture180Mil" value="0"><label for="facture180MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture180MilOui').change(function(){
                    if(this.checked)
                    $("#facture180Oui").css("display", "");  // show
                    else
                    $("#facture180Oui").css("display", "none");  // hide
                });
                $('#facture180MilNon').change(function(){
                    if(this.checked)
                    $("#facture180Oui").css("display", "none");
                    else
                    $("#facture180Oui").css("display", "");
                    });
                </script>

                <div id="facture180Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue180Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue180Mil" name="serviceEffectue180Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee180Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee180Mil" name="pieceChangee180Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur180Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur180MilOui" type="radio" name="nomDetenteur180Mil" value="1"><label for="nomDetenteur180MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur180MilNon" type="radio" name="nomDetenteur180Mil" value="0"><label for="nomDetenteur180MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur180MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur180Oui").css("display", "");  // show
                    else
                    $("#detenteur180Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur180MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur180Oui").css("display", "none");
                    else
                    $("#detenteur180Oui").css("display", "");
                    });
                </script>
                <div id="detenteur180Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom180Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom180Mil" name="prenom180Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp180Mil" name="cp180Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com180Mil" name="com180Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>
                <div><a data-toggle="collapse" data-target="#service200Mil"><i class="fa fa-plus blue" aria-hidden="true"></i> Ajouter un service</a></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 180Mil -->

            <!-- rapport 200Mil -->
            <div id="service200Mil" class="form-group collapse">
            <div class="rapportKm">0 - 200'000 km</div>
            <label class="control-label col-sm-1 small" for="releve200MilNature">Nature du relevé</label>
            <div class="col-sm-2">
            <select class="form-control" id="releve200MilNature" name="releve200MilNature">
                <option value="1ère révision constructeur">1ère révision constructeur</option>
                <option value="Expertise">Expertise</option>
                <option value="Réparation carrosserie">Réparation carrosserie</option>
                <option value="Réparation moteur">Réparation moteur</option>
                <option value="Réparation vitrage">Réparation vitrage</option>
                <option value="Changement d'assurance">Changement d'assurance</option>
                <option value="Autre">Autre</option>
            </select>
            </div>
            <label class="control-label col-sm-1 small" for="km200Mil">Insérez le kilométrage correspondant à ce service</label>
            <div class="col-sm-2">
            <input type="number" class="form-control" id="km200Mil" name="km200Mil">
            </div>

            <label class="control-label col-sm-1 small">Choisissez la date de ce service</label>
            <div class="col-sm-2 radio radio-primary">
            <select class="form-control" id="mois200Mil" name="mois200Mil">
  				<option value="" selected>mois</option>
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
            </select><br />
            <select class="form-control" id="annee200Mil" name="annee200Mil">
  		        <option value="" selected>année</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
                <option value="2010">2010</option>
                <option value="2009">2009</option>
                <option value="2008">2008</option>
                <option value="2007">2007</option>
                <option value="2006">2006</option>
                <option value="2005">2005</option>
                <option value="2004">2004</option>
                <option value="2003">2003</option>
                <option value="2002">2002</option>
                <option value="2001">2001</option>
                <option value="2000">2000</option>
            </select>
            </div>

            <label class="control-label col-sm-1 small" for="etablissement200Mil">Insérez le nom de l'établissement où ce service a été réalisé</label>
            <div class="col-sm-2">
            <input type="text" class="form-control" id="etablissement200Mil" name="etablissement200Mil">
            </div>

            <div class="clear"></div>
            <label class="control-label col-sm-1 small" for="facture200Mil">Possédez-vous la/les factures correspondantes à ce service ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="facture200MilOui" type="radio" name="facture200Mil" value="1"><label for="facture200MilOui">Oui</label>
            <br/>
            <input id="facture200MilNon" type="radio" name="facture200Mil" value="0"><label for="facture200MilNon">Non</label>
            </div>
            <script type="text/javascript">
                $('#facture200MilOui').change(function(){
                    if(this.checked)
                    $("#facture200Oui").css("display", "");  // show
                    else
                    $("#facture200Oui").css("display", "none");  // hide
                });
                $('#facture200MilNon').change(function(){
                    if(this.checked)
                    $("#facture200Oui").css("display", "none");
                    else
                    $("#facture200Oui").css("display", "");
                    });
                </script>

                <div id="facture200Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="serviceEffectue200Mil">Insérez le/les services qui ont été effectués</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="serviceEffectue200Mil" name="serviceEffectue200Mil">
                </div>
                <label class="control-label col-sm-1 small" for="pieceChangee200Mil">Insérez la/les pièces qui ont été changées</label>
                <div class="col-sm-2">
                <input type="text" class="form-control" id="pieceChangee200Mil" name="pieceChangee200Mil">
                </div>
                    <label class="control-label col-sm-1 small" for="nomDetenteur200Mil">Est-ce que le nom du détenteur est visible sur la/les factures ?</label>
            <div class="col-sm-1 radio radio-primary">
            <input id="nomDetenteur200MilOui" type="radio" name="nomDetenteur200Mil" value="1"><label for="nomDetenteur200MilOui">Oui</label>
            <br/>
            <input id="nomDetenteur200MilNon" type="radio" name="nomDetenteur200Mil" value="0"><label for="nomDetenteur200MilNon">Non</label>
            </div>
                </div>
                <script type="text/javascript">
                $('#nomDetenteur200MilOui').change(function(){
                    if(this.checked)
                    $("#detenteur200Oui").css("display", "");  // show
                    else
                    $("#detenteur200Oui").css("display", "none");  // hide
                });
                $('#nomDetenteur200MilNon').change(function(){
                    if(this.checked)
                    $("#detenteur200Oui").css("display", "none");
                    else
                    $("#detenteur200Oui").css("display", "");
                    });
                </script>
                <div id="detenteur200Oui" class="form-group" style="display:none;">
                <label class="control-label col-sm-1 small" for="prenom200Mil">Choisissez la 1ère lettre du prénom du détenteur et son code postal</label>
            <div class="col-sm-1">
            <select class="form-control" id="prenom200Mil" name="prenom200Mil">
  				<option value="" selected>1ère l.</option>
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
                <option>K</option>
                <option>L</option>
                <option>M</option>
                <option>N</option>
                <option>O</option>
                <option>P</option>
                <option>Q</option>
                <option>R</option>
                <option>S</option>
                <option>T</option>
                <option>U</option>
                <option>V</option>
                <option>W</option>
                <option>X</option>
                <option>Y</option>
                <option>Z</option>
            </select>
            </div>
            <div class="col-sm-1">
            <input class="form-control" id="cp200Mil" name="cp200Mil" type="number" min="1000" max="9999" maxlength="4">
            </div>
                </div>
                <div class="clear"></div>

                <textarea class="form-control" rows="5" id="com200Mil" name="com200Mil" placeholder="Inscrivez votre commentaire..."></textarea>

                <div class="clear"></div>

        </div>

        <div class="clear"></div>
            <!-- /rapport 200Mil -->

            <!-- raport securspot -->
        </div>

        <div class="clear"></div>
        <div class="row">
            <div class="col-xs-6">
                <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Sauvegarder <span class="hidden-xs">& continuer</span></button>
            </div>
        </div>
        <div class="clear"></div>
        </div>
    </div>
      <!-- ÉTAPE 3 : SECURSPOT -->

      <!-- ÉTAPE 4 : VENDRE + RAPIDEMENT -->
    <!--<div class="setup-content" id="step-4">-->
    <div class="setup-content" id="step-3" data-page='publier'>
        <div class="col-md-12">
            <hr class="blue">
            <div class="col-sm-4"><h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Vendez + rapidement - obtenez gratuitement votre bannière auto :</h4></div>
            <div class="col-sm-8"><i class="fa fa-exclamation blue" aria-hidden="true"></i> <span class="anoter">Veuillez vous assurer que votre adresse, votre numéro de téléphone et votre email soient corrects.</span></div>
            <hr class="clear blue">
                    <div class="col-sm-12">
                    <div class="col-sm-7">
<p>
Profitez sans plus attendre de cette promotion pour les 250 premières annonces uniquement.
</p>

<p>
En insérant votre annonce sur Autospot.ch, nous vous offrons gratuitement :
</p>

<p>
1. Une bannière auto<br />
2. Un feutre indélébile noir<br />
3. L'expédition à votre domicile Suisse en courrier A
</p>

<p>Nous avons crée cette bannière de 70 cm sur 24 cm pour aider nos utilisateurs à vendre leur voiture plus rapidement. Toutes les personnes qui croiseront votre voiture sauront qu'elle est en vente.</p>
<p>Il vous suffit de la fixer facilement avec ses deux ventouses sur une vitre de votre voiture puis d'écrire le prix, l'année, les kilomètres et votre numéro de téléphone. </p>
<p>Voici en exemple la photographie d'une bannière fixée sur une Mazda.</p>

</div>
<div class="col-sm-5">
    <img src="images/1-1.png" style="width: 100%;">
</div>
                    </div>

        <div class="clear"></div>
        <div class="clear"></div>

        <div class="panel-body form-group col-sm-12">

        <div class="banniereChoixObligatoire hidden" style="color: #a94442;"><b>Veuillez choisir pour passer à l'étape suivante</b></div>
        <div class="radio radio-primary col-sm-5 blueBox">
            <input id="banniereOui" type="radio" value="Oui" name="banniere">
            <label for="banniereOui">Je souhaite recevoir ma bannière</label><br />
            <input id="banniereNon" type="radio" value="Non" name="banniere">
            <label for="banniereNon">Je ne souhaite pas recevoir ma bannière</label>
        </div>

<?PHP
$requete = mysqli_query($connect1, "SELECT * FROM clients WHERE id='$_SESSION[id_client]'");
$client = mysqli_fetch_array($requete);
?>

        <div id="banniereForm" class="form-group col-sm-7" style="display: none;">
        <label class="col-sm-6" for="nomBanniere">Votre nom <span class="requis">*</span><br />
        <input type="text" class="form-control" id="nomBanniere" name="nomBanniere" value="<?PHP echo $client['nom'];?>" required></label>
        <label class="col-sm-6" for="prenomBanniere">Votre prénom <span class="requis">*</span><br />
        <input type="text" class="form-control" id="prenomBanniere" name="prenomBanniere" value="<?PHP echo $client['prenom'];?>" required></label>

        <label class="col-sm-10" for="adresseBanniere">Adresse complète <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="adresseBanniere" name="adresseBanniere" value="<?PHP echo $client['adresse'];?>" required></label>
        <label class="col-sm-2" for="numeroBanniere">N° <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="numeroBanniere" name="numeroBanniere" value="<?PHP echo $client['adresse_num'];?>" required></label>

        <label class="col-sm-4" for="cpBanniere">Code postal <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="cpBanniere" name="cpBanniere" value="<?PHP echo $client['code_postal'];?>" required></label>
        <label class="col-sm-8" for="villeBanniere">Ville <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="villeBanniere" name="villeBanniere" value="<?PHP echo $client['ville'];?>" required></label>
        
				</div>

        </div>

        <div class="clear"></div>
        <div class="row">
            <div class="col-xs-6">
                <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Sauvegarder <span class="hidden-xs">& continuer</span></button>
            </div>
        </div>
        <div class="clear"></div>
                    </div>
        </div>

     <!-- FIN ÉTAPE 4 : VENDRE + RAPIDEMENT -->

      <!-- ÉTAPE 5 : CONTACT & CONFIRMATION -->
    <!--<div class="setup-content" id="step-5">-->
    <div id="confAlert" class="alert alert-success" role="alert" style="display: none">
        Veuillez patienter pendant la création de votre annonce !
    </div>
    <div class="setup-content" id="step-4" data-page='publier'>
        <div class="col-md-12">
        <hr class="blue">
        <div class="col-sm-4"><h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Contact & confirmation :</h4></div>
        <div class="col-sm-8"><i class="fa fa-exclamation blue" aria-hidden="true"></i> <span class="anoter">Veuillez vous assurer que votre adresse, votre numéro de téléphone et votre email soient corrects.</span></div>
        <hr class="clear blue">

    <div class="panel-body form-group col-sm-12">

        <div class="col-sm-4">
          <label>Coordonnées de contact :</label>
        </div>

        <div class="form-group col-sm-8">

        <label class="col-sm-10" for="adresseC">Adresse complète <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="adresseC" name="adresseC" value="<?PHP echo $client['adresse'];?>"></label>
        <label class="col-sm-2" for="numeroC">N° <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="numeroC" name="numeroC" value="<?PHP echo $client['adresse_num'];?>"></label>

        <label class="col-sm-4" for="cpC">Code postal <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="cpC" name="cpC" value="<?PHP echo $client['code_postal'];?>"></label>
        <label class="col-sm-8" for="villeC">Ville <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="villeC" name="villeC" value="<?PHP echo $client['ville'];?>"></label>

        <label class="col-sm-4" for="numeroTelC">N° de téléphone <span class="requis">*</span><br />
        <input required type="text" class="form-control" id="numeroTelC" name="numeroTelC" value="<?PHP echo $client['telephone'];?>"></label>
        </div>
				
        <div class="col-sm-4">
          <div class="moyenDeContactsObligatoiresInfo hidden" style="color: #a94442;"><b>Ce champ est requis</b></div>
          <label>Moyens de contact : <span class="requis">*</span></label>
        </div>

        <div class="radio radio-primary col-sm-8">
        <input id="formulaireContact" type="radio" name="typeContactC" value="1"<?PHP if((isset($id_annonce)) && ($client['typecontact'] == 1)) echo " checked";?>><label for="formulaireContact">Formulaire de contact (recommandé)</label><br/>
        <input id="telContact" type="radio" name="typeContactC" value="2"<?PHP if((isset($id_annonce)) && ($client['typecontact'] == 2)) echo " checked";?>><label for="telContact">Téléphone</label><br/>
        <input id="formTelContact" type="radio" name="typeContactC" value="3"<?PHP if((isset($id_annonce)) && ($client['typecontact'] == 3)) echo " checked";?>><label for="formTelContact">Formulaire de contact & téléphone</label>
        </div>

        <?php 
            // Cacher J'adhère à la charte qualité pour les types 1 & 2
            $hideCharteQualite = in_array($client['type'], [1,2]) ? true : null;
        ?>
        <div class="col-sm-12" style="margin-bottom:25px; <?php if($hideCharteQualite) { echo 'display: none;'; } ?>">
            <div class="col-sm-4">
                <label>Charte qualité : <span class="requis">*</span></label>
            </div>
            <div class="charte col-sm-8">
                <div class="col-sm-10">Selon notre charte qualité, lorsqu'un vendeur dépose son annonce sur autospot il accepte la possibilité qu'un acheteur potentiel demande à ce que la voiture du vendeur soit vérifié chez l’un de nos garages partenaires. Ce service est gratuit pour le vendeur.</div>
									
                <!-- <div class="checkbox checkbox-primary col-sm-10">
                    <input id="charteQualite" type="checkbox" value="oui" name="charteQualite"<?PHP if(isset($id_annonce) || $hideCharteQualite) echo " checked";?> required> <label for="charteQualite">J'adhère à la charte qualité en acceptant ces deux points <span class="requis">*</span></label>
                </div> -->
                <div class="qualite col-sm-2">
                    <img src="images/icon_qualite.png" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <div class="checkbox checkbox-primary col-sm-4">
        </div>
        <div class="checkbox checkbox-primary col-sm-8">
        <input id="conditionsC" type="checkbox" value="oui" name="conditionsC"<?PHP if(isset($id_annonce)) echo " checked";?> required><label for="conditionsC">J'accepte les conditions générales <span class="requis">*</span></label>
        </div>

        <div class="clear"></div>

        <div class="row">
            <div class="col-xs-6">
                <button class="btn btn-default prevBtn" type="button">Précédent</button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-primary confBtn btn-lg pull-right toTop" type="button" name="envoyer">Sauvegarder <span class="hidden-xs">& continuer</span></button>
            </div>
        </div>
        <div class="clear"></div>
        </div>
        </div>

    </div>

     <!-- FIN ÉTAPE 5 : CONTACT & CONFIRMATION -->

    </form>


<!-- FIN CONTAINER FORMULAIRE EN 5 étapes -->
</div>

<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $.fn.datepicker.dates['en'] = {
        days: ["Dimanch", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        daysMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre"],
        monthsShort: ["Jan", "Fév", "Mar", "Avr", "Mai", "Jun", "Jul", "Aoû", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        clear: "Effacer",
        format: "yyyy-mm",
        titleFormat: "mm-yyyy", /* Leverages same syntax as 'format' */
        weekStart: 1
    };

    $(".datepicker").datepicker({
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        language: 'fr',
        autoclose: true,
    });
</script>


<?php include("footer.php"); ?>