<?php
session_start();

include("db_connexion.php");

$id_client = null;
$key = array_key_exists('key', $_POST) && !empty($_POST['key']) ? trim($_POST['key']) : (array_key_exists('key', $_GET) && !empty($_GET['key']) ? trim($_GET['key']) : null);
if (null === $key) {
    header('Location: inscription.php');
}

$sql = sprintf('SELECT id FROM clients WHERE MD5(email) = "%s"', mysqli_real_escape_string($connect1, $key));
$query = mysqli_query($connect1, $sql);
$client = mysqli_fetch_object($query);

if (null === $client) {
    header('Location: inscription.php');
}

if (sizeof($_POST) > 0) {
    $password = array_key_exists('password', $_POST) && !empty($_POST['password']) ? trim($_POST['password']) : null;
    $password_confirmation = array_key_exists('password_confirmation', $_POST) && !empty($_POST['password_confirmation']) ? trim($_POST['password_confirmation']) : null;

    if ($password === null) {
        $error = 'Votre mot de passe ne peut pas être vide !';
    } elseif ($password !== $password_confirmation) {
        $error = 'Les mots de passe ne sont pas identiques !';
    } else {
        $sql = sprintf('UPDATE clients SET mot_de_passe=MD5("%s") WHERE id = "%s"', mysqli_real_escape_string($connect1, $password), mysqli_real_escape_string($connect1, $client->id));
        mysqli_query($connect1, $sql);

        $_SESSION['id_client'] = $client->id;
        $update = true;
    }
}

include("header.php");
include("body.php");
?>

<div class="container-fluid">
    <h3 class="center">Réinitialisez votre mot de passe </h3>

    <p align="center">Veuillez s’il vous plaît entrer un nouveau mot de passe.</p>

    <form class="form-horizontal" action="/mot_de_passe_oublie_confirmation.php" method="post">
        <input type="hidden" name="key" value="<?php echo $key; ?>" />
        <?php if (isset($update)): ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="email"></label>
                <div class="col-sm-8">
                    <div class="alert alert-success" role="alert">
                        Nouveau mot de passe enregistré !
                    </div>
                </div>
            </div>
        <?php elseif (isset($error)): ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="email"></label>
                <div class="col-sm-8">
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label class="control-label col-sm-3" for="password">Nouveau mot de passe : <span class="requis">*</span></label>
            <div class="col-sm-8">
                <input type="password" class="form-control" name="password" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="password_confirmation">Confirmer votre mot de passe : <span class="requis">*</span></label>
            <div class="col-sm-8">
                <input type="password" class="form-control" name="password_confirmation" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3"></label>
            <div class="col-sm-8">
                <button type="button" class="btn btn-default">Annuler</button>
                <button type="submit" class="btn btn-small btn-primary">Envoyer</button>
            </div>
        </div>
    </form>
</div>

<?php if (isset($update)): ?>
    <script type="text/javascript">
        window.setTimeout(function() {
            window.location.href = '/compte.php?profil';
        }, 5000);
    </script>
<?php endif; ?>

<?php
include("footer.php");