<?php

require_once('appbackend/admin/include/db_connect.php');
require_once('appbackend/events.php');



session_start();

if (!isset($_SESSION['id_client'])) {
    header('Location: inscription.php');
}

if (sizeof($_POST) > 0) {
    include('db_connexion.php');

    $sql = sprintf(
        'DELETE FROM commandes_bannieres WHERE id_client = "%s";' .
        'DELETE FROM clients_favoris WHERE client = "%s";'.
        'DELETE FROM annonces_clients WHERE id_client = "%s";'.
        'DELETE FROM clients WHERE id = "%s";',
        mysqli_real_escape_string($connect1, $_SESSION['id_client']),
        mysqli_real_escape_string($connect1, $_SESSION['id_client']),
        mysqli_real_escape_string($connect1, $_SESSION['id_client']),
        mysqli_real_escape_string($connect1, $_SESSION['id_client'])
    );
	$query = mysqli_query($connect1, $sql);
	
	
	$sql = sprintf('SELECT * FROM annonces_clients WHERE id_client = "%s" AND visible=1', mysqli_real_escape_string($connect1, $_SESSION['id_client']));
	$query = mysqli_query($connect1, $sql);
	while($tab = mysqli_fetch_array($query)){
		
		$id_annonce = $tab['id'];
		$id_client = $_SESSION['id_client'];
		
		
		//todo à tester
		
		mysqli_query($connect1, "UPDATE annonces_clients SET visible=0 WHERE id='$id_annonce' && id_client='$id_client'");
		
		$req_demandes = mysqli_query($connect1, "SELECT * FROM demandes_exam WHERE id_annonces_clients='$id_annonce'");
		while($tab2 = mysqli_fetch_array($req_demandes)){
			$id_demande = $tab2['id'];
			// echo "id : ".$id_demande.',';
			delete_announce($dbh, $id_demande);
		}
		
	}

    $notice = 'Votre compte et les données associées ont été supprimées avec succès !'; 
    unset($_SESSION['id_client']);
}

include("header.php");
include("body.php");
?>

<div class="container-fluid">
    <h3 class="center">Supprimer mon compte</h3>

    <p align="center">En confirmant la suppression de votre compte, toutes les annonces et données associées seront supprimées.</p>

    <form class="form-horizontal" action="/mot_de_passe_oublie_confirmation.php" method="post">
        <?php if (isset($notice)): ?>
            <div class="form-group">
                <label class="control-label col-sm-3"></label>
                <div class="col-sm-8">
                    <div class="alert alert-success" role="alert">
                        <?php echo $notice; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="text-center">
            <a class="btn btn-default btn-inline" href="compte.php?profil">Annuler</a>
            <button type="submit" class="btn btn-small btn-primary btn-inline">Confirmer la suppression</button>
        </div>
    </form>
</div>

<?php if (isset($notice)): ?>
    <script type="text/javascript">
        window.setTimeout(function() {
            window.location.href = '/index.php';
        }, 5000);
    </script>
<?php endif; ?>

<?php
include("footer.php");