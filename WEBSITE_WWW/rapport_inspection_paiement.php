<?php
session_start();
require_once('classes/commande_rapport.php');

if (!count($_POST)) {
    header('Location: index.php');
    exit;
}

// Get POST parameters
$id_client = array_key_exists('id_client', $_POST) && intval($_POST['id_client']) != 0 ? intval($_POST['id_client']) : null;
$id_demande_exam = array_key_exists('id_demande', $_POST) && intval($_POST['id_demande']) != 0 ? intval($_POST['id_demande']) : null;

if ($id_client === null || $id_demande_exam === null) {
    header('Location: index.php');
    exit;
}

include('db_connexion.php');

$sql = mysqli_query($connect1, 'SELECT * FROM clients WHERE id="' . $id_client .'";');
$client = mysqli_fetch_object($sql);

$sql = mysqli_query($connect1, 'SELECT * FROM demandes_exam WHERE id="' . $id_demande_exam .'";');
$demande = mysqli_fetch_object($sql);

if ($client === false || $demande === false) {
    header('Location: index.php');
    exit;
}

// Insert row in demandes_exam table
$sql = "INSERT INTO commandes_rapports_inspection (id_demande_exam, id_client, paiement_status, created_at, updated_at) 
    VALUES ('".$id_demande_exam."', '".$id_client."', '".commande_rapport::PAIEMENT_PROCESSING."', NOW(), NOW())";

if (!mysqli_query($connect1, $sql)) {
    // Unexpected error
    header('Location: rapport_inspection.php?id='.$id_demande_exam.'&errorCode=1');
    exit;
}

$id_commande = mysqli_insert_id($connect1);

// Send form to klikandpay
?>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Autospot.ch</title>
</head>
<body>
<p>Vous allez être rediriger vers notre partenaire bancaire.</p>
<p>Si la page ne se charge pas dans quelques secondes, vous pouvez cliquer sur le bouton "Valider" ci-dessous.</p>
<form method="post" action="https://www.klikandpay.com/paiement/order1.pl" accept-charset="UTF8" name="prepayment">
    <input type="hidden" name="NOM" size="24" value="<?php echo $client->nom; ?>" />
    <input type="hidden" name="PRENOM" size="24" value="<?php echo $client->prenom; ?>" />
    <input type="hidden" name="ADRESSE" size="24" value="<?php echo $client->adresse_num . ' ' . $client->adresse; ?>" />
    <input type="hidden" name="CODEPOSTAL" size="24" value="<?php echo $client->code_postal; ?>" />
    <input type="hidden" name="VILLE" size="24" value="<?php echo $client->ville; ?>" />
    <input type="hidden" name="PAYS" size="24" value="CH" />
    <input type="hidden" name="TEL" size="24" value="<?php echo $client->telephone; ?>" />
    <input type="hidden" name="EMAIL" size="24" value="<?php echo $client->email; ?>" />
    <input type="hidden" name="ID" value="1504875514" />
    <input type="hidden" name="MONTANT" value="49" />
    <input type="hidden" name="RETOURVOK" value="?id=<?php echo $id_commande; ?>&type=rapport" />
    <input type="hidden" name="RETOURVHS" value="?id=<?php echo $id_commande; ?>&type=rapport" />
    <input type="submit" name="valider" value="Valider" />
</form>
<script type="text/javascript">
    window.onload = function() {
        window.setTimeout('document.prepayment.submit()', 3000)
    }
</script>
</body>
</html>
