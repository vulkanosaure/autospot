<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr-FR" lang="fr-FR">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
            if (session_status() == PHP_SESSION_NONE)
                session_start();
            if (isset($_GET['lang']))
                $_SESSION['lang'] = $_GET['lang'];
            else if (!isset($_SESSION['lang']))
                $_SESSION['lang'] = "fr";
            include("langues/lang_$_SESSION[lang].php");
            if (!isset($connect1))
                include("db_connexion.php");
            $tableannonces = "annonces_clients";
            ?>
            <?php
            if (preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
                ?><title>MotoSpot.ch | <?php echo $LANG[151]; ?></title>
                <?php
            } else {
                ?><title>AutoSpot.ch | <?php echo $LANG[151]; ?></title>
                <?php
            }
            ?>

                <link rel="stylesheet" href="css/bootstrap.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                    <link rel="stylesheet" href="css/custom_styles.css">
                        <?php
                        if (preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
                            ?><link rel="stylesheet" href="css/style_motospot.css">
                                <?php
                            } else {
                                ?><link rel="stylesheet" href="css/style_autospot.css">
                                    <?php
                                }
                                ?><link rel="stylesheet" href="css/index.css">
                                    <link rel="stylesheet" href="css/steps.css">
                                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
                                            <?php
                                            if (preg_match("/www.motospot.ch/", $_SERVER["HTTP_HOST"])) {
                                                ?><link rel="shortcut icon" type="image/x-icon" href="http://www.autospot.ch/motospot.ico">
                                                    <?php
                                                } else {
                                                    ?>
                                                    <link rel="shortcut icon" type="image/x-icon" href="http://www.autospot.ch/autospot.ico">
                                                        <?php
                                                    }
                                                    ?>

                                                    <script src="js/jquery.min.js"></script>
                                                    <script src="js/nav1.js" data-minify="1"></script>
                                                    <script src="js/navsb1.js" data-minify="1"></script>
                                                    <script src="js/bootstrap.min.js"></script>
                                                    <script src="js/steps.js"></script>

                                                    <!-- include summernote css/js-->
                                                    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
                                                        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>

                                                        <!-- Selectpicker pour les pictos carrosserie dans le select -->
                                                        <script src="js/bootstrap-select.js"></script>
                                                        <link rel="stylesheet" type="text/css" href="css/bootstrap-select.css" />

                                                        <!-- lightslider -->
                                                        <link rel="stylesheet" type="text/css" href="css/lightslider.css" />     
                                                        <script src="js/lightslider.js"></script>                
                                                        <link rel="stylesheet" type="text/css" href="css/lightgallery.css" />
                                                        <script src="js/lightgallery.js"></script>
                                                        <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.5/css/lightslider.css" />     
                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.5/js/lightslider.js"></script>                
                                        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/css/lightgallery.css" />
                                            <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.21/js/lightgallery.js"></script>-->

                                                        <!-- /lightslider  -->


                                                        <!--    SCRIPT onSCROLL POUR FIXER les FILTRES version MOBILE-->
                                                        <script type='text/javascript'>
                                                            $(function () {
                                                                $(window).scroll(function () {
                                                                    if ($(this).scrollTop() > 150) { //hauteur Ã  laquelle on veut dÃ©clencher la fixation en haut du menu
                                                                        $('#fixedMenu').addClass("fixMenu");
                                                                        $('#posRel').addClass("hidden");
                                                                        $('#posFix').removeClass("hidden");
                                                                        $('#pushFiltres').removeClass("noPush");
                                                                        $('#pushFiltres').addClass("push");
                                                                    } else {
                                                                        $('#fixedMenu').removeClass("fixMenu");
                                                                        $('#posRel').removeClass("hidden");
                                                                        $('#posFix').addClass("hidden");
                                                                        $('#pushFiltres').removeClass("push");
                                                                        $('#pushFiltres').addClass("noPush");
                                                                    }
                                                                });
                                                            });
                                                        </script>
                                                        <!--    SCRIPT onSCROLL POUR FIXER les FILTRES version MOBILE-->
                                                        <link rel="stylesheet" href="css/swiper.css">
                                                            <script src="js/swiper.js"></script>
                                                            </head>