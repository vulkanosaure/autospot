function changemarque(taille){
    if (taille !== undefined) {
       	r = document.getElementById('r').value;
        if(taille == 480)
            marque = document.getElementById('marque480').value;
    	else if(taille == 1280)
            marque = document.getElementById('marque1280').value;
    	else if(taille == 1920)
            marque = document.getElementById('marque1920').value;
        url = '//'+window.location.hostname+'/ajax_affiner.php';
    	params = 'marque='+marque+'&r='+r+'&taille='+taille;
    }
    else {
        marque = document.getElementById('marque').value;
        url = '//'+window.location.hostname+'/ajax_affiner.php';
    	params = 'marque='+marque;
    }
	ajax(url, params);
}

// function changemarque2() {
//     var modeleslist = $('#modeleslist').text().split(";"),
//         marque_selected = $('#marque').val(),
//         modeleselected = $('#modeleselected').text(),
//         options = ['<option value=""></option>'];
//     for (var i = modeleslist.length - 1; i >= 0; i--) {
//         var split = modeleslist[i].split(','),
//             marque = split[0],
//             modele = split[1];

//         if (marque == marque_selected) {
//             if (modele == modeleselected) { var selected = 'selected'; }else{ var selected = ''; }
//             option = '<option value='+modele+' '+selected+'>'+modele+'</option>';
//             options.push(option);
//         }
//     }
//     $('#modele').html(options.join(''));
// }
// changemarque2();





function favoris(annonce)
{
	console.log('favoris');
	
    url = '//'+window.location.hostname+'/ajax_favoris.php';
	params = 'annonce='+annonce;

    $.post(url, params, function(response, status) {
		
		var data = JSON.parse(response);
		// debugger;
		console.log('callback '+status);
		
        if (status == 'success') {
            if (!data.insert) {
                $('#desc-fav'+annonce).removeClass('desc-fav-active').addClass('desc-fav-inactive');
                message = 'L’annonce a été retirée de vos favoris avec succès !';
				type = 'success';
				
				//delete from listing
				$('#favoris-'+annonce).css('display', 'none');
				
            } else {
                $('#desc-fav'+annonce).removeClass('desc-fav-inactive').addClass('desc-fav-active');
                message = 'L\'annonce a été rajoutée à vos favoris avec succès !';
				type = 'warning';
				
            }
        } else {
            message = 'Impossible d\'ajouter l\'annonce à vos favoris';    
            type = 'danger';
        }

        $.notify({
            message: message
        }, {
            type: type,
            position: 'fixed',
            z_index: 9999
		});
		
		location.reload();
    });

    return false;
}
function ajax(url, params){
	$.post(url, params, function(response){
		jQuery.globalEval(response);
	});
}




function resetFormFilter(form)
{
	console.log('resetFormFilter');
	
	var inputs = form.elements;
	
	for (var i = 0, input; input = inputs[i++];) {
			if(input.type == 'select-one') input.selectedIndex  = '0';
			else if (input.type === "text") input.value = '';
			// debugger;
	}
	
}