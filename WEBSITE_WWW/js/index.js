$(document).ready(function() {
    $('.search_by_reference > input').on('keyup', function(e) {
        if (/^\d{6,}$/.test($(this).val())) {
            $(this).siblings('button').prop('disabled', false);
        } else {
            $(this).siblings('button').prop('disabled', true);
        }
    });

    $('.search_by_reference > button').on('click', function(e) {
        if ($(this).prop('disabled')) {
            e.preventDefault();
        }
    });
});



function changeTypeInscription(_type, _label1, _label2)
{
	var label = (_type == 0) ? _label1 : _label2;
	var display = (_type == 0) ? 'block' : 'none';
	
	$('#form-label-sunom').html(label);
	$('#form-item-suprenom').css('display', display);
}


