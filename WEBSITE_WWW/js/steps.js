var nbUpload = 0;
jQuery(document).ready(function() {
    var navListItems = jQuery('div.setup-panel div a'),
        allWells = jQuery('.setup-content'),
        allNextBtn = jQuery('.nextBtn'),
        allPrevBtn = jQuery('.prevBtn'),
        confBtn = jQuery('.confBtn');
    if (!jQuery('div.setup-panel').hasClass('ignore')) {
        allWells.hide();
    }
    navListItems.click(function(e) {
        e.preventDefault();
        var $target = jQuery(jQuery(this).attr('href')),
            $item = jQuery(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    var lastfilled = 0;
    for (var i = 0; i < 6; i++) {
        if (isModuleFilled(i)) lastfilled = i;
    }
    for (var i = 0; i < 6; i++) {
        var visible = (i == 0);
        if (!isModuleFilled(i)) {
            var visible = (i <= lastfilled);
            setModuleVisible(i, visible);
        }
    }
    updateInputs();

    function getValueByKey(_inputs, _key) {
        for (var i in _inputs) {
            var input = _inputs[i];
            if (input.name == _key) return input.value;
        }
        return "";
    }
    allNextBtn.click(function() {
        var curStep = jQuery(this).closest(".setup-content");
        var curStepBtn = curStep.attr("id"),
            nextNextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().next().children("a"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");
        var dataPage = curStep.data('page'); //choisis-forfait
        var curInputs = curStep.find("textarea,select,input[type='text'],input[type='url'],input[type='checkbox'],input[type='radio'],input[type='date'],input[type='number']");
        var isValid = true;
        var red = false;
        jQuery(".form-group").removeClass("has-error");
        jQuery("#redAlert").removeClass("alertShow");
        var automaticSelected = false;
        var noGarantiSelected = false;
        if (curStepBtn == "step-1" && dataPage == 'publier') {
            var value = curInputs[9].value;
            automaticSelected = (getValueByKey(curInputs, 'transmission') == 'automatique');
            noGarantiSelected = (getValueByKey(curInputs, 'typeGarantie') == 'Sans garantie');
        }
        var forceValid = false;
        /* 
        if(["step-1" , "step-2", "step-3"].indexOf(curStepBtn) != -1) forceValid = true;
        // forceValid = true;
         */
        if (!forceValid) {
            if (curStepBtn == "step-3" && dataPage == 'publier') {
                var banniereOuiChecked = $('#banniereOui').attr('checked');
                var banniereNonChecked = $('#banniereNon').attr('checked');
                if (!banniereOuiChecked && !banniereNonChecked) {
                    $('.banniereChoixObligatoire').removeClass('hidden');
                    isValid = false;
                    red = true;
                    jQuery(curInput).closest(".form-group").addClass("has-error");
                }
            } else {
                for (var i = 0; i < curInputs.length; i++) {
                    var curInput = curInputs[i];
                    if (curInput.id == 'vitesses' && automaticSelected) continue;
                    if (curInput.id == 'dureeGarantie' && noGarantiSelected) continue;
                    if (!curInput.validity.valid) {
                        isValid = false;
                        red = true;
                        jQuery(curInput).closest(".form-group").addClass("has-error");
                    }
                }
            }
            //images
            if (curStepBtn == "step-2" && dataPage == 'publier') {
                // if(nbpic < 1){
                if (!isModuleFilled(0)) {
                    isValid = false;
                    red = true;
                    var num = 1;
                    var input = jQuery('#publier').find('input[name="image' + num + '"]');
                    jQuery(input[0]).closest("div").find(".thumbnail").addClass("has-error2");
                }
            }
        }
        if (isValid) {
            if (jQuery(this).hasClass('skip-1')) {
                nextStepWizard.removeAttr('disabled');
                nextNextStepWizard.removeAttr('disabled').trigger('click');
            } else {
                nextStepWizard.removeAttr('disabled').trigger('click');
            }
        }
        if (red)
            jQuery("#redAlert").addClass("alertShow");
    });
    /* confirmation */
    confBtn.click(function() {
        var curStep = jQuery(this).closest(".setup-content"),
            curStepBtn = "step-5",
            nextStepWizard = jQuery('div.setup-panel div a[href="#step-5"]').parent().next().children("a"),
            curInputs = curStep.find("textarea,select,input[type='text'],input[type='url'],input[type='checkbox'],input[type='radio']"),
            isValid = true,
            red = false,
            typeContactC = $("input[name='typeContactC']:checked").val();
        jQuery(".form-group").removeClass("has-error");
        jQuery("#redAlert").removeClass("alertShow");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                red = true;
                jQuery(curInputs[i]).closest("label").addClass("has-error");
                jQuery(curInputs[i]).closest(".checkbox").addClass("has-error3");
            }
        }
        // Vérifier si Moyens de contact a été coché
        if (!typeContactC) { 
            isValid = false;
            red = true;
            $("label[for='formulaireContact'], label[for='telContact'], label[for='formTelContact']").css('color', '#a94442');
        }

        if (isValid) {
            confBtn.hide();
            jQuery("#publier").submit();
            jQuery('#confAlert').show();
        }
        if (red) jQuery("#redAlert").addClass("alertShow");
    });
    /* /confirmation */
    allPrevBtn.click(function() {
        var curStep = jQuery(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevPrevStepWizard = jQuery('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().prev().children("a"),
            prevStepWizard = jQuery('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
        jQuery("#redAlert").removeClass("alertShow");
        jQuery(".form-group").removeClass("has-error");
        if (jQuery(this).hasClass('skip-1')) {
            prevStepWizard.removeAttr('disabled');
            prevPrevStepWizard.removeAttr('disabled').trigger('click');
        } else {
            prevStepWizard.removeAttr('disabled').trigger('click');
        }
    });
    jQuery('div.setup-panel:not(.ignore) div a.btn-primary').trigger('click');
    jQuery('#banniereNon').change(function() {
        if (this.checked)
            document.getElementById("nomBanniere").required = false;
        document.getElementById("prenomBanniere").required = false;
        document.getElementById("adresseBanniere").required = false;
        document.getElementById("numeroBanniere").required = false;
        document.getElementById("cpBanniere").required = false;
        document.getElementById("villeBanniere").required = false;
        jQuery("#banniereForm").css("display", "none"); // hide
    });
    jQuery('#banniereOui').change(function() {
        if (this.checked)
            document.getElementById("nomBanniere").required = true;
        document.getElementById("prenomBanniere").required = true;
        document.getElementById("adresseBanniere").required = true;
        document.getElementById("numeroBanniere").required = true;
        document.getElementById("cpBanniere").required = true;
        document.getElementById("villeBanniere").required = true;
        jQuery("#banniereForm").css("display", ""); // show
    });
    jQuery('#transmission').change(function(e) {
        //jQuery('#transmission')
        var value = e.currentTarget.value;
        var visible = (value != 'automatique');
        var styledisplay = visible ? 'block' : 'none';
        $('#group-vitesses').css('display', styledisplay);
    });
});
//______________________________________________________
function deleteUpload(index) {
    // setModuleVisible(index, false);
    resetModule(index);
    updateInputs();
}

function onChangeInput(index) {
    var num = index + 1;
    var link = $('#link-delete' + num);
    var value = $('#input-file' + num).val();
    if (value != '') link.css('visibility', 'visible');
    else link.css('visibility', 'hidden');
    setTimeout(updateInputs, 200);
}

function updateInputs() {
    for (var i = 0; i < 6; i++) {
        if (i == 5 || !isModuleVisible(i + 1)) {
            //last visible
            if (isModuleFilled(i)) {
                resetModule(i + 1);
                setModuleVisible(i + 1, true);
            }
            break;
        }
    }
}

function setModuleVisible(index, visible) {
    var strdisplay = visible ? 'block' : 'none';
    $('#module-upload-' + (index + 1)).css('display', strdisplay);
}

function resetModule(index) {
    var num = index + 1;
    $('#image_preview' + num).find('img').attr('src', 'images/telecharger-les-photos.png');
    $('#link-delete' + num).css('visibility', 'hidden');
    $('#input-file' + num).attr('value', '');
}

function isModuleVisible(index) {
    return ($('#module-upload-' + (index + 1)).css('display') == 'block');
}

function isModuleFilled(index) {
    var num = index + 1;
    var src = $('#image_preview' + num).find('img').attr('src');
    return src != 'images/telecharger-les-photos.png';
}