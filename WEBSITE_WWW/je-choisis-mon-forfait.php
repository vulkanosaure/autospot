<?php
session_start();
require_once('vendor/autoload.php');
include('db_connexion.php');
require_once('classes/client.php');
require_once('classes/Display.php');

$id = mysqli_real_escape_string($connect1, $_GET['id']);
$requete1 = mysqli_query($connect1, "SELECT * FROM `annonces_clients` WHERE id='$id'");
$annonces = mysqli_fetch_object($requete1);
$requete2 = mysqli_query($connect1, "SELECT * FROM `clients` WHERE id='$annonces->id_client'");
$annonceur = mysqli_fetch_object($requete2);

$client = client::getCurrent();

include('header.php');
include('body.php');



$step1style = ' style="display: none;"';
$step2style = ' style="display: none;"';
$step3style = ' style="display: none;"';
$step4style = ' style="display: none;"';
$step5style = ' style="display: none;"';

if (isset($_GET['step2'])) {
    if (client::isLoggedIn()) {
        $step = 3;
        $step3style = ' style="display: block;"';
    } else {
        $step = 2;
        $step2style = ' style="display: block;"';
    }
} elseif (isset($_GET['step3'])) {
    $step = 3;
    $step3style = ' style="display: block;"';
} elseif (isset($_GET['step4'])) {
    $step = 4;
    $step4style = ' style="display: block;"';;
} elseif (isset($_GET['step5'])) {
    $step = 5;
    $step5style = ' style="display: block;"';
} else {
    $step = 1;
    $step1style = ' style="display: block;"';
}
?>

<!-- CONTAINER FORMULAIRE EN 5 étapes -->
<div class="container">

    <div class="stepwizard col-sm-12">
        <div class="stepwizard-row setup-panel etapes-row ignore">
            <div class="stepwizard-step">
                <p>Annonce</p>
                <a href="#step-1" type="button" class="btn btn-primary btn-circle etapeIcone">1</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Connexion / Inscription</p>
                <a href="#step-<?php echo (client::isLoggedIn() ? '3' : '2'); ?>" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 2 ? ' disabled="disabled"' : ''; ?>>2</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Forfait</p>
                <a href="#step-3" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 3 ? ' disabled="disabled"' : ''; ?>>3</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Moyen de paiement</p>
                <a href="#step-4" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 4 ? ' disabled="disabled"' : ''; ?>>4</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
            <div class="stepwizard-step">
                <p>Félicitations</p>
                <a href="#step-5" type="button" class="btn btn-default btn-circle etapeIcone"<?php echo $step < 5 ? ' disabled="disabled"' : ''; ?>>5</a>
                <div class="arrowStep"><i class="fa fa-caret-up fa-2x" aria-hidden="true"></i></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div id="redAlert" class="alert alert-danger">
        <span class="lettrine"><i class="fa fa-exclamation" aria-hidden="true"></i></span> <strong>Données manquantes :</strong> Veuillez remplir les champs marqués en rouges.
    </div>

        <?php if ($_GET['resend'] == 'ok'): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Un nouvel email vous a été envoyé !
            </div>
        <?php endif ?>

        <?php if ($_GET['update'] == 'ok'): ?>
            <div class="alert alert-<?= $_GET['type'] ;?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $_GET['message'] ;?>
            </div>
        <?php endif ?>

        <?php if ($_GET['email-confirmed'] == 'ok'): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Votre adresse email est à présent confirmée !
            </div>
        <?php endif ?>

        <!-- ÉTAPE 1 : CONTRÔLEZ L'ANNONCE -->
        <div class="setup-content" id="step-1"<?php echo $step1style; ?>>

            <h3 class="blue">Contrôlez l'annonce</h3>

            <div class="col-sm-12">
                <div class="col-sm-6">

                    <p class="larger">Félicitation, vous avez trouvé la voiture qui vous plaît.<br /><br />
                        Grâce à AutoSpot, vous pouvez connaître l'état réel de la <?PHP echo "$annonces->marque $annonces->modele $annonces->version"; ?> que vous envisagez d'acheter.</p>
                    <p class="larger">Pour poursuivre, merci de contrôler les données ci-après :</p>

                    <table class="table">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <h4>Données de base :</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>Prix :</td>
								<td><?php echo $annonces->prix; 
								if($annonces->prixdiscuter == 1) echo ' (à discuter)';
								?></td>
                            </tr>
                            <tr>
                                <td>Année :</td>
                                <td>
                                    <?php
                                    $premiereimm = explode("-", $annonces->premiereimm);
                                    $premiereimm = $premiereimm[0];
                                    echo $premiereimm; 
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Kilomètres :</td>
                                <td><?php echo $annonces->kilometrage; ?></td>
                            </tr>
														
														<?php if($annonces->prixneuf){ ?>
														<tr>
																<td>Prix neuf:</td>
																<td><?php echo $annonces->prixneuf.'.-'; ?></td>
														</tr>
														<?php } ?>
														
                            <tr>
                                <td colspan="2">
                                    <h4>Etat & garantie :</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>Catégorie :</td>
                                <td><?php echo $annonces->categorie; ?></td>
                            </tr>
                            <tr>
                                <td>Etat du véhicule :</td>
                                <td>
                                    <?php
                                    if ($annonces->accident == 1) {
                                        echo "Véhicule accidenté";
                                    } else {
                                        echo "Véhicule non accidenté";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Expertisée :</td>
                                <td>
                                    <?php
                                    if ($annonces->expertisee == "0000-00-00") {
                                        echo "N.D";
                                    } else {
                                        echo $annonces->expertisee;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Garantie :</td>
                                <td><?php echo $annonces->typeGarantie; ?></td>
							</tr>
							
							
							<?php if($annonces->typeGarantie != 'Sans garantie'){ ?>
							<tr>
								<td>Durée de la garantie</td>
								<td><?php echo Display::formatMonth($annonces->dureeGarantie); ?></td>
							</tr>
							<?php } ?>
							
							
							
                            <tr>
                                <td colspan="2">
                                    <h4>Motorisation:</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>Cylindrée :</td>
                                <td><?php echo $annonces->cylindrees; ?></td>
                            </tr>
                            <tr>
                                <td>Puissance en cv :</td>
                                <td><?php echo Display::formatChevaux($annonces->puissancechevaux); ?></td>
                            </tr>
                            <tr>
                                <td>Transmission :</td>
                                <td><?php echo $annonces->transmission; ?></td>
                            </tr>
                            <tr>
                                <td>Roues motrices :</td>
                                <td><?php echo ($annonces->rouesmotrices != '') ? $annonces->rouesmotrices : 'N.D'; ?></td>
							</tr>
							
							<?php if($annonces->transmission != 'automatique'){ ?>
                            <tr>
                                <td>Nb de vitesse :</td>
                                <td><?php echo $annonces->vitesses; ?></td>
							</tr>
							<?php } ?>
							
                            <tr>
                                <td>Carburant :</td>
                                <td><?php echo $annonces->carburant; ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>Carrosserie :</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>Type :</td>
                                <td><?php echo $annonces->carrosserie; ?></td>
                            </tr>
                            <tr>
                                <td>Couleur :</td>
                                <td>
									<?php echo $annonces->couleurexterne; ?>
									<?php echo $annonces->metallisee ? sprintf(' %s', $LANG[154]) : ''; ?>
								</td>
                            </tr>
                            <tr>
                                <td>Nb de portes :</td>
                                <td><?php echo $annonces->portes; ?></td>
                            </tr>
                            <tr>
                                <td>Nb de place :</td>
                                <td><?php echo ($annonces->sieges == 0) ? "N.D" : $annonces->sieges; ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>Annonceur :</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>Lieu :</td>
                                <td><?php echo $annonceur->code_postal . " " . $annonceur->ville; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div id="photoVendeur" class="col-sm-6" style="visibility: hidden;">
                    <!--  Lightgallery -->
                    <div class="carSlider">
                        <div class="prixannonce"><?PHP echo number_format($annonces->prix, 0, ",", "'") . ".-"; ?></div>

                        <ul id="imageGallery">
                            <?php
                            $imagei = 1;
                            $imagetotal = 1;
                            $object = "image" . $imagetotal;
                            $image = $annonces->$object;
                            while (isset($image)) {
                                if ($image != "")
                                    $imagetotal++;
                                $imagei++;
                                $object = "image" . $imagei;
                                if (isset($annonces->$object))
                                    $image = $annonces->$object;
                                else
                                    unset($image);
                            }
                            $imagetotal = $imagetotal - 1;
                            $imagei = 1;
                            $imagecompteur = 1;
                            $object = "image" . $imagei;
                            $image = $annonces->$object;
                            while (isset($image)) {
                                if ($image != "") {
                                    $lienannonce = "annonce.php?id=" . $annonces->id;
                                    ?>
                                    <li data-thumb="img/thumbs/<?PHP echo $image; ?>" data-src="img/<?PHP echo $image; ?>">
                                    <?PHP if ($imagei == 1) { ?><a href="<?PHP echo $lienannonce; ?>" class="spothide"><img src="img/<?PHP echo $image; ?>" /></a><?PHP } ?>
                                        <img src="img/<?PHP echo $image; ?>" <?PHP if ($imagei == 1) { ?>class="spotshow"<?PHP } ?>/>
                                        <span class="imgNum nimage"><p><?PHP echo $imagecompteur . " / " . $imagetotal; ?></p></span>
                                    </li>
                                    <?PHP
                                    $imagecompteur++;
                                }
                                $imagei++;
                                $object = "image" . $imagei;
                                if (isset($annonces->$object))
                                    $image = $annonces->$object;
                                else
                                    unset($image);
                            }
                            ?>
                        </ul>

                    </div>
                    <!-- /Lightgallery -->
                    <script type="text/javascript">
                        var j = jQuery.noConflict();
                        j(document).ready(function () {
                            j('#imageGallery').lightSlider({
                                gallery: true,
                                item: 1,
                                loop: true,
                                thumbItem: 10,
                                slideMargin: 0,
                                enableDrag: false,
                                currentPagerPosition: 'left',
                                onSliderLoad: function (el) {
																	$("#photoVendeur").css("opacity", "1");
                                    el.lightGallery({
                                        download: false,
                                        selector: '#imageGallery .lslide'
                                    });
                                }
                            });
                            j('#photoVendeur').css({'visibility': 'visible'});
                        });
                    </script>
                </div>
            </div>

            <div class="col-sm-12">
                <hr class="blue">
                <div class="col-sm-6" style="margin-bottom:20px;">
                    <h4>Description rapide</h4>
					<p><?PHP 
					if($annonces->desc1 != '') echo $annonces->desc1;
					else echo 'Aucune information supplémentaire disponible';
					?></p>
				</div>
				

                <div class="col-sm-6" style="margin-bottom:20px;">
                    <h4>Description détaillée</h4>
					<p><?PHP 
					if($annonces->desc2 != '') echo $annonces->desc2;
					else echo 'Aucune information supplémentaire disponible';
					?></p>
                </div>
            </div>

            <div class="col-sm-12">

                <!-- ACCORDEONS -->
                <div class="panel-group" id="accordion">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" data-target="#collEquipements" class="black collapsed">
                                <h4 class="panel-title">Les équipements du véhicule
                                    <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span></h4></a>
                        </div>
                    </div>

                    <!-- EQUIPEMENTS -->
                    <div id="collEquipements" class="panel-collapse collapse">
                        <!-- Sécurité -->
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="securite">Sécurité :</label>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="airbag" type="checkbox" value="oui" name="airbag" <?PHP if (preg_match("/Airbag conducteur et passager/", $annonces->equipement)) echo "checked"; ?> disabled><label for="airbag">Airbag conducteur et passager</label><br/>
                                    <input id="airbagL" type="checkbox" value="oui" name="airbagL" <?PHP if (preg_match("/Airbag latéraux pour conducteur et passager/", $annonces->equipement)) echo "checked"; ?> disabled><label for="airbagL">Airbag latéraux pour conducteur et passager</label><br/>
                                    <input id="isofix" type="checkbox" value="oui" name="isofix" <?PHP if (preg_match("/Fixation isofix pour siège enfant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="isofix">Fixation isofix pour siège enfant</label><br/>
                                    <input id="feuxAv" type="checkbox" value="oui" name="feuxAv" <?PHP if (preg_match("/Feux anti-brouillard avant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="feuxAv">Feux anti-brouillard avant</label><br/>
                                    <input id="feuxAr" type="checkbox" value="oui" name="feuxAr" <?PHP if (preg_match("/Feux anti-brouillard arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="feuxAr">Feux anti-brouillard arrière</label><br/>
                                    <input id="phareDir" type="checkbox" value="oui" name="phareDir" <?PHP if (preg_match("/Phares directionnels/", $annonces->equipement)) echo "checked"; ?> disabled><label for="phareDir">Phares directionnels</label><br/>
                                    <input id="xenon" type="checkbox" value="oui" name="xenon" <?PHP if (preg_match("/Éclairage Xénon/", $annonces->equipement)) echo "checked"; ?> disabled><label for="xenon">Éclairage Xénon</label>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="verrCent" type="checkbox" value="oui" name="verrCent" <?PHP if (preg_match("/Verrouillage centralisé/", $annonces->equipement)) echo "checked"; ?> disabled><label for="verrCent">Verrouillage centralisé</label><br/>
                                    <input id="verrCentDist" type="checkbox" value="oui" name="verrCentDist" <?PHP if (preg_match("/Verrouillage centralisé à distance/", $annonces->equipement)) echo "checked"; ?> disabled><label for="verrCentDist">Verrouillage centralisé à distance</label><br/>
                                    <input id="x3feustop" type="checkbox" value="oui" name="x3feustop" <?PHP if (preg_match("/3ème feu de stop/", $annonces->equipement)) echo "checked"; ?> disabled><label for="x3feustop">3ème feu de stop</label><br/>
                                    <input id="ledRetro" type="checkbox" value="oui" name="ledRetro" <?PHP if (preg_match("/Led clignotant placé dans les rétroviseurs extérieurs/", $annonces->equipement)) echo "checked"; ?> disabled><label for="ledRetro">Led clignotant placé dans les rétroviseurs extérieurs</label><br/>
                                    <input id="abs" type="checkbox" value="oui" name="abs" <?PHP if (preg_match("/ABS \(système anti-blocage des roues\)/", $annonces->equipement)) echo "checked"; ?> disabled><label for="abs">ABS (système anti-blocage des roues)</label><br/>
                                    <input id="asr" type="checkbox" value="oui" name="asr" <?PHP if (preg_match("/ASR \(anti-patinage des roues\)/", $annonces->equipement)) echo "checked"; ?> disabled><label for="asr">ASR (anti-patinage des roues)</label><br/>
                                    <input id="esp" type="checkbox" value="oui" name="esp" <?PHP if (preg_match("/ESP \(contrôle de stabilité\)/", $annonces->equipement)) echo "checked"; ?> disabled><label for="esp">ESP (contrôle de stabilité)</label>
                                </div>
                            </div></div>

                        <!-- Confort -->
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="confort">Confort :</label>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="dirAss" type="checkbox" value="oui" name="dirAss" <?PHP if (preg_match("/Direction assistée/", $annonces->equipement)) echo "checked"; ?> disabled><label for="dirAss">Direction assistée</label><br/>
                                    <input id="clim" type="checkbox" value="oui" name="clim" <?PHP if (preg_match("/Climatisation/", $annonces->equipement)) echo "checked"; ?> disabled><label for="clim">Climatisation</label><br/>
                                    <input id="climAuto" type="checkbox" value="oui" name="climAuto" <?PHP if (preg_match("/Climatisation automatique/", $annonces->equipement)) echo "checked"; ?> disabled><label for="climAuto">Climatisation automatique</label><br/>
                                    <input id="vitreTeinte" type="checkbox" value="oui" name="vitreTeinte" <?PHP if (preg_match("/Vitres teintées/", $annonces->equipement)) echo "checked"; ?> disabled><label for="vitreTeinte">Vitres teintées</label><br/>
                                    <input id="vitreElec" type="checkbox" value="oui" name="vitreElec" <?PHP if (preg_match("/Vitres avant et arrière électriques/", $annonces->equipement)) echo "checked"; ?> disabled><label for="vitreElec">Vitres avant et arrière électriques</label><br/>
                                    <input id="volantCuir" type="checkbox" value="oui" name="volantCuir" <?PHP if (preg_match("/Volant en cuir/", $annonces->equipement)) echo "checked"; ?> disabled><label for="volantCuir">Volant en cuir</label><br/>
                                    <input id="volantChauf" type="checkbox" value="oui" name="volantChauf" <?PHP if (preg_match("/Volant chauffant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="volantChauf">Volant chauffant</label><br/>
                                    <input id="siegeCuir" type="checkbox" value="oui" name="siegeCuir" <?PHP if (preg_match("/Sièges en cuir/", $annonces->equipement)) echo "checked"; ?> disabled><label for="siegeCuir">Sièges en cuir</label><br/>
                                    <input id="siegeChauf" type="checkbox" value="oui" name="siegeChauf" <?PHP if (preg_match("/Sièges avant chauffant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="siegeChauf">Sièges avant chauffant</label>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="detecteurPluie" type="checkbox" value="oui" name="detecteurPluie" <?PHP if (preg_match("/Détecteur de luminosité et de pluie/", $annonces->equipement)) echo "checked"; ?> disabled><label for="detecteurPluie">Détecteur de luminosité et de pluie</label><br/>
                                    <input id="essuieInter" type="checkbox" value="oui" name="essuieInter" <?PHP if (preg_match("/Essuie-glaces avant avec intervalle/", $annonces->equipement)) echo "checked"; ?> disabled><label for="essuieInter">Essuie-glaces avant avec intervalle</label><br/>
                                    <input id="regVitesse" type="checkbox" value="oui" name="regVitesse" <?PHP if (preg_match("/Régulateur de vitesse/", $annonces->equipement)) echo "checked"; ?> disabled><label for="regVitesse">Régulateur de vitesse</label><br/>
                                    <input id="startstop" type="checkbox" value="oui" name="startstop" <?PHP if (preg_match("/Fonction Start\/Stop/", $annonces->equipement)) echo "checked"; ?> disabled><label for="startstop">Fonction Start/Stop</label><br/>
                                    <input id="retroElec" type="checkbox" value="oui" name="retroElec" <?PHP if (preg_match("/Rétroviseurs extérieurs à dégivrage et réglage électrique/", $annonces->equipement)) echo "checked"; ?> disabled><label for="retroElec">Rétroviseurs extérieurs à dégivrage et réglage électrique</label><br/>
                                    <input id="parcageAr" type="checkbox" value="oui" name="parcageAr" <?PHP if (preg_match("/Capteur de distance de parcage arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="parcageAr">Capteur de distance de parcage arrière</label><br/>
                                    <input id="parcageAvAr" type="checkbox" value="oui" name="parcageAvAr" <?PHP if (preg_match("/Capteur de distance de parcage avant et arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="parcageAvAr">Capteur de distance de parcage avant et arrière</label><br/>
                                    <input id="cameraRecul" type="checkbox" value="oui" name="cameraRecul" <?PHP if (preg_match("/Caméra de recul/", $annonces->equipement)) echo "checked"; ?> disabled><label for="cameraRecul">Caméra de recul</label><br/>
                                    <input id="barreToit" type="checkbox" value="oui" name="barreToit" <?PHP if (preg_match("/Barres de toit/", $annonces->equipement)) echo "checked"; ?> disabled><label for="barreToit">Barres de toit</label>
                                </div>
                            </div></div>

                        <!-- Divertissements -->
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="divertissements">Divertissements :</label>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="ordinateur" type="checkbox" value="oui" name="ordinateur" <?PHP if (preg_match("/Ordinateur de bord/", $annonces->equipement)) echo "checked"; ?> disabled><label for="ordinateur">Ordinateur de bord</label><br/>
                                    <input id="gps" type="checkbox" value="oui" name="gps" <?PHP if (preg_match("/Système de navigation GPS/", $annonces->equipement)) echo "checked"; ?> disabled><label for="gps">Système de navigation GPS</label><br/>
                                    <input id="radio" type="checkbox" value="oui" name="radio" <?PHP if (preg_match("/Radio \/ CD/", $annonces->equipement)) echo "checked"; ?> disabled><label for="radio">Radio / CD</label><br/>
                                    <input id="usb" type="checkbox" value="oui" name="usb" <?PHP if (preg_match("/Connexion USB et AUX/", $annonces->equipement)) echo "checked"; ?> disabled><label for="usb">Connexion USB et AUX</label>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="volantMultiF" type="checkbox" value="oui" name="volantMultiF" <?PHP if (preg_match("/Volant multi-fonctions/", $annonces->equipement)) echo "checked"; ?> disabled><label for="volantMultiF">Volant multi-fonctions</label><br/>
                                    <input id="bluetooth" type="checkbox" value="oui" name="bluetooth" <?PHP if (preg_match("/Interface Bluetooth et téléphone mobile/", $annonces->equipement)) echo "checked"; ?> disabled><label for="bluetooth">Interface Bluetooth et téléphone mobile</label><br/>
                                    <input id="toitPano" type="checkbox" value="oui" name="toitPano" <?PHP if (preg_match("/Toit panoramique/", $annonces->equipement)) echo "checked"; ?> disabled><label for="toitPano">Toit panoramique</label><br/>
                                    <input id="toitOuvrant" type="checkbox" value="oui" name="toitOuvrant" <?PHP if (preg_match("/Toit ouvrant/", $annonces->equipement)) echo "checked"; ?> disabled><label for="toitOuvrant">Toit ouvrant</label>
                                </div>
                            </div></div>

                        <!-- Équipements sport -->
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="sport">Équipements sport :</label>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="spoilerToit" type="checkbox" value="oui" name="spoilerToit" <?PHP if (preg_match("/Spoiler de toit/", $annonces->equipement)) echo "checked"; ?> disabled><label for="spoilerToit">Spoiler de toit</label><br/>
                                    <input id="jupes" type="checkbox" value="oui" name="jupes" <?PHP if (preg_match("/Jupes latérales/", $annonces->equipement)) echo "checked"; ?> disabled><label for="jupes">Jupes latérales</label><br/>
                                    <input id="chassisSport" type="checkbox" value="oui" name="chassisSport" <?PHP if (preg_match("/Châssis sportif/", $annonces->equipement)) echo "checked"; ?> disabled><label for="chassisSport">Châssis sportif</label>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="siegeSport" type="checkbox" value="oui" name="siegeSport" <?PHP if (preg_match("/Sièges sport/", $annonces->equipement)) echo "checked"; ?> disabled><label for="siegeSport">Sièges sport</label><br/>
                                    <input id="retroChrome" type="checkbox" value="oui" name="retroChrome" <?PHP if (preg_match("/Rétrovieurs extérieurs chromés/", $annonces->equipement)) echo "checked"; ?> disabled><label for="retroChrome">Rétrovieurs extérieurs chromés</label><br/>
                                    <input id="barreToitAlu" type="checkbox" value="oui" name="barreToitAlu" <?PHP if (preg_match("/Barrres de toit en aluminium/", $annonces->equipement)) echo "checked"; ?> disabled><label for="barreToitAlu">Barrres de toit en aluminium</label>
                                </div>
                            </div></div>

                        <!-- Autres -->
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="autres">Autres :</label>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="roueSecours" type="checkbox" value="oui" name="roueSecours" <?PHP if (preg_match("/Roue de secours/", $annonces->equipement)) echo "checked"; ?> disabled><label for="roueSecours">Roue de secours</label><br/>
                                    <input id="x2PneusRoues" type="checkbox" value="oui" name="x2PneusRoues" <?PHP if (preg_match("/2 jeux de pneus\/roues/", $annonces->equipement)) echo "checked"; ?> disabled><label for="x2PneusRoues">2 jeux de pneus/roues</label><br/>
                                    <input id="antiCrevaison" type="checkbox" value="oui" name="antiCrevaison" <?PHP if (preg_match("/Kit anti-crevaison/", $annonces->equipement)) echo "checked"; ?> disabled><label for="antiCrevaison">Kit anti-crevaison</label><br/>
                                    <input id="hardTop" type="checkbox" value="oui" name="hardTop" <?PHP if (preg_match("/Hard top/", $annonces->equipement)) echo "checked"; ?> disabled><label for="hardTop">Hard top</label>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="checkbox checkbox-primary">
                                    <input id="tapisCaoutchouc" type="checkbox" value="oui" name="tapisCaoutchouc" <?PHP if (preg_match("/Tapis en caoutchouc avant et arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="tapisCaoutchouc">Tapis en caoutchouc avant et arrière</label><br/>
                                    <input id="tapisRevetus" type="checkbox" value="oui" name="tapisRevetus" <?PHP if (preg_match("/Tapis revêtus avant et arrière/", $annonces->equipement)) echo "checked"; ?> disabled><label for="tapisRevetus">Tapis revêtus avant et arrière</label><br/>
                                    <input id="crochetAttelage" type="checkbox" value="oui" name="crochetAttelage" <?PHP if (preg_match("/Crochet d'attelage/", $annonces->equipement)) echo "checked"; ?> disabled><label for="crochetAttelage">Crochet d'attelage</label><br/>
                                    <input id="grille" type="checkbox" value="oui" name="grille" <?PHP if (preg_match("/Grille de séparation/", $annonces->equipement)) echo "checked"; ?> disabled><label for="grille">Grille de séparation</label>
                                </div>
                            </div></div>
                    </div>
                    <!-- /EQUIPEMENTS -->

                    <div class="clear"></div>

                    <!-- Informations complémentaires -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" data-target="#collInfoComp" class="black collapsed">
                                <h4 class="panel-title">Informations complémentaires
                                    <span class="rightAlign">[<i class="fa fa-plus">Étendre</i><i class="fa fa-minus">Fermer</i>]</span></h4></a>
                        </div>
                    </div>

                    <div id="collInfoComp" class="panel-collapse collapse">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="complementaires">Informations complémentaires :</label>

                            <div class="col-sm-3">
                                <div class="checkbox checkbox-primary">
                                    <input id="importDirectPara" type="checkbox" value="oui" name="importDirectPara" <?PHP if (preg_match("/Importation directe\/parallèle/", $annonces->equipement)) echo "checked"; ?> disabled><label for="importDirectPara">Importation directe/parallèle</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="handicape" type="checkbox" value="oui" name="handicape" <?PHP if (preg_match("/Pour handicapé/", $annonces->equipement)) echo "checked"; ?> disabled><label for="handicape">Pour handicapé</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="tuning" type="checkbox" value="oui" name="tuning" <?PHP if (preg_match("/Tuning/", $annonces->equipement)) echo "checked"; ?> disabled><label for="tuning">Tuning</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox checkbox-primary">
                                    <input id="voitureCourse" type="checkbox" value="oui" name="voitureCourse" <?PHP if (preg_match("/Voiture de course/", $annonces->equipement)) echo "checked"; ?> disabled><label for="voitureCourse">Voiture de course</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="checkbox checkbox-primary">
                                    <input id="nonFumeur" type="checkbox" value="oui" name="nonFumeur" <?PHP if (preg_match("/Véhicule non-fumeur/", $annonces->equipement)) echo "checked"; ?> disabled><label for="nonFumeur">Véhicule non fumeur</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Informations complémentaires -->

                </div>
                <!-- END ACCORDEONS -->

            </div>


            <div class="clear"></div>
            <button class="btn btn-primary nextBtn btn-lg pull-right toTop <?php echo (client::isLoggedIn() ? 'skip-1' : ''); ?>" type="button">Continuer</button>
            <div class="clear"></div>
        </div>
        <!-- FIN ÉTAPE 1 -->

        <!-- ÉTAPE 2 : CONTRÔLEZ VOS COORDONÉES -->
        <div class="setup-content" id="step-2"<?php echo $step2style; ?>>
            <?php if ($client !== null) : ?>
                <!-- 2A -->
                <h3 class="blue">Contrôlez vos coordonnées</h3>
                <div class="col-md-12">
                    <p class="larger">Pour poursuivre votre demande, il vous suffit de contrôler vos coordonnées ci-après :</p>
                    <!-- CONTROL COORD FORM -->
                    <form id="coordForm" class="form-horizontal" role="form" data-toggle="validator">
                        <div class="form-group">
                            <label class="control-label col-xs-6" for="coordNom">Nom : <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordNom" placeholder="Votre nom" value="<?PHP echo $client->nom; ?>" required>
                            </label>

                            <label class="control-label col-xs-6" for="coordPrenom">Prénom : <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordPrenom" placeholder="Votre prénom" value="<?PHP echo $client->prenom; ?>" required>
                            </label>

                            <label class="control-label col-xs-9" for="coordAdresse">Adresse complète :  <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordAdresse" placeholder="Votre adresse" value="<?PHP echo $client->adresse; ?>" required>
                            </label>

                            <label class="control-label col-xs-3" for="coordNumero">N° :  <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordNumero" placeholder="N° de rue, voie etc." value="<?PHP echo $client->adresse_num; ?>" required>
                            </label>

                            <label class="control-label col-xs-3" for="coordCP">Code postal :  <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordCP" placeholder="Votre code postal" value="<?PHP echo $client->code_postal; ?>" required>
                            </label>

                            <label class="control-label col-xs-9" for="coordVille">Ville :  <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordVille" placeholder="Votre ville" value="<?PHP echo $client->ville; ?>" required>
                            </label>

                            <label class="control-label col-xs-12" for="coordTel">N° de téléphone :  <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordTel" placeholder="Votre n° de téléphone" value="<?PHP echo $client->telephone; ?>" required>
                            </label>

                            <label class="control-label col-xs-12" for="coordMail">Email :  <span class="requis">*</span>
                                <input type="text" class="form-control" id="coordMail" placeholder="Votre email" value="<?PHP echo $client->email; ?>" required>
                            </label>

                        </div>
                    </form>
                    <!-- END CONTROL COORD FORM -->
                </div>
            <?php else : ?>
                <!-- 2B  -->
                <h3 class="blue">Connectez-vous à votre compte ou créez-en un gratuitement</h3>
                <div class="col-md-12">
                    <p class="larger">Pour poursuivre votre demande, il vous suffit de vous connecter à votre compte utilisateur ou, si vous n'en possedez pas encore, d'en créer un gratuitement :</p>
                    <!-- SIGN IN -->
                    <div class="col-sm-6" id="signin">
                        <?php
                        $msg = new \Plasticbrain\FlashMessages\FlashMessages();
                        if ($msg->hasErrors()) {
                            $msg->display();
                        }
                        ?>
                        <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> S'IDENTIFIER</h4>
                        <hr class="blue">
                        <div id="connexion">

                            <!-- formulaire de login -->
                            <form class="form-horizontal" role="form" data-toggle="validator" action="connexion.php?referer=<?php echo urlencode('je-choisis-mon-forfait.php?id='.$id.'&step2'); ?>" method="post">
                                <input type="hidden" name="action" value="login" />
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="sipseudo">Email : <span class="requis">*</span></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="sipseudo" name="sipseudo" placeholder="Votre email" required>
                                    </div>
                                </div>
                                <div class="clear"></div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="sipwd">Mot de passe : <span class="requis">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="sipwd" name="sipwd" placeholder="Entrez votre mot de passe" required>
                                    </div>
                                </div>
                                <div class="clear"></div>

                                <div class="col-sm-offset-3 col-sm-4 checkbox checkbox-primary">
                                    <input id="checksouvenir" type="checkbox" value="oui" name="souvenir"> <label for="checksouvenir">Se souvenir de moi</label>
                                </div>
                                <div class="col-sm-4 checkbox checkbox-primary">
                                    <a href="/mot_de_passe_oublie.php">Mot de passe oublié</a>
                                </div>
                                <div class="clear"></div>

                                <div class="form-group">
                                    <label class="col-sm-3"></label>
                                    <div class="col-sm-8">
                                        <button type="submit" name="seconnecter" class="btn btn-register noMargin mk_form_right1">Se connecter</button>
                                    </div>
                                </div>

                            </form>
                            <!-- fin du formulaire de login -->
                        </div>
                    </div>
                    <!-- END SIGN IN -->

                    <!-- SIGN UP -->
                    <div class="col-sm-6" id="signup">
                        <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i> S'INSCRIRE</h4>
                        <div id="introsignup" class="collapse in">
                            <hr class="blue">
                            <p><i>Vous n’avez pas encore de compte utilisateur ? Inscrivez-vous rapidement et profitez des avantages suivants :</i>
                            <ul>
                                <li>Une plateforme innovante permettant de mandater une inspection technique de 60 minutes sur la voiture que vous souhaitez acheter (forfait ScanExpert)</li>
                                <li>Un réseau de garages partenaires réparti dans toute la suisse romande.</li>
                                <li>Profitez, grâce au forfait ScanExpert Protect, d'une sécurité supplémentaire avec la garantie mécanique (à venir).</li>
                                <li>Insérez votre annonce et recevez gratuitement notre bannière auto (en savoir +). Pour les 250 premières insertions.</li>
                            </ul>
                            </p>
                        </div>
                        <hr class="blue">

                        <div class="col-sm-offset-4 col-sm-8">
                            <button data-toggle="collapse" data-target="#introsignup, #inscription" class="btn btn-register noMargin">Inscription gratuite</button>
                        </div>

                        <div id="inscription" class="collapse">
                            <div class="clear"></div>
                            <!-- formulaire d'inscription -->
                            <form id="signupform" class="form-horizontal" role="form" data-toggle="validator" action="connexion.php?referer=<?php echo urlencode('je-choisis-mon-forfait.php?id='.$id.'&step2'); ?>&ref=choisir-forfait&step=3&annonce=<?= $_GET['id'] ;?>" method="post">
                                <input type="hidden" name="action" value="signup" />
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="sunom">Nom : <span class="requis">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="sunom" name="sunom" placeholder="Votre nom" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="suprenom">Prénom : <span class="requis">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="suprenom" name="suprenom" placeholder="Votre prénom" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="suemail">Email : <span class="requis">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" id="suemail" name="suemail" placeholder="Votre email" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="adresse">Adresse & N° :  <span class="requis">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Votre adresse" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="numero" name="numero" placeholder="N°" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="codePostal">Code postal & Ville :  <span class="requis">*</span></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="codePostal" name="codePostal" placeholder="Votre NPA" required>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="ville" name="ville" placeholder="Votre ville" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="supwd">Mot de passe : <span class="requis">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="supwd" name="supwd" placeholder="Choisissez un mot de passe" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="confirmpwd">Confirmez mot de passe : <span class="requis">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="confirmpwd" name="confirmpwd" placeholder="Confirmez votre mot de passe" required>
                                    </div>
                                </div>
																<!--
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="avatar">Choisir un avatar :</label>
                                    <div class="col-sm-8">
                                        Plugin à insérer (je laisse le choix le plus judicieux à faire à Guillaume exemple ici : <a href="http://plugins.krajee.com/file-avatar-upload-demo">http://plugins.krajee.com/file-avatar-upload-demo</a>)
                                    </div>
                                </div>
																-->
																<div class="form-group">
																		<label class="control-label col-sm-4" for="telephone">Téléphone : <span class="requis">*</span></label>
																		<div class="col-sm-8">
																				<input name="telephone" type="text" class="form-control" id="telephone" placeholder="Ex. 079/123.45.67" required pattern="^0.+" title="Le numéro doit commnecer par un 0">
																		</div>
																</div>
																<div class="clear"></div>
																<input type="hidden" name="type_client" value="0" />
																
																
																<div class="form-group">
																<label class="control-label col-sm-4"></label>
																<div class="col-sm-8"> 
																<div class="g-recaptcha" data-sitekey="6LfUhlUUAAAAADFtz6TSa-0HVxBQYmxATuHJLlNt"></div>
																</div>
																</div>
																<div class="clear"></div>
																
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-register noMargin">S'inscrire</button>
                                    </div>
                                </div>
                            </form>
                            <!-- fin du formulaire d'inscription -->
                        </div>
                    </div>
                    <!-- END SIGN UP -->
                </div>
            <?php endif; ?>

            <div class="clear"></div>
            <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>
            <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
            <div class="clear"></div>
            <!-- FIN 2B  -->

        </div>
        <!-- FIN ÉTAPE 2 -->

        <!-- ÉTAPE 3 : CHOISISSEZ VOTRE FORFAIT -->
        <div class="setup-content" id="step-3"<?php echo $step3style; ?> data-page='choisis-forfait'>
            <?php if ($client->confirmed_email): ?>

                <h3 class="blue">Choisissez votre forfait</h3>
                <div class="col-md-12">
                    <form id="forfaitForm" class="form-horizontal" role="form" data-toggle="validator">
                        <!-- ScanExpert -->
                        <div id="visio" class="col-md-6 carteForfait controlForfait">
                            <div class="carteHead visio">
                                <p>ScanExpert</p>
                            </div>
                            <div class="carteBody">
                                <p>
                                                            Suivant un cahier des charges précis, nos garages partenaires effectuent, pour le forfait ScanExpert, 82 points de contrôle structurés en 9 catégories :
                                                                        
                    <!-- update -->
                                </p><br />
                                <?php include('scanexpert_controles.php'); ?>
                            </div>
                            <div class="carteFoot visio">
                                <span class="radioForfait">
                                    <input id="forfaitVisio" type="radio" name="forfait" onclick="if (this.checked) {
                                                visio()
                                            }" required>&nbsp;<label class="control-label" for="forfaitVisio">Choisir ce forfait</label> <span class="requis" required>*</span></span><br/>
                                <span class="prixChoixForfait">199.-</span>
                            </div>
                        </div>
                        <!-- /VISIOEXPERT -->

                        <!-- SCAN EXPERT -->
                        <!--<div id="scan" class="col-md-6 carteForfait controlForfait">
                            <div class="carteHead scan">
                                <p>ScanExpert <span class="protect_mk3">Protect</span></p>
                            </div>
                            <div class="carteBody">
                                <p>Forfait ScanExpert + garantie mécanique (à venir)</p><br />
                                <?php 
                                /*$occurence = 2;
                                $collapse = true;
                                $subtitle = true;
                                include('scanexpert_controles.php');*/
                                ?>
                            </div>
                            <div class="carteFoot scan">
                                <span class="radioForfait"><input id="forfaitScan" type="radio" name="forfait" onclick="if (this.checked) {
                                            scan()
                                        }" required>&nbsp;<label class="control-label" for="forfaitScan">Choisir ce forfait</label> <span class="requis" required>*</span></span>
                                <br/>
                                <span class="prixChoixForfait">PRIX</span>
                            </div>
                        </div>


                        <!-- /SCANEXPERT -->

                        <!-- Script pour mettre en arrière le forfait non choisi -->
                        <!--<script>
                            function visio() {
                                document.getElementById("forfaitVisio").value =
                                        $("#visio").animate({
                                    opacity: 1,
                                }, 500, function () {
                                    // Animation complete.
                                });
                                $("#scan").animate({
                                    opacity: 0.5,
                                }, 500, function () {
                                    // Animation complete.
                                });
                            }
                            function scan() {
                                document.getElementById("forfaitScan").value =
                                        $("#scan").animate({
                                    opacity: 1,
                                }, 500, function () {
                                    // Animation complete.
                                });
                                $("#visio").animate({
                                    opacity: 0.5,
                                }, 500, function () {
                                    // Animation complete.
                                });
                            }
                        </script>-->

                        <div class="clear"></div>

                        <div class="col-sm-12 checkbox checkbox-primary">
                            <input id="presentControle" type="checkbox" value="oui" name="presentControle">
                            <label for="presentControle" class="notReqBlack">Je souhaite être présent lors du contrôle technique</label>
                        </div>
                        <script type="text/javascript">
                            jQuery(document).ready(function ($) {
                                $('#presentControle').change(function () {
                                    if (this.checked) {
                                        $(".showPresent").show();
                                        $('#prepayment input[name="present"]').val(1);
                                    }
                                    else {
                                        $(".showPresent").hide();
                                        $('#prepayment input[name="present"]').val(0);
                                    }
                                });
                            });
                        </script>
                        <p class="larger blue showPresent" style="display:none;">Vous serez averti par email et dans votre espace client dès que la date du contrôle technique sera connue.</p>
                    </form>
                </div>

                <div class="clear"></div>
                <button class="btn btn-default prevBtn toTop <?php echo (client::isLoggedIn() ? 'skip-1' : ''); ?>" type="button">Précédent</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right toTop" type="button">Continuer</button>
                <div class="clear"></div>

            <?php else: ?>
                <div class="container">
                    <h3 class="center">Confirmez votre inscription</h3>

                    <div class="container">
                        <p>Pour des raisons de sécurité, veuillez confirmer votre inscription avant de pouvoir effectuer une demande d’inspection technique.</p>
                        <p>Un email de confirmation a été envoyé à l'adresse email <strong><?php echo $client->email; ?></strong>. Il vous suffit de suivre les instructions contenues dans cet email. Si vous ne le recevez pas, pensez à contrôler dans votre boite spam.</p>
                        <p>Une fois votre email validé, vous pourrez passer à l'étape 3.</p>
                        <p>Si l'adresse email est incorrecte, veuillez cliquer sur le bouton ci-contre <a class="btn btn-inline btn-sm btn-default" data-toggle="modal" data-target="#modalEmailUpdate" style="padding: 5px 10px; font-size: 12px; line-height: 1.5; border-radius: 3px;">Corriger l'adresse email</a></p>
                        <p>Si dans les 15 prochaines minutes vous n’avez pas reçu d’email de confirmation, veuillez cliquer sur le bouton ci-contre <a class="btn btn-inline btn-sm btn-default" href="inscription_confirmation.php?referer=choisir-forfait&step=3&annonce=<?= $id ;?>&action=resend" style="padding: 5px 10px; font-size: 12px; line-height: 1.5; border-radius: 3px;">Renvoyer l'email de confirmation</a></p>
                    </div>

                    <div class="clear"></div>
                    <button class="btn btn-default prevBtn toTop skip-1" type="button">Précédent</button>
                    <div class="clear"></div>
                </div>

                <div id="modalEmailUpdate" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Corriger mon adresse email</h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" role="form" action="inscription_confirmation.php?referer=choisir-forfait&step=3&annonce=<?= $id ;?>&action=update" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"
                                                for="formemail">Adresse email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control"
                                                   id="formemail" name="formemail" placeholder="Email"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-default">Enregistrer</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            <?php endif ?>
        </div>
        <!-- FIN ÉTAPE 3 -->

        <!-- ÉTAPE 4 : CHOISISSEZ VOTRE MOYEN DE PAIEMENT -->
        <div class="setup-content" id="step-4"<?php echo $step4style; ?>>

            <?php if ($client->confirmed_email): ?>
                <h3 class="blue">Moyen de paiement</h3>
                <?php if (isset($_GET['step4'])): ?>
                    <div class="col-md-12">
                        <p class="larger text-warning">Une erreur est survenue lors du paiement.</p>
                        <p class="larger text-warning">Veuillez réessayer en vérifiant vos informations de votre carte bancaire.</p>
                    </div>
                <?php endif; ?>
                <div class="col-md-12">
                    <p class="larger">Vous serez redirigé vers notre plateforme de paiement sécurisé Klikandpay avec les moyens de paiement disponibles suivants : Mastercard, Visa, Postfinance ou Postfinance Card.</p>
                    <p class="larger">Dès que le paiement est validé, il n’est plus possible d’annuler votre demande d’inspection technique.</p>
                </div>  

                <div class="clear"></div>
                <button class="btn btn-default prevBtn toTop" type="button">Précédent</button>

                <form method="post" id="prepayment" action="/je-choisis-mon-forfait-paiement.php">
                    <input type="hidden" name="id_client" value="<?php echo $client->id; ?>" />
                    <input type="hidden" name="id_annonce" value="<?php echo $annonces->id; ?>" /> 
                    <input type="hidden" name="type_forfait" value="ScanExpert" />
                    <input type="hidden" name="present" value="0" />
                    <button class="btn btn-primary btn-lg pull-right toTop" onclick="this.form.submit();">Continuer</button>
                </form>

                <div class="clear"></div>
            <?php else: ?>
                <?php if ($_GET['step4'] !== null): ?>
                    <script type="text/javascript"> window.location = 'je-choisis-mon-forfait.php?id=<?= $_GET['id'] ;?>&step3'; </script>
                <?php endif ?>
            <?php endif ?>
        </div>
        <!-- FIN ÉTAPE 4 -->

        <!-- ÉTAPE 5 : FÉLICITATIONS -->
        <div class="setup-content" id="step-5"<?php echo $step5style; ?>>
            <?php if ($client->confirmed_email): ?>
                <?php
    						
    						$id_client = $_SESSION["id_client"];
    						
                $sql = sprintf('SELECT * FROM demandes_exam WHERE id_annonces_clients="%s" and id_client_acheteur="%s";', mysqli_real_escape_string($connect1, $id), mysqli_real_escape_string($connect1, $id_client));
                $query = mysqli_query($connect1, $sql);
                $demande = mysqli_fetch_object($query);
                $client = client::getById($demande->id_client_acheteur);
                ?>
                <h3 class="blue">Félicitations, votre demande d'inspection technique N°<?php echo $demande->id; ?> a bien été prise en compte</h3>
                <div class="col-md-12">
                    <p class="larger">Nous vous avons envoyé un email de confirmation à l'adresse <?php echo $client->email; ?></p>
                    <p class="larger">Si vous avez choisi d'être présent lors de l'inspection technique, vous serez informé de la date, de l'heure et du lieu de rendez-vous par e-mail, SMS et dans votre espace utilisateur.</p>
                </div>

                <div class="clear"></div>
                <a href="/index.php?r=voiture&marque=Toutes&modele=Tous" role="button" class="btn btn-primary btn-lg pull-left white">Retourner à la page des annonces</a>
                <div class="clear"></div>
            <?php else: ?>
                <?php if ($_GET['step5'] !== null): ?>
                    <script type="text/javascript"> window.location = 'je-choisis-mon-forfait.php?id=<?= $_GET['id'] ;?>&step3'; </script>
                <?php endif ?>
            <?php endif ?>
        </div>
        <!-- FIN ÉTAPE 5 :  -->
</div>

<script type="text/javascript">
    jQuery('.scanexpert-controles-toggle').on('click', function(e) {
        $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle')
    });

    jQuery('.testRoutierControl').off('click');
    jQuery('.testRoutierControl').on('click', function(e) {
        $(this).toggleClass('fa-plus-circle fa-minus-circle')
    });

    jQuery('.reload').click(function(e) {
        e.preventDefault();
        window.location = 'je-choisis-mon-forfait.php?id=<?= $_GET['id'] ;?>&step3';
    });
</script>

<!-- FIN CONTAINER FORMULAIRE EN 5 étapes -->
<?php include("footer.php"); ?>