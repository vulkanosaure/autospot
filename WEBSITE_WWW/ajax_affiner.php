<?PHP
include("db_connexion.php");
if(isset($_POST["r"]))
{
    $marque = $_POST['marque'];
    $r = $_POST["r"];
    if($r == "Toutes")
    	$requetetr = mysqli_query($connect1, "SELECT modele FROM formulaires_recherche WHERE marque='$marque' ORDER BY modele ASC");
    else
    	$requetetr = mysqli_query($connect1, "SELECT modele FROM formulaires_recherche WHERE marque='$marque' && choix='$r' ORDER BY modele ASC");
    $modeleslist = "";
    while($modeles = mysqli_fetch_object($requetetr))
    {
    	if((!preg_match("/\|$modeles->modele\|/", $modeleslist)) && ($modeles->modele != ""))
    		$modeleslist = $modeleslist."|$modeles->modele|";
    }
    $modeleslist = preg_replace("/^\|/", "", $modeleslist);
    $modeleslist = preg_replace("/\|$/", "", $modeleslist);
    if($_POST['taille'] == 480)
    {
        ?>
        document.getElementById('modele480').options.length = 0; document.getElementById('modele480').options[document.getElementById('modele480').options.length] = new Option('Tous', 'Tous');
        document.getElementById('modelebis480').options.length = 0; document.getElementById('modelebis480').options[document.getElementById('modelebis480').options.length] = new Option('Tous', 'Tous');
        <?PHP
        $i=0;
        $explode = explode("||", $modeleslist);
        while(isset($explode[$i]))
        {
        	?>document.getElementById('modele480').options[document.getElementById('modele480').options.length] = new Option('<?PHP echo addslashes($explode[$i]);?>', '<?PHP echo addslashes($explode[$i]);?>');
        	document.getElementById('modelebis480').options[document.getElementById('modelebis480').options.length] = new Option('<?PHP echo addslashes($explode[$i]);?>', '<?PHP echo addslashes($explode[$i]);?>');<?PHP
        	$i++;
        }
    }
    else if($_POST['taille'] == 1280)
    {
        ?>
        document.getElementById('modele1280').options.length = 0; document.getElementById('modele1280').options[document.getElementById('modele1280').options.length] = new Option('Tous', 'Tous');
        document.getElementById('modelebis1280').options.length = 0; document.getElementById('modelebis1280').options[document.getElementById('modelebis1280').options.length] = new Option('Tous', 'Tous');
        <?PHP
        $i=0;
        $explode = explode("||", $modeleslist);
        while(isset($explode[$i]))
        {
        	?>document.getElementById('modele1280').options[document.getElementById('modele1280').options.length] = new Option('<?PHP echo addslashes($explode[$i]);?>', '<?PHP echo addslashes($explode[$i]);?>');
        	document.getElementById('modelebis1280').options[document.getElementById('modelebis1280').options.length] = new Option('<?PHP echo addslashes($explode[$i]);?>', '<?PHP echo addslashes($explode[$i]);?>');<?PHP
        	$i++;
        }
    }
    else if($_POST['taille'] == 1920)
    {
        ?>
        document.getElementById('modele1920').options.length = 0; document.getElementById('modele1920').options[document.getElementById('modele1920').options.length] = new Option('Tous', 'Tous');
        document.getElementById('modelebis1920').options.length = 0; document.getElementById('modelebis1920').options[document.getElementById('modelebis1920').options.length] = new Option('Tous', 'Tous');
        <?PHP
        $i=0;
        $explode = explode("||", $modeleslist);
        while(isset($explode[$i]))
        {
        	?>document.getElementById('modele1920').options[document.getElementById('modele1920').options.length] = new Option('<?PHP echo addslashes($explode[$i]);?>', '<?PHP echo addslashes($explode[$i]);?>');
        	document.getElementById('modelebis1920').options[document.getElementById('modelebis1920').options.length] = new Option('<?PHP echo addslashes($explode[$i]);?>', '<?PHP echo addslashes($explode[$i]);?>');<?PHP
        	$i++;
        }
    }
}
else
{
    $marque = $_POST['marque'];
    $requetetr = mysqli_query($connect1, "SELECT modele FROM formulaires_recherche WHERE marque='$marque' && choix='voiture' ORDER BY modele ASC");
    $modeleslist = "";
    while($modeles = mysqli_fetch_object($requetetr))
    {
    	if((!preg_match("/\|$modeles->modele\|/", $modeleslist)) && ($modeles->modele != ""))
    		$modeleslist = $modeleslist."|$modeles->modele|";
    }
    $modeleslist = preg_replace("/^\|/", "", $modeleslist);
    $modeleslist = preg_replace("/\|$/", "", $modeleslist);
    ?>
    document.getElementById('modele').options.length = 0; document.getElementById('modele').options[document.getElementById('modele').options.length] = new Option('', '');
    <?PHP
    $i=0;
    $explode = explode("||", $modeleslist);
    while(isset($explode[$i]))
    {
        ?>document.getElementById('modele').options[document.getElementById('modele').options.length] = new Option('<?PHP echo addslashes($explode[$i]);?>', '<?PHP echo addslashes($explode[$i]);?>');<?PHP
        $i++;
    }
}
?>