<?php
$pagestotal = ceil($row['numberOfRows'] / $nbresultperpage);
if ($pagestotal > 1)
{
	$pageactuelle = ($start / $nbresultperpage) + 1;
	$pagestart = $pageactuelle - 5;
	if($pagestart <= 0)
		$pagestart = 1;
	$pageend = $pagestart + 9;
}
else
    $pageactuelle = ($start / $nbresultperpage) + 1;
?>
<!-- PAGINATION -->

<?php if ($pagestotal > 1): ?>
	<div class="text-center">
		<img src="/images/logomini.png" alt="" />
	</div>
<?php endif; ?>

<div class="npages text-center">
	<ul class="pagination">
		<?php if ($pageactuelle > 1): ?>
			<li class="spot">
				<?php
				$pageprecedente = $pageactuelle - 1;
				$startreq = ($pageprecedente*$nbresultperpage)-$nbresultperpage;
				$query_string = preg_replace("/\&start=\d+/", "", $_SERVER["QUERY_STRING"]);
				?>
				<a href="<?php if($startreq != 0) echo "?$query_string&start=$startreq"; else echo "?$query_string";?>"><i class="fa fa-chevron-left"></i></a>
			</li>
		<?php endif; ?>
		<?php for ($i=$pagestart ; $i <= $pageend ; $i++): ?>
			<?php $startreq = ($i*$nbresultperpage)-$nbresultperpage; ?>
			<?php $query_string = preg_replace("/\&start=\d+/", "", $_SERVER["QUERY_STRING"]); ?>
			<li class="oo" style="<?php if($i == $pageactuelle) echo "font-weight:bold;color:#009ada;"?>">
				<a href="<?php if($startreq != 0) echo "?$query_string&start=$startreq"; else echo "?$query_string";?>">
					<?php echo $i; ?>
				</a>
			</li>
			<?php if($i == $pagestotal) break; ?>
		<?php endfor; ?>
		<?php if ($pageactuelle != $pagestotal): ?>
			<li class="spot">
				<?php			
				$pagesuivante = $pageactuelle + 1;
				$startreq = ($pagesuivante*$nbresultperpage)-$nbresultperpage;
				$query_string = preg_replace("/\&start=\d+/", "", $_SERVER["QUERY_STRING"]);
				?>
				<a href="<?php if($startreq != 0) echo "?$query_string&start=$startreq"; else echo "?$query_string";?>">
					<i class="fa fa-chevron-right"></i>
				</a>
			</li>
		<?php endif; ?>
	</ul>
</div>

</div>

<div class="clear"></div>

 <!-- FIN PAGINATION -->