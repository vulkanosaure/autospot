<?php

require_once('appbackend/admin/include/db_connect.php');
require_once('appbackend/events.php');
require_once('appbackend/include/time_utils.php');
require_once('classes/Display.php');

session_start();

function formatJsonSuivi($json) {
    $decoded = json_decode($json, true);
    $results = array();

    if (is_array($decoded) && count($decoded) > 0) {
        foreach ($decoded as $item) {
            $results[$item['key']] = array(
                'ts' => $item['ts'],
                'params' => $item['params'],
            );
        }
    }

    return $results;
}

if (!isset($_SESSION['id_client']))
{
    if (isset($_GET['annonces']))
        $referer = 'annonces';
    elseif(isset($_GET['demandes']))
        $referer = 'demandes';
    elseif(isset($_GET['favoris']))
        $referer = 'favoris';
    elseif(isset($_GET['profil']))
        $referer = 'profil';

    header('Location: inscription.php?referer='.urlencode('compte.php'.(isset($referer) ? '?'.$referer : '')));
    exit();
}

include("header.php");
include("body.php");

if (isset($_GET['supprimer'])) {
    $id_annonce = mysqli_real_escape_string($connect1, $_GET['supprimer']);
    $id_client = mysqli_real_escape_string($connect1, $_SESSION['id_client']);
    $requete_annonces = mysqli_query($connect1, "SELECT id, image1, image2, image3, image4, image5, image6 FROM annonces_clients WHERE id='$id_annonce' && id_client='$id_client'");
    if (mysqli_num_rows($requete_annonces) == 1) {
		
		
        $annonce = mysqli_fetch_object($requete_annonces);
        if ($annonce->image1 != "") {
            unlink("/home/autospot/www/img/" . $annonce->image1);
            unlink("/home/autospot/www/img/thumbs/" . $annonce->image1);
        }
        if ($annonce->image2 != "") {
            unlink("/home/autospot/www/img/" . $annonce->image2);
            unlink("/home/autospot/www/img/thumbs/" . $annonce->image2);
        }
        if ($annonce->image3 != "") {
            unlink("/home/autospot/www/img/" . $annonce->image3);
            unlink("/home/autospot/www/img/thumbs/" . $annonce->image3);
        }
        if ($annonce->image4 != "") {
            unlink("/home/autospot/www/img/" . $annonce->image4);
            unlink("/home/autospot/www/img/thumbs/" . $annonce->image4);
        }
        if ($annonce->image5 != "") {
            unlink("/home/autospot/www/img/" . $annonce->image5);
            unlink("/home/autospot/www/img/thumbs/" . $annonce->image5);
        }
        if ($annonce->image6 != "") {
            unlink("/home/autospot/www/img/" . $annonce->image6);
            unlink("/home/autospot/www/img/thumbs/" . $annonce->image6);
        }
		// mysqli_query($connect1, "DELETE FROM annonces_clients WHERE id='$id_annonce' && id_client='$id_client'");
		mysqli_query($connect1, "UPDATE annonces_clients SET visible=0 WHERE id='$id_annonce' && id_client='$id_client'");
		
		
        $requete_securspot = mysqli_query($connect1, "SELECT id, fichier FROM annonces_securspot WHERE id_annonce='$id_annonce'");
        if (mysqli_num_rows($requete_securspot) == 1) {
            $securspot = mysqli_fetch_object($requete_securspot);
            $id_securspot = $securspot->id;
            if ($securspot->fichier != "")
                unlink("/home/autospot/www/rapports/" . $securspot->fichier);
            mysqli_query($connect1, "DELETE FROM annonces_securspot WHERE id_annonce='$id_annonce'");
            mysqli_query($connect1, "DELETE FROM annonces_securspot_detail WHERE id_securspot='$id_securspot'");
		}
		
		
		$req_demandes = mysqli_query($connect1, "SELECT * FROM demandes_exam WHERE id_annonces_clients='$id_annonce'");
		while($tab = mysqli_fetch_array($req_demandes)){
			$id_demande = $tab['id'];
			// echo "id : ".$id_demande.',';
			delete_announce($dbh, $id_demande);
		}
		
		
		
    }
}
?>
<a id="haut"></a>

<!-- ESPACE UTILISATEUR -->
<div class="container">

    <h3 class="center">Espace utilisateur</h3>

    <div class="panel-group col-sm-12" id="utilisateur">

        <!-- *** AFFICHAGE DES MENUS *** -->

        <!-- MENU ANNONCES -->
        <div class="panel panel-blank col-sm-3">
            <div class="panel-heading annonces">
                <a href="#annonces" data-toggle="collapse" data-target="#annonces" data-parent="#utilisateur"><img src="images/utilisateur/picto_mes_annonces.png" alt="mes annonces" class="img-responsive center-block"></a>
                <a href="#annonces" data-toggle="collapse" data-target="#annonces" data-parent="#utilisateur" class="btn btn-autospot" role="button">Mes publications</a>
                <div class="clear"></div>
            </div>
        </div>

        <!-- MENU DEMANDES -->
        <div class="panel panel-blank col-sm-3">
            <div class="panel-heading demandes">
                <a href="#demandes" data-toggle="collapse"  data-target="#demandes" data-parent="#utilisateur"><img src="images/utilisateur/picto_suivi_demandes.png" alt="mes demandes" class="img-responsive center-block"></a>
                <a href="#demandes" data-toggle="collapse" data-target="#demandes" data-parent="#utilisateur" class="btn btn-autospot" role="button">Suivi de mes demandes</a>
                <div class="clear"></div>
            </div>
        </div>

        <!-- MENU FAVORIS -->
        <div class="panel panel-blank col-sm-3">
            <div class="panel-heading favoris">
                <a href="#favoris" data-toggle="collapse" data-target="#favoris" data-parent="#utilisateur"><img src="images/utilisateur/picto_mes_favoris.png" alt="mes favoris" class="img-responsive center-block"></a>
                <a href="#favoris" data-toggle="collapse" data-target="#favoris" data-parent="#utilisateur" class="btn btn-autospot" role="button">Mes favoris</a>
                <div class="clear"></div>
            </div>
        </div>

        <!-- MENU PROFIL -->
        <div class="panel panel-blank col-sm-3">
            <div class="panel-heading profil">
                <a href="#profil" data-toggle="collapse" data-target="#profil" data-parent="#utilisateur" class="mk_newtrigger_but" ><img src="images/utilisateur/picto_mon_profil.png" alt="mon profil" class="img-responsive center-block"></a>
                <a href="#profil" data-toggle="collapse" data-target="#profil" data-parent="#utilisateur" class="mk_newtrigger btn btn-autospot" role="button">Mon profil</a>
                <div class="clear"></div>
            </div>
        </div>
        <?php if (isset($_GET['annonces'])) :?>
            <script>
                jQuery(document).ready(function () {
                   $('#annonces').addClass('in');
                });
            </script>
        <?php endif ?>
        <!-- *** AFFICHAGE DES SECTIONS *** -->
        <div class="clear"></div>

        <!-- MES ANNONCES -->
        <div class="panel panel-blank col-sm-12">
            <div id="annonces" class="panel-collapse collapse">
                <hr class="blue">
                <h3>Mes publications <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#annonces" data-parent="#utilisateur">X</button></h3>
                <hr class="blue">
        <?PHP
        $id_client = mysqli_real_escape_string($connect1, $_SESSION['id_client']);
        $requete_annonces = mysqli_query($connect1, "SELECT * FROM annonces_clients WHERE id_client='$id_client' AND visible=1");
        if (mysqli_num_rows($requete_annonces) == 0) {
            ?>
                    <!-- pas d'annonces -->
                    <div id="annoncesVide" class="col-sm-12">
                        <p class="larger">Vous n'avez pour l'instant aucune annonce publiée</p>
                        <a href="publier.php" class="btn btn-primary" role="button">Publier une annonce maintenant</a>
                    </div>
                    <!-- Liste des annonces -->
                    <?PHP
                } else {
                    $nondefini = $LANG[5];
                    $requete_annonces = mysqli_query($connect1, "SELECT * FROM annonces_clients WHERE id_client='$id_client' AND visible=1");
                    while ($annonces = mysqli_fetch_object($requete_annonces)) {
                        $annee = explode("-", $annonces->premiereimm);
                        $annee = $annee[1] . "." . $annee[0];
                        $parution = explode("-", $annonces->parution);
                        $parution = $parution[2] . "." . $parution[1] . "." . $parution[0];
                        $expertisee = explode("-", $annonces->expertisee);
						$expertisee = $expertisee[1] . "." . $expertisee[0];
						
                        $ville = "";
                        $canton = "";
                        if ($annonces->ville == "") {
                            $requete_ville = mysqli_query($connect1, "SELECT ville FROM liste_npa WHERE code_postal='$annonces->codepostal'");
                            $ville = mysqli_fetch_object($requete_ville);
                            if (isset($ville->ville))
                                $ville = $ville->ville;
                        } else
                            $ville = $annonces->ville;
                        if ($annonces->canton == "") {
                            $requete_canton = mysqli_query($connect1, "SELECT canton FROM liste_npa WHERE code_postal='$annonces->codepostal'");
                            $canton = mysqli_fetch_object($requete_canton);
                            if (isset($canton->canton))
                                $canton = $canton->canton;
                        } else
                            $canton = $annonces->canton;
                        $ville = trim($ville);
                        if (preg_match("/ $canton/", $ville))
                            $ville = preg_replace("/ $canton/", "", $ville);
                        else if (preg_match("/\/ $canton/", $ville))
                            $ville = preg_replace("/\/ $canton/", "", $ville);
                        else if (preg_match("/($canton)/", $ville))
                            $ville = preg_replace("/($canton)/", "", $ville);
                        $image = "";
                        if ($annonces->image1 != "")
                            $image = $annonces->image1;
                        else if ($annonces->image2 != "")
                            $image = $annonces->image2;
                        else if ($annonces->image3 != "")
                            $image = $annonces->image3;
                        else if ($annonces->image4 != "")
                            $image = $annonces->image4;
                        else if ($annonces->image5 != "")
                            $image = $annonces->image5;
                        else if ($annonces->image6 != "")
                            $image = $annonces->image6;
                        ?>
                        <!-- annonce 1 -->
                        <div id="annonce-101" class="utilTab">
                            <div class="col-sm-3 centerGrey">
                                <div class="prixmobile">
                                    <?php echo number_format($annonces->prix, 0, ",", "'") . ".-"; ?>
                                </div>
                                <img src="img/<?PHP if ($image != "") echo $image;
                                else echo "no_image.png"; ?>" class="img-responsive">
                            </div>
                            <div class="col-sm-9">
                                <div class="row bgBlue">
                                    <div class="col-xs-8 col-sm-4 desc-modele">
                                        <h5><?PHP echo $annonces->marque." ".$annonces->modele." ".$annonces->version; ?></h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 desc-annee">
                                        <h5><?PHP if ($annee != "00.0000") echo $annee;
                                            else echo $nondefini; ?></h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 desc-km">
                                        <h5><?PHP echo $annonces->kilometrage; ?> km</h5>
                                    </div>
                                    <div class="hidden-xs col-sm-2 desc-prix">
                                        <?PHP echo number_format($annonces->prix, 0, ",", "'") . ".-"; ?>
                                    </div>
                                    <!-- <div class="col-sm-2">
                                        <div class="checkbox checkbox-slider--b">
                                            <label><input type="checkbox" id="aciveAnnonce-101"><span><span></span></span></label>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="row mk_div_cont bgBlue-xs">
                                    <div class="col-xs-4 col-sm-3 desc-cm">
                                        <?PHP if ($annonces->cylindrees != 0) echo $annonces->cylindrees . " cm3";
                                        else echo "Cylindrée: " . $nondefini; ?>
                                    </div>
                                    <div class="col-xs-4 col-sm-3 desc-chevaux">
        <?PHP if ($annonces->puissancechevaux != 0) echo Display::formatChevaux($annonces->puissancechevaux) . " " . $LANG[6];
        else echo $LANG[7] . ": " . $nondefini; ?>
                                    </div>
                                    <div class="col-xs-4 col-sm-3 desc-carbu">
        <?PHP if (($annonces->carburant != "") && (preg_match("/Essence/ui", $annonces->carburant))) echo $LANG[8];
        else if (($annonces->carburant != "") && ($annonces->carburant != "Autres")) echo $annonces->carburant;
        else echo $LANG[9] . ": " . $nondefini; ?>
                                    </div>
                                    <div class="hidden-xs col-sm-3 descrapide">
                                        <?php echo $annonces->desc1; ?>
                                    </div>
                                </div>
                                <div class="row mk_div_cont1 bgBlue-xs">
                                    <div class="col-xs-4 col-sm-3 desc-boite">
                                        <?PHP if ($annonces->transmission != "") echo $LANG[10] . " " . $annonces->transmission;
                                        else echo $LANG[11] . ": " . $nondefini; ?><br /><br />
                                    </div>
                                    <div class="col-xs-4 col-sm-3 desc-expert">
                                        <?PHP echo $LANG[12]; ?>: <?PHP if ($expertisee != "00.0000") echo $expertisee;
										else if ($annonces->year >= date("Y") - 4) echo $LANG[13];
										else echo 'N.D'; ?>
                                    </div>
                                    <div class="col-xs-4 col-sm-3 desc-lieu">
        <?PHP if ($annonces->codepostal != 0) echo $annonces->codepostal . " " . $ville . " (" . $canton . ")"; ?>
                                    </div>
                                </div>
                                <div class="row visible-xs bgBlue-xs">
                                    <div class="col-xs-12 descrapide">
                                        <?php echo $annonces->desc1; ?>
                                    </div>
                                </div>
                                <div class="row mk_div_cont1">
                                    <div class="col-sm-3">
                                        <?PHP echo sprintf("%s%d", $LANG[152], $annonces->id); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php echo sprintf("%s %s", $LANG[153], $parution); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 mk_div_cont2">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="/annonce.php?id=<?PHP echo $annonces->id; ?>"><span class="picto voir">Voir l'annonce</span></a>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="/publier.php?modifier=<?PHP echo $annonces->id; ?>"><span class="picto modifier">Modifier</span></a>
                                            </div>
                                            <?PHP
                                            $requete_securspot = mysqli_query($connect1, "SELECT fichier FROM annonces_securspot WHERE id_annonce='$annonces->id'");
                                            if (mysqli_num_rows($requete_securspot) == 1) {
                                                $securspot = mysqli_fetch_object($requete_securspot);
                                                ?>
                                                <div class="col-xs-12 col-sm-3">
                                                    <a href="/rapports/<?PHP echo $securspot->fichier; ?>"><span class="picto visualiser">Visualiser le rapport Securspot</span></a>
                                                </div>
                                                <?PHP
                                            }
                                            ?>
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="/compte.php?supprimer=<?PHP echo $annonces->id; ?>&annonces"><span class="picto supprimer">Supprimer</span></a>	<!-- mes annonces-->	
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <?PHP
                                        if ($annonces->valide == 0) {
                                            ?>
                                            <div id="orangeAlert" class="alert alert-warning pull-right alertShow">
                                                <span class="lettrine"><i class="fa fa-check"></i></span> Pour des raisons de sécurité, nous sommes entrain d'examiner votre annonce. Elle sera publiée d'ici quelques minutes.
                                            </div>
                                            <?PHP
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
        <?PHP
    }
}
?>
            </div>
        </div>
        <!-- FIN MES ANNONCES -->

        <!-- MES DEMANDES -->
        <div class="panel panel-blank col-sm-12">
            <div id="demandes" class="panel-collapse collapse">
                <hr class="blue">
                <h3>Suivi de mes demandes <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#demandes" data-parent="#utilisateur">X</button></h3>
                <hr class="blue">

                <?php
                $sql = sprintf(
                    'SELECT * FROM demandes_exam WHERE id_client_acheteur = "%s"',
                    mysqli_real_escape_string($connect1, $_SESSION['id_client'])
                );
                $demandes = mysqli_query($connect1, $sql);
                $count = mysqli_num_rows($demandes);
                ?>

                <!-- pas de demandes -->
                <?php if ($count == 0): ?>
                    <div id="demandesVide" class="col-sm-12">
                        <p class="larger">Vous n'avez pour l'instant aucune demande d'expertise</p>
                        <div class="clear"></div>
                    </div>
                <?php else: ?>
                    <!-- Liste des demandes -->
                    <?php while($demande = $demandes->fetch_object()): ?>
						<?php 
						$jsonSuivi = formatJsonSuivi($demande->json_suivi);
						$tabSuivi = json_decode($demande->json_suivi, true)
						?>
                        <?php
                        $sql = sprintf(
                            'SELECT * FROM annonces_clients WHERE id="%s"',
                            mysqli_real_escape_string($connect1, $demande->id_annonces_clients)
                        );
                        $query = mysqli_query($connect1, $sql);
						$annonce = mysqli_fetch_object($query);
						if($annonce == null) continue;
                        ?>
                        <div id="demandes-<?php echo $demande->id; ?>">
                            <?php
                            $nondefini = $LANG[5];
                            $annee = explode("-", $annonce->premiereimm);
                            $annee = $annee[1] . "." . $annee[0];
                            $expertisee = explode("-", $annonce->expertisee);
                            $expertisee = $expertisee[1] . "." . $expertisee[0];
                            $ville = "";
                            $canton = "";
                            if ($annonce->ville == "") {
                                $requete_ville = mysqli_query($connect1, "SELECT ville FROM liste_npa WHERE code_postal='$annonce->codepostal'");
                                $ville = mysqli_fetch_object($requete_ville);
                                if (isset($ville->ville))
                                    $ville = $ville->ville;
                            } else
                                $ville = $annonce->ville;
                            if ($annonce->canton == "") {
                                $requete_canton = mysqli_query($connect1, "SELECT canton FROM liste_npa WHERE code_postal='$annonce->codepostal'");
                                $canton = mysqli_fetch_object($requete_canton);
                                if (isset($canton->canton))
                                    $canton = $canton->canton;
                            } else
                                $canton = $annonce->canton;
                            $ville = trim($ville);
                            if (preg_match("/ $canton/", $ville))
                                $ville = preg_replace("/ $canton/", "", $ville);
                            else if (preg_match("/\/ $canton/", $ville))
                                $ville = preg_replace("/\/ $canton/", "", $ville);
                            else if (preg_match("/($canton)/", $ville))
                                $ville = preg_replace("/($canton)/", "", $ville);
                            $lienannonce = "annonce.php?id=" . $annonce->id;
                            $imagei = 1;
                            $object = "image" . $imagei;
                            while (isset($annonce->$object)) {
                                $object = "image" . $imagei;
                                if ((isset($annonce->$object)) && ($annonce->$object != "")) {
                                    $image = $annonce->$object;
                                    break;
                                } else
                                    unset($image);
                                $imagei++;
                            }
                            ?>
                            <!-- suivi demande -->
                            <div id="favoris-101" class="utilTab">
                                <div class="col-sm-3">
                                    <div class="prixmobile">
                                        <?php echo number_format($annonce->prix, 0, ",", "'") . ".-"; ?>
                                    </div>
                                    <?php $imgPath = isset($image) ? $image : 'no_image.png'; ?>
                                    <img src="img/<?php echo $imgPath; ?>" class="img-responsive">
                                </div>
                                <div class="col-sm-9">
                                    <div class="row bgBlue bgBlue-xs">
                                        <div class="col-xs-8 col-sm-4 desc-modele">
                                            <h5><?php echo $annonce->marque." ".$annonce->modele." ".$annonce->version; ?></h5>
                                        </div>
                                        <div class="col-xs-2 col-sm-2 desc-annee">
                                            <h5><?php echo $annee != "00.0000" ? $annee : $nondefini; ?></h5>
                                        </div>
                                        <div class="col-xs-2 col-sm-2 desc-km">
                                            <h5><?php echo $annonce->kilometrage; ?> km</h5>
                                        </div>
                                        <div class="hidden-xs col-sm-2 desc-prix">
                                            <?php echo number_format($annonce->prix, 0, ",", "'") . ".-"; ?>
                                        </div>
                                    </div>
                                    <div class="row mk_div_cont bgBlue-xs">
                                        <div class="col-xs-4 col-sm-3 desc-cm">
                                            <?php echo $annonce->cylindrees != 0 ? $annonce->cylindrees . " cm3" : "Cylindrée: " . $nondefini; ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-3 desc-chevaux">
                                            <?PHP if ($annonce->puissancechevaux != 0) echo Display::formatChevaux($annonce->puissancechevaux) . " " . $LANG[6];
                                            else echo $LANG[7] . ": " . $nondefini; ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-3 desc-carbu">
                                            <?PHP if (($annonce->carburant != "") && (preg_match("/Essence/ui", $annonce->carburant))) echo $LANG[8];
                                            else if (($annonce->carburant != "") && ($annonce->carburant != "Autres")) echo $annonce->carburant;
                                            else echo $LANG[9] . ": " . $nondefini; ?>
                                        </div>
                                        <div class="hidden-xs col-sm-3 descrapide">
                                            <?PHP echo $annonce->desc1; ?>
                                        </div>
                                    </div>
                                    <div class="row mk_div_cont bgBlue-xs">
                                        <div class="col-xs-4 col-sm-3 desc-boite">
                                            <?PHP if ($annonce->transmission != "") echo $LANG[10] . " " . $annonce->transmission;
                                            else echo $LANG[11] . ": " . $nondefini; ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-3 desc-expert">
                                            <?PHP echo $LANG[12]; ?>: <?PHP if ($expertisee != "00.0000") echo $expertisee;
                                    else if ($annonce->year >= date("Y") - 4) echo $LANG[13];
                                    else echo 'N.D'; ?>
                                        </div>
                                        <div class="col-xs-4 col-sm-3 desc-lieu">
                                            <?PHP if ($annonce->codepostal != 0) echo $annonce->codepostal . " " . $ville . " (" . $canton . ")"; ?>
                                        </div>
                                    </div>
                                    <div class="row visible-xs bgBlue-xs">
                                        <div class="col-xs-12 descrapide">
                                            <?php echo $annonce->desc1; ?>
                                        </div>
									</div>
									
									<?php if($annonce->visible == 1){ ?>
                                    <div class="row mk_div_cont2">
                                        <div class="col-xs-12 col-sm-4">
                                            <a href="<?PHP echo $lienannonce; ?>"><span class="picto voir">Voir l'annonce</span></a>
                                        </div>
									</div>
									<?php } ?>
									
									
                                </div>
                            </div>
                            <div class="clear"></div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="2">Inspection technique N°<?php echo $demande->id; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Date et heure</th>
                                        <th>Suivi de ma demande d'inspection technique</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php 
                                            $datetime = \DateTime::createFromFormat('Y-m-d H:i:s', $demande->demande_ts, new \DateTimeZone('Europe/Paris'));
                                            echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                            ?>
                                        </td>
                                        <td class="text-justify">
                                            <p>Votre demande d'inspection technique N°<?php echo $demande->id; ?> sur la <?php echo sprintf("%s %s %s %s %s", '<strong>', $annonce->marque, $annonce->modele, $annonce->version, '</strong>'); ?> a bien été prise en compte et a été transférée au garage partenaire correspondant.</p>
                                            <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                        </td>
                                    </tr>
                                    <?php if (array_key_exists('garage_accept_demande', $jsonSuivi)): ?>
                                        <?php 
                                        $sql = sprintf(
                                            'SELECT * FROM garages WHERE id = "%s"',
                                            mysqli_real_escape_string($connect1, $jsonSuivi['garage_accept_demande']['params']['id_garage'])
                                        );
                                        $query = mysqli_query($connect1, $sql);
                                        $garage = mysqli_fetch_object($query);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTimeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['garage_accept_demande']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <p>Votre demande d'inspection technique a été validée par le garage partenaire <?php echo sprintf('%s %s %s', '<strong>', $garage->name, '</strong>'); ?> et sera en charge d'effectuer l'inspection technique sur la <?php echo sprintf("%s %s %s %s %s", '<strong>', $annonce->marque, $annonce->modele, $annonce->version, '</strong>'); ?>.</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                    
																		
																		
                                    <?php if (array_key_exists('garage_accept_demande', $jsonSuivi)): ?>
                                        <?php 
                                        $sql = sprintf(
                                            'SELECT * FROM garages WHERE id = "%s"',
                                            mysqli_real_escape_string($connect1, $jsonSuivi['garage_accept_demande']['params']['id_garage'])
                                        );
                                        $query = mysqli_query($connect1, $sql);
                                        $garage = mysqli_fetch_object($query);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTimeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['garage_accept_demande']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <p>Le vendeur particulier a reçu un email et un SMS l'informant qu'une demande d'inspection technique a été effectuée sur son véhicule.</p>
                                                <p>Le vendeur a un délai de 7 jours ouvrables pour contacter le garage partenaire <?php echo sprintf('%s %s %s', '<strong>', $garage->name, '</strong>'); ?> afin de fixer le rendez-vous de l'inspection technique.</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
																		
																		
																		
																		
                                    <?php if (array_key_exists('garage_set_time1', $jsonSuivi)): ?>
                                        <?php 
                                        $sql = sprintf(
                                            'SELECT * FROM garages WHERE id = "%s"',
                                            mysqli_real_escape_string($connect1, $jsonSuivi['garage_accept_demande']['params']['id_garage'])
                                        );
                                        $query = mysqli_query($connect1, $sql);
                                        $garage = mysqli_fetch_object($query);
                                        ?>
                                        <tr>
                                            <td>
																								<?php 
																								
                                                $datetime = new \Datetime('now');
                                                $datetime->setTimestamp($jsonSuivi['garage_set_time1']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <?php
																								$jsonSuivi['garage_set_time1']['params']['ts'] -= getUTCshift();
																								$datetime = new \Datetime('now');
                                                $datetime->setTimestamp($jsonSuivi['garage_set_time1']['params']['ts']);
                                                ?>
                                                <p>Félicitations ! La date de l’inspection technique est fixée au : <?php echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i')); ?></p>

                                                <p>Si vous avez souhaité être présent lors de l’inspection technique, voici les coordonnées du garage partenaire en charge d’effectuer l’inspection technique :</p>
                                                <p class="text-center">
                                                    <?php echo $garage->name; ?><br />
                                                    <?php echo $garage->address; ?><br />
                                                    <?php echo $garage->cp_unique.' '.$garage->city; ?><br />
                                                    <?php echo $garage->tel; ?>
                                                </p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
																		<?php endif; ?>
                                    
																		
																		
																		
																		
																		<?PHP
																		
																		foreach($tabSuivi as $k => $v){
																			
																			
																			if($v['key'] == 'garage_set_time2'){
																				
																				
                                        $sql = sprintf(
                                            'SELECT * FROM garages WHERE id = "%s"',
                                            mysqli_real_escape_string($connect1, $jsonSuivi['garage_accept_demande']['params']['id_garage'])
                                        );
                                        $query = mysqli_query($connect1, $sql);
                                        $garage = mysqli_fetch_object($query);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTImeZone('Europe/Paris'));
                                                $datetime->setTimestamp($v['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <?php
																								
																								$v['params']['ts'] -= getUTCshift();
                                                $datetime = new \Datetime('now', new \DateTImeZone('Europe/Paris'));
                                                $datetime->setTimestamp($v['params']['ts']);
																								//correction
																								?>
																								
                                                Le rendez-vous de l'inspection technique avec notre garage partenaire <?php echo '<strong>'.$garage->name.'</strong>'; ?> a été reporté par le vendeur au : <?php echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i')); ?>
                                                <p>Si vous avez souhaité être présent lors de l’inspection technique, voici les coordonnées du garage partenaire en charge d’effectuer l’inspection technique :</p>
                                                <p class="text-center">
                                                    <?php echo $garage->name; ?><br />
                                                    <?php echo $garage->address; ?><br />
                                                    <?php echo $garage->cp_unique.' '.$garage->city; ?><br />
                                                    <?php echo $garage->tel; ?>
                                                </p>
                                                <p>Si vous avez d'éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
																			<?php
																			}
																			
																		}
																		
																		
																		?>
																		
																		
																		
																		
									
                                    <?php if (array_key_exists('exam_complete', $jsonSuivi)): ?>
                                        <?php
                                        $sql = sprintf(
                                            'SELECT * FROM clients WHERE id = "%s"',
                                            mysqli_real_escape_string($connect1, $_SESSION['id_client'])
                                        );
                                        $query = mysqli_query($connect1, $sql);
                                        $client = mysqli_fetch_object($query);
                                        ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTImeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['exam_complete']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <?php 
                                                if ($client->lang2 == 'fr') {
                                                    $langue = 'français';
                                                } elseif ($client->lang2 == 'de') {
                                                    $langue = 'allemand';
                                                } else {
                                                    $langue = 'italien';
												}
												
												
												
												$sql = sprintf(
													'SELECT * FROM documents WHERE id_demande = %s AND type="rapport_client"',
													mysqli_real_escape_string($connect1, $demande->id)
												);
												$documents = mysqli_query($connect1, $sql);
												$filename_rapport = "";
												if(mysqli_num_rows($demandes) > 0){
													$document = mysqli_fetch_array($documents);
													$filename_rapport = 'appbackend/'.$document['filename'];
												}
												
												
                                                ?>
                                                <p>Félicitations ! L’inspection technique est terminée et votre rapport d’inspection technique est disponible <a href="<?php echo $filename_rapport;?>" target="_blank">en cliquant ici</a>.</p>
                                                <p>Le vendeur de ce véhicule a inséré cette annonce en <?php echo $langue; ?></p>
                                                <p>Afin de communiquer avec un vendeur parlant une langue étrangère, n’hésitez pas à utiliser gratuitement notre plate-forme de discussions, disponible en dessous de ce tableau. (à venir)</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
									<?php endif; ?>
									
									
									
																		
																		
																		
																		
																		
																		
																		
																		
																		
																		
																		<!--------------------- annulations  ------------------->
																		
																		<?php if (array_key_exists('garage_unavailable', $jsonSuivi)): ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTimeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['garage_unavailable']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <p>Malheureusement, votre demande d'inspection technique a été annulée pour la raison suivante :</p>
                                                <p>Votre demande d'inspection technique a été transmise aux deux garages partenaires les plus proches du lieu de domicile du vendeur.</p>
                                                <p>Malheureusement, pour des raisons d’emploi du temps complet, ces deux garages sont dans l'impossibilité d’effectuer cette inspection technique dans les 7 jours ouvrables suivant la date de la demande.</p>
                                                <p>Comme convenu dans nos Conditions générales d’utilisations, nous procéderons au remboursement total du paiement lié à cette demande d’inspection technique.</p>
                                                <p>Veuillez nous excuser pour l'issue défavorable de votre demande d’inspection technique.</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
																		
																		<?php if ($annonce === null || array_key_exists('delete_announce', $jsonSuivi)): ?>
                                        <tr>
                                            <td>
																								<?php 
																								/* 
                                                $datetime = \DateTime::createFromFormat('Y-m-d H:i:s', $demande->demande_ts, new \DateTimeZone('Europe/Paris'));
																								echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
																								 */
																								$datetime = new \Datetime('now', new \DateTimeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['delete_announce']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <p>Malheureusement, cette demande d'inspection technique a été annulée pour la raison suivante : Le véhicule a été entre-temps vendu.</p>
                                                <p>Comme convenu dans nos Conditions générales d’utilisations, nous procéderons au remboursement total du paiement lié à cette demande d’inspection technique.</p>
                                                <p>Veuillez nous excuser pour l'issue défavorable de votre demande d'inspection technique.</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
																		
																		<?php if(array_key_exists('seller_iddle_complete', $jsonSuivi)): ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTImeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['seller_iddle_complete']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <p>Malheureusement, votre demande d'inspection technique a été annulée pour la raison suivante :</p>
                                                <p>Impossibilité de fixer le rendez-vous de l’inspection technique. Aucune nouvelle du vendeur dans les 7 jours ouvrables suivant la date de la demande d’inspection technique.</p>
                                                <p>Comme convenu dans nos Conditions générales d’utilisations, nous procéderons au remboursement total du paiement lié à cette demande d’inspection technique.</p>
                                                <p>Veuillez nous excuser pour l’issue défavorable de votre demande d’inspection technique.</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
																		
																		<?php if (array_key_exists('garage_declare_absent', $jsonSuivi)): ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTImeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['garage_declare_absent']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <p>Malheureusement, votre demande d’inspection technique a été annulée pour la raison suivante : Le vendeur était absent le jour de l’inspection technique.</p>
                                                <p>Comme convenu dans nos Conditions générales d’utilisations, nous procéderons au remboursement total du paiement lié à cette demande d’inspection technique.</p>
                                                <p>Veuillez nous excuser pour l’issue défavorable de votre demande d’inspection technique.</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
																		<?php endif; ?>
																		
																		<?php if (array_key_exists('garage_declare_error', $jsonSuivi)): ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTImeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['garage_declare_error']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
                                                <p>Malheureusement, cette demande d’inspection technique a été annulée pour la raison suivante: Problème technique (wifi ou tablette tactile)</p>
                                                <p>Comme convenu dans nos Conditions générales d’utilisations, nous procéderons au remboursement total du paiement lié à cette demande d’inspection technique.</p>
                                                <p>Notre service technique a contacté le garage partenaire en charge de cette inspection technique.</p>
                                                <p>S’il s’avère que le problème technique provient de la tablette tactile, nous procéderons rapidement à son remplacement. S’il s’avère que le problème technique provient du garage (dysfonctionnement du router wifi ou autre), il lui sera demandé de procéder rapidement aux réparations.</p>
                                                <p>Veuillez nous excuser pour l’issue défavorable de votre demande d’inspection technique.</p>
                                                <p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
                                            </td>
                                        </tr>
																		<?php endif; ?>
																		
																		<?php if (array_key_exists('garage_declare_sold', $jsonSuivi)): ?>
                                        <tr>
                                            <td>
                                                <?php 
                                                $datetime = new \Datetime('now', new \DateTImeZone('Europe/Paris'));
                                                $datetime->setTimestamp($jsonSuivi['garage_declare_sold']['ts']);
                                                echo str_replace(' ', '&nbsp;', $datetime->format('d/m/Y H:i'));
                                                ?>
                                            </td>
                                            <td class="text-justify">
																							<p>Malheureusement, cette demande d’inspection technique a été annulée pour la raison suivante: Le véhicule a été entre-temps vendu</p>
																							<p>Comme convenu dans nos Conditions générales d’utilisations, nous procéderons au remboursement total du paiement lié à cette demande d’inspection technique.</p>
																							<p>Veuillez nous excuser pour l’issue défavorable de votre demande d’inspection technique.</p>
																							<p>Si vous avez d’éventuelles questions ou remarques n’hésitez pas à nous les communiquer par email à l’adresse contact@autospot.ch, via notre formulaire de contact disponible <a href="/contact.php">ici</a> ou directement sur notre système de conversation en temps réel (live chat).</p>
																						</td>
                                        </tr>
                                    <?php endif; ?>
																		
																		
																		
																		
                                </tbody>
                            </table>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- FIN MES DEMANDES -->

        <!-- MES FAVORIS -->
        <div class="panel panel-blank col-sm-12">
            <div id="favoris" class="panel-collapse<?PHP if (isset($_GET['favoris'])) echo ' collapsed';
                                else echo " collapse"; ?>">
                <hr class="blue">
                <h3>Mes favoris <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#favoris" data-parent="#utilisateur">X</button></h3>
                <hr class="blue">

				<?PHP
				$requete_favoris = mysqli_query($connect1, "SELECT id, annonce FROM clients_favoris WHERE client='$_SESSION[id_client]'");
				$nb_favoris = mysqli_num_rows($requete_favoris);
				if ($nb_favoris == 0) {
				?>
                    <!-- pas de favoris -->
                    <div id="favorisVide" class="col-sm-12">
                        <p class="larger">Vous n'avez pour l'instant aucune annonce enregistrée dans vos favoris</p>
                        <div class="clear"></div>
                    </div>
                    <?PHP
                } else {
                    ?>
                    <!-- Liste des favoris -->
                    
                    <?PHP
					
					$hasDeletedAnnonce = false;
					$nb_favoris2 = 0;
					$todelete = [];
					
					while ($favoris = mysqli_fetch_object($requete_favoris)) {
						$requete_annonces = mysqli_query($connect1, "SELECT * FROM annonces_clients WHERE id='$favoris->annonce'");
						$annonces = mysqli_fetch_object($requete_annonces);
						// var_dump($annonces);
						if($annonces->visible == 0){
							$hasDeletedAnnonce = true;
							$todelete[] = $favoris->annonce;
							
						}
						else $nb_favoris2++;
					}
					/* 
					echo "nb_favoris : ".$nb_favoris."<br />";
					echo "nb_favoris2 : ".$nb_favoris2."<br />";
					echo "hasDeletedAnnonce : ".$hasDeletedAnnonce."<br />";
					echo "todelete<br />";
					var_dump($todelete);
					 */
					if(isset($_GET['favoris'])){
						foreach($todelete as $annonce){
							mysqli_query($connect1, "DELETE FROM clients_favoris WHERE client='$_SESSION[id_client]' AND annonce='$annonce'");
						}
					}
					
					?>
					<p><?PHP echo $nb_favoris2; ?> annonces classées dans vos favoris</p>
					<?php
					
					if($hasDeletedAnnonce){
						?>
						<div id='favoris_deleted' class="alert alert-warning" role="alert">Une annonce présente dans vos favoris a été supprimée car le vendeur l’a retiré d’Autospot</div>
						<script>
						$('#favoris_deleted').delay(4000).fadeTo("slow", 0);
						</script>
						<?php
					}
					
					$requete_favoris = mysqli_query($connect1, "SELECT id, annonce FROM clients_favoris WHERE client='$_SESSION[id_client]'");
					
                    while ($favoris = mysqli_fetch_object($requete_favoris)) {
                        $requete_annonces = mysqli_query($connect1, "SELECT * FROM annonces_clients WHERE id='$favoris->annonce'");
						$annonces = mysqli_fetch_object($requete_annonces);
						
						if($annonces->visible == 1){
							
							$nondefini = $LANG[5];
							$annee = explode("-", $annonces->premiereimm);
							$annee = $annee[1] . "." . $annee[0];
							$expertisee = explode("-", $annonces->expertisee);
							$expertisee = $expertisee[1] . "." . $expertisee[0];
							$ville = "";
							$canton = "";
							if ($annonces->ville == "") {
								$requete_ville = mysqli_query($connect1, "SELECT ville FROM liste_npa WHERE code_postal='$annonces->codepostal'");
								$ville = mysqli_fetch_object($requete_ville);
								if (isset($ville->ville))
									$ville = $ville->ville;
							} else
								$ville = $annonces->ville;
							if ($annonces->canton == "") {
								$requete_canton = mysqli_query($connect1, "SELECT canton FROM liste_npa WHERE code_postal='$annonces->codepostal'");
								$canton = mysqli_fetch_object($requete_canton);
								if (isset($canton->canton))
									$canton = $canton->canton;
							} else
								$canton = $annonces->canton;
							$ville = trim($ville);
							if (preg_match("/ $canton/", $ville))
								$ville = preg_replace("/ $canton/", "", $ville);
							else if (preg_match("/\/ $canton/", $ville))
								$ville = preg_replace("/\/ $canton/", "", $ville);
							else if (preg_match("/($canton)/", $ville))
								$ville = preg_replace("/($canton)/", "", $ville);
							$lienannonce = "annonce.php?id=" . $annonces->id;
							$imagei = 1;
							$object = "image" . $imagei;
							while (isset($annonces->$object)) {
								$object = "image" . $imagei;
								if ((isset($annonces->$object)) && ($annonces->$object != "")) {
									$image = $annonces->$object;
									break;
								} else
									unset($image);
								$imagei++;
							}
							?>
							<!-- favoris -->
							<div id="favoris-<?php echo $annonces->id;?>" class="utilTab">
								<div class="col-sm-3">
									<div class="prixmobile">
										<?php echo number_format($annonces->prix, 0, ",", "'") . ".-"; ?>
									</div>
									<?php $imgPath = isset($image) ? $image : 'no_image.png'; ?>
									<img src="img/<?php echo $imgPath; ?>" class="img-responsive">
								</div>
								<div class="col-sm-9">
									<div class="row bgBlue bgBlue-xs">
										<div class="col-xs-8 col-sm-4 desc-modele">
											<h5><?php echo $annonces->marque." ".$annonces->modele." ".$annonces->version; ?></h5>
										</div>
										<div class="col-xs-2 col-sm-2 desc-annee">
											<h5><?php echo $annee != "00.0000" ? $annee : $nondefini; ?></h5>
										</div>
										<div class="col-xs-2 col-sm-2 desc-km">
											<h5><?php echo $annonces->kilometrage; ?> km</h5>
										</div>
										<div class="hidden-xs col-sm-2 desc-prix">
											<?php echo number_format($annonces->prix, 0, ",", "'") . ".-"; ?>
										</div>
									</div>
									<div class="row mk_div_cont bgBlue-xs">
										<div class="col-xs-4 col-sm-3 desc-cm">
											<?php echo $annonces->cylindrees != 0 ? $annonces->cylindrees . " cm3" : "Cylindrée: " . $nondefini; ?>
										</div>
										<div class="col-xs-4 col-sm-3 desc-chevaux">
											<?PHP if ($annonces->puissancechevaux != 0) echo Display::formatChevaux($annonces->puissancechevaux) . " " . $LANG[6];
											else echo $LANG[7] . ": " . $nondefini; ?>
										</div>
										<div class="col-xs-4 col-sm-3 desc-carbu">
											<?PHP if (($annonces->carburant != "") && (preg_match("/Essence/ui", $annonces->carburant))) echo $LANG[8];
											else if (($annonces->carburant != "") && ($annonces->carburant != "Autres")) echo $annonces->carburant;
											else echo $LANG[9] . ": " . $nondefini; ?>
										</div>
										<div class="hidden-xs col-sm-3 descrapide">
											<?PHP echo $annonces->desc1; ?>
										</div>
									</div>
									<div class="row mk_div_cont bgBlue-xs">
										<div class="col-xs-4 col-sm-3 desc-boite">
											<?PHP if ($annonces->transmission != "") echo $LANG[10] . " " . $annonces->transmission;
											else echo $LANG[11] . ": " . $nondefini; ?>
										</div>
										<div class="col-xs-4 col-sm-3 desc-expert">
											<?PHP echo $LANG[12]; ?>: <?PHP if ($expertisee != "00.0000") echo $expertisee;
											else if ($annonces->year >= date("Y") - 4) echo $LANG[13];
											else echo 'N.D'; ?>
										</div>
										<div class="col-xs-4 col-sm-3 desc-lieu">
											<?php if ($annonces->codepostal != 0) echo $annonces->codepostal . " " . $ville . " (" . $canton . ")"; ?>
										</div>
									</div>
									<div class="row visible-xs bgBlue-xs">
										<div class="col-xs-12 descrapide">
											<?php echo $annonces->desc1; ?>
										</div>
									</div>
									<div class="row mk_div_cont2">
										<div class="col-xs-12 col-sm-4">
											<a href="<?php echo $lienannonce; ?>"><span class="picto voir">Voir l'annonce</span></a>
										</div>
										<div class="col-xs-12 col-sm-4">
											<a onclick="favoris('<?php echo $annonces->id; ?>');"><span class="picto supprimer">Supprimer</span></a>
										</div>
									</div>
								</div>
							</div>
							
						
						<?php
						}
						
						?>
						<div class="clear"></div>
						<?php
						
    }
}
?>
            </div>
        </div>
        <!-- FIN MES FAVORIS -->

        <!-- MON PROFIL -->
        <div class="panel panel-blank col-sm-12">
            <div id="profil" class="panel-collapse<?PHP if (isset($_GET['profil'])) echo " collapsed";
else echo " collapse"; ?>">
                <hr class="blue">
                <h3>Mon profil <button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="collapse" data-target="#profil" data-parent="#utilisateur">X</button></h3>
                <hr class="blue">
                <div class="clear"></div>

                <div class="col-sm-4">
                    <div class="clear"></div>
                    <a href="modifier_mail.php"><img src="images/utilisateur/picto_modifier_mail.png" alt="Modifier mon email" class="img-responsive center-block"></a>
                    <a href="modifier_mail.php" class="btn btn-autospot" role="button">Modifier mon email</a>
                </div>
                <div class="clearfix visible-xs-block"></div>

                <div class="col-sm-4">
                    <div class="clear"></div>
                    <a href="modifier_coordonnees.php"><img src="images/utilisateur/picto_modifier_coordonnees.png" alt="Modifier mes coordonnées" class="img-responsive center-block"></a>
                    <a href="modifier_coordonnees.php" class="btn btn-autospot" role="button">Modifier mes coordonnées</a>
                </div>
                <div class="clearfix visible-xs-block"></div>

                <div class="col-sm-4">
                    <div class="clear"></div>
                    <a href="modifier_passe.php"><img src="images/utilisateur/picto_modifier_passe.png" alt="Modifier mon mot de passe" class="img-responsive center-block"></a>
                    <a href="modifier_passe.php" class="btn btn-autospot" role="button">Modifier mon mot de passe</a>
                </div>
                <div class="clearfix visible-xs-block"></div>
            </div>
        </div>
        <!-- FIN MON PROFIL -->


    </div>

    <!-- FIN ESPACE UTILISATEUR -->
</div>

<script>
    jQuery('#annonces, #favoris, #profil, #demandes').on('shown.bs.collapse', function () {
        var target = jQuery('#'+this.id);

        if (target.length) {
            jQuery('html, body').animate({
                scrollTop: target.offset().top - $('#header').height()
            }, 1000);
        }
    });
</script>

<?php include("footer.php"); ?>