angular.
module('starter').
component('pageQuestions', {

	templateUrl: "js/exam/page_questions/page_questions.template.html",
	
	bindings: {
		so: '='
	},

	controller: ["$scope", "$stateParams", "ionicDatePicker", 'ionicTimePicker', '$state', 'ServerService', '$ionicModal', 'ControlManager', 'ControlItemManager',
	function PageQuestionController($scope, $stateParams, ionicDatePicker, ionicTimePicker, $state, ServerService, $ionicModal, ControlManager, ControlItemManager) {

		console.log("PageQuestionsController");
		var _this = this;
		$scope.valid = {showLoader : false, error_msg : ""};
		
		ControlItemManager.init($scope);
		
		_this.isEnd = false;
		
		
		
		_this.clickValid = function()
		{
			
			console.log("PageQuestionCtrl.clickValid");
			if(_this.isEnd){
				$scope.$emit("FINISH_EXAM");
			}
			else{
				ControlItemManager.valid();
			}
			
			
		};
		
		_this.so.showLoader = function(_value)
		{
			console.log("page_question.showLoader("+_value+")");
			$scope.valid.showLoader = _value;
			if(_value) $scope.valid.error_msg = "";
		};
		
		_this.so.setError = function(_err)
		{
			$scope.valid.error_msg = _err;
		};
		
		
		//page = questions / end
		_this.so.gotoPage = function(_page)
		{
			_this.isEnd = (_page == "end");
			$scope.submit_label = (_page == "end" ) ? "exam_valid_final" : "exam_next";
		}
		
		
		
		if(true){
			// _this.clickValid();
		}
		
		
	}]
});