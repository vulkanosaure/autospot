angular.
module('starter').
component('pageInfo', {

	templateUrl: "js/exam/page_info/page_info.template.html",

	controller: ["$scope", "$stateParams", "ionicDatePicker", 'ionicTimePicker', '$state', 'ServerService', '$ionicModal', function ExamController($scope, $stateParams, ionicDatePicker, ionicTimePicker, $state, ServerService, $ionicModal) {

		console.log("PageInfoController");
		var _this = this;
		_this.itemId = $stateParams.itemId;
		
		
		_this.load = function() {
			_this.msg_error_connexion = "";
			_this.error_connexion = false;
			_this.showLoader = true;

			ServerService.getItem(_this.itemId).then(function(_data) {
				console.log("ListingController.ok");
				_this.datavehicule = _data;
				_this.showLoader = false;
				

			}, function(_data) {
				console.log("ListingController.error");
				_this.showLoader = false;
				_this.msg_error_connexion = "Problème de connexion, veuillez rééssayer";
				_this.error_connexion = true;

			});
		};
		
		_this.load();
		
		/*
		_this.datavehicule = {
			brand: "Opel",
			model: "Astra",
			version: "1.2.4",
			year: "1999",
			carburant: "SP95",
			engine: "Boite manuelle",
			nbdoors: "4",
			shortdesc: "La toute nouvelle Opel Astra aura droit à un bloc essence inédit dans sa gamme.",
			longdesc: "Le constructeur allemand vient de dévoiler sa toute nouvelle Astra de cinquième génération, qui sera exposée sur le stand de la marque à l'occasion du prochain salon de Francfort.On apprend cette fois que l'auto inaugurera également une nouvelle génération de moteurs essence.",
			otherinfo: "Véhicule en bonne état général"
			
		};
		*/
		
		_this.clickNext = function()
		{
			$scope.$emit("PAGE_INFO_NEXT");
		}
		
	}]
});