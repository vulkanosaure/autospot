angular.
module('starter').
component('examcomp', {

	templateUrl: "js/exam/exam.template.html",

	controller: ["$scope", "$window", "$compile", "$stateParams", "ionicDatePicker", 'ionicTimePicker', '$state', 'ServerService', '$ionicModal', 'ControlManager', 'ItemStorage', 'Config', 'FileUploadService', 'ControlItemManager',
		function ExamController($scope, $window, $compile, $stateParams, ionicDatePicker, ionicTimePicker, $state, ServerService, $ionicModal, ControlManager, ItemStorage, Config, FileUploadService, ControlItemManager) {


			var _this = this;
			_this.itemId = "";
			_this.page_question = {};

			//________________________________________________________________________________________________________
			//functions



			_this.initData = function(data) {

				console.log("ExamController.initData");
				//debugger;

				

				ServerService.getItem($stateParams.itemId).then(function(_data) {

					var _initskips_c = [];
					var _initskips_o = [];
					var _nbdoor = ServerService.item["nbdoors"];
					console.log("Exam.component, data : "+_nbdoor);
					
					if(_nbdoor == 3){
						_initskips_c.push("portiere_br");
						_initskips_c.push("portiere_bl");
						
						//here
						_initskips_o.push("ec05sz9y");
						_initskips_o.push("ec05e2g0");
					}

					//todo set
					ControlManager.set_initskips_c(_initskips_c);
					ControlManager.set_initskips_o(_initskips_o);
					ControlManager.updateChapterMenu();
					
				});


				ControlManager.init(data, $stateParams.itemId, _this.chapters);
				ControlItemManager.idDemande = $stateParams.itemId;
				

				var _nbchapter = data.length;
				//debugger;

				for (var i = 0; i < _nbchapter; i++) {
					var _objdata = data[i];
					var _objchapter = {
						type: "questions"
					};
					_objchapter.label = _objdata.title;
					
					_this.chapters.push(_objchapter);
				}
				
				
				var _indexstart = 0;
				if(Config.debug) _indexstart= 1;
				if(!$stateParams.itemId) _indexstart= 1;
				console.log('$_indexstart : '+_indexstart);
				
				
				_this.selectChapter(_indexstart);
			};



			_this.selectChapter = function(_index) {

				if(_this.page_question.gotoPage != undefined) _this.page_question.gotoPage("questions");
				_this.curchapter = _this.chapters[_index];
				var _labelchapter = _this.curchapter.label;

				_this.subheadertitle = _this.curchapter.label;
				_this.indexchapter = _index;

				for (var i in _this.pageshow) {
					_this.pageshow[i] = false;
				}
				_this.pageshow[_this.curchapter.type] = true;
				console.log("_labelchapter : "+_labelchapter);

				if (_index >= _this.nbstaticchapter) {
					
					var _initskips = [];
					
					//essais routier
					if(_labelchapter == "examchapter_essais_routier"){
						var _engine = ServerService.item["engine"];
						console.log("1-_engine : "+_engine);
						
						if(_engine == "manuelle") _initskips.push("question_essais_routier_9");
						else if(["automatique", "sequentielle"].indexOf(_engine) != -1) _initskips.push("question_essais_routier_8");
					}
					//habitacle arrière
					else if(_labelchapter == "examchapter_hab_back"){
						var _nbsiege = ServerService.item["sieges"];
						console.log("_nbsiege : "+_nbsiege);
						if(_nbsiege == 2) _initskips.push("question_hab_back_1");
					}
					//entretien
					else if(_labelchapter == "examchapter_entretien"){
						var _datecirculation = ServerService.item["premiereimm"];
						console.log("_datecirculation : "+_datecirculation);
						var _tsnow = new Date().getTime() / 1000;
						var _delta = _tsnow - _datecirculation;
						var _yeardelta = _delta / 3600 / 24 / 365;
						console.log("_yeardelta : "+_yeardelta);

						if(_yeardelta < 5) _initskips.push("question_entretien_5");
					}
					//démarrage moteur
					else if(_labelchapter == "examchapter_startengine"){
						var _engine = ServerService.item["engine"];
						console.log("2-_engine : "+_engine);
						
						if(["automatique", "sequentielle"].indexOf(_engine) != -1) _initskips.push("question_startengine_8");
					}
					console.log("_initskips : "+_initskips);
					
					ControlManager.startList(_index - 1, Config.debug_integration, _initskips);
				}


			};



			//________________________________________________________________________________________________________
			//events


			_this.clickMenu = function() {
				console.log("ExamController.clickmenu");
				_this.modalMenu.show();
			};


			_this.onClickChapter = function(_index) {

				if (!Config.debug_integration && _index > 0 && !ControlManager.isChapterComplete(_index - 1)) {
					console.log("not done yet");
					return;
				}
				if (_this.indexchapter != _index) _this.selectChapter(_index);
				_this.modalMenu.hide();
			};

			_this.onCloseMenuChapter = function() {
				_this.modalMenu.hide();
			};


			_this.onPageInfoNext = function() {
				console.log("ExamController.onPageInfoNext");
				
				_this.pageshow.info = false;
				_this.pageshow.questions = true;
				
				_this.onChapterComplete();

			};


			
			
			_this.onChapterComplete = function() {
				console.log("ExamController.onChapterComplete");

				var _nbchapter = _this.chapters.length;
				console.log("_nbchapter : " + _nbchapter);

				var _firstchapteruncomplete = _this.getFirstChapterUncomplete();
				console.log("_firstchapteruncomplete : " + _firstchapteruncomplete);



				if (_firstchapteruncomplete != -1) {
					_this.indexchapter = _firstchapteruncomplete;
					_this.selectChapter(_this.indexchapter);
					_this.validateAll(true);
				} else {
					console.log('finished all chapters');
					
					_this.page_question.gotoPage("end");
					
					
				}

			};
			ControlManager.setChapterCompleteHandler(_this.onChapterComplete);
			
			
			
			_this.onFinishExam = function()
			{
				console.log("ExamComponent.onFinishExam");
				
				_this.validateAll(false);
				ItemStorage.deleteLocalItem(_this.itemId);
				
			};
			$scope.$on("FINISH_EXAM", _this.onFinishExam);





			_this.getFirstChapterUncomplete = function() {

				console.log("getFirstChapterUncomplete");

				var _len = _this.chapters.length;
				//debugger;
				for (var i = 0; i < _len; i++) {

					if(i == 0 || !ControlManager.isChapterSkipped(i - 1)){
						if (!_this.chapters[i].done) return i;
					}
					else{
						console.log("skip chapter "+i);
						ControlManager.setChapterSkipped(i - 1, true);
					}
					
				}

				return -1;
			};
			
			
			
			
			
			_this.validateAll = function(_background) {
				
				console.log("validateAll " + _this.itemId);
				
				if(!_this.itemId){
					if(!_background) $state.go("home");
					return;
				}
				
				
				var _data = ItemStorage.getLocalItem(_this.itemId, true);
				//console.log("_data : "+_data);

				var _args = {
					id: _this.itemId,
					type: "data_exam",
					value: _data,
					"complete" : (_background) ? "0" : "1"
				};

				if (!_background) _this.page_question.showLoader(true);

				ServerService.updateDetail(_args).then(function() {

					console.log("update ok");

					if (!_background) {
						_this.page_question.showLoader(false);
						$state.go("home");
					}

				}, function(err) {
					if (!_background) {
						_this.page_question.showLoader(false);
						_this.page_question.setError(err);
					}

				});



			};


			//____________________________________________________
			//init


			console.log("ExamController " + $stateParams.itemId);
			_this.headertitle = "titles_exam";
			_this.curchapter = null;
			_this.indexchapter;
			_this.pageshow = {
				"info": false,
				questions: false
			};
			_this.nbstaticchapter = 1;

			_this.chapters = [];
			_this.chapters.push({
				type: "info",
				label: "examchapter_infovendeur",
				done: true
			});



			$ionicModal.fromTemplateUrl('js/exam/chapter-menu-modal.html', {
				scope: $scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				_this.modalMenu = modal;

			});


			//utilisé par page info pour l'instant
			$scope.$on("PAGE_INFO_NEXT", _this.onPageInfoNext);


			//chargement des données (online ou en cookie)

			var _datacookie;
			if($stateParams.itemId) _datacookie = ItemStorage.getLocalItem($stateParams.itemId);
			
			_this.itemId = $stateParams.itemId;
			console.log('_datacookie: '+_datacookie);

			if (!Config.debug && _datacookie != undefined) {
				console.log("ok set " + $stateParams.itemId);
				_this.initData(_datacookie);
				ControlManager.updateChapterMenu();

			} else {
				ControlManager.loadQuestions().then(function(data) {
					//debugger;
					_this.initData(data);
					ControlManager.updateChapterMenu(-1);
					ItemStorage.setLocalItem($stateParams.itemId, data);

				}, function() {

				});
			}



			if (Config.debug) {
				var element = angular.element(window);
				element.bind("keydown keypress", function(event) {

					console.log("keydown ");
					if (event.keyCode == 37 || event.keyCode == 39) {
						var _bnext = (event.keyCode == 39);
						var _delta = (_bnext) ? 1 : -1;
						ControlManager.navigateQuestion(_delta);
						//debugger;
					}
				});
			}

			if (Config.debug_client) {
				$scope.$on('clickDebugExam', function (event, _delta) {
					ControlManager.navigateQuestion(_delta);

				});
			}



		}
	]
});