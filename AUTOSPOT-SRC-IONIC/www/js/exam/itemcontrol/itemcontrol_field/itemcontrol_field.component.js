angular.
module('starter').
component('itemControlField', {

	templateUrl: "js/exam/itemcontrol/itemcontrol_field/itemcontrol_field.template.html",
	//template: '<div ng-include="$ctrl.templateUrl">',

	bindings: {
		text: '@',
		type: '@',
		iditem: '@',
		idparent: '@',
		customstyle: '@',
		so: '=',
		dataobj: '='/*,
		templateUrl: '@'*/
	},

	controller: ["$scope", "ServerService",
		function ItemControlField($scope, ServerService) {

			//console.log("ItemControlField");
			var _this = this;

			_this.showError = false;
			_this.msgError = "";

			_this.pictures = [];
			_this.dynvalue = "";



			//inputs
			//console.log("iditem : " + _this.iditem + ", type : "+_this.type + ", text : "+_this.text+", _this.dataobj.dyndata : " + _this.dataobj.dyndata);

			if (_this.dataobj.dyndata != undefined) {
				_this.dynvalue = ServerService.item[_this.dataobj.dyndata];
				console.log("_this.dynvalue : " + _this.dynvalue);
				_this.data = _this.dynvalue;
			}


			if (_this.so.data != undefined) {
				_this.data = _this.so.data;
			}
			if (_this.so.pictures != undefined) {
				_this.pictures = _this.so.pictures;
			}



			this.so.testfunction = function() {
				console.log("_testfunction " + _this.iditem);
			};

			this.so.setVisible = function(_value) {
				//_this.visible = (_value) ? 1 : 0;
				_this.visible = _value;
			};



			this.so.getData = function() {
				//tested on checkbox
				return _this.data;
			};

			this.so.setData = function(_value) {
				_this.data = _value;
			};



			this.so.displayError = function(_value, _msg) {
				_this.showError = _value;
				if (_value) _this.msgError = _msg;
			};


			this.so.addPicture = function(_url) {
				console.log("item.addPicture(" + _url + ")");
				_this.pictures.push(_url);
			};

			this.so.getPictures = function() {
				return _this.pictures;
			};



			this.onclick = function($event) {
				console.log("field.onclick " + _this.data);

				$scope.$emit("clickitem", _this.dataobj);


			};

			/*
			this.onchange = function($event)
			{
				console.log("onchange");
			};
			*/
		
			/*
			this.$onChanges = (changes) => {
				if (changes.templateUrl && this.templateUrl) {
					//this.templateUrl = this.type + '.html';
					console.log("templateUrl change");
				}
			}
			*/
			
		}
	]
});