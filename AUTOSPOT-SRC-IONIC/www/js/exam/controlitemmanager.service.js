angular.module('starter')
	.service('ControlItemManager', ["$q", "$window", "$compile", "CameraService", "FileUploadService", '$ionicPopup', '$translate',
		function($q, $window, $compile, CameraService, FileUploadService, $ionicPopup, $translate) {

			var _this = this;
			this.scope = null;
			this.listscope = null;
			this.levelrec;
			this.prefixdiv = "cont_";
			this.iditeminit = "0";
			this.idDemande;

			this.listErrors;
			this.handlerComplete = null;
			this.questionSkip;
			this.questionSkip_c;
			this.skipindexes_o = [];



			this.setHandlerComplete = function(_handler) {
				_this.handlerComplete = _handler;
			};

			this.handleQuestion = function(_data, _islast) {
				//debugger;

				_this.scope.data = _data;
				_this.levelrec = 0;


				_this.scope.showLoader2 = true;

				var _idcontainer = "container_questions";

				//reset dom
				var _dom = document.getElementById(_idcontainer);
				angular.element(_dom).empty();

				_this.scope.radiodata = {};
				_this.scope.cbdata = {};
				
				
				_this.reset();

				_this.rec_browserData(_data, null, _this.levelrec, _this.iditeminit, 0, _this.constructItem);

				_this.rec_browserData(_data, null, _this.levelrec, _this.iditeminit, 0, _this.initItem);

				$window.setTimeout(function() {

					_this.scope.$apply(function() {
						_this.scope.showLoader2 = false;
					});
				}, 100);


				if (!_islast) _this.scope.submit_label = "exam_next";
				else _this.scope.submit_label = "exam_valid_final";


				//debugger;


			};



			this.init = function(_scope) {
				_this.scope = _scope;
				_this.scope.showLoader2 = false;
				_this.scope.$on("clickitem", _this.onClickItem);
			};
			
			this.reset = function()
			{
				if(_this.listscope == null) return;
				/*
				console.log("reset");
				console.log("_this.listscope.length : "+_this.listscope.length);
				*/
				
				var _len = _this.listscope.length;
				for(var i=0; i<_len; i++){
					var sc = _this.listscope[i];
					sc.$destroy();
				}
				
				_this.listscope = new Array();
			}



			this.rec_browserData = function(_object, _objectParent, _levelrec, _iditem, _index, _func, _islast) {
				/*
				var _str = "";
				for (var i = 0; i < _levelrec; i++) _str += "  ";
				*/



				var _idparent = (_objectParent != null) ? _objectParent.id : "";
				//console.log(_str + " -- " + _idparent + " // " + _iditem);

				if (_islast == undefined) _islast = false;

				var _continueBrowsing = _func(_object, _objectParent, _iditem, _levelrec, _index, _islast);

				_idparent = _iditem;

				var _nextlevel = _levelrec;
				if (_object.type != "label") _nextlevel++;

				if (_object.items != undefined && _continueBrowsing) {
					var _len = _object.items.length;
					var _counter = 0;

					for (var i = 0; i < _len; i++) {

						var _obj = _object.items[i];

						var _counterhex = _counter.toString(16);
						var _iditem = _idparent + _counterhex;
						var _islast = (i == _len - 1);

						_this.rec_browserData(_obj, _object, _nextlevel, _iditem, i, _func, _islast);
						_counter++;

					}
				}
			};



			//_________________________________________________________________________________________________________


			this.constructItem = function(_obj, _objparent, _iditem, _levelrec, _index, _islast) {

				_obj.so = {};
				_obj.id = _iditem;
				_obj.parent = _objparent;
				_obj.ind = _index;

				if (_obj.req == undefined) _obj.req = true;

				if (_objparent == null) _idcontainerparent = "container_questions";
				else _idcontainerparent = _this.prefixdiv + _objparent.id;

				var _idparent = (_objparent == null) ? "" : _objparent.id;

				var _idcontainer = _this.prefixdiv + _iditem;

				var _dom = document.getElementById(_idcontainerparent);

				var _text = _obj.text;


				var _display = (_this.skipindexes_o.indexOf(_text) == -1);
				//console.log("display : "+_display+", "+_this.skipindexes_o);

				//_text = _text.replace("todo:", "");
				if(_text.substr(0, 1) == "*") _text = "";

				var _type = _obj.type;


				_text = _text.replace(/'/g, "&#39;");
						
				if(_type == "label"){
					console.log("replace : "+_text);
					_text = _text.replace(/(?:\r\n|\r|\n)/g, '<br />');
				}

				var _stritem = "data";
				var _strlen = _iditem.length;
				for (var i = 1; i < _strlen; i++) {
					var _char = _iditem.charAt(i);
					var _indexsearch = parseInt("0x"+_char);
					_stritem += ".items[" + _indexsearch + "]";
					//console.log("_indexsearch : "+_indexsearch+", _stritem : "+_stritem);
				}
				var _so = _stritem + ".so";


				var _margintop = (_index == 0) ? 20 : 0; //0 if collé, more if espacées
				var _marginbottom = (_islast) ? 20 : 0;

				var _levelindent = _levelrec;
				var _styleinner = "margin-left:" + (_levelindent * 35) + "px;";

				var _stylediv = "";
				_stylediv += "margin-top:" + _margintop + "px;";
				_stylediv += "margin-bottom:" + _marginbottom + "px;";
				
				var _stylediv2 = "";
				if(!_display) _stylediv2 += "display:none;";
				
				//console.log("_stylediv : "+_stylediv);


				var _isvisible;
				if (_objparent != null && (_objparent.type == "radio" || _objparent.type == "checkbox")) _isvisible = false;
				else _isvisible = true;




				//set previous values

				if (_obj.value != undefined) {

					if (_type == "checkbox") {
						_obj.so.data = _obj.value;

					} else if (_type == "text" || _type == "number") {
						_obj.so.data = _obj.value;

					} else if (_type == "picture") {
						_obj.so.pictures = _obj.value;
					}
				}



				if (_obj.visibility == undefined || _obj.visibility == "app") {
					var _html = "";
					
					var _typetemplate = _type;
					var _templateUrl = "js/exam/itemcontrol/itemcontrol_field/sub/itemcontrol_field-"+_typetemplate+".template.html";
					
					_html += "<div id='" + _idcontainer + "' style='" + _stylediv + "'>";
					_html += "<item-control-field text='" + _text + "' type='" + _type + "' iditem='" + _iditem + "'";
					_html += " idparent='" + _idparent + "' so='" + _so + "' dataobj='" + _stritem + "' customstyle='" + _styleinner + "'";
					_html += " template-url='"+_templateUrl+"'";
					_html += " style='" + _stylediv2 + "'";

					_html += ">";
					_html += "</item-control-field>";
					_html += "</div>";
					
					
					
					
					if(_this.listscope == null){
						_this.listscope = new Array();
					}
					var newscope = _this.scope.$new();
					_this.listscope.push(newscope);
					
					var compiledElement = $compile(_html)(newscope);
					angular.element(_dom).append(compiledElement);
					
					/*
					newscope.$on("$destroy", function(){
						console.log("newscope.destroy");
					});
					*/
				}



				//console.log("_so : " + _obj.so);

				return true;
			};



			this.initItem = function(_obj, _objparent, _iditem, _levelrec, _index, _islast) {
				//console.log("initItem(" + _iditem + ")");
				_this.scope.data;

				if (_objparent != null) {

					//set previous values
					if (_obj.type == "radio" && _obj.value) {
						_this.scope.radiodata[_objparent.id] = _index;
					}


					//open / close nodes

					if ((_objparent.type == "radio" || _objparent.type == "checkbox") && !_objparent.value) {
						_this.setItemVisible(_obj, false);
					}


				}

				return true;
			};



			this.validItem = function(_obj, _objparent, _iditem, _levelrec, _index, _islast) {

				var _continueBrowsing = true;
				_obj.text;

				if (_obj.type == "radio" || _obj.type == "checkbox" || _obj.type == "text" || _obj.type == "number" || _obj.type == "picture") {

					var _value = "";

					if (_obj.type == "radio") {

						//if unselected
						if (_this.scope.radiodata == undefined || _this.scope.radiodata[_objparent.id] == undefined) {
							//debugger;
							var _id = _objparent.id;

							//filter to do it once only
							if (_this.listErrors.indexOf(_id) == -1) {

								_this.listErrors.push(_id);
								console.log("radio undefined " + _id);

								_obj.so.displayError(true, "error_field_radio");

							}
							//else if selected
						} else {
							_obj.so.displayError(false);
						}


						if (_this.scope.radiodata == undefined) {
							_value = false;
						} else {
							_value = (_this.scope.radiodata[_objparent.id] == _index);
						}

						if (!_value) _continueBrowsing = _value;

					} else if (_obj.type == "checkbox") {
						var _data = _obj.so.getData();
						if (_data == undefined) _value = false;
						else _value = _data;

						if (_value) {
							_this.scope.cbdata[_objparent.id] = _index;
						}

						if (_islast) {

							var _firstso = _objparent.items[0].so;
							if (_firstso != undefined) {
								if (_this.scope.cbdata[_objparent.id] == undefined && _objparent.req) {
									_this.listErrors.push(_id);
									console.log("error cb");
									_firstso.displayError(true, "error_field_radio");

								} else _firstso.displayError(false);
							}


						}



						//error (select at least one)


						if (!_value) _continueBrowsing = _value;

					} else if (_obj.type == "text" || _obj.type == "number") {

						//cas du non visible
						if (_obj.so.getData == undefined) {
							if (_obj.dyncalculation != undefined) {
								console.log("dyncalculation todo " + _obj.dyncalculation);

								var _tabindex = _obj.dyncalculation.match(/\$(\d)+/g);
								_tabindex = _tabindex.map(function(_str) {
									var _index = parseFloat(_str.substr(1, _str.length - 1));
									var _value = parseFloat(_objparent.items[_index].value);
									return _value;
								});

								var _index = 0.
								var _dyncalculation = _obj.dyncalculation.replace(/\$(\d)+/g, function(x) {
									var _output = _tabindex[_index];
									_index++;
									return _output;
								});
								_value = eval(_dyncalculation);

							}

						} else {
							_value = _obj.so.getData();
							
							if (_value == undefined) _value = "";
							
							if (_obj.req) {
								if (_value === "") {
									_this.listErrors.push(_obj.id);

									_obj.so.displayError(true, "error_field_text");
								} else _obj.so.displayError(false);
							}
						}



					} else if (_obj.type == "picture") {

						_value = _obj.so.getPictures();
						if (_value.length == 0 && _obj.req) {

							_this.listErrors.push(_obj.id);
							var _msgerror = "form_fill_all";
							_obj.so.displayError(true, "error_field_picture");
						} else _obj.so.displayError(false);


					}


					_obj.value = _value;


					//skip
					//todo : ptet add checkbox

					if (_value && _obj.type == "radio") {
						//console.log("text : " + _obj.text + ", skip : " + _obj["skip"]);
						if (_obj["skip"] != undefined) {
							_this.questionSkip = _obj["skip"];
						}
						if (_obj["skip_c"] != undefined) {
							_this.questionSkip_c = _obj["skip_c"];
						}
						if (_obj["skip_o"] != undefined) {
							for(var _k in _obj["skip_o"]) _this.skipindexes_o.push(_obj["skip_o"][_k]);
							console.log("add skipindexes_o : "+_this.skipindexes_o);
						}
					}


				}

				return _continueBrowsing;

			};





			this.valid = function() {
				console.log("ControlItemManager.valid()");
				_this.scope.data;
				var translations;
				
				$translate(["ec04xk5i", "examglobal_confirm_desc_unavailable", "examglobal_cancel", "examglobal_confirm",
					_this.scope.data.confirm_empty,
					]).then(function (t) {
					return t;
				})
				.then(function (t) {
					
					translations = t;
					
					var titleConfirm = "";
					var showConfirm = false;
					if(_this.scope.data.confirm){
						showConfirm = true;
						titleConfirm = t.ec04xk5i;
					}
					
					if(showConfirm){
						return $ionicPopup.confirm({
							title: titleConfirm,
							template: "",
							okText: t.examglobal_confirm,
							cancelText: t.examglobal_cancel,
							okType: "button-calm",
							cssClass: "popup_confirm"
						});
					}
					return true;
					
				})
				.then(function (res){
					console.log("res : "+res);

					if(res){
						_this.listErrors = [];
						_this.questionSkip = null;
						_this.questionSkip_c = null;

						_this.rec_browserData(_this.scope.data, null, _this.levelrec, _this.iditeminit, 0, _this.validItem);
						/* 
						_this.scope.data;
						debugger;
						 */
						//confirm_empty
						
						var titleConfirm = "";
						var showConfirm = false;
						
						if(_this.scope.data.confirm_empty != undefined){
							var title = _this.scope.data.confirm_empty;
							
							//check at least one item checked
							//postulat : item = checkbox
							//(pas générique, faudra checker au cas par cas)
							var checked = false;
							var items = _this.scope.data.items;
							for(var k in items){
								var item = items[k];
								if(item.value) checked = true;
							}
							console.log('checked : '+checked);
							
							//check if empty
							if(!checked){
								titleConfirm = translations[title];
								showConfirm = true;
							}
						}
						
						if(showConfirm){
							return $ionicPopup.confirm({
								title: titleConfirm,
								template: "",
								okText: translations.examglobal_confirm,
								cancelText: translations.examglobal_cancel,
								okType: "button-calm",
								cssClass: "popup_confirm"
							});
						}
						return true;
						
					}
				})
				.then(function (res){
					
					if(res){
						var _nberrors = _this.listErrors.length;
						var _isvalid = (_nberrors == 0);
						_this.scope.data.valid = _isvalid;
						
						_this.handlerComplete(_isvalid, _this.questionSkip, _this.questionSkip_c);
					}
				});
				
				
				
			};



			this.onClickItem = function($event, _data) {
				//console.log("onclickitem");

				if (_data.type == "radio") {

					var _objparent = _data.parent;
					for (var i in _objparent.items) {

						var _obj = _objparent.items[i];
						var _visible = (_obj.id == _data.id);
						//console.log("_visible : " + _visible);
						_this.setChildrenVisible(_obj, _visible);


					}
				} else if (_data.type == "checkbox") {
					console.log("todo open checkbox");

					var _selected = _data.so.getData();
					if (_selected == undefined) _selected = false;

					_this.setChildrenVisible(_data, _selected);

				} else if (_data.type == "picture") {

					console.log("todo picture");
					//debugger;

					var _filename = new Date().getTime() + '-' + _this.guid() + ".jpg";

					CameraService.getPicture({
							targetWidth: 800,
							correctOrientation: true
						})
						.then(function(_dataURI) {
							console.log("CameraService success");
							
							console.log('_this.idDemande : '+_this.idDemande);
							if(_this.idDemande){
								FileUploadService.upload(_dataURI, _filename);
							}
							else console.log('no upload, idDemande undefined');
							
							_data.so.addPicture(_filename);
						})
						.catch(function(err) {
							console.log("CameraService err : " + err);
						});

				}

			};



			this.setSkipIndexes_o = function(_data)
			{
				_this.skipindexes_o = _data;
			};



			this.setChildrenVisible = function(_obj, _value) {

				for (var i in _obj.items) {
					var o = _obj.items[i];
					_this.setItemVisible(o, _value);

				}
			};


			this.setItemVisible = function(_item, _value) {

				var _prop = (_value) ? "block" : "none";
				var _idcontainer = _this.prefixdiv + _item.id;
				var _dom = document.getElementById(_idcontainer);
				angular.element(_dom).css({
					'display': _prop
				});
			};


			this.guid = function() {
				function s4() {
					return Math.floor((1 + Math.random()) * 0x10000)
						.toString(16)
						.substring(1);
				}
				return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
					s4() + '-' + s4() + s4() + s4();
			};


		}
	])