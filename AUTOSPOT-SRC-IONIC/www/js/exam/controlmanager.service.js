angular.module('starter')
	.service('ControlManager', ["$q", "$http", "Config", "$window", "ControlItemManager", "ItemStorage", "$ionicScrollDelegate", 'Config',
		function($q, $http, Config, $window, ControlItemManager, ItemStorage, $ionicScrollDelegate, Config) {

			var _this = this;
			_this.data = null;
			_this.curchapter = null;
			_this.indexQuestion;
			_this.indexChapter;
			_this.idDemande;
			_this.listchapters;
			_this.skipindexes = [];
			_this.skipindexes_c = [];
			_this.debugQuestion = false;


			this.init = function(_data, _iddemande, _listchapters) {

				console.log("init");
				_this.data = _data;
				_this.idDemande = _iddemande;
				_this.listchapters = _listchapters;
				
			};


			this.set_initskips_c = function(_data){
				_this.skipindexes_c = _data;
			};

			this.set_initskips_o = function(_data){
				ControlItemManager.setSkipIndexes_o(_data);
			};
			

			
			this.startList = function(_index, _debugmode, _initskips) {
				console.log("handleList");
				//console.log("data : " + _this.data);

				_this.skipindexes = _initskips;

				_this.indexChapter = _index;
				_this.indexQuestion = 0;

				
				if(!_this.debugQuestion && Config.question_debug != ""){
					_this.debugQuestion = true;
					outer_loop:
					for(var i in _this.data){
						var _chap = _this.data[i];
						for(var j in _chap.questions){
							var q = _chap.questions[j];
							var _title = q.text;
							console.log('if '+_title+' : '+Config.question_debug);
							if(_title == Config.question_debug){
								_this.indexChapter = +i;

								_this.indexQuestion = +j;
								break outer_loop;	
							}
						}
					}
					
					if(Config.question_debug == 'complete'){
						this.indexChapter = _this.data.length - 1;
						this.indexQuestion = _chap.questions.length - 1;
					}

				}
				
				console.log('indexchapter : '+_this.indexChapter+', '+_this.indexQuestion);
				

				_this.curchapter = _this.data[_this.indexChapter];
				var _listquestions = _this.curchapter.questions;

				
				if (_debugmode) _this.indexQuestion = _listquestions.length - 1;

				ControlItemManager.handleQuestion(_listquestions[_this.indexQuestion]);
				//debugger;

				$ionicScrollDelegate.scrollTop();

			};



			this.setChapterCompleteHandler = function(_handler) {
				_this.handlerChapterComplete = _handler;
			};


			this.isChapterComplete = function(_index) {
				return _this.listchapters[_index].done;
			};




			this.updateChapterMenu = function(_index) {
				console.log("ControlManager.updateChapterMenu(" + _index + ")");
				console.log(_this.skipindexes_c);

				_this.listchapters[0].visible = true;
				var _nbchapter = _this.data.length;
				for (var i = 0; i < _nbchapter; i++) {

					var _id = _this.data[i].id;
					var _visible = (_this.skipindexes_c.indexOf(_id) == -1);
					_this.listchapters[i + 1].visible = _visible;
					
					var _objchapter = _this.data[i];
					var _listquestions = _objchapter.questions;
					var _len = _listquestions.length;
					var _complete = true;

					if (_index == -1) _complete = false;
					else {

						if (_index == undefined || _index == i) {
							for (var j = 0; j < _len; j++) {

								var _objquestion = _listquestions[j];
								if (!_objquestion.valid) {
									_complete = false;
									break;
								}
							}
						}
					}

					if (Config.debug){
						_complete = i <= _index;
						_this.listchapters[i + 1].done =  _complete;
					}

					if (_index == undefined || _index == -1 || _index == i) {
						_this.listchapters[i + 1].done = _complete;

						//if(Config.debug) _this.listchapters[i + 1].done = (i <= _this.indexChapter);
						//indexchapter est figé sur le premier
						//ce qui faut concretement, c'est que tous les chapitres avant celui testé soit considéré rempli
					}

				}

				//debugger;

			};





			this.isChapterSkipped = function(_index)
			{
				var _id = _this.data[_index].id;
				//console.log("isChapterSkipped "+_index+", "+_id);
				return (_this.skipindexes_c.indexOf(_id) != -1);
			};


			this.setChapterSkipped = function(_index, _value)
			{
				var _obj = _this.data[_index];
				_obj["sk_c"] = _value ? 1 : 0;
				
			};


			this.loadQuestions = function() {

				var deferred = $q.defer();
				var promise = deferred.promise;
				// var _baseurl = (Config.online) ? Config.base_url_online : Config.base_url_local;
				var _baseurl = Config.getBaseUrl();

				var _params = {};

				$http({
						method: 'GET',
						url: _baseurl + 'load_questions.php',
						params: _params

					})
					.then(function successCallback(response) {
						console.log("loadQuestions.successCallback");
						deferred.resolve(response.data);

					}, function errorCallback(response) {
						console.log("errorCallback");
						deferred.reject('error_servor');

					});


				return promise;
			};



			this.onQuestionComplete = function(_isvalid, _questionskip, _questionskip_c) {
				console.log("ControlManager.onQuestionComplete " + _isvalid);
				
				

				if (_questionskip != null) {
					for (var i in _questionskip){
						//questionskip contient des id (text du noeud racine)
						_this.skipindexes.push(_questionskip[i]);
					}
				}

				if(_questionskip_c != null){
					for (var i in _questionskip_c){
						//questionskip contient des id (text du noeud racine)
						_this.skipindexes_c.push(_questionskip_c[i]);
					}
				}


				console.log("_this.skipindexes : " + _this.skipindexes);
				console.log("_this.skipindexes_c : " + _this.skipindexes_c);
				console.log("_this.indexQuestion : " + _this.indexQuestion);


				if (_isvalid) {

					var _nbquestion = _this.curchapter.questions.length;
					var _listquestions = _this.curchapter.questions;
					//_listquestions[_this.indexQuestion].customprop = "test";	//ça, ça passe
					
					_this.indexQuestion++;

					var _idquestion = "";	//text from racine node
					if(_listquestions[_this.indexQuestion] != undefined) _idquestion = _listquestions[_this.indexQuestion].text;
					
					//check skip
					while (_this.skipindexes.indexOf(_idquestion) != -1) {
						console.log("- skip index");
						//debugger;
						//set complete
						if(_listquestions[_this.indexQuestion] != undefined){

							//console.log("  (set prop sk "+_this.indexQuestion+")");
							_listquestions[_this.indexQuestion].valid = true;
							_listquestions[_this.indexQuestion].sk = 1;
						}

						_this.indexQuestion++;
						
						if(_listquestions[_this.indexQuestion] != undefined) _idquestion = _listquestions[_this.indexQuestion].text;
						else _idquestion = "";


					}
					
					
					
					
					//save cookie (déplacé)
					ItemStorage.setLocalItem(_this.idDemande, _this.data);
					
					_this.updateChapterMenu(_this.indexChapter);
					
					console.log("_this.indexQuestion : "+_this.indexQuestion);

					if (_this.indexQuestion < _nbquestion) {
						
						console.log("chapter not finished");
						_listquestions[_this.indexQuestion].sk = 0;
						
						//just for button label

						var _islast = false;
						var _nbchapter = _this.listchapters.length - 1;
						if (_this.indexChapter == _nbchapter - 1) {
							console.log("last chapter");
							if (_this.indexQuestion == _listquestions.length - 1) {
								console.log("last question");
								_islast = true;
							}
						}

						ControlItemManager.handleQuestion(_listquestions[_this.indexQuestion], _islast);

					} else {
						console.log("chapter finished");
						_this.handlerChapterComplete();

						//ajouter une puce "done"
						//attention, faut aussi que ça marche pour previous cookie
						//plutot une methode qui synchronise le menu en fonction de l'objet
						//debugger;
					}

					$ionicScrollDelegate.scrollTop();

				}

			};

			ControlItemManager.setHandlerComplete(_this.onQuestionComplete);


			this.navigateQuestion = function(_delta) {
				var _nbquestion = _this.curchapter.questions.length;
				var _newindex = _this.indexQuestion + _delta;
				if (_newindex >= 0 && _newindex <= _nbquestion - 1) {

					_this.indexQuestion = _newindex;
					var _listquestions = _this.curchapter.questions;
					ControlItemManager.handleQuestion(_listquestions[_this.indexQuestion]);
				}
			};


		}
	])