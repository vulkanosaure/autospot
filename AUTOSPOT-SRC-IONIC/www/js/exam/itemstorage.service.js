angular.module('starter')
	.service('ItemStorage', ['$window',
		function($window) {
				
				var _this = this;
				var _keyLocalStorage = "demandes_";
				
				this.getLocalItem = function(_id, _keepjson)
				{
					if(_keepjson == undefined) _keepjson = false;
					var _key = _keyLocalStorage + _id;
					
					if($window.localStorage[_key] == undefined){
						
						return undefined;
					}
					
					var _objjson = $window.localStorage[_key];
					var _obj;
					if(_keepjson) _obj = _objjson;
					else _obj = angular.fromJson(_objjson);
					
					return _obj;
					
				};
				
				
				this.setLocalItem = function(_id, _data)
				{
					var _key = _keyLocalStorage + _id;
					
					var _json = _this.stringify(_data);
					
					$window.localStorage[_key] = _json;
					
				};
				
				this.deleteLocalItem = function(_id)
				{
					var _key = _keyLocalStorage + _id;
					$window.localStorage.removeItem(_key);
				};
				
				
				
				this.stringify = function(_data)
				{
					var _keytoremoves = ["parent", "id", "ind", "so"];
					
					return JSON.stringify(_data, function (_key, _value){
						
						if(_keytoremoves.indexOf(_key) != -1) return;
						return _value;
											
					});
				};
				
				
				
				
		}
	])