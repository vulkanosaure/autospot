angular.
	module('starter').
	component('detail', {

		templateUrl: "js/detail/detail.template.html",

		controller: ["$scope", "$stateParams", "ionicDatePicker", 'ionicTimePicker', '$state', 'ServerService', '$ionicPopup', '$translate', function DetailController($scope, $stateParams, ionicDatePicker, ionicTimePicker, $state, ServerService, $ionicPopup, $translate) {

			console.log("DetailController");
			var _this = this;
			_this.itemId = $stateParams.itemId;
			_this.data = null;
			_this.mytext = "value of mytext";

			_this.dtpicker_so = [];
			_this.dtpicker_so[0] = {};
			_this.dtpicker_so[1] = {};
			_this.dtpicker_so[2] = {};
			_this.dtpicker_so[3] = {};

			_this.error_msg_time1 = "";
			_this.error_msg_time2 = "";
			_this.error_msg_unavailable = "";
			_this.valid_msg = ["", "", ""];

			_this.disable_btns = [false, false, false];

			_this.headertitle = "titles_detail";
			_this.showbtnback = true;

			$scope.$on('clickback', function (event, data) {
				$state.go("home");
			});
			



			_this.timeset = false;



			/*
			setTimeout(function(){
				$scope.$apply(function(){
					_this.showLoader = false;
				});
			}, 2000);
			*/



			_this.load = function () {
				_this.msg_error_connexion = "";
				_this.error_connexion = false;
				_this.showLoader = true;

				ServerService.getItem(_this.itemId).then(function (_data) {
					console.log("ListingController.ok");
					_this.data = _data;
					_this.showLoader = false;
					_this.processData(_data);

				}, function (_data) {
					console.log("ListingController.error");
					_this.showLoader = false;
					_this.msg_error_connexion = "Problème de connexion, veuillez rééssayer";
					_this.error_connexion = true;

				});
			};

			_this.retry = function () {
				_this.load();
			};



			_this.load();


			$scope.clickBack = function () {
				console.log("click back");
				$state.go("home");
			};


			_this.processData = function (_data) {
				console.log("pressdata");

				_this.timeset = (_data.ct_ts != false || _data.ct_ts2 != false);
				console.log("_this.timeset : " + _this.timeset);

				_data.present_str = _data.present ? "examglobal_yes" : "examglobal_no";


				//unavability

				if (_data.status == "") {
					_this.unavailable_checked = false;
				} else {
					_this.unavailable_checked = true;
					if (_data.status == "vehicule_sold") _this.unavailable_reason = 1;
					else if (_data.status == "seller_absent") _this.unavailable_reason = 2;
				}


				_this.dtpicker_so[0].setEditable(!_this.timeset);
				_this.dtpicker_so[1].setEditable(!_this.timeset);

				_this.dtpicker_so[0].setData(_data.ct_ts * 1000);
				_this.dtpicker_so[1].setData(_data.ct_ts * 1000);

				_this.dtpicker_so[2].setData(_data.ct_ts2 * 1000);
				_this.dtpicker_so[3].setData(_data.ct_ts2 * 1000);



				if (_data.ct_ts2) {
					_this.checked2 = true;
				}


			};



			//____________________________________________________
			//events


			_this.onClickUnavailable = function (event) {
				console.log("onClickUnavailable " + event.target.checked);


				if (event.target.checked) {
					_this.unavailable_reason = 1;
				} else {
					_this.unavailable_reason = false;
				}


			};

			_this.onClickReason = function (event) {
				console.log("onClickReason ");
				_this.unavailable_checked = true;
			};





			_this.submit_unavailable = function () {
				console.log(_this.unavailable_reason);


				$translate(["examglobal_confirm_title", "examglobal_confirm_desc_unavailable", "examglobal_cancel", "examglobal_confirm"]).then(function (t) {
					return t;
				})
					.then(function (t) {

						_this.error_msg_unavailable = "";
						if (!_this.unavailable_reason) {
							throw new Error("error_mustcheck");
						}

						return $ionicPopup.confirm({
							title: t.examglobal_confirm_title,
							template: t.examglobal_confirm_desc_unavailable,
							okText: t.examglobal_confirm,
							cancelText: t.examglobal_cancel,
							okType: "button-calm",
							cssClass: "popup_confirm"
						});
					})
					.then(function (res) {

						//if clicked confirm
						if (res) {
							console.log("clicked yes");

							_this.valid_msg[2] = "";
							_this.disable_btns[2] = true;

							var _value = _this.unavailable_reason;
							if (!_this.unavailable_reason) _value = "0";

							var _args = {
								id: _this.itemId,
								type: "unavailable",
								value: _value
							};

							return ServerService.updateDetail(_args);
						}
						else {
							throw new Error("canceled");
						}
					})
					.then(function () {
						console.log("go home");
						_this.valid_msg[2] = "form_saved";
						$state.go("home");
					})
					.catch(function (error) {
						console.log("catch, " + error.message);
						if (error.message != "canceled") {
							_this.error_msg_unavailable = error.message;
						}
					})
					.then(function () {
						console.log("then final (hide loading)");
						_this.updateComplete(2);
					});


			};



			_this.submit_date = function (_index) {

				console.log("submit_date " + _index);

				var _indexstart = _index * 2;

				var _tsdate = _this.dtpicker_so[_indexstart + 0].getOutput();
				var _tstime = _this.dtpicker_so[_indexstart + 1].getOutput();

				$translate(["examglobal_confirm_title", "examglobal_confirm_desc_settime", "examglobal_cancel", "examglobal_confirm"]).then(function (t) {
					return t;
				}).then(function (t) {

					if (_index == 0) _this.error_msg_time1 = "";
					else _this.error_msg_time2 = "";

					_this.valid_msg[_index] = "";

					if (_tsdate == 0 || _tstime == 0) {
						//error
						throw new Error("error_datetime");
					}
					else {
						if (_index == 1) {
							return $ionicPopup.confirm({
								title: t.examglobal_confirm_title,
								template: t.examglobal_confirm_desc_settime,
								okText: t.examglobal_confirm,
								cancelText: t.examglobal_cancel,
								okType: "button-calm",
								cssClass: "popup_confirm"
							});
						}
						else{
							return true;
						}


					}

				}).then(function (res) {

					if (res) {
						//validate
						console.log("res yes");
						if (_index == 1) _this.checked2 = true;
						var _ts = _tsdate + _tstime;
						_errormsg = "";
						_this.disable_btns[_index] = true;

						var _args = {
							id: _this.itemId,
							type: "time" + _index,
							value: _ts
						};
						return ServerService.updateDetail(_args);
					}
					else throw new Error("canceled");

				}).then(function () {

					_this.updateComplete(_index);
					_this.valid_msg[_index] = "form_saved";

					console.log("state go");
					$state.go("home");

				}).catch(function (error) {

					console.log("updatedetail.error");
					_this.updateComplete(_index);

					if (error.message != "canceled") {
						if (_index == 0) _this.error_msg_time1 = error.message;
						else _this.error_msg_time2 = error.message;
					}

				});

			};







			_this.updateComplete = function (_index) {
				_this.disable_btns[_index] = false;
			};



			_this.onClick_cbtime2 = function () {
				console.log("onClick_cbtime2 ", _this.checked2);

				if (!_this.checked2) {
					_this.dtpicker_so[2].setData(0);
					_this.dtpicker_so[3].setData(0);
				}

			};

			_this.click_exam = function () {
				$state.go("exam", { itemId: _this.itemId });
			};

		}]
	});