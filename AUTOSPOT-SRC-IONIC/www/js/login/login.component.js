angular.
module('starter').
component('login', {
	
	templateUrl: 'js/login/login.template.html',

	controller: ["LoginService", "$state", function (LoginService, $state) {
		
		console.log("login controller");
		var _this = this;
		_this.disable_btn = false;
		_this.email = "";
		_this.password = "";
		_this.showLoader = false;
		_this.headertitle = "titles_login";
		
		//$translate('login_title').then(function(_str){ _this.headertitle = _str;});


		this.clickLogin = function(){

			
			if(_this.email == "" || _this.password == ""){
				
				_this.error_msg = "form_fill_all";
				
			}
			else{
				
				_this.disable_btn = true;
				_this.error_msg = "";
				_this.showLoader = true;
				
				LoginService.validForm(this.email, this.password)
				.then(function(){
					$state.go("home");
					_this.disable_btn = false;
					_this.showLoader = false;
					
					LoginService.setLogged();
				},
				function(_msg){
					console.log("error login");
					
					if(_msg == "error_credentials") _this.error_msg = "error_credential";
					else if(_msg == "error_servor") _this.error_msg = "error_connexion_msg";
					
					
					_this.disable_btn = false;
					_this.showLoader = false;
				});
			}


		};

		
	}]    
});