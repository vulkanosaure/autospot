angular.module('starter')
	.service('LoginService', ["$q", "$http", "Config", "$window", function($q, $http, Config, $window) {

		var _codeok = "qjdk1jdlz6$";


		return {
			validForm: function(email, pw) {

				var deferred = $q.defer();
				var promise = deferred.promise;
				// var _baseurl = (Config.online) ? Config.base_url_online : Config.base_url_local;
				var _baseurl = Config.getBaseUrl();

				var _params = {
					email: email,
					password: pw
				};

				$http({
						method: 'GET',
						url: _baseurl + 'login.php',
						params: _params

					})
					.then(function successCallback(response) {
						console.log("validForm.successCallback");
						//debugger;

						if (response.data.answer == _codeok) {
							$window.localStorage.id_user = response.data.data.id;


							//storage in websql

							var d = response.data.data;
							var db = openDatabase('userdata', '1.0', 'db storing user data', 1 * 1024 * 1024);
							db.transaction(function(tx) {
								tx.executeSql('DROP TABLE `garages`');
								tx.executeSql('CREATE TABLE IF NOT EXISTS `garages` (`id`, `name`, `address`, `cp`, `city`, `email`, `tel`,`password`)');
								tx.executeSql("INSERT INTO `garages` (id, name, address, cp, city, email, tel) VALUES (?, ?, ?, ?, ?, ?, ?)", [d.id, d.name, d.address, d.cp, d.city, d.email, d.tel]);
							});


							deferred.resolve('ok');
						} else deferred.reject('error_credentials');

					}, function errorCallback(response) {
						console.log("errorCallback");
						deferred.reject('error_servor');

					});



				return promise;
			},


			setLogged: function() {
				$window.localStorage.isLogged = _codeok;
			},


			
			isLogged: function() {
				return ($window.localStorage.isLogged == _codeok);
			},


			getUserData: function(_callback) {
				
				var db = openDatabase('userdata', '1.0', 'db storing user data', 1 * 1024 * 1024);
				db.transaction(function(tx) {
					tx.executeSql('SELECT * FROM `garages`', [], function(tx, results){
						_callback(results);
					});
				});

			},
			
			getUserID: function(){
				
				return $window.localStorage.id_user;
			}

		}
	}])