angular.module('starter')
    .service('PopupQueue', ['$window', 'ServerService', '$ionicModal',
        function ($window, ServerService, $ionicModal) {

            var _this = this;
            this.tabid = null;
            this.curindex;
            this.scope;
            this.idDemandePopup = null;
            this._handlerComplete;


            this.start = function (_scope, _handlerComplete, _tabid) {
                console.log("start");
                _this.scope = _scope;
                _this.curindex = 0;
                _this.tabid = _tabid;
                _this._handlerComplete = _handlerComplete;

                var _firstid = _this.tabid[_this.curindex];
                _this.displayPopup(_firstid);


                _scope.click_valid_demande = function () {
                    console.log("click_valid_demande " + _this.scope.answer_popup);

                    if (_this.scope.answer_popup == undefined) {
                        _this.scope.error_msg = "error_field_radio";
                        console.log("yes error");
                        return;
                    }

                    _this.scope.popup_submit = true;
                    _this.scope.error_msg = "";
                    _this.validatePopupAnswer(_this.scope.idDemandePopup, _this.scope.answer_popup);
                };

                _scope.click_answer = function (_value) {
                    console.log("click_answer " + _value);
                    _this.scope.answer_popup = _value;
                };

            };



            this.displayPopup = function (_id) {
                console.log("displayPopup(" + _id + ")");
                _this.scope.idDemandePopup = _id;

                _this.scope.popup_submit = false;
                _this.scope.answer_popup = undefined;
                _this.scope.error_msg = "";

                $ionicModal.fromTemplateUrl('js/listing/form-accept-demande-modal.html', {
                    scope: _this.scope,
                    animation: 'slide-in-up',
                    backdropClickToClose: false,
                    hardwareBackButtonClose: false
                }).then(function (modal) {
                    _this.scope.modal = modal;
                    console.log("scope.modal");
                    _this.scope.modal.show();
                });


            };


            this.validatePopupAnswer = function (_iddemande, _answer) {

                ServerService.acceptDemande(_iddemande, _answer).then(function (_data) {
                    console.log("acceptDemande " + _data);
                    _this.handleEndPopup();
                })
                    .catch(function () {
                        console.log("error connexion");
                        _this.scope.error_msg = "error_connexion_msg";
                    }).then(function () {
                        _this.scope.popup_submit = false;
                    });
            };



            this.handleEndPopup = function () {
                _this.scope.modal.hide();

                _this.curindex++;
                if (_this.curindex < _this.tabid.length) {
                    var _id = _this.tabid[_this.curindex];
                    setTimeout(function () {
                        _this.displayPopup(_id);
                    }, 1000);
                }
                else {
                    //finish
                    console.log("list popup finished");
                    _this._handlerComplete();
                }


            };



        }
    ])