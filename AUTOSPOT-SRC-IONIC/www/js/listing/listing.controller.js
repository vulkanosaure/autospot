angular.
module('starter')


.controller('Listing', ["$scope", "ServerService", "LoginService", "$filter", "Config", "$ionicModal", "PopupQueue", "$translate", "$ionicPopup", "$state", function Listing($scope, ServerService, LoginService, $filter, Config, $ionicModal, PopupQueue, $translate, $ionicPopup, $state) {

	console.log("ListingController.const");

	$scope.listItems = [];
	$scope.listItemsDisplay;
	$scope.btnback_visible = false;

	$scope.headertitle = "titles_home";
	$scope.showbtnback = false;
	$scope.no_result = false;



	$scope.retry = function() {
		$scope.load();
	};


	$scope.load = function() {
		$scope.showLoader = true;
		$scope.error_connexion = false;
		console.log("ListingController.load");

		//LoginService.getUserData(function(result) {

		var _id = LoginService.getUserID();
		if(Config.debug) _id = "13";


		console.log("_id : " + _id);

		ServerService.getListing(_id).then(function(_data) {

			console.log("ListingController.ok");
			$scope.listItems = _data.list;
			console.log("len : " + $scope.listItems.length);
			$scope.showLoader = false;

			var _filterdate = $filter('date');
			var _counter = 0;
			var _timezone = '+0';

			//debugger;
			var _newDemandes = [];
			$scope.listItemsDisplay = [];

			for (var i in $scope.listItems) {
				var _item = $scope.listItems[i];

				var _ctts = _item.ct_ts;
				var _ctts2 = _item.ct_ts2;
				var _dmts = _item.demande_ts;


				_item.ts_order = _dmts;
				if (_ctts) _item.ts_order = _ctts;
				if (_ctts2) _item.ts_order = _ctts2;


				_item.displayCtdate = (_ctts || _ctts2);
				var _ctts_final = (_ctts2) ? _ctts2 : _ctts;
				console.log("_item.id : " + _item.id + ", _item.displayCtdate : " + _item.displayCtdate+", _item.accepted : "+_item.accepted);


				if (_item.displayCtdate) {
					_item.ct_date_formated = _filterdate(_ctts_final * 1000, 'dd-MM-yyyy', _timezone);
					_item.ct_time_formated = _filterdate(_ctts_final * 1000, 'HH:mm', _timezone);
				} else {
					_item.dm_date_formated = _filterdate(_dmts * 1000, 'dd-MM-yyyy', _timezone);
					_item.dm_time_formated = _filterdate(_dmts * 1000, 'HH:mm', _timezone);
				}


				
				if(_item.accepted == 0){
					_newDemandes.push(_item.id);
				}
				else{
					$scope.listItemsDisplay.push(_item);
				}

				_counter++;
			}

			
			$scope.no_result = ($scope.listItemsDisplay.length == 0);

			console.log("_newDemandes : "+_newDemandes);
			if(_newDemandes.length > 0){
				PopupQueue.start($scope, $scope.onPopupComplete, _newDemandes);
			}
			

			$scope.listItemsDisplay.sort(sortFunction);

			function sortFunction(a, b) {
				if (a["ts_order"] === b["ts_order"]) {
					return 0;
				} else {
					return (a["ts_order"] > b["ts_order"]) ? -1 : 1;
				}
			}


		}, function(_data) {
			console.log("ListingController.error");
			$scope.showLoader = false;
			$scope.error_connexion = true;

		});


	};

	$scope.onPopupComplete = function()
	{
		console.log("onPopupComplete");
		setTimeout(function(){
			$scope.load();
		}, 1000);
	};
	
	
	
	$scope.$on('clickdemo', function (event, data) {
		
			$translate(["demo_popup_content", "examglobal_continue", "examglobal_cancel", "btn_demo"]).then(function (t) {
				return t;
			})
			.then(function (t) {
				
				return $ionicPopup.confirm({
					title: t.btn_demo,
					template: t.demo_popup_content,
					okText: t.examglobal_continue,
					cancelText: t.examglobal_cancel,
					okType: "button-calm",
					cssClass: "popup_confirm"
				});
			})
			.then(function (res) {
				
				if (res) {
					console.log("click yes");
					return $state.go("exam");
				}
				else {
					console.log('click no');
					throw new Error("canceled");
				}
			})
			.catch(function (error) {
				console.log("catch, " + error.message);
			})
			.then(function () {
				console.log("then final");
			});
		
		
	});


	$scope.load();


	


}]);