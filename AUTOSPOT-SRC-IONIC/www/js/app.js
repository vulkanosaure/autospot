


angular.module('starter', ['externals', 'ionic', 'core', 'ionic-datepicker', 'ionic-timepicker', 'pascalprecht.translate', 'ngCookies'])

.run(['$ionicPlatform', 'LoginService', '$state', '$rootScope', 'Config', function($ionicPlatform, LoginService, $state, $rootScope, Config) {
	
	var env = window.cordova ? 'prod' : 'dev';
	
	console.log('env : '+env);
	Config.init(env);
	
	$ionicPlatform.ready(function() {
		
		// console.log('window.cordova.plugins : '+window.cordova.plugins);
		
		if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

			// Don't remove this line unless you know what you are doing. It stops the viewport
			// from snapping when text inputs are focused. Ionic handles this internally for
			// // a much nicer keyboard experience.
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if (window.StatusBar) {
			StatusBar.styleDefault();
		}
		
		
		console.log("LoginService.islooged : " + LoginService.isLogged());
		if (LoginService.isLogged()) {
			if(Config.debug){
				$state.go("exam", {itemId: "2"});
				//$state.go("home");
			}
			else $state.go("home");
			//else $state.go("exam", {itemId: "2"});
		} else{
			if(!Config.debug){
				console.log("goto login");
				$state.go("login");
			}
			else $state.go("exam", {itemId: "2"});
			//else $state.go("home");
		}
		
		
		
	});
		
}])



.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$translateProvider', function($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider) {
	
	
	$httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
	
	//LangService.init();
	
	$translateProvider.useCookieStorage();
	
	if(true){
		
		$translateProvider.useStaticFilesLoader({
			prefix: 'locales/locale-',
			suffix: '.json'
		});
		
	}
	else{
		var base_url_online = 'https://www.autospot.ch/appbackend/';
		$translateProvider.useUrlLoader(base_url_online + "admin/lang/include/admin-lang-export-json.php");
		$translateProvider.forceAsyncReload(true);
	}

	
	$translateProvider.preferredLanguage('fr');
	
	



	$stateProvider

		.state('login', {
			url: '/login',
			template: '<login></login>',
		})
		.state('home', {
			url: '/home',
			controller: 'Listing',
			templateUrl: 'js/listing/listing.template.html'
		})
		.state('detail', {
			url: '/detail/:itemId',
			template: "<detail></detail>"
		})
		.state('exam', {
			url: '/exam/:itemId',
			template: '<examcomp></examcomp>'
		})

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/login');
}]);