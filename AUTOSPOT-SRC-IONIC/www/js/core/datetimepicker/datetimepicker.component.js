//wrapping dateformat library

angular.module('externals',[])
.factory('dateFormat', function ($window) {

	if($window.dateFormat){
		$window._thirdParty = $window._thirdParty || {};
		$window._thirdParty.dateFormat = $window.dateFormat;
		try { delete $window.dateFormat; } catch (e) {$window.dateFormat = undefined;}
	}
	var service = $window._thirdParty.dateFormat;
	return service;

});





angular.
module('core').
component('datetimepicker', {
	templateUrl: 'js/core/datetimepicker/datetimepicker.template.html',
	bindings: {
		type: '@',
		/*onUpdate: '&',*/
		so: '='
	},
	controller: DateTimePickerController   
});




function DateTimePickerController(ionicDatePicker, ionicTimePicker, dateFormat) {

	this.label = "";
	this.type = "";
	this.outputTS = 0;
	this.outputStr = "";
	this.visible = true;
	this.isEditable = true;
	
	//this.sharedData = {"value" : 2};

	var _this = this;


	this.onSelect = function(val)
	{
		console.log("this.onSelect(", val, ")");
		console.log("childattr : "+_this.childattr);
		
		
		console.log('Return value from the datepicker popup is : ' + val, _this.outputTS);	
		
		if (typeof (val) === 'undefined') {
			console.log('Time not selected');
		}else{
			
			var _date = new Date();
			var _offset = _date.getTimezoneOffset() * 60;
			if(_this.type == "date") val -= _offset * 1000;
			
			if(_this.type == "time") val *= 1000;
			_this.so.setData(val, false);
			
		}
	};
	
	
	
	this.so.reset = function()
	{
		_this.outputTS = 0;
		_this.outputStr = "";
	};
	
	this.so.setVisible = function(_value)
	{
		_this.visible = _value;
	};
	
	
	
	this.so.setData = function(_ts, _utc)
	{
		if(_utc == undefined) _utc = true;
		//problem : click sur calendar renvoit local time
		
		if(_ts == 0){
			_this.outputStr = "";
			_this.outputTS = _ts;
		}
		else{
			if(_this.type == "date"){
				
				var d = new Date(_ts);
				d.setUTCHours(0);
				d.setUTCMinutes(0);
				d.setUTCSeconds(0);
				_this.outputTS = d.getTime() / 1000;
				
				_this.outputStr = ("0" + d.getUTCDate()).slice(-2) + "-" + ("0"+(d.getUTCMonth()+1)).slice(-2) + "-" +
				d.getUTCFullYear();

			}
			else if(_this.type == "time"){
				if (typeof (_ts) === 'undefined') {
					console.log('Time not selected');
				} else {
					var _newdate = new Date(_ts);		//new Date contient 6h de trop a ce stade
					_this.outputTS = _newdate.getUTCHours() * 3600 + _newdate.getUTCMinutes() * 60;
					
					var d = _newdate;
					_this.outputStr = ("0" + d.getUTCHours()).slice(-2) + ":" + ("0" + d.getUTCMinutes()).slice(-2);
				}
			}
		}
		_this.so.setVisible(true);
		
	};
	
	this.so.getOutput = function()
	{
		return _this.outputTS;
	};
	
	this.so.setEditable = function(_value)
	{
		_this.isEditable = _value;
	};



    
    //inputs

    this.$onChanges = function (_changes) {
    	if(_changes.type){
    		var _type = _changes.type.currentValue;
    		if(_type == "date"){
    			this.label = "form_date";
    		}
    		else if(_type == "time"){
    			this.label = "form_time";
    		}
    	}
    };




    //___________________________________________________
    //
    //
    

	this.param_date = {
		callback: this.onSelect,
        inputDate: new Date(),      //Optional
        mondayFirst: true,          //Optional
        closeOnSelect: true,       //Optional
        templateType: 'popup'       //Optional
    };

    this.param_time = {
    	callback: this.onSelect,
        inputTime: 50400,   //Optional
        format: 24,         //Optional
        step: 5,           //Optional
    };
    
    this.choose = function()
    {
    	console.log("choose "+this.type);
    	this.open(this.type);
    };
    
    this.open = function(_type)
    {
    	if(_type == "date") ionicDatePicker.openDatePicker(this.param_date);
    	else if(_type == "time") ionicTimePicker.openTimePicker(this.param_time);
    };


}