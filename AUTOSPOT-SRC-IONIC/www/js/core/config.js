// console.log('process env : '+process.env);


angular.module('core')

.factory('Config', function(){
	return{
		
		
		online:true,
		environement:'online',
		debug_client:false,
		debug:false,
		debug_integration: false,
		question_debug: "",
		
		
		init:function(env)
		{
			if(env == 'dev'){
				
				this.online = false;
				// this.online = true;
				this.environement = 'local';
				// this.environement = 'online';
				this.debug_client = false;
				this.debug = false;
				this.debug_integration =  false;
				// this.question_debug =  "ec07dqmi";
				//this.debug = false si question_debug
				
			}
			else{
				
				this.online = true;
				this.environement = 'online';
				this.debug_client = false;
				this.debug = false;
				this.debug_integration =  false;
				this.question_debug =  "";
				
			}
		},
		
		
		//add https
		//attention définis à 2 endroits
		base_url_online:'https://www.autospot.ch/appbackend/',	
			
		base_url_local:'http://localhost/CODEURS/autospot/WEBSITE_WWW/appbackend/',
		base_url_remotelocal:'http://192.168.1.7/CODEURS/autospot/WEBSITE_WWW/appbackend/',
		
		
		getBaseUrl:function(){
			var output;
			if(this.environement == 'online') output = this.base_url_online;
			else if(this.environement == 'local') output = this.base_url_local;
			else if(this.environement == 'remotelocal') output = this.base_url_remotelocal;
			console.log('base_url : '+output);
			return output;
		},
		
		
		
	};
});