angular.
module('core').
component('header', {

	bindings: {
		title: "<",
		showback: "<",
		demo: "<",
	},

	templateUrl: 'js/core/header/header.template.html',

	controller: ["$scope", "$ionicModal", "$translate", "$cookies", "$state", "Config", function($scope, $ionicModal, $translate, $cookies, $state, Config) {
		
		var _this = this;
		_this.obj = {show_debug_exam : false};
		
		this.lang = $cookies.get("NG_TRANSLATE_LANG_KEY");


		this.clickBack = function() {
			$scope.$emit('clickback');

		};
		
		this.clickDemo = function() {
			console.log('header.clickDemo');
			$scope.$emit('clickdemo');
		};

		this.clickDebugExam = function(_direction){
			$scope.$emit("clickDebugExam", _direction);
		}
		
		this.clickLanguage = function()
		{
			$scope.modal.show();
		};
		
		$scope.langitems = [
			{code:"fr", label:"FR"},
			{code:"de", label:"DE"},
			{code:"it", label:"IT"},
		];
		
		this.select = function()
		{
			console.log("select "+this.lang);
			$scope.modal.hide();
			$translate.use(this.lang);
		};


		$ionicModal.fromTemplateUrl('modal-lang.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
			
		});
		
		_this.obj.show_debug_exam = ($state.current.name == "exam" && Config.debug_client);
		
		

	}]
});