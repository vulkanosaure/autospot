angular.module('core')
	.service('ServerService', ["$q", "$http", "Config", "LoginService", function($q, $http, Config, LoginService) {
		
		
		var _obj = {

			updateDetail: function(_params) {

				console.log("updatedetail");
				console.log(_params);
				//debugger;

				var deferred = $q.defer();
				var promise = deferred.promise;
				// var _baseurl = (Config.online) ? Config.base_url_online : Config.base_url_local;
				var _baseurl = Config.getBaseUrl();
				
				var _encodeparams = function ObjecttoParams(obj) {
				    var p = [];
				    for (var key in obj) {
				        p.push(key + '=' + encodeURIComponent(obj[key]));
				    }
				    return p.join('&');
				};
				_params = _encodeparams(_params);
				
				
				$http({
					method: 'POST',
					url: _baseurl + 'update_detail.php',
					data: _params,
					headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}

				})

				.then(function successCallback(response) {
					console.log("updateDetail.successCallback");
					deferred.resolve(response.data);

				}, function errorCallback(response) {
					console.log("errorCallback");
					deferred.reject(new Error("error_connexion_msg"));

				});

				return promise;

			},


			getListing: function(_id) {

				console.log("ServerService.getListing");
				var deferred = $q.defer();
				var promise = deferred.promise;
				// var _baseurl = (Config.online) ? Config.base_url_online : Config.base_url_local;
				var _baseurl = Config.getBaseUrl();
				var _params = {
					"iduser": _id
				};

				$http({
						method: 'GET',
						url: _baseurl + 'load.php',
						params: _params

					})
					.then(function successCallback(response) {
						console.log("getListing.successCallback");
						deferred.resolve(response.data);

					}, function errorCallback(response) {
						console.log("errorCallback");
						deferred.reject('error_servor');

					});

				return promise;
			},



			getItem: function(_id) {

				console.log("ServerService.getItem(" + _id + ")");
				var deferred = $q.defer();
				var promise = deferred.promise;
				// var _baseurl = (Config.online) ? Config.base_url_online : Config.base_url_local;
				var _baseurl = Config.getBaseUrl();
				var _params = {
					id: _id
				};

				$http({
						method: 'GET',
						url: _baseurl + 'loaditem.php',
						params: _params

					})
					.then(function successCallback(response) {
						console.log("successCallback");
						deferred.resolve(response.data);
						_obj.item = response.data;

					}, function errorCallback(response) {
						console.log("errorCallback");
						deferred.reject('error_servor');

					});

				return promise;
			},


			acceptDemande: function(_id, _value)
			{
				console.log("ServerService.acceptDemande(" + _id + ", "+_value+")");
				var deferred = $q.defer();
				var promise = deferred.promise;
				// var _baseurl = (Config.online) ? Config.base_url_online : Config.base_url_local;
				var _baseurl = Config.getBaseUrl();
				var _params = {
					id: _id,
					value: _value,
					id_garage: LoginService.getUserID()
				};

				$http({
						method: 'GET',
						url: _baseurl + 'accept_item.php',
						params: _params

					})
					.then(function successCallback(response) {
						console.log("successCallback");
						deferred.resolve(response.data);

					}, function errorCallback(response) {
						console.log("errorCallback");
						deferred.reject('error_servor');

					});

				return promise;

			},


		}
		
		return _obj;
	}])