angular.module('core')
.filter('yesNo', function() {
    return function(input) {
        return input ? 'OUI' : 'NON';
    }
});