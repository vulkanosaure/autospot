angular.module('core')
	.service('CameraService', ['$rootScope', '$q', function($rootScope, $q) {

		var _this = this;

		_this.getPicture = function(options) {

			// init $q
			var deferred = $q.defer();
			var _cameraSupport = (navigator.camera != undefined);
			console.log("CameraService.getPicture "+_cameraSupport);
			
			if (!_cameraSupport) {

				// create file input without appending to DOM
				var fileInput = document.createElement('input');
				fileInput.setAttribute('type', 'file');

				fileInput.onchange = function() {
					var file = fileInput.files[0];
					var reader = new FileReader();
					reader.readAsDataURL(file);
					reader.onloadend = function() {
						$rootScope.$apply(function() {
							// strip beginning from string
							//var encodedData = reader.result.replace(/data:image\/jpeg;base64,/, '');
							//var encodedData = reader.result.split(',')[1];
							var encodedData = reader.result;
							deferred.resolve(encodedData);
						});
					};
				};

				fileInput.click();

			} else {

				// set some default options
				var defaultOptions = {
					quality: 75,
					destinationType: Camera.DestinationType.DATA_URL,
					allowEdit: false
				};

				// allow overriding the default options
				options = angular.extend(defaultOptions, options);

				// success callback
				var success = function(imageData) {
					imageData = "data:image/jpeg;base64," + imageData;
					
					$rootScope.$apply(function() {
						deferred.resolve(imageData);
					});
				};

				// fail callback
				var fail = function(message) {
					console.log("fail");
					$rootScope.$apply(function() {
						deferred.reject(message);
					});
				};

				// open camera via cordova
				navigator.camera.getPicture(success, fail, options);

			}

			// return a promise
			return deferred.promise;

		};


	}])