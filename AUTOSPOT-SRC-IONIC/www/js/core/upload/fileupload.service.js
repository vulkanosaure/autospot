angular.module('core')
	.service('FileUploadService', ["$q", "$http", "Config", "$window", "Config", function($q, $http, Config, $window, Config) {

		var _this = this;

		_this.upload = function(_dataURI, _filename) {

			console.log("FileUploadService.upload()");
			
			// var _baseurl = (Config.online) ? Config.base_url_online : Config.base_url_local;
			var _baseurl = Config.getBaseUrl();


			var fd = new FormData();
			var imgBlob = _this.dataURItoBlob(_dataURI);
			fd.append('myfile', imgBlob);
			fd.append('filename', _filename);
			
			
			$http.post(
					_baseurl + 'upload_img.php',
					fd, {
						transformRequest: angular.identity,
						headers: {
							'Content-Type': undefined
						}
					}
				)
				.success(function(response) {
					console.log('success', response);
				})
				.error(function(response) {
					console.log('error', response);
				});

		};



		//you need this function to convert the dataURI
		_this.dataURItoBlob = function(dataURI) {
			
			
			//var _start = dataURI.substr(0, 50);
			//desktop, "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwA"
			//webview android : "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD"
			
			var binary = atob(dataURI.split(',')[1]);
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
			var array = [];
			for (var i = 0; i < binary.length; i++) {
				array.push(binary.charCodeAt(i));
			}
			return new Blob([new Uint8Array(array)], {
				type: mimeString
			});
		};
		
		
		
		_this.convertImgToDataURLviaCanvas = function(_imgurl, callback) {

			var canvas = document.createElement("canvas");
			context = canvas.getContext('2d');

			base_image = new Image();
			base_image.src = _imgurl;
			base_image.onload = function() {
				console.log("base_image.onload");
				context.drawImage(base_image, 140, 110);
				var _dataURI = canvas.toDataURL();
				callback(_dataURI);
			}

		}


	}])